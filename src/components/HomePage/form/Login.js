import React,{Component} from 'react'
import './Login.css'
import { connect } from 'react-redux';
// import {LoginCheck} from '../../../action/accountAction'
import {LoginCheck,showLoader,clearLoginErr} from '../../../actions/accountAction'
// import {ChangeLogin} from '../../../action/donationActions'
import {ChangeLogin} from '../../../actions/donationActions'
import Donationform from '../../DonationForm/form/Donationform'
import Loader from 'react-loader-spinner';
import Popup from 'reactjs-popup' 
import Button from 'react-bootstrap/Button'
import MediaQuery from 'react-responsive'
import { GoogleLogin } from 'react-google-login';

class Login extends Component{

    state={
        email:'',
        password:'',
        mobilesignup:false,

    }

    componentWillReceiveProps(nextProps){
      if(this.props.ErrMsg != nextProps.ErrMsg){
      setTimeout(()=>{this.props.clearLoginErr()},3000)

      }
    }
    // Toggle Form
    ChangeForm = () =>{
        this.props.formChanged()
        }
        mobileopenModal=()=>{
            this.setState({ mobilesignup: true })
          }
    mobilecloseModal=()=>{
            this.setState({ mobilesignup: false })
          } 

        onChange = e => this.setState({ [e.target.name]: e.target.value });
    render(){
       const Login = (e) =>{
            e.preventDefault()
            const {email,password} = this.state
           const details={
                email,
                password,
            }
            this.props.showLoader()
            this.props.LoginCheck(details)
            // if(this.props.user_id !== null){
            //     this.props.ChangeLogin()
            // }
        }
        console.log(this.props.ErrMsg,"LogErr");
         
        const responseGoogle = (response) => {
            // console.log(response.profileObj.email);
            const details={
              email:response.profileObj.email,
              password:13,   
            }
            this.props.LoginCheck(details)
          }
          const responseGoogleFailure=()=>{
            
          }
        // var  inputstye={ margin:"1em 2em",outline:'0' ,border:'0',width:'76%',
        // background: 'transparent',borderBottom :'1px solid #c5c0c0' ,color:'#c5c0c0'}
        if(this.props.UserToken === null || this.props.userId === null){
            // alert("page")
            // alert(this.props.userId)
        return(
            
             // body of the Doner Login  form page

            <div>
            {/* Quotes */}
            <MediaQuery minWidth={539}>

            <p id='L_quote1'>
            <b>CHANGE LIVES RIGHT WHERE YOU ARE</b>
             </p>
             <p id='L_quote2'>
             #Donate Books  
             And Help Readers
             </p>
             {/* <img  src={require('./DonationHomePagetopicon.png')} style={{ position:'absolute',marginTop:'30%',marginLeft:'32%' }} alt=""/> */}
             <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/DonationHomePagetopicon.96d433d4.png`} id="donationsignupimg" alt=""/>


            <p id="L_formabove">
            <b>Please Log In To Donate Your Books </b>
            </p>
            {/* // Form area */}
            <div id="L_formdiv">  
            <form id="L_form" onSubmit={Login}>

                        <input type="email" placeholder="Enter Email ID" 
            id="inputstye" name="email" value={this.state.email}
            onChange={e => this.setState({email: e.target.value})}
            required
            maxLength="40"
            />
            <input type="password" placeholder="Enter Password" 
            //  style={inputstye} name="password" value={this.state.password}
            id="inputstye" name="password" value={this.state.password}

             onChange={e => this.setState({password: e.target.value})}
             required
            maxLength="40"

            />
            <span style={{ color:'red',fontSize:'90%',marginLeft:'4%'}}>{this.props.ErrMsg}</span>
            <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>
            <input type="submit" value="Log In" id="L_login" />
            
            <input type="button" value="New To MyPustak? Sign Up" onClick={this.ChangeForm} id="L_redirect" />
            <div id="Google"> 
            {/* ID=Google from userLogin.css */}
            <GoogleLogin 
                theme="dark"
                onSuccess={responseGoogle}
                onFailure={responseGoogleFailure} 
                buttonText="Log In"
                clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
                cookiePolicy={'single_host_origin'} />
            </div>
            </form> 
            
        </div>
        
        </MediaQuery>

        </div>
    
        )}else{
            // alert(this.props.userId)
            return(<Donationform/>)
        }
    }
}


const mapStateToProps = state => ({
    userId:state.accountR.userId,
    UserToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
    loader:state.accountR.loading,
  })


export default connect(mapStateToProps,{LoginCheck,ChangeLogin,showLoader,clearLoginErr})(Login)