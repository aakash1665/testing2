import React,{Component} from 'react'
import './form.css'
import Signup from './Signup'
import Login from './Login'
import Static from '../static/Static'
import Donationform from '../../DonationForm/form/Donationform'
import MainHeader from '../../MainHeader';
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import MainFooter from '../../MainFooter'
class Form extends Component{

    state={
        formChanged:false,
        LoginSuccess:false
    }

    // Toggle Form
    loginSuccess = () =>{
        this.setState({LoginSuccess:!this.state.LoginSuccess})
        this.setState({formChanged:!this.state.formChanged})
        }

    ChangeForm = () =>{
        // alert("inform")
        this.setState({formChanged:!this.state.formChanged})
        
        }

    render(){
        if(this.state.formChanged === false)
        {
            return( 
                <div><div id="signupformbody" >
                <MainHeader/>
                <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/DONATIONPageIMAGE1.84a8d95b.jpg`} id="donatebacksignupimg"/>
                <Signup formChanged={this.ChangeForm}/>
                </div>
                <div id={(this.props.userToken === null)?"ToggleDivLogout":"ToggleDiv"}>
        
                <Static/> 
                </div>

                
                </div>);
        }

        else{
            return( 
                <div>
                <div  id="signupformbody" >
                <MainHeader/>
                <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/DONATIONPageIMAGE1.84a8d95b.jpg`} id="donatebacksignupimg"/>
                <Login formChanged={this.ChangeForm}/>
                </div>
                <div id={(this.props.userToken === null)?"ToggleDivLogout":"ToggleDiv"}>
                <Static/>
                </div>
                {/* <MainFooter/> */}
                </div>
                );
        }
    
    }
}
const mapStateToProps = state => ({
    pickup:state.donationR.pickup,
    userToken:state.accountR.token,
  })
export default connect(mapStateToProps,null)(Form)


