import React,{Component} from 'react'
import './Signup.css'
import Popup from 'reactjs-popup' 
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import PropTypes from 'prop-types'
import {signupCheck,showLoader,LoginCheck} from '../../../actions/accountAction'
// import {ChangeLogin} from '../../../action/donationActions'
import {ChangeLogin} from '../../../actions/donationActions';
import Modal from 'react-awesome-modal';
import Donationform from '../../DonationForm/form/Donationform'
import Loader from 'react-loader-spinner';
import Button from 'react-bootstrap/Button'
import MediaQuery from 'react-responsive'

import { GoogleLogin } from 'react-google-login';

class Signup extends Component{

    state={
        fullname:'',
        email:'',
        mobile:'',
        password:'',
        Conpassword:'',
        toDOnationForm:false,
        mobilesignup:false,
        ToggleForm:true,
    }
    // Toggle Form
    ChangeForm = () =>{
        this.props.formChanged()
        }
    mobileopenModal=()=>{
            this.setState({ mobilesignup: true })
          }
    mobilecloseModal=()=>{
            this.setState({ mobilesignup: false })
          } 

        
    render(){
        let MinPasswordErr=""
        let PhoneErr=""
        if(this.state.password.length < 6){
          MinPasswordErr=<span id="passsigncheck">Minimum password length must be 6</span>
        }else{
            MinPasswordErr=""
        }
        console.log(this.state.mobile);
        
        if(this.state.mobile.length !== 10){
            //   alert("stop")
           PhoneErr = <span id="mobilecheck">Enter 10 digits</span>
            // document.getElementsById("mobile").disable=true
            // this.mobilechecker()
        }else if(isNaN(this.state.mobile)){
             PhoneErr = <span id="mobilecheck">Enter digits</span>
        }else{
             PhoneErr=""
        }
      const signup = (e) =>{
            e.preventDefault()
            const {fullname,mobile,email,password} = this.state
            const details={
                fullname,
                email,
                password,
                mobile,
            }
            if(MinPasswordErr === "" && PhoneErr === ""){
            this.props.showLoader()
            this.props.signupCheck(details)
            this.ChangeForm()
        }    
            else{
                // alert("eww")
            }
            // if(this.props.user_id !== null){
            //     this.props.ChangeLogin()
            // }
            if(this.props.userId !== undefined ){
              
                this.setState({toDOnationForm:true})
            }else{
                // alert("okkpp")
            }
        }

        const Login = (e) =>{
            e.preventDefault()
            const {email,password} = this.state
           const details={
                email,
                password,
            }
            this.props.showLoader()
            this.props.LoginCheck(details)
            // if(this.props.user_id !== null){
            //     this.props.ChangeLogin()
            // }
        }
        const responseGoogle = (response) => {
            // console.log(response.profileObj.email);
            const details={
              email:response.profileObj.email,
              password:13,   
            }
            this.props.LoginCheck(details)
          }
          const responseGoogleFailure=()=>{
            
          }
        // console.log(`${this.props.visible} + okk`)
        // var  inputstye={ margin:"1% 13%",outline:'0' ,border:'0',width:'76%',
        // background: 'transparent',borderBottom :'1px solid #c5c0c0' ,color:'#c5c0c0',fontSize: '1.5vw'}
        
        // if(this.props.userId === undefined){
            // let Hits= 1
            if(this.props.UserToken === null ){
        return(
            
             // body of the form page

            <div>
            {/* Quotes */}
            <MediaQuery maxWidth={538}>
            <Button as="input" type="submit" value="Please Sign Up/Log In to Fill The Form" id="mobilesignup" onClick={this.mobileopenModal}/>
            <Popup 
                       open={this.state.mobilesignup}
                       contentStyle={{ 
                         width:'90%',
                        //  height:'90%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         border:'0px',
                        }}
                       >
                       <div>
                       <a className="close" onClick={this.mobilecloseModal}
                       style={{color: 'black',backgroundColor: 'white'}}>
              &times;
            </a>
                  
                      {/* <div id="S_formdiv"> */}
            {this.state.ToggleForm?<form id="S_form" onSubmit={signup}>
            {this.props.Userstatus}
                <input type="text" placeholder="Enter Full Name" id="inputstye"
                name="fullname" value={this.state.fullname}
                onChange={e => this.setState({fullname: e.target.value})}
                required
                maxLength="40"
                />
                <input type="text" placeholder="Mobile Number" id="inputstye"
                 name="mobile" value={this.state.phone}
                 onChange={e => this.setState({mobile: e.target.value})}
                 required
                 maxLength="10"
                />
               {PhoneErr}
                <input type="email" placeholder="Email Address" id="inputstye"
                name="email" value={this.state.email}
                onChange={e => this.setState({email: e.target.value})}
                required
                maxLength="40"
                />
                {/* <input type="email" placeholder="Enter Email ID/Mobile Mumber" 
                    id="inputstye" name="email" value={this.state.email}
                    onChange={e => this.setState({email: e.target.value})}
                    required
                    /> */}
                <input type="password" placeholder="Create Password" id="inputstye"
                name="password" value={this.state.password}
                onChange={e => this.setState({password: e.target.value})}
                required
                maxLength="40"
                />
                {MinPasswordErr}
                <input type="password" placeholder="Confirm Password" id="inputstye"
                name="Conpassword" value={this.state.Conpassword}
                onChange={e => this.setState({Conpassword: e.target.value})}
                required
                maxLength="40"
                /> 
                {(this.state.password !== this.state.Conpassword) && <span style={{ color:'white',marginLeft:'18%'}}>Password Do Not Match</span>}
                <input type="submit" value="Sign Up" id="S_signup"     
                />   
               
                <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>
                <input type="button" value="Already Have An Account? Log In" onClick={()=>this.setState({ToggleForm:!this.state.ToggleForm})} id="S_redirect"/>
                <div id="Google"> 
            {/* ID=Google from userLogin.css */}
                <GoogleLogin 
                theme="dark"
                onSuccess={responseGoogle}
                onFailure={responseGoogleFailure} 
                buttonText="Sign Up"
                clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
                cookiePolicy={'single_host_origin'} />
                </div>
            </form> :<div>
            <form id="L_form" onSubmit={Login}>
            <input type="email" placeholder="Enter Email ID" 
            id="inputstye" name="email" value={this.state.email}
            onChange={e => this.setState({email: e.target.value})}
            required
            maxLength="40"
            />
            <input type="password" placeholder="Enter Password" 
            id="inputstye" name="password" value={this.state.password}
             onChange={e => this.setState({password: e.target.value})}
             required
            maxLength="40"

            />
            <span style={{ color:'white',fontSize:'90%',marginLeft:'4%'}}>{this.props.ErrMsg}</span>
            <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                        </Popup>
            <input type="submit" value="Log In" id="L_login" />
            
            <input type="button" value="New To MyPustak? Sign Up" onClick={()=>this.setState({ToggleForm:!this.state.ToggleForm})} id="L_redirect" />
            <div id="Google"> 
            {/* ID=Google from userLogin.css */}
            <GoogleLogin 
                theme="dark"
                onSuccess={responseGoogle}
                onFailure={responseGoogleFailure} 
                buttonText="Sign Up"
                clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
                cookiePolicy={'single_host_origin'} />
                </div>
            </form> 
            </div>}
    </div>
    {/* </div> */}
                       </Popup>
                       </MediaQuery>
          
            <p id='S_quote1'>
            <b>CHANGE LIVES RIGHT WHERE YOU ARE</b>
             </p>
             <p id='S_quote2'>
             #Donate Books 
             And Help Readers
             </p>
             <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/DonationHomePagetopicon.96d433d4.png`} id="donationsignupimg" alt=""/>

              
            {/* // Form area */}
            <MediaQuery minWidth={539}>
                         {/* from above para */}
                         <p id="S_formabove">
            <b>Please Sign Up To Donate Your Books</b>
            </p>

            <div id="S_formdiv">
            
            <form id="S_form" onSubmit={signup}>
            {this.props.Userstatus}
                <input type="text" placeholder="Enter Full Name" id="inputstye"
                name="fullname" value={this.state.fullname}
                onChange={e => this.setState({fullname: e.target.value})}
                required
                maxLength="40"
                />
                <input type="text" placeholder="Mobile Number" id="inputstye"
                 name="mobile" value={this.state.phone}
                 onChange={e => this.setState({mobile: e.target.value})}
                 required
                 maxLength="10"
                />
               {PhoneErr}
                <input type="email" placeholder="Email Address" id="inputstye"
                name="email" value={this.state.email}
                onChange={e => this.setState({email: e.target.value})}
                required
                maxLength="40"
                />
                {/* <input type="email" placeholder="Enter Email ID/Mobile Mumber" 
                    id="inputstye" name="email" value={this.state.email}
                    onChange={e => this.setState({email: e.target.value})}
                    required
                    /> */}
                <input type="password" placeholder="Create Password" id="inputstye"
                name="password" value={this.state.password}
                onChange={e => this.setState({password: e.target.value})}
                required
                maxLength="40"
                />
                {MinPasswordErr}
                <input type="password" placeholder="Confirm Password" id="inputstye"
                name="Conpassword" value={this.state.Conpassword}
                onChange={e => this.setState({Conpassword: e.target.value})}
                required
                maxLength="40"
                /> 
                {(this.state.password !== this.state.Conpassword) && <span style={{ color:'white',marginLeft:'18%'}}>Password Do Not Match</span>}
                <input type="submit" value="Sign Up" id="S_signup"     
                />   
               
                <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>
                <input type="button" value="Already Have An Account? Log In" onClick={this.ChangeForm} id="S_redirect"/>
            {/* ID=Google from userLogin.css */}
                <div id="Google"> 
                    <GoogleLogin 
                    theme="dark"
                    onSuccess={responseGoogle}
                    onFailure={responseGoogleFailure} 
                    buttonText="Sign Up"
                    clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
                    cookiePolicy={'single_host_origin'} />
                </div>
            </form> 
    </div>
    </MediaQuery>
        </div>
        )
    }else{
            return(
            <div><Donationform/>
            </div>)
        }
    }}
const mapStateToProps = state => ({
    visible:state.donationR.visible,
    userId:state.accountR.userId,
    Userstatus:state.accountR.status,
    UserToken:state.accountR.token,
    loader:state.accountR.loading,
})
// const mapStateToProps = state => ({
//     userId:state.accountR.userId
//   })

export default connect(mapStateToProps,{signupCheck,ChangeLogin,showLoader,LoginCheck})(Signup);
