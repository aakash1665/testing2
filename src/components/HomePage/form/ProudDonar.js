import React, { Component } from 'react';
import './prouddonors.css'
import {Helmet} from 'react-helmet';
import MainHeader from '../../MainHeader';
import MainFooter from '../../MainFooter';
import { connect } from 'react-redux';
import {donardetails,searchdonor,setResponseMsg} from '../../../actions/prouddonarAction'
// import JwPagination from 'jw-react-pagination';
import ReactPaginate from 'react-paginate';
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import Table from 'react-bootstrap/Table'
import MediaQuery from 'react-responsive';
class ProudDonor extends Component {
  state={
    SearchValue:'',
    name:'',
    city:'',
    state:'',
    donations:'',
    avatar:'',
    // exampleItems:[...Array(150).keys()].map(i => ({ id: (i+1) })),
    // pageOfItems: [],
    // data:[20]
  }
  // see fro pagination page:http://jasonwatmore.com/post/2018/04/10/npm-jw-react-pagination-component
  onChangePage=(pageOfItems)=> {
    // update local state with new page of items
    // alert(pageOfItems.map(id=>id.id))
    this.setState({ pageOfItems });
    // this.onChangePage = this.onChangePage.bind(this);
}
  componentDidMount() {
    window.scrollTo(0, 0);
    this.props.donardetails(1)}
  render() {
    const {getdonar}=this.props
    const customStyles = {
      ul: {
          backgroundColor: 'white'
      },
      li: {
          border: '1px solid green'
      },
      a: {
          color: 'blue'
      }
    };
   const handlePageClick = data => {
      // let selected = data.selected;
      // let offset = Math.ceil(selected * this.props.perPage);
  
      // this.setState({ offset: offset }, () => {
      //   this.loadCommentsFromServer();
      // });
      this.props.donardetails(data.selected+1)
      // alert(data.selected+1)
      window.scrollTo(0, 0);
  };
  // const paginationLi={style={ display:'inline'}}
  const SearchSubmit =(e)=> {
      e.preventDefault();
      // alert(this.state.SearchValue)
      this.props.searchdonor(this.state.SearchValue)
  }
  const ValueChange=(e)=>{
    this.props.setResponseMsg()
    this.setState({[e.target.name]:e.target.value})
    if(e.target.value.length == 0){
    this.props.donardetails(1);

    }
  }
    return (
      
   <div>
         {/* desktop */}
    <MainHeader/>
<Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style></Helmet>
<div >
<p id="Our-proud-donor">Our Proud Donors</p>
 <hr id="phr"/>
 {/* <label id="totalbook">Total Book Donation So Far = <b>#98967</b></label>
 <label id="nodonar">No Of Donors = <b>#1617</b></label> */}
  {/* <p id="Donar-details">Our Donor's Details</p> */}
  <hr id="phr1"/>

  {/*---------------------------------desktop-------------------------------------------*/}
  <MediaQuery minWidth={992}>
                  
        <div id="DonorSearch">
        <form id="DonorSearchForm" onSubmit={SearchSubmit}>
          <input type="text"
          placeholder="Enter Donation ID OR Name" 
          id="DonorSearchInput"
          maxLength="30"
          name="SearchValue"
          value={this.state.SearchValue}
          onChange={ValueChange}  
            />
          <input type="submit" value="Search" id="DonorSearchbtn"/>
        </form>
        </div>
        <p id={(this.props.noResultFound)?"NoResponse":"ShowResponse" }>NO RESULT FOUND</p>

  </MediaQuery>
  {/*------------------------------tab------------------------------------------------------*/}
  <MediaQuery maxWidth={991} and minWidth={768}>
                    
  <div id="DonorSearch">
        <form id="DonorSearchForm" onSubmit={SearchSubmit}>
          <input type="text"
          placeholder="Enter Donation ID OR Name" 
          id="DonorSearchInput"
          maxLength="30"
          name="SearchValue"
          value={this.state.SearchValue}
          onChange={ValueChange}  
            />
          <input type="submit" value="Search" id="DonorSearchbtn"/>
        </form>
        </div>
        <p id={(this.props.noResultFound)?"NoResponse":"ShowResponse" }>NO RESULT FOUND</p>

  </MediaQuery>
  {/* ------------------------------larger mobile---------------------------------------*/}
  <MediaQuery maxWidth={767} and minWidth={539}>
  <div id="DonorSearch">
        <form id="DonorSearchForm" onSubmit={SearchSubmit}>
          <input type="text"
          placeholder="Enter Donation ID OR Name" 
          id="DonorSearchInput"
          maxLength="30"
          name="SearchValue"
          value={this.state.SearchValue}
          onChange={ValueChange}  
            />
          <input type="submit" value="Search" id="DonorSearchbtn"/>
        </form>
        </div>
        <p id={(this.props.noResultFound)?"NoResponse":"ShowResponse" }>NO RESULT FOUND</p>

  </MediaQuery>
  {/*---------------------------------mobile---------------------------------*/}
  <MediaQuery maxWidth={538} >
  <div id="DonorSearch">
        <form id="DonorSearchForm" onSubmit={SearchSubmit}>
          <input type="text"
          placeholder="Enter Donation ID OR Name" 
          id="DonorSearchInput"
          maxLength="30"
          name="SearchValue"
          value={this.state.SearchValue}
          onChange={ValueChange}  
            />
          <input type="submit" value="Search" id="DonorSearchbtn"/>
        </form>
        </div>
        <p id={(this.props.noResultFound)?"NoResponse":"ShowResponse" }>NO RESULT FOUND</p>
  </MediaQuery>
  <div id="PDdiv">
{/* <input type="text" placeholder="Search By Name" name="search" id="searchinput"></input> */}
  <MediaQuery  minWidth={1366}  > 
  {/* <InputGroup className="mb-3" id="searchinput">
    <FormControl
      aria-label="Default"
      aria-describedby="inputGroup-sizing-default"
      placeholder="Search By Donar Name"
    />
  </InputGroup>
  {/* <button type="submit" id="btn"> Search</button> */}
  {/* <table id="table"> */}
  {/* <Button variant="primary" size="sm" id="btn">
      Search
    </Button>  */}
  <Table responsive="lg"  id="table">
  <thead style={{backgroundColor:'#dde5ed'}}>
  <tr id="heading">
  <th style={{textAlign:'center'}}>Photo</th>
  <th  style={{textAlign:'center'}}>Name</th>
  <th style={{textAlign:'center'}}>State</th>
  <th style={{textAlign:'center'}}>City</th>
  <th style={{textAlign:'center'}}>Donated Books</th>
  </tr>
  </thead>
  {getdonar.map(donardetails=>
    <tbody >
  <tr>
 <th  style={{textAlign:'center'}}><span id="photo"><img src={`https://ik.imagekit.io/kbktyqbj4rrik/uploads/avatar/${donardetails.avatar}`} alt="" style={{borderRadius: '50%',height:'51px',width:'53px'}}></img></span></th>
  <th  style={{textAlign:'center'}}>{donardetails.name}</th>
  <th style={{textAlign:'center'}}>{donardetails.state}</th>
  <th style={{textAlign:'center'}}>{donardetails.city}</th>
  <th style={{textAlign:'center'}}>{donardetails.donations}</th>
  </tr>
  </tbody>
   )}
  </Table>
  </MediaQuery>
  <MediaQuery  maxWidth={1365}  > 
          {/* <div id="PDdiv"> */}
{/* <input type="text" placeholder="Search By Name" name="search" id="searchinput"></input> */}
{/* <div id="mpdiv"> */}
{/* <InputGroup className="mb-3" id="searchinput">
    <FormControl
      aria-label="Default"
      aria-describedby="inputGroup-sizing-default"
      placeholder="Search By Donar Name"
    />
  </InputGroup>
  {/* <button type="submit" id="btn"> Search</button> */}
 
  {/* <Button variant="primary" size="sm" id="btn">
      Search
    </Button>  */}
    
  <Table responsive="lg"  id="table">
  <thead style={{backgroundColor:'#dde5ed'}}>
  <tr id="heading">
  <th style={{textAlign:'center'}}>Photo</th>
  <th  style={{textAlign:'center'}}>Name</th>
  <th style={{textAlign:'center'}}>Donated Books</th>
  </tr>
  </thead>
  {getdonar.map(donardetails=>
    <tbody >
  <tr>
 <th  style={{textAlign:'center'}}><span id="photo"><img src={`https://ik.imagekit.io/kbktyqbj4rrik/uploads/avatar/${donardetails.avatar}`} alt="" style={{borderRadius: '50%',height:'51px',width:'53px'}}></img></span></th>
  <th  style={{textAlign:'center'}}>{donardetails.name}<br/><span id="mspan">{donardetails.state},{donardetails.city}</span></th>
  <th style={{textAlign:'center'}}>{donardetails.donations}</th>
  </tr>
  </tbody>
   )}
  </Table>
 
  {/* </div> */}

          </MediaQuery>
   {/* <JwPagination items={this.state.exampleItems} onChangePage={this.onChangePage} styles={customStyles}/> */}
   {/* --------------------------- till larger mobiles------------------------ */}

   <MediaQuery minWidth={539}>

   <div style={{ textAlign:'center',width:'100%' ,display:'inline'}}>
   <ReactPaginate
          // previousLabel={'previous'}
          // nextLabel={'next'}
          // breakLabel={'...'}
          // breakClassName={'break-me'}
          pageCount={300}
          marginPagesDisplayed={1}
          pageRangeDisplayed={9}
          onPageChange={handlePageClick}
          pageClassName={'paginationLi'}
          previousClassName={'paginationLi'}
          nextClassName={'paginationLi'}
          breakClassName={'paginationLi'}
          // containerClassName={'pagination'}
          // subContainerClassName={'pages pagination'}
          // activeClassName={'active'}
/></div>
</MediaQuery>


<MediaQuery maxWidth={538}>
   <div style={{ textAlign:'center',width:'100%' ,display:'inline'}}>
   <ReactPaginate
          // previousLabel={'previous'}
          // nextLabel={'next'}
          // breakLabel={'...'}
          // breakClassName={'break-me'}
          pageCount={300}
          marginPagesDisplayed={1}
          pageRangeDisplayed={5}
          onPageChange={handlePageClick}
          pageClassName={'paginationLi'}
          previousClassName={'paginationLi'}
          nextClassName={'paginationLi'}
          breakClassName={'paginationLi'}
          // containerClassName={'pagination'}
          // subContainerClassName={'pages pagination'}
          // activeClassName={'active'}
/></div>
</MediaQuery>
  </div>
  {/* <p id="Our-Testimonials">Our Testimonials</p>
  <hr style={{marginLeft:'8.6%',marginRight:'10%',marginTop:'7.4%'}}/>
  <div id="PDdiv1">
  <div id="PDdiv2">
  <label id="label">Thanks a lot for the great job
you are doing. I only wish 
that those books can be used 
by someone in a fruitful way.
All the best MyPustak team. 
You people are really doing a 
great.</label>
 </div>
 <div id="PDdiv3">
  <label id="label">Thanks a lot for the great job
you are doing. I only wish 
that those books can be used 
by someone in a fruitful way.
All the best MyPustak team. 
You people are really doing a 
great.</label>
 </div>
 
 <div id="PDdiv4">
  <label id="label">Thanks a lot for the great job
you are doing. I only wish 
that those books can be used 
by someone in a fruitful way.
All the best MyPustak team. 
You people are really doing a 
great.</label>
 </div>
 <div id="PDdiv5">
  <label id="label">Thanks a lot for the great job
you are doing. I only wish 
that those books can be used 
by someone in a fruitful way.
All the best MyPustak team. 
You people are really doing a 
great.</label>
 </div>
 <span id="photo1">
 </span>
 <label id="label1">Dr V P Singh
Allahabad, U.P.</label>
<span id="photo2">
 </span>
 <label id="label1">Dr V P Singh
Allahabad, U.P.</label>
<span id="photo3">
 </span>
 <label id="label1">Dr V P Singh
Allahabad, U.P.</label>
<span id="photo4">
 </span>
 <label id="label1">Dr V P Singh
Allahabad, U.P.</label>
  </div> */}
  
</div>
  <MainFooter/>
    </div>
    );
  }
}

const mapStateToProps = state => ({
  getdonar:state.ProuddonarR.getdonar,
  noResultFound:state.ProuddonarR.NoDonorFoundS,
})
export default connect(mapStateToProps,{donardetails,searchdonor,setResponseMsg})(ProudDonor);
