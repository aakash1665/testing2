
import React, { Component } from 'react';
import './Static.css'
import { connect } from 'react-redux';
import MainFooter from '../../MainFooter';
import {donardetails} from '../../../actions/prouddonarAction'
import Slider from "react-slick";

class Static extends Component {
  state={
    count:2,
  }
// function Static() {
  componentDidMount() {
    window.scrollTo(0, 0);
    this.props.donardetails(1)
  }
    render() {
      const GetMoreDonor=()=>{
        this.setState({count:this.state.count+1})
        // alert(this.state.count)
      this.props.donardetails(this.state.count)

      }
      const SamplePrevArrow=(props)=> {
        const { className, style, onClick } = props;
        return (
         //   className={className}
          //   // style={{ ...style, display: "block", background: "green" }}
          //   onClick={onClick}
          // ></div>
          <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/SliderLeftArrow.42325be1.png`} onClick={()=>{onClick() ;} }
           className={className}  />
      
        );
      }
      const SampleNextArrow=(props)=> {
       
        const { className, style, onClick } = props;
        return (
          // <div
          //   className={className}
          //   // style={{ ...style, display: "block", background: "red" }}
            
          //   onClick={onClick}
          // ></div>
          
          <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/SliderRightArrow.c683f018.png`} className={className} 
          onClick={()=>{onClick(); GetMoreDonor()}} />
        );
      }

      const {getdonar}=this.props
      const settings = {
        // dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 4,
        slidesToScroll: 4,
        // dots: true,
        // infinite: true,
        // speed: 500,
        // slidesToShow: 5,
        // slidesToScroll: 5,
        nextArrow: <SampleNextArrow />,
        prevArrow: <SamplePrevArrow />,
      };
  return (

<div id="StaticBody">
    <div id="HowWeProcess">
        <div id="S_line"/>
        <div id="S_text">
            How we Process
        </div>
        <div className="circle" id="firstcircle" >
                     <p id="firstcirclep"> &nbsp; &nbsp;Sign Up <br/>
                     &nbsp; &nbsp; &nbsp;& <br/>
                     &nbsp; &nbsp; Fill The Form</p>
                     <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/formfill.09c1c92c.png`}/>

         </div>
         <div className="circle" id="secondcircle">
                   <p id="secondcirclep"> 
                    &nbsp;  Pack Your <br/>
                     &nbsp;  Books</p>
                     <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/pack_book.9d1d64aa.png`}/>
         </div>
         <div className="circle" id="thirdcircle"> 
                     <p id="thirdcirclep"> 
                     &nbsp; &nbsp;Hand Over Books <br/>
                     &nbsp;  To Our Person <br/>
                     &nbsp;  At Your Door Step</p>
                     <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/pack_book.9d1d64aa.png`}/>
         </div>
         <div className="circle" id="forthcircle">
                     <p id="forthcirclep"> &nbsp; We Take All Care to  <br/>
                     &nbsp; Deliver Your Book to <br/>
                     &nbsp; Needy Readers </p>
                     <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/children.7e2107b5.png`}/>
         </div>
    </div>
    <div className="statement">
        <p id="ourMission">
            <span id="quote1up">Our Mission </span><br/>
            <p id="quote1down">READ AND SHARE !</p>
        </p>
        <img id="S_static2" 
                src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/imgchild.8d03e962.jpg`} alt=""/>
        <p id="ourvision">Our Vision</p>
        <p id="ourvisiondata">                                                   
            A world where  books are available to all , without the barriers of location or cost. <br/>
            <span id="ourvisiondata2">A world where to read a book trees do not have to be cut.</span> <br/>
            A world where every printed book has been read by five people before it is recycled.<br/>
        </p>
    </div>
    <div id="S_static3">
                   <div id="S_line3"/>
                   <div id="S_text3">
                           Why Donate? How It Helps?
                 </div>
                 <ul >
                     <li>Feel the joy of giving a book to some one who needs it.</li>
                     <li>Clear the clutter of books at your home.</li>
                     <li>Let someone else also have the pleasure of reading that book.</li>
                     <li>Reusing books helps in reducing carbon footprint.</li>
                     <li id="test">Develop the culture of read and share.</li> 
                 </ul>
      </div>
      <div id="S_static4">
                 <div id="S_line4"/>
                    <p id="S_static4Header">Our Proud Donors</p>
                    {/* <img id="S_static4Img"src={require('./proud_back.jpg')}/> */}
                <div id="ProudDonorSlick">  
                    <Slider {...settings}>
                          {getdonar.map(donordetails=>
                          <div id="SingleProudDonor">
                            {(donordetails.avatar !== null)?
                              <img src={`https://ik.imagekit.io/kbktyqbj4rrik/uploads/avatar/${donordetails.avatar}`}  style={{ width:'8vw',borderRadius:'50%',height:'8vw',marginLeft:'19%'}}></img>
                              :<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/member-attendee-icon.png'alt=""  style={{ width:'8vw',borderRadius:'50%',height:'8vw',marginLeft:'19%'}}></img>}
                              <p id="SingleProudDonorName">{donordetails.name}</p>
                              <p id="SingleProudDonorAddress">{donordetails.city},{donordetails.state}</p>
                              
                          </div>
                          )}
                    </Slider>
                </div>
      </div>
               <div id="S_static5">
           <div id="S_line5"></div>
           <div id="S_text5">
           Some &nbsp; Facts
           </div>
           <p id="donatepara">By donating 30 books you will !</p>
           <ul>
           <li>Reduce approx 85 kg. Carbon footprint.</li>
           <li>Save life of a tree (A ton of recycled paper saves 17 trees).</li>
           <li>Save someone’s approx Rs. 9000 which would otherwise</li>
           <li>have been spent in buying new books.</li>
           </ul>
       </div>
       <div style={{  }}>
         <MainFooter/>
     </div>
</div>
    )

  }  }
const mapStateToProps = state => ({
  getdonar:state.ProuddonarR.getdonar,
})
// export default connect()(Static)
export default connect(mapStateToProps,{donardetails})(Static);