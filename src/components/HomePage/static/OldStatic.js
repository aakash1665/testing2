
import React, { Component } from 'react';
import './Static.css'
import { connect } from 'react-redux';
import MainFooter from '../../MainFooter';
import {donardetails} from '../../../actions/prouddonarAction'
import Slider from "react-slick";
class Static extends Component {
// function Static() {
  componentDidMount() {
    window.scrollTo(0, 0);
    this.props.donardetails(1)}
    render() {
      const {getdonar}=this.props
  return (
<div id="S_static">

    <div id="S_line"></div>
        <div id="S_text">
              How we Process
        </div>
        <div className="circle" id="firstcircle" >
                    <p id="firstcirclep"> &nbsp; &nbsp;Sign Up <br/>
                    &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;& <br/>
                    Fill The Form</p>
                    <img src={require('./formfill.png')}/>

        </div>
        <div className="circle" id="secondcircle">
                  <p id="secondcirclep"> 
                    Pack Your <br/>
                    &nbsp; &nbsp; Books</p>
                    <img src={require('./pack_book.png')}/>
        </div>
        <div className="circle" id="thirdcircle"> 
                    <p id="thirdcirclep"> 
                    &nbsp; &nbsp;Hand Over Books <br/>
                    &nbsp; &nbsp; &nbsp; &nbsp; To Our Person <br/>
                    &nbsp; &nbsp; At Your Door Step</p>
                    <img src={require('./pack_book.png')}/>
        </div>
        <div className="circle" id="forthcircle">
                    <p id="forthcirclep"> &nbsp; We Take All Care to Deliver <br/>
                    &nbsp; Your Book to Needy<br/>
                    &nbsp; Readers & Keep You <br/>
                    &nbsp; Updated For Your Support<br/></p>
                    <img src={require('./children.png')}/>
        </div>

        <div className="statement">
                    <p id="ourMission">
                        <span id="quote1up">Our Mission </span><br/>
                        <p id="quote1down">READ AND SHARE !</p>
                    </p>
                    <img id="S_static2" 
                          src={require('./imgchild.jpg')} alt=""/>
                    <p id="ourvision">Our Vision</p>
                    <p id="ourvisiondata">                                                   
                        A world where  books are available to all , without the barriers of location or cost. <br/>
                        <span id="ourvisiondata2">A world where to read a book trees do not have to be cut.</span> <br/>
                        A world where every printed book has been read by five people before it is recycled.<br/>
                    </p>
        </div>

             <div id="S_static3">
                  <div id="S_line3"/>
                  <div id="S_text3">
                          Why Donate? How It Helps?
                </div>
                <ul >
                    <li>Feel the joy of giving a book to some one who needs it.</li>
                    <li>Clear the clutter of books at your home.</li>
                    <li>Let someone else also have the pleasure of reading that book.</li>
                    <li>Reusing books helps in reducing carbon footprint.</li>
                    <li id="test">Develop the culture of read and share.</li> 
                </ul>
                  {/* </div> */}
              {/* </div> */}
              
              <div id="S_static4">
                <div id="S_line4"></div>
                <p id="S_static4Header">Our Proud Donors</p>
                <img id="S_static4Img"src={require('./proud_back.jpg')}/>

  
          {/* {getdonar.map(donardetails=> */}
            <div>
            <div className="circle2" id="circle2a">
                <p id="circle2pa">Surendra Agarwal<br/> &nbsp;  &nbsp;  &nbsp;  &nbsp; Howrah</p>
            </div>
            <div className="circle2" id="circle2b">
                <p id="circle2pb">&nbsp;  &nbsp; &nbsp;  &nbsp;  Malvika <br/> &nbsp; &nbsp; &nbsp;  &nbsp;  &nbsp; Delhi</p>
            </div>
            <div className="circle2" id="circle2c">
                <p id="circle2pc">Dipinn Verma<br/> &nbsp;  &nbsp; &nbsp;  &nbsp;  &nbsp; Delhi</p>
            </div>
            <div className="circle2" id="circle2d">
                <p id="circle2pd">Mahendra Jain<br/> &nbsp;  &nbsp;  &nbsp;  &nbsp; Kolkata</p>
            </div>
        </div>
        </div>
       
          {/* )} */}

        <div id="S_static5">
          <div id="S_line5"></div>
          <div id="S_text5">
          Some &nbsp; Facts
          </div>
          <ul>
          <li>By donating 30 books you will.</li>
          <li>Reduce approx 85 kg. Carbon footprint.</li>
          <li>Save life of a tree (A ton of recycled paper saves 17 trees).</li>
          <li>Save someone’s approx Rs. 9000 which would otherwise</li>
          <li>have been spent in buying new books.</li>
          </ul>
      </div>




      </div>
      
    <div style={{ marginTop:'30%' }}>
        <MainFooter/>
    </div>
</div>
    )

  }  }
const mapStateToProps = state => ({
  getdonar:state.ProuddonarR.getdonar,
})
// export default connect()(Static)
export default connect(mapStateToProps,{donardetails})(Static);