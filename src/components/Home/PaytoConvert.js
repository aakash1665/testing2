import React, { Component } from 'react'
import { Link,Redirect} from 'react-router-dom'
// import './Proceedpay.css'
// import {OrderDetails} from '../actions/cartAction';
// import {Get_Rp_Id} from '../actions/donationActions'
import { connect } from 'react-redux';
import axios from 'axios'
import config from 'react-global-configuration'
class PaytoConvert extends Component {

state={
   count:1,
   GotOThankYou:false,
}
	// { "data" :
	// 	{
	// 	"amount"  : 100,
	// 	"no_of_book" : 2,
	// 	"payusing" : "test_2502",
	// 	"billing_add_id" : 15753,
	// 	"shipping_add_id" : 15753
			
	// 	}
	// } 
  closePopup=()=>{
    this.props.closePopup()
  }

// ***********************RAZORPAY PART********************************
options = {
  "key": "rzp_live_pvrJGzjDkVei3G", //Paste your API key here before clicking on the Pay Button. 
  // "key": "rzp_test_jxY6Dww4U2KiSA",
  "amount" : `${Number(this.props.ConvertAmt)}00`,
  "name": `Mypustak.com`,
  "description": `Total No of Books ${this.props.CartLength}`,
  "Order Id": `${this.props.RAZORPAY}`, //Razorpay Order id
  "currency" : "INR",


  "handler":  (response)=>{ 
      // console.log("in Handler Func")   
      const razorpay_payment_id=response.razorpay_payment_id;
      // const razorpay_order_id=response.razorpay_order_id;
      // const razorpay_signature=response.razorpay_signature;
      var AllbookId=[]
      var AllbookInvId=[]
      var AllrackNo=[]
      var AllQty=[]
     
       this.props.cartDetails.map((book,index)=>{
         
         AllbookId.push(`${book.bookId}`);
         AllbookInvId.push(book.bookInvId[0]);
         AllrackNo.push(book.bookRackNo[0]);
         AllQty.push(book.bookQty[0]);
        //  index;
       })
      console.log(AllbookId);
      
      const SendData={
        // {"payment_id":"12","payment_url":"www.test.com"}
        "payment_id":razorpay_payment_id,
        "payment_url":'https://razorpay.com/',
        "book_id":AllbookId,
        "book_inv_id":AllbookInvId,
        "rack_no":AllrackNo,
        "qty":AllQty,
      }
      // console.log(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`);
      
      // axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`  
      // axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
      axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
          // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
          'Authorization': `Token ${this.props.userToken}`,
          // ''
        }})
        .then(res=>console.log('Getting Response '))
        .catch(err=>console.log(err) 
        )
      // }
      // )
        this.setState({GotOThankYou:true})
        // --------------------------Add Wallet 
        if(this.props.walletRazorpay.length !== 0){
        var today = new Date();
      var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var dateTime = date+' '+time;
      //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
      const SendDataWallet={
        "user_id":`${this.props.UserId}`,
        "user_email":`${this.props.UserEmail}`,
        "transaction_id":`PaidFromWallet${Math.round(this.props.walletRazorpay.walletbalance)}`,
        "deposit": 0,
        "withdrawl":Math.round(this.props.walletRazorpay.walletbalance),
        "payvia":"wallet",
        "time": `${dateTime}`,
        "comment":"Paid From Wallet",
        "added_by":`${this.props.UserEmail}`
      }
      axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendDataWallet,{
        headers:{'Authorization':`Token ${this.props.userToken}`,
                  'Content-Type':'application/json'}
      })
      .then(res=>{
        console.log("Wallet Transaction Done ");  
        this.setState({SuccessWalletAdded:true})

      })
        .catch(err=>console.log(err,SendData))
    }
      // }
      // console.log(response);   
      // console.log(razorpay_payment_id)
      // console.log(razorpay_order_id)
      // console.log(razorpay_signature)
      // console.log(`${this.props.Selectedphone_no} , ${this.props.UserEmail}`);
      
      // alert(response.razorpay_payment_id);
      // alert(response.razorpay_order_id);
      // alert(response.razorpay_signature);
      
      

        // alert(response.razorpay_order_id)
        // alert(response.razorpay_signature)
                                },

  "prefill": {
      // "method":"card,netbanking,wallet,emi,upi",
      "contact" : `${this.props.Selectedphone_no}`,
      "email" : `${this.props.UserEmail}`
      // "email":"ph15041996@gmail.com"
  },
         //             "notes": {
    //                     "order_id": "your_order_id",
    //                                 "transaction_id": "your transaction_id",
    //                                 "Receipt": “your_receipt_id"
    //             },
  "notes": {
      "Order Id": this.props.OrderId, //"order_id": "your_order_id", // Our Order id
      "address" : "customer address to be entered here"
  },
  "theme": {
      "color": "#1c92d2",
       "emi_mode" : true
              
  },
  
//   external: {
// //  wallets: ['mobikwik' , 'paytm' , 'jiomoney' , 'payumoney'],
// wallets: ['paytm'],

//   handler: function(data) {
//   console.log(this, data)
//   }
// }
};

componentDidMount(){
  this.setState({GotOThankYou:false})
  // this.props.GetOrderId(data)
  this.rzp1 = new window.Razorpay(this.options);
  }
  closeProcedToPay(){
  this.props.closeProcedToPay()
}
  render() {
      console.log(this.props.ConvertAmt);
      
    if(this.state.GotOThankYou){
      return <Redirect to="view-cart/thank-you"/>
    }
    // alert(this.props.Selectedphone_no)
    // console.log(this.props.TotalPrice)
  //   const OrderData={
  //     data :{
  //    amount  : this.props.CartPrice.TotalPayment,
  //    no_of_book : 2,
  //    payusing : "razorpay",
  //    billing_add_id : 15753,
  //    shipping_add_id : 15753,
  //    }
     
  //  }



//    { "data" :
//    {
//    "amount"  : 100,
//    "no_of_book" : 2,
//    "payusing" : "test_2502",
//    "billing_add_id" : 15753,
//    "shipping_add_id" : 15753
     
//    }
// //  }
// const token= `Token ${this.props.userToken}`

// if(this.props.OrderId !== 0 && this.props.RAZORPAY === 0){
//   let  Rpdata={
//     data :
//     {
//         ref_id : `${this.props.OrderId}`,
//         amount : `${this.props.CartPrice.TotalPayment}`,
//     }
// }
// // console.log(data);

//     this.props.Get_Rp_Id(Rpdata)
//     alert("l")
// }
// let count =1
// if(this.props.RAZORPAY !== 0 && this.state.count === 1){
//   alert("o")
//   this.rzp1.open();
//   // count+=2 
//   this.setState({count:2})
// }
const {SelectedAddress} =this.props
    return (
      
        <div style={{ marginLeft:"10%",marginTop:'10%' }}>
        {/* <a style={{ color:'rgb(0,186,242)',float:'right',position:'absolute' }}> &times;</a> */}
          <p style={{ padding:'1%' }}></p>
          <button id="PopupProceedToPayBtn" onClick={()=>this.rzp1.open()} >Confirm And Pay Rs. {Number(this.props.ConvertAmt)}</button>
        </div>
    
    )
  }
}

const mapStateToProps = state => ({
  // getuserdetails:state.userdetailsR.getuserdetails,
  // getadd:state.accountR.getadd,
  userToken:state.accountR.token,
  // CartPrice:state.cartReduc.CartPrice,
  CartLength:state.cartReduc.cartLength,
  cartDetails:state.cartReduc.MyCart,
  walletRazorpay:state.cartReduc.walletRazorpayState,
  // AddresId:state.cartReduc.AddresId,
  OrderId:state.cartReduc.OrderId,
  RAZORPAY:state.donationR.rp_id,
  TotalPrice:state.cartReduc.TotalPrice,
  UserEmail:state.userdetailsR.getuserdetails.email,
  // Selectedphone_no:state.accountR.selectedAddress.phone_no,
  Selectedphone_no:state.accountR.getuserdetails.phone,
})

export default connect(mapStateToProps,null)(PaytoConvert)