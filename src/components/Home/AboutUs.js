import React, { Component } from 'react';
import './AboutUs.css';
import {Helmet} from 'react-helmet';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import {BreadcrumbsItem} from 'react-breadcrumbs-dynamic'
import { getSeoData } from "../../actions/seodataAction";
import { connect } from "react-redux";  


class AboutUs extends Component {
componentDidMount(){
  window.scrollTo(0, 0);
  this.props.getSeoData(
    "https://www.mypustak.com" + window.location.pathname
  );
  
}
// constructor(props) {
//   super(props);
//   this.props.getSeoData(
//     "https://www.mypustak.com" + window.location.pathname
//   );
// }
render(){
  const seoData = this.props.SeoData;

return (
<div>
<BreadcrumbsItem to='/pages/about-us'>About Us</BreadcrumbsItem>

  <MainHeader/>
  <Helmet>
    <title>{seoData.title_tag}</title>
    <meta name="description" content={seoData.meta_desc} />
    <style>{'body{background-color:#F8F8FF;}'}</style>
  </Helmet>
<div id="AUdiv3">
  <p id="About-Us">About Us</p>
  <div id="AUdiv2">
  <label style={{color:'red',fontWeight:'bold'}}> Brief Introduction</label>
  <hr style={{ size:'5px'}}/>
<label style={{textAlign:'justify'}}>
MyPustak.com is India’s first online platform which works towards making education available to all, across the geographical and social boundaries. We make this possible by providing free books to 
those who have the desire to read but lack of resources to buy.
With the help of our team of highly energetic volunteers and logistic partners, today MyPustak has its reach in more than five thousand pincode across the country. We reach out for collecting books from 
donors and  also for delivering them at reader’s doorstep. As we are a philanthropic organization  we don’t charge anything neither from the donors  nor the receivers,except a very nominal shipping & 
handling charge is to be borne by the one who wants the book.</label>
<br/>
<label style={{color:'red',fontWeight:'bold'}}>
Our Impact on Society</label>
<hr/>
<label style={{textAlign:'justify'}}>
We are committed to  provide free books to the needy , but we do this on a priority to those who live in countryside and mostly in such places where quality books are unavailable or/and expensive. 
Thanks to the internet and its connectivity, anyone can go to our site and place a request for any book that is displayed there.
Due to constraints some people are not able to access the books they desire to read.  We @ MyPustak wish to fill this gap. We are reaching out to those who have books(donors) and meeting the 
demands of those who don’t have them(needy). Thus we are bridging the gap of resources.</label>
<br/><br/>
<label style={{color:'red',fontWeight:'bold'}}>
Our Impact on Environment</label>
<hr/>
<label style={{textAlign:'justify'}}>
We  also support  sustainability in a novel way.  We believe that every book has minimum life span of three years and work on a philosophy that “your book will live on”.
Lets understand this, each book when published and printed releases ~2.71 kg CO2 equivalent . If this book goes from one hand to another five times, it saves nearly 14 kg of CO2 equivalent(which 
would otherwise be released while production of new copies of the book), thus reducing the carbon footprint manifolds. Additionally, a book when redistributed, saves lot of trees as well that would be cut 
to make paper, thus it is supporting a sustainable environment as well.</label>
<br/><br/>
<label style={{color:'red',fontWeight:'bold'}}>
How do we identify and help “Needy” readers</label>
<hr/>
<label style={{textAlign:'justify'}}>
The books we get in donations may help someone who wants it but can’t buy because of several constraints such as money, geographic location etc. There may be an argument that how do people, 
whom we intend to target, in distant rural places can avail the benefits of MyPustak.com since it’s an online platform and services can be availed only when you have a decent internet connection and 
internet banking facility/ATM cards. This too can be done with the a little help of people who wish to join a noble cause. One can order on behalf of someone who lacks the knowledge of e-commerce. 
With a nominal shipping charge we have striven to reduce the constraint of money and with our huge logistics network, geography too is not a challenge anymore. Moreover, with government’s push for 
digital India and financial inclusion, we expect more people to join us .</label>
  </div>
  </div>
  <MainFooter/>
</div>

);
}
}
const mapStateToProps = state => ({
  SeoData: state.seodata.seodata
});
export default connect(
  mapStateToProps,
  { getSeoData }
)(AboutUs);
