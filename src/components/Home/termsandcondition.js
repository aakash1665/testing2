import React, { Component } from 'react';
import './termsandcondition.css';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
// import React from 'react';
import {Helmet} from 'react-helmet';


class TermsCondtion extends Component {
  componentDidMount(){
    // console.log(getfaq())
    window.scrollTo(0, 0);
  }  ;
  render() {
    return (
      <div>
    <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style>
</Helmet>
      <MainHeader/>
      <div id="termsandconditionsouterdiv">
      <p id="terms-condition">Terms & Conditions</p>
      <hr id="termshr"/>
      <div id="termsdiv">
     <p> These Terms of Use govern your access to and use of the Site. Your access to and use of the Site are expressly conditioned on your compliance with these Terms of Use. 
By accessing or using the Site, you agree to be bound by these Terms of Use, our Privacy Policy, and any other terms, guidelines or rules that are applicable to any portion of the Site that 
you may access, without limitation or qualification. If you do not agree to these Terms of Use, you must exit the Site immediately and discontinue any use of information, products or services 
from the Site.
</p>
<p id="termspara">Use and Sharing of Information</p>
<p>
At no time will we sell your personally-identifiable data without your permission unless set forth in this Privacy Policy. The information we receive about you or from you may be used by us or 
shared by us with our corporate affiliates, dealers, agents, vendors and other third parties to help process your request, to comply with any law, regulation, audit or court order, to help improve
 our website or the products or services we offer, for research, to better understand our customer’s needs, to develop new offerings, and to alert you to new products and services (of us or our
 business associates) in which you may be interested. We may also combine information you provide us with information about you that is available to us internally or from other sources in 
order to better serve you.
We do not share, sell, trade or rent your personal information to third parties for unknown reasons.
</p>
<p id="termspara">
Cookies</p>
<p>From time to time, we may place “cookies” on your personal computer. “Cookies” are small identifiers sent from a web server and stored on your computer’s hard drive, that help us to 
recognize you if you visit our website again. Also, our site uses cookies to track how you found our site. To protect your privacy we do not use cookies to store or transmit any personal 
information about you on the Internet. You have the ability to accept or decline cookies. Most web browsers
automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. If you choose to decline cookies certain features of the site may not function 
properly or at all as a result.
</p>
<p id="termspara">Links</p>
<p>
Our website may contains links to other sites. Such other sites may use information about your visit to this website. Our Privacy Policy does not apply to practices of such sites that we do not 
own or control or to people we do not employ. Therefore, we are not responsible for the privacy practices or the accuracy or the integrity of the content included on such other sites. We 
encourage you to read the individual privacy statements of such websites.
</p>
<p id="termspara">
Security</p>
<p>
We safeguard your privacy using known security standards and procedures and comply with applicable privacy laws. Our websites combine industry-approved physical, electronic and 
procedural safeguards to ensure that your information is well protected though it’s life cycle in our infrastructure. Sensitive data is hashed or encrypted when it is stored in our infrastructure. 
Sensitive data is decrypted, processed and immediately re-encrypted or discarded when no longer necessary. We host web services in audited data centers, with restricted access to the data 
processing servers. Controlled access, recorded and live-monitored video feeds, 24/7 staffed security and biometrics provided in such data centers ensure that we provide secure hosting.
</p>
<p id="termspara">Opt-Out Policy</p>
<p>
Please write to us at info@mypustak.com if you no longer wish to receive any information from us regarding changes to this Privacy Policy
Our privacy policy was last updated on December 20, 2014. We may change our Privacy Policy from time to time. If we do, we will update this Privacy Policy on our website.
If you have any questions about our Privacy Policy, please write to us info@mypustak.com. We will more than happy to answer your queries
</p>
<p id="termspara">
Ownership</p>
<p>
All of the content on the Site (which includes, without limitation, all graphics, text, images, photographs, illustrations, and the design, selection and arrangement thereof) and/or other 
intellectual property or proprietary rights owned by, or licensed to,Mypustak.
</p>
<p id="termspara">
User Submissions</p>
<p>
All communications, comments, feedback, bug reports, suggestions, ideas, content, and other submissions submitted to Mypustak through the Site (collectively, “Submissions”) shall be and 
remain Mypustak’ property with all worldwide rights, titles and interests in all copyrights and other intellectual property in such Submissions hereby being assigned to Mypustak by you. All 
Submissions, as well as all communications made by you through the Site, including, without limitation, blog and chat messages shall not (i) be illegal, defamatory, trade libelous, threatening 
invasive of privacy or harassing; (ii) be obscene or contain any pornography; (iii) contain any software viruses, Trojan horses, worms, time bombs, cancelbots or other computer programming 
routines that are intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information; (iv) infringe upon any third party’s copyright,
 patent, trademark, trade secret or other intellectual property or proprietary rights or rights of publicity or privacy; (v) consist of or contain political campaigning, commercial solicitation, chain 
letters, mass mailings or any form of “spam”; or (vi) otherwise create or result in any liability for Mypustak. You may not use a false email address, impersonate any person or entity, or 
otherwise mislead as to the origin of any Submissions. Mypustak reserves the right (but has no obligation) to remove or edit any such Submissions. Mypustak has no responsibility and 
assumes no liability for any Submissions posted on the Site by you or any third party.
</p>
<p id="termspara">
Electronic Communications</p>
<p>
When you visit the Site or send e-mails to us, you are communicating with us electronically. By doing so, you thereby consent to receive communications from us electronically. We may 
communicate with you by e-mail or by posting notices on this Site. You agree that all agreements, notices, disclosures and other communications that we provide to you electronically satisfy 
any legal requirement that such communications be in writing.
</p>
<p id="termspara">
Registration</p>
<p>
If you open an Account or register to use the Site, you are required to select a password and user name, which shall consist of an email address that you provide. If you register, you agree to 
provide us with accurate and complete registration information, and to inform us immediately of any updates or other changes to such information. You are responsible for safeguarding any 
password that you use to access any areas of the Site. You agree to take sole responsibility for any activities or actions under your password, whether or not you have authorized such 
activities or actions. You shall be responsible for any and all fees and charges incurred under your Account or using your password, whether or not incurred by you or a third party, with or 
without your authorization, provided that the foregoing shall be subject to any remedies or rights you may have under applicable law through your bank or other financial institution. You agree 
to reimburse us for all costs and expenses, including, without limitation, attorneys, fees and collection fees, incurred by us in our efforts to collect any outstanding balance from you. You agree 
to immediately notify Mypustak of any unauthorized use of your password. We reserve the right to refuse registration of, or cancel your Account at our discretion.
</p>
<p>
Order Information</p>
<p>
By making a purchase from the Site, you understand and agree that the Site may share information about you and your transaction with other companies for the purpose of processing your 
transaction, including, without limitation, for purposes of fraud prevention, vendor direct shipping and credit card authorization.
</p>
<p id="termspara">
Trademarks</p>
<p>
The trademarks, names, logos and service marks (collectively “trademarks”) displayed on this Site are registered and unregistered trademarks of Mypustak and/or its third-party licensors or 
partners. Nothing contained on this Site should be construed as granting you any license or right to use any trademark without the prior written permission of Mypustak.
</p>
<p>
External links</p>
<p>
The Site contains links to other web sites on the Internet. We provide these links for your convenience only, and we explicitly disclaim any responsibility for the accuracy, content, or availability
 of information found on websites that link to or from the Site. We cannot ensure that you will be satisfied with, and we hereby disclaim any and all liability and responsibility with regard to, any
 products or services that you purchase from a third-party web site that links to or from the Site or third-party content on our Site. We do not endorse any of the such products or services, nor 
have we taken any steps to confirm the accuracy or reliability of, any of the information contained in such third-party websites or content. We do not make any representations or warranties 
as to the security of any information (including, without limitation, credit card and other personal information) you might be requested to give any third-party, and you hereby irrevocably waive 
any claim against us with respect to such websites and third-party content. We strongly encourage you to make whatever investigation you feel necessary or appropriate before proceeding 
with any online or offline transaction with any of these third parties. In addition, we disclaim all warranties express or implied, as to the accuracy, legality, reliability or validity of any content on 
any such web site, or that such web site will be free from viruses or other harmful elements.
</p>
<p id="termspara">
Notice and Procedure for Making Claims of Copyright Infringement</p>
<p>
If you believe that your work has been copied in a way that constitutes copyright infringement, please provide Mypustak’ copyright agent the written information specified below. Please note 
that this procedure is exclusively for notifying Mypustak that your copyrighted material has been infringed.

An electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest;

A description of the copyrighted work that you claim has been infringed upon;

A description of where the material that you claim is infringing is located on the Site, including the ISBN if applicable;

Your address, telephone number, and e-mail address;

A statement by you that you have a good-faith belief that the disputed use is not authorized by the copyright owner, its agent, or the law;

A statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or authorized to act on the copyright owner’s 
behalf.
</p>
<p id="termspara">
Product Descriptions</p>
<p>
Mypustak attempts to be as accurate as possible with regard to the descriptions of books and other products that it sells on the Site. However, Mypustak does not warrant that such 
descriptions or other content of the Site is accurate, complete, reliable, current, or error-free. If a product offered by Mypustak is not as described on the Site, your sole remedy is to return it, 
in the same condition as received, either for replacement with the same or a comparable product or for a refund within 30 days after receipt.
</p>
<p id="termspara">
Disclaimer</p>
<p>
This site and its contents are provided without any representations or warranties of any kind, either express or implied. Mypustak disclaims all representations and warranties to the fullest 
extent permitted by applicable laws. In addition, mypustak does not represent or warrant that the information, products and/or services on or accessible through the use of the site are accurate
, complete or current, or that this site will be free of defects, including, without limitation, viruses or other harmful elements. The user of this site assumes all risk of losses or damages arising 
out of the use of the site.
</p>
<p id="termspara">
Limitation of Imitation</p>
<p>
Mypustak shall not be responsible for and disclaims all liability for any loss, liability, damage (whether direct, indirect, special or consequential), personal injury or expense of any nature 
whatsoever which may be suffered by you or any third party , as a result of, or which may be attributable directly or indirectly to, your access to and use of the site, any information contained 
on the site, or your personal information or material and information transmitted over our system. In particular, neither mypustak nor any third party or data-or content-provider shall be liable 
in any way to you or to any third party for any loss, liability, damage (whether direct, indirect or consequential), personal injury or expense of any nature whatsoever arising from any delays, 
inaccuracies, errors in, or omissions of, any information or data or the transmission thereof, or for any actions taken in reliance thereon or occasioned thereby, or by reason of non-
performance or interruption of performance.Our aggregate liability and the aggregate liability of any of our suppliers, service providers, or third-party affiliates, arising from or relating to these 
terms of use and/or your use of the site (regardless of the form of action or claim, whether in contract, warranty, tort, strict liability, negligence, fraud or any other legal theory) is limited to one-
thousand rupee (inr1000).
</p>
<p id="termspara">
Indemnification</p>
<p>
You agree to indemnify and hold harmless Mypustak from and against all liabilities, claims, damages, costs and expenses, including, without limitation, attorneys’ fees arising out of your use 
of the Site; any information or other materials you post, upload, e-mail or otherwise transmit using the Site (including, without limitation, credit card information); or your violation, breach, or 
alleged violation or breach, of these Terms of Use.
</p>
<p id="termspara">
Conflict of terms</p>
<p>
If there is a direct conflict or contradiction between the provisions of these Terms of use and any other relevant terms and conditions, policies or notices that apply to a particular section or 
module of the Site, the other relevant terms and conditions, policies or notices which relate specifically to a particular section or module of the Site shall prevail in respect of your use of that 
relevant section or module of the Site.
</p>
<p>
Severability</p>
<p>
Any provision of these Terms of Use which is or becomes unenforceable in any jurisdiction, whether due to being void, invalidity, illegality, unlawfulness or for any reason whatsoever, shall, in 
such jurisdiction only and only to the extent that it is so unenforceable, be treated as void, and the remaining provisions of any relevant terms and conditions, policies and notices shall remain 
in full force and effect.
</p>
<p id="termspara">
Modification of the Site or Terms of Use</p>
<p>
You acknowledge and agree that Mypustak reserves the right to change, modify, alter, update or discontinue the terms, conditions and notices under which the Site is offered and/or the 
products, services, features, links, content, information and any other materials are offered via the Site, at any time and from time to time, without notice or further obligation to you. By 
continuing to access or use the Site after Mypustak makes any such modifications, alterations or updates, you agree to be bound by such modifications, alterations or updates (as applicable).
</p>
<p id="termspara">
Waiver; Entire Agreement</p>
<p>
Any waiver of any provision of these Terms of Use must be in writing and signed by Mypustak to be valid. A waiver of any provision hereunder shall not operate as a waiver of any other 
provision, or a continuing waiver of the same provision in the future. These Terms of Use and our Privacy Policy constitute the entire understanding and agreement between you and us 
relating to the subject matter hereof and supersede any and all prior statements, understandings or agreements, whether oral or written regarding such subject matter, and shall not be 
modified except in writing, signed by you and Mypustak.
</p>
<p id="termspara">
Termination</p>
<p>
Your access to the Site and your Account may be terminated immediately without notice from us, if, in our sole discretion, you fail to comply with any term or provision of these Terms of Use. 
Upon termination, you must cease use of the Site and your Account, and neither you, nor any third party on your behalf, shall access or attempt to access the Site for any purpose whatsoever.
</p>    
</div>
</div>
          <MainFooter/>
                    </div>
 );
  }
}

export default TermsCondtion;