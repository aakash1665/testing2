import React, { Component } from 'react'
import './mobilecategory.css'
import {navurl} from '../../actions/accountAction'
import { connect } from 'react-redux';
// import Loader from 'react-loader-spinner';
// import Popup from 'reactjs-popup';
// import MainHeader from '../MainHeader';
// import MainFooter from '../MainFooter';
import {Helmet} from 'react-helmet';
// import school from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/school.png'
// import university from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/university.png'
// import competitive from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/competitive.png'
// import fictionicon from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fictionicon.png'
// import cat from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/cat.png'
// import engmed from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/engmed.png'
// import GK from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/GK.png'
// import govt from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/govt.png'
// import international from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/international.png'
// import children from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/children.png'
// import classno from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/classno.png'
// import dic from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/dic.png'
// import NCERT from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/NCERT.png'
// import engg from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/engg.png'
// import fiction from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fiction.png'
// import finance from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/finance.png'
// import medical from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/medical.png'
// import nonfic from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/nonfic.png'
// import others from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png'
// import religion from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/religion.png'
// import sci from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sci.png'
// import selfhelp from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/selfhelp.png'
// import schoollevel from 'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/schoollevel.png'
import MediaQuery from 'react-responsive'
import {setCategory} from '../../actions/homeAction'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { Link,Redirect,browserHistory } from 'react-router-dom'
class Mobilecat extends Component {
    state = {
        url:["http://127.0.0.1:8000/category/competitive-exams", 
              "http://127.0.0.1:8000/category/competitive-exams/cat",
              "http://127.0.0.1:8000/category/competitive-exams/general-knowledge-learni",
              "http://127.0.0.1:8000/category/competitive-exams/engineering",
              "http://127.0.0.1:8000/category/competitive-exams/government-jobs",
              "http://127.0.0.1:8000/category/competitive-exams/international-exams"
            ], 
           count:1,  
           Click:'', 
           CompViewAll:'/category/competitive-exams/',
           FictViewAll:'/category/fiction-non-fiction/',
           SCBViewAll:'/category/school-children-books/',
           UBViewAll:'/category/university-books/',
           ShowClick:'',
           SClick:'',
           display:0,
           menuS:'block',
        }
        
        componentDidMount() {
          window.addEventListener('onClick', this.handleScroll);
         
        }
        componentWillUnmount() {
          window.removeEventListener('onClick', this.handleScroll);
      }
      handleScroll=()=>{
        const scroll = window.scrollY
        this.setState({display:scroll});
        console.log(this.state.display);
        if(this.state.display < 170){
          this.setState({menuS:'block'});
        }else{
          this.setState({menuS:'none'});
        }
      }

  render() {
    const urlto= window.location.pathname;
    // console.log(window.location.pathname);
    const SetCategory=(category)=>{
      // alert(category);
      this.props.setCategory(category);
      // this.props.CloseCatPopup()
    }
    var settings = {
      arrows:false,
      dots: true,
      infinite: false,
      speed: 500,
      // slidesToShow: 4,
      // slidesToScroll: 4,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 6,
            slidesToScroll: 6,
            // initialSlide: 2
          }
        },
        // {
        //   breakpoint: 480,
        //   settings: {
        //     slidesToShow: 1,
        //     slidesToScroll: 1
        //   }
        // }
      ]
    };
    var settings1 = {
      arrows:false,
      dots: true,
      infinite: false,
      speed: 500,
      // slidesToShow: 4,
      // slidesToScroll: 4,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 5,
            slidesToScroll: 5,
            // initialSlide: 2
          }
        },
        // {
        //   breakpoint: 480,
        //   settings: {
        //     slidesToShow: 1,
        //     slidesToScroll: 1
        //   }
        // }
      ]
    };
    // console.log(urlto);
    if(urlto ==="/category/school-children-books/"){
      // this.setState({Click:''})
      // this.props.navurl();
    }
       return (
             <div>
    <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style>
</Helmet>
    {/* <MainHeader/> */}
      <div id="mcategorydiv">
      <div style={{display:"flex"}}>
               <div style={{flex:"1"}}>
      <Link to="/category/school-children-books/" style={{textDecoration:'none'}}><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sschool.34c48952.png`} id="layer1img"
       onClick={()=>{SetCategory('/category/school-children-books/')
       this.setState({Click:'S&CB',SCBViewAll:'/category/school-children-books/',ShowClick:'School Books'})}} 
       className={(this.state.Click ==='S&CB')?"MobileShowclicked":""}></img>
              <p id={(urlto==="/category/school-children-books/")?"layer1colorbooks":"layer1books"}>School Books</p>
              
      </Link>
      </div>
      <div style={{flex:"1"}}>
      <Link to="/category/university-books/" style={{textDecoration:'none'}}>
      <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/university.png`} id="layer1img"
      onClick={()=>{ SetCategory('/category/university-books/')
      this.setState({Click:'UB',UBViewAll:'/category/university-books/',ShowClick:'University Books'})}}
      className={(this.state.Click ==='UB')?"MobileShowclicked":""}></img>
      <p id={(urlto==="/category/university-books/")?"layer1colorbooks":"layer1books"}>University Books</p>
      </Link>
      </div>
      <div style={{flex:"1"}}>
      <Link to="/category/competitive-exams/" style={{textDecoration:'none'}}>
      <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/competitive.png`} id="layer1img"
      onClick={()=>{SetCategory('/category/competitive-exams/')
      this.setState({Click:'compExm',CompViewAll:'/category/competitive-exams/',ShowClick:'Competitive Books'})}}
      className={(this.state.Click ==='compExm')?"MobileShowclicked":""}></img>
      <p id={(urlto==="/category/competitive-exams/")?"layer1colorbooks":"layer1books"}>Competitive Exams</p>
      </Link>
      </div>
      <div style={{flex:"1"}}>
      <Link to="/category/fiction-non-fiction/" style={{textDecoration:'none'}}>
      <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fictionicon.png`} id="layer1img"
      onClick={()=>{SetCategory('/category/fiction-non-fiction/')
      this.setState({Click:'f&nf',FictViewAll:'/category/fiction-non-fiction/',ShowClick:'Fiction Books'})}}
      className={(this.state.Click ==='f&nf')?"MobileShowclicked":""}></img>
      <p id={(urlto==="/category/fiction-non-fiction/")?"layer1colorbooks":"layer1books"}>Fiction Books</p>
     </Link> 
     </div>
     
   
    {/* <span style={{ color:'black' }} ><Link to={this.state.CompViewAll} style={{ textDecoration:'none',color:'black',marginLeft: '4%'}}>View All <span style={{borderBottom:'1px solid'}}>{this.state.ShowClick}</span></Link></span> */}
    </div> <hr/>
                 {/* ------------------2nd layer------------------------------ */}
         <div>
              {(this.state.Click==='compExm')? (
                <div style={{display:"flex"}}>
                 <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none',width:'18%' }}to="/category/competitive-exams/cat/"
               onClick={()=>{SetCategory('/category/competitive-exams/cat/')
               this.setState({ShowClick:'CAT',CompViewAll:'/category/competitive-exams/cat/'})}} 
               className={(this.state.ShowClick ==='CAT')?"MobileShowclicked":""}>
               <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/cat.png`} id="layer2img"></img>
               <p id="layer2title">CAT</p>
               </Link>
               </div>
               <div style={{flex:"1"}}>
               <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/engineering/">
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/engmed.png`} id="layer2img" 
              onClick={()=>{SetCategory('/category/competitive-exams/engineering/')
              this.setState({ShowClick:'EAM',CompViewAll:'/category/competitive-exams/engineering/'})}} 
              className={(this.state.ShowClick ==='EAM')?"MobileShowclicked":""}></img>
               <p id="layer2title">Engineering <br/>& Medical</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/general-knowledge-learni/"
               onClick={()=>{SetCategory('/category/competitive-exams/general-knowledge-learni/')
               this.setState({ShowClick:'GK',CompViewAll:'/category/school-children-books/'})}} 
               className={(this.state.ShowClick ==='GK')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/GK.png`} id="layer2img"></img>
              <p id="layer2title">Knowledge <br/>& Learning</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/"
               onClick={()=>{SetCategory('/category/competitive-exams/government-jobs/')
               this.setState({ShowClick:'Govt',CompViewAll:'/category/school-children-books/'})}} 
               className={(this.state.ShowClick ==='Govt')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/govt.png`} id="layer2img"></img>
              <p id="layer2title">Government Jobs</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/international-exams/" 
              onClick={()=>{SetCategory('/category/competitive-exams/international-exams/')
              this.setState({ShowClick:'IJ',CompViewAll:'/category/school-children-books/'})}} 
              className={(this.state.ShowClick ==='IJ')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/international.png`} id="layer2img"></img>
              <p id="layer2title">International Exams</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/school-level/"
               onClick={()=>{SetCategory('/category/competitive-exams/school-level/')
               this.setState({ShowClick:'SL',CompViewAll:'/category/school-children-books/'})}} 
               className={(this.state.ShowClick ==='SL')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/schoollevel.png`} id="layer2img"></img>
              <p id="layer2title">School Level</p>
               </Link>
               </div> 
               </div>   
              ):""}

{(this.state.Click==='S&CB')? (
                <div style={{display:"flex"}}>
                 <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/"
              onClick={()=>{SetCategory('/category/school-children-books/children-books/')
              this.setState({ShowClick:'CB',CompViewAll:'/category/school-children-books/'})}} 
              className={(this.state.ShowClick ==='CB')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/children.png`} id="layer2Simg"></img>
              <p id="layer2Stitle">Children Books</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/"
              onClick={()=>{SetCategory('/category/school-children-books/classes/')
              this.setState({ShowClick:'Class',CompViewAll:'/category/school-children-books/'})}} 
              className={(this.state.ShowClick ==='Class')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/classno.png`} id="layer2Simg"></img>
              <p id="layer2Stitle">Classes</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/dictionary-level-/"
              onClick={()=>{SetCategory('/category/school-children-books/dictionary-level-/')
              this.setState({ShowClick:'DL',CompViewAll:'/category/school-children-books/'})}} 
              className={(this.state.ShowClick ==='DL')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/dic.png`} id="layer2Simg"></img>
              <p id="layer2Stitle">Dictonary Level</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/"
              onClick={()=>{SetCategory('/category/school-children-books/ncert/')
              this.setState({ShowClick:'Ncert',CompViewAll:'/category/school-children-books/'})}} 
              className={(this.state.ShowClick ==='Ncert')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/NCERT.png`} id="layer2Simg"></img>
              <p id="layer2Stitle">NCERT </p>
              </Link>
             </div>
              </div>
              ):""}

{(this.state.Click==='UB')? (
               <div style={{display:"flex"}}>
               <div style={{flex:"1"}}>
               <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/"
              onClick={()=>{SetCategory('/category/university-books/engineering/')
             this.setState({ShowClick:'Engg',CompViewAll:'/category/school-children-books/'})}}
             className={(this.state.ShowClick ==='Engg')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/engg.png`} id="layer2UFimg"></img>
              <p id="layer2UFtitle">Engineering</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/university-books/finance/"
               onClick={()=>{SetCategory('/category/university-books/finance/')
               this.setState({ShowClick:'Finance',CompViewAll:'/category/school-children-books/'})}}
               className={(this.state.ShowClick ==='Finance')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/finance.png`} id="layer2UFimg"></img>
              <p id="layer2UFtitle">Finance</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/"
              onClick={()=>{SetCategory('/category/university-books/medical/')
             this.setState({ShowClick:'Medical',CompViewAll:'/category/school-children-books/'})}}
             className={(this.state.ShowClick ==='Medical')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/medical.png`} id="layer2UFimg"></img>
              <p id="layer2UFtitle">Medical</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/"
               onClick={()=>{SetCategory('/category/university-books/science-arts/')
               this.setState({ShowClick:'S&A',CompViewAll:'/category/school-children-books/'})}}
               className={(this.state.ShowClick ==='S&A')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sci.png`} id="layer2UFimg"></img>
              <p id="layer2UFtitle">Science  & Arts</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/"
               onClick={()=>{SetCategory('/category/university-books/others/')
               this.setState({ShowClick:'UO',CompViewAll:'/category/school-children-books/'})}}
               className={(this.state.ShowClick ==='UO')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png`} id="layer2UFimg"></img>
              <p id="layer2UFtitle">Others</p>
              </Link>
             </div>
              </div>
              
              ):""}

{(this.state.Click==='f&nf')? (
              <div style={{display:"flex"}}>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/"
              onClick={()=>{SetCategory('/category/fiction-non-fiction/fiction/')
              this.setState({ShowClick:'Frict',CompViewAll:'/category/school-children-books/'})}}
              className={(this.state.ShowClick ==='Frict')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fiction.png`} id="layer2UFimg"></img>
              <p id="layer2Ftitle">Fiction</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/motivation-self-help/"
               onClick={()=>{SetCategory('/category/fiction-non-fiction/motivation-self-help/')
               this.setState({ShowClick:'M&SH',CompViewAll:'/category/school-children-books/'})}}
               className={(this.state.ShowClick ==='M&SH')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/selfhelp.png`} id="layer2UFimg"></img>
              <p id="layer2Ftitle">Motivation <br/>& Self-help</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/"
              onClick={()=>{SetCategory('/category/fiction-non-fiction/non-fiction/')
              this.setState({ShowClick:'NF',CompViewAll:'/category/school-children-books/'})}}
              className={(this.state.ShowClick ==='NF')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/nonfic.png`} id="layer2UFimg"></img>
              <p id="layer2Ftitle">Non-Friction</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/religion-spirituality/"
               onClick={()=>{SetCategory('/category/fiction-non-fiction/religion-spirituality/')
               this.setState({ShowClick:'R&S',CompViewAll:'/category/school-children-books/'})}}
               className={(this.state.ShowClick ==='R&S')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/religion.png`} id="layer2UFimg"></img>
              <p id="layer2Ftitle">Religion <br/>& Spirituality</p>
              </Link>
              </div>
              <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/"
               onClick={()=>{SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/')
               this.setState({ShowClick:'NFO',CompViewAll:'/category/school-children-books/'})}}
               className={(this.state.ShowClick ==='NFO')?"MobileShowclicked":""}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png`} id="layer2UFimg"></img>
              <p id="layer2Ftitle">Others</p>
              </Link>
              </div>
              </div>
              ):""}
         </div>
         {/* ------------------------End of 2nd layer --------------------------------------- */}

         {/* -----------------------------3rd layer------------------------------------------------------------ */}
         <div>
                      {/* -----------------Competitive Exams --------------------------*/}
         {(this.state.ShowClick==='EAM')? (
        
                  <div >
                       <hr/>
                  <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/engineering/aieee/"
              onClick={()=>SetCategory('/category/competitive-exams/engineering/aieee/')}>
              <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/aieee.png' id="layer3Simg"></img>
              </Link>
              <p id="layer3Stitle">AIEEE</p>
              </div>
              <div style={{flex:"1"}}>

              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/engineering/engineering-post-graduate/"
              onClick={()=>SetCategory('/category/competitive-exams/engineering/engineering-post-graduate/')}>
              <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/gate.png' id="layer3Simg"></img>
              </Link>
              <p id="layer3Stitle">GATE</p>
              </div>

              <div style={{flex:"1"}}>

              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/engineering/medical/"
              onClick={()=>SetCategory('/category/competitive-exams/engineering/medical/')}>
              <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/medical.png' id="layer3Simg"></img>
              </Link>
              <p id="layer3Stitle">Medical</p>
              </div>

              <div style={{flex:"1"}}>

              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/engineering/others/"
              onClick={()=>SetCategory('/category/competitive-exams/engineering/others/')}>
              <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/cothers.png' id="layer3Simg"></img>
              </Link>
              <p id="layer3Stitle">Others </p>
              </div>

              <div style={{flex:"1"}}>

              <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/engineering/state-level/"
              onClick={()=>SetCategory('/category/competitive-exams/engineering/state-level/')}>
              <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/statelevel.png' id="layer3Simg"></img>
              </Link>
              <p id="layer3Stitle">State Level </p>
              </div>
              </div>
              </div>
              ):""}
              {(this.state.ShowClick==='IJ')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/international-exams/gmat/"
    onClick={()=>SetCategory('/category/competitive-exams/international-exams/gmat/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/gmat.png' id="layer3Iimg"></img>
    <p id="layer3Ititle">GMAT</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/international-exams/gre/"
    onClick={()=>SetCategory('/category/competitive-exams/international-exams/gre/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/gre.png' id="layer3Iimg"></img>
    <p id="layer3Ititle">GRE</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/international-exams/others/"
    onClick={()=>SetCategory('/category/competitive-exams/international-exams/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png' id="layer3Iimg"></img>
    <p id="layer3Ititle">Others</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/international-exams/gmat/"
    onClick={()=>SetCategory('/category/competitive-exams/international-exams/gmat/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sat.png' id="layer3Iimg"></img>
    <p id="layer3Ititle">SAT</p>
    </Link>
    </div>
      </div>
    </div>
    ):""}
{/* 
{(this.state.ShowClick==='GK')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/general-knowledge-learni/dic/"
    onClick={()=>SetCategory('/category/competitive-exams/general-knowledge-learni/dic/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/dictionary.png' id="layer3GKimg"></img>
    <p id="layer3GKtitle">Dictonary</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/general-knowledge-learni/gk/"
    onClick={()=>SetCategory('/category/competitive-exams/general-knowledge-learni/gk/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/geko.png' id="layer3GKimg"></img>
    <p id="layer3GKtitle">GK</p>
    </Link>
    </div>
      </div>
    </div>
    ):""} */}

    {(this.state.ShowClick==='SL')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/school-level/ntse/"
    onClick={()=>SetCategory('/category/competitive-exams/school-level/ntse/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ntse.png' id="layer3Simg"></img>
    <p id="layer3Stitle">NTSE</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
   <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/school-level/olympiads/"
    onClick={()=>SetCategory('/category/competitive-exams/school-level/olympiads/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/olympiad.png' id="layer3Simg"></img>
    <p id="layer3Stitle">Olympiads</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/school-level/others/"
    onClick={()=>SetCategory('/category/competitive-exams/school-level/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sothers.png' id="layer3Simg"></img>
    <p id="layer3Stitle">Others</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
   <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/school-level/navodaya-vidyalaya/"
    onClick={()=>SetCategory('/category/competitive-exams/school-level/navodaya-vidyalaya/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sschool.png' id="layer3Simg"></img>
    <p id="layer3Stitle">School </p>
    </Link>
    </div>

    <div style={{flex:"1"}}>
   <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/school-level/sainik-school/"
    onClick={()=>SetCategory('/category/competitive-exams/school-level/sainik-school/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sainikschool.png' id="layer3Simg"></img>
    <p id="layer3Stitle">Sainiks</p>
    </Link>
    </div>
    </div>
    </div>
    ):""}
    {(this.state.ShowClick==='Govt')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/banking/"
    onClick={()=>SetCategory('/category/competitive-exams/government-jobs/banking/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/banking.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Banking</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/others/"
    onClick={()=>SetCategory('/category/competitive-exams/government-jobs/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Others</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/railway/"
    onClick={()=>SetCategory('/category/competitive-exams/government-jobs/railway/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/railway.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Railway</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/ssc/"
    onClick={()=>SetCategory('/category/competitive-exams/government-jobs/ssc/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/SSC.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">SSC</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/teaching-related-exams/"
    onClick={()=>SetCategory('/category/competitive-exams/government-jobs/teaching-related-exams/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/teaching.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Teaching Related Exams</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/upsc/"
    onClick={()=>SetCategory('/category/competitive-exams/government-jobs/upsc/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/UPSC.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">UPSC</p>
    </Link>
    </div>
    <br/>
      </div>
    </div>
    ):""}
    {/* -----------------------End of Comp exams---------------- */}
    {/* ------------------------------------Children Books---------------------- */}
                   {(this.state.ShowClick==='Class')? (
        
        <div>
             <hr/>
             <MediaQuery minWidth={768}>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/kg/"
    onClick={()=>SetCategory('/category/school-children-books/classes/kg/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/KG.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">KG</p>
    </Link> 
    </div>
   
                <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-1/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-1/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/1.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 1</p>
    </Link>
    </div>
  <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-2/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-2/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/2.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 2</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-3/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-3/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/3.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 3 </p>
    </Link>
    </div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-4/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-4/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/4.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 4</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-5/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-5/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/5.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 5</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-6/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-6/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/6.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 6</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-7/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-7/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/7.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 7</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-8/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-8/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/8.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 8</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-9/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-9/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/9.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 9</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-10/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-10/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/10.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 10</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-11/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-11/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/11.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 11</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-12/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-12/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/12.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 12</p>
    </Link>
    </div>
    <br/>
    </div>
    </MediaQuery>
    <MediaQuery maxWidth={767}>
    <div>
         
    <Slider {...settings}>
                <div >
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/kg/"
    onClick={()=>SetCategory('/category/school-children-books/classes/kg/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/KG.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">KG</p>
    </Link> 
    </div>
                <div>
  <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-1/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-1/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/1.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 1</p>
    </Link>
    </div>
  <div>
 <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-2/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-2/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/2.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 2</p>
    </Link>
    </div>
 <div>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-3/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-3/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/3.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 3 </p>
    </Link>
    </div>
<div>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-4/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-4/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/4.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 4</p>
    </Link>
    </div>
 <div>
 <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-5/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-5/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/5.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 5</p>
    </Link>
    </div>
 <div>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-6/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-6/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/6.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 6</p>
    </Link>
    </div>
    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-7/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-7/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/7.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 7</p>
    </Link>
    </div>

    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-8/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-8/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/8.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 8</p>
    </Link>
    </div>

    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-9/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-9/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/9.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 9</p>
    </Link>
    </div>

    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-10/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-10/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/10.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 10</p>
    </Link>
    </div>

    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-11/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-11/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/11.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 11</p>
    </Link>
    </div>

    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/class-12/"
    onClick={()=>SetCategory('/category/school-children-books/classes/class-12/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/12.png' id="layer3CLimg"></img>
    <p id="layer3CLtitle">Class 12</p>
    </Link>
    </div>
    </Slider>
    </div>
    </MediaQuery>
    </div>
    ):""}

{(this.state.ShowClick==='Ncert')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/class-6/"
    onClick={()=>SetCategory('/category/school-children-books/ncert/class-6/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/N6.png' id="layer3NCimg"></img>
    <p id="layer3Ntitle">Class 6</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/class-7/"
    onClick={()=>SetCategory('/category/school-children-books/ncertncert/class-7/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/N7.png' id="layer3NCimg"></img>
    <p id="layer3Ntitle">Class 7</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/class-8/"
    onClick={()=>SetCategory('/category/school-children-books/ncert/class-8/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/N8.png' id="layer3NCimg"></img>
    <p id="layer3Ntitle">Class 8</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/class-9/"
    onClick={()=>SetCategory('/category/school-children-books/ncert/class-9/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/N9.png' id="layer3NCimg"></img>
    <p id="layer3Ntitle">Class 9</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/class-10/"
    onClick={()=>SetCategory('/category/school-children-books/ncert/class-10/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/N10.png' id="layer3NCimg"></img>
    <p id="layer3Ntitle">Class 10</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/class-11/"
    onClick={()=>SetCategory('/category/school-children-books/ncert/class-11/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/N11.png' id="layer3NCimg"></img>
    <p id="layer3Ntitle">Class 11</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/class-12/"
    onClick={()=>SetCategory('/category/school-children-books/ncert/class-12/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/N12.png' id="layer3NCimg"></img>
    <p id="layer3Ntitle">Class 12</p>
    </Link>
    </div>
    <br/>
    </div>
    </div>
    ):""}

{(this.state.ShowClick==='DL')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/dictionary-level-/english-to-english-/"
    onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/english-to-english-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ETE.png' id="layer3Simg"></img>
    <p id="layer3Stitle">English To English</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
   <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/dictionary-level-/english-to-hindi-/"
    onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/english-to-hindi-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ETH.png' id="layer3Simg"></img>
    <p id="layer3Stitle">English To Hindi</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/dictionary-level-/foreign-language/"
    onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/foreign-language/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Foreign.png' id="layer3Simg"></img>
    <p id="layer3Stitle">Foreign Language</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
   <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/dictionary-level-/hindi-to-english-/"
    onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/hindi-to-english-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HTE.png' id="layer3Simg"></img>
    <p id="layer3Stitle">Hindi To English </p>
    </Link>
    </div>

    <div style={{flex:"1"}}>
   <Link style={{ textDecoration: 'none' }}to="/category/school-children-books/dictionary-level-/subject-wise-/"
    onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/subject-wise-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/subjectwise.png' id="layer3Simg"></img>
    <p id="layer3Stitle">Subject Wise</p>
    </Link>
    </div>

    <br/>
    </div>
    </div>
    ):""}

{(this.state.ShowClick==='CB')? (
        
  <div>
       <hr/>
       <MediaQuery minWidth={768}>
       <div style={{display:"flex"}}>
       <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/action-and-adventure/"
onClick={()=>SetCategory('/category/school-children-books/children-books/action-and-adventure/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/action.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Action & Adventure</p>
</Link>
</div>
          <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/activity-books/"
onClick={()=>SetCategory('/category/school-children-books/children-books/activity-books/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/activity.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Activity Books</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/childrens-literature/"
onClick={()=>SetCategory('/category/school-children-books/children-books/childrens-literature/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ChildrensLiterature.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Children's literature</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/comics-graphic-novels/"
onClick={()=>SetCategory('/category/school-children-books/children-books/comics-graphic-novels/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/comic.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Comics & Graphic Novels</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/disney-store/"
onClick={()=>SetCategory('/category/school-children-books/children-books/disney-store/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/disney.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Disney Store</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/fun-humour/"
onClick={()=>SetCategory('/category/school-children-books/children-books/fun-humour/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/humor.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Fun & Humor</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/history-mythology-/"
onClick={()=>SetCategory('/category/school-children-books/children-books/history-mythology-/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/history.png' id="layer3CBimg"></img>
<p id="layer3Stitle">History & Mythology</p>
</Link>
</div>
<div style={{flex:"1"}}>

<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/knowledge-and-learning/"
onClick={()=>SetCategory('/category/school-children-books/children-books/knowledge-and-learning/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/knowledge.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Knowledge & Learning</p>
</Link>
</div>

<div style={{flex:"1"}}>

<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/others/"
onClick={()=>SetCategory('/category/school-children-books/children-books/others/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Others</p>
</Link>
</div>

<div style={{flex:"1"}}>

<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/picture-book/"
onClick={()=>SetCategory('/category/school-children-books/children-books/picture-book/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/picbook.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Picture Book</p>
</Link>
</div>
</div>
</MediaQuery>
<MediaQuery maxWidth={767}>
<div>
<Slider {...settings1}>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/action-and-adventure/"
onClick={()=>SetCategory('/category/school-children-books/children-books/action-and-adventure/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/action.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Action & Adventure</p>
</Link>
</div>
          <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/activity-books/"
onClick={()=>SetCategory('/category/school-children-books/children-books/activity-books/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/activity.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Activity Books</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/childrens-literature/"
onClick={()=>SetCategory('/category/school-children-books/children-books/childrens-literature/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ChildrensLiterature.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Children's literature</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/comics-graphic-novels/"
onClick={()=>SetCategory('/category/school-children-books/children-books/comics-graphic-novels/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/comic.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Comics & Graphic Novels</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/disney-store/"
onClick={()=>SetCategory('/category/school-children-books/children-books/disney-store/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/disney.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Disney Store</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/fun-humour/"
onClick={()=>SetCategory('/category/school-children-books/children-books/fun-humour/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/humor.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Fun & Humor</p>
</Link>
</div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/history-mythology-/"
onClick={()=>SetCategory('/category/school-children-books/children-books/history-mythology-/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/history.png' id="layer3CBimg"></img>
<p id="layer3Stitle">History & Mythology</p>
</Link>
</div>
<div style={{flex:"1"}}>

<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/knowledge-and-learning/"
onClick={()=>SetCategory('/category/school-children-books/children-books/knowledge-and-learning/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/knowledge.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Knowledge & Learning</p>
</Link>
</div>

<div style={{flex:"1"}}>

<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/others/"
onClick={()=>SetCategory('/category/school-children-books/children-books/others/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Others</p>
</Link>
</div>

<div style={{flex:"1"}}>

<Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/picture-book/"
onClick={()=>SetCategory('/category/school-children-books/children-books/picture-book/')}>
<img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/picbook.png' id="layer3CBimg"></img>
<p id="layer3Stitle">Picture Book</p>
</Link>
</div>
</Slider>
</div>
</MediaQuery>
</div>
):""}
{/* ------------------------------End of Children Books------------------------------ */}
{/* ---------------------------------------University Books-------------------- */}
    {(this.state.ShowClick==='Medical')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/ayurveda/"
    onClick={()=>SetCategory('/category/university-books/medical/ayurveda/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ayur.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Ayurveda</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/dental/"
    onClick={()=>SetCategory('/category/university-books/medical/dental/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/dental.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Dental</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/mbbs/"
    onClick={()=>SetCategory('/category/university-books/medical/mbbs/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mbbs.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">MBBS</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/others/"
    onClick={()=>SetCategory('/category/university-books/medical/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mothers.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Others</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/pg/"
    onClick={()=>SetCategory('/category/university-books/medical/pg/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/PG.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">PG</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/pharmacy/"
    onClick={()=>SetCategory('/category/university-books/medical/pharmacy/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/pharm.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Pharmacy</p>
    </Link>
    </div>
    <br/>
      </div>
    </div>
    ):""}

{(this.state.ShowClick==='S&A')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/ba/"
    onClick={()=>SetCategory('/category/university-books/science-arts/ba/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/BA.png' id="layer3Simg"></img>
    <p id="layer3SAtitle">B.A.</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/bca/"
    onClick={()=>SetCategory('/category/university-books/science-arts/bca/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/BCA.png' id="layer3Simg"></img>
    <p id="layer3SAtitle">BCA</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/bsc/"
    onClick={()=>SetCategory('/category/university-books/science-arts/bsc/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/BSC.png' id="layer3Simg"></img>
    <p id="layer3SAtitle">BSc</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/ma/"
    onClick={()=>SetCategory('/category/university-books/science-arts/ma/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MA.png' id="layer3Simg"></img>
    <p id="layer3SAtitle">M.A.</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/mca/"
    onClick={()=>SetCategory('/category/university-books/science-arts/mca/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MCA.png' id="layer3Simg"></img>
    <p id="layer3SAtitle">MCA</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/msc/"
    onClick={()=>SetCategory('/category/university-books/science-arts/msc/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MSC.png' id="layer3Simg"></img>
    <p id="layer3SAtitle">MSc</p>
    </Link>
    </div>
    <br/>
    </div>
    </div>
    ):""}
    {(this.state.ShowClick==='UO')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/bba/"
    onClick={()=>SetCategory('/category/university-books/others/bba/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bba.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">BBA</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/bcom/"
    onClick={()=>SetCategory('/category/university-books/others/bcom/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bcom.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">B Com</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/law/"
    onClick={()=>SetCategory('/category/university-books/others/law/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/LAW.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">Law</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/mba/"
    onClick={()=>SetCategory('/category/university-books/others/mba/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MBA.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">MBA</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/mcom/"
    onClick={()=>SetCategory('/category/university-books/others/mcom/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Mcom.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">M Com</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/others/"
    onClick={()=>SetCategory('/category/university-books/others/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/uoothers.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">Others</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/probability-statistics/"
    onClick={()=>SetCategory('/category/university-books/others/probability-statistics/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/p&s.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">Probability & Statistics</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/others/phd/"
    onClick={()=>SetCategory('/category/university-books/others/phd/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/phd.png' id="layer3UOimg"></img>
    <p id="layer3Utitle">PhD</p>
    </Link>
    </div>
    <br/>
    </div>
    </div>
    ):""}
    

{(this.state.ShowClick==='Engg')? (
        
        <div>
             <hr/>
             <MediaQuery minWidth={768}>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/aeronautical/"
    onClick={()=>SetCategory('/category/university-books/engineering/aeronautical/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/aero.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Aeronautical</p>
    </Link> 
    </div>
   
                <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/bio-technology/"
    onClick={()=>SetCategory('/category/university-books/engineering/bio-technology/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bio.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Biotechnology</p>
    </Link>
    </div>
  <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/chemical-/"
    onClick={()=>SetCategory('/category/university-books/engineering/chemical-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/chem.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Chemical</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/civil-/"
    onClick={()=>SetCategory('/category/university-books/engineering/civil-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/civil.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Civil</p>
    </Link>
    </div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/computer-science/"
    onClick={()=>SetCategory('/category/university-books/engineering/computer-science/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/comp.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Computer Science</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/electrical-/"
    onClick={()=>SetCategory('/category/university-books/engineering/electrical-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/electrical.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Electrical</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/electronics/"
    onClick={()=>SetCategory('/category/university-books/engineering/electronics/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/electronics.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Electronics</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/marine/"
    onClick={()=>SetCategory('/category/university-books/engineering/marine/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/marine.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Marine</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/mechanical/"
    onClick={()=>SetCategory('/category/university-books/engineering/mechanical/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mechanical.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Mechanical</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/others/"
    onClick={()=>SetCategory('/category/university-books/engineering/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ubother.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Others</p>
    </Link>
    </div>
    </div>
    </MediaQuery>
    <MediaQuery maxWidth={767}>
    <div>
    <Slider {...settings1}>
    <div >
    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/aeronautical/"
    onClick={()=>SetCategory('/category/university-books/engineering/aeronautical/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/aero.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Aeronautical</p>
    </Link> 
    </div>
                <div>
  <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/bio-technology/"
    onClick={()=>SetCategory('/category/university-books/engineering/bio-technology/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bio.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Biotechnology</p>
    </Link>
    </div>
  <div>
 <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/chemical-/"
    onClick={()=>SetCategory('/category/university-books/engineering/chemical-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/chem.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Chemical</p>
    </Link>
    </div>
 <div>
<Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/civil-/"
    onClick={()=>SetCategory('/category/university-books/engineering/civil-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/civil.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Civil</p>
    </Link>
    </div>
<div>
<Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/computer-science/"
    onClick={()=>SetCategory('/category/university-books/engineering/computer-science/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/comp.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Computer Science</p>
    </Link>
    </div>
 <div>
 <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/electrical-/"
    onClick={()=>SetCategory('/category/university-books/engineering/electrical-/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/electrical.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Electrical</p>
    </Link>
    </div>
 <div>
<Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/electronics/"
    onClick={()=>SetCategory('/category/university-books/engineering/electronics/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/electronics.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Electronics</p>
    </Link>
    </div>
    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/marine/"
    onClick={()=>SetCategory('/category/university-books/engineering/marine/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/marine.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Marine</p>
    </Link>
    </div>

    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/mechanical/"
    onClick={()=>SetCategory('/category/university-books/engineering/mechanical/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mechanical.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Mechanical</p>
    </Link>
    </div>

    <div>

    <Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/others/"
    onClick={()=>SetCategory('/category/university-books/engineering/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ubother.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Others</p>
    </Link>
    </div>
    </Slider>
    </div>
    </MediaQuery>
    </div>
    ):""}
{/* -----------------------------------End of University Books--------------------- */}
{/* ---------------------------------Friction Books-------------------------- */}
{(this.state.ShowClick==='Frict')? (
        
        <div>
             <hr/>
             <MediaQuery minWidth={768}>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/comics-graphic-novel/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/comics-graphic-novel/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/comic.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Comics & Graphic novels</p>
    </Link>
    </div>
  <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/ fiction-non-fiction/fiction/drama"
    onClick={()=>SetCategory('/category/ fiction-non-fiction/fiction/drama')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/drama.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Drama</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/horror-and-thriller/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/horror-and-thriller/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/horror.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Horror & Thriller</p>
    </Link>
    </div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/mystery/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/mystery/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mystery.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Mystery</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/romance/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/romance/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/romance.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Romance</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/science-fiction/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/science-fiction/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/scfrict.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Science Fiction</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/short-stories/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/short-stories/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/shortstories.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Short Stories</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/teens/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/teens/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/teen.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Teens</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/literary/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/literary/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/literary.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Literary</p>
    </Link>
    </div>
    </div>
    </MediaQuery>
    <MediaQuery maxWidth={767}>
    <div>
    <Slider {...settings1}>
    <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/comics-graphic-novel/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/comics-graphic-novel/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/comic.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Comics & Graphic novels</p>
    </Link>
    </div>
  <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/ fiction-non-fiction/fiction/drama"
    onClick={()=>SetCategory('/category/ fiction-non-fiction/fiction/drama')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/drama.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Drama</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/horror-and-thriller/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/horror-and-thriller/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/horror.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Horror & Thriller</p>
    </Link>
    </div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/mystery/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/mystery/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/comp.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Mystery</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/romance/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/romance/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/romance.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Romance</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/science-fiction/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/science-fiction/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/scfrict.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Science Fiction</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/short-stories/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/short-stories/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/shortstories.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Short Stories</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/teens/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/teens/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/teen.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Teens</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/literary/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/literary/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/literary.png' id="layer3Fimg"></img>
    <p id="layer3Stitle">Literary</p>
    </Link>
    </div>
    </Slider>
    </div>
    </MediaQuery>
    </div>
    ):""}

{(this.state.ShowClick==='NFO')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
                <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/astrology/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/astrology/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/astro.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Astrology</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/business-management/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/business-management/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/busi.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Business & Management</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/health/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/health/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/health.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Health</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/history-and-politics/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/history-and-politics/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/hist.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">History & Politics</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/others/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Others</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/sports-games/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/sports-games/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sports.png' id="layer3Simg"></img>
    <p id="layer3Mtitle">Sports & Games</p>
    </Link>
    </div>
    <br/>
      </div>
    </div>
    ):""}

{(this.state.ShowClick==='NF')? (
        
        <div>
             <hr/>
             <MediaQuery minWidth={768}>
             <div style={{display:"flex"}}>
             <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/biographies/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/biographies/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/biography.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Biographies</p>
    </Link>
    </div>
                <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/coffee-table/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/coffee-table/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/coffeetable.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Coffee Table</p>
    </Link>
    </div>
  <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/computer-and-internet/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/computer-and-internet/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/computer.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Computer & Internet</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/cooking/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/cooking/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/horror.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Cooking</p>
    </Link>
    </div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/entertainment/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/entertainment/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/entertainment.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Entertainment</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/househome/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/househome/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/house.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">House & Home</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/pets/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/pets/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/pet.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Pets</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/photography/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/photography/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/photography.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Photography</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/sports/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/sports/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sports.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Sports</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/travel/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/travel/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/travel.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Travel</p>
    </Link>
    </div>
    </div>
    </MediaQuery>
    <MediaQuery maxWidth={767}>
    <div>
    <Slider {...settings1}>
    <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/biographies/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/biographies/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/biography.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Biographies</p>
    </Link>
    </div>
                <div style={{flex:"1"}}>
  <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/coffee-table/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/coffee-table/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/coffeetable.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Coffee Table</p>
    </Link>
    </div>
  <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/computer-and-internet/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/computer-and-internet/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/drama.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Computer & Internet</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/cooking/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/cooking/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/horror.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Cooking</p>
    </Link>
    </div>
<div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/entertainment/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/entertainment/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/entertainment.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Entertainment</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
 <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/househome/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/househome/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/electrical.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">House & Home</p>
    </Link>
    </div>
 <div style={{flex:"1"}}>
<Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/pets/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/pets/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/pet.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Pets</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/photography/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/photography/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/shortstories.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Photography</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/sports/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/sports/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/sports.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Sports</p>
    </Link>
    </div>

    <div style={{flex:"1"}}>

    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/travel/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/travel/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/travel.png' id="layer3Cimg"></img>
    <p id="layer3Stitle">Travel</p>
    </Link>
    </div>
    </Slider>
    </div>
    </MediaQuery>
    </div>
    ):""}

{(this.state.ShowClick==='R&S')? (
        
        <div>
             <hr/>
             <div style={{display:"flex"}}>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/religion-spirituality/holy-books/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/holy-books/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/holy.png' id="layer3Simg"></img>
    <p id="layer3Rtitle">Holy Books</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/religion-spirituality/others/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/others/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/others.png' id="layer3Simg"></img>
    <p id="layer3Rtitle">Others</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/religion-spirituality/philosophy/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/philosophy/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/PHILOSOPHY.png' id="layer3Simg"></img>
    <p id="layer3Rtitle">Philosophy</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/religion-spirituality/religions/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/religions/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/religionbook.png' id="layer3Simg"></img>
    <p id="layer3Rtitle">Religions</p>
    </Link>
    </div>
    <div style={{flex:"1"}}>
    <Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/religion-spirituality/spirituality/"
    onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/spirituality/')}>
    <img src='https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/SPIRITUALITY.png' id="layer3Simg"></img>
    <p id="layer3Rtitle">Spirituality</p>
    </Link>
    </div>
    <br/>
      </div>
    </div>
    ):""}
              </div>
              
          </div>      
           </div>
    )
  }
}
const mapStateToProps = state => ({
  ClickedCategory:state.homeR.ClickedCategory,
  navurlstate:state.accountR.navurlstate,
})

export default connect(mapStateToProps,{setCategory,navurl})(Mobilecat);