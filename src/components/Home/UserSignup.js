import React, { Component } from 'react'
import './login.css'
import './userLogin.css'
import {signupCheck,showLoader,LoginCheck,clearAll,clearAllUserStatus} from '../../actions/accountAction'
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';
import Popup from 'reactjs-popup';
import { GoogleLogin } from 'react-google-login';

class UserSignup extends Component {
  
    state={
        Sopen:false,
        email:'',
        mobile:'',
        password:'',
        moblielength:false,
        fadeout:'',
        fadeoutSec:'',
        SignUpSuccessPopup:false,
    }
    sopenModal=()=>{
        this.setState({ sopen: true })
      }
      scloseModal=()=>{
        this.props.scloseModal()
    }
    changeShowLS=()=>
    {this.props.changeShowLS()}


  mobilechecker=()=>{this.setState({moblielength:true})}
  onChange = e => {this.setState({ [e.target.name]: e.target.value })};
  render() {
      var Userstatus = `${this.props.Userstatus}`
      var SignedupErr= `${this.props.SignUpErrMsg}`
      if(SignedupErr.search("Email Registered") !== -1){
        // this.changeShowLS()
      }
      let PhoneErr=""
      let PasswordErr =""
    //   For password Phone Number validation
    if(Userstatus.length !== 0){
      
      // alert("chan")
      // this.setState({SignUpSuccessPopup:true})
      setTimeout(()=>{this.props.clearAllUserStatus();this.props.scloseModal();},3000)
      
    }else{
      // this.setState({})
    }

    if(SignedupErr.length !== 0){
      // alert(SignedupErr.length)
      setTimeout(()=>{this.props.clearAll()},3000)
    }else{
      // this.setState({fadeoutSec:""})
    }

      if(this.state.mobile.length !== 10){
        //   alert("stop")
       PhoneErr = "Enter 10 Digits"
        // document.getElementsById("mobile").disable=true
        // this.mobilechecker()
    }else if(isNaN(this.state.mobile)){
         PhoneErr = "Enter Digits"
    }else{
         PhoneErr=""
    }
    // For password Validation
    if(this.state.password.length < 6){
        PasswordErr = "minimum Password length must be 6"
     }else{
        PasswordErr=""
     }
     const signupUser = (e) =>{
      e.preventDefault()
      const {mobile,email,password} = this.state
     const details={
          email,
          mobile,
          password,
      }
      if(PhoneErr === '' && PasswordErr === '' && this.state.email !== '' && this.state.password !== ''){
        this.props.showLoader()
        this.setState({fadeoutSec:""})
        this.setState({loading:true})
      this.props.signupCheck(details)
      // if(SignedupErr.length !== 0){
      //   alert(SignedupErr.length)
        // setTimeout(()=>{this.setState({fadeoutSec:"DisplayNone"})},3000)
      // }else{
      //   alert(SignedupErr.length)
      //   this.setState({fadeoutSec:""})
      // }
    
    }
      // this.props.LoginCheck(details)
      // if(this.props.user_id !== null){
      //     this.props.ChangeLogin()
      // }
      if(Userstatus.length !== ''){
        // alert("Al")
        // const {mobile,email,password} = this.state
       const  detailsAutoLogin={
          email,
          password,
        }
        // this.props.LoginCheck(detailsAutoLogin)
      }
  }
  const responseGoogle = (response) => {
    // console.log(response.profileObj.email);
    const details={
      email:response.profileObj.email,
      password:13,   
    }
    this.props.signupCheck(details)
  }
  const responseGoogleFailure=()=>{

  }
    return (
<div id="SignPopupBody">
    
    {/* <div className="formleft">
          <a style={{color:'orange',width:'2.5%',height:'4.8%',marginLeft:'-50.5%',marginTop:'-2%',position:'absolute',backgroundColor:'transparent'}} onClick={this.scloseModal}>
                  &times;
    </a>
            <button id="bt">Sign In/Sign Up</button>
    </div>
    
          <div className="formright">
               <div className="tab">
              <button className="tablinks1" onClick={this.changeShowLS} >Sign In</button>
              <button className="tablinks"  onClick={this.handleClick}>Sign Up</button>
          </div>
          <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,height:'5%'}}>

            <span id={this.state.fadeout}>{Userstatus}</span>
            
          {(Userstatus.length !== 0)?null:
            <span id={this.state.fadeoutSec}>{SignedupErr}</span>
            
          }  </p>
          <form style={{padding:  '0 40px'}} onSubmit={signupUser}>
    
                    <input type="text"  style={{height:'20%',marginTop:'0px',lineHeight:'0px'}} name="mobile" id="mobile"
                    placeholder="Enter Mobile Number"required
                    onChange={e => this.setState({mobile: e.target.value})}
                    disabled={this.state.moblielength}
                    maxLength="10"
                    required
                   />
        <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,fontWeight:'lighter',height:'5%',lineHeight:'0px'}}>{PhoneErr}</p>
                   
                   <input type="email"  style={{height:'20%',marginTop:'0px',lineHeight:'0px'}} name="email"
                    placeholder="Enter Email ID"required 
                    onChange={e => this.setState({email: e.target.value})}
                    required maxLength="50"
                    />
                    <input type="Password"  style={{height:'20%',marginTop:'0px',lineHeight:'0px'}} name="password"
                    placeholder="Create MyPustak Password"required
                    onChange={e => this.setState({password: e.target.value})}
                    required maxLength="50"
                    />
                     <p style={{ fontSize:"70%",color:'red',padding:'0px',fontWeight:'lighter',margin:'0px' ,height:'5%',lineHeight:'0px'}}>{PasswordErr}</p>
                    
                
                <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>

                       <Popup 
                       open={this.props.Userstatus.length !== 0}
                       contentStyle={{ 
             
                         border:'0px',
                        }}
                       >
                       <p style={{ color:'#00baf2' }}>Your Accout is Created Succesfully </p>
                       </Popup>
    
          <input style={{ backgroundColor:'#00baf2',color:'white',borderRadius:'5px',border:'none',
                      width:'59.2%',height:'10.2%',marginLeft:'2.2%',marginTop:'2%' ,padding:'0px'
                }} type="submit" value="Sign Up" 
                />
                <br/>  </form>
                <label style={{color:'black',marginTop:'1%',position:'absolute',marginLeft:'20%'}}><b>OR</b></label>
        <label style={{color:'black',marginTop:'8.1%',marginLeft:'6%'}}>Simply Signup In Using</label>

        <img src={require('./images/fb.png')} style={{width:'8%',position:'absolute',marginTop:'17.5%',marginLeft:'-30%'}}></img>
        <img src={require('./images/google.png')} style={{width:'8%',position:'absolute',marginTop:'17.5%',marginLeft:'-19%'}}></img>
        <label style={{color:'black',fontWeight:'normal',marginTop:'0%',marginLeft:'4%'}}>By Logging In, You Agree To Our <b>Terms & </b></label>
        <label style={{color:'black',fontWeight:'normal',marginLeft:'-55%',marginTop:'5%',position:'absolute'}}><b>Conditions</b> & Privacy Policy.</label>
          </div> */}

          <div id="LoginpopupLeft">
              <p >Sign In/Sign Up</p>
          </div>
          <div id="LoginpopupRight">
                <a style={{ backgroundColor:'transparent',marginTop:"-4%",marginLeft:'24%',color:'red',position:'absolute',fontSize:'1.2rem'}}
                    onClick={this.scloseModal}>
                            &times;
                </a>
              <p><span className="BtnPopUptop"  onClick={this.changeShowLS}>Sign In</span><span className="BtnPopUptop" id="ULShowClickedBtn" onClick={this.handleClick} >Sign Up</span></p>
              <div>
                <form onSubmit={signupUser}  style={{ padding:'0% 10%',marginTop:'5%',}}>
                <input type="text"  style={{height:'20%',marginTop:'0px',lineHeight:'0px',paddingTop:'4%'}} name="mobile" id="mobile"
                    placeholder="Enter Mobile Number"required
                    onChange={e => this.setState({mobile: e.target.value})}
                    disabled={this.state.moblielength}
                    maxLength="10"
                    required
                   />
    
              <p style={{ display:'block',textAlign:'center',fontSize:"70%",color:'red',fontWeight:'lighter',height:'5%',lineHeight:'120%'}}>{PhoneErr}</p>
                   
                   <input type="email"  style={{height:'20%',marginTop:'0px',lineHeight:'0px',paddingTop:'4%'}} name="email"
                    placeholder="Enter Email ID" required 
                    onChange={e => this.setState({email: e.target.value})}
                     maxLength="50"
                    />
                    <input type="Password"  style={{height:'20%',marginTop:'0px',lineHeight:'0px',paddingTop:'4%'}} name="password"
                    placeholder="Create MyPustak Password"required
                    onChange={e => this.setState({password: e.target.value})}
                     maxLength="50"
                    />
                     <p style={{ display:'block',textAlign:'center',fontSize:"70%",color:'red',padding:'0px',fontWeight:'lighter',margin:'0px' ,height:'5%',lineHeight:'120%'}}>{PasswordErr}</p>
                    
                       <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>
                        <Popup 
                       open={this.props.Userstatus.length !== 0}
                       contentStyle={{ 
             
                         border:'0px',
                        }}
                       >
                       <p style={{ color:'#00baf2' }}>Your Accout is Created Succesfully </p>
                       </Popup>
                       {/* <input style={{ backgroundColor:'#00baf2',color:'white',borderRadius:'5px',border:'none',
                      width:'59.2%',height:'10.2%',marginLeft:'2.2%',marginTop:'2%' ,padding:'0px'
                }} type="submit" value="Sign Up" 
                /> */}
              <span id="signUpErr">{SignedupErr}</span>

              <div style={{ textAlign:'center' }}>    
                 <input className="SignupBtnHomePopup" 
                    type="submit" value="Sign Up" 
                  />
            </div>
            <GoogleLogin 
  theme="dark"
   onSuccess={responseGoogle}
    onFailure={responseGoogleFailure} 
    buttonText="Sign Up"
    clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
    cookiePolicy={'single_host_origin'} />
                {/* <p style={{ textAlign:'center',display:'block' }}>OR</p>
        <p style={{ textAlign:'center',display:'block',fontWeight:'700',fontSize:'110%'}}>Simply Signup In Using</p>
        <div style={{ display:'flex',  }}>
        <span style={{ flex:'1',}}><img src={require('./images/fb.png')} style={{ height:'65%',width:'22%',float:'right',marginRight:'5%'}}></img></span>
        <span style={{ flex:'1'}}><img src={require('./images/google.png')} style={{ height:'65%',width:'22%',float:'left',marginLeft:'5%'}}></img></span>
       
        </div> */}
        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%',marginTop:'10%' }}>By Logging In,You Agree To Our<b> Terms &</b></p>
        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%' }}><b>Conditions</b>&PrivacyPolicy.</p>

                </form>
              </div>
          </div>

           </div>
    )
  }
}
const mapStateToProps = state => ({
    Userstatus:state.accountR.status,
    ErrMsg:state.accountR.ErrMsg,
    loader:state.accountR.loading,
    SignUpErrMsg:state.accountR.SignUpErrMsg,
  })
export default connect(mapStateToProps,{LoginCheck,signupCheck,showLoader,clearAll,clearAllUserStatus})(UserSignup);