import React, { Component } from 'react'
import './book.css'
import MediaQuery from 'react-responsive';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import Skeleton from '@grovertb/react-skeletor'
import SmoothImage from 'react-smooth-image'

export default class Book extends Component {
    details(){
        // alert("details")
    }
    ToProduct(){

        window.location = `/product`
        
          }
    state={
        MouseHover:false,
    }
  render() {
      
    const {title,price,thumb,is_out_of_stack,slug} = this.props.book;
    const src=`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${thumb}`
    const ResizeTitle=(title)=>{
        if(title.length > 38){
            return title.substr(0,38)+'...'
        }else{
            return title
        }
    }
    const RedirectToNewTab=(url)=>{
        window.open(url);
    }
    return (
        
        // const {id,name,email,phone,website} = this.props.contact;
    //   <div>
            // <div class="book"> style={{ backgroundColor:'rgba(255, 255, 255, .7)' }}
            // <div id="book" style={{
            // boxShadow: '1px 1px 1px 1px rgb(0,0,0,0.5)',marginLeft:'5%',
            // textAlign:'center',height:'320px',margin:'.5%',borderRadius:'3px',
            // width:'17%',float:'left'}}>
            
            <div>
            {/* Desktop */}
            <MediaQuery minWidth={992} >     
                    <div id={this.state.MouseHover?"Hoversbook":"sbook"} 
                    onMouseEnter={()=>this.setState({MouseHover:!this.state.MouseHover})}
                    onMouseLeave={()=>this.setState({MouseHover:!this.state.MouseHover})}

                    >
                        {/* <Link style={{ textDecoration: 'none' }} to={`/product/${slug}`} > */}
                            <span id={(is_out_of_stack === "Y")?"out":"boxNone"}><p>OUT OF STOCK</p></span>
                            <img src={src} style={{ height: '16vw',width:' 77%',paddingTop:'5%' }} onClick={()=>RedirectToNewTab(`/product/${slug}`)} />
  
                        {/* </Link> */}
                        <span>

                          {/* <Link style={{ textDecoration: 'none' }} to={`/product/${slug}`} > */}
                              <p  onClick={()=>RedirectToNewTab(`/product/${slug}`)} 
                              style={this.state.MouseHover?{color:'#333',cursor:'pointer',
                                                                fontSize:'90%',height:'4vw',
                                                                overflow:'hidden',padding:'1%',
                                                                paddingBottom:'0%', marginBottom:'0rem',
                                                                marginTop: '3%',
                                                            }:{ color:'DodgerBlue',fontSize:'90%',height:'4vw',overflow:'hidden',
                                          padding:'1%',paddingBottom:'0%', marginBottom:'0rem',marginTop: '3%',}}>
                                          {ResizeTitle(title)} 
                              </p>
                          {/* </Link> */}
                          <span style={{ fontSize:'100%',marginBottom:'0px',textDecoration:'line-through' }}>Rs: {price} </span>
                          <span style={{ float:'center',marginBottom:'0px',color:'red',fontWeight:'bold'}}>&nbsp;FREE </span>
                          </span>
                      </div>
            </MediaQuery>
            {/* Tab */}
            <MediaQuery minWidth={768} maxWidth={991}>   
            <div id="sbook" >
             <div style={{ height: '70%',width:'85%',paddingLeft:'15%',marginTop:'4%' }}>
             <Link style={{ textDecoration: 'none' }} to={`/product/${slug}`} >
             <span id={(is_out_of_stack === "Y")?"out":"boxNone"}><p>OUT OF STOCK</p></span>
             {/* <img src={src} style={{ height: '15.5vw',width:' 95%',paddingTop:'2%' }} onClick={this.BlankSearch} /> */}
             <LazyLoadImage
                                // placeholder={<p>Loading....</p>}
                                effect="blur"
                                // alt="Loading..."
                                // height={305}
                                // placeholderSrc="Loading...."
                                style={{ height: '15.5vw',width:' 95%',paddingTop:'2%' }}
                                onClick={this.BlankSearch}
                                src={src} />
             </Link></div>
            <span><Link style={{ textDecoration: 'none' }} to={`/product/${slug}`}>
                <p style={{ color:'#00bff3',fontSize:'80%',height:'auto',overflow:'hidden',maxHeight:'4.5vw',
                padding:'1%',paddingBottom:'0%', marginBottom:'0rem',marginTop:'-5%'}}>{title} </p></Link>
                <span style={{ fontSize:'80%',marginBottom:'0px',textDecoration:'line-through' }}>Rs: {price} </span>
                <span style={{ float:'center',marginBottom:'0px',color:'red',fontWeight:'bold',fontSize:'80%'}}>&nbsp;FREE </span>
                </span>
            </div>
            </MediaQuery>
            {/* Larger Mobile */} 
            <MediaQuery maxWidth={767} minWidth={539} >   
            <div id="sbook" >
             <div style={{ height: '70%',width:'85%',paddingLeft:'15%',marginTop:'4%' }}>
             <Link style={{ textDecoration: 'none' }} to={`/product/${slug}`} >
             <span id={(is_out_of_stack === "Y")?"out":"boxNone"}><p>OUT OF STOCK</p></span>
             {/* <img src={src} style={{ height: '20.5vw',width:' 95%',paddingTop:'2%' }} onClick={this.BlankSearch} /> */}
             <LazyLoadImage
                                // placeholder={<p>Loading....</p>}
                                effect="blur"
                                // alt="Loading..."
                                // height={305}
                                // placeholderSrc="Loading...."
                                style={{ height: '20.5vw',width:' 95%',paddingTop:'2%' }}
                                onClick={this.BlankSearch}
                                src={src} />
             </Link></div>
            <span><Link style={{ textDecoration: 'none' }} to={`/product/${slug}`}>
                <p style={{ color:'#00bff3',fontSize:'80%',height:'auto',overflow:'hidden',maxHeight:'6.5vw',
                padding:'1%',paddingBottom:'0%', marginBottom:'0rem',marginTop:'-10%',lineHeight:'128%'}}>{title} </p></Link>
                <span style={{ fontSize:'80%',marginBottom:'0px',textDecoration:'line-through' }}>Rs: {price} </span>
                <span style={{ float:'center',marginBottom:'0px',color:'red',fontWeight:'bold',fontSize:'80%'}}>&nbsp;FREE </span>
                </span>
            </div>
            </MediaQuery>

             {/* Smaller Mobile */} 
            <MediaQuery maxWidth={538} >  
           
            <div id="sbook" >
             <div style={{ height: '70%',width:'85%',paddingLeft:'15%',marginTop:'4%' }}>
             <Link style={{ textDecoration: 'none' }} to={`/product/${slug}`} >
             <span id={(is_out_of_stack === "Y")?"out":"boxNone"}><p>OUT OF STOCK</p></span>
             {/* <img src={src} style={{ height: '42.5vw',width:' 114%',paddingTop:'1%',marginLeft:'-7%' }} onClick={this.BlankSearch} /> */}
             <LazyLoadImage
                                // placeholder={<p>Loading....</p>}
                                effect="blur"
                                // alt="Loading..."
                                // height={305}
                                // placeholderSrc="Loading...."
                                style={{ height: '42.5vw',width:' 114%',paddingTop:'1%',marginLeft:'-7%' }}
                                onClick={this.BlankSearch}
                                src={src} />
             </Link></div>
            <span><Link style={{ textDecoration: 'none' }} to={`/product/${slug}`}>
                <p style={{ color:'#00bff3',fontSize:'3.1vw',height:'auto',overflow:'hidden',maxHeight:'7.6vw',
                padding:'1%',paddingBottom:'0%', marginBottom:'0rem',marginTop:'4%',lineHeight:'128%'}}>{title} </p></Link>
                <span style={{ fontSize:'4vw',marginBottom:'0px',textDecoration:'line-through' }}>Rs: {price} </span>
                <span style={{ float:'center',marginBottom:'0px',color:'red',fontWeight:'bold',fontSize:'4vw'}}>&nbsp;FREE </span>
                </span>
            </div>
            </MediaQuery>
            </div>
    )
  }
}
// width: 186px;
// height: 280px;