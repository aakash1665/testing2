import React, { Component } from 'react';
import './order.css';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import {Get_Rp_Id} from '../../actions/donationActions'
import img from'./images/img.jpg';
import loc from './images/loc.png';
import {Helmet} from 'react-helmet';
import MainHeader from '../MainHeader';
import { Reset_Rp_Id } from "../../actions/donationActions";
import MainFooter from '../MainFooter';
import {orderdetails ,CancelOrder,SetReloadOrder,ConvertOrder,ViewOrderDetails,MakeCancelOrderBlank} from '../../actions/orderAction'
import { connect } from 'react-redux';
import {userdetails,} from '../../actions/accountAction'
import axios from 'axios'
import config from 'react-global-configuration'
import Popup from 'reactjs-popup' 
// import del from './images/del.png'
import ReactPaginate from 'react-paginate';
import OrderPopUp from './orderpopup'
import MediaQuery from 'react-responsive';
import Loader from 'react-loader-spinner'
import PaytoConvert from './PaytoConvert';
import { number } from 'prop-types';
import ReactGA from 'react-ga';
import ConvProceedToPay from '../ConvProceedToPay';


class Order extends Component {
  state={
    name:'',
    email:'',
    phone:'',
    title:'',
    amount:'',
    order_no:'',
    no_of_book:'',
    book_thumb:'',
    book_price:'',
    i_date:'',
    status:'',
    orderopen:false,
    SelectedBook:[],
    count:1,
    SelectedOrder:'',
    Token:'',
    page:1,
    RelodPage:false,
    SelectedPageNo:1,
    CancelOrderPopup:false,
    Cartloader:true,
    ConvertAmtResult:this.props.convertAmt,
    cancelOrderId:0,
    ConvOrderPopup:false,
    OpenProceedPayBtn:false,
    ConvCost:0,
    Convcount:1,
    ConvOrderId:0,
    Order_track_popup: false,
    Order_track_popup_status:"",
  }
  orderopenModal=(Order_id)=>{
    const details=`Token ${this.props.userToken}`
    // alert(Order_id)
    this.props.ViewOrderDetails(details,Order_id);
    this.setState({ orderopen: true,})
  }
 ordercloseModal=()=>{
    
    this.setState({ orderopen: false })
   }

   renderRedirect1  (e) {
  window.location.href = "/orders";
  }
  GetTrack=(order_id,e)=>{
    // alert(order_id)
    const passdata = {
      data :
        {
          "ref_id" : order_id
      }
      }
  
    // e.preventDefault();
    // console.log(passdata)
    axios.post(`http://127.0.0.1:8000/api/v1/post/get_ordertrack_url`,passdata   
    ).then(res=>{
      console.log(`${res.data} search book`);
      
       window.location.href=res.data.output
        
    }).catch(err=>{
    this.setState({Order_track_popup: true, Order_track_popup_status:err.response.data.message})  
     console.log(err)});
};



  componentDidMount() {
    ReactGA.pageview(window.location.pathname + window.location.search);
    
    if(localStorage.getItem('user') === null){
      // alert("RED")
      window.location.href="/"
    }
      window.scrollTo(0, 0);
      this.setState({count:1});
      this.setState({CancelOrderPopup:false,cancelOrderId:0})


      // const self = this;
      // self.interval = setInterval(function() {
      //   self.setState({
      //     now: Date.now(),
          
      //   });
      // }, 1000);
    
  
    // const details=`Token ${this.props.userToken}`
    const details=`Token ${this.props.userToken}`
  //   const timestamp = Date.now(); // This would be the timestamp you want to format
  //   const prev=timestamp.getDate()-30
  // console.log(new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(prev));
    this.props.orderdetails(details)
    this.props.userdetails(details)
  }
  componentWillReceiveProps(userToken){
    if(userToken.userToken !== this.state.Token ){
      const details=`Token ${userToken.userToken}`
      // this.props.userdetails(details)
   
      // this.props.Getaddress(details)
      // alert(this.state.page)
      this.props.orderdetails(details,this.state.page)
      this.props.userdetails(details)
      this.setState({Token:userToken.userToken})

      // this.setState({AddedAddressState:AddedAddress.AddedAddress})
    }
  }

  closeModal = () => this.setState({Order_track_popup:false})


render(){

  const {getorderdetails,singleOrderDetailsValue}=this.props
  var SelectedBook=[]
const  getorderid=(order)=>{
  console.log(order)
  this.setState({SelectedOrder:order})
console.log(SelectedBook);

      }
const handlePageClick = data => {
const details=`Token ${this.props.userToken}`

  this.props.orderdetails(details,data.selected+1)
  this.setState({SelectedPageNo:data.selected+1})
  // alert(data.selected+1)
  window.scrollTo(0, 0);
};
const DoRedirect=()=>{
  // alert("Okk")
  // return <Red
}
const CancelOrder=(orderid)=>{

  this.setState({CancelOrderPopup:true})
  this.setState({cancelOrderId:orderid})

}

const ConfirmCancelOrder=()=>{

  const Data ={
    data :
        {
          "order_id" : this.state.cancelOrderId
      }
   

  }
  const token = `Token ${this.props.userToken}`
  
  // alert(token)
  this.props.CancelOrder(Data,token)
  // alert(Data,token)
  this.setState({RelodPage:true})
}

const ConvertOrder=(orderid)=>{
// alert("Okk")
  const token = `Token ${this.props.userToken}`
  
  // alert(token)
  this.props.ConvertOrder(orderid,token)
  this.setState({ConvOrderId:orderid})


  this.setState({ConvOrderPopup:true})
}
if(this.props.convertAmt.length !== 0){
  // const SendDataRzpzy={
  //   data:{
  //    "ref_id" : `CoverttToPrepaid${this.props.convertAmt}`,
  //    "amount" : `${number(this.props.convertAmt)}`
  //   }
  // }
  // this.props.Get_Rp_Id(SendDataRzpzy)
  // alert("P")
  // this.HitRzPay()
}

if(this.props.RAZORPAY !==0 && this.state.Convcount==1){
  // alert("oo")
  this.setState({Cartloader:false,Convcount:2})
  // this.props.Reset_Rp_Id();

}

if(this.props.ReloadOrders ){
  const details=`Token ${this.props.userToken}`
  this.props.orderdetails(details,this.state.SelectedPageNo)
  this.props.SetReloadOrder()
}

// ORDER STATUS :: 0- processing, 1- complete, 2- cancel, 3- refund, 4- fail, 5- shipping, 6- shippment booked, 7- ready to ship


// Display None 3 btn
//  Reload page  
// http://103.217.220.149:80/api/v1/order_details_api/fetch-order-book-by-orderid/132/

const GetStatusMsg=(status)=>{
  if(Number(status) === 0)
  {
    return <span>Processing</span>
  } else if(Number(status) === 1){
    return <span>Complete</span>
  }
  else if(Number(status) === 2){
    return <span>Canceled </span>
  }
  else if(Number(status) === 3){
    return <span>Refund</span>
  }
  else if(Number(status) === 4){
    return <span>Fail</span>
  }
  else if(Number(status) === 5){
    return <span>Shipping</span>
  }
  else if(Number(status) === 6){
    return <span>Shippment booked</span>
  }
  else if(Number(status) === 7){
    return <span>Ready to ship</span>
  }
  else{
    return <span></span>
  }

}
const ConfirmConv= (amount)=>{
  this.setState({ConvCost:amount})
  const SendDataRzpzy={
    data:{
     "ref_id" : `CoverttToPrepaid${this.props.convertAmt}`,
     "amount" : Math.round(amount)*100
    }
  }
  this.props.Get_Rp_Id(SendDataRzpzy)
  this.setState({OpenProceedPayBtn:true})
  // this.s
}
return (
      <div >
         
         <MainHeader/>
     <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style></Helmet>
{/* <MediaQuery minWidth={992}>
      <p id="Your-Account">Your Account>Your Orders & Book Donations</p>
</MediaQuery> */}
    <label id="Your-Orders-Books-Donations">Your Orders & Books Donations
    {/* <input type="search" placeholder="Search All Orders & Books Donations" name="search" id="allordersinput"/>
  <button type="submit" id="searchbtn"> Search</button> */}
  </label>
<br/><br/><br/>
{/* <p>{new Intl.DateTimeFormat('en-US', {year: 'numeric', month: '2-digit',day: '2-digit'}).format(this.state.now)}</p> */}
  {/* <table id="otable" >
      <tr> */}
        {/* <th >  */}
        <Link to='/customer/customer_order'><button type="submit" id="ordersbtn" >Orders</button></Link>
       <Link to='/donor/donor_donation_request'> <button type="button" id="donationbtn" >Book Donations</button></Link>
       {/* ---------------------------Popup for convert order-------------------- */}
             {/*---------------------------------- Desktop--------------------------------------- */}
<MediaQuery minWidth={992} >
       <Popup
                      open={this.state.ConvOrderPopup}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({ConvOrderPopup:false})}}
                      // closeOnDocumentClick={false}
                      contentStyle={{ width:'20%',height:'25%',borderRadius:'3px'}}
                      > 
                   
                     <div id="ConvOrderdata">
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.props.GotConvertResponse?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>

                     <p id="convertPayText">You Need To Pay &#8377; {this.props.convertAmt} To Convert Your Order</p>
                     <div style={{ textAlign:'center' }}>
                     <button id="OrderproceedToPayBtn" onClick={()=>ConfirmConv(this.props.convertAmt)}>Proceed Pay &#8377; {this.props.convertAmt}</button>
                     </div>
                     </div>
                  </Popup>

                  <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false,ConvCost:2,ConvOrderId:0});this.props.Reset_Rp_Id()}}
                      closeOnDocumentClick={false}
                      contentStyle={{ width:'20%',borderRadius:'3px'}}
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?
                      
                     <ConvProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false})}} ConvCost={this.state.ConvCost} ConvOrderId={this.state.ConvOrderId}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
 {/* ------------------------------------tab---------------------------------------*/}
                <MediaQuery maxWidth={991} and minWidth={768}>
                    <Popup
                      open={this.state.ConvOrderPopup}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({ConvOrderPopup:false})}}
                      // closeOnDocumentClick={false}
                      contentStyle={{ width:'25%',height:'25%',borderRadius:'3px'}}
                      > 
                   
                     <div id="ConvOrderdata">
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.props.GotConvertResponse?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>

                     <p id="convertPayText">You Need To Pay &#8377; {this.props.convertAmt} To Convert Your Order</p>
                     <div style={{ textAlign:'center' }}>
                     <button id="OrderproceedToPayBtn" onClick={()=>ConfirmConv(this.props.convertAmt)}>Proceed Pay &#8377; {this.props.convertAmt}</button>
                     </div>
                     </div>
                  </Popup>

                  <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false,ConvCost:2,ConvOrderId:0});this.props.Reset_Rp_Id()}}
                      closeOnDocumentClick={false}
                      contentStyle={{ width:'30%',borderRadius:'3px'}}
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?
                      
                     <ConvProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false})}} ConvCost={this.state.ConvCost} ConvOrderId={this.state.ConvOrderId}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
              {/* ----------------------------larger mobile------------------------- */}
              <MediaQuery maxWidth={767} and minWidth={539}>
                    <Popup
                      open={this.state.ConvOrderPopup}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({ConvOrderPopup:false})}}
                      // closeOnDocumentClick={false}
                      contentStyle={{ width:'35%',height:'25%',borderRadius:'3px'}}
                      > 
                   
                     <div id="ConvOrderdata">
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.props.GotConvertResponse?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>

                     <p id="convertPayText">You Need To Pay &#8377; {this.props.convertAmt} To Convert Your Order</p>
                     <div style={{ textAlign:'center' }}>
                     <button id="OrderproceedToPayBtn" onClick={()=>ConfirmConv(this.props.convertAmt)}>Proceed Pay &#8377; {this.props.convertAmt}</button>
                     </div>
                     </div>
                  </Popup>

                  <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false,ConvCost:2,ConvOrderId:0});this.props.Reset_Rp_Id()}}
                      closeOnDocumentClick={false}
                      contentStyle={{ width:'33%',borderRadius:'3px'}}
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?
                      
                     <ConvProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false})}} ConvCost={this.state.ConvCost} ConvOrderId={this.state.ConvOrderId}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
{/*---------------------------------- smaller mobiles-------------------------------- */}
                    <MediaQuery maxWidth={538} >
                    <Popup
                      open={this.state.ConvOrderPopup}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({ConvOrderPopup:false})}}
                      // closeOnDocumentClick={false}
                      // contentStyle={{ width:'45%',height:'25%',borderRadius:'3px'}}
                      > 
                   
                     <div id="ConvOrderdata">
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.props.GotConvertResponse?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>

                     <p id="convertPayText">You Need To Pay &#8377; {this.props.convertAmt} To Convert Your Order</p>
                     <div style={{ textAlign:'center' }}>
                     <button id="OrderproceedToPayBtn" onClick={()=>ConfirmConv(this.props.convertAmt)}>Proceed Pay &#8377; {this.props.convertAmt}</button>
                     </div>
                     </div>
                  </Popup>

                  <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false,ConvCost:2,ConvOrderId:0});this.props.Reset_Rp_Id()}}
                      closeOnDocumentClick={false}
                      contentStyle={{ width:'55%',borderRadius:'3px',height:'8.5%'}}
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?
                      
                     <ConvProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false})}} ConvCost={this.state.ConvCost} ConvOrderId={this.state.ConvOrderId}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
       {/* ---------------------------End Popup for convert order-------------------- */}

                  {/* <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false,ConvCost:2,ConvOrderId:0});this.props.Reset_Rp_Id()}}
                      closeOnDocumentClick={false}
                      contentStyle={{ width:'20%',borderRadius:'3px'}}
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?
                      
                     <ConvProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false})}} ConvCost={this.state.ConvCost} ConvOrderId={this.state.ConvOrderId}/>:null}
                     </div>
                  </Popup> */}
{/* ---------------------Confirm Cancel Popup------------------------- */}
      {/* -----------------------For Other than Small Mobile---------------- */}

<MediaQuery minWidth={538}>
                  <Popup
                      open={this.state.CancelOrderPopup}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({CancelOrderPopup:false,cancelOrderId:0});
                                    this.props.MakeCancelOrderBlank()
                    }}
                      // closeOnDocumentClick={false}
                      contentStyle={{ width:'auto',height:'25%',borderRadius:'3px', }}
                      > 
                     <div>
                       {(this.props.CancelOrderMessage.length === 0)?
                     <button type="submit" id="cancelbtnPopup" onClick={()=>ConfirmCancelOrder()}>
                      Cancel Order No {this.state.cancelOrderId}
                      </button>
                      :
                      <div>
                      <p style={{ textAlign:'center',color:'#38ACEC' ,marginTop:"10%"}}>{this.props.CancelOrderMessage.message}</p>
                       <p style={{ textAlign:'center'}}>{this.props.CancelOrderMessage.message ==="Cancellation Not Allowed"?" Please Contact Our Support Team write us support@mypustak.com for more details":null}</p>
                       </div>
                      }
                     </div>
                  </Popup>
              </MediaQuery>
                  {/* -----------------------For Small Mobile---------------- */}

              <MediaQuery maxWidth={538}>
                  <Popup
                      open={this.state.CancelOrderPopup}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({CancelOrderPopup:false,cancelOrderId:0});
                                    this.props.MakeCancelOrderBlank()
                    }}
                      // closeOnDocumentClick={false}
                      contentStyle={{ height:'25%',borderRadius:'3px',width:'auto'}}
                      > 
                     <div>
                       {(this.props.CancelOrderMessage.length === 0)?
                     <button type="submit" id="cancelbtnPopup" onClick={()=>ConfirmCancelOrder()} >
                      Cancel Order No {this.state.cancelOrderId}
                      </button>
                      :
                      <div>
                      <p style={{ textAlign:'center',color:'#38ACEC' ,marginTop:"10%"}}>{this.props.CancelOrderMessage.message}</p>
                       <p style={{ textAlign:'center'}}>{this.props.CancelOrderMessage.message ==="Cancellation Not Allowed"?" Please Contact Our Support Team write us support@mypustak.com for more details":null}</p>
                       </div>
                      }
                     </div>
                  </Popup>
              </MediaQuery>
        {/* </th>
        <th > </th>
        <th > </th>
        <th ></th> */}

        {/* <th >  <button type="submit" id="openordersbtn" onClick={()=>this.renderRedirect2()}>Open Orders</button></th>
        <th > <button type="submit" id="cancelordersbtn" onClick={()=>this.renderRedirect3()}>Cancelled Orders</button></th> */}
        {/* <th > <button type="submit" id="donationbtn" onClick={()=>this.renderRedirect()}>Book Donations</button></th> */}
      {/* </tr>
  </table> */}
  <hr id="ohr"/>

   {/* <label id="Orders-Placed-In"> Orders Placed In </label>


<select id="select">
  <option value="" id="op">Last 30 Days</option>
  <option value="" id="op">Last 6 Months</option>
  <option value="" id="op">Last 1 Year</option>
</select>
 <label id="Payment-Method"> Payment Method </label> 
    
<select id="select">
  <option value="" id="op">Cash On Delivery</option>
  <option value="" id="op">Prepaid</option>
</select>
<label id="Order-Status"> Order Status </label>
<select id="select">
  <option value="" id="op">Delivered</option>
  <option value="" id="op">Yet To Deliver</option>
  <option value="" id="op"></option>
</select> */}
{getorderdetails.map(order=>
<div id="div1">
    <br/>
    <MediaQuery minWidth={539}>
    <label id="Order-Placed"> Order Placed On :</label>
     <label id="Shipped-To"> Shipped To </label>
      <label id="Placed-by"> Placed By </label>
       <label id="order-total"> Order Total </label>
       <label id="order-id"> Order ID: #<b>{order.order_id}</b></label>
        <br/>
        {/* {var d = new Date(this.state.getorderdetails.i_date);} */}
       {/*date*/ }{/* {order.actual_date_upload} */}
         <label id="Order-Placed"><b> {order.actual_date_upload}</b></label>
         <label id="dt"> {this.props.getuserdetails.name} </label> 
          <label id="dt1"> {this.props.getuserdetails.name} </label> 
           <label id="dt2">&#8377;{order.amount}</label> 
            {/* <label id="dt3"> View Order Details </label>  */}
                <hr id="hr1"/>
                <p id="totalbooks">Total No Of Books In the order ={order.no_of_book}</p>
            <p id="payusing"> Pay Using : {order.payusing}</p>
            <p id="orderstatus">  Order Status: {GetStatusMsg(order.status)} </p>
            <label id="dt3" onClick={()=>{this.orderopenModal(order.order_id);getorderid(order)}}> <b>View Order Details</b> </label>
            <label id="deliverydate"></label>
            </MediaQuery>
                {/* smaller mobile */}
            <MediaQuery maxWidth={538}>

       <label id="order-id"> Order ID: #<b>{order.order_id}</b></label>
       <label id="dt3" onClick={()=>{this.orderopenModal(order.order_id);getorderid(order)}}> <b>View Order Details</b> </label>
        <br/>
        {/* {var d = new Date(this.state.getorderdetails.i_date);} */}
       {/*date*/ }{/* {order.actual_date_upload} */}

            {/* <label id="dt3"> View Order Details </label>  */}
                <hr id="hr1"/>
                <p id="Shipped-To"> Shipped To: {this.props.getuserdetails.name}</p>
                <p id="Placed-by"> Placed By: {this.props.getuserdetails.name}</p>
                <p id="Order-Placed"> Order Placed On : {order.actual_date_upload}</p>
                <p id="order-total"> Order Total: &#8377;{order.amount}</p>
    {/* <label id="Order-Placed"><b> </b></label>
         <label id="dt"> {this.props.getuserdetails.name} </label> 
          <label id="dt1"> {this.props.getuserdetails.name} </label>  */}
           {/* <label id="dt2"></label>  */}

                <p id="totalbooks">Total No Of Books In the order ={order.no_of_book}</p>
            <p id="payusing"> Pay Using : {order.payusing}</p>
            <p id="deliverydate"> </p>
            <p id="orderstatus">  Order Status: {GetStatusMsg(order.status)} </p>
           
            </MediaQuery>
            
            {/* <img src={img} alt="Add" style={{width:'10%'}}></img> */}
            {/* <label style={{marginTop:'2%',position:'absolute',width:'20%'}}>{order.title}</label> */}
            {/* <label style={{marginTop:'4%',position:'absolute'}}> &#8377;<strike>{order.price}</strike></label> */}
            {/* <label style={{marginTop:'4%',position:'absolute',marginLeft:'3%',color:'red'}}><b> Free</b></label> */}
            {(Number(order.status) !== 2 && (Number(order.status) !== 4))?
            <div>
          
            <button type="submit" id="trackbtn"  onClick={()=>this.GetTrack(order.order_id)}><img src={loc} alt="location" id="trackimg"></img> Track Package</button>
            {(order.payusing === "cod")?<button type="submit" id="convertbtn" onClick={()=>ConvertOrder(order.order_id)}> Convert To Prepaid</button>:null}
            <button type="submit" id="cancelbtn" onClick={()=>CancelOrder(order.order_id)}> Cancel Order</button></div>:null
            }
            {/* <button type="submit" id="tnxbtn"> Thanks To Donar</button> */}
           
            {/* <button type="submit" id="feedbackbtn"> Write Delivery Feedback</button> */}
            {/* <select id="select1">
            <option value="" id="op">View More</option>
            <option value="" id="op"></option>
            <option value="" id="op"></option>
            </select> */}
</div>
)}

<Popup

          open={this.state.Order_track_popup}
          // on="hover"
          position="bottom center"
          closeOnDocumentClick
          onClose={this.closeModal}
          contentStyle={{
                        width:'27%',
                        height:'27%',
                        marginTop:"10%",
                        backgroundColor:"rgba(250,220,213,1)"
                        }} 
        >
          <div style={{ color:'blue' , margin:'2%'}} >
            <p style={{ textAlign:'center'}}>{`${this.state.Order_track_popup_status}. For further query, kindly contact to our customer support at `}<span style={{color:'red'}}>support@mypustak.com</span></p>
            
          </div>
        </Popup>

{/* -----------------------------------till tab-------------------------------------- */}
<MediaQuery minWidth={768}>
<Popup
          open={this.state.orderopen}
          closeOnDocumentClick
          onClose={()=>this.setState({orderopen:false})}
          contentStyle={{ 
                        width:'90%',
                        // height:'69vh',
                        borderRadius:'5px',
                        marginLeft:'6%',
                        overflow:'auto',
                        }} 
                        overlayStyle={{ 
                          // background:'#d3d3d3',
                          background:'transparent',
                         }}
        >
        <div >
            <a className="close" onClick={this.ordercloseModal} style={{
    color: 'white',
    marginLeft: '',float:'left',color:'black'}}>
              &times;
            </a>
            {/* <span id="order-id"> Order ID: #<b>{this.state.SelectedOrder.order_id}</b></span> */}
          {(singleOrderDetailsValue.length != 0)?

            <div id="popuporderdiv">
          {singleOrderDetailsValue.map(order=>
            <div id="SelectedOrderBody">
                <div id="OrderBookImgDiv">
                  {/* <p id="order-id"> Order ID: #<b>{this.state.SelectedOrder.order_id}</b></p> */}
                    <img src={`https://d1m4cm33ta71ff.cloudfront.net/uploads/books/medium/${order.thumb}`}  id="OrderBookImg"></img>
                </div>
                <div id="OrderBookData">
                    <p id="OrderTitle">Title:-{order.title}</p>
                </div>
                <div id="orderDelBtn">
                    <p  id="OrderPrice">Shipping Cost: &#8377;{order.new_price}</p>
                </div>
            </div>
            // }
          )}
            </div>:
            <div>
              <p>
              No Details Available for this Book
              </p>

            </div>}
        </div>
        </Popup>       
        </MediaQuery>

        {/* -----------------------------------till large mobiles-------------------------------------- */}
<MediaQuery maxWidth={767} and minWidth={539}>
<Popup
          open={this.state.orderopen}
          closeOnDocumentClick
          onClose={()=>this.setState({orderopen:false})}
          contentStyle={{ 
                        width:'90%',
                        // height:'69vh',
                        borderRadius:'5px',
                        marginLeft:'6%',
                        overflow:'auto',
                        }} 
                        overlayStyle={{ 
                          // background:'#d3d3d3',
                          background:'transparent',
                         }}
        >
        <div >
            <a className="close" onClick={this.ordercloseModal} style={{
    color: 'white',
    marginLeft: '',float:'left',color:'black'}}>
              &times;
            </a>
            {/* <span id="order-id"> Order ID: #<b>{this.state.SelectedOrder.order_id}</b></span> */}
          {(singleOrderDetailsValue.length != 0)?

            <div id="popuporderdiv">
          {singleOrderDetailsValue.map(order=>
            <div id="SelectedOrderBody">
                <div id="OrderBookImgDiv">
                
                  {/* <p id="order-id"> Order ID: #<b>{this.state.SelectedOrder.order_id}</b></p> */}
                    <img src={`https://d1m4cm33ta71ff.cloudfront.net/uploads/books/medium/${order.thumb}`}  id="OrderBookImg"></img>
                </div>
                <div id="OrderBookData">
                    <p id="OrderTitle">Title:-{order.title}</p>
                </div>
                <div id="orderDelBtn">
                    <p  id="OrderPrice">Shipping Cost: &#8377;{order.new_price}</p>
                </div>
            </div>
            // }
          )}
            </div>:
            <div>
              <p>
              No Details Available for this Book
              </p>

            </div>}
        </div>
        </Popup>       
        </MediaQuery>

        {/* -----------------------------------till large mobiles-------------------------------------- */}
<MediaQuery maxWidth={538}>
<Popup
          open={this.state.orderopen}
          closeOnDocumentClick
          onClose={()=>this.setState({orderopen:false})}
          contentStyle={{ 
                        width:'95%',
                        height:'84vw',
                        borderRadius:'5px',
                        marginLeft:'3%',
                        overflow:'auto',
                        }} 
                        overlayStyle={{ 
                          // background:'#d3d3d3',
                          background:'transparent',
                         }}
        >
        <div >
            <a className="close" onClick={this.ordercloseModal} style={{
    color: 'white',
    marginLeft: '',float:'left',color:'black'}}>
              &times;
            </a>
            {/* <span id="order-id"> Order ID: #<b>{this.state.SelectedOrder.order_id}</b></span> */}
          {(singleOrderDetailsValue.length != 0)?

            <div id="popuporderdiv">
          {singleOrderDetailsValue.map(order=>
            <div id="SelectedOrderBody">
                <div id="OrderBookImgDiv">
                  {/* <p id="order-id"> Order ID: #<b>{this.state.SelectedOrder.order_id}</b></p> */}
                    <img src={`https://d1m4cm33ta71ff.cloudfront.net/uploads/books/medium/${order.thumb}`}  id="OrderBookImg"></img>
                </div>
                <div id="OrderBookData">
                    <p id="OrderTitle">Title:-{order.title}</p>
                </div>
                <div id="orderDelBtn">
                    <p  id="OrderPrice">Shipping Cost: &#8377;{order.new_price}</p>
                </div>
            </div>
            // }
          )}
            </div>:
            <div>
              <p>
              No Details Available for this Book
              </p>

            </div>}
        </div>
        </Popup>       
        </MediaQuery>
   <div style={{ textAlign:'center',width:'100%' ,display:'inline'}}>
   <MediaQuery minWidth={539}>
   <ReactPaginate
          // previousLabel={'previous'}
          // nextLabel={'next'}
          // breakLabel={'...'}
          // breakClassName={'break-me'}
          pageCount={300}
          marginPagesDisplayed={1}
          pageRangeDisplayed={9}
          onPageChange={handlePageClick}
          pageClassName={'OrderpaginationLi'}
          previousClassName={'OrderpaginationLi'}
          nextClassName={'OrderpaginationLi'}
          breakClassName={'OrderpaginationLi'}
          // containerClassName={'pagination'}
          // subContainerClassName={'pages pagination'}
          // activeClassName={'active'}
/>
</MediaQuery>
<MediaQuery maxWidth={538}>
   <ReactPaginate
          // previousLabel={'previous'}
          // nextLabel={'next'}
          // breakLabel={'...'}
          // breakClassName={'break-me'}
          pageCount={300}
          marginPagesDisplayed={1}
          pageRangeDisplayed={4}
          onPageChange={handlePageClick}
          pageClassName={'OrderpaginationLi'}
          previousClassName={'OrderpaginationLi'}
          nextClassName={'OrderpaginationLi'}
          breakClassName={'OrderpaginationLi'}
          // containerClassName={'pagination'}
          // subContainerClassName={'pages pagination'}
          // activeClassName={'active'}
/>
</MediaQuery>
</div>

{/* <div id="div1">
    <br/>
    <label id="Order-Placed"> Order Placed :</label>
     <label id="Shipped-To"> Shipped To </label>
      <label id="Placed-by"> Placed By </label>
       <label id="order-total"> Order Total </label>
        <label id="order-id"> Order ID: #<b>519368</b></label>
        <br/>
         <label id="Order-Placed"><b> 26th January,19</b></label>
         <label id="dt"> Mukul Agarwal </label> 
          <label id="dt1"> Mukul Agarwal </label> 
           <label id="dt2">&#8377;293</label> 
            <label id="dt3"> View Order Details </label> 
                <hr id="hr1"/>
            <p style={{marginLeft:'1%'}}><b>Total No Of Books Follows =2</b></p>
            <img src={img} alt="Add" style={{width:'10%',marginTop:'-9%'}}></img>
            <label style={{marginTop:'1%',position:'absolute',width:'20%'}}>THE WINNING WAY:LEARNINGS FROM
SPORT MANAGERS:Learning</label>
<label style={{marginTop:'0.8%',position:'absolute',marginLeft:'30%'}}> Delivery Expected By 5th feb,19</label>
<label style={{marginTop:'4%',position:'absolute'}}> &#8377;<strike>60</strike></label>
<label style={{marginTop:'4%',position:'absolute',marginLeft:'3%',color:'red'}}><b> Free</b></label>
<button type="submit" id="trackbtn"><img src={loc} alt="location" style={{width:'10%',marginTop:'2px'}}></img> Track Package</button>
<button type="submit" id="tnxbtn"> Thanks To Donar</button>
<button type="submit" id="convertbtn"> Convert To Prepaid</button>
<button type="submit" id="feedbackbtn"> Write Delivery Feedback</button>
<select id="select1">
  <option value="" id="op">View More</option>
  <option value="" id="op"></option>
  <option value="" id="op"></option>
</select>
</div> */}

<MainFooter/>
</div>
);
}
}

const mapStateToProps = state => ({
  getorderdetails:state.orderdetailsR.getorderdetails,
  singleOrderDetailsValue:state.orderdetailsR.singleOrderDetails,
  convertAmt:state.orderdetailsR.convertOrderState,
  getuserdetails:state.userdetailsR.getuserdetails,
  userToken:state.accountR.token,
  CancelOrderMessage:state.orderdetailsR.CancelOrderHit,
  ReloadOrders:state.orderdetailsR.ReloadOrders,
  RAZORPAY:state.donationR.rp_id,
  GotConvertResponse:state.orderdetailsR.LoaderConvOrder,

})
export default connect(mapStateToProps,{orderdetails,userdetails,CancelOrder,SetReloadOrder,ConvertOrder,Get_Rp_Id,ViewOrderDetails,Reset_Rp_Id,MakeCancelOrderBlank})(Order);



