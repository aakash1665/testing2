import React, { Component } from 'react'
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import Popup from 'reactjs-popup'
import './resetPassword.css'
import axios from 'axios'
import config from 'react-global-configuration'
import { Link,Redirect,browserHistory } from 'react-router-dom'

class ResetPassword extends Component {
  state={
    ResetPassword:'',
    ConfirmPassword:'',
    PasswordErr:'',
    SuccessReset:false,
  }
  componentDidMount(){
    window.scrollTo(0, 0);

  }
  render() {
    //   alert("Okk")
    const onChange=(e)=>{
      e.preventDefault();
      this.setState({[e.target.name]:e.target.value})

    }
   const onSubmit=(e)=>{
    e.preventDefault();
    if(this.state.ResetPassword !== this.state.ConfirmPassword){
      this.setState({PasswordErr:"Password Do Not Match"})
    }else if(this.state.ResetPassword === this.state.ConfirmPassword && this.state.ResetPassword.length <6){
      this.setState({PasswordErr:"Minimum Password Length must be 6 "})
    }else{
      this.setState({PasswordErr:""})
      DoSubmit()
    }
    // if(this)
    }
    const DoSubmit=()=>{
      var GetTokken=this.props.match.params.token
      var TokenReplace=GetTokken.replace('='," ")
      var TokenCapitalized=TokenReplace.charAt(0).toUpperCase()+TokenReplace.slice(1)
      // alert(TokenCapitalized)
      // const passdata = {
      //   "password" : this.state.ResetPassword
      //    }
        //  http://103.217.220.149:80/core/reset_password/
        //  axios.post(`http://127.0.0.1:8000/core/forgot_password/`,passdata)
       axios.post(`${config.get('apiDomain')}/core/reset_password/`,{"password":this.state.ResetPassword},{headers:{
        'Authorization': TokenCapitalized,
        'content-type':'application/json',
        // 'cache-control':'no-cache',
       }})
     .then(res=>{console.log(res.status);
                // window.location.href="/"  
                SuccessReset();    
                setTimeout(()=>{window.location.href="/"},3000)      
    }
     
     )
     .catch(err=>{console.log(err)}
     )
    }
   const  SuccessReset=()=>{
    this.setState({SuccessReset:true})
    }
    return (
      <div>
          <MainHeader/>
        {/* <h1>RESET Password</h1> */}
        <div id="ResetPasswordBody">
        <div id="ResetPassPart">
        <div id="ResetPasswordLayerOne">
        {/* {this.props.match.params.token.charAt(0).toUpperCase()+this.props.match.params.token.slice(1)} */}
            <p id="">Reset Your Password</p>
        </div>
     
        <form onSubmit={onSubmit}>
            <div id="ResetPassForm">
                <div className="PasswordDiv">
                    <input type="password" placeholder="Enter Your Password" name="ResetPassword" id="RPassword" maxLength="40"
                    onChange={onChange}
                    /> 
                </div>
                <div className="PasswordDiv">
                    <input type="password" placeholder="Confirm Your Password" name="ConfirmPassword" id="ConfirmRPassword"  maxLength="40"
                    onChange={onChange}
                    />
                </div>
                <p style={{ lineHeight:'1px',textAlign:'center'}} >{this.state.PasswordErr}</p>
                
                <input type="submit" value="submit"id="ResetSubmitBtn" />
            </div>
        </form> 
        {/* <div */}
        <Popup
          open={this.state.SuccessReset}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addressopen:false})}
          contentStyle={{ 
                        width:'40%',
                        height:'47%',
                        borderRadius:'5px',
                        // marginLeft:'67%',
                        marginTop:'7%',
                        // background:'transparent'
                        // pa
                        }} 
          overlayStyle={{ 
            // background:'transparent',
           }}
        >
        <p id="PRSuccesMsg">Password Reset Successfully  You are redirected to Home Page.</p>
         </Popup>
        </div>
        </div>
        <div  style={{ marginTop:'80vh' }}>
        <MainFooter/>
        </div>
      </div>
    )
  }
}
export default ResetPassword
