import React, { Component,lazy,Suspense } from 'react';
import './home.css';
import Bgimage from "./Bgimage.js";
import Slideras from './Slider';
// import Slider from './Slider';
// import Footer from "./Footer";
import Books from "./Books";
import MainFooter from '../MainFooter';
// import Slider  from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import MediaQuery from 'react-responsive';
// import MainHeader from '../MainHeader'
// const MainFooter = lazy(() => import('../MainFooter'));
import handleViewport from 'react-in-viewport';

class Home extends Component {
  state = {
    fetched:false
  }
  render() {
   
    return (
      <div>
        {/* Desktop */}
         <MediaQuery  minWidth={992}  > 
       <Bgimage/>
       <Slideras/>
       <Books/>       
    

          {/* </div> */}
          </MediaQuery>
          {/* Tab,Mobile */}
          {/* <MediaQuery  maxWidth={1366}  >  */}
          <MediaQuery  maxWidth={991}  > 

          {/* <MainHeader/> */}
          {/* <Bgimage/> */}
       <Slideras/>
       <Books/>
    
          </MediaQuery>
          {/* <React.Fragment>
            <Suspense fallback={<div>Loading...</div>}>
                  <MainFooter/>
            </Suspense>
          </React.Fragment> */}
          <MainFooter/>
      </div>
    );
  }
}

export default Home;