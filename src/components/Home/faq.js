import React, { Component } from 'react';
import './faq.css';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
// import React from 'react';
import {getfaq} from '../../actions/faqAction'
import { connect } from 'react-redux';
import { Accordion, AccordionItem } from 'react-light-accordion';
import 'react-light-accordion/demo/css/index.css';
import {Helmet} from 'react-helmet'; 
import {BreadcrumbsItem} from 'react-breadcrumbs-dynamic'

class Faq extends Component {
  state={
    category:'',
    clicked:"All",
  }
  ClickedAll  () {
    this.setState({ category: "" })
    }
  ClickedDonatedbooks  () {
    // alert("ok")
    this.setState({ category: "Donate books" })
   }
  Clickedgeneral  () {
    // alert("ok")
    this.setState({ category: "General" })
    }
  Clickedpayments  () {
    // alert("ok")
    this.setState({ category: "Payments" })
      }
  Clickedvolunteer  () {
    // alert("ok")
    this.setState({ category: "Volunteer" })
        }
  Clickeddeliveryoption  () {
    // alert("ok")
    this.setState({ category: "Delivery options" })
          }
componentDidMount(){
  // console.log(getfaq())
  window.scrollTo(0, 0);
  this.props.getfaq()
}  ; 
  render() {
    const {gfaq}=this.props
    var DummyContent ="";
    if(this.state.category === ""){
      try {
        

      DummyContent=  gfaq.map(faqs=> 
        <Accordion atomic={true}>
   {/* if(faqs.category==="Donate books") { */}
        <AccordionItem title={faqs.question}>
         {faqs.answer}
        </AccordionItem>
   
      </Accordion> 
        )
      } catch (error) {
        
      }

    }
    else if(this.state.category==="Donate books") {
      try {
        

      DummyContent=gfaq.map(faqs=> faqs.category !== "Donate books"? null:
        <Accordion atomic={true}>
   {faqs.category==="Donate books" ? 
        (<AccordionItem title={ faqs.question}>
         {faqs.answer}
        </AccordionItem>):null}
   
      </Accordion>  
        )

      } catch (error) {
        
      }
    }
    else if(this.state.category==="General") {
      try {
      
      DummyContent=gfaq.map(faqs=> faqs.category !== "General"? null:
        <Accordion atomic={true}>
   {faqs.category==="General" ? 
        (<AccordionItem title={faqs.question}>
         {faqs.answer}
        </AccordionItem>):null}
   
      </Accordion>  
        )
      } catch (error) {
        
      }
    }
    else if(this.state.category==="Payments") {
      try {
        
      DummyContent=gfaq.map(faqs=> faqs.category !== "Payments"? null:
        <Accordion atomic={true}>
 {faqs.category==="Payments" ? 
       ( <AccordionItem title={faqs.question}>
         {faqs.answer}
        </AccordionItem>):null}
   
      </Accordion>  
        )
      } catch (error) {
        
      }
    }
    else if(this.state.category==="Volunteer") {
      try {
        
      DummyContent=gfaq.map(faqs=> faqs.category !== "Volunteer"? null:
        <Accordion atomic={true}>
  {faqs.category==="Volunteer" ? 
        (<AccordionItem title={faqs.question}>
         {faqs.answer}
        </AccordionItem>):null}
   
      </Accordion>  
        )
      } catch (error) {
        
      }
    }
    else if(this.state.category==="Delivery options"){
      try {
        
      DummyContent=gfaq.map(faqs=> faqs.category !== "Delivery options"? null:
        <Accordion atomic={true}>
    {faqs.category==="Delivery options" ? 
        (<AccordionItem title={faqs.question}>
         {faqs.answer}
        </AccordionItem>):null}
   
      </Accordion>  
        )
      } catch (error) {
        
      }
    }
//   const DummyContent = () => (
//     <p style={{ padding: '18px' }}>
// DQoJCQkJCQkJCQkJCQkJDQoJCQkJCQkJCQkJCQkJDQoJCQkJCQkJCQkJCQkJDQoJCQkJCQkJCQkJCQkJPHAgY2xhc3M9ImFib3V0LWxhYmVsIiBhbGlnbj0iY2VudGVyIiBzdHlsZT0idGV4dC1hbGlnbjogbGVmdDsgbWFyZ2luOiAwY20gMGNtIDMuNDVwdDsgbGluZS1oZWlnaHQ6IDcuNTVwdDsgYmFja2dyb3VuZC1pbWFnZTogaW5pdGlhbDsgYmFja2dyb3VuZC1hdHRhY2htZW50OiBpbml0aWFsOyBiYWNrZ3JvdW5kLXNpemU6IGluaXRpYWw7IGJhY2tncm91bmQtb3JpZ2luOiBpbml0aWFsOyBiYWNrZ3JvdW5kLWNsaXA6IGluaXRpYWw7IGJhY2tncm91bmQtcG9zaXRpb246IGluaXRpYWw7IGJhY2tncm91bmQtcmVwZWF0OiBpbml0aWFsOyI+PHNwYW4gc3R5bGU9ImNvbG9yOiByZ2IoMTE2LCAxMTYsIDExNik7IGxpbmUtaGVpZ2h0OiAyMHB4OyI+PGZvbnQgc2l6ZT0iMyIgZmFjZT0iSGVsdmV0aWNhIj5JdOKAmXMgcXVpdGUgc2ltcGxlIHlvdSBqdXN0IGhhdmUgdG8gbG9naW4gdG8gb3VyIHdlYnNpdGUgYW5kIGZpbGwgdGhlIGluZm9ybWF0aW9uIGZyb20gYW5kIHdlIHdpbGwgZ2V0IGJhY2sgdG8geW91IHdpdGggYWxsIHRoZSBkZXRhaWxzIHJlZ2FyZGluZyBjb2xsZWN0aW9uIG9mIGJvb2tzIGluIDI0IHdvcmtpbmcgaG91cnMuPC9mb250Pjwvc3Bhbj48YnI+PC9wPgkNCgkJCQkJCQkJCQkJCQkJDQoJCQkJCQkJCQkJCQkJCQ0KCQkJCQkJCQkJCQkJCQkNCgkJCQkJCQkJCQkJCQk=
//     </p>
//   );
    return (
      <div>
      <BreadcrumbsItem to='/faq'>Faq</BreadcrumbsItem>

      <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style>
</Helmet>
   <MainHeader/>
   {/* <div style={{backgroundColor:'white',marginTop: '7%',paddingBottom:'2%'}}> */}
   <div id="faqdiv">

      {/* <MainHeader/> */}
          <p className="Frequently-Asked-Questions">Frequently Asked Questions</p>

          <table id="faqtable">
            <tr>
            <th onClick={()=>{this.ClickedAll(); this.setState({clicked:'All'})}}
            id={(this.state.clicked ==='All')?"ShowClicked":""}><input type="button" id="faqbtn" value="All"/></th>
            
            <th onClick={()=>{this.ClickedDonatedbooks(); this.setState({clicked:'Donate Books'})}}
             id={(this.state.clicked ==='Donate Books')?"ShowClicked":""}><button id="faqbtn">Donate Books</button></th>
            
            <th onClick={()=>{this.Clickedgeneral(); this.setState({clicked:'General'})}}
             id={(this.state.clicked ==='General')?"ShowClicked":""}><button id="faqbtn">General</button></th>
           
            <th onClick={()=>{this.Clickedpayments(); this.setState({clicked:'Payments'})}}
            id={(this.state.clicked ==='Payments')?"ShowClicked":""}><button id="faqbtn">Payments</button></th>
           
            <th onClick={()=>{this.Clickedvolunteer(); this.setState({clicked:'Volunteer'})}}
             id={(this.state.clicked ==='Volunteer')?"ShowClicked":""}><button id="faqbtn">Volunteer</button></th>
            
            <th onClick={()=>{this.Clickeddeliveryoption(); this.setState({clicked:'Delivery Options'})}}
             id={(this.state.clicked ==='Delivery Options')?"ShowClicked":""}><button id="faqbtn">Delivery Options</button></th>
            </tr>
            </table>
            <hr id="faqhr"/>    
 {DummyContent}
  {/* {gfaq.map(faqs=>
     <Accordion atomic={true}>
     <AccordionItem title={faqs.question}>
      <p style={{ padding: '18px' }}>
   {faqs.answer} </p>   
     </AccordionItem>

   </Accordion>  
     )}  */}
</div>
          <MainFooter/>
                    </div>
          
        
        
        
    
    );
  }
}
const mapStateToProps = state => ({
  gfaq:state.faqR.gfaq
})
export default connect(mapStateToProps,{getfaq})(Faq);