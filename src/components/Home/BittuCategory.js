import React, { Component } from "react";
import MainHeader from "../MainHeader";
import MainFooter from "../MainFooter";
import GBook from "./GBook";
import axios from "axios";
import "./category.css";
import { Breadcrumbs } from "react-breadcrumbs-dynamic";
import { connect } from "react-redux";
import { Link, Redirect, browserHistory } from "react-router-dom";
import config from "react-global-configuration";
import InfiniteScroll from "react-infinite-scroll-component";
import MediaQuery from "react-responsive";
import Mobilecat from "./mobilecategory";
import ReactGA from "react-ga";
import { Helmet } from "react-helmet";
import { getSeoData } from "../../actions/seodataAction";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";

import GetcategoryID from "./GetCategoryId"

// import DocumentMeta from "react-document-meta";
// import FormControl from "@material-ui/core/FormControl";
// import Radio from "@material-ui/core/Radio";
// import RadioGroup from "@material-ui/core/RadioGroup";
// import FormHelperText from "@material-ui/core/FormHelperText";

// import {Breadcrumbs} from 'react-breadcrumbs-dynamic'

// http://103.217.220.149:80/api/v1/get/category/competitive-exams/engineering/engineering-post-graduate/1/
// http://103.217.220.149:80/api/v1/get/category/competitive-exams/engineering/engineering-post-graduate/1/
// http://103.217.220.149:80/api/v1/get/category/competitive-exams/engineering/engineering-post-graduate/1/
// http://localhost:3000/category/competitive-exams
// http://localhost:3000/category/competitive-exams/
class Category extends Component {
  state = {
    GetBookResult: [],
    start: 1,
    MEWURL: "",
    COUNT: 1,
    ShowCategory: "",
    display: 0,
    menuS: "none",
    showLS: true,
    // minPrice: "",
    // maxPrice: "",
    arrival_date_min: 0,
    arrival_date_max: Math.floor(new Date().getTime() / 1000),
    binding_cover: [],
    book_condition: [],
    language: [],
    latestLast7Days: false,
    latestLast30Days: false,
    latestLast180Days: false,
    bindingPaperback: false,
    bindingHardcover: false,
    conditionBrandNew: false,
    conditionAlmostNew: false,
    conditionVeryGood: false,
    conditionReadable: false,
    LanguageHindi: false,
    LanguageEnglish: false,
    LanguageBengali: false,
    LanguageDutch: false,
    LanguageEnglishFrench: false,
    LanguageFrench: false,
    LanguageGerman: false,
    LanguageGujarati: false,
    LanguageKannada: false,
    LanguageTamil: false,
    LanguageTelugu: false,
    LanguageSanskrit: false,
    LanguageHindiAndEnglish: false,
    LanguageMarathi: false,
    LanguageForeignLanguage: false,

    filterFlag: 0
  };
  url = window.location.href;
  //  urlLength = this.url.length
  CatIndex = this.url.indexOf("/category/");
  newUrl = this.url.slice(this.CatIndex);
  componentDidMount() {
    // alert(
    //   `In componentDidMount and start is ${this.state.start} and newUrl is ${
    //     this.newUrl
    //   }`
    // );
    ReactGA.pageview(window.location.pathname + window.location.search);


    window.scrollTo(0, 0);
    window.addEventListener("scroll", this.handleScroll);
    console.log("in component Did mount upp");
    this.props.getSeoData(
      "https://www.mypustak.com" + window.location.pathname
    );

 
    // Get Category Id
    console.log(`${this.newUrl}`,"Url");
    let CATID=GetcategoryID(this.newUrl)
    console.log(CATID,"GotCAtID")

    this.setState({ ShowCategory: this.newUrl });
    axios
      .get(`http://data.mypustak.com/api/v1/get${this.newUrl}${this.state.start}/`)
      .then(res => {
        console.log(this.state.ShowCategory,"in componenetDidMount And the Data are ", res.data);
        this.setState({ GetBookResult: res.data, FilteredData: res.data,ShowCategory: this.newUrl });
      })
      .catch(err => console.log("The error is ", err));
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.ShowCategory !== nextProps.ClickedCategory && nextProps.ClickedCategory !=="") {
      console.log(
        "in component will receive props ClickedCategory ",
        nextProps.ClickedCategory
      );

      console.log(
        "in component will receive props ShowCategory ",
        this.state.ShowCategory
      );
      const url = window.location.href;
      //  urlLength = this.url.length
      const CatIndex = url.indexOf("/category/");
      const newUrl = url.slice(CatIndex);
      console.log(`${newUrl}`,"Url");
      let CATID=GetcategoryID(newUrl)
      console.log(CATID,"GotCAtID")

      axios
        .get(
          `http://data.mypustak.com/api/v1/get${nextProps.ClickedCategory}${
            this.state.start
          }/`
        )
        .then(res => {
          this.props.getSeoData(
            "https://www.mypustak.com" + window.location.pathname
          );
          console.log("WillRecive Props Before SetState");
          
          this.setState({
            GetBookResult: res.data,
            ShowCategory: nextProps.ClickedCategory,

            // minPrice: "",
            // maxPrice: "",
            arrival_date_min: 0,
            binding_cover: [],
            book_condition: [],
            latestLast7Days: false,
            latestLast30Days: false,
            latestLast180Days: false,
            bindingPaperback: false,
            bindingHardcover: false,
            conditionBrandNew: false,
            conditionAlmostNew: false,
            conditionVeryGood: false,
            conditionReadable: false,
            LanguageHindi: false,
            LanguageEnglish: false,
            LanguageBengali: false,
            LanguageDutch: false,
            LanguageEnglishFrench: false,
            LanguageFrench: false,
            LanguageGerman: false,
            LanguageGujarati: false,
            LanguageKannada: false,
            LanguageTamil: false,
            LanguageTelugu: false,
            LanguageSanskrit: false,
            LanguageHindiAndEnglish: false,
            LanguageMarathi: false,
            LanguageForeignLanguage: false,
            filterFlag: 0
          });
        })
        .catch(err =>
          {console.log("in will err",
            err.response.status,
            `${config.get("apiDomain")}/api/v1/get${
              nextProps.ClickedCategory
            }${this.state.start}/`
          )
        
          if(err.response.status==404){
            axios.get(`${config.get('apiDomain')}/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
            .then(res=>{
              console.log(nextProps,"next_ok",res.data,this.state.ShowCategory)
              this.setState({GetBookResult:res.data,ShowCategory:nextProps.ClickedCategory});
                          this.props.getSeoData(
              "https://www.mypustak.com" + window.location.pathname
            );
                        })
            .catch(err=>{console.log(err,"error in catch");
            }) }
        
        }
        );
      this.setState({ GetBookResult: [], start: 1 });
      window.scrollTo(0, 0);
    

  }
  }

  // onChangeMinPrice = e => {
  //   // alert(`${!isNaN(e.target.value)}`);
  //   if (!isNaN(e.target.value)) {
  //     this.setState({ minPrice: e.target.value });
  //   }
  // };
  // onChangeMaxPrice = e => {
  //   if (!isNaN(e.target.value)) {
  //     this.setState({ maxPrice: e.target.value });
  //   }
  // };

  handleChangeCheckBoxHindi = e => {
    // alert(e.target.checked)
    this.setState({ LanguageHindi: e.target.checked });
    // this.onClickFilter()

  };

  handleChangeCheckBoxEnglish = e => {
    this.setState({ LanguageEnglish: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxBengali = e => {
    this.setState({ LanguageBengali: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxMarathi = e => {
    this.setState({ LanguageMarathi: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxTamil = e => {
    this.setState({ LanguageTamil: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxTelugu = e => {
    this.setState({ LanguageTelugu: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxFrench = e => {
    this.setState({ LanguageFrench: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxDutch = e => {
    this.setState({ LanguageDutch: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxSanskrit = e => {
    this.setState({ LanguageSanskrit: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxGujarati = e => {
    this.setState({ LanguageGujarati: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxGerman = e => {
    this.setState({ LanguageGerman: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxKannada = e => {
    this.setState({ LanguageKannada: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxHindiAndEnglish = e => {
    this.setState({ LanguageHindiAndEnglish: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxEnglishFrench = e => {
    this.setState({ LanguageEnglishFrench: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxForeignLanguage = e => {
    this.setState({ LanguageForeignLanguage: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxLast7Days = e => {
    this.setState({ latestLast7Days: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxLast30Days = e => {
    this.setState({ latestLast30Days: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxLast180Days = e => {
    this.setState({ latestLast180Days: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxPaperback = e => {
    this.setState({ bindingPaperback: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxHardcover = e => {
    this.setState({ bindingHardcover: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxBrandNew = e => {
    this.setState({ conditionBrandNew: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxAlmostNew = e => {
    this.setState({ conditionAlmostNew: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxVeryGood = e => {
    this.setState({ conditionVeryGood: e.target.checked });
    this.onClickFilter()

  };

  handleChangeCheckBoxReadable = e => {
    this.setState({ conditionReadable: e.target.checked });
    this.onClickFilter()

  };

  onClickClearFilter = () => {
    this.setState({
      GetBookResult: this.state.FilteredData,
      // minPrice: "",
      // maxPrice: "",
      arrival_date_min: 0,
      binding_cover: [],
      book_condition: [],
      latestLast7Days: false,
      latestLast30Days: false,
      latestLast180Days: false,
      bindingPaperback: false,
      bindingHardcover: false,
      conditionBrandNew: false,
      conditionAlmostNew: false,
      conditionVeryGood: false,
      conditionReadable: false,
      LanguageHindi: false,
      LanguageEnglish: false,
      LanguageBengali: false,
      LanguageDutch: false,
      LanguageEnglishFrench: false,
      LanguageFrench: false,
      LanguageGerman: false,
      LanguageGujarati: false,
      LanguageKannada: false,
      LanguageTamil: false,
      LanguageTelugu: false,
      LanguageSanskrit: false,
      LanguageHindiAndEnglish: false,
      LanguageMarathi: false,
      LanguageForeignLanguage: false,
      filterFlag: 0,
      start: 1
    });
  };

  onClickFilter = () => {
    let url = window.location.href;
    let data = url.split("/");
    let slug = data[data.length - 2];
    this.setState({ filterFlag: 1, start: 1 });
    let today = new Date();
    let bindingcover = [];
    let bookcondition = [];
    let minustimestamp, finaltimestamp;
    let timestamptoday = Math.floor(today.getTime() / 1000);
    let Language = [];
    if (this.state.latestLast180Days) {
      minustimestamp = 180 * 24 * 3600;
      finaltimestamp = timestamptoday - minustimestamp;

      // this.setState({ arrival_date_min: finaltimestamp });
    } else if (this.state.latestLast30Days) {
      minustimestamp = 30 * 24 * 3600;
      finaltimestamp = timestamptoday - minustimestamp;

      // this.setState({ arrival_date_min: finaltimestamp });
    } else if (this.state.latestLast7Days) {
      minustimestamp = 7 * 24 * 3600;
      finaltimestamp = timestamptoday - minustimestamp;

      // this.setState({ arrival_date_min: finaltimestamp });
    } else {
      finaltimestamp = 0;
      // this.setState({ arrival_date_min: 0 });
    }

    if (this.state.bindingPaperback) {
      bindingcover.push("Paperback");
      // this.setState({ binding_cover: bindingcover });
    }
    if (this.state.bindingHardcover) {
      bindingcover.push("Hardcover");
      // this.setState({ binding_cover: bindingcover });
    }

    if (this.state.conditionBrandNew) {
      bookcondition.push("BrandNew");
      // this.setState({ book_condition: bookcondition });
    }

    if (this.state.conditionAlmostNew) {
      bookcondition.push("AlmostNew");
      // this.setState({ book_condition: bookcondition });
    }

    if (this.state.conditionVeryGood) {
      bookcondition.push("VeryGood");
      // this.setState({ book_condition: bookcondition });
    }

    if (this.state.conditionReadable) {
      bookcondition.push("AverageButInReadableCondition");
      // this.setState({ book_condition: bookcondition });
    }

    if (this.state.LanguageHindi) {
      Language.push("Hindi");
    }

    if (this.state.LanguageEnglish) {
      Language.push("English");
    }

    if (this.state.LanguageTamil) {
      Language.push("Tamil");
    }

    if (this.state.LanguageTelugu) {
      Language.push("Telugu");
    }

    if (this.state.LanguageSanskrit) {
      Language.push("Sanskrit");
    }

    if (this.state.LanguageMarathi) {
      Language.push("Marathi");
    }

    if (this.state.LanguageGujarati) {
      Language.push("Gujarati");
    }

    if (this.state.LanguageKannada) {
      Language.push("Kannada");
    }

    if (this.state.LanguageFrench) {
      Language.push("French");
    }

    if (this.state.LanguageDutch) {
      Language.push("Dutch");
    }

    if (this.state.LanguageGerman) {
      Language.push("German");
    }

    if (this.state.LanguageHindiAndEnglish) {
      Language.push("Hindi & English");
    }

    if (this.state.LanguageBengali) {
      Language.push("Bengali");
    }

    if (this.state.LanguageEnglishFrench) {
      Language.push("English(French)");
    }

    if (this.state.LanguageForeignLanguage) {
      Language.push("Foreign Language");
    }

    this.setState({
      book_condition: bookcondition,
      binding_cover: bindingcover,
      arrival_date_min: finaltimestamp,
      language: Language
    });

    let body = {
      // minPrice: this.state.minPrice,
      // maxPrice: this.state.maxPrice,
      binding: bindingcover,
      condition: bookcondition,
      date_min: finaltimestamp,
      date_max: timestamptoday,
      language: Language
    };

    console.log("Body ", body);
    // http://localhost:8000/api/v1/filter/price/others/1
    axios
      .post(`${config.get('apiDomain')}/api/v1/filter/price/${slug}/1`, body)
      .then(res => {
        console.log("in onClickFilter And the Data are ", res.data.Books);
        this.setState({ GetBookResult: res.data.Books });
      })
      .catch(err => console.log("err", err));
  };

  //   api/v1/get/category/ competitive-exams/engineering/iit/<int:pg>/
  // /api/v1/get/category/ competitive-exams/enginnering/iit/1/


// --------------------------Filter Part-------------------------------------------
// let Language = [];
// let latestLast30Days=""
// let latestLast7Days=""
// let latestLast180Days=""
// let bindingPaperback=""
// let bindingHardcover=""
// let conditionBrandNew=""
// let conditionAlmostNew=""
// let conditionVeryGood=""
// let conditionReadable=""
// let LanguageHindi=""
// let LanguageEnglish=""
// let LanguageTamil=""
// let LanguageTelugu=""
// let LanguageSanskrit=""
// let LanguageMarathi=""
// let LanguageGujarati=""
// let LanguageKannada=""
// let LanguageFrench=""
// let LanguageHindiAndEnglish=""
// let LanguageGerman=""
// let LanguageDutch=""
// let LanguageBengali=""
// let LanguageEnglishFrench=""
// let LanguageForeignLanguage=""


//  handleChangeCheckBoxHindi = e => {
//   // alert(e.target.checked)
  
//    LanguageHindi=e.target.checked ;
//   onClickFilter()
//   console.log(Language,"HindiLang");


// };

//  handleChangeCheckBoxEnglish = e => {

//    LanguageEnglish= e.target.checked ;
//   onClickFilter()
//   console.log(Language,"EngLang");

// };

//  handleChangeCheckBoxBengali = e => {
//    LanguageBengali= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxMarathi = e => {
//    LanguageMarathi= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxTamil = e => {
//    LanguageTamil= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxTelugu = e => {
//    LanguageTelugu= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxFrench = e => {
//    LanguageFrench= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxDutch = e => {
//    LanguageDutch= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxSanskrit = e => {
//    LanguageSanskrit= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxGujarati = e => {
//    LanguageGujarati= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxGerman = e => {
//    LanguageGerman= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxKannada = e => {
//    LanguageKannada= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxHindiAndEnglish = e => {
//    LanguageHindiAndEnglish= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxEnglishFrench = e => {
//    LanguageEnglishFrench= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxForeignLanguage = e => {
//    LanguageForeignLanguage= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxLast7Days = e => {
//    latestLast7Days= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxLast30Days = e => {
//    latestLast30Days= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxLast180Days = e => {
//    latestLast180Days= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxPaperback = e => {
//    bindingPaperback= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxHardcover = e => {
//    bindingHardcover= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxBrandNew = e => {
//    conditionBrandNew= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxAlmostNew = e => {
//    conditionAlmostNew= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxVeryGood = e => {
//    conditionVeryGood= e.target.checked ;
//   onClickFilter()

// };

//  handleChangeCheckBoxReadable = e => {
//    conditionReadable= e.target.checked ;
//   onClickFilter()

// };


//  onClickFilter = () => {
//   console.log(Language,"Language",LanguageHindi,"oo",LanguageEnglish);
  
//   let url = window.location.href;
//   let data = url.split("/");
//   let slug = data[data.length - 2];
//   this.setState({ filterFlag: 1, start: 1 });
//   let today = new Date();
//   let bindingcover = [];
//   let bookcondition = [];
//   let minustimestamp, finaltimestamp;
//   let timestamptoday = Math.floor(today.getTime() / 1000);
//   // let Language = [];
//   if (latestLast180Days) {
//     minustimestamp = 180 * 24 * 3600;
//     finaltimestamp = timestamptoday - minustimestamp;

//     // this.setState({ arrival_date_min: finaltimestamp });
//   } else if (latestLast30Days) {
//     minustimestamp = 30 * 24 * 3600;
//     finaltimestamp = timestamptoday - minustimestamp;

//     // this.setState({ arrival_date_min: finaltimestamp });
//   } else if (latestLast7Days) {
//     minustimestamp = 7 * 24 * 3600;
//     finaltimestamp = timestamptoday - minustimestamp;

//     // this.setState({ arrival_date_min: finaltimestamp });
//   } else {
//     finaltimestamp = 0;
//     // this.setState({ arrival_date_min: 0 });
//   }

//   if (bindingPaperback) {
//     bindingcover.push("Paperback");
//     // this.setState({ binding_cover: bindingcover });
//   }
//   if (bindingHardcover) {
//     bindingcover.push("Hardcover");
//     // this.setState({ binding_cover: bindingcover });
//   }

//   if (conditionBrandNew) {
//     bookcondition.push("BrandNew");
//     // this.setState({ book_condition: bookcondition });
//   }

//   if (conditionAlmostNew) {
//     bookcondition.push("AlmostNew");
//     // this.setState({ book_condition: bookcondition });
//   }

//   if (conditionVeryGood) {
//     bookcondition.push("VeryGood");
//     // this.setState({ book_condition: bookcondition });
//   }

//   if (conditionReadable) {
//     bookcondition.push("AverageButInReadableCondition");
//     // this.setState({ book_condition: bookcondition });
//   }

//   if (LanguageHindi) {
//     Language.push("Hindi");
//   }

//   if (LanguageEnglish) {
//     Language.push("English");
//   }

//   if (LanguageTamil) {
//     Language.push("Tamil");
//   }

//   if (LanguageTelugu) {
//     Language.push("Telugu");
//   }

//   if (LanguageSanskrit) {
//     Language.push("Sanskrit");
//   }

//   if (LanguageMarathi) {
//     Language.push("Marathi");
//   }

//   if (LanguageGujarati) {
//     Language.push("Gujarati");
//   }

//   if (LanguageKannada) {
//     Language.push("Kannada");
//   }

//   if (LanguageFrench) {
//     Language.push("French");
//   }

//   if (LanguageDutch) {
//     Language.push("Dutch");
//   }

//   if (LanguageGerman) {
//     Language.push("German");
//   }

//   if (LanguageHindiAndEnglish) {
//     Language.push("Hindi & English");
//   }

//   if (LanguageBengali) {
//     Language.push("Bengali");
//   }

//   if (LanguageEnglishFrench) {
//     Language.push("English(French)");
//   }

//   if (LanguageForeignLanguage) {
//     Language.push("Foreign Language");
//   }

//   // this.setState({
//   //   book_condition: bookcondition,
//   //   binding_cover: bindingcover,
//   //   arrival_date_min: finaltimestamp,
//   //   language: Language
//   // });
//   console.log(Language,"lang");
  

//   let body = {
//     // minPrice: this.state.minPrice,
//     // maxPrice: this.state.maxPrice,
//     binding: bindingcover,
//     condition: bookcondition,
//     date_min: finaltimestamp,
//     date_max: timestamptoday,
//     language: Language
//   };

//   console.log("Body ", body);
//   // http://localhost:8000/api/v1/filter/price/others/1
//   axios
//     .post(`${config.get('apiDomain')}/api/v1/filter/price/${slug}/1`, body)
//     .then(res => {
//       console.log("in onClickFilter And the Data are ", res.data.Books,Language);
//       this.setState({ GetBookResult: res.data.Books });
//     })
//     .catch(err => console.log("err", err));
//   console.log(Language,"langEnd");

// };

// --------------------------End Filter Part ----------------------------



  render() {
     const seoData = this.props.SeoData;

    let meta = {
      title: seoData.title_tag,
      description: seoData.title_tag,
      meta: {
        charset: "utf-8",
        name: {
          title: seoData.title_tag,
          description: seoData.title_tag
        }
      }
    };

    const fetchBooks = () => {
      console.log(this.newUrl,"FetchBooks");
      this.setState({ start: this.state.start + 1 });

      console.log(this.state.start);

      if (this.state.filterFlag == 1) {
        let url = window.location.href;
        let data = url.split("/");
        let slug = data[data.length - 2];
        let body = {
          // minPrice: this.state.minPrice,
          // maxPrice: this.state.maxPrice,
          binding: this.state.binding_cover,
          condition: this.state.book_condition,
          date_min: this.state.arrival_date_min,
          date_max: this.state.arrival_date_max,
          language: this.state.language
        };

        axios
          .post(
            `${config.get('apiDomain')}/api/v1/filter/price/${slug}/${
              this.state.start
            }`,
            body
          )
          .then(res => {
            console.log("in onClickFilter And the Data are ", res.data.Books,`${this.state.start}`);
            this.setState({
              GetBookResult: this.state.GetBookResult.concat(res.data.Books),
              filterFlag: 1
            });
          })
          .catch(err =>
            console.log("No more data are present on these filters")
          );
      }

      // axios.get(`http://103.217.220.149:80/api/v1/get/get-books/${this.state.start}/`)
      // axios.get(`${config.get('apiDomain')}/api/v1/get/${this.newUrl}/${this.state.start}/`)
      else {

        // axios.get(`${config.get('apiDomain')}/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
        // .then(res=> {this.setState({GetBookResult:this.state.GetBookResult.concat(res.data)})})
        // .catch(err=>console.log(err)
        // )
        
        axios
          .get(
            `${config.get("apiDomain")}/api/v1/get${this.newUrl}${
              this.state.start
            }/`
          )
          .then(res => {
            this.setState({
              GetBookResult: this.state.GetBookResult.concat(res.data),
              filterFlag: 0
            });
          })
          .catch(err => console.log(err,"e",this.state.ShowCategory,"errorInFetch", `${config.get("apiDomain")}/api/v1/get${this.state.ShowCategory}${
            this.state.start
          }/`));
        // alert(`${config.get('apiDomain')}/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
      }
    };
    console.log(this.state.ShowCategory,"GetStateCAt");
    
    let CategoryUrl= this.state.ShowCategory
    let ReplacedCategoryUrl= CategoryUrl.replace( /[/]/g, "," );
    let ResultUrl=ReplacedCategoryUrl.split(',')
    // ResultUrl=ResultUrl.pop();
    console.log(ResultUrl,"ResultUrl",ResultUrl.length);
    let ListOfLinks=[]
          ResultUrl.forEach((element,index) => {
            console.log(element,index);
            if(element!=="category" && index!==1  && index!==5 ){
              if(index == 2 ){
                ListOfLinks.push(<span><a href={`/category/${element}/`} style={(this.state.ChangeBreadCrumLink1)?{
                  color:'#2874f0',
                  marginRight:'1%',
                  textDecoration:'None'
                }:
              {
                color:'grey',
                marginRight:'1%',
                textDecoration:'None'
              }
              }
                onMouseEnter={()=>this.setState({ChangeBreadCrumLink1:!this.state.ChangeBreadCrumLink1})}
                onMouseLeave={()=>this.setState({ChangeBreadCrumLink1:!this.state.ChangeBreadCrumLink1})}
                
                >{element} </a></span>)
              }else{
                if(index == 3 && ResultUrl.length > 4){
                    console.log(this.state.ShowCategory.lastIndexOf(element),33,this.state.ShowCategory.length,);
                    var links = this.state.ShowCategory.split('/')
                    var onclickLinkThird=""
                    onclickLinkThird.concat(`${links[3]}`)
                    console.log(onclickLinkThird,"Thi",links[3]);
                    ListOfLinks.push(<span>> <a href={`/category/${links[2]}/${links[3]}/`} style={(this.state.ChangeBreadCrumLink2)?{
                      color:'#2874f0',
                      marginRight:'1%',
                      textDecoration:'None'
                    }:
                  {
                    color:'grey',
                    marginRight:'1%',
                    textDecoration:'None'
                  }
                  }
                    onMouseEnter={()=>this.setState({ChangeBreadCrumLink2:!this.state.ChangeBreadCrumLink2})}
                    onMouseLeave={()=>this.setState({ChangeBreadCrumLink2:!this.state.ChangeBreadCrumLink2})}
                  >{element}</a></span>)
                }else{
                
                }
                if(index == 4 && ResultUrl.length > 5){
                    ListOfLinks.push(<span>> <a href={this.state.ShowCategory}  style={(this.state.ChangeBreadCrumLink3)?{
                      color:'#2874f0',
                      marginRight:'1%',
                      textDecoration:'None'
                    }:
                  {
                    color:'grey',
                    marginRight:'1%',
                    textDecoration:'None'
                  }
                  }
                    onMouseEnter={()=>this.setState({ChangeBreadCrumLink3:!this.state.ChangeBreadCrumLink3})}
                    onMouseLeave={()=>this.setState({ChangeBreadCrumLink3:!this.state.ChangeBreadCrumLink3})}
                    
                    >{element} </a></span>)
                }

              }

          }
          });
      // let ListOfLinks=[]
    const handlePriceChange = ( newValue) => {
      // setValue(newValue);
      console.log(newValue,"new Value");
    };
    function valuetext(value) {
      return `${value}°C`;
    }
     console.log(this.state.GetBookResult,"Result");

    return (
      <div>
        {/* {alert("Hello world")} */}
        <MainHeader />
        <MediaQuery maxWidth={991}>
          <Mobilecat />
        </MediaQuery>
        {/* <DocumentMeta {...meta} /> */}
        <Helmet  bodyAttributes={{style: 'background-color : aliceblue'}}>
          <title>{seoData.title_tag}</title>
          <meta
            name="Description"
            property="og:description"
            content={seoData.meta_desc}
          />
          <meta
            name="og_title"
            property="og:title"
            content={seoData.title_tag}
          />
          <meta
            name="og_url"
            property="og:url"
            content={window.location.href}
          />
        </Helmet>
        <MediaQuery minWidth={992}>
          <div
            style={{
              height: '99vh',
              width: '21%',
              marginTop: 8,
              float: "left",
              backgroundColor: "white",
              padding: 10,
              // position: "sticky",
              top: 0,
              // overflow: "scroll"
              // border:'4px solid aliceblue'
            }}
          >
            <h4 style={{ color: "dodgerblue", textAlign: "center" }}>Filters</h4>

            <form noValidate>
             
              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                LATEST ARRIVALS
              </FormLabel>

              <div style={{ }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      value="checkedA"
                      // checked={this.state.latestLast7Days}
                      onChange={this.handleChangeCheckBoxLast7Days}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Last 7 Days"
                  style={{
                    height: 20
                  }}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.latestLast30Days}
                      onChange={this.handleChangeCheckBoxLast30Days}
                    />
                  }
                  label="Last 30 Days"
                  style={{
                    height: 20
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.latestLast180Days}
                      onChange={this.handleChangeCheckBoxLast180Days}
                    />
                  }
                  label="Last 180 Days"
                  style={{
                    height: 20
                  }}
                />
              </div>

         

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                BINDING COVER
              </FormLabel>
              <div style={{ display:'flex' }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      value="checkedA"
                      // checked={this.state.bindingPaperback}
                      onChange={this.handleChangeCheckBoxPaperback}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Paperback"
                  style={{
                    height: 20,
                  }}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.bindingHardcover}
                      onChange={this.handleChangeCheckBoxHardcover}
                    />
                  }
                  label="Hardcover"
                  style={{
                    height: 20
                  }}
                />
              </div>

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                BOOK CONDITION
              </FormLabel>
              <div style={{ display:'flex'  }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.conditionBrandNew}
                      onChange={this.handleChangeCheckBoxBrandNew}
                    />
                  }
                  label="Brand New"
                  style={{
                    height: 20
                    
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.conditionAlmostNew}
                      onChange={this.handleChangeCheckBoxAlmostNew}
                    />
                  }
                  label="Almost New"
                  style={{
                    height: 20
                  }}
                />
                </div>
              <div style={{ display:'flex' }}>

                <FormControlLabel
                  control={
                    <Checkbox
                      value="checkedA"
                      // checked={this.state.conditionVeryGood}
                      onChange={this.handleChangeCheckBoxVeryGood}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Very Good"
                  style={{
                    height: 20
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.conditionReadable}
                      onChange={this.handleChangeCheckBoxReadable}
                    />
                  }
                  label="Readable"
                  style={{
                    height: 20
                  }}
                />
              </div>

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                LANGUAGES
              </FormLabel>
              <div style={{ marginTop: 25,display:'flex',flexFlow:'row wrap', }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.state.LanguageHindi}
                      // checked={this.state.LanguageHindi}
                      onChange={this.handleChangeCheckBoxHindi}
                    />
                  }
                  label="Hindi"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageEnglish}
                      onChange={this.handleChangeCheckBoxEnglish}
                    />
                  }
                  label="English"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value="checkedA"
                      // checked={this.state.LanguageMarathi}
                      onChange={this.handleChangeCheckBoxMarathi}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Marathi"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageEnglishFrench}
                      onChange={this.handleChangeCheckBoxEnglishFrench}
                    />
                  }
                  label="English(French)"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageBengali}
                      onChange={this.handleChangeCheckBoxBengali}
                    />
                  }
                  label="Bengali"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageSanskrit}
                      onChange={this.handleChangeCheckBoxSanskrit}
                    />
                  }
                  label="Sanskrit"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value="checkedA"
                      // checked={this.state.LanguageDutch}
                      onChange={this.handleChangeCheckBoxDutch}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Dutch"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageGujarati}
                      onChange={this.handleChangeCheckBoxGujarati}
                    />
                  }
                  label="Gujarati"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageTelugu}
                      onChange={this.handleChangeCheckBoxTelugu}
                    />
                  }
                  label="Telugu"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageGerman}
                      onChange={this.handleChangeCheckBoxGerman}
                    />
                  }
                  label="German"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value="checkedA"
                      // checked={this.state.LanguageKannada}
                      onChange={this.handleChangeCheckBoxKannada}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Kannada"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageFrench}
                      onChange={this.handleChangeCheckBoxFrench}
                    />
                  }
                  label="French"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageTamil}
                      onChange={this.handleChangeCheckBoxTamil}
                    />
                  }
                  label="Tamil"
                  style={{
                    height: 20,
                    width:'45%'
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value="checkedB"
                      // checked={this.state.LanguageHindiAndEnglish}
                      onChange={this.handleChangeCheckBoxHindiAndEnglish}
                    />
                  }
                  label="Hindi & English"
                  style={{
                    height: 20,
                   
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value="checkedA"
                      // checked={this.state.LanguageForeignLanguage}
                      onChange={this.handleChangeCheckBoxForeignLanguage}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Foreign Language"
                  style={{
                    height: 20,
                    // width:'45%'
                  }}
                />
              </div>

              <div>
                <Button
                  variant="contained"
                  color="primary"
                  margin="normal"
                  style={{
                    marginLeft: "theme.spacing(1)",
                    marginRight: "theme.spacing(1)",
                    width: 80,
                    display: "flex",
                    flexWrap: "wrap",
                    margintop: "theme.spacing(1)",
                    float: "left"
                  }}
                  onClick={this.onClickFilter}
                >
                  Apply
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  margin="normal"
                  style={{
                    marginLeft: "theme.spacing(1)",
                    marginRight: "theme.spacing(1)",
                    width: 80,
                    display: "flex",
                    flexWrap: "wrap",
                    margintop: "theme.spacing(1)",
                    float: "right"
                  }}
                  onClick={this.onClickClearFilter}
                >
                  Clear
                </Button>
              </div>
            </form>
          </div>
        </MediaQuery>

        <div>
          {console.log(Link)}
          <Breadcrumbs
            separator={<b> / </b>}
            item={Link}
            finalItem={"b"}
            finalProps={{
              style: { color: "red" }
            }}
          />
        </div>
        {/* Desktop */}
        <MediaQuery minWidth={992}>
          <div id="categoryBody">
          <div id="BreadCrumb" style={{ marginLeft:'2%' }}>
          {ListOfLinks}

          </div>

            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
        </MediaQuery>
        {/* Tab */}
        <MediaQuery minWidth={768} maxWidth={991}>
          <div id="categoryBody">
            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (
                  //  console.log(bookdata);

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
        </MediaQuery>
        <MediaQuery maxWidth={767} minWidth={539}>
          <div id="categoryBody">
            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (
                  //  console.log(bookdata);

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
        </MediaQuery>
        <MediaQuery maxWidth={538}>
          <div id="categoryBody">
            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (
                  //  console.log(bookdata);

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
                 ((bookdata.document.is_out_of_stack !== `Y`)?
                <SBook key={bookdata.document.title} 
                    book={bookdata.document}
                    />:null  
                 ))
                 } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
        </MediaQuery>
        <MainFooter />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ClickedCategory: state.homeR.ClickedCategory,
  SeoData: state.seodata.seodata
});
// export default  Category
export default connect(
  mapStateToProps,
  { getSeoData }
)(Category);
