import React, { Component } from 'react';
// import Header from './Header';
import Popup from 'reactjs-popup' 
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from 'react-responsive-carousel';
import axios from 'axios'
import { Link,Redirect,browserHistory } from 'react-router-dom'
import {doSearch,makeSearchBlank,setCategory} from '../../actions/homeAction'
import {logout,ActivteSuccesPopup } from '../../actions/accountAction'
import {AddToCart,CartopenModal,CartcloseModal,removeAllCart,RemoveCart,ToBeAddedToCart,RemoveToBeAddedToCart,MobileCartRedirect} from '../../actions/cartAction'
import { connect } from 'react-redux';
import PUserLogin from './PUserLogin'
import PUserSignup from './PUserSignup';
// import del from '../product/Product_Images/del.png';
import Popupcat from './Popupcategories'
import MediaQuery from 'react-responsive';
import homeicon from '../mobimages/homeicon.png'
import searchicon from '../mobimages/searchicon.png'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
class Slideras extends Component {

  state={
    display:0,
    menuS:'none',
    name:'',
    showLS:true,
    Sopen:false,
    RedirectCart:false,
    userBookQty:1,
    cartMsg:null,
    goTocart:false,
    ASignupopen:false,
    Cat:false,
    CloseNav:false,
    GotToken:null,
  }
  changeShowLS=()=>{
    this.setState({showLS:!this.state.showLS})
  }
  ToggleASignupOpenModal=()=>{
    // alert("onnnn")
    this.setState({ ASignupopen: !this.state.ASignupopen })
  }
  toDonation()
  {
    // alert("okk")
    window.location.href = `/donate`
  }
  OPENCategory=()=>{
    this.setState({Cat:true})
  }
  CLOSECategory=()=>{
    this.setState({Cat:false})
  }
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
    this.listAllItems()
        // this.closeNav()
    // this.openNav()
    if(localStorage.getItem('user') === null){
      // alert("RED")
      this.setState({GotToken:null})
    }else{
      this.setState({GotToken:localStorage.getItem('user')})

    }
    sessionStorage.removeItem("UserOrderId")
  }
  openNav=()=>{
    // alert("OpenNavs")
    this.setState({CloseNav:!this.state.CloseNav})
    // document.getElementById("mySidenav").style.display = "block";
  }

 closeNav=()=>{
  //  alert("change")
    // document.getElementById("mySidenav").style.display = "none";
    this.setState({CloseNav:!this.state.CloseNav})
  }
   listAllItems=()=>{  
      // alert("in see")
      if(sessionStorage.length !==0){
        this.props.removeAllCart()
     for (var i=0; i<=sessionStorage.length-1; i++)  
     {   
      if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){

         let key = sessionStorage.key(i); 
         try {
          let val = JSON.parse(sessionStorage.getItem(key));
          // console.log(val.bookInvId  )
          if(localStorage.getItem('user') === null){
            this.props.RemoveToBeAddedToCart()
            this.props.ToBeAddedToCart({"book_id" :val.bookId ,"book_inv_id":val.bookInvId[0] })
  
            }
            else{
              this.props.RemoveToBeAddedToCart()
            }
          // val.map(det=>console.log(det))
          this.props.AddToCart(val)
          // alert(val)
         } catch (error) {
          //  console.log(error);
           
         } 

      }
     } }
   
   }
   RemoveFromCart=(bookInvId,Cart_id)=>{
// alert("Remove",Cart_id)
    for (var i=0; i<=sessionStorage.length-1; i++)  
    {   
      // alert("rmo")
        let key = sessionStorage.key(i);  
        try {
            if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){

              let val = JSON.parse(sessionStorage.getItem(key));
              if(`${val.bookInvId}` === `${bookInvId}`)  {
                // alert("rm")
                sessionStorage.removeItem(bookInvId);
              }
            }
        } catch (error) {
          // console.log(error);          
        }

        // val.map(det=>console.log(det))
        // this.props.AddToCart(val)
        // alert(val)
    }
    if(localStorage.getItem('user') === null){

      this.props.removeFromCartLogout(bookInvId)
      }else{
      const data={"is_deleted":"Y"}

      this.props.RemoveCart(Cart_id,bookInvId,data)
  
      }
   }
  sopenModal=()=>{
    this.setState({ Sopen:!this.state.Sopen })
  }
  scloseModal=()=>{
    // this.setState({ sopen: false })
}
  
  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
}
handleScroll=()=>{
  const scroll = window.scrollY
  this.setState({display:scroll});
  // console.log(this.state.display);
  if(this.state.display > 170){
    this.setState({menuS:'block'});
  }else{
    this.setState({menuS:'none'});
  }
  // this.setState({display:Scroll})
}
DoSearch=(searchValue)=>{
  // axios.get(`http://127.0.0.1:8000/search/${searchValue}/`)
  // .then(res=>console.log(res.data))
  // .catch(err=>console.log(err)
  // )
}

getName(){
  // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
  // axios.get(`http://103.217.220.149:80/core/hello/ `,{headers:{
  //   'Authorization': `Token ${this.props.userToken}`
  // }})
  // .then(res=>this.setState({name:res.data.name}))
  // .catch(err=>console.log(err)
  // )
  
}
Userlogout=()=>{
  this.props.logout()
  localStorage.removeItem('user');
  // this.sopenModal()
  this.setState({Sopen:false})
  window.location.href = ''
  // this.setState({Sopen:false})
  return <Redirect to =''></Redirect>
}
successLoginStickyPop=()=>{
  this.setState({ succesLoginPop: false })
}
onSearchChange = e => {
this.props.doSearch(e.target.value)
this.DoSearch(e.target.value)
// if(this.props.searchBook !== ''){
//   alert("redirect")
// return  <Redirect to='/home/search'/>;
// }
}
// LOG_IN=<PUserLogin scloseModal={this.scloseModal} changeShowLS={this.changeShowLS} />
// SIGN_IN=<PUserSignup scloseModal={this.scloseModal} changeShowLS={this.changeShowLS}/>
LOG_IN=<PUserLogin scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS} ShowMsg={this.props.ErrMsg}/>
    SIGN_IN=<PUserSignup scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS}/>

    render() {
      const SetCategory=(category)=>{
        // alert(category);
        this.props.setCategory(category);
        // this.props.CloseCatPopup()
      }
      const CheckLogin=()=>{
        if(this.props.userToken === null){
        //  this.sopenModal()
        this.ToggleASignupOpenModal()
         this.setState({goTocart:true})
         // if(this.props.userToken !== null){
         //   this.setState({RedirectCart:true})
         // }
       }else{
         this.setState({RedirectCart:true})
       }
       }
       const MobileCheckLogin=()=>{
        if(this.props.userToken === null ){
          this.props.MobileCartRedirect()
          // alert("re")
        // return <Redirect to="/login"/>
       }
       else{
        // return <Redirect to="/view-cart"/>
         this.setState({RedirectCart:true})
       }
       }
       if(this.props.cartRedirectMobile){
         
        return <Redirect to="/login"/>

       }
       if(this.state.goTocart === true && this.props.userToken === null){
        if(window.innerWidth > 0){
          if(window.innerWidth < 991){
        // return  <Redirect to='/view-cart'/>;
          }
        }
      }
      if(this.state.goTocart === true && this.props.userToken !== null){
        return  <Redirect to='/view-cart'/>;
      }
      if(this.props.searchBook.length !== 0){
        // alert("redirect")
      return  <Redirect to='/search'/>;
      }
      if(this.props.userToken !== '')
      {this.getName()
      }
      if(this.state.RedirectCart){
        // alert("redirect")
      return  <Redirect to='/view-cart'/>;
      }    const {cartDetails} = this.props
      var TotalShipCost =0
      cartDetails.map(cart=> TotalShipCost+=Number(cart.bookShippingCost) )
      const settings1 = {
        className: "center",
        infinite: true,
        // centerPadding: "60px",
        slidesToShow: 3,
        swipeToSlide: true,
      arrows:false,
        afterChange: function(index) {

          // console.log(
          //   `Slider Changed to: ${index + 1}, background: #222; color: #bada55`
          // );
        }
      }
      const settings2 = {
        className: "center",
        infinite: true,
        centerPadding: "60px",
        slidesToShow: 2,
        swipeToSlide: true,
      arrows:false,
        afterChange: function(index) {

          // console.log(
          //   `Slider Changed to: ${index + 1}, background: #222; color: #bada55`
          // );
        }
      }
        return (
          <div>
            {/* tab           */}
    <MediaQuery maxWidth={991} and minWidth={768}>

<div>
<div id="modiv">
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
{/* <div id="mySidenav" className="sidenav"> */}
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>
{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}

{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>
<Link style={{ textDecoration: 'none' }} to="/"> 

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>

</Link>
<span id="cart">
<span>{(this.props.ItemsInCart === 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
</span> 

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} id="cartimg" onClick={()=>MobileCheckLogin()}/>

</div>
</div>
{/*#################### Tab StickyTop SEARCH BOX ############################ */}
<div id="homeMenusearch" >
       {/* <input className="search-txt" type="search" 
       name='searchBook' value={this.props.searchBook}
       onChange={this.onSearchChange} ref="searchBookFixed"
         style={{ position:'absolute',width:'88%',marginTop:'0.4%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'6%'}}
         placeholder="Search Books, Author, Publisher, Title, ISBN #"/> */}

       {/* <a id="homeMenuSearchBtn" href="#">
       <i className="fas fa-search"
         style={{ position:'absolute',marginTop:'0%' }}
         />
       </a>   */}
       <Link style={{ textDecoration: 'none' }} to="/"> 
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mypustaknewlogo.png`} style={{width:'7vw',height:'7vw'}} onClick={this.props.makeSearchBlank}/>
 </Link>

                  
                  <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                  style={{width:'3vw'}} />
                  <span id="DonateBooksMH">Donate Book</span></button></Link>
                    <input className="search-txt" type="search" 
                    name='searchBook' value={this.props.searchBook}
                    onChange={this.onSearchChange} ref="searchBookFixed"
                      style={{ position:'absolute',width:'73%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'18%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"
                      />

                    <a  href="#">
                    
                    <i className="fas fa-search"
                      style={{ position:'absolute',marginTop:'2.5%',marginLeft:'86%' }}
                      />
                    </a> 
     </div>

            <div className="rectangle" id="SliderImages">
            <div className="column1"> 
            <Link to="search?value=ncert upsc">
            <div>
            <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mobilencert.229b7080.jpg`} style={{ float:'left' }}/>
            </div>
            </Link>
                  {/* <div className="textbox"></div> */}
               </div>
            <div className="column2"> 
              <Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay={true} infiniteLoop={true}>

                <Link to="/donate-books">
                <div>
                <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/donate-books.jpg`}/>
                </div>
                </Link>
                <Link to="/product/Ten-Best-Novels-To-Read">
                <div>
                 <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/MostLovedBooks10.jpeg`} />
                 </div>
                 </Link>
              <Link to="search?value=study material">
              <div>
               <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/Study+material+book+banner+final.jpg`}/>
               </div>
               </Link>
              </Carousel >
               </div>
               </div>
                {/* <Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay={true} infiniteLoop={true}> */}
 
             <Slider {...settings1}>

<div>
<Link to='/product/Ten-Best-Novels-To-Read'>
<div style={{ height: '11vw',paddingTop:'3%',fontSize:'27px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af'}}>
<p style={{fontSize:'2vw',marginTop:'3%'}}>
<div>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HomepageMostLovedBookBox.png`}
style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto'}}/>
</div>
<span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'11%'}}> Most Loved Book <br/> Box </span>
</p>
</div> 
</Link>
</div>
<div>
<Link to='/product/i-genius-a-twist-in-the-tale'>
  <div style={{ height: '11vw',paddingTop:'3%',fontSize:'24px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
      <p  style={{fontSize:'2vw',marginTop:'3%'}}>
      <div>
      <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/BestGiftForYourChildIcon.png`} 
        style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto'}}/>
        </div>
      <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'11%'}}>  Best Gift For <br/> Your Child </span>
      </p>
  </div>
  </Link>
  </div>
<div>
<Link to='/product/viswasar-postmortem-by-shrimati-mukkoti'>
  <div style={{ height: '11vw',paddingTop:'3%',fontSize:'24px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
    <p  style={{fontSize:'2vw',marginTop:'3%'}}>
    <div>
  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Viswaserpostmortem.png`} 
  style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto'}}/>
  </div>
  <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'17%',textDecoration:'none'}}>  Viswas er postmortem <br/> </span>
    (<span style={{ color:'#e84118',fontSize:'80%' }}>Thriller Bengali Novel</span>)
    </p>
</div>
</Link>
</div>
<div>
  <Link to='/category/fiction-non-fiction/'>
    <div style={{height: '11vw',paddingTop:'3%',fontSize:'27px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
      <p  style={{fontSize:'2vw',marginTop:'1%'}}>
        <div>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bestCollectionbooks.png`} 
        style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto'}}/>
        </div>
        <span style={{fontSize: '1.9vw',lineHeight:'6%',paddingLeft:'4%',marginLeft:'11%'}}> Best Collection Of Novels, Fictions And <br/> Non-Fiction Books</span>
      </p>
    </div>
    </Link>
</div>

</Slider>
 
   {/* </Carousel>       */}
             </MediaQuery>
             {/* End of tab */}
             {/* larger mobile */}
             <MediaQuery maxWidth={767} and minWidth={539}>
             <div>
<div id="modiv">

  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>

<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>

{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}

{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>

<Link style={{ textDecoration: 'none' }} to="/"> 

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>
</Link>
 <span id="cart">
 <span>{(this.props.ItemsInCart === 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
  </span>

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} id="cartimg"  onClick={()=>MobileCheckLogin()}/>

</div>
</div>
 {/*#################### Mobile StickyTop SEARCH BOX ############################ */}
 <div id="homeMenusearch" >
 {/* <img  src={require('./images/mypustaknewlogo.png')} style={{width:'7vw',height:'7vw'}}/> */}
 <Link style={{ textDecoration: 'none' }} to="/"> 
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mypustaknewlogo.png`} style={{width:'7vw',height:'7vw'}} onClick={this.props.makeSearchBlank}/>
 </Link>
 <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                  style={{width:'3vw'}} />
                   <span id="DonateBooksMH">Donate Book</span></button></Link>
                    <input className="search-txt" type="search" 
                    name='searchBook' value={this.props.searchBook}
                    onChange={this.onSearchChange} ref="searchBookFixed"
                      // style={{ position:'absolute',width:'89%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'1%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      style={{ position:'absolute',width:'73%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'18%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}

                      placeholder="Search Books, Author, Publisher, Title, ISBN #"/>
                     
          

                    <a  href="#">
                    
                    <i className="fas fa-search"
                      style={{ position:'absolute',marginTop:'2.5%',marginLeft:'86%' }}
                      />
                    </a>  
                  </div>
<div className="rectangle" style={{marginTop:'-5%'}}>
<div className="column1">
<Link to="search?value=ncert upsc">
            <div>
    <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mobilencert.229b7080.jpg`} style={{ float:'left' }}/>
    </div>
    </Link>
     {/* <div className="textbox"></div> */}
  </div>
<div className="column2"> 
 <Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay={true} infiniteLoop={true}>

                 <Link to="/donate-books">
                <div>
               <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/donate-books.jpg`}/>
               </div>
               </Link>
               <Link to="/product/Ten-Best-Novels-To-Read">
               <div>
               <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/MostLovedBooks10.jpeg`} href="/product/Ten-Best-Novels-To-Read"/>
               </div>
               </Link>

              <Link to="search?value=study material">  
              <div>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/Study+material+book+banner+final.jpg`}/>
              </div>
              </Link>
 </Carousel >
  </div>
</div>

<Slider {...settings1}>

             <div>
             <Link to='/product/Ten-Best-Novels-To-Read'>
             <div style={{ height: '11vw',paddingTop:'3%',fontSize:'27px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af'}}>
             <p style={{fontSize:'2vw',marginTop:'3%'}}>
   <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HomepageMostLovedBookBox.png`}
style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto' }}/>
       <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'11%'}}> Most Loved Book <br/> Box </span>
 </p>
          </div> 
          </Link>
          </div>
          <div>
            <Link to='/product/i-genius-a-twist-in-the-tale'>
              <div style={{ height: '11vw',paddingTop:'3%',fontSize:'24px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
              <p  style={{fontSize:'2vw',marginTop:'3%'}}>
                <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/BestGiftForYourChildIcon.png`} 
                style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto' }}/>
                <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'11%'}}>  Best Gift For <br/> Your Child </span>
              </p>
           </div>
           </Link>
           </div>
           <div>
           <Link to='/product/viswasar-postmortem-by-shrimati-mukkoti'>
              <div style={{ height: '11vw',paddingTop:'3%',fontSize:'24px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                <p  style={{fontSize:'2vw',marginTop:'3%'}}>
                <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Viswaserpostmortem.png`} 
                style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto' }}/>
                <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'11%'}}>  Viswas er postmortem <br/> </span>
                (<span style={{ color:'#e84118',fontSize:'80%' }}>Thriller Bengali Novel</span>)
                </p>
                </div>
                </Link>
                </div>
                <div>
              <Link to='/category/fiction-non-fiction/'>

                  <div style={{height: '11vw',paddingTop:'3%',fontSize:'27px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                      <p  style={{fontSize:'2vw',marginTop:'1%'}}>
                        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bestCollectionbooks.png`} 
                        style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.4%',width:'1%',height:'auto' }}/>
                        <span style={{fontSize: '1.9vw',lineHeight:'6%',paddingLeft:'4%',marginLeft:'11%'}}> Best Collection Of Novels, Fictions And <br/> Non-Fiction Books</span>
                      </p>
                  </div>
                </Link>
                </div>
      
           </Slider>
</MediaQuery>
{/* End of larger mobile */}

{/* smaller mobiles */}
<MediaQuery maxWidth={538} >
             <div>
<div id="modiv">
 {/* <img src={homeicon} id="homeicon" onClick={()=>this.openNav()}></img>
 <div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a> */}
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>

{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}
{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>

<Link style={{ textDecoration: 'none' }} to="/"> 

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>

</Link>
 <span id="cart">
 <span>{(this.props.ItemsInCart === 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
  </span>

<i class="fas fa-shopping-cart" id="cartimg" onClick={()=>MobileCheckLogin()}></i>

</div>
</div>
 {/*#################### Mobile StickyTop SEARCH BOX ############################ */}
 <div id="homeMenusearch" >

  <div>
  <InputGroup className="mb-3">
    <InputGroup.Prepend>
      <InputGroup.Text style={{height:'8vw'}}> 
      <i class="fa fa-search" style={{fontSize:'3vw'}}></i>
                    </InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl style={{height:'8vw',borderBottom:'0px'}}
    value={this.props.searchBook}
    onChange={this.onSearchChange} ref="searchBookFixed"
      placeholder="Search Books, Author, Publisher, Title, ISBN #"
    />
     <InputGroup.Append>
      <Button id="donatebook"><Link style={{ textDecoration: 'none' }} to="/donate-books">  
        
                  <b >Donate Book</b></Link></Button>
    </InputGroup.Append>
  </InputGroup>
  </div>
                  </div>
<div className="rectangle">
<div className="column1">
<Link to="search?value=ncert upsc">
            <div>
    <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mobilencert.229b7080.jpg`} style={{ float:'left' }}/>
    </div>
    </Link>
     {/* <div className="textbox"></div> */}
  </div>
<div className="column2"> 
 <Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay={true} infiniteLoop={true}>

                <Link to="/donate-books">
                <div>
               <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/donate-books.jpg`}/>
               </div>
               </Link>
               <Link to="/product/Ten-Best-Novels-To-Read">
               <div>
               <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/MostLovedBooks10.jpeg`}/>
               </div>
               </Link>
             <Link to="search?value=study material">
             <div>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/Study+material+book+banner+final.jpg`}/>
              </div>
              </Link>
 </Carousel >
  </div>
</div>
<div id="sliderdiv">
<Slider {...settings1}>

             <div>
             <Link to='/product/Ten-Best-Novels-To-Read'>

             <div style={{ height: '11vw',paddingTop:'3%',fontSize:'27px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af'}}>
             <p style={{fontSize:'2vw',marginTop:'3%'}}>
   <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HomepageMostLovedBookBox.png`}
style={{ marginTop:'0.4%',position:'absolute',marginLeft:'0.5%',width:'1%',height:'auto'}}/>
       <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'11%',color:'#868383'}}> Most Loved Book <br/> Box </span>
 </p>
          </div>
          </Link>
           </div>
          <div>
          <Link to='/product/i-genius-a-twist-in-the-tale'>
            <div style={{ height: '11vw',paddingTop:'3%',fontSize:'24px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
              <p  style={{fontSize:'2vw',marginTop:'3%'}}>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/BestGiftForYourChildIcon.png`} 
              style={{ marginTop:'0.2%',position:'absolute',marginLeft:'0.5%',width:'1%',height:'auto'}}/>
                <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'11%',color:'#868383'}}>  Best Gift For <br/> Your Child </span>
              </p>
            </div>
            </Link>
            </div>
           <div>
             <Link to='/product/viswasar-postmortem-by-shrimati-mukkoti'>
              <div style={{ height: '11vw',paddingTop:'3%',fontSize:'24px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                <p  style={{fontSize:'2vw',marginTop:'3%'}}>
                    <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Viswaserpostmortem.png`} 
                  style={{ marginTop:'0.5%',position:'absolute',marginLeft:'0.5%',width:'1%',height:'auto'}}/>
                  {/* <span style={{fontSize: '2.5vw',lineHeight:'6%',marginLeft:'17%',color:'#868383'}}>  Viswas er postmortem <br/> </span> */}
                  <span style={{fontSize:'2.5vw',marginLeft:'10%',fontWeight:'bold',color:'#868383'}}>Best Thriller Bengali Novel</span>
                </p>
            </div>
            </Link>
            </div>
            <div>
            <Link to='/category/fiction-non-fiction/'>

                <div style={{height: '11vw',paddingTop:'3%',fontSize:'27px',backgroundColor: 'white',fontWeight:'bold',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                    <p  style={{fontSize:'2vw',marginTop:'3%'}}>
                    <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bestCollectionbooks.png`} 
                    style={{ marginTop:'0.2%',position:'absolute',marginLeft:'0.5%',width:'1%',height:'auto'}}/>
                    <span style={{fontSize: '2.5vw',lineHeight:'6%',paddingLeft:'4%',marginLeft:'11%',color:'#868383'}}> Best Fictions And <br/> <span style={{marginLeft:'11%'}}> Non-Fiction Books</span></span>
                    </p>
                </div>
                </Link>
            </div>
           </Slider>
           </div>
</MediaQuery>
{/* End of smaller mobile */}
{/* Desktop */}

               <MediaQuery minWidth={992}>
               {/* {alert(this.props.SuccessSentRestMail)} */}
               {/* Popup For Showing Message for Reset Mail */}
               <Popup
          open={this.props.SuccessSentRestMail}
          // closeOnDocumentClick={false}
          // onClose={()=>this.setState({addressopen:false})}
          // onOpen={()=>()=>{setTimeout(alert("okk"),3000)}}
          contentStyle={{ 
                        width:'40%',
                        height:'47%',
                        borderRadius:'5px',
                        // marginLeft:'67%',
                        marginTop:'7%',
                        // background:'transparent'
                        // pa
                        }} 
          overlayStyle={{ 
            // background:'transparent',
           }}
        >
                <a className="close" onClick={this.props.ActivteSuccesPopup} style={{
                  color: 'black',zIndex:'999',opacity:'1',fontWeight:'normal',
                  float:'right'}}>
              &times;
            </a>
        <p id="HomePassRestMsg">Password Rest Successfully.</p>
        <p id="HomePassRestMsg_2"> We have send a rest link on your email please check your email to rest your Password</p>
         </Popup>
               <div className="rectangle">
            <div className="column1"> 
              <Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay={true} infiniteLoop={true}>

                <Link to="/donate-books">
                <div>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/donate-books.jpg`}/>
              </div>
              </Link>
              <Link to="/product/Ten-Best-Novels-To-Read">
              <div>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/MostLovedBooks10.jpeg`} onClick={()=>{return <Redirect to="/product/Ten-Best-Novels-To-Read"/>}}/>
              </div>
              </Link>
              <Link to="search?value=study material"> 
              <div>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/Study+material+book+banner+final.jpg`}/>
              </div>
              </Link>


 
              </Carousel >
               </div>
                  <div className="column2">
                  <Link to="search?value=ncert upsc">
                    <div>
                              {/* https://ik.imagekit.io/kbktyqbj4rrik/uploads/banner/HomePageNCERTBanner.jpg */}
                    <img src={'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/banner/HomePageNCERTBanner.jpg'} style={{ float:'left' }}/>
                  {/* <div className="textbox"></div> */}
                  </div>
                  </Link>
               </div>
             </div>
             <div id="homeBlock3">
             <Link to='/product/Ten-Best-Novels-To-Read'>
            <div style={{ borderRight:'1px solid grey',width:'23%' }}>
            
             <p>
               <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HomepageMostLovedBookBox.png`}
 style={{ marginTop:'1%',position:'absolute',marginLeft:'-2.5%',width:'2%' }}/>
 <span style={{  fontSize: '1.1vw'}}> Most Loved Book <br/> Box </span>
             </p>
            </div>
            </Link>


          <Link to='/product/i-genius-a-twist-in-the-tale'>
          <div style={{ borderRight:'1px solid grey',width:'23%' }}>
              <p>
              <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/BestGiftForYourChildIcon.png`} 
 style={{  marginTop:'.5%',position:'absolute',marginLeft:'-2.8%' ,width:'2%' }}/>
 <span style={{fontSize: '1.1vw'}}>  Best Gift For <br/> Your Child </span>
              </p>
          </div>
          </Link>

          <Link to='/product/viswasar-postmortem-by-shrimati-mukkoti'>
              <div style={{ borderRight:'1px solid grey',width:'23%' }}>
                 <p>
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Viswaserpostmortem.png`} 
                  style={{ marginTop:'1%',position:'absolute',marginLeft:'-2.8%',width:'2%'  }}/>
                  <span style={{fontSize: '1.1vw'}}>  Viswas er postmortem <br/> </span>
                  (<span style={{ color:'#e84118',fontSize:'80%' }}>Thriller Bengali Novel</span>)
                 </p>
              </div>
              </Link>

              <Link to='/category/fiction-non-fiction/'>
                  <div style={{ borderRight:'1px solid grey',width:'31%' }}>
                  <p>
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/bestCollectionbooks.png`} 
style={{ marginTop:'1%',position:'absolute',marginLeft:'-2.8%',width:'2%'  }}/>
<span style={{fontSize: '1.1vw'}}> Best Collection Of Novels, Fictions And <br/> Non-Fiction Books</span>
                  </p>
                  </div>
              </Link>
 
               </div> 
                {/*####################StickyTop NAV ############################ */}
              <nav id="homeMenu" style={{ display:this.state.menuS }}>
              <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} style={{ height:'90%',cursor:'pointer',width:'10vw' }} onClick={()=>window.location.reload()}/>
                  <Popup
          open={this.state.Cat}
          // closeOnDocumentClick={false}
          onClose={this.CLOSECategory}
          // lockScroll={true}
          contentStyle={{ width:'80%',marginTop:'5.5%',height: '69%'}}
        >
          <div style={{ color:'black' }} >
            <a className="close" onClick={this.CLOSECategory} style={{marginTop:'-0.5%'}}>
              &times;
            </a>
           <Popupcat/>
          </div>
        </Popup>
                  <div id="homeMenusearch" >
                    <button id="slidercategorybtn"
                    onClick={this.OPENCategory}

                    ><span id="categoryword">Categories </span><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/categoriesicon.png`} id="categoryimg"/> </button>  
                    {/*####################StickyTop SEARCH BOX ############################ */}
                    <input className="search-txt" type="search" 
                    name='searchBook' value={this.props.searchBook}
                    onChange={this.onSearchChange}
                    style={{ position:'absolute',width:'24.8vw',marginTop:'-1.1%'}}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"/>



                    <a id="homeMenuSearchBtn" href="#">
                    <i className="fas fa-search"
                      style={{ position:'absolute',marginTop:'0%'  }}
                      />
                    </a>  
                  </div>
                {/* <button> Donate Book</button> */}
                <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                  /><span id="DonateBooksMH">Donate Book</span></button></Link>
                  <span id="loginSignup"><img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/loginsignUp.png`} /> { (this.props.userToken === null)?
      <a onClick={this.ToggleASignupOpenModal}>Log In/Sign Up
<Popup
          // open={this.state.Sopen}
          open={this.state.ASignupopen}
          closeOnDocumentClick={false}
          // onClose={this.scloseModal}
          contentStyle={{ 
                        width:'58.9%',
                        height:'70.6%',
                        borderRadius:'5px'
                        }} 
        > 
        {(this.state.showLS === true)?this.LOG_IN:this.SIGN_IN}
        </Popup>

</a>:<Popup trigger={<a style={{ fontSize:'60%' }}> Hi, Reader</a>}
          on="hover"
          open={this.state.Sopen}
          contentStyle={{ 
                        width:'15%',
                        // height:'120px',
                        borderRadius:'5px',
                        // position:'absolute'
                        }}              
        > 
        <div style={{ color:'black',height:'30%' }}>
        <div id="listonProfile"> {this.state.name}</div>
        <div id="listonProfile"><Link to='/customer/customer_account' > View Profile</Link></div>
        <div id="listonProfile"><Link to='/customer/customer_order' > Your Orders</Link></div>
        <div id="listonProfile"><Link to='/my-wish-list' > Your Wishlist</Link></div>
        <div id="listonProfile"><Link to='/mypustak-wallet' >Your Wallet</Link></div>
        <div id="listonProfile" onClick={this.Userlogout}> Logout</div>
      </div>
        </Popup>
}</span>

<Popup
          trigger={<span id="cart" onMouseEnter={this.props.CartopenModal}><span>{(this.props.ItemsInCart === 0)?null:<span id="DisplayCartNos">{this.props.ItemsInCart}</span>}</span>
          <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} />
          
          Cart </span>}

          on='hover'
          open={this.props.PopupCart}
          // on="hover"
          modal={false}
          lockScroll={true}
          position="bottom center"
          onClose={this.props.CartcloseModal}
          onOpen={this.CartFadeOut}
          // closeOnDocumentClick
          // onClose={this.closeModal}
          overlayStyle={{ 
            backgroundColor:'transparent',
            // display:'none'
           }}
           offsetX='20px'
           disabled={(this.props.cartDetails.length === 0)?true:false}
          contentStyle={{ 
                        width:'35%',
                      //  height:'559%',
                      //  marginTop:'4%',
                      //  marginRight:'10%',
                      //  paddingRight:'10%',
                      // zIndex:'999',
                      // height:'220px',
                      // overflow: 'scroll',
                      // width:'220px',
                      // marginLeft:'2%'
                      // backgroundColor:'yellow',
                      left: '865.2px',

                        }} 
        >
    <div style={{ }} id={(this.props.ItemsInCart === 1)?"showCartOne":"showCart"}>
          

{this.props.cartDetails.map(cart=>
  <div id="popupCartBody">
  <div id="FirstpartPopupCart">
{/* const  src=`https://ik.imagekit.io/kbktyqbj4rrik/uploads/books/medium/${thumb}`; */}

  <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${cart.bookThumb}`}  style={{ float:'left',height:'75.75px',width:'53.56px' }}id="book"/>
  <div id="PopupbookDetails">
  <span id="title">{cart.bookName}</span>
  

</div>

<span id="BookCond">{cart.bookCond}<label id="newprice"><b> <span id="mrp">MRP:<span id="orgprice"> &#8377;<strike>{Math.round(cart.bookPrice)}</strike></span>
</span><span style={{ color:'#e26127' }}>Free</span></b></label> </span>
<div>

  <span id="shipping">Shipping Handling Charge &#8377;{Math.round(cart.bookShippingCost)}</span>

  <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png" id="delete" onClick={()=>this.RemoveFromCart(cart.bookInvId,cart.Cart_id)}></img>


</div>


</div>
<hr id="cartPopupHr"/>
</div>
)}
{this.state.cartMsg && <span id="cartMsg">{this.state.cartMsg}</span>}

</div>
{/* <Link to="/view-cart"> */}
{/* // {(TotalShipCost>80)?<button id="checkout" onClick={()=>CheckLogin()} >Proceed To Checkout</button>:<span style={{ marginLeft:'21%' }}>minimum order value should be Rs. 80</span>} */}
<button id="checkout" onClick={()=>CheckLogin()} >Proceed To Checkout</button>
<button id="continueShopingCart">Continue Shopping > </button>
{/* </Link> */}
        </Popup>
                  {/* <span id="more">More<i className="fas fa-angle-down" style={{ marginLeft:'.2%' }}
                   /></span> */}
              </nav>
              </MediaQuery>
          </div>
        );
      }
}
// export default Slider;
const mapStateToProps = state => ({
  searchBook:state.homeR.searchBook,
  userToken:state.accountR.token,
  searchBook:state.homeR.searchBook,
  userToken:state.accountR.token,
  ErrMsg:state.accountR.ErrMsg,
  cartDetails:state.cartReduc.MyCart,
  PopupCart:state.cartReduc.PopupCart,
  ItemsInCart:state.cartReduc.cartLength,
  SuccessSentRestMail:state.accountR.ActivateSuccPopupState,
  ClickedCategory:state.homeR.ClickedCategory,
  cartRedirectMobile:state.cartReduc.cartRedirectMobile,

  // cartRedirectMobile:sta

})
export default connect(mapStateToProps,{CartopenModal,CartcloseModal,doSearch,logout,ActivteSuccesPopup,ToBeAddedToCart,RemoveToBeAddedToCart,setCategory,MobileCartRedirect,
  AddToCart,removeAllCart,RemoveCart,makeSearchBlank})(Slideras);