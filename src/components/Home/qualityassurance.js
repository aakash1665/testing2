import React, { Component } from 'react';
// import logo from './logo.png';
 import './qualityassurance.css';
//  import donarassurance from './donorassurance.png';
// import group from'./Qualitycontrolteam.png';
// import shipment from './Qualitybeforeshipment.png';
// import packing from './Qualitypacking.png';
import MediaQuery from 'react-responsive';
// import Jumbotron from 'react-bootstrap/Jumbotron'
import { Carousel } from 'react-responsive-carousel';
class Qualityassurance extends Component {
  render() {
    return (
        <div id="qadiv1">
        {/* <Jumbotron> */}
        <MediaQuery  minWidth={992}> 
        <p id="qalayer1">Our Book Quality Assurance Test</p>
        <hr style={{marginTop: '2%'}}/>
        <div style={{display:'flex'}}>
             <div id= "DAssurance" style={{flex:'1'}}>
                   <div id ="icon">
                     <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/donorassurance.10f21d27.png`} id="donarimg" alt="Profile image"></img>
                  </div>
                  <p id="qalayer2">Donor Assurance</p>
                   <p id="qadonar">Donors insure quality books before
                  sending books to us using User 
                   Book Condition Guideline.</p>
              </div>
               <div id="Qualityctrl" style={{flex:'1'}}>
                 <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitycontrolteam.36746bb2.png" id="groupimg" alt="Profile image"></img>
                  <p id="quality">Our Quality Control Team</p>
                  <p id="group">We check every single book thrice
                     (By Our Quality Control Team) to 
                  ensure  that our book meets user’s
                   expectation.</p>
                 </div>
               <div id ="QualityShip" style={{flex:'1'}}>
                      <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitybeforeshipment.a80d6f28.png" id="shipmentimg" alt="Profile image"></img> 
                      <p id="shipment" >Quality Before Shipment</p>
                      <p id="ship">Before we ship your books our 
                     packing team verify quality.</p>
              </div>
              <div id= "qualityPack" style={{flex:'1'}}>
                     <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitypacking.55cabb1a.png" id="packingimg" alt="Profile image"></img>
                    <p id="packing" >Quality Packing</p>
                    <p id="pack">We use best methods of 
                   packings to ensure that no 
                    books should damage during
                    transit.</p>
             </div>
        
         <div style={{flex:'1.5'}} id="vidpos">
        <iframe src="https://www.youtube.com/embed/q37G8IR-T-U"
         frameborder="0" 
       allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" 
         allowfullscreen
        title='video'
        scrolling="no"
        height="100%"
        width="100%"
        marginLeft= "-5%"
/>
</div>
       </div>
</MediaQuery>
<MediaQuery  maxWidth={991}  and minWidth={539}> 
<p id="qalayer1">Our Book Quality Assurance Test</p>
        <hr style={{marginTop: '2%'}}/>
<Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay={true} infiniteLoop={true}>

        {/* <hr style={{marginTop: '2%'}}/> */}
        <div style={{display:'flex'}}>
             <div id= "DAssurance" style={{flex:'1'}}>
                   <div id ="icon">
                     <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/donorassurance.10f21d27.png`} id="donarimg" alt="Profile image"></img>
                  </div>
                  <p id="qalayer2">Donor Assurance</p>
                   <p id="qadonar">Donors insure quality books before
                  sending books to us using User 
                   Book Condition Guideline.</p>
              </div>
               <div id="Qualityctrl" style={{flex:'1'}}>
                 <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitycontrolteam.36746bb2.png" id="groupimg" alt="Profile image"></img>
                  <p id="quality">Our Quality Control Team</p>
                  <p id="group">We check every single book thrice
                     (By Our Quality Control Team) to 
                  ensure  that our book meets user’s
                   expectation.</p>
                 </div>
               <div id ="QualityShip" style={{flex:'1'}}>
                      <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitybeforeshipment.a80d6f28.png" id="shipmentimg" alt="Profile image"></img> 
                      <p id="shipment" >Quality Before Shipment</p>
                      <p id="ship">Before we ship your books our 
                     packing team verify quality.</p>
              </div>
              <div id= "qualityPack" style={{flex:'1'}}>
                     <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitypacking.55cabb1a.png" id="packingimg" alt="Profile image"></img>
                    <p id="packing" >Quality Packing</p>
                    <p id="pack">We use best methods of 
                   packings to ensure that no 
                    books should damage during
                    transit.</p>
             </div>
        
         
       </div>  </Carousel >
              <div id="vidpos">
 <iframe src="https://www.youtube.com/embed/q37G8IR-T-U"
         frameborder="0" 
                 allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" 
      allowfullscreen
      title='video'
        scrolling="no"
        height="400"
       width="100%"      
       marginLeft='2%'
         marginBottom='100%'
 />
 </div>
</MediaQuery>
<MediaQuery  maxWidth={538}> 
<div id="qualityassurancediv"><p id="qalayer1">Our Book Quality Assurance Test</p></div>
        {/* <hr style={{marginTop: '2%'}}/> */}
<Carousel showThumbs={false} showStatus={false} showArrows={false} autoPlay={true} infiniteLoop={true}>
<div style={{display:'flex'}}>
             <div id= "DAssurance" style={{flex:'1'}}>
                   <div id ="icon">
                     <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/donorassurance.10f21d27.png`} id="donarimg" alt="Profile image"></img>
                  </div>
                  <p id="qalayer2">Donor Assurance</p>
                   <p id="qadonar">Donors insure quality books before
                  sending books to us using User 
                   Book Condition Guideline.</p>
              </div>
               <div id="Qualityctrl" style={{flex:'1'}}>
                <div id="iconQ"> 
                <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitycontrolteam.36746bb2.png" id="groupimg" alt="Profile image"></img>
                </div>
                  <p id="quality">Our Quality Control Team</p>
                  <p id="group">We check every single book thrice
                     (By Our Quality Control Team) to 
                  ensure  that our book meets user’s
                   expectation.</p>
                 </div>
               <div id ="QualityShip" style={{flex:'1'}}>
                     <div id="iconS"> 
                       <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitybeforeshipment.a80d6f28.png" id="shipmentimg" alt="Profile image"></img> 
                     </div>
                      <p id="shipment" >Quality Before Shipment</p>
                      <p id="ship">Before we ship your books our 
                     packing team verify quality.</p>
              </div>
              <div id= "qualityPack" style={{flex:'1'}}>
                   <div id="iconP">
                       <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Qualitypacking.55cabb1a.png" id="packingimg" alt="Profile image"></img>
                    </div>
                    <p id="packing" >Quality Packing</p>
                    <p id="pack">We use best methods of packings to ensure that no 
                    books should damage during
                    transit.</p>
             </div>
        
         
       </div>               </Carousel >
              <div id="vidpos">
 <iframe src="https://www.youtube.com/embed/q37G8IR-T-U"
         frameborder="0" 
                 allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" 
      allowfullscreen
      title='video'
        scrolling="no"
        height="200"
       width="100%"      
         marginBottom='100%'
 />
 </div>
</MediaQuery>


 {/* </Jumbotron> */}
          </div>
    );
        
  }
}


 export default Qualityassurance;
