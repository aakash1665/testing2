import React, { Component } from 'react';
import {Redirect} from'react-router-dom';
import './DonarAccDonarReq.css';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import {Helmet} from 'react-helmet';
import { Link} from 'react-router-dom'
import { connect } from 'react-redux';
import {GetDonationHistory} from '../../actions/donationActions'
import Table from 'react-bootstrap/Table'

class DonarAccDonarReq extends Component {
   constructor(props) {
    super(props);
  this.state={
    isdata:false,
   
 }
 

this.handleClick = this.handleClick.bind(this);
}

componentDidMount() {
  if(localStorage.getItem('user') === null){
    // alert("RED")
    window.location.href="/"
  }else{
    const token =`Token ${localStorage.getItem('user')}`
    this.props.GetDonationHistory(token)
  }
}

  //  renderRedirect  (e) {
  
  //  window.location.href = "";
  // }
  //  renderRedirect1  (e) {
  
  //  window.location.href = "/orders";
  // }
  //  renderRedirect2  (e) {
  
  //  window.location.href = "";
  // }
  //  renderRedirect  (e) {
  
  //  window.location.href = "/donate/DonationReq/";
  // }
   handleClick(e) {
    e.preventDefault();
    this.setState(prevstate => {
return{
      isdata: !prevstate.isdata}
    });
  }

  render() {
    var DOnatedBooks=[]
    Object.keys(this.props.donationHistory).map((val)=>{
      var books = this.props.donationHistory[val].books
     books.map(data=>{
      DOnatedBooks.push(data)
       
     })
      
     
      

    })
    console.log(DOnatedBooks);
    const Chnage_iDate=(date)=>{
      let  convDate = new Date(date)
      return convDate.getDate()+ "-" + (convDate.getMonth() + 1) + "-" + convDate.getFullYear();
     }
     
    const CheckStatus=(status)=>{
      if(status === 1){
        return "Pending"
      }else if(status === 2){
        return "In Queue"
      }else if(status === 3){
        return "Processing"
      }else if(status === 4){
        return "Shipping"
      }else if(status === 5){
        return "Delivered/Dispatch"
      }else if(status === 8){
        return "Not Shipping"
      }else{

      }
    }
    const ConvDate=(date)=>{
      try {
        // alert(date)
        var cDate = new Date(date*1000)
        return cDate.toDateString()
      } catch (error) {
        var covdate = new Date(date.getTime()/1000)
          // alert(covdate)
        return date
      }

    }
   if(this.state.isdata===false){
return (
      <div>
               <MainHeader/>
     <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style></Helmet>
      {/* <p className="Your-Account">Your Account>Your Orders & Book Donations</p><br/> */}
    <label className="Your-Orders-Books-Donations">Your Orders & Books Donations</label>
    {/* <input type="text" placeholder="Search All Orders & Books Donations" name="search" id="buttonorders"></input>
  <input type="submit" id="btnsubmit" value="Search"/> */}
<br/><br/><br/>
  {/* <table id="donatetable" >
      <tr> */}
        <Link to='/customer/customer_order'><button type="submit" id="btnorder" >Orders</button></Link>
         <Link to='/donor/donor_donation_request'><button type="submit" id="btndonate" >Book Donations</button></Link>
          {/* <button type="submit" id="ordersbtn" onClick={()=>this.renderRedirect1()}>Orders</button>
       <Link to='/donor/donor_donation_request'> <button type="button" id="donationbtn" >Book Donations</button></Link> */}
        
   
{/*    
      </tr>
  </table> */}
  {/* <span id="donatehr"></span> */}
  <ul id="uldonate">
  <li id="active" >Donation Request</li>
  
  <li id="nonactive"onClick={this.handleClick}>Donated Books</li>
</ul>
<div id="donardonationtable">  
      <Table id="donatetable1" responsive="lg">

 <th id="All-Donation-Requests">All Donation Request</th>
</Table>
{/* <hr id="donatehr1"/> */}

<Table id="donatetable1" responsive="lg">
<thead>
<tr>
<th>Id</th>
<th>Book Pick Up Date And Time</th>
<th>Books Donated</th>
{/* <th id="heading">Date<br/></th> */}
<th>Book Weight</th>
<th>Status</th>
</tr>
</thead>
<tbody>
  {this.props.donationHistory.map(data=>
<tr>
  <td>{data.req_id}</td>
  <td>{ConvDate(data.pickup_date_time)}</td>
  <td>{data.no_of_book}</td>
  {/* <td>{Chnage_iDate(data.i_date)}</td> */}
  <td>{data.app_books_weight}KG</td>
  {/* <td><button type="button" id="btnpending"> {CheckStatus(data.status)}</button></td> */}
  <td><button id="btnpending"> {CheckStatus(data.status)}</button></td>
</tr>
  )}

{/* <tr>
<td>5552</td>
<td>24-09-2018 12:09AM</td>
<td>20</td>
<td>07-09-2018</td>
<td>1KG</td>
<td><button type="submit" id="btnpending"> Dispatched</button></td>
</tr> */}
</tbody>
</Table>
   </div>
   <MainFooter/>
</div>
);
}
else{
  return(
    <div >
             <MainHeader/>
     <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style></Helmet>
    {/* <p className="Your-Account">Your Account>Your Orders & Book Donations</p><br/> */}
  <label className="Your-Orders-Books-Donations">Your Orders & Books Donations</label>
  {/* <input type="text" placeholder="Search All Orders & Books Donations" name="search" id="buttonorders"></input> */}
{/* <input type="submit" id="btnsubmit" value="Search"/> */}
<br/><br/><br/>
<Link to='/customer/customer_order'><button type="submit" id="btnorder" >Orders</button></Link>
         <Link to='/donor/donor_donation_request'><button type="submit" id="btndonate" >Book Donations</button></Link>
{/* <hr id="donatehr"/> */}
<ul id="uldonate">
<li id="nonactive" onClick={this.handleClick}>Donation Request</li>

<li id="active">Donated Books</li>
</ul>
<div id="donardonationtable">  

<Table id="donatetable2" responsive="lg">
<thead>
<tr>
<th>Book Cover</th>
<th>Books Title</th>
{/* <th>Category</th> */}
<th>Status</th>
{/* <th style={{paddingTop:'1%',paddingBottom:'1%'}}>Reading<br/></th> */}
</tr>
</thead>
<tbody>
  {DOnatedBooks.map(book=>
 <tr>
  <td><img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${book.thumb}`} id="donatereqbooksimg"/></td>
  <td>{book.title}</td>
  {/* <td></td> */}
  <td>{book.status}</td>
  {/* <td></td> */}
  {/* <td><button type="button" id="btnpending"> {book.status}</button></td> */}
</tr> 

  )}
</tbody>
</Table>
</div>
<MainFooter/>
</div>
);
}
}
}
const mapStateToProps = state => ({
  donationHistory:state.donationR.donationHistoryS,
})
export default connect(mapStateToProps,{GetDonationHistory})(DonarAccDonarReq);


