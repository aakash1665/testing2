import React, { Component } from 'react'
import { Link,Redirect,browserHistory } from 'react-router-dom'

import MediaQuery from 'react-responsive';

import { LazyLoadImage } from 'react-lazy-load-image-component';
import 'react-lazy-load-image-component/src/effects/blur.css';
// import Skeleton from 'react-loading-skeleton';
export default class Book extends Component {
    details(){
        alert("details")
    }
    ToProduct(){

        // window.location = `/product`
        
          }
  render() {
      
    const {title,price,thumb,slug} = this.props.book;
    // https://ik.imagekit.io/kbktyqbj4rrik//tr:w-200,h-300/uploads/books/medium/0776269001547473077.jpg
    const src=`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${thumb}`
    return (
        
        // const {id,name,email,phone,website} = this.props.contact;
    //   <div>
            // <div class="book"> 
            <div class="booklist">
                        {/* Desktop */}
            <MediaQuery minWidth={992}>
                <Link to={`/product/${slug}`}>
                    {/* {(src)? */}
                    <img src={src} />
                    {/* // :<Skeleton height={200} width={200}/>} */}
                    {/* <LazyLoadImage
                        effect="blur"
                        height={220}
                        src={src} /> */}
                </Link>
                <span>
                <Link to={`/product/${slug}`}  style={{ textDecoration: 'none' }}>
                    <p style={{ color:'#008cff',fontSize:'130%',padding:'2%',paddingBottom:'0%',height:'15%', marginBottom:'0rem',overflow:'hidden'}}>{title }</p>
                   
                </Link>
                <span style={{ fontSize:'120%',textDecoration:'line-through'}} >Rs: {Math.round(price)} </span>
                <span style={{ color:'red' }}>FREE</span>
                </span>
                </MediaQuery>
{/* End of desktop */}
{/* Tab */}
                 <MediaQuery  maxWidth={991} and minWidth={768}>
                        <Link to={`/product/${slug}`}>
                                {/* <img src={src} style={{  }} onClick={this.ToProduct}/> */}
                                <LazyLoadImage
                                // placeholder={<p>Loading....</p>}
                                effect="blur"
                                // alt="Loading..."
                                // height={305}
                                // placeholderSrc="Loading...."
                                src={src} />
                        </Link>
                        <span>
                            <Link to={`/product/${slug}`}  style={{ textDecoration: 'none' }}>
                                <div style={{maxHeight: '15vw',
                                            width: '100%',
                                            overflow: 'hidden',
                                            maxWidth: '282px'
                                            }}>   
                                    <p style={{ color:'#008cff',fontSize:'1.5vw',padding:'2%',
                                                paddingBottom:'0%',height:'15%', marginBottom:'0rem',
                                                overflow:'hidden',width:'31vw'
                                            }}>
                                                {title}
                                    </p>
                                </div>
                            </Link>
                            <span style={{ fontSize:'2vw',textDecoration:'line-through'}} >Rs: {price} </span>
                            <span style={{fontSize:'2vw',color:'red'}}>FREE</span>
                        </span>
                </MediaQuery>
                {/* End of tab */}

                {/* Larger Mobile */}
                <MediaQuery  maxWidth={767} and minWidth={539}>
                        <Link to={`/product/${slug}`}>
                            {/* <img src={src} style={{  }} onClick={this.ToProduct}/> */}
                            <LazyLoadImage
                            // placeholder={<p>Loading....</p>}
                            effect="blur"
                            // alt="Loading..."
                            // height={305}
                            // placeholderSrc="Loading...."
                            src={src} />
                        </Link>
                        <span>
                            <Link to={`/product/${slug}`}  style={{ textDecoration: 'none' }}>
                                <div style={{maxHeight: '7vw',
                                            width: '100%',
                                            overflow: 'hidden',
                                            maxWidth: '29vw'
                                            }}>   
                                                <p style={{ color:'#008cff',fontSize:'2.2vw',
                                                            padding:'2%',paddingBottom:'0%',height:'15%', 
                                                            marginBottom:'0rem',overflow:'hidden',width:'30vw'
                                                        }}>
                                                        {title}
                                                </p>
                                </div>
                            </Link>
                            <span style={{ fontSize:'2vw',textDecoration:'line-through'}} >Rs: {price} </span>
                            <span style={{fontSize:'2vw',color:'red'}}>FREE</span>
                        </span>
                </MediaQuery>
                {/* End of Larger Mobile */}


                                {/* Smaller Mobile */}
            <MediaQuery  maxWidth={538}>
                        <Link to={`/product/${slug}`}>
                            {/* <img src={src} style={{  }} onClick={this.ToProduct}/> */}
                            <LazyLoadImage
                            // placeholder={<p>Loading....</p>}
                            effect="blur"
                            // alt="Loading..."
                            // height={305}
                            // placeholderSrc="Loading...."
                            src={src} />
                        </Link>
                        <span>
                            <Link to={`/product/${slug}`}  style={{ textDecoration: 'none' }}>
                                <div style={{maxHeight: '9vw',
                                            width: '100%',
                                            overflow: 'hidden',
                                            maxWidth: '30vw'
                                            }}>   
                                            <p style={{ color:'#007bff',fontSize:'2.8vw',padding:'2%',paddingBottom:'0%',
                                                        height:'15%', marginBottom:'0rem',overflow:'hidden',width:'30vw'
                                                    }}>
                                                    {title}
                                            </p>
                                </div>
                            </Link>
                            <span style={{ fontSize:'3.5vw',textDecoration:'line-through'}} >Rs: {price} </span>
                            <span style={{fontSize:'3.5vw',color:'red'}}>FREE</span>
                        </span>
                </MediaQuery>
                {/* End of Smaller Mobile */}
            </div>
    //   </div>
    )
  }
}
