import React, { Component } from 'react';
import './header.css';
class Header extends Component {
  state={
    display:0,
    menuS:'none'
  }
  
  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll);
}
handleScroll=()=>{
  const scroll = window.scrollY
  this.setState({display:scroll});
  console.log(this.state.display);
  if(this.state.display > 650){
    this.setState({menuS:'block'});
  }else{
    this.setState({menuS:'none'});
  }
  // this.setState({display:Scroll})
}

  render() {
    return (
    <div style={{ position:'fixed' }}>
        <div style={{backgroundColor:'#2DE6F3',width:'100%',height:'13%'}}>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo.png`} alt="MyPustak" style={{marginLeft:'3%',position:'absolute',marginTop:'0.2%',width:'10%'}}></img>
         <button type="submit" id="donatebooks">
        <img src={require('./images/Donatebookslogo.png')} alt="DonateBooks" style={{marginLeft:'-2.3%',position:'absolute',marginTop:'-0.4%',width:'2%'}}></img> Donate Book</button>
        </div>
  
    </div>
    );
  }
}

export default Header;
