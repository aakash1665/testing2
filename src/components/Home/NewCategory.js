import React, { Component } from "react";
import MainHeader from "../MainHeader";
import MainFooter from "../MainFooter";
import GBook from "./GBook";
import axios from "axios";
import "./category.css";
import { Breadcrumbs } from "react-breadcrumbs-dynamic";
import { connect } from "react-redux";
import { Link, Redirect, browserHistory } from "react-router-dom";
import config from "react-global-configuration";
import InfiniteScroll from "react-infinite-scroll-component";
import MediaQuery from "react-responsive";
import Mobilecat from "./mobilecategory";
import ReactGA from "react-ga";
import { Helmet } from "react-helmet";
import { getSeoData } from "../../actions/seodataAction";

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
// import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';

import GetcategoryID from "./GetCategoryId"
import Popup from 'reactjs-popup' 

class Category extends Component {
  state = {
    GetBookResult: [],
    start: 1,
    MEWURL: "",
    COUNT: 1,
    ShowCategory: "",
    display: 0,
    menuS: "none",
    showLS: true,
    // minPrice: "",
    // maxPrice: "",
    arrival_date_min: 0,
    arrival_date_max: Math.floor(new Date().getTime() / 1000),
    binding_cover: [],
    book_condition: [],
    language: [],
    latestLast7Days: false,
    latestLast30Days: false,
    latestLast180Days: false,
    bindingPaperback: false,
    bindingHardcover: false,
    conditionBrandNew: false,
    conditionAlmostNew: false,
    conditionVeryGood: false,
    conditionReadable: false,
    LanguageHindi: false,
    LanguageEnglish: false,
    LanguageBengali: false,
    LanguageDutch: false,
    LanguageEnglishFrench: false,
    LanguageFrench: false,
    LanguageGerman: false,
    LanguageGujarati: false,
    LanguageKannada: false,
    LanguageTamil: false,
    LanguageTelugu: false,
    LanguageSanskrit: false,
    LanguageHindiAndEnglish: false,
    LanguageMarathi: false,
    LanguageForeignLanguage: false,

    filterFlag: 0,
    bindingcover:[],
    FilterToBeApplid:false,
    ShowFilterLoader:false
  };
  url = window.location.href;
  //  urlLength = this.url.length
  CatIndex = this.url.indexOf("/category/");
  newUrl = this.url.slice(this.CatIndex);
  componentDidMount() {
    // alert(
    //   `In componentDidMount and start is ${this.state.start} and newUrl is ${
    //     this.newUrl
    //   }`
    // );
    ReactGA.pageview(window.location.pathname + window.location.search);


    window.scrollTo(0, 0);
    window.addEventListener("scroll", this.handleScroll);
    console.log("in component Did mount upp");
    this.props.getSeoData(
      "https://www.mypustak.com" + window.location.pathname
    );

 
    // Get Category Id
    console.log(`${this.newUrl}`,"Url");
    let CATID=GetcategoryID(this.newUrl)
    console.log(CATID,"GotCAtID DidMount")
// /api/v1/get/category/187/1
    this.setState({ ShowCategory: this.newUrl });
    axios
      // .get(`http://data.mypustak.com/api/v1/get${this.newUrl}${this.state.start}/`)
      .get(`http://localhost:8000/api/v1/get/category/${CATID.id}/${this.state.start}`)
      .then(res => {
        console.log(this.state.ShowCategory,"in componenetDidMount And the Data are ", res.data);
        this.setState({ GetBookResult: res.data.Books, FilteredData: res.data.Books,FilterToBeApplid:res.data.Filter,ShowCategory: this.newUrl });
      })
      .catch(err => console.log("The error is ", err));
  }

  componentWillReceiveProps(nextProps) {
    if (this.state.ShowCategory !== nextProps.ClickedCategory && nextProps.ClickedCategory !=="") {
      console.log(
        "in component will receive props ClickedCategory ",
        nextProps.ClickedCategory
      );

      console.log(
        "in component will receive props ShowCategory ",
        this.state.ShowCategory
      );
      const url = window.location.href;
      //  urlLength = this.url.length
      const CatIndex = url.indexOf("/category/");
      const newUrl = url.slice(CatIndex);
      console.log(`${newUrl}`,"Url");
      let CATID=GetcategoryID(newUrl)
      console.log(CATID,"GotCAtID WillRecProps")

      axios
        // .get(
        //   `http://data.mypustak.com/api/v1/get${nextProps.ClickedCategory}${
        //     this.state.start
        //   }/`
        // )
      .get(`http://localhost:8000/api/v1/get/category/${CATID.id}/${this.state.start}`)
        .then(res => {
          this.props.getSeoData(
            "https://www.mypustak.com" + window.location.pathname
          );
          console.log("WillRecive Props Before SetState");
          
          this.setState({
            GetBookResult: res.data.Books,
            ShowCategory: nextProps.ClickedCategory,
            FilterToBeApplid:res.data.Filter,
            // minPrice: "",
            // maxPrice: "",
            arrival_date_min: 0,
            binding_cover: [],
            book_condition: [],
            latestLast7Days: false,
            latestLast30Days: false,
            latestLast180Days: false,
            bindingPaperback: false,
            bindingHardcover: false,
            conditionBrandNew: false,
            conditionAlmostNew: false,
            conditionVeryGood: false,
            conditionReadable: false,
            LanguageHindi: false,
            LanguageEnglish: false,
            LanguageBengali: false,
            LanguageDutch: false,
            LanguageEnglishFrench: false,
            LanguageFrench: false,
            LanguageGerman: false,
            LanguageGujarati: false,
            LanguageKannada: false,
            LanguageTamil: false,
            LanguageTelugu: false,
            LanguageSanskrit: false,
            LanguageHindiAndEnglish: false,
            LanguageMarathi: false,
            LanguageForeignLanguage: false,
            filterFlag: 0
          });
        })
        .catch(err =>
          {console.log("in will err",
            err.response.status,
            `${config.get("apiDomain")}/api/v1/get${nextProps.ClickedCategory}${this.state.start}/`
          )
        
          if(err.response.status==404){
            // let CATID=GetcategoryID(this.newUrl)
            let CATID=GetcategoryID(this.state.ShowCategory)
            console.log(CATID,"GotCAtID")

            axios.get(`http://localhost:8000/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
            .then(res=>{
              console.log(nextProps,"next_ok",res.data,this.state.ShowCategory)
              this.setState({GetBookResult:res.data,ShowCategory:nextProps.ClickedCategory});
                          this.props.getSeoData(
              "https://www.mypustak.com" + window.location.pathname
            );
                        })
            .catch(err=>{console.log(err,"error in catch");
            }) }
        
        }
        );
      this.setState({ GetBookResult: [], start: 1 });
      window.scrollTo(0, 0);
    

  }
  }

  // onChangeMinPrice = e => {
  //   // alert(`${!isNaN(e.target.value)}`);
  //   if (!isNaN(e.target.value)) {
  //     this.setState({ minPrice: e.target.value });
  //   }
  // };
  // onChangeMaxPrice = e => {
  //   if (!isNaN(e.target.value)) {
  //     this.setState({ maxPrice: e.target.value });
  //   }
  // };

  handleChangeCheckBoxHindi = e => {
    // alert(e.target.checked)
    this.setState({ LanguageHindi: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };
  handelChanageCheckBoxTillToday =e =>{
    this.setState({latestLast180Days:false,latestLast30Days:false,latestLast7Days:false})
    setTimeout(()=>this.onClickFilter(),1)

  }
  handleChangeCheckBoxEnglish = e => {
    this.setState({ LanguageEnglish: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxBengali = e => {
    this.setState({ LanguageBengali: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxMarathi = e => {
    this.setState({ LanguageMarathi: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxTamil = e => {
    this.setState({ LanguageTamil: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxTelugu = e => {
    this.setState({ LanguageTelugu: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxFrench = e => {
    this.setState({ LanguageFrench: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxDutch = e => {
    this.setState({ LanguageDutch: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxSanskrit = e => {
    this.setState({ LanguageSanskrit: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxGujarati = e => {
    this.setState({ LanguageGujarati: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxGerman = e => {
    this.setState({ LanguageGerman: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxKannada = e => {
    this.setState({ LanguageKannada: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxHindiAndEnglish = e => {
    this.setState({ LanguageHindiAndEnglish: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxEnglishFrench = e => {
    this.setState({ LanguageEnglishFrench: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxForeignLanguage = e => {
    this.setState({ LanguageForeignLanguage: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxLast7Days = e => {
    this.setState({ latestLast7Days: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxLast30Days = e => {
    this.setState({ latestLast30Days: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxLast180Days = e => {
    this.setState({ latestLast180Days: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxPaperback = e => {
    this.setState({ bindingPaperback: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)   

  };

  handleChangeCheckBoxHardcover = e => {
    this.setState({ bindingHardcover: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxBrandNew = e => {
    this.setState({ conditionBrandNew: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxAlmostNew = e => {
    this.setState({ conditionAlmostNew: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxVeryGood = e => {
    this.setState({ conditionVeryGood: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  handleChangeCheckBoxReadable = e => {
    this.setState({ conditionReadable: e.target.checked });
    setTimeout(()=>this.onClickFilter(),1)

  };

  onClickClearFilter = () => {
    this.setState({
      GetBookResult: this.state.FilteredData,
      // minPrice: "",
      // maxPrice: "",
      arrival_date_min: 0,
      binding_cover: [],
      book_condition: [],
      latestLast7Days: false,
      latestLast30Days: false,
      latestLast180Days: false,
      bindingPaperback: false,
      bindingHardcover: false,
      conditionBrandNew: false,
      conditionAlmostNew: false,
      conditionVeryGood: false,
      conditionReadable: false,
      LanguageHindi: false,
      LanguageEnglish: false,
      LanguageBengali: false,
      LanguageDutch: false,
      LanguageEnglishFrench: false,
      LanguageFrench: false,
      LanguageGerman: false,
      LanguageGujarati: false,
      LanguageKannada: false,
      LanguageTamil: false,
      LanguageTelugu: false,
      LanguageSanskrit: false,
      LanguageHindiAndEnglish: false,
      LanguageMarathi: false,
      LanguageForeignLanguage: false,
      filterFlag: 0,
      start: 1
    });
  };
bindingcover=[]
bookcondition=[]
Language=[]
finaltimestamp=0
listTime=""
onClickFilter = () => {
  this.setState({ShowFilterLoader:true})
  let url = window.location.href;
  let data = url.split("/");
  let slug = data[data.length - 2];
  this.setState({ filterFlag: 1, start: 1 });
  let today = new Date();
  let bindingcover = [];
  let bookcondition = [];
  let minustimestamp, finaltimestamp;
  let timestamptoday = Math.floor(today.getTime() / 1000);
  let Language = [];
  if (this.state.latestLast180Days) {
    minustimestamp = 180 * 24 * 3600;
    finaltimestamp = timestamptoday - minustimestamp;

    // this.setState({ arrival_date_min: finaltimestamp });
  } else if (this.state.latestLast30Days) {
    minustimestamp = 30 * 24 * 3600;
    finaltimestamp = timestamptoday - minustimestamp;

    // this.setState({ arrival_date_min: finaltimestamp });
  } else if (this.state.latestLast7Days) {
    minustimestamp = 7 * 24 * 3600;
    finaltimestamp = timestamptoday - minustimestamp;

    // this.setState({ arrival_date_min: finaltimestamp });
  } else {
    finaltimestamp = 0;
    // this.setState({ arrival_date_min: 0 });
  }

  if (this.state.bindingPaperback) {
    bindingcover.push("Paperback");
    // this.setState({ binding_cover: bindingcover });
  }
  if (this.state.bindingHardcover) {
    bindingcover.push("Hardcover");
    // this.setState({ binding_cover: bindingcover });
  }

  if (this.state.conditionBrandNew) {
    bookcondition.push("BrandNew");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.conditionAlmostNew) {
    bookcondition.push("AlmostNew");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.conditionVeryGood) {
    bookcondition.push("VeryGood");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.conditionReadable) {
    bookcondition.push("AverageButInReadableCondition");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.LanguageHindi) {
    Language.push("Hindi");
  }

  if (this.state.LanguageEnglish) {
    Language.push("English");
  }

  if (this.state.LanguageTamil) {
    Language.push("Tamil");
  }

  if (this.state.LanguageTelugu) {
    Language.push("Telugu");
  }

  if (this.state.LanguageSanskrit) {
    Language.push("Sanskrit");
  }

  if (this.state.LanguageMarathi) {
    Language.push("Marathi");
  }

  if (this.state.LanguageGujarati) {
    Language.push("Gujarati");
  }

  if (this.state.LanguageKannada) {
    Language.push("Kannada");
  }

  if (this.state.LanguageFrench) {
    Language.push("French");
  }

  if (this.state.LanguageDutch) {
    Language.push("Dutch");
  }

  if (this.state.LanguageGerman) {
    Language.push("German");
  }

  if (this.state.LanguageHindiAndEnglish) {
    Language.push("Hindi & English");
  }

  if (this.state.LanguageBengali) {
    Language.push("Bengali");
  }

  if (this.state.LanguageEnglishFrench) {
    Language.push("English(French)");
  }

  if (this.state.LanguageForeignLanguage) {
    Language.push("Foreign Language");
  }


  this.setState({
    book_condition: bookcondition,
    binding_cover: bindingcover,
    arrival_date_min: finaltimestamp,
    language: Language
  });

  let body = {
    // minPrice: this.state.minPrice,
    // maxPrice: this.state.maxPrice,
    binding: bindingcover,
    condition: bookcondition,
    date_min: finaltimestamp,
    date_max: timestamptoday,
    language: Language
  };

    console.log("Body First ", body);
    let CATID=GetcategoryID(this.newUrl)
    console.log(CATID,"GotCAtID")
    // http://localhost:8000/api/v1/filter/price/others/1
    axios
      .post(`http://localhost:8000/api/v1/filter/${CATID.id}/1`, body)
      .then(res => {
        console.log("in onClickFilter And the Data are ", res.data.Books);
        this.setState({ GetBookResult: res.data.Books,ShowFilterLoader:false });
      })
      .catch(err => {console.log("err", err)
      this.setState({ShowFilterLoader:false})

      });
  };

 CheckBinding=(binding)=>{
    try {
      if(this.state.FilterToBeApplid.binding.includes(binding)){
        console.log("True Try",binding,this.state.FilterToBeApplid);
        
        return true
      }else
      {
       console.log("False Try",binding);

        return false
      }
    } catch (error) {
     console.log("False Catch",binding);

     return false
      
    }
  }

  CheckLanguage=(language)=>{
    try {
      if(this.state.FilterToBeApplid.language.includes(language)){
        console.log("True Try",language,this.state.FilterToBeApplid);
        
        return true
      }else
      {
       console.log("False Try",language);

        return false
      }
    } catch (error) {
     console.log("False Catch",language);

     return false
      
    }
  }

  CheckBookCondition=(bookCondition)=>{
    try {
      if(this.state.FilterToBeApplid.condition.includes(bookCondition)){
        console.log("True Try",bookCondition,this.state.FilterToBeApplid);
        
        return true
      }else
      {
       console.log("False Try",bookCondition);

        return false
      }
    } catch (error) {
     console.log("False Catch",bookCondition);

     return false
      
    }
  }
  render() {
     const seoData = this.props.SeoData;

    let meta = {
      title: seoData.title_tag,
      description: seoData.title_tag,
      meta: {
        charset: "utf-8",
        name: {
          title: seoData.title_tag,
          description: seoData.title_tag
        }
      }
    };

    const fetchBooks = () => {
      // console.log(this.newUrl,"FetchBooks");
      this.setState({ start: this.state.start + 1 });

      console.log(this.state.start);

      if (this.state.filterFlag == 1) {
        // let url = window.location.href;
        // let data = url.split("/");
        // let slug = data[data.length - 2];
        let body = {
          // minPrice: this.state.minPrice,
          // maxPrice: this.state.maxPrice,
          binding: this.state.binding_cover,
          condition: this.state.book_condition,
          date_min: this.state.arrival_date_min,
          date_max: this.state.arrival_date_max,
          language: this.state.language
        };
        const url = window.location.href;
        //  urlLength = this.url.length
        const CatIndex = url.indexOf("/category/");
        const newUrl = url.slice(CatIndex);
        console.log(`${newUrl}`,"Url");
        let CATID=GetcategoryID(newUrl)
        console.log(CATID,"GotCAtID inFetchBooks Filter")
        axios
          .post(
            `http://localhost:8000/api/v1/filter/${CATID.id}/${
              this.state.start
            }`,
            body
          )
          .then(res => {
            console.log("in onClickFilter And the Data are ", res.data.Books,`${this.state.start}`);
            this.setState({
              GetBookResult: this.state.GetBookResult.concat(res.data.Books),
              filterFlag: 1
            });
          })
          .catch(err =>
            console.log("No more data are present on these filters")
          );
      }
      else {
        const url = window.location.href;
        //  urlLength = this.url.length
        const CatIndex = url.indexOf("/category/");
        const newUrl = url.slice(CatIndex);
        console.log(`${newUrl}`,"Url");
        let CATID=GetcategoryID(newUrl)
        console.log(CATID,"GotCAtID inFetchBooks")
        
        axios
          // .get(
          //   `${config.get("apiDomain")}/api/v1/get${this.newUrl}${
          //     this.state.start
          //   }/`
          // )
          .get(`http://localhost:8000/api/v1/get/category/${CATID.id}/${this.state.start}`)
          .then(res => {
            this.setState({
              GetBookResult: this.state.GetBookResult.concat(res.data.Books),
              filterFlag: 0
            });
          })
          .catch(err => console.log(err,"e",this.state.ShowCategory,"errorInFetch", `${config.get("apiDomain")}/api/v1/get${this.state.ShowCategory}${
            this.state.start
          }/`));
        // alert(`${config.get('apiDomain')}/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
      }
    };
    // console.log(this.state.ShowCategory,"GetStateCAt",this.state.GetBookResult,"GETBook");
    
    let CategoryUrl= this.state.ShowCategory
    let ReplacedCategoryUrl= CategoryUrl.replace( /[/]/g, "," );
    let ResultUrl=ReplacedCategoryUrl.split(',')
    // ResultUrl=ResultUrl.pop();
    console.log(ResultUrl,"ResultUrl",ResultUrl.length);
    let ListOfLinks=[]
          ResultUrl.forEach((element,index) => {
            // console.log(element,index);
            if(element!=="category" && index!==1  && index!==5 ){
              if(index == 2 ){
                ListOfLinks.push(<span><a href={`/category/${element}/`} style={(this.state.ChangeBreadCrumLink1)?{
                  color:'#2874f0',
                  marginRight:'1%',
                  textDecoration:'None'
                }:
              {
                color:'grey',
                marginRight:'1%',
                textDecoration:'None'
              }
              }
                onMouseEnter={()=>this.setState({ChangeBreadCrumLink1:!this.state.ChangeBreadCrumLink1})}
                onMouseLeave={()=>this.setState({ChangeBreadCrumLink1:!this.state.ChangeBreadCrumLink1})}
                
                >{element} </a></span>)
              }else{
                if(index == 3 && ResultUrl.length > 4){
                    // console.log(this.state.ShowCategory.lastIndexOf(element),33,this.state.ShowCategory.length,);
                    var links = this.state.ShowCategory.split('/')
                    var onclickLinkThird=""
                    onclickLinkThird.concat(`${links[3]}`)
                    // console.log(onclickLinkThird,"Thi",links[3]);
                    ListOfLinks.push(<span>> <a href={`/category/${links[2]}/${links[3]}/`} style={(this.state.ChangeBreadCrumLink2)?{
                      color:'#2874f0',
                      marginRight:'1%',
                      textDecoration:'None'
                    }:
                  {
                    color:'grey',
                    marginRight:'1%',
                    textDecoration:'None'
                  }
                  }
                    onMouseEnter={()=>this.setState({ChangeBreadCrumLink2:!this.state.ChangeBreadCrumLink2})}
                    onMouseLeave={()=>this.setState({ChangeBreadCrumLink2:!this.state.ChangeBreadCrumLink2})}
                  >{element}</a></span>)
                }else{
                
                }
                if(index == 4 && ResultUrl.length > 5){
                    ListOfLinks.push(<span>> <a href={this.state.ShowCategory}  style={(this.state.ChangeBreadCrumLink3)?{
                      color:'#2874f0',
                      marginRight:'1%',
                      textDecoration:'None'
                    }:
                  {
                    color:'grey',
                    marginRight:'1%',
                    textDecoration:'None'
                  }
                  }
                    onMouseEnter={()=>this.setState({ChangeBreadCrumLink3:!this.state.ChangeBreadCrumLink3})}
                    onMouseLeave={()=>this.setState({ChangeBreadCrumLink3:!this.state.ChangeBreadCrumLink3})}
                    
                    >{element} </a></span>)
                }

              }

          }
          });
      // let ListOfLinks=[]
    const handlePriceChange = ( newValue) => {
      // setValue(newValue);
      // console.log(newValue,"new Value");
    };
    function valuetext(value) {
      return `${value}°C`;
    }
     console.log(this.state.GetBookResult,"Result");
    //  const CheckBinding=(binding)=>{
    //    try {
    //      if(this.state.FilterToBeApplid.binding.includes(binding)){
    //        console.log("True Try",binding);
           
    //        return true
    //      }else
    //      {
    //       console.log("False Try",binding);

    //        return false
    //      }
    //    } catch (error) {
    //     console.log("False Catch",binding);

    //     return false
         
    //    }
    //  }
    // const useStyles = makeStyles(theme => ({
    //   progress: {
    //     margin: theme.spacing(2),
    //   },
    // }));
    // const classes = useStyles();
    return (
      <div>
        {/* {alert("Hello world")} */}
        <MainHeader />
        <MediaQuery maxWidth={991}>
          <Mobilecat />
        </MediaQuery>
        {/* <DocumentMeta {...meta} /> */}
        <Helmet  bodyAttributes={{style: 'background-color : aliceblue'}}>
          <title>{seoData.title_tag}</title>
          <meta
            name="Description"
            property="og:description"
            content={seoData.meta_desc}
          />
          <meta
            name="og_title"
            property="og:title"
            content={seoData.title_tag}
          />
          <meta
            name="og_url"
            property="og:url"
            content={window.location.href}
          />
        </Helmet>
        <MediaQuery minWidth={992}>
          <div
            style={{
              height: '99vh',
              width: '21%',
              marginTop: 8,
              float: "left",
              backgroundColor: "white",
              padding: 10,
              // position: "sticky",
              top: 0,
              // overflow: "scroll"
              // border:'4px solid aliceblue'
            }}
          >
            <h4 style={{ color: "dodgerblue", textAlign: "center" }}>Filters</h4>

            <form noValidate>
             
              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                LATEST ARRIVALS
              </FormLabel>

              {/* <div style={{ }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      name="latestLast7Days"
                      // value="checkedA"
                      checked={this.listTime === "latestLast7Days"}
                      onChange={this.onClickFilter}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Last 7 Days"
                  style={{
                    height: 20
                  }}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.listTime === "latestLast30Days"}
                      name="latestLast30Days"
                      onChange={this.onClickFilter}
                    />
                  }
                  label="Last 30 Days"
                  style={{
                    height: 20
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.listTime === "latestLast180Days"}
                      onChange={this.onClickFilter}
                      name="latestLast180Days"
                    />
                  }
                  label="Last 180 Days"
                  style={{
                    height: 20
                  }}
                />
              </div> */}
              <div>
                <FormControlLabel
                  control={
                    
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }} 
                    onChange={this.handleChangeCheckBoxLast7Days}  
                    value="latestLast7Days"
                    />
                  }
                  label="Last 7 Days"
                  style={{ width:'80%' }}
                />

                <FormControlLabel
                  control={
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }}
                    onChange={this.handleChangeCheckBoxLast30Days}      
                    value="latestLast30Days"

                    />
                 
                  }
                  label="Last 30 Days"
                  style={{ width:'80%' }}

                />

                <FormControlLabel
                  control={
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }} 
                    onChange={this.handleChangeCheckBoxLast180Days}  
                    value="latestLast180Days"

                    />
                    
                  }
                  label="Last 180 Days"
                  style={{ width:'80%' }}

                />

                <FormControlLabel
                  control={
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }} 
                    onChange={this.handelChanageCheckBoxTillToday}  
                    value="tilltoday"

                    />
                    
                  }
                  label="Till Today"
                  style={{ width:'80%' }}

                />
              </div>

         

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                BINDING COVER
              </FormLabel>
              {this.state.FilterToBeApplid?
              <div style={{ display:'flex' }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      // value={true}
                      name="bindingPaperback"
                      checked={this.state.bindingPaperback}
                      // onClick={()=>this.setState({bindingPaperback:!this.state.bindingPaperback})}
                      onChange={this.handleChangeCheckBoxPaperback}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Paperback"
                  style={this.CheckBinding("Paperback")?{ height:20 }:{ display:'none' }}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.state.bindingHardcover}
                      name="bindingHardcover"
                      checked={this.state.bindingHardcover}
                      onChange={this.handleChangeCheckBoxHardcover}
                    />
                  }
                  label="Hardcover"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckBinding("Hardcover")?{ height:20 }:{ display:'none' }}
                />
              </div>:
            <div>
              <p>Fetching Available Bindings...</p>
            </div>}

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                BOOK CONDITION
              </FormLabel>
              {this.state.FilterToBeApplid?
              <React.Fragment>
              <div style={{ display:'flex'  }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.state.conditionBrandNew}
                      name="BrandNew"
                      onChange={this.handleChangeCheckBoxBrandNew}
                    />
                  }
                  label="Brand New"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckBookCondition("BrandNew")?{height: 20,width:'47%'}:{ display:'none' }}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.state.conditionAlmostNew}
                      name="AlmostNew"
                      onChange={this.handleChangeCheckBoxAlmostNew}
                    />
                  }
                  label="Almost New"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckBookCondition("AlmostNew")?{height: 20,width:'47%'}:{ display:'none' }}
                  
                />
                </div>
              <div style={{ display:'flex' }}>

                <FormControlLabel
                  control={
                    <Checkbox
                      // value="checkedA"
                      checked={this.state.conditionVeryGood}
                      name="VeryGood"
                      onChange={this.handleChangeCheckBoxVeryGood}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Very Good"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckBookCondition("VeryGood")?{height: 20,width:'47%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.state.conditionReadable}
                      name="Readable"
                      onChange={this.handleChangeCheckBoxReadable}
                    />
                  }
                  label="Readable"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckBookCondition("AverageButInReadableCondition")?{height: 20,width:'47%'}:{ display:'none' }}

                />
              </div>
              </React.Fragment>
              :
              <div>
                <p>
                  Fetching Available Language ...
                </p>
              </div>
            }

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                LANGUAGES
              </FormLabel>
              {this.state.FilterToBeApplid?
                <React.Fragment>
              <div style={{display:'flex',flexFlow:'row wrap', }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("Hindi")}
                      checked={this.state.LanguageHindi}
                      name="Hindi"
                      onChange={this.handleChangeCheckBoxHindi}
                    />
                  }
                  label="Hindi"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Hindi")?{height: 20,width:'45%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("English")}
                      // checked={this.state.LanguageEnglish}
                      name="English"
                      onChange={this.handleChangeCheckBoxEnglish}
                    />
                  }
                  label="English"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("English")?{height: 20,width:'45%'}:{ display:'none' }}
                  
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value={this.Language.includes("Marathi")}
                      checked={this.state.LanguageMarathi}
                      onChange={this.handleChangeCheckBoxMarathi}
                      name="Marathi"
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Marathi"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Marathi")?{height: 20,width:'45%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("English(French)")}
                      checked={this.state.LanguageEnglishFrench}
                      name="English(French)"
                      onChange={this.handleChangeCheckBoxEnglishFrench}
                    />
                  }
                  label="English(French)"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("English(French)")?{height: 20,width:'45%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("Bengali")}
                      checked={this.state.LanguageBengali}
                      name="Bengali"
                      onChange={this.handleChangeCheckBoxBengali}
                    />
                  }
                  label="Bengali"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Bengali")?{height: 20,width:'45%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("Sanskrit")}
                      checked={this.state.LanguageSanskrit}
                      name="Sanskrit"
                      onChange={this.handleChangeCheckBoxSanskrit}
                    />
                  }
                  label="Sanskrit"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Sanskrit")?{height: 20,width:'45%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value={this.Language.includes("Dutch")}
                      checked={this.state.LanguageDutch}
                      name="Dutch"
                      onChange={this.handleChangeCheckBoxDutch}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Dutch"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Dutch")?{height: 20,width:'45%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("Gujarati")}
                      checked={this.state.LanguageGujarati}
                      name="Gujarati"
                      onChange={this.handleChangeCheckBoxGujarati}
                    />
                  }
                  label="Gujarati"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Gujarati")?{height: 20,width:'45%'}:{ display:'none' }}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("Telugu")}
                      checked={this.state.LanguageTelugu}
                      name="Telugu"
                      onChange={this.handleChangeCheckBoxTelugu}
                    />
                  }
                  label="Telugu"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Telugu")?{height: 20,width:'45%'}:{ display:'none' }}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("German")}
                      checked={this.state.handleChangeCheckBoxGerman}
                      name="German"
                      onChange={this.onClickFilter}
                    />
                  }
                  label="German"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("German")?{height: 20,width:'45%'}:{ display:'none' }}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value={this.Language.includes("Kannada")}
                      checked={this.state.LanguageKannada}
                      name="Kannada"
                      onChange={this.handleChangeCheckBoxKannada}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Kannada"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Kannada")?{height: 20,width:'45%'}:{ display:'none' }}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("French")}
                      checked={this.state.LanguageFrench}
                      name="French"
                      onChange={this.handleChangeCheckBoxFrench}
                    />
                  }
                  label="French"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("French")?{height: 20,width:'45%'}:{ display:'none' }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("Tamil")}
                      checked={this.state.LanguageTamil}
                      name="Tamil"
                      onChange={this.handleChangeCheckBoxTamil}
                    />
                  }
                  label="Tamil"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Tamil")?{height: 20,width:'45%'}:{ display:'none' }}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      value={this.Language.includes("Hindi & English")}
                      checked={this.state.LanguageHindiAndEnglish}
                      name="Hindi & English"
                      onChange={this.onClickhandleChangeCheckBoxHindiAndEnglishilter}
                    />
                  }
                  label="Hindi & English"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Hindi & English")?{height: 20}:{ display:'none' }}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      value={this.Language.includes("Foreign Language")}
                      checked={this.state.LanguageForeignLanguage}
                      name="Foreign Language"
                      onChange={this.handleChangeCheckBoxForeignLanguage}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Foreign Language"
                  style={!this.state.FilterToBeApplid?{ display:'none' }:this.CheckLanguage("Foreign Language")?{height: 20,width:'45%'}:{ display:'none' }}

                />
              </div>
              </React.Fragment>
              :
            <div>
              <p>
                Fetching Available Languages ...
              </p>
            </div>
            }

              <div>
                {/* <Button
                  variant="contained"
                  color="primary"
                  margin="normal"
                  style={{
                    marginLeft: "theme.spacing(1)",
                    marginRight: "theme.spacing(1)",
                    width: 80,
                    display: "flex",
                    flexWrap: "wrap",
                    margintop: "theme.spacing(1)",
                    float: "left"
                  }}
                  onClick={this.onClickFilter}
                >
                  Apply
                </Button> */}
                {/* <Button
                  variant="contained"
                  color="primary"
                  margin="normal"
                  style={{  }}
                    marginLeft: "theme.spacing(1)",
                    marginRight: "theme.spacing(1)",
                    width: 80,
                    display: "flex",
                    flexWrap: "wrap",
                    margintop: "theme.spacing(1)",
                    float: "right"
                  }}
                  onClick={this.onClickClearFilter}
                >
                  Clear
                </Button> */}
              </div>
            </form>
          </div>
        </MediaQuery>

        <div>
          {/* {console.log(Link)} */}
          <Breadcrumbs
            separator={<b> / </b>}
            item={Link}
            finalItem={"b"}
            finalProps={{
              style: { color: "red" }
            }}
          />
        </div>
        {/* Desktop */}
        <MediaQuery minWidth={992}>
          <div id="categoryBody">
          <div id="BreadCrumb" style={{ marginLeft:'2%' }}>
          {ListOfLinks}

          </div>

            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
            {/* ----------------------------Popup Shown on Slecting Filter */}
          <Popup
          open={this.state.ShowFilterLoader}
          closeOnDocumentClick={false}
          overlayStyle={{ 
            width:"100%",
            height:"90%",
            backgroundColor:'rgba(255, 255, 255, 0.5)',
            marginTop:'5%',
            // opacity:'0.8',

           }}
           contentStyle={{ 
            width: '5%',
            borderWidth:'0px',
            backgroundColor:'transparent'
            }}
          >
              <div>
              <CircularProgress style={{ margin: 'spacing(2)',color:'primary',opacity:'1' }}/>
              </div>
              
          </Popup> 

        </MediaQuery>
        {/* Tab */}
        <MediaQuery minWidth={768} maxWidth={991}>
          <div id="categoryBody">
            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (
                  //  console.log(bookdata);

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
        </MediaQuery>
        <MediaQuery maxWidth={767} minWidth={539}>
          <div id="categoryBody">
            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (
                  //  console.log(bookdata);

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
        </MediaQuery>
        <MediaQuery maxWidth={538}>
          <div id="categoryBody">
            {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
            <div style={{ color: "black" }}>
              <InfiniteScroll
                dataLength={this.state.GetBookResult.length}
                scrollThreshold={"70%"}
                next={fetchBooks}
                hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
              >
                {this.state.GetBookResult.map(bookdata => (
                  //  console.log(bookdata);

                  <GBook key={bookdata.slug} book={bookdata} />
                ))}
                {/* {this.state.GetBookResult.map((bookdata)=>
                 ((bookdata.document.is_out_of_stack !== `Y`)?
                <SBook key={bookdata.document.title} 
                    book={bookdata.document}
                    />:null  
                 ))
                 } */}
              </InfiniteScroll>
            </div>
            {/* ------------------------------------------------------------------------ */}
          </div>
        </MediaQuery>
        <MainFooter />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  ClickedCategory: state.homeR.ClickedCategory,
  SeoData: state.seodata.seodata
});
// export default  Category
export default connect(
  mapStateToProps,
  { getSeoData }
)(Category);
