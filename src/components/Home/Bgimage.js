import React, { Component } from 'react';
import Popup from 'reactjs-popup' 
import axios from 'axios'
import './login.css';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import {doSearch} from '../../actions/homeAction'
// import {logout } from '../../actions/accountAction'
import {AddToCart,CartopenModal,CartcloseModal,removeAllCart,RemoveCart,CartSession,ToBeAddedToCart} from '../../actions/cartAction'
import {logout,Getaddress,userdetails,getSavedToken,} from '../../actions/accountAction'
import { connect } from 'react-redux';
import UserLogin from './UserLogin'
import UserSignup from './UserSignup';
import config from 'react-global-configuration'
import Loader from 'react-loader-spinner'
import Popupcat from './Popupcategories'
import {BreadcrumbsItem} from 'react-breadcrumbs-dynamic'
// import del from '../product/Product_Images/del.png';
import ReactGA from 'react-ga';

class Bgimage extends Component {
  state = {
    fetched:false,
    open:false,
    Signupopen:false,
    value:'',
    showLS:false,
    name:'',
    ErrMsg:this.props.ErrMsg,
    Category:false,
    showPopup: false,
    succesLogin:false,
    uname:'',
    toRedirect:false,
    goTocart:false,
    RedirectCart:false,
    // searchBook:'',
    CartSessionCall:true,
    Order_track_popup: false,
    Order_track_popup_status:"",
    // Token:this.pr
  }

  componentDidMount(){
    ReactGA.pageview(window.location.pathname + window.location.search);

    window.scrollTo(0, 0);
    // if(localStorage.getItem('user') !== null){
    const value =localStorage.getItem('user');
    // alert("ok")
    this.props.getSavedToken(value);
  // }
  this.setupBeforeUnloadListener();
  if(localStorage.getItem('user') !== null){
    const token= localStorage.getItem('user')
    const details=`Token ${token}`
    this.props.CartSession(details)
    // axios.get(`${config.get('apiDomain')}/common/cart_items/`,{headers:{
    //   'Authorization': "Token 3f27c209c5377bcad7e7da67e16f5126dabc3813"
    //   }} 
    //   ).then(res=>{
    //           console.log(res)
              
    //   }).catch(err=>console.log(err)
    //   )

    
  }else{
    this.props.cartDetails.map(book=>(
      this.props.ToBeAddedToCart({"book_id" : book.book_id,"book_inv_id":book.bookInvId[0]})
    ))
    console.log("No");
    
  }
  this.setState({CartSessionCall:true})

  }


  componentWillReceiveProps(nextProps){

    if(nextProps.userToken !== this.props.userToken ){
      const details=`Token ${localStorage.getItem('user')}`
      this.props.userdetails(details)
      // const details=`Token ${nextProps.userToken}`
      sessionStorage.clear();
      try {
      this.props.ToBeAddedToCartState.map(book=>{
        const sendCartSession = { 
          "book_id" : book.book_id,
          "book_inv_id":book.book_inv_id,
          'content-type': 'application/json',
           }
        axios.post(`${config.get('apiDomain')}/common/addtocart/`,sendCartSession,{headers:{
        'Authorization': details,
        }} )
      .then(res=>{
      //   console.log(res.status,sendCartSession);
      // console.log("In Main Header Succes");

        RefreshCart();
        })
        .catch(err=>{
          // console.log(err);
          // console.log(sendCartSession);
        }
        )

        const RefreshCart=()=>{
          const token= localStorage.getItem('user')
          const details=`Token ${token}`
          this.props.CartSession(details)
        }

      })  
        } catch (error) {
        // console.log(error);
        
      }
      // console.log("Refreshing Header");

      this.props.CartSession(details)

    }
    if(this.props.CartSessionData.length !== nextProps.CartSessionData.length){
      // console.log(nextProps.CartSessionData.length);
      if(localStorage.getItem('user') != null){
        // console.log("Doing Refresh");
        
      nextProps.CartSessionData.map((book)=>{
        console.log(book,"Form Db")
        if(book != 0){
          const MyCart ={
          Cart_id:book.cart_id,
          bookId:book.book_id,
          bookName:book.title,
          bookSlug:book.slug,
          bookPrice:Math.round(book.price),
          bookShippingCost:Math.round(book.shipping_cost),
          bookThumb:book.thumb,
          // bookDonor:DonorName,
          bookQty:1,
          bookCond:book.condition,
          bookRackNo:book.rack_no,
          bookInvId:book.book_inv_id,
          // bookDonner
          }
          // Count+=1
          // console.log(MyCart);
        this.props.AddToCart(MyCart)          
          try {
        // console.log(book,"Form Db To Local")
              
            sessionStorage.setItem(book.book_inv_id,JSON.stringify(MyCart));


          } catch (error) {
          }

          if(sessionStorage.length !==0){
            this.props.removeAllCart()
         for (var i=0; i<=sessionStorage.length-1; i++)  
         {   
          if(sessionStorage.key(i) !== "UserOrderId"){
    
             let key = sessionStorage.key(i);  
             try {
              let val = JSON.parse(sessionStorage.getItem(key));
              // console.log(val.bookInvId  )
              // val.map(det=>console.log(det))
              
              this.props.AddToCart(val)
              // alert(val)
             } catch (error) {
              //  console.log(error);
               
             }
    
          }
         } }
        }
      }
        )
      }else{
        console.log("Not Login");
        
      }
  }
  }


  // componentWillReceiveProps(userToken){
  //   if(userToken.userToken !== this.state.Token ){
  //     const details=`Token ${userToken.userToken}`
  //     this.props.CartSession(details)

  //   // this.props.CartSession(userToken.userToken)
  //     // this.props.Getwalletd(details)
  //   }
  // }
  
  setupBeforeUnloadListener = () => {
    window.addEventListener("beforeunload", (ev) => {
        // ev.preventDefault();
        // sessionStorage.clear();
    });
};
RemoveFromCart=(bookInvId,Cart_id)=>{

  for (var i=0; i<=sessionStorage.length-1; i++)  
  {   
    // alert("rmo")
      let key = sessionStorage.key(i);  
      try {
        if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){
          let val = JSON.parse(sessionStorage.getItem(key));
          if(`${val.bookInvId}` === `${bookInvId}`)  {
            // alert("rm")
            sessionStorage.removeItem(bookInvId);
          }
      }
      } catch (error) {
        
      }

      // val.map(det=>console.log(det))
      // this.props.AddToCart(val)
      // alert(val)
  }
  if(localStorage.getItem('user') === null){

    this.props.removeFromCartLogout(bookInvId)
    }else{

      const data={"is_deleted":"Y"}
    this.props.RemoveCart(Cart_id,bookInvId,data)

    }
 }
  changeShowLS=()=>{
    this.setState({showLS:!this.state.showLS})
  }

  toDonation()
  {
    // alert("okk")
    // this.props.history.push = `/donate`
    // <Redirect to="/dashboard"/>
  }
  toAboutUs()
  {
    // alert("okk")
    // window.location.href = `/pages/about-us`
  }
  openModal=()=>{
    this.setState({ open: true })
  }
  closeModal=()=>{
    this.setState({ open: false ,Order_track_popup: false})
  }
  OPENCategoryModal=()=>{
    this.setState({Category:true})
  }
  CLOSECategoryModal=()=>{
    this.setState({Category:false})
  }

  signupOpenModal=()=>{
    // alert("O")
    this.setState({ Signupopen: true })
  }
  signupCloseModal=()=>{
    // alert('c')
    this.setState({ Signupopen: false })
  }
  scloseModal=()=>{
    // alert("col")
    // this.setState({ Sopen: false })
}
successLoginModal=()=>{
  this.setState({ succesLogin: false })
}
togglePopup() {
  this.setState({
    showPopup: !this.state.showPopup
  });
}
  order_id = this.state.value
  Userlogout=()=>{
    sessionStorage.clear();
    localStorage.clear();
    this.props.removeAllCart();
    localStorage.removeItem('user');
    this.signupCloseModal()
    this.props.logout();
    // window.location.href = ''
    this.setState({Sopen:false})
    // return <Redirect to =''></Redirect>
  }
  GetTrack=(e,order_id)=>{
// alert(this.state.value)
    const passdata = {
      data :
      {
     "ref_id" : this.state.value,
      }
  }
    e.preventDefault();
    // console.log(passdata) 
    // http://103.217.220.149:80/api/v1/post/get_ordertrack_url
    if(this.state.value.length > 0){
    axios.post(`${config.get('apiDomain')}/api/v1/post/get_ordertrack_url`,passdata   
    // axios.post('http://127.0.0.1:8000/api/v1/post/get_ordertrack_url',passdata 
    ).then(res=>{
      // console.log(`${res.data} search book`);
      // console.log(res);
      
       window.location.href=res.data.output
        
    }).catch(err=>{
      this.setState({Order_track_popup_status:err.response.data.message, Order_track_popup:true})
      console.log(err.response.data);})
    }
};
// 
DoSearch=(searchValue)=>{
  axios.get(`${config.get('apiDomain')}/search/${searchValue}/`)
  // axios.get(`http://127.0.0.1:8000/search/${searchValue}/`)
  .then(res=>res.data)
  .catch(err=>
    console.log()
  )
}
getName(){
  axios.get(`${config.get('apiDomain')}/core/user_details/ `,{headers:{

      'Authorization': `Token ${this.props.userToken}`
    }})
    .then(res=>this.setState({name:res.data.name}))
    .catch(err=>
      console.log()
    )  
  }


  onChange = e => this.setState({[e.target.name]:e.target.value})
  onSearchChange = e => {
    this.props.doSearch(e.target.value)
    this.DoSearch(e.target.value)
    }

    After_Login =<li onClick={this.sopenModal}><a> Sign in </a></li>
    LOG_IN=<UserLogin scloseModal={this.signupCloseModal} changeShowLS={this.changeShowLS} ShowMsg={this.props.ErrMsg}/>
    SIGN_IN=<UserSignup scloseModal={this.signupCloseModal} changeShowLS={this.changeShowLS}/>

  render() {
    const {CartSessionData}= this.props
    if(this.props.userToken !== null){
      localStorage.setItem('user', this.props.userToken)
    }
    const CheckLogin=()=>{
      if(this.props.userToken === null){
       this.signupOpenModal()
       this.setState({goTocart:true})
       // if(this.props.userToken !== null){
       //   this.setState({RedirectCart:true})
       // }
     }else{
       this.setState({RedirectCart:true})
     }
     }
     if(this.state.RedirectCart){
      // alert("redirect")
    return  <Redirect to='/view-cart'/>;
    }
    if(this.state.goTocart === true && this.props.userToken !== null){
      return  <Redirect to='/view-cart'/>;
    }

    if(CartSessionData.length !== 0 && this.state.CartSessionCall && this.props.userToken !== null){  
      var Count =1
      CartSessionData.map((book)=>{
      // console.log(book.length)
      if(book != 0){
        const MyCart ={
          cartId:book.cart_id,
        bookId:book.book_id,
        bookName:book.title,
        bookSlug:book.slug,
        bookPrice:Math.round(book.price),
        bookShippingCost:Math.round(book.shipping_cost),
        bookThumb:book.thumb,
        // bookDonor:DonorName,
        bookQty:1,
        bookCond:book.condition,
        bookRackNo:book.rack_no,
        bookInvId:book.book_inv_id,
        // bookDonner
        }
        Count+=1
        // console.log(MyCart);
        
        try {
          sessionStorage.setItem(book.book_inv_id,JSON.stringify(MyCart));
        } catch (error) {
        }
      }
    }
      )

      // const MyCart ={
      //   bookId:book.book_id,
      //   bookName:book.title,
      //   bookSlug:this.state.bookSlug,
      //   bookPrice:Math.round(book.price),
      //   bookShippingCost:Math.round(SHIPPINGCOST),
      //   // bookShippingCost:Math.round(SHIPPING),
      //   bookThumb:book.thumb,
      //   bookDonor:DonorName,
      //   bookQty:TOTALQUANTITY,
      //   bookCond:this.state.SelectCond,
      //   bookRackNo:RACK_NO,
      //   bookInvId:BOOK_INV_ID
      //   // bookDonner
      // }

      try {
        // sessionStorage.setItem(BOOK_INV_ID,JSON.stringify(MyCart));
        
      } catch (error) {
        
      }
      this.setState({CartSessionCall:false})

    }

    
    if(this.props.searchBook.length !== 0){
    return  <Redirect to='/search'/>;
    }
    // if(this.state.showLS === false){
    //   var  LorS=<UserLogin scloseModal={this.scloseModal}/>
    // }
    // console.log(this.props.userToken);
    
    if(this.props.userToken !== null && this.state.name ==='' )
    {

      this.getName()
      // alert("name")
    //  this.setState({sopen:false}) 
    if(this.state.name !=='')
    {
    this.scloseModal()}
    }
    const {cartDetails} = this.props
    var TotalShipCost =0
    cartDetails.map(cart=> TotalShipCost+=Number(cart.bookShippingCost) )
    return (
      <div>
        <BreadcrumbsItem to='/'>Main Page</BreadcrumbsItem>
          <div className="bgimage">
          <span className="menu">
          <div className="leftmenu">
          </div>
          <div className="centermenu">
          <ul id="bgTopMostNav">
            <li style={{ marginTop:'-.5%',marginLeft:'9%' }}><Link style={{ textDecoration:'none',color:'white' }} to='/contact-us'>Help</Link></li>
          <li style={{ marginTop:'-.5%' }}  ><Link to="/pages/about-us"  style={{ textDecoration:'none',color:'white' }} >About Us</Link></li>
          <Popup
          trigger={<li style={{ marginTop:'-.5%' }} onClick={this.openModal}><a>Track Order
          {/* <i className="fas fa-truck-moving"></i> */}
          </a></li>}
          on='click'

          // open={this.state.open}
          // on="hover"
          position="bottom center"
          closeOnDocumentClick
          onClose={this.closeModal}
          contentStyle={{
                        width:'20%',
                        marginTop:'-1%'
                        }} 
        >
          <div style={{ color:'black' }} >
            <p style={{ textAlign:'center',fontWeight:'bolder' ,fontSize:'110%'}}>Track Your Order Here</p>
            <form onSubmit={this.GetTrack}>
              <label style={{ marginLeft:'10%' }}> Enter Order ID/Tracking ID</label>
              <input type="text" style={{ border:'1px solid #828282',width:'76%',marginLeft:'10%',height:'30px',fontSize:'80%',color:'#828282',borderRadius:'5px'}}
              placeholder="Enter Your Order ID/Tracking ID Here"
              value={this.state.value}
              onChange={this.onChange}
              name="value"
              />
            
              <input style={{ backgroundColor:'#00baf2',color:'white',borderRadius:'5px',border:'none',
                  width:'22%',marginLeft:'36%',marginTop:'2%' 
            }} onClick={this.GetTrack} type="submit" value="Track"/>
            </form>
          </div>
        </Popup>

        <Popup
          // on={this.state.Order_track_popup}

          open={this.state.Order_track_popup}
          // on="hover"
          position="bottom center"
          closeOnDocumentClick
          onClose={this.closeModal}
          contentStyle={{
                        width:'27%',
                        height:'27%',
                        marginTop:"10%",
                        backgroundColor:"white"
                        }} 
        >
          <div style={{ color:'black' , margin:'2%'}} >
            <p style={{ textAlign:'center'}}>{`${this.state.Order_track_popup_status}. For further query, kindly contact to our customer support or write us at `}<span style={{color:"red"}}>support@mypustak.com</span></p>
            
          </div>
        </Popup>
        </ul>

</div>


<div className="rightmenu">
<ul style={{ marginLeft:'5%' }}>
{/* <li onClick={this.sopenModal}><a> {  (this.props.userToken === '')?" Sign in ":`Hello ${this.state.name}`}</a> */}
{ (this.props.userToken === null)?
//  *********************************Popup for the Login an Sign up************************************
<li id="logSign" ><a onClick={this.signupOpenModal}>Log In/Sign Up</a>
<Popup
          open={this.state.Signupopen}
          closeOnDocumentClick={false}
          onClose={this.signupCloseModal}
          contentStyle={{ 
                        width:'58.9%',
                        height:'70.6%',
                        borderRadius:'5px'
                        }} 
        > 
        {(this.state.showLS === false)?this.LOG_IN:this.SIGN_IN}

          
        </Popup>

</li>:<Popup trigger={<li 
 style={{ lineHeight:'20px',width:'30%',marginTop:'-1.5%' }}><li><a style={{ fontSize:'80%', }}> Hi, Reader </a></li>
 
</li>}
          on="hover"
          contentStyle={{ 
                        width:'15%',
                        height:'30%',
                        borderRadius:'5px',
                        marginTop:'-1%'
                        }}              
        > 
        <div style={{ color:'black' }}>
        <div id="listonProfile"> {this.props.getuserdetails.name}</div>
        {/* <div id="listonProfile"><Link to='/getaddress' > View Profile</Link></div> */}
        <div id="listonProfile"><Link to='/customer/customer_account' > View Profile</Link></div>
        <div id="listonProfile"><Link to='/customer/customer_order' > Your Orders</Link></div>
        <div id="listonProfile"><Link to='/my-wish-list' > Your Wishlist</Link></div>
        <div id="listonProfile"><Link to='/mypustak-wallet' >Your Wallet</Link></div>
        <div id="listonProfile" onClick={this.Userlogout}><a> Logout</a></div>
      </div>
        </Popup>
}<li style={{marginTop:'-1.5%'}}>
<Popup
          trigger={<span><span style={{ paddingRight:'4%' }}>{(this.props.ItemsInCart === 0)?null:this.props.ItemsInCart}</span><a><i className="fas fa-cart-arrow-down"/>Cart</a></span>}
          on='hover'
          contentStyle={{ 
            // color:'black',
            width:'31.3%',
            // left:'920.783px'
            // left:'68.5%',
            left:'67vw'
           }}
           offsetX="10px"
           disabled={(this.props.cartDetails.length === 0)?true:false}
          >
<div style={{ }} id={(this.props.ItemsInCart === 1)?"showCartOne":"showCart"}>
          

          {this.props.cartDetails.map(cart=>
            <div id="BgCartPopupBody">
            <div id="BgCartBookData">
          {/* const  src=`https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/${thumb}`; */}
              <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${cart.bookThumb}`}  style={{ float:'left',height:'75.75px',width:'53.56px' }}id="book"/>
              <div id="BgCartBookName">
                <span id="">{cart.bookName}</span>
              </div>
              <div id="BgCartBookCondPrice">
              <div id="BgCartBookprice">
                  <span >{cart.bookCond}<label id="newprice"><b> 
                      <span id="mrp">MRP:<span id="orgprice"> &#8377;<strike>{Math.round(cart.bookPrice ) }</strike></span>
                      </span>
                  <span style={{ color:'#e26127' }}>Free</span></b></label> </span>
              </div>          
            <span id="BgCartBookshipping">Shipping Handling Charge &#8377;{Math.round(cart.bookShippingCost)}
            <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png" id="" onClick={()=>this.RemoveFromCart(cart.bookInvId,cart.Cart_id)}></img></span>

          </div>
          </div>
          <hr id=""/>
          </div>
          )}
          {this.state.cartMsg && <span id="cartMsg">{this.state.cartMsg}</span>}
          
          </div>
          {/* <Link to="/view-cart"> */}
          <button id="BgCartcheckoutBtn" onClick={()=>CheckLogin()} >Proceed To Checkout</button>
          {/* {(TotalShipCost>80)?null */}
          {/* </Link> */}
          </Popup></li>
</ul>


</div>
</span>
{/* <div style={{ position:'absolute',marginLeft:'5%',marginTop:'1%' }}> */}
<div style={{  }} id="bgLogoDiv">
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="myPustakLogo"  onClick={()=>window.location.reload()}/>
</div>
       
       <div id="contentbox">
                <div className="row">
                       <div id="box1">
                       <Link style={{ textDecoration: 'none',color:'white' }} to="/get-books/">
                            <div>  
                            <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/GetBooks.0c753ab8.png`} id="BgGetBooksIcon" />
                            <p className="pageLink" >Get Books</p>
                            </div>
                            </Link>
                           </div>
                       {/* <div id="box2" onClick={()=>this.toDonation()}> */}
                       <div id="box2"><Link style={{ textDecoration: 'none' }}to="/donate-books">
                          {/* <center> */}
                            {/* <div className="circle"> */}
                            <div >  
                            <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Donatebookslogo.6057d18d.png`} id="BgDonateBooksIcon" />
                            <p  className="pageLink" style={{ color:'white',textDecoration:'none' }} >Donate Books</p>
                            
                            </div>
                            </Link>
                         {/* </center> */}
                        </div>
                       <div id="box3">
                           {/* <center> */}
                            {/* <div className="circle"> */}
                           <Link to='/proud-donors' style={{ textDecoration:'none',color:'white' }}> <div>  
                            <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Prouddonors.6c6bbd5a.png`} id="BgProudDonorsIcon" />
                            <p  className="pageLink" >Proud Donors</p>
                            
                            </div></Link>
                          {/* </center> */}
                       </div>
                </div>
          <Popup
          open={this.state.Category}
          closeOnDocumentClick
          onClose={this.CLOSECategoryModal}
          // lockScroll={true}
          contentStyle={{ width:'80%',marginTop:'13%',height: '69%'}}
        >
          <div style={{ color:'black' }} >
            <a className="close" onClick={this.CLOSECategoryModal} style={{marginTop:'-0.5%'}}>
              &times;
            </a>
           <Popupcat/>
          </div>
        </Popup>

                <div className="search-box" style={{ width:'76%' }}>
 {/* #####################################Categories Button ############################################# */}

                 <button  onClick={this.OPENCategoryModal} id="categorybtn"
                  style={{ }}><span id="categoryword">Categories </span><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/categoriesicon.png`} id="categoryimg"/></button> 
    
{/* 
    float:'left',border:'none',backgroundColor:'black',
                                fontWeight:'bold',height:'100%',padding:'1%',
                                color:'white',borderRadius:'5px' */}
    


        {/*#################### Not FixedTop  SEARCH BOX ############################ */}
                   <input className="search-txt" type="search" 
                   name='searchBook' value={this.props.searchBook}
                   onChange={this.onSearchChange}
                    style={{ position:'absolute',width:'70%',
                    marginTop:'-1%'
                  }}
                    autoComplete="off" 
                   placeholder="Search Books, Author, Publisher, Title, ISBN #"/>
                   <a className="search-btn" href="#" >
                   <i className="fas fa-search" 
                   style={{ position:'absolute',marginTop:'0%',cursor:'default' }}
                   ></i>
                   </a>
                </div>
                {this.props.searchBook !== '' &&
                <ul style={{ position:'absolute' ,zIndex:'5', listStyle: 'none', 
                backgroundColor:'white',color:'black',marginLeft:'10%',marginLeft:'30%',marginTop:'3%'}}>
                  {/* <li>Testing</li>
                  <li>Testing</li>
                  <li>Testing</li>
                  <li>Testing</li>
                  <li>Testing</li>
                  <li>Testing</li>

                  <li>uauuaua</li> */}
                </ul>}

        </div>
        <div className="contentbox1">
          <div className="row">
            <div className="box-1">
                <center>
                            {/* <div className="circlesmall">
                             o
                            </div> */}
                </center>
            </div><div className="box-2">
                <center>
                            {/* <div className="circlesmall">
                              h
                            </div> */}
                </center>
            </div>
          </div>
           {/* <div className="search-box">
                   <input className="search-txt" type="search" placeholder="search here..."/>
                   <a className="btn" href="#">
                   <i className="fas fa-search"></i>
                   </a>
           </div>  */}
        </div>
     </div>
      </div>
      
    );
  }
}

const mapStateToProps = state => ({
  searchBook:state.homeR.searchBook,
  userToken:state.accountR.token,
  ErrMsg:state.accountR.ErrMsg,
  cartDetails:state.cartReduc.MyCart,
  ItemsInCart:state.cartReduc.cartLength,
  getuserdetails:state.userdetailsR.getuserdetails,
  CartSessionData:state.cartReduc.CartSessionData,
  ToBeAddedToCartState:state.cartReduc.ToBeAddedToCartState,

})
export default connect(mapStateToProps,{doSearch,logout,Getaddress,getSavedToken,CartopenModal,CartcloseModal,doSearch,logout,ToBeAddedToCart,
  AddToCart,removeAllCart,getSavedToken,CartSession,RemoveCart,Getaddress,userdetails,})(Bgimage);
// export default connect(mapStateToProps,{AddDonation,ChangeLogin},)(Donationform)