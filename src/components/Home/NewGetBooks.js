import React, { Component } from 'react'
import axios from 'axios'
import Popup from 'reactjs-popup' 
import { connect } from 'react-redux';
import UserLogin from './UserLogin'
import UserSignup from './UserSignup';
import {doSearch} from '../../actions/homeAction'
import GBook from './GBook'
import InfiniteScroll from 'react-infinite-scroll-component';
import MediaQuery from 'react-responsive';
// import { Link,Redirect,browserHistory } from 'react-router-dom'
import MainHeader from '../MainHeader';
import config from 'react-global-configuration'
import ReactGA from 'react-ga';

import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormLabel from "@material-ui/core/FormLabel";
import CircularProgress from '@material-ui/core/CircularProgress';

class Search extends Component {
state={
        display:0,
        menuS:'block',
        name:'',
        showLS:true,
        Sopen:false,
        // SearchResult:[],
        GetBookResult:[],
        start:1,
        count:1,
        ShowFilterLoader:false

}
componentDidMount() {
  ReactGA.pageview(window.location.pathname + window.location.search);
  
  // window.addEventListener('scroll', this.handleScroll);
  // var searchValue= this.refs.searchBookFixed.value
  // this.DoSearch(searchValue,this.state.start)
//   http://127.0.0.1:8000/api/v1/get/get-books/9000/
  axios.get(`${config.get('apiDomain')}/api/v1/get/get-books/${this.state.start}/`)
  // axios.get(`http://103.217.220.149:80/api/v1/get/get-books/${this.state.start}/`)
  .then(res=>this.setState({GetBookResult:res.data}))
  .catch(err=>console.log(err)
  )
}
componentWillUnmount() {
  window.removeEventListener('scroll', this.handleScroll);
}
// handleScroll=()=>{
//   const searchValue= this.refs.searchBookFixed.value
//   const scroll = window.scrollY
//   this.setState({display:scroll});
//   console.log(this.state.display);
//   if(this.state.display < 284 ){
//     axios.get(`http://103.217.220.149:80/search/${searchValue}/3/`)
//     // axios.get(`http://127.0.0.1:8000/search/${searchValue}/3/`)
//     .then(res=>this.setState({GetBookResult:this.state.SearchResult.concat(res.data.hits)}))
//     .catch(err=>console.log(err)
//     )
//   }
//   if(this.state.display === 142 ){
//     axios.get(`http://103.217.220.149:80/search/${searchValue}/2/`)
//     // axios.get(`http://127.0.0.1:8000/search/${searchValue}/2/`)
//     .then(res=>this.setState({SearchResult:this.state.SearchResult.concat(res.data.hits)}))
//     .catch(err=>console.log(err)
//     )
//   }
// }
changeShowLS=()=>{
        this.setState({showLS:!this.state.showLS})
}
sopenModal=()=>{
    this.setState({ sopen: true })
  }
  scloseModal=()=>{
    this.setState({ sopen: false })
}
DoSearch=(searchValue,page)=>{
  // console.log("okkk");
  
    axios.get(`${config.get('apiDomain')}/search/${searchValue}/${page}/`)
    // axios.get(`http://127.0.0.1:8000/search/${searchValue}/${page}/`)
    .then(res=>this.setState({SearchResult:this.state.SearchResult.concat(res.data.hits)}))
    .catch(err=>console.log(err)
    )
  }
  
  fetchBooks=()=>{
    // var searchValue = this.refs.searchBookFixed.value
    console.log(this.state.start);
    this.setState({start:this.state.start+1})
    // var page = this.state.start
    
    console.log(this.state.start);
    // axios.get(`http://103.217.220.149:80/api/v1/get/get-books/${this.state.start}/`)
    axios.get(`${config.get('apiDomain')}/api/v1/get/get-books/${this.state.start}/`)
    .then(res=> this.setState({GetBookResult:this.state.GetBookResult.concat(res.data)}))
    .catch(err=>console.log(err)
    )
  }
  
  onSearchChange = e => {
  this.props.doSearch(e.target.value)
  // this.DoSearch(e.target.value,1)
  axios.get(`${config.get('apiDomain')}/search/${this.refs.searchBookFixed.value}/${this.state.start}/`)
    .then(res=> this.setState({SearchResult:res.data.hits}))
    .catch(err=>console.log(err)
    )
  }
LOG_IN=<UserLogin scloseModal={this.scloseModal} changeShowLS={this.changeShowLS} />
SIGN_IN=<UserSignup scloseModal={this.scloseModal} changeShowLS={this.changeShowLS}/>


handleChangeCheckBoxHindi = e => {
  // alert(e.target.checked)
  this.setState({ LanguageHindi: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};
handelChanageCheckBoxTillToday =e =>{
  this.setState({latestLast180Days:false,latestLast30Days:false,latestLast7Days:false})
  setTimeout(()=>this.onClickFilter(),1)

}
handleChangeCheckBoxEnglish = e => {
  this.setState({ LanguageEnglish: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxBengali = e => {
  this.setState({ LanguageBengali: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxMarathi = e => {
  this.setState({ LanguageMarathi: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxTamil = e => {
  this.setState({ LanguageTamil: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxTelugu = e => {
  this.setState({ LanguageTelugu: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxFrench = e => {
  this.setState({ LanguageFrench: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxDutch = e => {
  this.setState({ LanguageDutch: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxSanskrit = e => {
  this.setState({ LanguageSanskrit: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxGujarati = e => {
  this.setState({ LanguageGujarati: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxGerman = e => {
  this.setState({ LanguageGerman: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxKannada = e => {
  this.setState({ LanguageKannada: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxHindiAndEnglish = e => {
  this.setState({ LanguageHindiAndEnglish: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxEnglishFrench = e => {
  this.setState({ LanguageEnglishFrench: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxForeignLanguage = e => {
  this.setState({ LanguageForeignLanguage: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxLast7Days = e => {
  this.setState({ latestLast7Days: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxLast30Days = e => {
  this.setState({ latestLast30Days: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxLast180Days = e => {
  this.setState({ latestLast180Days: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxPaperback = e => {
  this.setState({ bindingPaperback: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)   

};

handleChangeCheckBoxHardcover = e => {
  this.setState({ bindingHardcover: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxBrandNew = e => {
  this.setState({ conditionBrandNew: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxAlmostNew = e => {
  this.setState({ conditionAlmostNew: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxVeryGood = e => {
  this.setState({ conditionVeryGood: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};

handleChangeCheckBoxReadable = e => {
  this.setState({ conditionReadable: e.target.checked });
  setTimeout(()=>this.onClickFilter(),1)

};


onClickFilter = () => {
  this.setState({ShowFilterLoader:true})

  let url = window.location.href;
  let data = url.split("/");
  let slug = data[data.length - 2];
  this.setState({ filterFlag: 1, start: 1 });
  let today = new Date();
  let bindingcover = [];
  let bookcondition = [];
  let minustimestamp, finaltimestamp;
  let timestamptoday = Math.floor(today.getTime() / 1000);
  let Language = [];
  if (this.state.latestLast180Days) {
    minustimestamp = 180 * 24 * 3600;
    finaltimestamp = timestamptoday - minustimestamp;

    // this.setState({ arrival_date_min: finaltimestamp });
  } else if (this.state.latestLast30Days) {
    minustimestamp = 30 * 24 * 3600;
    finaltimestamp = timestamptoday - minustimestamp;

    // this.setState({ arrival_date_min: finaltimestamp });
  } else if (this.state.latestLast7Days) {
    minustimestamp = 7 * 24 * 3600;
    finaltimestamp = timestamptoday - minustimestamp;

    // this.setState({ arrival_date_min: finaltimestamp });
  } else {
    finaltimestamp = 0;
    // this.setState({ arrival_date_min: 0 });
  }

  if (this.state.bindingPaperback) {
    bindingcover.push("Paperback");
    // this.setState({ binding_cover: bindingcover });
  }
  if (this.state.bindingHardcover) {
    bindingcover.push("Hardcover");
    // this.setState({ binding_cover: bindingcover });
  }

  if (this.state.conditionBrandNew) {
    bookcondition.push("BrandNew");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.conditionAlmostNew) {
    bookcondition.push("AlmostNew");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.conditionVeryGood) {
    bookcondition.push("VeryGood");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.conditionReadable) {
    bookcondition.push("AverageButInReadableCondition");
    // this.setState({ book_condition: bookcondition });
  }

  if (this.state.LanguageHindi) {
    Language.push("Hindi");
  }

  if (this.state.LanguageEnglish) {
    Language.push("English");
  }

  if (this.state.LanguageTamil) {
    Language.push("Tamil");
  }

  if (this.state.LanguageTelugu) {
    Language.push("Telugu");
  }

  if (this.state.LanguageSanskrit) {
    Language.push("Sanskrit");
  }

  if (this.state.LanguageMarathi) {
    Language.push("Marathi");
  }

  if (this.state.LanguageGujarati) {
    Language.push("Gujarati");
  }

  if (this.state.LanguageKannada) {
    Language.push("Kannada");
  }

  if (this.state.LanguageFrench) {
    Language.push("French");
  }

  if (this.state.LanguageDutch) {
    Language.push("Dutch");
  }

  if (this.state.LanguageGerman) {
    Language.push("German");
  }

  if (this.state.LanguageHindiAndEnglish) {
    Language.push("Hindi & English");
  }

  if (this.state.LanguageBengali) {
    Language.push("Bengali");
  }

  if (this.state.LanguageEnglishFrench) {
    Language.push("English(French)");
  }

  if (this.state.LanguageForeignLanguage) {
    Language.push("Foreign Language");
  }


  this.setState({
    book_condition: bookcondition,
    binding_cover: bindingcover,
    arrival_date_min: finaltimestamp,
    language: Language
  });

  let body = {
    // minPrice: this.state.minPrice,
    // maxPrice: this.state.maxPrice,
    binding: bindingcover,
    condition: bookcondition,
    date_min: finaltimestamp,
    date_max: timestamptoday,
    language: Language
  };

    console.log("Body First ", body);
    // let CATID=GetcategoryID(this.newUrl)
    // console.log(CATID,"GotCAtID")
    // http://localhost:8000/api/v1/filter/price/others/1 /GetAllbooks/<int:pg>
    axios
      .post(`http://localhost:8000/api/v1/filter/GetAllbooks/1`, body)
      .then(res => {
        console.log("in onClickFilter And the Data are ", res.data.Books);
        this.setState({ GetBookResult: res.data.Books,ShowFilterLoader:false });
      })
      .catch(err => {
        console.log("err", err)
        this.setState({ShowFilterLoader:false})
      
      });
  };


  render() {
    console.log(`${process.env.API_URL}  API`);
    
      const{ SearchResult }= this.state
    return (
      <div>
        <MainHeader/>
        
                        {/*####################StickyTop SEARCH BOX ############################ */}
              
 {/*####################### BODY FOR THE SEARCH PART ################################*/}
 {/* https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/0499274001546591981.jpg
 https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/0042265001547208288.jpg */}

 {/* Desktop */}
 <MediaQuery minWidth={992}>
          <div
            style={{
              height: '99vh',
              width: '21%',
              marginTop: 8,
              float: "left",
              backgroundColor: "white",
              padding: 10,
              // position: "sticky",
              top: 0,
              // overflow: "scroll"
              // border:'4px solid aliceblue'
            }}
          >
            <h4 style={{ color: "dodgerblue", textAlign: "center" }}>Filters</h4>

            <form noValidate>
             
              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                LATEST ARRIVALS
              </FormLabel>

              {/* <div style={{ }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      name="latestLast7Days"
                      // value="checkedA"
                      checked={this.listTime === "latestLast7Days"}
                      onChange={this.onClickFilter}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Last 7 Days"
                  style={{
                    height: 20
                  }}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.listTime === "latestLast30Days"}
                      name="latestLast30Days"
                      onChange={this.onClickFilter}
                    />
                  }
                  label="Last 30 Days"
                  style={{
                    height: 20
                  }}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.listTime === "latestLast180Days"}
                      onChange={this.onClickFilter}
                      name="latestLast180Days"
                    />
                  }
                  label="Last 180 Days"
                  style={{
                    height: 20
                  }}
                />
              </div> */}
              <div>
                <FormControlLabel
                  control={
                    
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }} 
                    onChange={this.handleChangeCheckBoxLast7Days}  
                    value="latestLast7Days"
                    />
                  }
                  label="Last 7 Days"
                  style={{ width:'80%' }}
                />

                <FormControlLabel
                  control={
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }}
                    onChange={this.handleChangeCheckBoxLast30Days}      
                    value="latestLast30Days"

                    />
                 
                  }
                  label="Last 30 Days"
                  style={{ width:'80%' }}

                />

                <FormControlLabel
                  control={
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }} 
                    onChange={this.handleChangeCheckBoxLast180Days}  
                    value="latestLast180Days"

                    />
                    
                  }
                  label="Last 180 Days"
                  style={{ width:'80%' }}

                />

                <FormControlLabel
                  control={
                    <input type="radio" name="selectDayPassed" style={{ width:'20%' }} 
                    onChange={this.handelChanageCheckBoxTillToday}  
                    value="tilltoday"

                    />
                    
                  }
                  label="Till Today"
                  style={{ width:'80%' }}

                />
              </div>

         

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                BINDING COVER
              </FormLabel>
              
              <div style={{ display:'flex' }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      // value={true}
                      name="bindingPaperback"
                      checked={this.state.bindingPaperback}
                      // onClick={()=>this.setState({bindingPaperback:!this.state.bindingPaperback})}
                      onChange={this.handleChangeCheckBoxPaperback}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Paperback"
                  style={{ height:20 }}
                />
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.state.bindingHardcover}
                      name="bindingHardcover"
                      checked={this.state.bindingHardcover}
                      onChange={this.handleChangeCheckBoxHardcover}
                    />
                  }
                  label="Hardcover"
                  style={{ height:20 }}
                />
              </div>
             

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                BOOK CONDITION
              </FormLabel>
              
              <React.Fragment>
              <div style={{ display:'flex'  }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.state.conditionBrandNew}
                      name="BrandNew"
                      onChange={this.handleChangeCheckBoxBrandNew}
                    />
                  }
                  label="Brand New"
                  style={{height: 20,width:'47%'}}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.state.conditionAlmostNew}
                      name="AlmostNew"
                      onChange={this.handleChangeCheckBoxAlmostNew}
                    />
                  }
                  label="Almost New"
                  style={{height: 20,width:'47%'}}
                  
                />
                </div>
              <div style={{ display:'flex' }}>

                <FormControlLabel
                  control={
                    <Checkbox
                      // value="checkedA"
                      checked={this.state.conditionVeryGood}
                      name="VeryGood"
                      onChange={this.handleChangeCheckBoxVeryGood}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Very Good"
                  style={{height: 20,width:'47%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value="checkedB"
                      checked={this.state.conditionReadable}
                      name="Readable"
                      onChange={this.handleChangeCheckBoxReadable}
                    />
                  }
                  label="Readable"
                  style={{height: 20,width:'47%'}}

                />
              </div>
              </React.Fragment>
              

              <FormLabel component="legend" style={{ color: "rgb(255, 165, 0)" }}>
                LANGUAGES
              </FormLabel>
             
                <React.Fragment>
              <div style={{display:'flex',flexFlow:'row wrap', }}>
                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("Hindi")}
                      checked={this.state.LanguageHindi}
                      name="Hindi"
                      onChange={this.handleChangeCheckBoxHindi}
                    />
                  }
                  label="Hindi"
                  style={{height: 20,width:'45%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}

                      name="English"
                      onChange={this.handleChangeCheckBoxEnglish}
                    />
                  }
                  label="English"
                  style={{height: 20,width:'45%'}}
                  
                />

                <FormControlLabel
                  control={
                    <Checkbox
                 
                      checked={this.state.LanguageMarathi}
                      onChange={this.handleChangeCheckBoxMarathi}
                      name="Marathi"
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Marathi"
                  style={{height: 20,width:'45%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                   
                      checked={this.state.LanguageEnglishFrench}
                      name="English(French)"
                      onChange={this.handleChangeCheckBoxEnglishFrench}
                    />
                  }
                  label="English(French)"
                  style={{height: 20,width:'45%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("Bengali")}
                      checked={this.state.LanguageBengali}
                      name="Bengali"
                      onChange={this.handleChangeCheckBoxBengali}
                    />
                  }
                  label="Bengali"
                  style={{ height: 20,width:'45%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("Sanskrit")}
                      checked={this.state.LanguageSanskrit}
                      name="Sanskrit"
                      onChange={this.handleChangeCheckBoxSanskrit}
                    />
                  }
                  label="Sanskrit"
                  style={{height: 20,width:'45%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      // value={this.Language.includes("Dutch")}
                      checked={this.state.LanguageDutch}
                      name="Dutch"
                      onChange={this.handleChangeCheckBoxDutch}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Dutch"
                  style={{height: 20,width:'45%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("Gujarati")}
                      checked={this.state.LanguageGujarati}
                      name="Gujarati"
                      onChange={this.handleChangeCheckBoxGujarati}
                    />
                  }
                  label="Gujarati"
                  style={{height: 20,width:'45%'}}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("Telugu")}
                      checked={this.state.LanguageTelugu}
                      name="Telugu"
                      onChange={this.handleChangeCheckBoxTelugu}
                    />
                  }
                  label="Telugu"
                  style={{height: 20,width:'45%'}}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("German")}
                      checked={this.state.handleChangeCheckBoxGerman}
                      name="German"
                      onChange={this.onClickFilter}
                    />
                  }
                  label="German"
                  style={{height: 20,width:'45%'}}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      // value={this.Language.includes("Kannada")}
                      checked={this.state.LanguageKannada}
                      name="Kannada"
                      onChange={this.handleChangeCheckBoxKannada}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Kannada"
                  style={{height: 20,width:'45%'}}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("French")}
                      checked={this.state.LanguageFrench}
                      name="French"
                      onChange={this.handleChangeCheckBoxFrench}
                    />
                  }
                  label="French"
                  style={{height: 20,width:'45%'}}
                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("Tamil")}
                      checked={this.state.LanguageTamil}
                      name="Tamil"
                      onChange={this.handleChangeCheckBoxTamil}
                    />
                  }
                  label="Tamil"
                  style={{height: 20,width:'45%'}}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      inputProps={{ style: { display: "none" } }}
                      // value={this.Language.includes("Hindi & English")}
                      checked={this.state.LanguageHindiAndEnglish}
                      name="Hindi & English"
                      onChange={this.onClickhandleChangeCheckBoxHindiAndEnglishilter}
                    />
                  }
                  label="Hindi & English"
                  style={{height: 20}}

                />

                <FormControlLabel
                  control={
                    <Checkbox
                      // value={this.Language.includes("Foreign Language")}
                      checked={this.state.LanguageForeignLanguage}
                      name="Foreign Language"
                      onChange={this.handleChangeCheckBoxForeignLanguage}
                      inputProps={{ style: { display: "none" } }}
                    />
                  }
                  label="Foreign Language"
                  style={{height: 20,width:'80%'}}

                />
              </div>
              </React.Fragment>
              

              <div>
             
              </div>
            </form>
          </div>
        </MediaQuery>
 <MediaQuery minWidth={992} >
 <div id="searchBody">
{/* ----------------------------Popup Shown on Slecting Filter */}
          <Popup
          open={this.state.ShowFilterLoader}
          closeOnDocumentClick={false}
          overlayStyle={{ 
            width:"100%",
            height:"90%",
            backgroundColor:'rgba(255, 255, 255, 0.5)',
            marginTop:'5%',
            // opacity:'0.8',

           }}
           contentStyle={{ 
            width: '5%',
            borderWidth:'0px',
            backgroundColor:'transparent'
            }}
          >
              <div>
              <CircularProgress style={{ margin: 'spacing(2)',color:'primary',opacity:'1' }}/>
              </div>
              
          </Popup> 

{/* -------------------------------Infinite Scroll----------------------------------------------------- */}
<div style={{ color:'black' }}>
              <InfiniteScroll
              dataLength = {this.state.GetBookResult.length}
             scrollThreshold={'70%'}
              next={this.fetchBooks}
              hasMore={true}
             //  loader={<h4>NO RESULT FOUND</h4>}
             //  height={400}
              >



          {this.state.GetBookResult.map((bookdata)=>
             //  console.log(bookdata);
              
             <GBook key={bookdata.slug}
                 book={bookdata}
                 />
              )
              }
              {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
          

              </InfiniteScroll>


              </div>
          {/* ------------------------------------------------------------------------ */}
          </div>

 </MediaQuery>
 {/* Tab */}
  <MediaQuery minWidth={768} maxWidth={991} > 
             <div id="searchBody" >


{/* -------------------------------Infinite Scroll----------------------------------------------------- */}
<div style={{ color:'black' }}>
              <InfiniteScroll
              dataLength = {this.state.GetBookResult.length}
             scrollThreshold={'70%'}
              next={this.fetchBooks}
              hasMore={true}
             //  loader={<h4>NO RESULT FOUND</h4>}
             //  height={400}
              >



          {this.state.GetBookResult.map((bookdata)=>
             //  console.log(bookdata);
              
             <GBook key={bookdata.slug}
                 book={bookdata}
                 />
              )
              }
              {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
          

              </InfiniteScroll>


              </div>
          {/* ------------------------------------------------------------------------ */}
          </div>
             
             </MediaQuery>
             <MediaQuery maxWidth={767} minWidth={539} > 
             <div id="searchBody" >


{/* -------------------------------Infinite Scroll----------------------------------------------------- */}
<div style={{ color:'black' }}>
              <InfiniteScroll
              dataLength = {this.state.GetBookResult.length}
             scrollThreshold={'70%'}
              next={this.fetchBooks}
              hasMore={true}
             //  loader={<h4>NO RESULT FOUND</h4>}
             //  height={400}
              >



          {this.state.GetBookResult.map((bookdata)=>
             //  console.log(bookdata);
              
             <GBook key={bookdata.slug}
                 book={bookdata}
                 />
              )
              }
              {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
          

              </InfiniteScroll>


              </div>
          {/* ------------------------------------------------------------------------ */}
          </div>
             
             </MediaQuery>
             <MediaQuery  maxWidth={538}  > 

              <div id="searchBody" >


   {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
   <div style={{ color:'black' }}>
                 <InfiniteScroll
                 dataLength = {this.state.GetBookResult.length}
                scrollThreshold={'70%'}
                 next={this.fetchBooks}
                 hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
                 >



             {this.state.GetBookResult.map((bookdata)=>
                //  console.log(bookdata);
                 
                <GBook key={bookdata.slug}
                    book={bookdata}
                    />
                 )
                 }
                 {/* {this.state.GetBookResult.map((bookdata)=>
                 ((bookdata.document.is_out_of_stack !== `Y`)?
                <SBook key={bookdata.document.title} 
                    book={bookdata.document}
                    />:null  
                 ))
                 } */}
             

                 </InfiniteScroll>


                 </div>
             {/* ------------------------------------------------------------------------ */}
             </div>
             </MediaQuery>

      </div>
    )
  }
}
const mapStateToProps = state => ({
    searchBook:state.homeR.searchBook,
    userToken:state.accountR.token,
  })
export default connect(mapStateToProps,{doSearch})(Search);