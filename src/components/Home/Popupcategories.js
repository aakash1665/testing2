import React, { Component } from 'react'
import './Popcat.css'
import { connect } from 'react-redux';
import {setCategory} from '../../actions/homeAction'
import { Link,Redirect,browserHistory } from 'react-router-dom'
 class Popupcat extends Component {
  render() {
    const SetCategory=(category)=>{
      // alert(category);
      this.props.setCategory(category);
      this.props.CloseCatPopup()
    }
    return (
      <div style={{ display:'flex'}}>
         <div style={{ color:'black',flex:'1',    flexBasis: '2%'}}  >
     
           <label id="Competitive-Exams"><Link style={{ textDecoration: 'none',width:'18%' }}to="/category/competitive-exams/"
           onClick={()=>SetCategory('/category/competitive-exams/')}>Competitive Exams</Link></label>
             <label id="Engineering-Medical"><Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/engineering/"
             onClick={()=>SetCategory('/category/competitive-exams/engineering/')}>Engineering & Medical</Link>
             </label>
             <div id="divsection1">
             <ul id="link">

             {/* <Link to={{
  pathname: '/tylermcginnis',
  state: {
    fromNotifications: true
  }
}}>Tyler McGinnis</Link> */}

               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}} to={{ pathname:"/category/competitive-exams/engineering/aieee/"}} 
                onClick={()=>SetCategory('/category/competitive-exams/engineering/aieee/')}>AIEEE</Link></li>

               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }} to={{ pathname:"/category/competitive-exams/engineering/engineering-post-graduate/" }} 
                onClick={()=>SetCategory('/category/competitive-exams/engineering/engineering-post-graduate/')}>Engineering(Post Graduate)</Link></li>

               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }} to={{ pathname:"/category/competitive-exams/engineering/iit/" }} 
               onClick={()=>SetCategory('/category/competitive-exams/engineering/iit/')} >IIT</Link></li>

               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }} to={{ pathname:"/category/competitive-exams/engineering/medical/"}} 
               onClick={()=>SetCategory('/category/competitive-exams/engineering/medical/')}>Medical</Link></li>

               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }} to="/category/competitive-exams/engineering/others/" 
               onClick={()=>SetCategory('/category/competitive-exams/engineering/others/')}>Others</Link></li>

               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }} to="/category/competitive-exams/engineering/state-level/" 
               onClick={()=>SetCategory('/category/competitive-exams/engineering/state-level/')}>State Level</Link></li>

               </ul>
               </div>
               <label id="Government-Jobs">
               <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/government-jobs/"
               onClick={()=>SetCategory('/category/competitive-exams/government-jobs/')}> Government Jobs</Link>
             </label>
             <div id="divsection1">
             <ul id="link">
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/government-jobs/banking/"
                onClick={()=>SetCategory('/category/competitive-exams/government-jobs/banking/')}>Banking</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/government-jobs/others/"
                onClick={()=>SetCategory('/category/competitive-exams/government-jobs/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/government-jobs/railway/"
                onClick={()=>SetCategory('/category/competitive-exams/government-jobs/railway/')}>Railway</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/government-jobs/ssc/"
                onClick={()=>SetCategory('/category/competitive-exams/government-jobs/ssc/')}>SSC</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/government-jobs/teaching-related-exams/"
                onClick={()=>SetCategory('/category/competitive-exams/government-jobs/teaching-related-exams/')}>Teaching Related Exams</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/government-jobs/upsc/"
                onClick={()=>SetCategory('/category/competitive-exams/government-jobs/upsc/')}>UPSC</Link></li>
               </ul>
               </div>
               <label id="International-Exams">
               <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/international-exams/" onClick={()=>SetCategory('/category/competitive-exams/international-exams/')}>
             International Exams
           </Link>  </label>
             <div id="divsection1">
             <ul id="link">
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/international-exams/gmat/"
               onClick={()=>SetCategory('/category/competitive-exams/international-exams/gmat/')}>GMAT</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/international-exams/gre/"
               onClick={()=>SetCategory('/category/competitive-exams/international-exams/gre/')}>GRE</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/international-exams/others/"
               onClick={()=>SetCategory('/category/competitive-exams/international-exams/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/international-exams/sat/"
               onClick={()=>SetCategory('/category/competitive-exams/international-exams/sat/')}>SAT</Link></li>
               </ul>
               </div>
               <label id="General-Knowledge-Learning">
               <Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/general-knowledge-learni/"
               onClick={()=>SetCategory('/category/competitive-exams/general-knowledge-learni/')}>
     General knowledge  & Learning
           </Link>  </label>
             <div id="divsection2">
             {/* <ul id="link">
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/general-knowledge-learni/dic/"
               onClick={()=>SetCategory('/category/competitive-exams/general-knowledge-learni/dic/')}>Dictionary</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/competitive-exams/general-knowledge-learni/gk/"
               onClick={()=>SetCategory('/category/competitive-exams/general-knowledge-learni/gk/')}>GK</Link></li>
               </ul> */}
               </div>
           </div>

           <div style={{ border:'1px solid white',flex:'1',flexBasis: '1%',borderBottomColor:'transparent',borderLeftColor:'transparent',borderRightColor:'transparent'}}>
           <label id="School-Level"><Link style={{ textDecoration: 'none' }}to="/category/competitive-exams/school-level/"
           onClick={()=>SetCategory('/category/competitive-exams/school-level/')}>
           School Level
            </Link> </label>
             <div id="divsection3" >
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/competitive-exams/school-level/navodaya-vidyalaya/"
               onClick={()=>SetCategory('/category/competitive-exams/school-level/navodaya-vidyalaya/')}>Navodaya Vidyalaya</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/competitive-exams/school-level/ntse/"
               onClick={()=>SetCategory('/category/competitive-exams/school-level/ntse/')}>NTSE</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/competitive-exams/school-level/olympiads/"
               onClick={()=>SetCategory('/category/competitive-exams/school-level/olympiads/')}>Olympiads</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/competitive-exams/school-level/others/"
               onClick={()=>SetCategory('/category/competitive-exams/school-level/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/competitive-exams/school-level/sainik-school/"
               onClick={()=>SetCategory('/category/competitive-exams/school-level/sainik-school/')}>Sainiks</Link></li>
               </ul>
               </div>
               <label id="Fiction-Non-fiction-Books"><Link style={{ textDecoration: 'none',width:'18%' }}to="/category/fiction-non-fiction/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/')}> Fiction & Non-fiction Books</Link></label>
               <label id="Friction"><Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/')}>
          Fiction
            </Link> </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/fiction/comics-graphic-novel/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/comics-graphic-novel/')}>Comics & Graphic novels</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/drama/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/drama/')}>Drama</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/horror-and-thriller/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/horror-and-thriller/')}>Horror & Thriller</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/literary/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/literary/')}>Literary</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/mystery/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/mystery/')}>Mystery</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/romance/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/romance/')}>Romance</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/science-fiction/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/science-fiction/')}>Science Fiction</Link> </li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/short-stories/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/short-stories/')}>Short Stories</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction/teens/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction/teens/')}>Teens</Link></li>
               </ul>
               </div>
               <label id="Fiction-Non-Fiction-others"><Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/fiction-non-fiction-others/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/')}>
          Friction & Non Friction others
          </Link>   </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction-non-fiction-others/astrology/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/astrology/')}>Astrology</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction-non-fiction-others/business-management/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/business-management/')}>Business & Management</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction-non-fiction-others/health/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/health/')}>Health</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction-non-fiction-others/history-and-politics/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/history-and-politics/')}>History & Politics</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction-non-fiction-others/others/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/fiction-non-fiction-others/sports-games/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/fiction-non-fiction-others/sports-games/')}> Sports & Games </Link></li>
               </ul>
               </div>
                </div>

          <div style={{ border:'1px solid white',flex:'1',flexBasis: '1%',borderBottomColor:'transparent',borderLeftColor:'transparent',borderRightColor:'transparent'}}>
          <label id="Non-Friction"><Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/non-fiction/"
          onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/')}>
           Non Friction
             </Link></label>
             <div id="divsection3" >
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/biographies/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/biographies/')}>Biographies</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/coffee-table/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/coffee-table/')}>Coffee Table</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/computer-and-internet/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/computer-and-internet/')}>Computer & Internet</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/cooking/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/cooking/')}>Cooking</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/entertainment/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/entertainment/')}>Entertainment</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/househome/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/househome/')}>House & Home</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/pets/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/pets/')}>Pets</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/photography/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/photography/')}>Photography</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/sports/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/sports/')}>Sports</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/non-fiction/travel/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/non-fiction/travel/')}>Travel</Link></li>
               </ul>
               </div>
               <label id="Religion-Sprituality"><Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/religion-spirituality/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/')}>
          Religion & Spirituality
            </Link> </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/fiction-non-fiction/religion-spirituality/holy-books/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/holy-books/')}>Holy Books</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/religion-spirituality/others/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/religion-spirituality/philosophy/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/philosophy/')}> philosophy</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/religion-spirituality/religions/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/religions/')}>Religions</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/fiction-non-fiction/religion-spirituality/spirituality/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/religion-spirituality/spirituality/')}>Spirituality</Link></li>
               </ul>
               </div>
               <label id="Motivation-Self-help"><Link style={{ textDecoration: 'none' }}to="/category/fiction-non-fiction/motivation-self-help/"
               onClick={()=>SetCategory('/category/fiction-non-fiction/motivation-self-help/')}>
          Motivation & Self-help
           </Link>  </label>
                <label id="Fiction-Non-fiction-Books"><Link style={{ textDecoration: 'none',marginTop: '11.5%',display: 'block'}}to="/category/school-children-books/"
                onClick={()=>SetCategory('/category/school-children-books/')}> School & Children Books</Link></label>
               <label id="Dictionary-level"><Link style={{ textDecoration: 'none' }}to="/category/school-children-books/dictionary-level-/"
               onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/')}>
         Dictionary Level
           </Link>  </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/dictionary-level-/english-to-english-/"
               onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/english-to-english-/')}>English To English</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/dictionary-level-/english-to-hindi-/"
               onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/english-to-hindi-/')}>English To Hindi</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/dictionary-level-/foreign-language/"
               onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/foreign-language/')}>Foreign Language</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/dictionary-level-/hindi-to-english-/"
               onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/hindi-to-english-/')}>Hindi To English</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/dictionary-level-/subject-wise-/"
               onClick={()=>SetCategory('/category/school-children-books/dictionary-level-/subject-wise-/')}>Subject Wise</Link></li>
               </ul>
               </div>
          </div>
          <div style={{ border:'1px solid white',flex:'1',borderBottomColor:'transparent',borderLeftColor:'transparent',borderRightColor:'transparent'}}>
          <label id="Childrens-books"><Link style={{ textDecoration: 'none' }}to="/category/school-children-books/children-books/"
          onClick={()=>SetCategory('/category/school-children-books/children-books/')}>
           Children books
            </Link> </label>
             <div id="divsection3" >
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/action-and-adventure/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/action-and-adventure/')}>Action & Adventure</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/activity-books/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/activity-books/')}>Activity Books</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/childrens-literature/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/childrens-literature/')}>Children's literature</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/comics-graphic-novels/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/comics-graphic-novels/')}>Comics & Graphic Novels</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/disney-store/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/disney-store/')}>Disney Store</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/fun-humour/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/fun-humour/')}>Fun & humor</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/history-mythology-/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/history-mythology-/')}>History & Mythology</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/knowledge-and-learning/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/knowledge-and-learning/')}>Knowledge & Learning</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/others/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/children-books/picture-book/"
               onClick={()=>SetCategory('/category/school-children-books/children-books/picture-book/')}>Picture Book</Link></li>
               </ul>
               </div>
               <label id="Classes"><Link style={{ textDecoration: 'none' }}to="/category/school-children-books/classes/"
               onClick={()=>SetCategory('/category/school-children-books/classes/')}>
          Classes
           </Link>  </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/classes/kg/"
               onClick={()=>SetCategory('/category/school-children-books/classes/kg/')}>KG</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-1/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-1/')}>Class 1</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-2/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-2/')}> Class 2</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-3/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-3/')}>Class 3</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-4/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-4/')}>Class 4</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-5/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-5/')}>Class 5</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-6/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-6/')}>Class 6</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-7/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-7/')}>Class 7</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-8/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-8/')}>Class 8</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-9/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-9/')}>Class 9</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-10/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-10/')}>Class 10</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-11/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-11/')}>Class 11</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/school-children-books/classes/class-12/"
               onClick={()=>SetCategory('/category/school-children-books/classes/class-12/')}>Class 12</Link></li>
               </ul>
               </div>
          </div>
          <div style={{ border:'1px solid white',flex:'1',borderBottomColor:'transparent',borderLeftColor:'transparent',borderRightColor:'transparent'}}>
          <label id="NCERT"><Link style={{ textDecoration: 'none' }}to="/category/school-children-books/ncert/"
          onClick={()=>SetCategory('/category/school-children-books/ncert/')}>
         NCERT
            </Link> </label>
             <div id="divsection3" >
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/ncert/class-6/"
               onClick={()=>SetCategory('/category/school-children-books/ncert/class-6/')}>Class 6</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/ncert/class-7/"
               onClick={()=>SetCategory('/category/school-children-books/ncert/class-7/')}>Class 7</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/ncert/class-8/"
               onClick={()=>SetCategory('/category/school-children-books/ncert/class-8/')}>Class 8</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/ncert/class-9/"
               onClick={()=>SetCategory('/category/school-children-books/ncert/class-9/')}>Class 9</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/ncert/class-10/"
               onClick={()=>SetCategory('/category/school-children-books/ncert/class-10/')}>Class 10</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/ncert/class-11/"
               onClick={()=>SetCategory('/category/school-children-books/ncert/class-11/')}>Class 11</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/school-children-books/ncert/class-12/"
               onClick={()=>SetCategory('/category/school-children-books/ncert/class-12/')}>Class 12</Link></li>
               </ul>
               </div>
               <label id="Fiction-Non-fiction-Books"><Link style={{ textDecoration: 'none',width:'18%' }}to="/category/competitive-exams/cat/"
               onClick={()=>SetCategory('/category/competitive-exams/cat/')}> CAT(MBA)</Link></label>
               <label id="Fiction-Non-fiction-Books"><Link style={{ textDecoration: 'none',width:'81%', display: 'block', marginTop: '11%' }}to="/category/university-books/"
               onClick={()=>SetCategory('/category/university-books/')}>University Books</Link></label>
               <label id="Engineering"><Link style={{ textDecoration: 'none' }}to="/category/university-books/engineering/"
               onClick={()=>SetCategory('/category/university-books/engineering/')}>
          Engineering
          </Link>   </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/engineering/aeronautical/"
               onClick={()=>SetCategory('/category/university-books/engineering/aeronautical/')}>Aeronautical</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/bio-technology/"
               onClick={()=>SetCategory('/category/university-books/engineering/bio-technology/')}>Bio Technology</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/chemical-/"
               onClick={()=>SetCategory('/category/university-books/engineering/chemical-/')}>Chemical</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/civil-/"
               onClick={()=>SetCategory('/category/university-books/engineering/civil-/')}>Civil</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/computer-science/"
               onClick={()=>SetCategory('/category/university-books/engineering/computer-science/')}>Computer Science</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/electrical-/"
               onClick={()=>SetCategory('/category/university-books/engineering/electrical-/')}>Electrical</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/electronics/"
               onClick={()=>SetCategory('/category/university-books/engineering/electronics/')}>Electronics </Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/marine/"
               onClick={()=>SetCategory('/category/university-books/engineering/marine/')}>Marine</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/mechanical/"
               onClick={()=>SetCategory('/category/university-books/engineering/mechanical/')}>Mechanical</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/engineering/others/"
               onClick={()=>SetCategory('/category/university-books/engineering/others/')}>Others</Link></li>
               </ul>
               </div>
               <label id="Finance"><Link style={{ textDecoration: 'none' }}to="/category/university-books/finance/"
               onClick={()=>SetCategory('/category/university-books/finance/')}>
          Finance
            </Link> </label>
        
          </div>
          <div style={{ border:'1px solid white',flex:'1',borderBottomColor:'transparent',borderLeftColor:'transparent',borderRightColor:'transparent'}}>
          <label id="Medical"><Link style={{ textDecoration: 'none' }}to="/category/university-books/medical/"
          onClick={()=>SetCategory('/category/university-books/medical/')}>
         Medical
            </Link> </label>
             <div id="divsection3" >
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/medical/ayurveda/"
               onClick={()=>SetCategory('/category/university-books/medical/ayurveda/')}>Ayurveda</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/medical/dental/"
               onClick={()=>SetCategory('/category/university-books/medical/dental/')}>Dental</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/medical/mbbs/"
               onClick={()=>SetCategory('/category/university-books/medical/mbbs/')}>MBBS</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/medical/others/"
               onClick={()=>SetCategory('/category/university-books/medical/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/medical/pg/"
               onClick={()=>SetCategory('/category/university-books/medical/pg/')}>PG</Link></li>
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/medical/pharmacy/"
               onClick={()=>SetCategory('/category/university-books/medical/pharmacy/')}>Pharmacy</Link></li>
               </ul>
               </div>
               {/* <label id="Fiction-Non-fiction-Books"><Link style={{ textDecoration: 'none',width:'18%' }}to="/category/competitive-exams"> Fiction & Non-fiction Books</Link></label> */}
               <label id="Others"><Link style={{ textDecoration: 'none' }}to="/category/university-books/others/"
               onClick={()=>SetCategory('/category/university-books/others/')}>
         Others
            </Link> </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none' ,color:"#4b4b4b"}}to="/category/university-books/others/bba/"
               onClick={()=>SetCategory('/category/university-books/others/bba/')}>BBA</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/others/bcom/"
               onClick={()=>SetCategory('/category/university-books/others/bcom/')}>B Com</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/others/law/"
               onClick={()=>SetCategory('/category/university-books/others/law/')}>Law</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/others/mba/"
               onClick={()=>SetCategory('/category/university-books/others/mba/')}>MBA</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/others/mcom/"
               onClick={()=>SetCategory('/category/university-books/others/mcom/')}>M Com</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/others/others/"
               onClick={()=>SetCategory('/category/university-books/others/others/')}>Others</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/others/probability-statistics/"
               onClick={()=>SetCategory('/category/university-books/others/probability-statistics/')}> Probability & Statistics </Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/others/phd/"
               onClick={()=>SetCategory('/category/university-books/others/phd/')}>PhD</Link></li>
               </ul>
               </div>
               <label id="Science-arts"><Link style={{ textDecoration: 'none' }}to="/category/university-books/science-arts/"
               onClick={()=>SetCategory('/category/university-books/science-arts/')}>
         Science & Arts
          </Link>   </label>
               <div id="divsection3">
             <ul id="link1">
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/science-arts/ba/"
               onClick={()=>SetCategory('/category/university-books/science-arts/ba/')}>B.A.</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/science-arts/bca/"
               onClick={()=>SetCategory('/category/university-books/science-arts/bca/')}>BCA</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/science-arts/bsc/"
               onClick={()=>SetCategory('/category/university-books/science-arts/bsc/')}>BSc</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/science-arts/ma/"
               onClick={()=>SetCategory('/category/university-books/science-arts/ma/')}>M.A.</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/science-arts/mca/"
               onClick={()=>SetCategory('/category/university-books/science-arts/mca/')}>MCA</Link></li>
               <li><Link style={{ textDecoration: 'none',color:"#4b4b4b" }}to="/category/university-books/science-arts/msc/"
               onClick={()=>SetCategory('/category/university-books/science-arts/msc/')}> MSc</Link> </li>
               </ul>
               </div>
          </div>
        
      </div>
    )
  }
}

const mapStateToProps = state => ({
      ClickedCategory:state.homeR.ClickedCategory,
})

export default connect(mapStateToProps,{setCategory})(Popupcat);