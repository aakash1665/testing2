import React, { Component } from 'react'
import {LoginCheck,showLoader,clearLoginErr,ActivteSuccesPopup} from '../../actions/accountAction'
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
import RCG from 'react-captcha-generator';
import './login.css'
import './userLogin.css'
import axios from 'axios'
import Loader from 'react-loader-spinner'
import config from 'react-global-configuration'
import Popup from 'reactjs-popup';

import ReactGA from 'react-ga';


import { GoogleLogin } from 'react-google-login';

class UserLogin extends Component {
    state={
        Sopen:false,
        email:'',
        password:'',
        openForget:false,
        captcha: '',
        Imgcaptcha: '',
        loginCliked:false,
        LoginErrMsg:this.props.ErrMsg,
        EmailErrrmsg:'',
        PassErrmsg:'',
        CaptchaErr:'',
        ForgetResponse:0,
        CountMsg:1,
        OpenForgetLoader:false,
    }
    result=(text)=>{
      this.setState({
        Imgcaptcha: text
      })
    }
    
    componentDidMount(){

      ReactGA.pageview(window.location.pathname + window.location.search);
      
      this.setState({ForgetResponse:0})
      this.setState({CountMsg:1})
    }
    
    
    sopenModal=()=>{
        this.setState({ Sopen: true })
      }
      scloseModal=()=>{
        this.props.scloseModal()
    }
    changeShowLS=()=>
    {this.props.changeShowLS()}

    ForgetPassword =(e)=>{
      // alert("h")
      this.setState({email:"",password:""})
      if (e.key !== 'Enter')
      this.setState({openForget:!this.state.openForget})

    }
    EmailErrrmsg=''
    CaptchaErr=''
    CheckSubmitForgot=()=>{
      this.check()
      if(this.state.email === ''){
        // this.setState({EmailErrrmsg:'Please Fill up the Email'})
        this.EmailErrrmsg='Please Fill up the Email'
      }else{
        // this.setState({EmailErrrmsg:''})
        this.EmailErrrmsg=''
      }
      if(this.state.captcha !== this.state.Imgcaptcha ){
        // this.setState({CaptchaErr:"Captcha Did not match"});
        this.setState({CaptchaErr:'Captcha Did not match'})
        // return;
        this.CaptchaErr='Captcha Did not match'
      }else{
        this.setState({CaptchaErr:""});
        this.CaptchaErr=''
        console.log("SuccessErrBlank1")
      }
      // console.log(this.captchaEnter.value);
      if(this.CaptchaErr === '' && this.EmailErrrmsg === ''){
        this.SubmitForgot()
      }
      console.log(this.state.Imgcaptcha, this.state.captcha, this.state.Imgcaptcha === this.state.captcha)
    }
    check() {
      // console.log(this.state.Imgcaptcha, this.captchaEnter.value, this.state.Imgcaptcha === this.state.captcha)
    }
    SubmitForgot = ()=>{
      this.setState({OpenForgetLoader:true})

      console.log("SuccessErrBlank2")

      // alert("1")
      const passdata = {
        
        "email" : this.state.email,
        'content-type': 'multipart/form-data',
         'boundary':'----WebKitFormBoundary7MA4YWxkTrZu0gW'
 
         }
         
        //  axios.post(`http://127.0.0.1:8000/core/forgot_password/`,passdata)
       axios.post(`${config.get('apiDomain')}/core/forgot_password/`,passdata)
     .then(res=>{console.log(res.status);this.setState({ForgetResponse:res.status})
     this.setState({OpenForgetLoader:false})
    
    }
     
     )
     .catch(err=>{console.log(err.response.status,"SuccessErrBlank3");this.setState({ForgetResponse:err.response.status})}
     )
    //  this.props.scloseModal()
    }
    onChange = e => {
      this.setState({PassErrmsg:'',EmailErrrmsg:''})
      this.setState({ [e.target.name]: e.target.value });
                        if(this.state.email === ''){

                        }
                    };
    
  render() {
    // console.log(this.captchaEnter);
    
     var EmailErrrmsg='okk'
     var PassErrmsg=''
     let CaptchaErr=""
    // const Login=()=>{
    //   alert("L")
    //     if(this.state.email === ''){
    //     this.setState({EmailErrrmsg:'Please Fill up the Email'})}
    //     else{
    //         this.setState({EmailErrrmsg:''})
    //     }
    //     if(this.state.password === ''){
    //       this.setState({PassErrmsg:"Please Fill up the password"})}
    //     else{
    //       this.setState({PassErrmsg:""})
    //     }
    //     // if(this.state.email )

    //     if(this.state.email !== '' && this.state.password !== ''){
    //     //   alert("okk")
    //       SuccLogin()
    //     }
    //     // this.setState({EmailErrrmsg:"",PassErrmsg:""})
    //   }

    // if(this.state.captcha !== captchaEnter.value ){
    //   CaptchaErr="Captcha Did not match"
    // }

   const Login = (e) =>{
    e.preventDefault()
    //  alert("S")
     if(this.state.email === ''){
      this.setState({EmailErrrmsg:'Please Fill up the Email'})}
      else{
          this.setState({EmailErrrmsg:''})
      }
      if(this.state.password === ''){
        this.setState({PassErrmsg:"Please Fill up the password"})}
      else{
        this.setState({PassErrmsg:""})
      }
        
        // console.log("in login func");
        // document.getElementById("loginBtn").
        // this.setState({loginCliked:true})
        // this.state.password
        // e.preventDefault()
        // this.props.showLoader()
        const {email,password} = this.state
       const details={
            email,
            password,
        }
    if(this.state.EmailErrrmsg === '' && this.state.PassErrmsg === '' && this.state.email !== '' && this.state.password !== ''){
      // alert("p")
      this.props.showLoader()

        this.setState({loading:true})
        this.props.LoginCheck(details)
        }
    }
   const ShowSuccessPopup=()=>{
     console.log("SuccessErrBlank4");
     if(this.state.CountMsg === 1){
      this.props.scloseModal()
      // this.setState({OpenSendMailPop:'true'})
      this.props.ActivteSuccesPopup()
      this.setState({CountMsg:2})
      // setTimeout(()=>{this.props.ActivteSuccesPopup()},4000)
    }
    }
    // alert(`${this.props.ErrMsg} er`)
    let LoginErrMsg= `${this.props.ErrMsg}`
    if(LoginErrMsg.length !== 0){
      // alert(LoginErrMsg.length)
      setTimeout(()=>{this.props.clearLoginErr()},3000)
    }else{
      // this.setState({fadeoutSec:""})
    }
  //  setTimeout(()=>{alert("okk")},2000)
  const responseGoogle = (response) => {
    // console.log(response.profileObj.email);
    const details={
      email:response.profileObj.email,
      password:13,   
    }
    this.props.LoginCheck(details)
  }
  const responseGoogleFailure=()=>{
    
  }
    return (
        <div id="LoginPopupBody">
        {/* <div className="formleft">
              <a style={{color:'orange',width:'2.5%',height:'4.8%',marginLeft:'-50.5%',marginTop:'',position:'absolute',backgroundColor:'transparent'}}
               onClick={this.scloseModal}>
                      &times;
        </a>
                <button id="bt">Sign In/Sign Up</button>
        </div>
        
              <div className="formright">
                   <div className="tab">
                  <button className="tablinks">{(this.state.openForget ===false)?'Sign In':'Forget Password'}</button>
                  <button className="tablinks1"  onClick={this.changeShowLS} >Sign Up</button><br/>
                  
              </div>
            
            
              <form style={{ padding:'040px',marginTop:'-2%' }} onSubmit={Login}>
        
                  
                        <input type="email" style={{ lineHeight:'0px',paddingTop:'0px',marginTop:'15%' }} name="email"
                        maxLength="50"
                         placeholder={(this.state.openForget ===false)?"Registered MyPustak Mobile Number Or Email ID":"Enter Email to Get Reset Password"}required
                         onChange={e => this.setState({email: e.target.value})} required
                         />
                       {(this.state.email === '') && <h6>{this.state.EmailErrrmsg}</h6>}
    
             
                        <input type="Password" style={{ lineHeight:'0px' }} name="password" maxLength="50"
                         placeholder="MyPustak Password"required id={(this.state.openForget ===false)?null:'NoPassword'}
                         onChange={e => this.setState({password: e.target.value})} required
                         />
                        {(this.state.password === '') && <h6>{this.state.PassErrmsg}</h6>}
                       <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>
         {(this.state.openForget ===false)?
         <div>
        <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,
        height:'0%',marginTop:'',marginBottom:'0px',lineHeight:'0px'}}>
            { LoginErrMsg}</p>
        <input style={{width:'40%',border:'none',marginLeft:'62%',padding:'0px',cursor:'pointer'}} type="button" value="Forgot Password" maxLength="50"
        onClick={this.ForgetPassword}/>

        <input style={{ backgroundColor:'#00baf2',color:'white',borderRadius:'5px',border:'none',
        width:'59.2%',height:'10.2%',marginLeft:'2.2%',marginTop:'2%' ,padding:'0px', 
        }} id={(this.state.loginCliked)?"loginBtn":null} 
         type="submit" value="Log In Securely" 
        /><br/>

        <label style={{color:'black',marginTop:'-1.1%',position:'absolute',marginLeft:'16%'}}><b>OR</b></label>
        <label style={{color:'black',marginTop:'6.1%'}}>Simply Signup In Using</label>

        <img src={require('./images/fb.png')} style={{ width:'8%' }},position:'absolute',marginTop:'15%',marginLeft:'-30%'}}></img>
        <img src={require('./images/google.png')} style={{width:'8%',position:'absolute',marginTop:'15%',marginLeft:'-19%'}}></img>
        <label style={{ color:'black',fontWeight:'normal',marginTop:'3%'>ByLoggingIn,YouAgreeToOur<b>Terms&</b></label>
<labelstyle=color:'black',fontWeight:'normal',marginLeft:'-55%',marginTop:'9%',position:'absolute'><b>Conditions</b>&PrivacyPolicy.</label> }}</div>
        :
        <div>

        <div id="ForgotCaptcha">
        <span ></span>
        <div id="Forgotdisplaycaptcha">
        <RCG result={this.result} />
        </div>
        <input type='text' name="captcha" className={'xxx'} placeholder="Enter Captcha"
        ref={ref => this.captchaEnter = ref} id="ForgetCapataInput" maxLength="50"
         onChange={e => this.setState({captcha: e.target.value})} required/>
        </div>
        
        {this.state.CaptchaErr}
  
        <input style={{width:'40%',backgroundColor:'white',border:'none',marginLeft:'62%',padding:'0px',cursor:'pointer'}} type="button"
        value="Forgot Password" maxLength="50"
        onClick={this.ForgetPassword}/>
        <p style={{ color:'red',fontWeight:'normal',lineHeight:'0px',paddingBottom:'1%' }}>{(this.state.ForgetResponse===0)?null:(this.state.ForgetResponse===200)?ShowSuccessPopup():"Enter Vaild email id" }</p>
          <input style={{ backgroundColor:'#00baf2',color:'white',borderRadius:'5px',border:'none',
          width:'59.2%',height:'10.2%',marginLeft:'2.2%',marginTop:'2%' ,paddingTop:'0px',fontSize:'140%',fontWeight:'bold'
          }}  type="submit" 
          onClick={this.CheckSubmitForgot}
           value="Click Here" />
        </div>}
        </form>
              </div> */}
          <div id="LoginpopupLeft">
              <p >Sign In/Sign Up</p>
          </div>
          <div id="LoginpopupRight">
        
                <a style={{ backgroundColor:'transparent',marginTop:"-4%",marginLeft:'24%',color:'red',position:'absolute',fontSize:'1.2rem'}}
                    onClick={this.scloseModal}>
                      &times;
                </a>
              <p><span className="BtnPopUptop" id="ULShowClickedBtn">{(this.state.openForget ===false)?'Sign In':'Forget Password'}</span><span className="BtnPopUptop" onClick={this.changeShowLS} >Sign Up</span></p>
              <div>
                <form onSubmit={Login}  style={{ padding:'0% 10%',marginTop:'5%',}}>
                <input type="email" style={{ lineHeight:'0px',paddingTop:' 4%',height:'20%'}} name="email"
                        maxLength="50" value={this.state.email}
                         placeholder={(this.state.openForget ===false)?"Registered MyPustak Email ID":"Enter Email to Get Reset Password"}required
                         onChange={e => this.setState({email: e.target.value})} required
                         />
                       {(this.state.email === '') && <h6>{this.state.EmailErrrmsg}</h6>}
    
             
                        {/* <input type="Password" style={{ lineHeight:'0px' }} name="password" maxLength="50"
                         placeholder="MyPustak Password"required id={(this.state.openForget ===false)?null:'NoPassword'}
                         onChange={e => this.setState({password: e.target.value})} required
                         /> */}
                        {/* <button id={(this.state.openForget ===false)?"ForgetPassBtn":'NoPassword'}
                              onClick={this.ForgetPassword} type="button">Forgot Password</button> */}
                       <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>
                       {(this.state.openForget ===false)?
         <div>
                          {(this.state.password === '') && <h6>{this.state.PassErrmsg}</h6>}

                             <input type="Password" style={{ lineHeight:'0px' }} name="password" maxLength="50"
                         placeholder="MyPustak Password"required id={(this.state.openForget ===false)?null:'NoPassword'}
                         onChange={e => this.setState({password: e.target.value})} required value={this.state.password}
                         />
        <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,
        height:'0%',marginTop:'',marginBottom:'0px',lineHeight:'0px'}}>
            { LoginErrMsg}</p>
            <input style={{width:'40%',border:'none',marginLeft:'62%',padding:'0px',cursor:'pointer'}} type="button" value="Forgot Password" maxLength="50"
        onClick={this.ForgetPassword}/>
            <div style={{ textAlign:'center' }}>    
                 <input className="LoginBtnHomePopup" id={(this.state.loginCliked)?"loginBtn":null} 
                type="submit" value="Log In Securely" 
                />
            </div>
  <GoogleLogin 
  theme="dark"
   onSuccess={responseGoogle}
    onFailure={responseGoogleFailure} 
    buttonText="Sign In"
    clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
    cookiePolicy={'single_host_origin'} />
        {/* <p style={{ textAlign:'center',display:'block' }}>OR</p>
        <p style={{ textAlign:'center',display:'block',fontWeight:'700',fontSize:'110%'}}>Simply Signup In Using</p>
        <div style={{ display:'flex',  }}>
        <span style={{ flex:'1',}}><img src={require('./images/fb.png')} style={{ height:'65%',width:'22%',float:'right',marginRight:'5%'}}></img></span>
        <span style={{ flex:'1'}}><img src={require('./images/google.png')} style={{ height:'65%',width:'22%',float:'left',marginLeft:'5%'}}></img></span>
        </div> */}

        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%', marginTop:'10%'}}> By Logging In,You Agree To Our <b> Terms &</b></p>
        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%' }}><b> Conditions </b> & PrivacyPolicy.</p>

       </div>
        :
        <div>

<Popup 
                       open={this.state.OpenForgetLoader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.state.OpenForgetLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>

        <div id="ForgotCaptcha">
        <span ></span>
        <div id="Forgotdisplaycaptcha">
        <RCG result={this.result} />
        </div>
        <input type='text' name="captcha" className={'xxx'} placeholder="Enter Captcha"
        ref={ref => this.captchaEnter = ref} id="ForgetCapataInput" maxLength="50"
         onChange={e => this.setState({captcha: e.target.value})} required/>
        </div>
        
        {this.state.CaptchaErr}
  
        <input style={{width:'40%',backgroundColor:'white',border:'none',marginLeft:'62%',padding:'0px',cursor:'pointer'}} type="button"
        value="Forgot Password" maxLength="50"
        onClick={this.ForgetPassword}/>
        <p style={{ color:'red',fontWeight:'normal',lineHeight:'0px',paddingBottom:'1%' }}>{(this.state.ForgetResponse===0)?null:(this.state.ForgetResponse===200)?ShowSuccessPopup():"Enter Vaild email id" }</p>
          <input id="ClickHereBtnForgetPass" type="submit" 
          onClick={this.CheckSubmitForgot}
           value="Click Here" />
        </div>}
                </form>
              </div>
          </div>
    </div>
    )
  }
}
const mapStateToProps = state => ({
  ErrMsg:state.accountR.ErrMsg,
  loader:state.accountR.loading,
})
export default connect(mapStateToProps,{LoginCheck,showLoader,clearLoginErr,ActivteSuccesPopup})(UserLogin)