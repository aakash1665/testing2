import React, { Component } from 'react';
import './bookguidlines.css';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import {Helmet} from 'react-helmet';

// import React from 'react';

class BookGuidlines extends Component {
  componentDidMount(){
    // console.log(getfaq())
    window.scrollTo(0, 0);
  }  ; 
  render() {
    return (
      <div>
      <MainHeader/>
      <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style></Helmet>
      <div id="bgdiv"> 
      <p id="bg">Book Condition Guidelines</p>
       <hr id="bghr"/>
       <p id="bgpara">Before You Donate A Book Remember That Someone Will Be 
Getting Just As Much Joy From Reading It As You Did.If The 
Condition of Book Is Not Up To The Mark Your Effort Of Sending 
And Our Efforts Of Restoration Will Not Be Fruitful,So Kindly 
Avoid Sending Books Which Are Not Up To The Mark.</p>
<p id="bgpara1">
We At MyPustak Will Not Be Able To Restore Reuse Books With:
</p>
<hr id="bghr1"/>
<ul id="guidlines">
  <li>
  Significant Damage To The Binding </li>
  <li>Torn Cover Pages</li>
  <li>Missing, Loose Or Torn Pages</li>
  <li>Strong Smell Of Any Kind (Including Cigarette Order)</li>
  <li>Water, Moisture Or Mould Mamage</li>
  <li>Workbooks With Writing In Them (e.g. Tests Or 
 Worksheets Partly Or Fully Completed)</li>
  <li> Corrected Or Uncorrected Proof Copies Of Books</li>
  <li> Book Club Editions</li>
  <li>Fire Or Smoke Damage</li>
  <li>Misprinted</li>
  <li> Stained</li>
  <li>Pirated Or Photocopied Books</li>
  <li>Newspapers</li>
  <li> Magazines</li>
  <li>Promotional  Copies</li>
  <li> Porn Material</li>
</ul>

</div>
          <MainFooter/>
                    </div>
 );
  }
}

export default BookGuidlines;