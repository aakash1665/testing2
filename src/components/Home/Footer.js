import React, { Component } from 'react';
import './footer.css';
// import ourlink from './images/ourlinkIcon.png';
// import ourstory from './images/ourStoryIcon.png';
// import paymentgate from './images/footerPaymentGatewayIcon.png';
// import fb from './images/fb.png';
// import twitter from './images/footerTwitter.png';
// import visa from './images/footerVisa.png';
// import mastercard from './images/Mastercard.png';
// import rupay from './images/footerRupay.png';
// import netbanking from './images/NetBanking.png';
// import wallet from './images/footerwalleticon.png';
// import upi from './images/footerupi.png';
// import logesticpart from './images/LogesticPartnerIcon.png';
// import delivery from './images/Deliveryicon.png';
// // import aramex from './images/Aramex.png';
// import fedex from './images/FedexIcon.png';
class App extends Component {
  render() {
    return (
    	
   <div>
<footer style={{backgroundColor:'black',marginTop:'15%',width:'100%',paddingBottom:'30.72%'}}>

<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ourlinkIcon.png`} style={{width:'2%',marginLeft:'11%',marginTop:'5%',position:'absolute'}}></img>
    <label id="fp">OUR LINKS</label>

<ul id="ful">
<li><a href="">Contact Us</a></li>
<li ><a href="">About Us</a></li>
<li ><a href="">Book Conditinal Guidelines</a></li>

<li ><a href="">Privacy Policy</a></li>
<li ><a href="">Terms And Condition</a></li>
</ul>


 <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ourStoryIcon.png`} style={{width:'2%',marginLeft:'32.5%',marginTop:'5%',position:'absolute'}}></img>  
 <label style={{marginLeft:'35%',marginTop:'5.3%',color:'white',position:'absolute'}}>OUR STORY</label>
<ul id="ful1">
<li><a href=""> Home</a></li>
<li ><a href="">Our Proud Donars</a></li>
<li ><a href="">FAQ</a></li>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fb.png`} style={{width:'12%',marginLeft:'6.5%',marginTop:'1%',position:'absolute'}}></img>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerTwitter.png`} style={{width:'12%',marginLeft:'23%',marginTop:'1%',position:'absolute'}}></img>
</ul>


 <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerPaymentGatewayIcon.png`} style={{width:'2.2%',marginLeft:'50%',marginTop:'5.2%',position:'absolute'}}></img>  
 <label style={{marginLeft:'52.8%',marginTop:'5.5%',color:'white',position:'absolute'}}>PAYMENT GATEWAY</label>
<p style={{color:'white',width:'21%',position:'absolute',marginLeft:'51%',marginTop:'9%',lineHeight:'1.7'}}>  

<span style={{color:'#f9a954'}}>Payment Through MyPustak is 
100% secured</span><br/>
Our website is secured by 256 bit 
SSL encryption issued by Verisign Inc, 
making your shopping at Sapnaonline 
as secure as possible.</p>


<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerVisa.png`} style={{width:'2.8%',marginLeft:'51%',marginTop:'22%',position:'absolute'}}></img>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Mastercard.png`} style={{width:'2.9%',marginLeft:'54.1%',marginTop:'22%',position:'absolute'}}></img>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/NetBanking.png`} style={{width:'2.8%',marginLeft:'57.4%',marginTop:'22.6%',position:'absolute'}}></img>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/NetBanking.png`} style={{width:'4.5%',marginLeft:'60.5%',marginTop:'22%',position:'absolute'}}></img>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerwalleticon.png`} style={{width:'2.3%',marginLeft:'65.3%',marginTop:'21.6%',position:'absolute'}}></img>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerupi.png`} style={{width:'3%',marginLeft:'67.9%',marginTop:'22.3%',position:'absolute'}}></img>

<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/LogesticPartnerIcon.png`} style={{width:'2%',marginLeft:'77%',marginTop:'5%',position:'absolute'}}></img>  
<label style={{marginLeft:'79.5%',marginTop:'5.3%',color:'white',position:'absolute'}}>OUR LOGISTIC PARTNER</label>

<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Deliveryicon.png`} style={{width:'3.8%',marginLeft:'77%',marginTop:'8.6%',position:'absolute'}}></img>
<img src={'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Aramex.207d1fcb.png'} style={{width:'6%',marginLeft:'81.7%',marginTop:'9%',position:'absolute'}}></img>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/FedexIcon.png`} style={{width:'4%',marginLeft:'89%',marginTop:'8.6%',position:'absolute'}}></img>
</footer>

    </div>
    );
  }
}

export default App;
