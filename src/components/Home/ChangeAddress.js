import React, { Component } from 'react';
import './ChangeAddress.css';
// import add from './images/add.jfif';
import {Helmet} from 'react-helmet';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import {logout,Getaddress,userdetails,Editaddress ,addAddressAction,AfterLoginRedirect} from '../../actions/accountAction'
import { connect } from 'react-redux';
import Popup from 'reactjs-popup' 
import UEditAddress from '../delivery/UEditAddress'
import AddUserAddress from '../delivery/AddUserAddress'
// import del from './images/del.png'
import config from 'react-global-configuration'
import axios from 'axios'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import MediaQuery from 'react-responsive';
// import {Link} from 'react-router-dom'

class ChangeAddress extends Component {

  state={
    Uadd:'',
    name:'',
    email:'',
    phone:'',
    addressopen:false,
    addaddressopen:false,
    // GetAddressofUser:this.props.getadd,
    UpdatedUserAddState:'',
    AddedAddressState:'',
  }
  componentDidMount() {
    if(localStorage.getItem('user') === null){
      // alert("RED")
      // this.props.AfterLoginRedirect(window.location.pathname)
      window.location.href="/"
      // return <Redirect to="/"></Redirect>
      // alert(window.location.pathname)
    }
    window.scrollTo(0, 0);
    const details=`Token ${localStorage.getItem('user')}`
    this.props.userdetails(details)
    this.props.Getaddress(details)
    this.props.addAddressAction(details)
  }
close=()=>{
  this.setState({ addaddressopen: false })
}
  addressopenModal=(data)=>{
    this.setState({ addressopen: true })
    console.log(data)
    this.props.Editaddress(data)
  }

  addresscloseModal=()=>{
    
    this.setState({ addressopen: false })
   
  }
  addaddressopenModal=()=>{
    this.setState({ addaddressopen: true })
  }

  addaddresscloseModal=()=>{
    
    this.setState({ addaddressopen: false })
   }
   DeleteDone=()=>{
      // window.location.href = `/customer/customer_account`
     }
    // DeleteSelectedAddress=()=>{
    //   alert("okk")
    // }
   Deleteaddress=(address_id)=>{
    // alert(address_id)
    const passdata = {
       "address_id" : address_id
      }
    // const token=`Token da0a3bed7fd86b67f0cddd7f49248813a14f00f4`
    const token=`Token ${this.props.userToken}`
    // e.preventDefault();
    // console.log(passdata)
    axios.post(`${config.get('apiDomain')}/api/v1/post/delete_address`,passdata,{headers:{
      'Authorization': token,
    }}  
    ).then(res=>{
      const details=`Token ${this.props.userToken}`
      this.props.Getaddress(details)
      // console.log(`${res.data}`);
      }).catch(err=>console.log(err));
};


  componentWillReceiveProps(UpdatedUserAdd){
    if(UpdatedUserAdd.UpdatedUserAdd !== this.state.UpdatedUserAddState ){
      const details=`Token ${this.props.userToken}`
      // this.props.userdetails(details)
      this.props.Getaddress(details)
      this.setState({UpdatedUserAddState:UpdatedUserAdd.UpdatedUserAdd})
      // this.setState({AddedAddressState:AddedAddress.AddedAddress})
    }
  }

  render() {
    if(localStorage.getItem('user') === null){
      // alert("RED")
      // this.props.AfterLoginRedirect(window.location.pathname)
      // window.location.href="/"
      // return <Redirect to="/"></Redirect>
      // alert(window.location.pathname)
    }
const {getadd}=this.props
const {uaddress}=this.props
    return (
 
      <div id="bodyCH">
    <MainHeader/>
    <div>
    <MediaQuery maxWidth={538}>
      <div style={{marginTop:'21%'}}></div>
      <Helmet><style>{'body{background-color:white}'}</style></Helmet>
      </MediaQuery>
      <MediaQuery minWidth={539}>
     <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style></Helmet></MediaQuery>
      <p className="User-Personal-Information "> User Personal Information</p>
      <hr id="mline"/>

      <label id="name">Name : <span style={{color:'black'}}>{this.props.getuserdetails.name}</span></label>
      <label id="email">Email ID : <span style={{color:'black'}}>{this.props.getuserdetails.email}</span></label>
      <label id="phone">Mobile Number : <span style={{color:'black'}}>{this.props.getuserdetails.phone}</span></label>
      <br/>
      {/* {this.props.getuserdetails.phone} */}
      <label id="useraddress">Address: </label>


      <Link to={`/forpassreset/token=${this.props.userToken}`}>
 <Button variant="primary" size="lg"  className="mbtn">
    Change Password
</Button>
</Link>
  <Button variant="light" size="lg" className="mdiv1" onClick={()=>this.addaddressopenModal()}>
  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/add.5c990599.jfif`} alt="Add" id="Addimg"></img>
           <p id="para">Add New Address</p>
    </Button>
     
        <div style={{display: 'inline-block'}}>
{getadd.map(uaddress=>
//  var selectedADD= uaddress
<div id="address1">
<button id="useredit" onClick={()=>this.addressopenModal(uaddress)} className="button">Edit
</button><span id="addressspan">{uaddress.address}</span><br/>
<span id="addressspan">{uaddress.landmark}</span><br/>
<span id="addressspan">{uaddress.city_name}</span> <br/>
<span id="addressspan">{uaddress.pincode}</span> <br/>
<span id="addressspan">{uaddress.state_name}</span> <br/>
<span id="addresstitle">{uaddress.title}</span>
 <button className="button" style={{ float: 'right',backgroundColor: '#f3f7f8',border: 'none',marginTop: '-3%'}}
  onClick={()=>{this.Deleteaddress(uaddress.address_id)}}>
 <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png`} alt="delete" id="addressdelete" ></img>
 </button>
</div>
 )}
    </div>
    <MediaQuery minWidth={992}>
 <Popup
          open={this.state.addressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addressopen:false})}
          contentStyle={{ 
                        // width:'58.9%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'67%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addresscloseModal} style={{
    color: 'white',
    marginLeft: '-7%',float:'left'}}>
              &times;
            </a> */}
        <UEditAddress uaddress= {uaddress} CloseEditAddress={()=>this.setState({addressopen:false})}/>
        </div>
        </Popup>
        <Popup
          open={this.state.addaddressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addaddressopen:false})}
          contentStyle={{ 
                        // width:'58.9%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'67%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addaddresscloseModal} style={{
    color: 'black',zIndex:'999',
    float:'left'}}>
              &times;
            </a> */}
        <AddUserAddress closemodule ={this.close}  CloseAddUserAddress={()=>this.setState({addaddressopen:false})}/>
        </div>
        </Popup>
        </MediaQuery>
        {/* tab */}
        <MediaQuery minWidth={768} and maxWidth={991}>
 <Popup
          open={this.state.addressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addressopen:false})}
          contentStyle={{ 
                        // width:'40%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'56%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addresscloseModal} style={{
    color: 'white',
    marginLeft: '-7%',float:'left'}}>
              &times;
            </a> */}
        <UEditAddress uaddress= {uaddress} CloseEditAddress={()=>this.setState({addressopen:false})}/>
        </div>
        </Popup>
        <Popup
          open={this.state.addaddressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addaddressopen:false})}
          contentStyle={{ 
                        // width:'40%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'56%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addaddresscloseModal} style={{
    color: 'black',zIndex:'999',
    float:'left'}}>
              &times;
            </a> */}
        <AddUserAddress closemodule ={this.close}  CloseAddUserAddress={()=>this.setState({addaddressopen:false})}/>
        </div>
        </Popup>
        </MediaQuery>
        <MediaQuery maxWidth={767} and minWidth={539}>
 <Popup
          open={this.state.addressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addressopen:false})}
          contentStyle={{ 
                        width:'57%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'43%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addresscloseModal} style={{
    color: 'white',
    marginLeft: '-7%',float:'left'}}>
              &times;
            </a> */}
        <UEditAddress uaddress= {uaddress} CloseEditAddress={()=>this.setState({addressopen:false})}/>
        </div>
        </Popup>
        <Popup
          open={this.state.addaddressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addaddressopen:false})}
          contentStyle={{ 
                        width:'57%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'43%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addaddresscloseModal} style={{
    color: 'black',zIndex:'999',
    float:'left'}}>
              &times;
            </a> */}
        <AddUserAddress closemodule ={this.close}  CloseAddUserAddress={()=>this.setState({addaddressopen:false})}/>
        </div>
        </Popup>
        </MediaQuery>
        <MediaQuery maxWidth={538}>
 <Popup
          open={this.state.addressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addressopen:false})}
          contentStyle={{ 
                        width:'70%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'30%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addresscloseModal} style={{
    color: 'white',
    marginLeft: '-7%',float:'left'}}>
              &times;
            </a> */}
        <UEditAddress uaddress= {uaddress} CloseEditAddress={()=>this.setState({addressopen:false})}/>
        </div>
        </Popup>
        <Popup
          open={this.state.addaddressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addaddressopen:false})}
          contentStyle={{ 
                        width:'70%',
                        height:'100%',
                        borderRadius:'5px',
                        marginLeft:'30%',
                        }} 
        > 
        <div >
            {/* <a className="close" onClick={this.addaddresscloseModal} style={{
    color: 'black',zIndex:'999',
    float:'left'}}>
              &times;
            </a> */}
        <AddUserAddress closemodule ={this.close}  CloseAddUserAddress={()=>this.setState({addaddressopen:false})}/>
        </div>
        </Popup>
        </MediaQuery>
{/* 
<div id="address1">Narayanpur,Rajarhat
Kolkata 700136
 West Bengal
</div>  */}

{/* <br/> */}
{/* <div>

</div> */}
{/* <div style={{paddingBottom:'2.5rem'}}>
 <p id="chpass">Change Password :</p>
 <Link to={`/forpassreset/token=${this.props.userToken}`}>

 <button type="submit" id="passmbtn"> Change Password</button>
 </Link>
 </div> */}
{/* <input type="text" placeholder="Enter Old Password" name="search" className="text"></input> */}
  {/* <InputGroup size="lg" className="text">
    <FormControl aria-describedby="basic-addon1" placeholder="Enter Old Password" />
  </InputGroup>
<br/> */}
{/* <input type="text" placeholder="Enter New Password" name="search" className="text"></input> */}
  {/* <InputGroup size="lg" className="text">
    <FormControl aria-describedby="basic-addon1" placeholder="Enter New Password"/>
  </InputGroup>
<br/> */}
{/* <input type="text" placeholder="Re-enter Your Password" name="search" className="text"></input> */}
  {/* <InputGroup size="lg" className="text">
    <FormControl aria-describedby="basic-addon1" placeholder="Re-enter Your Password" />
  </InputGroup>
    <br/> */}
     {/* <button type="submit" className="mbtn"> Change Password</button> */}
  <div >
  </div>
     <MainFooter/>
     </div>
      </div>
  
 );
}
}

const mapStateToProps = state => ({
  getuserdetails:state.userdetailsR.getuserdetails,
  getadd:state.getAddressR.getadd,
  userToken:state.accountR.token,
  editadd:state.accountR.editadd,
  UpdatedUserAdd:state.accountR.getuseradd,
  AddedAddress:state.accountR.added_address,
})
export default connect(mapStateToProps,{Getaddress,userdetails,Editaddress,addAddressAction,AfterLoginRedirect})(ChangeAddress);


