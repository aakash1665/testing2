import React, { Component } from 'react'
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import GBook from './GBook'
import axios from 'axios'
import './category.css'
import {Breadcrumbs} from 'react-breadcrumbs-dynamic'
import { connect } from 'react-redux';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import config from 'react-global-configuration'
import InfiniteScroll from 'react-infinite-scroll-component';
import MediaQuery from 'react-responsive';
import Mobilecat from './mobilecategory'
import ReactGA from 'react-ga';
import { Helmet } from "react-helmet";
import { getSeoData } from "../../actions/seodataAction";
import GetcategoryID from "./GetCategoryId"
// import {Breadcrumbs} from 'react-breadcrumbs-dynamic'

// http://103.217.220.149:80/api/v1/get/category/competitive-exams/engineering/engineering-post-graduate/1/
// http://103.217.220.149:80/api/v1/get/category/competitive-exams/engineering/engineering-post-graduate/1/
// http://103.217.220.149:80/api/v1/get/category/competitive-exams/engineering/engineering-post-graduate/1/
// http://localhost:3000/category/competitive-exams
// http://localhost:3000/category/competitive-exams/
class Category extends Component {
    state={
        GetBookResult:[],
        start:1,
        MEWURL:'',
        COUNT:1,
        ShowCategory:'',
        display:0,
        menuS:'none',
        showLS:true,
        seoCount:2,
        ChangeBreadCrumLink1:false,
        ChangeBreadCrumLink2:false,
        ChangeBreadCrumLink3:false,

    }

    // getCategoryId=(category)=>{
    //     switch (category) {
    //       case value:
            
    //         break;
        
    //       default:
    //         break;
    //     }
    // }



     url= window.location.href;
    //  urlLength = this.url.length
     CatIndex = this.url.indexOf("/category/")
     newUrl = this.url.slice(this.CatIndex)
    componentDidMount() {
        ReactGA.pageview(window.location.pathname + window.location.search);

        window.scrollTo(0, 0);
        window.addEventListener('scroll', this.handleScroll);

        console.log(`${this.newUrl}`,"Url");
        let CATID=GetcategoryID(this.newUrl)
        console.log(CATID,"GotCAtID");
        
        this.setState({ShowCategory:this.newUrl})
        axios.get(`${config.get('apiDomain')}api/v1/get${this.newUrl}${this.state.start}/`)
        .then(res=>this.setState({GetBookResult:res.data}))
        .catch(err=>console.log(err,`${config.get('apiDomain')}/api/v1/get${this.newUrl}${this.state.start}/`,"catErr")
        )

        // if(this.state.seoCount == 2){
        
          this.props.getSeoData(
            "https://www.mypustak.com" + window.location.pathname
          );
          this.setState({seoCount:1})
  
        //   }
      }
      // componentWillUpdate(){
      //   this.props.getSeoData(
      //     "https://www.mypustak.com" + window.location.pathname
      //   );
      // }
      componentWillReceiveProps(nextProps){

        console.log("data in willRec");

        if(this.state.ShowCategory !== nextProps.ClickedCategory && nextProps.ClickedCategory !==""){

          // if(this.state.seoCount == 1){
            // this.props.getSeoData(
            //   "https://www.mypustak.com" + window.location.pathname
            // );
            // this.setState({seoCount:4})
          // }
          console.log("data in willRec1");
          
            axios.get(`${config.get('apiDomain')}/api/v1/get${nextProps.ClickedCategory}${this.state.start}/`)
            // axios.get(`${config.get('apiDomain')}/api/v1/get${this.props.ClickedCategory}/${this.state.start}/`)
            .then(res=>{
              console.log(nextProps.ClickedCategory,"next_ok",this.state.ShowCategory)
              this.setState({GetBookResult:res.data,ShowCategory:nextProps.ClickedCategory});
                          this.props.getSeoData(
              "https://www.mypustak.com" + window.location.pathname
            );
          })
            .catch(err=>{console.log(err,`${config.get('apiDomain')}/api/v1/get${nextProps.ClickedCategory}${this.state.start}/`,nextProps.ClickedCategory,"next",this.state.ShowCategory)
                  
                      if(err.response.status=="404"){
                        console.log("in status 404 ");
                        
                        axios.get(`${config.get('apiDomain')}/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
                        .then(res=>{
                          console.log(nextProps.ClickedCategory,"next_ok",this.state.ShowCategory)
                          this.setState({GetBookResult:res.data,ShowCategory:nextProps.ClickedCategory});
                                      this.props.getSeoData(
                          "https://www.mypustak.com" + window.location.pathname
                        );
                                    })
                        .catch(err=>{console.log(err,"error in catch");
                        }) }
          }
            ) 
            this.setState({GetBookResult:[],start:1})
            window.scrollTo(0, 0);

        }
      }
    //   change= this.props.change
    //   getDerivedStateFromProps(){

    //   }
    //   api/v1/get/category/ competitive-exams/engineering/iit/<int:pg>/
    // /api/v1/get/category/ competitive-exams/enginnering/iit/1/
  render() {
            // if(this.state.MEWURL){
            //     axios.get(`${config.get('apiDomain')}/api/v1/get${this.props.ClickedCategory}/${this.state.start}/`)
            //     .then(res=>this.setState({GetBookResult:res.data}))
            //     .catch(err=>console.log(err)
            //     )
            // }
    const seoData = this.props.SeoData;
      const fetchBooks=()=>{
        // var searchValue = this.refs.searchBookFixed.value
        console.log(this.newUrl);
        this.setState({start:this.state.start+1})
        // var page = this.state.start
        
        // console.log(this.state.start);

        axios.get(`${config.get('apiDomain')}/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
        .then(res=> {this.setState({GetBookResult:this.state.GetBookResult.concat(res.data)})})
        .catch(err=>console.log(err)
        )
        // alert(`${config.get('apiDomain')}/api/v1/get${this.state.ShowCategory}${this.state.start}/`)
      }
      let CategoryUrl= this.state.ShowCategory
      let ReplacedCategoryUrl= CategoryUrl.replace( /[/]/g, "," );
      let ResultUrl=ReplacedCategoryUrl.split(',')
      // ResultUrl=ResultUrl.pop();
      console.log(ResultUrl,"ResultUrl",ResultUrl.length);
      let ListOfLinks=[]
            ResultUrl.forEach((element,index) => {
              console.log(element,index);
              if(element!=="category" && index!==1  && index!==5 ){
                if(index == 2 ){
                  ListOfLinks.push(<span><a href={`/category/${element}/`} style={(this.state.ChangeBreadCrumLink1)?{
                    color:'#2874f0',
                    marginRight:'1%',
                    textDecoration:'None'
                  }:
                {
                  color:'grey',
                  marginRight:'1%',
                  textDecoration:'None'
                }
                }
                  onMouseEnter={()=>this.setState({ChangeBreadCrumLink1:!this.state.ChangeBreadCrumLink1})}
                  onMouseLeave={()=>this.setState({ChangeBreadCrumLink1:!this.state.ChangeBreadCrumLink1})}
                  
                  >{element} </a></span>)
                }else{
                  if(index == 3 && ResultUrl.length > 4){
                      console.log(this.state.ShowCategory.lastIndexOf(element),33,this.state.ShowCategory.length,);
                      var links = this.state.ShowCategory.split('/')
                      var onclickLinkThird=""
                      onclickLinkThird.concat(`${links[3]}`)
                      console.log(onclickLinkThird,"Thi",links[3]);
                      ListOfLinks.push(<span>> <a href={`/category/${links[2]}/${links[3]}/`} style={(this.state.ChangeBreadCrumLink2)?{
                        color:'#2874f0',
                        marginRight:'1%',
                        textDecoration:'None'
                      }:
                    {
                      color:'grey',
                      marginRight:'1%',
                      textDecoration:'None'
                    }
                    }
                      onMouseEnter={()=>this.setState({ChangeBreadCrumLink2:!this.state.ChangeBreadCrumLink2})}
                      onMouseLeave={()=>this.setState({ChangeBreadCrumLink2:!this.state.ChangeBreadCrumLink2})}
                    >{element}</a></span>)
                  }else{
                  
                  }
                  if(index == 4 && ResultUrl.length > 5){
                      ListOfLinks.push(<span>> <a href={this.state.ShowCategory}  style={(this.state.ChangeBreadCrumLink3)?{
                        color:'#2874f0',
                        marginRight:'1%',
                        textDecoration:'None'
                      }:
                    {
                      color:'grey',
                      marginRight:'1%',
                      textDecoration:'None'
                    }
                    }
                      onMouseEnter={()=>this.setState({ChangeBreadCrumLink3:!this.state.ChangeBreadCrumLink3})}
                      onMouseLeave={()=>this.setState({ChangeBreadCrumLink3:!this.state.ChangeBreadCrumLink3})}
                      
                      >{element} </a></span>)
                  }

                }

            }
            });
      
    return (
      <div>
                    <Helmet>
          <title>{seoData.title_tag}</title>
          <meta name="description" content={seoData.meta_desc} />
        </Helmet>
            <MainHeader/>
            <MediaQuery maxWidth={991}>
            <Mobilecat/>
            </MediaQuery>
            {/* <Helmet>
          <title>{seoData.title_tag}</title>
          <meta name="description" content={seoData.meta_desc} />
        </Helmet> */}
            {/* <div id="filter" ><ul>
                <li >Filter by</li>
                <li style={{ display:'flex' }}><input type="checkbox" style={{ flex:'1' }}/><span style={{ flex:'1' }}>In-stock</span></li>
                <li>Author/Publisher</li>
                <li>Condition</li>
                <li>Rating</li>
                <li>Most-recent</li>
                <li>Languages</li>

            </ul></div> */}
            <div>
</div>
                 {/* Desktop */}
 <MediaQuery minWidth={992} >
 <div id="categoryBody" >
  {ListOfLinks}

{/* -------------------------------Infinite Scroll----------------------------------------------------- */}
<div style={{ color:'black' }}>
<InfiniteScroll
                 dataLength = {this.state.GetBookResult.length}
                scrollThreshold={'70%'}
                 next={fetchBooks}
                 hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
                 >



          {this.state.GetBookResult.map((bookdata)=>
             //  console.log(bookdata);
              
             <GBook key={bookdata.slug}
                 book={bookdata}
                 />
              )
              }
              {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
          

              </InfiniteScroll>


              </div>
          {/* ------------------------------------------------------------------------ */}
          </div>
             
 </MediaQuery>

{/* Tab */}
  <MediaQuery minWidth={768} maxWidth={991} > 
             <div id="categoryBody" >


{/* -------------------------------Infinite Scroll----------------------------------------------------- */}
<div style={{ color:'black' }}>
              <InfiniteScroll
              dataLength = {this.state.GetBookResult.length}
             scrollThreshold={'70%'}
              next={fetchBooks}
              hasMore={true}
             //  loader={<h4>NO RESULT FOUND</h4>}
             //  height={400}
              >



          {this.state.GetBookResult.map((bookdata)=>
             //  console.log(bookdata);
              
             <GBook key={bookdata.slug}
                 book={bookdata}
                 />
              )
              }
              {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
          

              </InfiniteScroll>


              </div>
          {/* ------------------------------------------------------------------------ */}
          </div>
             
             </MediaQuery>
             <MediaQuery maxWidth={767} minWidth={539} > 
             <div id="categoryBody" >


{/* -------------------------------Infinite Scroll----------------------------------------------------- */}
<div style={{ color:'black' }}>
              <InfiniteScroll
              dataLength = {this.state.GetBookResult.length}
             scrollThreshold={'70%'}
              next={fetchBooks}
              hasMore={true}
             //  loader={<h4>NO RESULT FOUND</h4>}
             //  height={400}
              >



          {this.state.GetBookResult.map((bookdata)=>
             //  console.log(bookdata);
              
             <GBook key={bookdata.slug}
                 book={bookdata}
                 />
              )
              }
              {/* {this.state.GetBookResult.map((bookdata)=>
              ((bookdata.document.is_out_of_stack !== `Y`)?
             <SBook key={bookdata.document.title} 
                 book={bookdata.document}
                 />:null  
              ))
              } */}
          

              </InfiniteScroll>


              </div>
          {/* ------------------------------------------------------------------------ */}
          </div>
             
             </MediaQuery>
             <MediaQuery  maxWidth={538}  > 

              <div id="categoryBody" >


   {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
   <div style={{ color:'black' }}>
                 <InfiniteScroll
                 dataLength = {this.state.GetBookResult.length}
                scrollThreshold={'70%'}
                 next={fetchBooks}
                 hasMore={true}
                //  loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
                 >



             {this.state.GetBookResult.map((bookdata)=>
                //  console.log(bookdata);
                 
                <GBook key={bookdata.slug}
                    book={bookdata}
                    />
                 )
                 }
                 {/* {this.state.GetBookResult.map((bookdata)=>
                 ((bookdata.document.is_out_of_stack !== `Y`)?
                <SBook key={bookdata.document.title} 
                    book={bookdata.document}
                    />:null  
                 ))
                 } */}
             

                 </InfiniteScroll>


                 </div>
             {/* ------------------------------------------------------------------------ */}
             </div>
             </MediaQuery>

            <MainFooter/>
      </div>
    )
  }
}

const mapStateToProps = state => ({
    ClickedCategory:state.homeR.ClickedCategory,
    SeoData: state.seodata.seodata
})
// export default  Category
export default connect(mapStateToProps, { getSeoData })(Category);