import React, { Component } from 'react';
/*import Header from './components/Layout/Header.js';*/
import axios from 'axios';

class DataItem extends Component {

  state = {
    items:[],
    isLoaded:false,
  };

  componentDidMount() {
           axios.get("{this.props.url}")
           .then(res=>this.setState({items:res.data, isLoaded:true}))

}
  render() {
      
      const {isLoaded, items} = this.state;
      console.log(items);
      console.log(isLoaded)
      if(isLoaded!=true){
        return <div>Loading...</div>
      } else {
        return (
          <div className="books">
             {items.map(item => (
               <div key={item.title} className="book"> 
                <img src="https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/thumbs/0787468001547273690.jpg" width="100" height="100"/>
                     <span>
                       <p>{item.title}</p>
                       <p>Rs: {item.price}</p>
                       <br/>
                     </span>
               </div>
             ))};
             </div>
        );
      }
  }
}
export default DataItem;
