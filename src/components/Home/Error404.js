import React, { Component } from 'react';
import {Helmet} from 'react-helmet';
//  import boy from './BOY404.png';
 import girl from './GIRL404.png';
//  import man from './MAN404.png';
 import error from './error.png';
 import './error404.css';

class Error404 extends Component {
  render() {

    // render={({ staticContext }) => {
      // if (staticContext) {
      //   staticContext.statusCode = 404
      // }
    //   return <NotFound />
    // }}
    // alert("okk")
    return (
        <div>
          <Helmet>
                <style>{'body { background-color: #b0e1ff;}'}</style>
            </Helmet>
    
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/GIRL404.cd779c44.png`} id="girl" alt="Profile image"></img>

        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/error.a84afbdd.png`} id="error" alt="Profile image"></img>
        <p id="layer1">You Are Lost In Space</p>
        <p id="layer2">Please Go Back To Your <a id="linkstyle" href=""> Homepage</a></p>
        
        </div>
    );
  }
}

export default Error404;