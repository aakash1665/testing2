import React, { Component } from 'react'
import axios from 'axios'
import Popup from 'reactjs-popup' 
import { connect } from 'react-redux';
import PUserSignup from './PUserSignup';
import PUserLogin from './PUserLogin'
import {doSearch,GetSearchResult,fetchBooksAcc,makeSearchBlank,setCategory} from '../../actions/homeAction'
import SBook from './SBook'
import InfiniteScroll from 'react-infinite-scroll-component';
import {AddToCart,CartopenModal,CartcloseModal,removeAllCart,RemoveCart,MobileCartRedirect} from '../../actions/cartAction'
import {logout,Getaddress,userdetails,Editaddress ,addAddressAction,getSavedToken} from '../../actions/accountAction'
import { Link,Redirect,browserHistory } from 'react-router-dom'
import Popupcat from './Popupcategories'
// import del from '../product/Product_Images/del.png';
import config from 'react-global-configuration'
import MediaQuery from 'react-responsive';
import homeicon from '../mobimages/homeicon.png'
import queryString from 'query-string';
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
// import Popupcat from './Home/Popupcategories'
import ReactGA from 'react-ga';

function decodeURIComponentSafe(s) {
  if (!s) {
      return s;
  }
  return decodeURIComponent(s.replace("%", '%20'));
}

class Search extends Component {
state={
        display:0,
        menuS:'block',
        name:'',
        SearchResult:this.props.SearchResultR,
        start:1,
        count:1,
        Cat:false,
        showLS:false,
        Sopen:false,
        userBookQty:1,
        cartMsg:null,
        // doRedirect:false,
        RedirectCart:false,
        ASignupopen:false,
        succesLogin:false,
        goTocart:false,
        logoutDone:false,
        BlankSerachRedirect:false,

}
componentDidMount() {
  ReactGA.pageview(window.location.pathname + window.location.search);

  let queryParameters = window.location.href.split('?')[1];

  if(queryParameters) {
    let serachValue = queryParameters.split('=')[1];
    console.log("hello");
    this.props.doSearch(decodeURIComponentSafe((serachValue)));
    this.props.GetSearchResult(decodeURIComponentSafe((serachValue)),this.state.start)
    if(serachValue === ''){
      // this.props.GetSearchGetBooks()
      this.setState({BlankSerachRedirect:true})
      // return <Redirect to="/get-books/"/>
    }
  }
  const value =localStorage.getItem('user');
  this.props.getSavedToken(value);
  this.setupBeforeUnloadListener();
  this.listAllItems()
  // window.addEventListener('scroll', this.handleScroll);
  var searchValue= this.refs.searchBookFixed.value
  // this.DoSearch(searchValue,this.state.start)
  // axios.get(`http://127.0.0.1:8000/search/${searchValue}/${this.state.start}/`)103.217.220.149:80
  axios.get(`${config.get('apiDomain')}/search/${searchValue}/${this.state.start}/`)
  .then(res=>this.setState({SearchResult:res.data.hits}))
  .catch(err=>console.log(err)
  )
}
componentWillReceiveProps(nextProps) {
  if(this.props.searchBook !== nextProps.searchBook) {
    this.props.history.push({
      pathname: '/search',
      search: `?value=${decodeURIComponentSafe(nextProps.searchBook)}`
    });
  }
}
listAllItems=()=>{  
  // alert("in see")
  if(sessionStorage.length !==0){
    this.props.removeAllCart()
 for (var i=0; i<=sessionStorage.length-1; i++)  
 {   
     let key = sessionStorage.key(i); 
     try {
      let val = JSON.parse(sessionStorage.getItem(key));
      console.log(val.bookInvId  )
      // val.map(det=>console.log(det))
      this.props.AddToCart(val)
     } catch (error) {
       
     } 

     // alert(val)
 } }

}
setupBeforeUnloadListener = () => {
  window.addEventListener("beforeunload", (ev) => {
      // ev.preventDefault();
      // sessionStorage.clear();
  });
};
// componentWillUnmount() {
//   window.removeEventListener('scroll', this.handleScroll);
// }
// handleScroll=()=>{
//   const searchValue= this.refs.searchBookFixed.value
//   const scroll = window.scrollY
//   this.setState({display:scroll});
//   console.log(this.state.display);
//   if(this.state.display < 284 ){
//     axios.get(`http://127.0.0.1:8000/search/${searchValue}/3/`)
//     .then(res=>this.setState({SearchResult:this.state.SearchResult.concat(res.data.hits)}))
//     .catch(err=>console.log(err)
//     )
//   }
//   if(this.state.display === 142 ){
//     axios.get(`http://127.0.0.1:8000/search/${searchValue}/2/`)
//     .then(res=>this.setState({SearchResult:this.state.SearchResult.concat(res.data.hits)}))
//     .catch(err=>console.log(err)
//     )
//   }
// }
OPENCategory=()=>{
  this.setState({Cat:true})
}
CLOSECategory=()=>{
  this.setState({Cat:false})
}
changeShowLS=()=>{
        this.setState({showLS:!this.state.showLS})
}
sopenModal=()=>{
    this.setState({ sopen: true })
  }
  scloseModal=()=>{
    this.setState({ sopen: false })
}
// DoSearch=(searchValue,page)=>{
//   // console.log("okkk");
  
//     axios.get(`http://127.0.0.1:8000/search/${searchValue}/${page}/`)
//     .then(res=>this.setState({SearchResult:this.state.SearchResult.concat(res.data.hits)}))
//     .catch(err=>console.log(err)
//     )
//   }
changeShowLS=()=>{
  this.setState({showLS:!this.state.showLS})
}
ToggleASignupOpenModal=()=>{
  // alert("onnnn")
  this.setState({ ASignupopen: !this.state.ASignupopen })
}
ASignupCloseModal=()=>{
  // alert("c")
  this.setState({ ASignupopen: false })
}
RemoveFromCart=(bookInvId,Cart_id)=>{

  for (var i=0; i<=sessionStorage.length-1; i++)  
  {   
    // alert("rmo")
      let key = sessionStorage.key(i);  
      try {
        if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){
            let val = JSON.parse(sessionStorage.getItem(key));
            if(`${val.bookInvId}` === `${bookInvId}`)  {
              // alert("rm")
              sessionStorage.removeItem(bookInvId);
            }
        }
      } catch (error) {
        console.log(error);
        
      }

      // val.map(det=>console.log(det))
      // this.props.AddToCart(val)
      // alert(val)
  }
  if(localStorage.getItem('user') === null){

  this.props.removeFromCartLogout(bookInvId)
  }else{
    const data={"is_deleted":"Y"}

  this.props.RemoveCart(Cart_id,bookInvId,data)

  }
 }
IncrementItem = () => {
  if(this.state.userBookQty >= this.props.cartDetails.bookQty){
    this.setState({cartMsg:'Book Out Of Stock'})
    setTimeout(()=>{this.setState({cartMsg:''})},2000)
  }else{
    
    this.setState({ userBookQty: this.state.userBookQty + 1 });
  }
}
DecreaseItem = () => {
  if(this.state.userBookQty !== 1){
  // this.setState({cartMsg:'Book Out Of Stock'})
  this.setState({ userBookQty: this.state.userBookQty - 1 });
}
}
  // getName(){
  //   axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
  //     'Authorization': `Token ${this.props.userToken}`
  //   }})
  //   .then(res=>this.setState({name:res.data.name}))
  //   .catch(err=>console.log(err)
  //   )
    
  // }
  fetchBooks=()=>{
    // var searchValue = this.refs.searchBookFixed.value
    console.log(this.state.start);
    this.setState({start:this.state.start+1})
    // var page = this.state.start

    this.props.fetchBooksAcc(this.refs.searchBookFixed.value,this.state.start)

    // console.log(this.state.start);
    // axios.get(`http://103.217.220.149:80/search/${this.props.searchBook}/${this.state.start}/`)
    // .then(res=> this.setState({SearchResult:this.state.SearchResult.concat(res.data.hits)}))
    // .catch(err=>console.log(err)
    // )
  }
  openNav=()=>{
    // alert("OpenNavs")
    this.setState({CloseNav:!this.state.CloseNav})
    // document.getElementById("mySidenav").style.display = "block";
  }

 closeNav=()=>{
  //  alert("change")
    // document.getElementById("mySidenav").style.display = "none";
    this.setState({CloseNav:!this.state.CloseNav})
  }

  onSearchChange = e => {
//     this.props.push({
//   pathname: this.props.location.pathname,
//   queryString.parse(props.location.search)
//   // search: stringifyQuery(Object.assign({}, parseQueryString(this.props.location.search), { foo: "bar" }))
// });
  // let params =queryString.parse(q[]=this.refs.searchBookFixed.value)
  // console.log(params);
  // alert(params)
  
  this.props.doSearch(e.target.value)
  // this.DoSearch(e.target.value,1)
  // this.props.SendSearchData(this.refs.searchBookFixed.value)
  
  this.props.GetSearchResult(this.refs.searchBookFixed.value,this.state.start)
    if(e.target.value === ''){
      // this.props.GetSearchGetBooks()
      this.setState({BlankSerachRedirect:true})
      // return <Redirect to="/get-books/"/>
    }
  // axios.get(`http://103.217.220.149:80/search/${this.refs.searchBookFixed.value}/${this.state.start}/`)
  //   .then(res=> {this.setState({SearchResult:res.data.hits}); console.log(res.data.hits);
  //   })
  //   .catch(err=>console.log(err)
  //   )
  }

  Userlogout=()=>{
    this.props.logout()
    localStorage.removeItem('user');
    // this.signupCloseModal()
    // window.location.href = ''
    // this.setState({Sopen:false})
    // return <Redirect to =''></Redirect>
    this.setState({ASignupopen:false})
    // return <Redirect to =''></Redirect>
    this.setState({logoutDone:true})
  }
  // getName(){
  //   axios.get(`${config.get('apiDomain')}/core/user_details/ `,{headers:{
  //       // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
  //       'Authorization': `Token ${this.props.userToken}`
  //     }})
  //     .then(res=>this.setState({name:res.0ata.name}))
  //     .catch(err=>console.log(err)
  //     )
  //   }
  LOG_IN=<PUserLogin scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS} ShowMsg={this.props.ErrMsg}/>
  SIGN_IN=<PUserSignup scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS}/>
  render() {
    const SetCategory=(category)=>{
      // alert(category);
      this.props.setCategory(category);
      // this.props.CloseCatPopup()
    }

    if(this.props.userToken !== null){
      localStorage.setItem('user', this.props.userToken)
    }
    const CheckLogin=()=>{
      if(this.props.userToken === null){
       this.ToggleASignupOpenModal()
       this.setState({goTocart:true})
       // if(this.props.userToken !== null){
       //   this.setState({RedirectCart:true})
       // }
     }else{
       this.setState({RedirectCart:true})
     }
     }
     const MobileCheckLogin=()=>{
      //  alert("Okk")
      if(this.props.userToken === null){
        this.props.MobileCartRedirect()
        return <Redirect to="/login"/>
     }else{
      // return <Redirect to="/view-cart"/>
       this.setState({RedirectCart:true})
     }
     }
     if(this.props.cartRedirectMobile){
      return <Redirect to="/login"/>

     }
     if(this.state.goTocart === true && this.props.userToken === null){
      return  <Redirect to='/login'/>;
    }
     if(this.state.goTocart === true && this.props.userToken !== null){
      return  <Redirect to='/view-cart'/>;
    }
    let url= window.location.href;
    var inCart =url.search('/view-cart')
    if(this.state.doRedirect ){
      // alert("redirect")
    // return  <Redirect to='/search'/>;
    }
    if(this.state.RedirectCart && inCart === -1){
      // alert("redirect")
    return  <Redirect to='/view-cart'/>;
    }
    console.log(this.props.cartDetails);
    
    const {cartDetails} = this.props
    let thumb = cartDetails.bookThumb;
    console.log(`${this.props.cartDetails} `);

    if(this.props.userToken !== null && this.state.name ==='' )
    {

      // this.getName()
      const details=`Token ${this.props.userToken}`
      // this.props.userdetails(details)
      // this.props.Getaddress(details)
      // this.props.addAddressAction(details)
      // alert("name")
    //  this.setState({sopen:false}) 
    if(this.state.name !=='')
    {
    this.scloseModal()}
    }

    if(this.state.logoutDone){
    return  <Redirect to=""/>
    }
    if(this.state.BlankSerachRedirect){
    return <Redirect to="/get-books/"/>
    }
      const{ SearchResult }= this.state
      var TotalShipCost =0
      cartDetails.map(cart=> TotalShipCost+=Number(cart.bookShippingCost))
      console.log(this.props.location.search);
      const queryParameters = this.props.location.search;
  
     const setSearchValue=()=>{
        // alert("oo")
      }
    return (
      <div>
         {/* tab           */}
    <MediaQuery maxWidth={991} and minWidth={768}>
    <div>
<div id="modiv">
  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>
{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}
{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>

      <Link style={{ textDecoration: 'none' }} to="">
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>
 </Link>
 <span id="cart">
 <span>{(this.props.ItemsInCart === 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
  </span>

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} id="cartimg" onClick={()=>MobileCheckLogin()} />

</div>
</div>
 {/*#################### Mobile StickyTop SEARCH BOX ############################ */}
 <div id="homeMenusearch" >
 <Link style={{ textDecoration: 'none' }} to="">
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mypustaknewlogo.png`} style={{width:'7vw',height:'7vw'}} onClick={this.props.makeSearchBlank}/>
        </Link>  
                  
                  <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                  style={{width:'3vw'}} />
                  <span id="DonateBooksMH">Donate Book</span></button></Link>
                    <input className="search-txt" type="search" autoFocus
                    name='searchBook' value={this.props.searchBook}
                    onChange={this.onSearchChange} ref="searchBookFixed" 
                      style={{ position:'absolute',width:'73%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'18%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"
                      />

                    <a href={`?value=${this.props.searchBook}`} >
                    
                    <i className="fas fa-search"
                      style={{ position:'absolute',marginTop:'2.5%',marginLeft:'86%' }}
                      />
                    </a> 
                    </div>



{/* </Carousel>       */}
    </MediaQuery>
    {/* End of tab */}
    {/* larger mobile */}
    <MediaQuery maxWidth={767} and minWidth={539}>
    <div>
<div id="modiv">

    <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>
{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}

{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>

     <Link style={{ textDecoration: 'none' }} to="">

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>
</Link>
<span id="cart">
<span>{(this.props.ItemsInCart === 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
</span>
{/* <Link to={(localStorage.getItem('user') === null)?"/login":"/view-cart"}> */}
<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} id="cartimg" onClick={()=>MobileCheckLogin()}/>
{/* </Link> */}
</div>
</div>
{/*#################### Mobile StickyTop SEARCH BOX ############################ */}
<div id="homeMenusearch" >
{/* <img  src={require('./images/mypustaknewlogo.png')} style={{width:'7vw',height:'7vw'}}/> */}
<Link style={{ textDecoration: 'none' }} to="">
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mypustaknewlogo.png`} style={{width:'7vw',height:'7vw'}} onClick={this.props.makeSearchBlank}/>
        </Link>   
<Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                  style={{width:'3vw'}} />
                   <span id="DonateBooksMH">Donate Book</span></button></Link>
                   <input className="search-txt" type="search" autoFocus
                    name='searchBook' value={this.props.searchBook}
                    onChange={this.onSearchChange} ref="searchBookFixed"
                      // style={{ position:'absolute',width:'89%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'1%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      style={{ position:'absolute',width:'73%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'18%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"/>
            
 

           <a href={`?value=${this.props.searchBook}`}>
           
           <i className="fas fa-search"
             style={{ position:'absolute',marginTop:'2.5%',marginLeft:'86%' }}
             />
           </a>  
         </div>

</MediaQuery>
{/* End of larger mobile */}

{/* smaller mobiles */}
<MediaQuery maxWidth={538} >
<div id="headerdiv">
    <div>
<div id="modiv">

    <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>
{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}

{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>

<Link style={{ textDecoration: 'none' }} to="">

<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>
</Link>
<span id="cart">
<span>{(this.props.ItemsInCart === 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
</span>

<i class="fas fa-shopping-cart" id="cartimg" onClick={()=>MobileCheckLogin()}></i>
</div>
</div>
{/*#################### Mobile StickyTop SEARCH BOX ############################ */}
<div id="homeMenusearch" >

         <div>
  <InputGroup className="mb-3">
    <InputGroup.Prepend>
      <InputGroup.Text style={{height:'8vw'}}> 
      <a  href={`?value=${this.props.searchBook}`} >
      <i class="fa fa-search" style={{fontSize:'3vw'}}></i></a>
                    </InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl style={{height:'8vw',borderBottom:'0px'}}
    value={this.props.searchBook}
    onChange={this.onSearchChange} ref="searchBookFixed" autoFocus
      placeholder="Search Books, Author, Publisher, Title, ISBN #"
    />
    
     <InputGroup.Append>
      <Button id="donatebook"><Link style={{ textDecoration: 'none' }} to="/donate-books">  

                  <b >Donate Book</b></Link></Button>
    </InputGroup.Append>
  </InputGroup>
  </div>
  </div>
  </div>
</MediaQuery>
{/* End of smaller mobile */}

    {/*-----------------------------------------------Desktop---------------------------------------- */}
      <MediaQuery minWidth={992}>

       {/*####################StickyTop NAV ############################ */}
     <nav id="homeMenu" style={{ display:this.state.menuS }}>
     <Link style={{ textDecoration: 'none' }} to="">
     <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} style={{ height:'90%',cursor:'pointer',width:'10vw' }} onClick={this.props.makeSearchBlank}/>
     </Link>
          <Popup
        open={this.state.Cat}
        closeOnDocumentClick
        onClose={this.CLOSECategory}
        // lockScroll={true}
        contentStyle={{ width:'80%',marginTop:'5.5%',height: '69%'}}
      >
        <div style={{ color:'black' }} >
          <a className="close" onClick={this.CLOSECategory} style={{marginTop:'-0.5%'}}>
            &times;
          </a>
         <Popupcat/>
        </div>
      </Popup>
                  <div id="homeMenusearch" >
                    {/* <button style={{ float:'left',border:'2px solid #13dafe',
                                  height:'100%',width:'22%',marginLeft:'2%',
                                  color:'#13dafe',borderRadius:'5px'}}>Categories </button> */}
        <button id="slidercategorybtn"
           onClick={this.OPENCategory} ><span id="categoryword">Categories </span><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/categoriesicon.png`} id="categoryimg"/></button>
                        {/*####################StickyTop SEARCH BOX ############################ */}
                        {/* <form> */}
                          
                    <input className="search-txt" type="search" 
                    name='searchBook' value={this.props.searchBook}  maxLength="70"
                    onChange={this.onSearchChange} ref="searchBookFixed" autoFocus
                      style={{ position:'absolute',width:'60%'
                      ,marginTop:'-1%'
                    }}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"/>
                        {/* </form> */}

                     {/* <input className="search-txt" type="search" 
                   name='searchBook' value={this.state.searchBook}
                   onChange={this.onSearchChange}
                    style={{ position:'absolute',width:'60%',marginTop:'-4%'}}
                   placeholder="Search Books, Author, Publisher, Title, ISBN #"/> */}

                    <a id="homeMenuSearchBtn" href={`?value=${this.props.searchBook}`}>
                    <i className="fas fa-search"
                      style={{ position:'absolute',marginTop:'0%'  }}
                      />
                    </a>  
                  </div>
                {/* <button> Donate Book</button> */}
                <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                 /> <span id="DonateBooksMH">Donate Book</span></button></Link>
                  <span id="loginSignup"><img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/loginsignUp.png`} /> 
                  { (this.props.userToken === null)?
                  <a onClick={this.ToggleASignupOpenModal}>Log In/Sign Up
<Popup
          open={this.state.ASignupopen}
          // closeOnDocumentClick
          // onClose={this.ASignupCloseModal}
          onClose={()=>this.setState({ ASignupopen: false})}
          contentStyle={{ 
                        width:'58.9%',
                        height:'70.6%',
                        borderRadius:'5px'
                        }} 
        > 
        {(this.state.showLS === true)?this.LOG_IN:this.SIGN_IN}
        </Popup>

</a>:<Popup trigger={<a onClick={this.sopenModal} style={{ fontSize:'60%' }}> Hi, Reader
</a>}
          on="click"
          // open={this.state.sopen}
          onClose={this.scloseModal}
          // closeOnDocumentClick
          // onClose={this.scloseModal}
          contentStyle={{ 
                        width:'15%',
                        // height:'30%',
                        borderRadius:'5px'
                        }}              
        > 
        <div style={{ color:'black',height:'30%'  }}>
        <div id="listonProfile"> {this.state.name}</div>
        <div id="listonProfile"><Link to='/customer/customer_account' > View Profile</Link></div>
        <div id="listonProfile"><Link to='/customer/customer_order' > Your Orders</Link></div>
        <div id="listonProfile"><Link to='/my-wish-list' > Your Wishlist</Link></div>
        <div id="listonProfile"><Link to='/mypustak-wallet' >Your Wallet</Link></div>
        <div id="listonProfile" onClick={this.Userlogout}> Logout</div>
      </div>
        </Popup>
}</span>
                  <Popup
          trigger={<span id="cart" onMouseEnter={this.props.CartopenModal}>
          <span>{(this.props.ItemsInCart === 0)?null:<span id="DisplayCartNos">{this.props.ItemsInCart}</span>}</span>
          <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} />
          
          Cart </span>}

          on='hover'
          // open={this.props.PopupCart}
          // on="hover"
          modal={false}
          lockScroll={true}
          position="bottom center"
          onClose={this.props.CartcloseModal}
          onOpen={this.CartFadeOut}
          // closeOnDocumentClick
          // onClose={this.closeModal}
          overlayStyle={{ 
            backgroundColor:'transparent',
            // display:'none'
           }}
           offsetX='20px'
           disabled={(this.props.cartDetails.length === 0)?true:false}
          contentStyle={{ 
                      width:'35%',
                      left: '865.2px',

                        }} 
        >
    <div style={{ }} id={(this.props.ItemsInCart === 1)?"showCartOne":"showCart"}>
          

{this.props.cartDetails.map(cart=>
  <div id="popupCartBody">
  <div id="FirstpartPopupCart">
{/* const  src=`https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/${thumb}`; */}

  <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${cart.bookThumb}`}  style={{ float:'left',height:'75.75px',width:'53.56px' }}id="book"/>
  <div id="PopupbookDetails">
  <span id="title">{cart.bookName}</span>
  

</div>

<span id="BookCond">{cart.bookCond}<label id="newprice"><b> <span id="mrp">MRP:<span id="orgprice"> &#8377;<strike>{Math.round(cart.bookPrice)  }</strike></span>
</span><span style={{ color:'#e26127' }}>Free</span></b></label> </span>
<div>

  {/* <span id="shipping"> &#8377;{cart.bookShippingCost}</span> */}
  <span id="shipping"> Shipping Handling Charge &#8377;{Math.round(cart.bookShippingCost)}</span>

  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png`} id="delete" onClick={()=>this.RemoveFromCart(cart.bookInvId,cart.Cart_id)}></img>


</div>


</div>
<hr id="cartPopupHr"/>
</div>
)}
{this.state.cartMsg && <span id="cartMsg">{this.state.cartMsg}</span>}

</div>
{/* <Link to="/view-cart"> */}
 {/* {(TotalShipCost>80)?<button id="checkout" onClick={()=>CheckLogin()} >Proceed To Checkout</button>:<span style={{ marginLeft:'21%' }}>minimum order value should be Rs. 80</span>} */}
<button id="checkout" onClick={()=>CheckLogin()} >Proceed To Checkout</button>
<Link to=""><button id="continueShopingCart">Continue Shopping > </button></Link>
{/* </Link> */}
</Popup>
          {/* <span id="more">More<i className="fas fa-angle-down" style={{ marginLeft:'.2%' }}
                   /></span> */}
                  
              </nav>
              </MediaQuery>
            {/*End of Desktop */} 
 {/*####################### BODY FOR THE SEARCH PART ################################*/}
 {/* https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/0499274001546591981.jpg
 https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/0042265001547208288.jpg */}
              <div id="searchBody" >

              {/* <Link to={{ pathname: "/search", search: "?q=zillow-group" }}>
              Zillow Group
            </Link> */}

   {/* -------------------------------Infinite Scroll----------------------------------------------------- */}
   <div style={{ color:'black' }}>
                 <InfiniteScroll
                 dataLength = {this.props.SearchResultR.length}
                // scrollThreshold={'300px'}
                scrollThreshold={'80%'}
                 next={this.fetchBooks}
                 hasMore={true}
                 loader={<h4>NO RESULT FOUND</h4>}
                //  height={400}
                 >


             {/* {this.props.SearchResultR.map((bookdata)=>
                 
                <SBook key={bookdata.document.title}
                    book={bookdata.document} 
                    />
                 )
                 } */}


             {this.props.SearchResultR.map((bookdata)=>
                 ((bookdata.document.is_out_of_stack === `N`)?
                <SBook key={bookdata.document.title}
                    book={bookdata.document} 
                    />:null  
                 ))
                 }
                 {this.props.SearchResultR.map((bookdata)=>
                 ((bookdata.document.is_out_of_stack === `Y`)?
                <SBook key={bookdata.document.title} 
                    book={bookdata.document} 
                    />:null  
                 ))
                 }
             

                 </InfiniteScroll>


                 </div>
             {/* ------------------------------------------------------------------------ */}
             </div>
      </div>
    );
  }
}
const mapStateToProps = state => ({
    searchBook:state.homeR.searchBook,
    userToken:state.accountR.token,
    SearchResultR:state.homeR.SearchResult,
    ErrMsg:state.accountR.ErrMsg,
    cartDetails:state.cartReduc.MyCart,
    PopupCart:state.cartReduc.PopupCart,
    ItemsInCart:state.cartReduc.cartLength,
    getuserdetails:state.userdetailsR.getuserdetails,
    getadd:state.getAddressR.getadd,
    editadd:state.accountR.editadd,
    ClickedCategory:state.homeR.ClickedCategory,
  cartRedirectMobile:state.cartReduc.cartRedirectMobile,

  })
export default connect(mapStateToProps,{doSearch,GetSearchResult,fetchBooksAcc,makeSearchBlank,CartopenModal,CartcloseModal,doSearch,logout,MobileCartRedirect,
  AddToCart,removeAllCart,getSavedToken,RemoveCart,Getaddress,userdetails,Editaddress,addAddressAction,setCategory})(Search);