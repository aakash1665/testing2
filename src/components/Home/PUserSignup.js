import React, { Component } from 'react'
// import './login.css'
import './userLogin.css'
import {signupCheck,showLoader,LoginCheck,clearAll,clearAllUserStatus} from '../../actions/accountAction'
import { connect } from 'react-redux';
import Loader from 'react-loader-spinner';
import Popup from 'reactjs-popup';
import MediaQuery from 'react-responsive'
import { GoogleLogin } from 'react-google-login';

class UserSignup extends Component {
  
    state={
        Sopen:false,
        email:'',
        mobile:'',
        password:'',
        moblielength:false,
        fadeout:'',
        fadeoutSec:'',
        SignUpSuccessPopup:false,
    }
    sopenModal=()=>{
        this.setState({ sopen: true })
      }
      scloseModal=()=>{
        this.props.scloseModal()
    }
    changeShowLS=()=>
    {this.props.changeShowLS()}


  mobilechecker=()=>{this.setState({moblielength:true})}
  onChange = e => {this.setState({ [e.target.name]: e.target.value })};
  render() {
      var Userstatus = `${this.props.Userstatus}`
      var SignedupErr= `${this.props.SignUpErrMsg}`
      if(SignedupErr.search("Email Registered") !== -1){
        // this.changeShowLS()
      }
      let PhoneErr=""
      let PasswordErr =""
    //   For password Phone Number validation
    if(Userstatus.length !== 0){
      
      // alert("chan")
      // this.setState({SignUpSuccessPopup:true})
      setTimeout(()=>{
        this.props.clearAllUserStatus();this.props.scloseModal();this.changeShowLS()
      },3000)
      
    }else{
      // this.setState({})
    }

    if(SignedupErr.length !== 0){
      // alert(SignedupErr.length)
      setTimeout(()=>{this.props.clearAll()},3000)
    }else{
      // this.setState({fadeoutSec:""})
    }

      if(this.state.mobile.length !== 10 || this.state.mobile.length < 10){
        //   alert("stop")
       PhoneErr = "Enter 10 Digits"
        // document.getElementsById("mobile").disable=true
        // this.mobilechecker()
    }else if(isNaN(this.state.mobile)){
         PhoneErr = "Enter Digits"
    }else{
         PhoneErr=""
    }
    // For password Validation
    if(this.state.password.length < 6){
        PasswordErr = "minimum Password length must be 6"
     }else{
        PasswordErr=""
     }
     const signupUser = (e) =>{
      e.preventDefault()
      const {mobile,email,password} = this.state
     const details={
          email,
          mobile,
          password,
      }
      if(PhoneErr === '' && PasswordErr === '' && this.state.email !== '' && this.state.password !== ''){
        this.props.showLoader()
        this.setState({fadeoutSec:""})
        this.setState({loading:true})
      this.props.signupCheck(details)
    
    }
     
      if(Userstatus.length !== ''){
        // alert("Al")
        // const {mobile,email,password} = this.state
       const  detailsAutoLogin={
          email,
          password,
        }
        // this.props.LoginCheck(detailsAutoLogin)
      }
  }
  const responseGoogle = (response) => {
    // console.log(response.profileObj.email);
    const details={
      email:response.profileObj.email,
      password:13,   
    }
    this.props.signupCheck(details)
  }
  const responseGoogleFailure=()=>{
    
  }
    return (
<div id="SignPopupBody">
    
   

          <div id="LoginpopupLeft">
              <p >Sign In/Sign Up</p>
          </div>
          <div id="LoginpopupRight">
                <a style={{ backgroundColor:'transparent',marginTop:"-3.7%",marginLeft:'48%',color:'red',position:'absolute',fontSize:'1.2rem'}}
                    onClick={this.scloseModal} id="DisplayX_none">
                            &times;
                </a>
              <p><span className="BtnPopUptop"  onClick={this.changeShowLS}>Sign In</span><span className="BtnPopUptop" id="ULShowClickedBtn" onClick={this.handleClick} >Sign Up</span></p>
              <div>
                <form onSubmit={signupUser}  style={{ padding:'0% 10%',marginTop:'5%',}}>
                {/* <input type="text"  style={{height:'20%',marginTop:'0px',lineHeight:'0px',paddingTop:'4%'}} name="mobile" id="mobile" */}
                <input type="text"  name="mobile" id="signupmobile"
                    placeholder="Enter Mobile Number"required
                    onChange={e => this.setState({mobile: e.target.value})}
                    disabled={this.state.moblielength}
                    maxLength="10"
                    required
                   />
    
              <p style={{ display:'block',textAlign:'center',fontSize:"70%",color:'red',fontWeight:'lighter',height:'5%',lineHeight:'120%'}}>{PhoneErr}</p>
                   
                   {/* <input type="email"  style={{height:'20%',marginTop:'0px',lineHeight:'0px',paddingTop:'4%'}} name="email" */}
                   <input type="email"  id="signupemail" name="email"

                    placeholder="Enter Email ID" required 
                    onChange={e => this.setState({email: e.target.value})}
                     maxLength="50"
                     autoComplete="on"

                    />
                    {/* <input type="Password"  style={{height:'20%',marginTop:'0px',lineHeight:'0px',paddingTop:'4%'}} name="password" */}
                    <input type="Password"  id="signuppassword" name="password"

                    placeholder="Create MyPustak Password"required
                    onChange={e => this.setState({password: e.target.value})}
                     maxLength="50"
                     autoComplete="on"
                    //  auto
                    />
                     <p style={{ display:'block',textAlign:'center',fontSize:"70%",color:'red',padding:'0px',fontWeight:'lighter',margin:'0px' ,height:'5%',lineHeight:'120%'}}>{PasswordErr}</p>
                    
                       <Popup 
                       open={this.props.loader}
                       contentStyle={{ 
                         width:'50%',
                         height:'50%',
                         marginTop:'25%',
                         color:'transparent',
                         backgroundColor:'transparent',
                         marginRight: '5.7%',
                         border:'0px',
                        }}
                       >
                       <div style={{  }}>
                       {this.props.loader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                               />  :null 
                      }</div>
                       </Popup>
                        <Popup 
                       open={this.props.Userstatus.length !== 0}
                       contentStyle={{ 
             
                         border:'0px',
                        }}
                       >
                       <p style={{ color:'#00baf2' }}>Your Accout is Created Succesfully , Plesae Login </p>
                       </Popup>

              <span id="signUpErr">{SignedupErr}</span>

              <div id="signupbtn">    
                 <input className="SignupBtnHomePopup" 
                    type="submit" value="Sign Up" 
                  />
            </div>
            <div id="Google">
            <GoogleLogin 
                theme="dark"
                onSuccess={responseGoogle}
                onFailure={responseGoogleFailure} 
                buttonText="Sign Up"
                clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
                cookiePolicy={'single_host_origin'} />
            </div>


        <MediaQuery minWidth={539}>
        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%',marginTop:'10%' }}>By Logging In,You Agree To Our <b>Terms &</b></p>
        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%',paddingBottom:'1%'}}><b>Conditions </b>& Privacy Policy.</p>
        </MediaQuery>
        <MediaQuery maxWidth={538}>
        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%',fontSize: '4vw'}}>By Logging In,You Agree To Our <b>Terms &</b></p>
        <p style={{ textAlign:'center',display:'block',fontWeight:'normal',lineHeight:'150%',paddingBottom:'1%',fontSize: '4vw' }}><b>Conditions </b>& Privacy Policy.</p>
        </MediaQuery>
                </form>
              </div>
          </div>

           </div>
    )
  }
}
const mapStateToProps = state => ({
    Userstatus:state.accountR.status,
    ErrMsg:state.accountR.ErrMsg,
    loader:state.accountR.loading,
    SignUpErrMsg:state.accountR.SignUpErrMsg,
  })
export default connect(mapStateToProps,{LoginCheck,signupCheck,showLoader,clearAll,clearAllUserStatus})(UserSignup);