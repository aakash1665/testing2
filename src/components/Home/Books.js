import React, { Component } from 'react';
/*import Header from './components/Layout/Header.js';*/
// import Data from "./Data";
// import Footer from "./Footer";
import { Link,Redirect,browserHistory } from 'react-router-dom'
import './books.css'
import { connect } from 'react-redux';
import Book from './Book'
import {getCompExam,getFriction,getSchoolbooksBooks,getUniversityBooks} from '../../actions/booksActions'
// import {} from '../../actions/booksActions'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import MediaQuery from 'react-responsive';
// import Skeleton from 'react-loading-skeleton';
import Skeleton from '@grovertb/react-skeletor'



class Books extends Component {
  state = {
    url:["http://127.0.0.1:8000/category/competitive-exams", 
          "http://127.0.0.1:8000/category/competitive-exams/cat",
          "http://127.0.0.1:8000/category/competitive-exams/general-knowledge-learni",
          "http://127.0.0.1:8000/category/competitive-exams/engineering",
          "http://127.0.0.1:8000/category/competitive-exams/government-jobs",
          "http://127.0.0.1:8000/category/competitive-exams/international-exams"
        ], 
       count:1,  
       ClickCompExm:'', 
       ClickFiction:'',
       ClickSB:'',
       ClickUB:'',
       CompViewAll:'/category/competitive-exams/',
       FictViewAll:'/category/fiction-non-fiction/',
       SCBViewAll:'/category/school-children-books/',
       UBViewAll:'/category/university-books/',
       ShowCompVA:'Competitive Exams',
       ShowFictVA:'Fiction&NonFiction',
       ShowScbVA:'School & Children ',
       ShowUbVA:'University ',
  }
   loadExamBooks(url,count){
    this.props.getCompExam(url,count)
  }
  loadFrictionBooks(url,count){
    this.props.getFriction(url,count)
  }
  loadschoolbooksBooks(url,count){
    this.props.getSchoolbooksBooks(url,count)
  }
  loadsUniversityBooks(url,count){
    this.props.getUniversityBooks(url,count)
  }
  componentDidMount(){
    
    this.props.getCompExam('/competitive-exams',this.state.count)
    this.props.getFriction('',this.state.count)
    this.props.getSchoolbooksBooks('',this.state.count)
    this.props.getUniversityBooks('',this.state.count)
    const {compExamBooks,FrictionBooks} = this.props
  // console.log(compExamBooks);
  
  }

  render() {

    const SamplePrevArrow=(props)=> {
      const { className, style, onClick } = props;
      return (

        <img src={'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/SliderLeftArrow.42325be1.png'} onClick={()=>{onClick() ;} }
         className={className}  />
    
      );
    }

    const SampleNextArrow=(props)=> {
      const { className, style, onClick } = props;
      return (

        <img src={'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/SliderRightArrow.c683f018.png'} className={className} 
        onClick={()=>{onClick()  }} />
      );
    }
    
  var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 5,
      slidesToScroll: 5,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      // afterChange: (index)=>{
      //   if(index === 0){
      //     this. loadExamBooks('/competitive-exams',this.state.count)
      //     console.log(this.state.count);
      //     this.setState({count:this.state.count+10})
      //     // count=10+count
      //   }
      // }
      // customPaging: function(i) {
      //   console.log(i);
      // },
    };  
     const settings1 = {
      className: "center",
      infinite: false,
      // centerPadding: "0px",
      slidesToShow: 3.5,
      slidesToScroll:2,
      swipeToSlide: true,
      arrows:false,
      afterChange: function(index) {
        console.log(
          `Slider Changed to: ${index + 1}, background: #222; color: #bada55`
        );
      }
    }
    var settings2 = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 3,
      slidesToScroll: 5,
      arrows:false,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
    }
    var settings3 = {
      dots: false,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 5,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
    }
    const {compExamBooks,FrictionBooks,SchoolBooks,UniversityBooks} = this.props
    // console.log(FrictionBooks)
    return (
      <div>
        {/* Desktop */}
        <MediaQuery minWidth={992}>

         <div className="rectangle2">
   
            <div id="menu">
                <ul id="list-nav">
                    {/* <li style={{ marginLeft:'3%',marginTop:'0px',fontWeight:'bold',width:'auto',fontSize:'100%'}}><img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/icons/Compititiveexams.png`}  />         */}
                    <li style={{ marginLeft:'3%',marginTop:'4px',fontWeight:'bold',width:'16%',fontSize:'100%'}}><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Compititiveexams.png`} style={{width:'15%'}}/>        

                        <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadExamBooks('/competitive-exams',this.state.count);
                        this.setState({ClickCompExm:'compExm',CompViewAll:'/category/competitive-exams/',ShowCompVA:'Competitive Exams'})}}
                        id={(this.state.ClickCompExm ==='compExm')?"Showclicked":""}
                        > Competitive Exams</a></span></li>

                    <li><span ><a onClick={(e)=>{this.loadExamBooks('/competitive-exams/cat',this.state.count);
                    this.setState({ClickCompExm:'cat',CompViewAll:'/category/competitive-exams/cat/',ShowCompVA:'CAT'})}} 
                    id={(this.state.ClickCompExm ==='cat')?"Showclicked":""}>CAT</a></span></li>

                    <li><span><a onClick={()=>{this.loadExamBooks('/competitive-exams/engineering',this.state.count);
                    this.setState({ClickCompExm:'engineering',CompViewAll:'/category/competitive-exams/engineering/',ShowCompVA:'Engineering & Medical'})}} 
                    id={(this.state.ClickCompExm ==='engineering')?"Showclicked":""}>Engineering & Medical</a></span></li>

                    <li><span><a onClick={()=>{this.loadExamBooks('/competitive-exams/government-jobs',this.state.count);
                  this.setState({ClickCompExm:'govjobs',CompViewAll:'/category/competitive-exams/government-jobs/',ShowCompVA:'Govt Jobs'})}} 
                  id={(this.state.ClickCompExm ==='govjobs')?"Showclicked":""}>Govt Jobs</a></span></li>

                    <li><span><a onClick={()=>{this.loadExamBooks('/competitive-exams/general-knowledge-learni',this.state.count);
                  this.setState({ClickCompExm:'gk',CompViewAll:'/category/competitive-exams/general-knowledge-learni/',ShowCompVA:'GK'})}}
                   id={(this.state.ClickCompExm ==='gk')?"Showclicked":""}>GK</a></span></li>

                    <li><span><a onClick={()=>{this.loadExamBooks('/competitive-exams/international-exams',this.state.count);
                  this.setState({ClickCompExm:'int_exm',CompViewAll:'/category/competitive-exams/international-exams/',ShowCompVA:'International Exams'})}}
                   id={(this.state.ClickCompExm ==='int_exm')?"Showclicked":""}>International Exams</a></span></li>

                    <li><span ><a onClick={()=>{this.loadExamBooks('/competitive-exams/school-level',this.state.count);
                  this.setState({ClickCompExm:'sc_leve',CompViewAll:'/category/competitive-exams/school-level/',ShowCompVA:'School Level'})}}
                   id={(this.state.ClickCompExm ==='sc_leve')?"Showclicked":""}>School Level</a></span></li>

                    <li style={{ float:'right',marginRight:'4%' }}><span style={{ color:'white' }} ><Link to={this.state.CompViewAll} style={{ textDecoration:'none',color:'white' }}>View All { this.state.ShowCompVA} Books</Link></span></li>
                </ul>
            </div>
            {/* <div className="books" id="books"> */}
            {(compExamBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings}>
                  {/* <DataItem data={this.state.url}/> */}
                  {compExamBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
            </div>
            :
          <div id="AllBooksSkeleton" >
            <div className="skeletonBook">
            <Skeleton height={250} width={200}/>              
            </div>
            <div className="skeletonBook">
            <Skeleton height={250} width={200}/>              
            </div>
            <div className="skeletonBook">
            <Skeleton height={250} width={200}/>              
            </div>
            <div className="skeletonBook">
            <Skeleton height={250} width={200}/>              
            </div>
            <div className="skeletonBook">
            <Skeleton height={250} width={200}/>              
            </div>
          </div>
          }
          </div>

{/* api/v1/get/category/ fiction-non-fiction
api/v1/get/category/ fiction-non-fiction/fiction */}
           <div className="rectangle2">
          
            <div id="menu">
                <ul id="list-nav">
                    {/* <li style={{ marginLeft:'3%',marginTop:'0px',fontWeight:'bold',width:'auto',fontSize:'100%'}}><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Compititiveexams.png`} style={{ width:'14%' }}/> />         */}
                    <li style={{ marginLeft:'3%',marginTop:'0px',fontWeight:'bold',width:'auto',fontSize:'100%'}}><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fictionandnonfictionicon.png`}  style={{ width:'14%' }}/>        

                        <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadFrictionBooks('',this.state.count);
                        this.setState({ClickFiction:'f&nf',FictViewAll:'/category/fiction-non-fiction/',ShowFictVA:'Fiction & Non Fiction'})}}
                        > Fiction & Non Fiction</a></span></li>
{/* /api/v1/get/category/fiction-non-fiction${url}/${page}/` */}
                    <li><span ><a  onClick={()=>{this. loadFrictionBooks('/fiction',this.state.count)
                  this.setState({ClickFiction:'fnfo',FictViewAll:'/category/fiction-non-fiction/fiction/',ShowFictVA:'Fiction'})}}
                   id={(this.state.ClickFiction ==='fnfo')?"Showclicked":""}>Fiction</a></span></li>
                  
                    <li><span ><a  onClick={()=>{this. loadFrictionBooks('/fiction-non-fiction-others',this.state.count)
                  this.setState({ClickFiction:'fnfo2',FictViewAll:'/category/fiction-non-fiction/fiction-non-fiction-others/',ShowFictVA:'Fiction & Non Fiction Others'})}} 
                  id={(this.state.ClickFiction ==='fnfo2')?"Showclicked":""}>Fiction & Non Fiction Others</a></span></li>

                    <li><span><a onClick={()=>{this. loadFrictionBooks('/motivation-self-help',this.state.count)
                  this.setState({ClickFiction:'msh',FictViewAll:'/category/fiction-non-fiction/motivation-self-help/',ShowFictVA:'Motivation & Self Help'})}}
                   id={(this.state.ClickFiction ==='msh')?"Showclicked":""}>Motivation & Self Help</a></span></li>

                    <li><span><a onClick={()=>{this. loadFrictionBooks('/non-fiction',this.state.count)
                  this.setState({ClickFiction:'nf',FictViewAll:'/category/fiction-non-fiction/non-fiction/',ShowFictVA:'Non-Fiction'})}}
                   id={(this.state.ClickFiction ==='nf')?"Showclicked":""}>Non-Fiction</a></span></li>

                    <li><span><a onClick={()=>{this. loadFrictionBooks('/religion-spirituality',this.state.count)
                  this.setState({ClickFiction:'rd',FictViewAll:'/category/fiction-non-fiction/religion-spirituality/',ShowFictVA:'Religion & Spirituality'})}}
                   id={(this.state.ClickFiction ==='rd')?"Showclicked":""}>Religion & Spirituality</a></span></li>

                    {/* <li><span><a onClick={()=>this. loadExamBooks('/competitive-exams/international-exams')}>International Exams</a></span></li> */}

                    {/* <li><span ><a onClick={()=>this. loadExamBooks('/competitive-exams/school-level')}>School Level</a></span></li> */}

                    <li style={{ float:'right',marginRight:'4%' }}><span style={{ color:'white' }}><Link to={this.state.FictViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowFictVA} Books</Link></span></li>
                </ul>
            </div>
            {/* <div className="books" id="books"> */}
            {(FrictionBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings}>
                  {/* <DataItem data={this.state.url}/> */}
                  {FrictionBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
               </div>
               :
               <div id="AllBooksSkeleton" >
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
            </div> 
                } 
          </div>

          {/* http://127.0.0.1:8000/api/v1/get/category/school-children-books */}

          <div className="rectangle2">
                        <div id="menu">
                <ul id="list-nav">
                    {/* <li style={{ marginLeft:'3%',marginTop:'0px',fontWeight:'bold',width:'auto',fontSize:'100%'}}><img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/icons/Compititiveexams.png`}  />         */}
                    <li style={{ marginLeft:'3%',marginTop:'0px',fontWeight:'bold',width:'20%',fontSize:'100%'}}><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Compititiveexams.png`}  style={{width:'12%'}}/>        

                        <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadschoolbooksBooks('');
                        this.setState({ClickFiction:'S&CB',CompViewAll:'/category/school-children-books/',ShowScbVA:'School & Children '})}} 
                        > School & Children Books</a></span></li>
                    <li><span ><a   onClick={()=>{this.loadschoolbooksBooks('/children-books',this.state.count)
                  this.setState({ClickSB:'cb',SCBViewAll:'/category/school-children-books/children-books/',ShowScbVA:'Children '})}} id={(this.state.ClickSB ==='cb')?"Showclicked":""}>Children Books</a></span></li>

                    <li><span ><a  onClick={()=>{this.loadschoolbooksBooks('/classes',this.state.count)
                  this.setState({ClickSB:'class',SCBViewAll:'/category/school-children-books/classes/',ShowScbVA:'Classes'})}} id={(this.state.ClickSB ==='class')?"Showclicked":""}>Classes</a></span></li>

                    <li><span><a onClick={()=>{this.loadschoolbooksBooks('/dictionary-level-',this.state.count)
                  this.setState({ClickSB:'dicl',SCBViewAll:'/category/school-children-books/dictionary-level-/',ShowScbVA:'Dictionary Level'})}} id={(this.state.ClickSB ==='dicl')?"Showclicked":""}>Dictionary Level</a></span></li>

                    <li><span><a onClick={()=>{this.loadschoolbooksBooks('/ncert',this.state.count)
                  this.setState({ClickSB:'ncrt',SCBViewAll:'/category/school-children-books/ncert/',ShowScbVA:'NCERT'})}} id={(this.state.ClickSB ==='ncrt')?"Showclicked":""}>NCERT</a></span></li>

                    {/* <li><span><a onClick={()=>this. loadFrictionBooks('/religion-spirituality')}>Religion & Spirituality</a></span></li> */}

                    {/* <li><span><a onClick={()=>this. loadExamBooks('/competitive-exams/international-exams')}>International Exams</a></span></li> */}

                    {/* <li><span ><a onClick={()=>this. loadExamBooks('/competitive-exams/school-level')}>School Level</a></span></li> */}

                    <li style={{ float:'right',marginRight:'4%' }}><span ><Link to={this.state.SCBViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowScbVA} Books</Link></span></li>
                </ul>
            </div>
            {/* <div className="books" id="books"> */}
            {(SchoolBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings}>
                  {/* <DataItem data={this.state.url}/> */}
                  {SchoolBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
               </div>
               :
              <div id="AllBooksSkeleton" >
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
            </div> 
              } 
          </div>


          <div className="rectangle2">
            
            <div id="menu">
                <ul id="list-nav">
                    {/* <li style={{ marginLeft:'3%',marginTop:'0px',fontWeight:'bold',width:'auto',fontSize:'100%'}}><img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/icons/Compititiveexams.png`}  />         */}
                    <li style={{ marginLeft:'3%',marginTop:'0px',fontWeight:'bold',width:'auto',fontSize:'100%'}}><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/universitylogoicon.png`}  style={{width:'15%'}}/>        

                        <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadsUniversityBooks('');
                        this.setState({ClickFiction:'UB',UBViewAll:'/category/university-books/',ShowUbVA:'University Books'})}}
                        > University Books</a></span></li>
                    <li><span ><a onClick={()=>{this.loadsUniversityBooks('/engineering',this.state.count); 
                    this.setState({ClickUB:'engg',UBViewAll:'/category/university-books/engineering/',ShowUbVA:'Engineering'})}} id={(this.state.ClickUB ==='engg')?"Showclicked":""}>
                    Engineering</a></span></li>

                    <li><span ><a onClick={()=>{this.loadsUniversityBooks('/finance',this.state.count)
                  this.setState({ClickUB:'finc',UBViewAll:'/category/university-books/finance/',ShowUbVA:'Finance'})}} id={(this.state.ClickUB ==='finc')?"Showclicked":""}>Finance</a></span></li>

                    {/* <li><span><a onClick={()=>this. loadsUniversityBooks('/science-arts')}>Science & Arts</a></span></li> */}

                    <li><span><a onClick={()=>{this.loadsUniversityBooks('/category/university-books/medical/',this.state.count)
                  
                  this.setState({ClickUB:'med',UBViewAll:'/category/university-books/medical/',ShowUbVA:'Medical'})}} id={(this.state.ClickUB ==='med')?"Showclicked":""}>Medical</a></span></li>

                    <li><span><a onClick={()=>{this.loadsUniversityBooks('/others',this.state.count)
                  this.setState({ClickUB:'unv',UBViewAll:'/category/university-books/others/',ShowUbVA:'Others'})}} id={(this.state.ClickUB ==='unv')?"Showclicked":""}>Others</a></span></li>

                    <li><span><a onClick={()=>{this.loadsUniversityBooks('/science-arts',this.state.count)
                  this.setState({ClickUB:'s&a',UBViewAll:'/category/university-books/science-arts/',ShowUbVA:'Science & Arts'})}} id={(this.state.ClickUB ==='s&a')?"Showclicked":""}>Science & Arts</a></span></li>

                    {/* <li><span ><a onClick={()=>this. loadExamBooks('/competitive-exams/school-level')}>School Level</a></span></li> */}
{/* ,this.state.count */}
                    <li style={{ float:'right',marginRight:'4%' }}><span><Link to={this.state.UBViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowUbVA} Books</Link></span></li>
                </ul>
            </div>
            {/* <div className="books" id="books"> */}
            {(UniversityBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings}>
                  {/* <DataItem data={this.state.url}/> */}
                  {UniversityBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
              :<div id="AllBooksSkeleton" >
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={250} width={200}/>              
              </div>
            </div>
              } 
          </div>
          </MediaQuery>
             {/*End of Desktop <Footer/> */}
{/* Tab */}
<MediaQuery maxWidth={991} and minWidth={768}>
          <div className="rectangle2">
            <div id="menu">
          
                    <label id="mcompimg"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Compititiveexams.png`} style={{width:'12%',height:'3vw'}} />        
                        <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadExamBooks('/competitive-exams',this.state.count);
                        this.setState({ClickCompExm:'compExm',CompViewAll:'/category/competitive-exams',ShowCompVA:'Competitive Exams'})}}
                        id={(this.state.ClickCompExm ==='compExm')?"Showclicked":""}
                        > Competitive Exams</a></span></label>

                    <label id="mlabel"><span style={{ color:'white' }} ><Link to={this.state.CompViewAll} style={{ textDecoration:'none',color:'white' }}>View All { this.state.ShowCompVA} Books</Link></span></label>
                {/* </ul> */}
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af'}}>
                              <span ><a onClick={(e)=>{this.loadExamBooks('/competitive-exams/cat',this.state.count);
                               this.setState({ClickCompExm:'cat',CompViewAll:'/category/competitive-exams/cat/',ShowCompVA:'CAT'})}} 
                               id={(this.state.ClickCompExm ==='cat')?"Showclicked":""}><span>CAT</span></a></span>
          </div> </div>
          <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                              <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/engineering',this.state.count);
                               this.setState({ClickCompExm:'engineering',CompViewAll:'/category/competitive-exams/engineering/',ShowCompVA:'Engineering & Medical'})}} 
                               id={(this.state.ClickCompExm ==='engineering')?"Showclicked":""}>Engineering & Medical</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/government-jobs',this.state.count);
                             this.setState({ClickCompExm:'govjobs',CompViewAll:'/category/competitive-exams/government-jobs/',ShowCompVA:'Govt Jobs'})}} 
                             id={(this.state.ClickCompExm ==='govjobs')?"Showclicked":""}>Govt Jobs</a></span>
           </div></div>
           <div><div style={{height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/general-knowledge-learni',this.state.count);
                             this.setState({ClickCompExm:'gk',CompViewAll:'/category/competitive-exams/general-knowledge-learni/',ShowCompVA:'GK'})}}
                              id={(this.state.ClickCompExm ==='gk')?"Showclicked":""}>GK</a></span>
           </div></div>
           <div><div style={{height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/international-exams',this.state.count);
                             this.setState({ClickCompExm:'int_exm',CompViewAll:'/category/competitive-exams/international-exams/',ShowCompVA:'International Exams'})}}
                              id={(this.state.ClickCompExm ==='int_exm')?"Showclicked":""}>International Exams</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span ><a onClick={()=>{this.loadExamBooks('/competitive-exams/school-level',this.state.count);
                             this.setState({ClickCompExm:'sc_leve',CompViewAll:'/category/competitive-exams/school-level/',ShowCompVA:'School Level'})}}
                              id={(this.state.ClickCompExm ==='sc_leve')?"Showclicked":""}>School Level</a></span>
           </div></div>
           </Slider>
            {/* <div className="books" id="books"> */}
            {(compExamBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {compExamBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
              <div id="AllBooksSkeleton" >
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>

            </div>
              }
          </div>

{/* api/v1/get/category/ fiction-non-fiction
api/v1/get/category/ fiction-non-fiction/fiction */}
           <div className="rectangle2">
          
            <div id="menu">
           
            <label id="mcompimgfiction"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fictionandnonfictionicon.png`}   style={{width:'12%',height:'3vw'}}/>        
            <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadFrictionBooks('',this.state.count);
                        this.setState({ClickFiction:'f&nf',FictViewAll:'/category/fiction-non-fiction/',ShowFictVA:'Fiction & Non Fiction'})}}
                        > Fiction&Nontion</a></span></label>

                    <label id="mlabel"><span style={{ color:'white' }}><Link to={this.state.FictViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowFictVA} Books</Link></span></label>
                {/* </ul> */}
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
             <span ><a  onClick={()=>{this. loadFrictionBooks('/fiction',this.state.count)
                  this.setState({ClickFiction:'fnfo',FictViewAll:'/category/fiction-non-fiction/fiction/',ShowFictVA:'Fiction'})}}
                   id={(this.state.ClickFiction ==='fnfo')?"Showclicked":""}>Fiction</a></span>
          </div> </div>
          <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
          <span ><a  onClick={()=>{this. loadFrictionBooks('/fiction-non-fiction-others',this.state.count)
                  this.setState({ClickFiction:'fnfo2',FictViewAll:'/category/fiction-non-fiction/fiction-non-fiction-others/',ShowFictVA:'Fiction & Non Fiction'})}} 
                  id={(this.state.ClickFiction ==='fnfo2')?"Showclicked":""}>Fiction & Non Fiction </a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this. loadFrictionBooks('/motivation-self-help',this.state.count)
                  this.setState({ClickFiction:'msh',FictViewAll:'/category/fiction-non-fiction/motivation-self-help/',ShowFictVA:'Motivation & Self Help'})}}
                   id={(this.state.ClickFiction ==='msh')?"Showclicked":""}>Motivation & Self Help</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this. loadFrictionBooks('/non-fiction',this.state.count)
                  this.setState({ClickFiction:'nf',FictViewAll:'/category/fiction-non-fiction/non-fiction/',ShowFictVA:'Non-Fiction'})}}
                   id={(this.state.ClickFiction ==='nf')?"Showclicked":""}>Non-Fiction</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this. loadFrictionBooks('/religion-spirituality',this.state.count)
                  this.setState({ClickFiction:'rd',FictViewAll:'/category/fiction-non-fiction/religion-spirituality/',ShowFictVA:'Religion & Spirituality'})}}
                   id={(this.state.ClickFiction ==='rd')?"Showclicked":""}>Religion & Spirituality</a></span>
           </div></div>

           </Slider>
         
            {/* <div className="books" id="books"> */}
            {(FrictionBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {FrictionBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
              <div id="AllBooksSkeleton" >
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
            </div>
              }
          </div>

          {/* http://127.0.0.1:8000/api/v1/get/category/school-children-books */}

          <div className="rectangle2">
                        <div id="menu">

            <label id="mcompimg1"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Compititiveexams.png`}   style={{width:'12%',height:'3vw'}}/>        
            <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadschoolbooksBooks('');
                        this.setState({ClickFiction:'S&CB',CompViewAll:'/category/school-children-books/',ShowScbVA:'School & Children Books'})}} 
                        > School & Children Books</a></span></label>

                    <label id="mlabel"><span><Link to={this.state.SCBViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowScbVA} Books</Link></span></label>
                {/* </ul> */}
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
             <span ><a   onClick={()=>{this.loadschoolbooksBooks('/children-books',this.state.count)
                  this.setState({ClickSB:'cb',SCBViewAll:'/category/school-children-books/children-books/',ShowScbVA:'Children Books'})}} id={(this.state.ClickSB ==='cb')?"Showclicked":""}>Children Books</a></span>
          </div> </div>
          <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
          <span ><a  onClick={()=>{this.loadschoolbooksBooks('/classes',this.state.count)
                  this.setState({ClickSB:'class',SCBViewAll:'/category/school-children-books/classes/',ShowScbVA:'Classes'})}} id={(this.state.ClickSB ==='class')?"Showclicked":""}>Classes</a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadschoolbooksBooks('/dictionary-level-',this.state.count)
                  this.setState({ClickSB:'dicl',SCBViewAll:'/category/school-children-books/dictionary-level-/',ShowScbVA:'Dictionary Level'})}} id={(this.state.ClickSB ==='dicl')?"Showclicked":""}>Dictionary Level</a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadschoolbooksBooks('/ncert',this.state.count)
                  this.setState({ClickSB:'ncrt',SCBViewAll:'/category/school-children-books/ncert/',ShowScbVA:'NCERT'})}} id={(this.state.ClickSB ==='ncrt')?"Showclicked":""}>NCERT</a></span>
           </div></div>
           </Slider>


            {/* <div className="books" id="books"> */}
            {(SchoolBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {SchoolBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
              <div id="AllBooksSkeleton" >
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
 
            </div>
              }
          </div>


          <div className="rectangle2">
            
            <div id="menu">
               
    
    <label id="mcompimguniversity"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/universitylogoicon.png`}   style={{width:'12%',height:'3vw'}}/>        
    <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadsUniversityBooks('');
                        this.setState({ClickFiction:'UB',UBViewAll:'/category/university-books/',ShowUbVA:'University Books'})}}
                        > University Books</a></span></label>

                    <label id="mlabel"><span style={{ color:'white' }}><Link to={this.state.UBViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowUbVA} Books</Link></span></label>
                {/* </ul> */}
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
             <span ><a onClick={()=>{this.loadsUniversityBooks('/engineering',this.state.count); 
                    this.setState({ClickUB:'engg',UBViewAll:'/category/university-books/engineering/',ShowUbVA:'Engineering'})}} id={(this.state.ClickUB ==='engg')?"Showclicked":""}>
                    Engineering</a></span>
          </div> </div>
          <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
          <span ><a onClick={()=>{this.loadsUniversityBooks('/finance',this.state.count)
                  this.setState({ClickUB:'finc',UBViewAll:'/category/university-books/finance/',ShowUbVA:'Finance'})}} id={(this.state.ClickUB ==='finc')?"Showclicked":""}>Finance</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadsUniversityBooks('/category/university-books/medical',this.state.count)
                  
                  this.setState({ClickUB:'med',UBViewAll:'/category/university-books/medical/',ShowUbVA:'Medical'})}} id={(this.state.ClickUB ==='med')?"Showclicked":""}>Medical</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadsUniversityBooks('/others',this.state.count)
                  this.setState({ClickUB:'unv',UBViewAll:'/category/university-books/others/',ShowUbVA:'Others'})}} id={(this.state.ClickUB ==='unv')?"Showclicked":""}>Others</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadsUniversityBooks('/science-arts',this.state.count)
                  this.setState({ClickUB:'s&a',UBViewAll:'/category/university-books/science-arts/',ShowUbVA:'Science & Arts'})}} id={(this.state.ClickUB ==='s&a')?"Showclicked":""}>Science & Arts</a></span>
           </div></div>

           </Slider>
          
            
            {/* <div className="books" id="books"> */}
            {(UniversityBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {UniversityBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
               <div id="AllBooksSkeleton" >
               <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
              <div className="skeletonBook">
              <Skeleton height={300} width={250}/>              
              </div>
            </div>
              }
          </div>   
          </MediaQuery>
                  {/* End of tab */}



                   {/* Mobile */}
          <MediaQuery maxWidth={767}>
          <div className="rectangle2">
            <div id="menu">
          
                    <label id="mcompimg"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Compititiveexams.png`} id="bookimg"/>        
                        <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadExamBooks('/competitive-exams',this.state.count);
                        this.setState({ClickCompExm:'compExm',CompViewAll:'/category/competitive-exams/',ShowCompVA:'Competitive Exams'})}}
                        id={(this.state.ClickCompExm ==='compExm')?"Showclicked":""}
                        > Competitive Exams</a></span></label>
   <div id="viewall">
                    <label id="mlabel"><span style={{ color:'white' }} ><Link to={this.state.CompViewAll} style={{ textDecoration:'none',color:'white' }}>View All { this.state.ShowCompVA} Books</Link></span></label>
                {/* </ul> */}
                </div>
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                              <span ><a onClick={(e)=>{this.loadExamBooks('/competitive-exams/cat',this.state.count);
                               this.setState({ClickCompExm:'cat',CompViewAll:'/category/competitive-exams/cat/',ShowCompVA:'CAT'})}} 
                               id={(this.state.ClickCompExm ==='cat')?"Showclicked":""}>CAT</a></span>
          </div> </div>
          <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                              <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/engineering',this.state.count);
                               this.setState({ClickCompExm:'engineering',CompViewAll:'/category/competitive-exams/engineering/',ShowCompVA:'Engg. & Medical'})}} 
                               id={(this.state.ClickCompExm ==='engineering')?"Showclicked":""}><span style={{color:'#868383'}}>Engg. & Medical</span></a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/government-jobs',this.state.count);
                             this.setState({ClickCompExm:'govjobs',CompViewAll:'/category/competitive-exams/government-jobs/',ShowCompVA:'Govt Jobs'})}} 
                             id={(this.state.ClickCompExm ==='govjobs')?"Showclicked":""}><span style={{color:'#868383'}}>Govt Jobs</span></a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/general-knowledge-learni',this.state.count);
                             this.setState({ClickCompExm:'gk',CompViewAll:'/category/competitive-exams/general-knowledge-learni/',ShowCompVA:'GK'})}}
                              id={(this.state.ClickCompExm ==='gk')?"Showclicked":""}><span style={{color:'#868383'}}>GK</span></a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span><a onClick={()=>{this.loadExamBooks('/competitive-exams/international-exams',this.state.count);
                             this.setState({ClickCompExm:'int_exm',CompViewAll:'/category/competitive-exams/international-exams/',ShowCompVA:'International Exams'})}}
                              id={(this.state.ClickCompExm ==='int_exm')?"Showclicked":""}><span style={{color:'#868383'}}>International Exams</span></a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
                               <span ><a onClick={()=>{this.loadExamBooks('/competitive-exams/school-level',this.state.count);
                             this.setState({ClickCompExm:'sc_leve',CompViewAll:'/category/competitive-exams/school-level/',ShowCompVA:'School Level'})}}
                              id={(this.state.ClickCompExm ==='sc_leve')?"Showclicked":""}><span style={{color:'#868383'}}>School Level</span></a></span>
           </div></div>
           </Slider>
            {/* <div className="books" id="books"> */}
            {(compExamBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {compExamBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
                <div id="AllBooksSkeleton" >
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                </div>
              }
          </div>

{/* api/v1/get/category/ fiction-non-fiction
api/v1/get/category/ fiction-non-fiction/fiction */}
           <div className="rectangle2">
          
            <div id="menu">
           
            <label id="mcompimgfiction"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fictionandnonfictionicon.png`}  id="bookimg"/>        
            <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadFrictionBooks('',this.state.count);
                        this.setState({ClickFiction:'f&nf',FictViewAll:'/category/fiction-non-fiction',ShowFictVA:'Fiction & Non Fiction'})}}
                        > Fiction & Non Fiction</a></span></label>
   <div id="viewallfiction">
                    <label id="mlabel"><span style={{ color:'white' }}><Link to={this.state.FictViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowFictVA} Books</Link></span></label>
                {/* </ul> */}
                </div>
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
             <span ><a  onClick={()=>{this. loadFrictionBooks('/fiction',this.state.count)
                  this.setState({ClickFiction:'fnfo',FictViewAll:'/category/fiction-non-fiction/fiction/',ShowFictVA:'Fiction'})}}
                   id={(this.state.ClickFiction ==='fnfo')?"Showclicked":""}>Fiction</a></span>
          </div> </div>
          <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
          <span ><a  onClick={()=>{this. loadFrictionBooks('/fiction-non-fiction-others',this.state.count)
                  this.setState({ClickFiction:'fnfo2',FictViewAll:'/category/fiction-non-fiction/fiction-non-fiction-others/',ShowFictVA:'Fiction & Non Fiction'})}} 
                  id={(this.state.ClickFiction ==='fnfo2')?"Showclicked":""}>Fiction & Non Fiction </a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',lineHeight:'100%',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this. loadFrictionBooks('/motivation-self-help',this.state.count)
                  this.setState({ClickFiction:'msh',FictViewAll:'/category/fiction-non-fiction/motivation-self-help/',ShowFictVA:'Motivation & Self Help'})}}
                   id={(this.state.ClickFiction ==='msh')?"Showclicked":""}>Motivation & Self Help</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this. loadFrictionBooks('/non-fiction',this.state.count)
                  this.setState({ClickFiction:'nf',FictViewAll:'/category/fiction-non-fiction/non-fiction/',ShowFictVA:'Non-Fiction'})}}
                   id={(this.state.ClickFiction ==='nf')?"Showclicked":""}>Non-Fiction</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',lineHeight:'100%',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this. loadFrictionBooks('/religion-spirituality',this.state.count)
                  this.setState({ClickFiction:'rd',FictViewAll:'/category/fiction-non-fiction/religion-spirituality/',ShowFictVA:'Religion & Spirituality'})}}
                   id={(this.state.ClickFiction ==='rd')?"Showclicked":""}>Religion & Spirituality</a></span>
           </div></div>

           </Slider>
         
            {/* <div className="books" id="books"> */}
            {(FrictionBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {FrictionBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
                <div id="AllBooksSkeleton" >

                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                </div>
              }
          </div>

          {/* http://127.0.0.1:8000/api/v1/get/category/school-children-books */}

          <div className="rectangle2">
                        <div id="menu">

            <label id="mcompimg1"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Compititiveexams.png`} id="bookimg"/>        
            <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadschoolbooksBooks('');
                        this.setState({ClickFiction:'S&CB',CompViewAll:'/category/school-children-books/',ShowScbVA:'School & Children '})}} 
                        > School & Children Books</a></span></label>
   <div id="viewallschool">
                    <label id="mlabel"><span><Link to={this.state.SCBViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowScbVA} Books</Link></span></label>
                {/* </ul> */}
                </div>
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
             <span ><a   onClick={()=>{this.loadschoolbooksBooks('/children-books',this.state.count)
                  this.setState({ClickSB:'cb',SCBViewAll:'/category/school-children-books/children-books/',ShowScbVA:'Children '})}} id={(this.state.ClickSB ==='cb')?"Showclicked":""}>Children Books</a></span>
          </div> </div>
          <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
          <span ><a  onClick={()=>{this.loadschoolbooksBooks('/classes',this.state.count)
                  this.setState({ClickSB:'class',SCBViewAll:'/category/school-children-books/classes/',ShowScbVA:'Classes'})}} id={(this.state.ClickSB ==='class')?"Showclicked":""}>Classes</a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadschoolbooksBooks('/dictionary-level-',this.state.count)
                  this.setState({ClickSB:'dicl',SCBViewAll:'/category/school-children-books/dictionary-level-/',ShowScbVA:'Dictionary Level'})}} id={(this.state.ClickSB ==='dicl')?"Showclicked":""}>Dictionary Level</a></span>
           </div></div>
           <div><div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadschoolbooksBooks('/ncert',this.state.count)
                  this.setState({ClickSB:'ncrt',SCBViewAll:'/category/school-children-books/ncert/',ShowScbVA:'NCERT'})}} id={(this.state.ClickSB ==='ncrt')?"Showclicked":""}>NCERT</a></span>
           </div></div>
           </Slider>


            {/* <div className="books" id="books"> */}
            {(SchoolBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {SchoolBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
                <div id="AllBooksSkeleton" >
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                </div>
                    
              }
          </div>


          <div className="rectangle2">
            
            <div id="menu">
               
    
    <label id="mcompimguniversity"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/universitylogoicon.png`} id="bookimg"/>        
    <span >
                        <a  style={{ color:'white',textDecoration:'none',borderBottom:'0px'}}
                        onClick={()=>{this.loadsUniversityBooks('');
                        this.setState({ClickFiction:'UB',UBViewAll:'/category/university-books',ShowUbVA:'University '})}}
                        > University Books</a></span></label>
   <div id="viewallubooks">
                    <label id="mlabel"><span style={{ color:'white' }}><Link to={this.state.UBViewAll} style={{ textDecoration:'none',color:'white' }}>View All {this.state.ShowUbVA} Books</Link></span></label>
                {/* </ul> */}
                </div>
            </div>
            <Slider {...settings1}>
             <div>
             <div style={{  height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
             <span ><a onClick={()=>{this.loadsUniversityBooks('/engineering',this.state.count); 
                    this.setState({ClickUB:'engg',UBViewAll:'/category/university-books/engineering/',ShowUbVA:'Engineering'})}} id={(this.state.ClickUB ==='engg')?"Showclicked":""}>
                    Engineering</a></span>
          </div> </div>
          <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
          <span ><a onClick={()=>{this.loadsUniversityBooks('/finance',this.state.count)
                  this.setState({ClickUB:'finc',UBViewAll:'/category/university-books/finance/',ShowUbVA:'Finance'})}} id={(this.state.ClickUB ==='finc')?"Showclicked":""}>Finance</a></span>
           </div></div>
           
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadsUniversityBooks('/category/university-books/medical',this.state.count)
                  this.setState({ClickUB:'med',UBViewAll:'/category/university-books/medical/',ShowUbVA:'Medical'})}} id={(this.state.ClickUB ==='med')?"Showclicked":""}>Medical</a></span>
           </div></div>

           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadsUniversityBooks('/others',this.state.count)
                  this.setState({ClickUB:'unv',UBViewAll:'/category/university-books/others/',ShowUbVA:'Others'})}} id={(this.state.ClickUB ==='unv')?"Showclicked":""}>Others</a></span>
           </div></div>
           <div><div style={{ height: '6.5vw',fontWeight:'bold',paddingTop:'1vw',fontSize:'2.5vw',color:'#868383',backgroundColor: 'white',textAlign: 'center',border: '1px solid #a9a8af',borderLeft:'0px'}}>
           <span><a onClick={()=>{this.loadsUniversityBooks('/science-arts',this.state.count)
                  this.setState({ClickUB:'s&a',UBViewAll:'/category/university-books/science-arts/',ShowUbVA:'Science & Arts'})}} id={(this.state.ClickUB ==='s&a')?"Showclicked":""}>Science & Arts</a></span>
           </div></div>

           </Slider>
          
            
            {/* <div className="books" id="books"> */}
            {(UniversityBooks.length != 0)?
            <div style={{ marginLeft:'3%' ,width:'94%',marginTop:'.7%'}}>
                <Slider {...settings2}>
                  {/* <DataItem data={this.state.url}/> */}
                  {UniversityBooks.map(book=>(
                    <Book key={book.title}
                    book={book}
                    />
                  ))}
                </Slider>
                </div>
               :
                <div id="AllBooksSkeleton" >
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                <div className="skeletonBook">
                <Skeleton height={140} width={90}/>              
                </div>
                </div>
              }
          </div>   
          </MediaQuery>
                  {/* End of mobile */}
             
      </div>
    );
  }
}

const mapStateToProps = state => ({
  compExamBooks:state.compExam.compExamBooks,
  FrictionBooks:state.friction.FrictionBooks,
  SchoolBooks:state.schoolbooks.schoolBooks,
  UniversityBooks:state.universitybooks.universityBooks,
})

export default connect(mapStateToProps,{getCompExam,getFriction,getSchoolbooksBooks,getUniversityBooks})(Books);
