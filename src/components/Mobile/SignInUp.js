import React, { Component } from 'react'
import MainHeader from '../MainHeader';
import PUserSignup from '../Home/PUserSignup';
import PUserLogin from '../Home/PUserLogin'
import { connect } from 'react-redux';
import {MobileCartRedirect} from '../../actions/cartAction'
import { Link,Redirect,browserHistory } from 'react-router-dom'
import ReactGA from 'react-ga';


class SignInUp extends Component {
    state={
        ASignupopen:false,
        showLS:false,
    }
    componentDidMount(){
      ReactGA.pageview(window.location.pathname + window.location.search);
    }
    ToggleASignupOpenModal=()=>{
        // alert("onnnn")
        this.setState({ ASignupopen: !this.state.ASignupopen })
      }
      changeShowLS=()=>{
        this.setState({showLS:!this.state.showLS})
      }

      LOG_IN=<PUserLogin scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS} ShowMsg={this.props.ErrMsg}/>
      SIGN_IN=<PUserSignup scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS}/>
  render() {
    if(this.props.userToken !== null){
      localStorage.setItem('user', this.props.userToken)
    }

    if(window.innerWidth > 0){
      if(window.innerWidth >990){
        return <Redirect to="/"/>
        // RedirectUrl="/login"
        // alert(window.innerWidth)
      }
    }
    // alert(document.referrer)
    // alert((window.innerWidth > 0) ? `${window.innerWidth} aa` : window.screen.width)
      if(this.props.cartRedirectMobile && this.props.userToken !== null ){
        // alert("Okk")
        // this.props.MobileCartRedirect()
        this.props.MobileCartRedirect()
        return <Redirect to="/view-cart"/>
      }
    return (
      <div>
          <MainHeader/>
          <div id="MobileSignInUp">
          {/* <PUserLogin scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS} ShowMsg={this.props.ErrMsg}/> */}
          {(this.state.showLS === true)?this.LOG_IN:this.SIGN_IN}
          </div>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  searchBook:state.homeR.searchBook,
  userToken:state.accountR.token,
  // searchBook:state.homeR.searchBook,
  // userToken:state.accountR.token,
  ErrMsg:state.accountR.ErrMsg,
  cartRedirectMobile:state.cartReduc.cartRedirectMobile,
  PopupCart:state.cartReduc.PopupCart,
  ItemsInCart:state.cartReduc.cartLength,
  SuccessSentRestMail:state.accountR.ActivateSuccPopupState,
  ClickedCategory:state.homeR.ClickedCategory,
  // cartRedirectMobile:sta

})
// export default SignInUp;
export default connect(mapStateToProps,{MobileCartRedirect})(SignInUp);
