import React, { Component } from 'react';
import './mypassbook.css';
import MainHeader from './MainHeader';
import MainFooter from './MainFooter';
import {Getwalletd} from '../actions/walletAction'
import {passbookd} from '../actions/passbookAction'
import { connect } from 'react-redux';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import Table from 'react-bootstrap/Table'
import moment from "moment";


class Passbook extends Component {
  state={
    datetime:this.props.passdetails.time,
    userToken:'',

}
 componentDidMount() {
  if(localStorage.getItem('user') === null){
    // alert("RED")
    window.location.href="/"
  }
  window.scrollTo(0, 0);
  const details=`Token ${this.props.userToken}`
    this.props.Getwalletd(details)
    const details1=`Token ${this.props.userToken}`

    this.props.passbookd(details1)
 }
 componentWillReceiveProps(userToken){
  // alert(userToken)
  // console.log(userToken.userToken);
  if(this.state.userToken !== userToken.userToken){
  const details=`Token ${userToken.userToken}`
  this.props.Getwalletd(details)
  // const details=`Token f16305a76f487e29fb19b5c0efca3860353dd86a`
  this.props.passbookd(details);
  this.setState({userToken:userToken.userToken})
}
 }
  render() {
    const {passdetails}=this.props
    console.log(passdetails,"Time");
    
    const {datetime}=this.state
    // console.log(datetime)
    var date=''
    var time=''
    
    return (
      <div style={{backgroundColor:'white'}}>
        <MainHeader/>
        <p id="passp">PASSBOOK</p>
        <hr style={{marginLeft:'5%',width:'91%',marginTop:'2%'}}/>
        <p id="totalbalance">Total Balance</p>
        <p id="pbalance">Rs {(this.props.walletbalance.length === 0)?"0":this.props.walletbalance}</p>
        <Link to='/mypustak-wallet' id="passaddbtn" style={{ textDecoration:'none' }}><input type="submit" id="PassWalletbtn" value="Add Money"/></Link>
        <div  id="passtable">
        <Table responsive="lg">
  <thead style={{backgroundColor:'#dde5ed'}}>
    <tr>
      <th>Order Details</th>
      <th>Amount</th>
      <th>Comment</th>

    </tr>
  </thead>
  {passdetails.map(pdetails=>
  <tbody >
    <tr>
      <td>Order Id:{pdetails.transaction_id}
          <br/>
          Via:{pdetails.payvia}
          <br/>
          {/* Date:{pdetails.time} */}

          Date:{moment(pdetails.time).format("dddd, MMMM Do YYYY, h:mm:ss a")}

          
          <br/>
          Time:
      </td>
      <td>{(pdetails.deposit !== "0")? <span> + &#8377;{ pdetails.deposit}</span>: <span> - &#8377;{ pdetails.withdrawl}</span>}</td>
      <td>{pdetails.comment}</td>
    </tr>
  </tbody>
  )}
</Table>
</div>   
        <MainFooter/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  walletbalance:state.walletR.walletbalance,
  userToken:state.accountR.token,
  passdetails:state.PassbookR.passdetails,
})
export default connect(mapStateToProps,{Getwalletd,passbookd})(Passbook);
