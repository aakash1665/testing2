import React from "react";
import { Redirect } from 'react-router-dom';

import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import FilterListIcon from "@material-ui/icons/FilterList";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import { lighten } from "@material-ui/core/styles/colorManipulator";
import Divider from "@material-ui/core/Divider";
import moment from "moment";
import { connect } from "react-redux";

import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Search from "@material-ui/icons/Search";
import Clear from "@material-ui/icons/Clear";

import { getWalletList, addWallet } from "../../actions/backendWalletActions";

import WalletHead from "./WalletHead";

import { desc, stableSort, getSorting } from '../../util/helpers';

import { getUserDetails } from '../../actions/backofficeWalletUpdateActions';

import { getSearchResults } from "../../actions/backofficewalletsearchAction";

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020
  },
  tablecell: {
    fontSize: '11pt',
    padding: '4px'
  },
  tableWrapper: {
    overflowX: "auto"
  },
  button: {
    whiteSpace: "nowrap",
    marginTop: '4px',
    marginLeft: "6px"
  },
  margin: {
    margin: "0 4px"
  }
});

class Wallet extends React.Component {
  state = {
    order: "desc",
    orderBy: "time",
    selected: [],
    walletList: [],
    page: 0,
    rowsPerPage: 10,
    clearsearchValue : true,
    
    token: '',
    openDialogAddWallet: false,
    'email_id':"",
    'user_id':""
  };


  userState = {
    order_id: "",
    user_id: "",
    user_email: "",
    status: "",
    message: ""
  };

  onOrderIdChange = e => {this.userState.order_id = e.target.value };



  onClickHandler = () => {
    // console.log(" the order_id is ", this.userState.order_id)
    this.props.getUserDetails(this.state.token , this.userState.order_id)
    // console.log("The useR data are ", this.props);
    // this.userState.user_id = this.props.UserDetails.user_id;
    // this.userState.user_email = this.props.UserDetails.user_email;

  };


  componentDidMount() {
    this.setState({ token: localStorage.getItem('user') });
    if(!this.props.walletList.length) {
      this.props.getWalletList({ page: this.state.page, token: localStorage.getItem('user') });
    }else {
      this.setState({
        walletList: this.props.walletList
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.walletList.length !== nextProps.walletList.length) {
      let { walletList } = nextProps;
      this.setState(
        {
          walletList: walletList
        }
      );
    }
  }

    // create data is a function which will create data for table row,
  // in this file we use this, you can check below.
  // In this we are creating data as per table header
  createData = ({
    transaction_id,
    user_id,
    user_email,
    deposit,
    withdrawl,
    comment,
    added_by,
    time
  }) => {
    let statusObj = {};
    return {
      id: transaction_id+user_id+time,
      transaction_id,
      user_id,
    user_email,
    deposit,
    withdrawl,
    comment,
    added_by,
    time: moment(time).format("DD MMMM YYYY, h:mm a"),
      action: <div> Actions </div>
    };
  }

  // it will handle sorting request
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  // it is for handle select all click, right now we're not using but if we need in future so it's there.
  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.walletList.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  // handle click on Checkbox and select accordingly
  // its for signlular selection - you can only select one row at a time
  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);

    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = [id];
    } else if (selectedIndex === 0) {
      newSelected = [];
    }
    this.setState({ selected: newSelected });
  };

  // handle request for page change
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  // handle pagination using type
  handlePaging = type => {
    if (type == "right") {
      if(this.props.walletList.length / this.state.rowsPerPage == this.state.page + 1 ) {
        this.props.getWalletList({ page: this.state.page + 1, token: this.state.token });
      }
      this.setState({ page: this.state.page + 1 });
    } else {
      this.setState({ page: this.state.page - 1 });
    }
  };


  onChangeEmailIdHandler = (e) =>{
    this.setState({email_id: e.target.value})
  }

  onChangeUserIdHandler = (e) =>{
    this.setState({user_id: e.target.value})
  }


  handleaddWalletAclick() {
    this.setState({
      openDialogAddWallet: true
    })
  }

  addWallet = (e) => {
    e.preventDefault();

    // console.log(e.target.userId.value, e.target.emailId.value, e.target.amount.value, e.target.purpose.value, e.target.addedBy.value);

    let wallet = {
      user_id: e.target.userId.value,
      user_email: e.target.emailId.value,
      comment: e.target.purpose.value,
      deposit: e.target.amount.value,
      withdrawl: "0",
      payvia: "MyPustak",
      time: new Date(),
      added_by: e.target.addedBy.value,
    };
    this.props.addWallet({ wallet, token: this.state.token });

    this.handleClose();

  }

  // hanldClose shipping address 
  handleClose = () => {
    this.setState({
      openDialogAddWallet : false
    })
  };

  setValue = (e) => {
    this.setState({searchValue: e.target.value})
    // this.props.getSearchResults(localStorage.getItem('user'), e.target.value);
  }



  onSearchHandler = () => {
    // console.log(e.target.value, data);
    const searchValue = this.state.searchValue;
    if(searchValue) {
      this.setState({page:0, clearsearchValue : false})
      this.props.getSearchResults(localStorage.getItem('user'), searchValue);
    }
  }

  handleChangeSearch = () => {
    // console.log(e.target.value, data);
    const searchValue = this.state.searchValue;
    if(searchValue) {
      this.setState({ page: 0 });
      this.props.getWalletList({ page: this.state.page, token: this.state.token, search: true, searchValue });
    } else {
      this.setState({ page: 0 });
      this.props.getWalletList({ page: this.state.page, token: this.state.token, search: true });
    }
  }

  clearSearch = () => {
    this.setState({ searchValue: "" ,page:0, clearsearchValue : true});
  }



  clearSearchHandler = () =>{
    this.setState({clearsearchValue : true, searchValue :'',});
  }

  render() {

    console.log("This props SearchResults ",this.props.SearchResults);

    const { wallet } = this.props.SearchResults;
    let walletStatus;
    let clearsearchValue = true;

    if(wallet){
      // walletStatus = this.props.Search.status;
      console.log("Wallet is ", wallet);
    }
    else{
      console.log("Wallet is not working well", wallet);
    }
    

    const { classes, loading } = this.props;
    let {
      walletList,
      order,
      orderBy,
      selected,
      rowsPerPage,
      page
    } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, walletList.length - page * rowsPerPage);

    walletList = walletList.map(wallet => {
      return this.createData(wallet);
    });

    console.log("WalletList is ", walletList)        

    let {
      order_id,
      user_id,
      user_email,
      status
    } = this.props.UserDetails;

    // console.log("In render status ", status);
    // if(status != 200){
    //   order_id = this.props.UserDetails.message;
    //   user_id = "";
    //   user_email = "";
    // }

    let walletListdub = walletList; 

    if (wallet && this.state.searchValue!=""){
      walletList = wallet;
    }
    else if(this.state.searchValue ==""){
      walletList = walletListdub;

    }


    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "16px",
            flexWrap: 'wrap',
            alignItems: 'flex-end',
          }}
        >
          <div style={{ display: "flex", paddingBottom: "16px", flexGrow: '8' }}>
          <TextField
            id="search"
            className={classNames(classes.margin, classes.textField)}
            variant="outlined"
            type={"text"}
            label="Search"
            placeholder="Seach by UserId/Email Id/Transaction Id/Wallet Id"
            value={this.state.searchValue}
            onChange={this.setValue}
              InputLabelProps={{ shrink: true }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Search"
                      onClick={this.clearSearch}
                    >
                      <Clear />
                    </IconButton>
                    <IconButton
                      aria-label="Search"
                      // onClick={this.handleChangeSearch}
                      onClick = {this.onSearchHandler}
                    >
                      <Search />
                    </IconButton>
                  </InputAdornment>
                )
            }}
            fullWidth
          />
        </div>
        <div style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "16px",
            flexWrap: 'wrap',
          }}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                onClick={ () => this.handleaddWalletAclick() }
              >
                Add Wallet
              </Button>
          </div>
        </div>
        <Divider />

                    <Paper className={classes.root}>
          <div
              style={{
                padding: "12px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <IconButton
                aria-label="Delete"
                disabled={this.state.page === 0}
                className={classes.margin}
                onClick={() => this.handlePaging("left")}
              >
                <KeyboardArrowLeft fontSize="small" />
              </IconButton>
              <IconButton
                aria-label="Delete"
                className={classes.margin}
                onClick={() => this.handlePaging("right")}
                disabled={ this.props.loading }
              >
                <KeyboardArrowRight fontSize="small" />
              </IconButton>
              Page: {this.state.page}
            </div>
            <div className={classes.tableWrapper}>
              <Table className={classes.table} aria-labelledby="tableTitle">
                <WalletHead
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={this.handleSelectAllClick}
                  onRequestSort={this.handleRequestSort}
                  rowCount={walletList.length}
                />
                <TableBody>
                  {
                    loading 
                    ? <tr style={{ textAlign: 'center' }}>  <td colSpan={9}> Loading... </td> </tr>
                    : walletList.length 
                      ? stableSort(walletList, getSorting(order, orderBy))
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map((n, i) => {
                          const isSelected = this.isSelected(n.id);
                          {/* Here below we setting row data and divs conditionally */}
                          return (
                            <TableRow
                              hover
                              onClick={event => this.handleClick(event, n.id)}
                              role="checkbox"
                              aria-checked={isSelected}
                              tabIndex={-1}
                              key={i}
                              selected={isSelected}
                            >
                              <TableCell style={{ padding: 'unset', width: '12px' }}/>
                              
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '60px', maxWidth: '120px', wordBreak: 'break-word' }}> {n.user_id} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '60px', maxWidth: '120px', wordBreak: 'break-word' }}> {n.user_email} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '90px', maxWidth: '120px', wordBreak: 'break-word' }}> {n.deposit} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '60px', maxWidth: '120px', wordBreak: 'break-word' }}> {n.withdrawl} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '120px', maxWidth: '120px', wordBreak: 'break-word' }}> {n.comment} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '130px', maxWidth: '120px', wordBreak: 'break-word' }}> {n.added_by} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '120px', maxWidth: '200px', wordBreak: 'break-word' }}> {n.time} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '150px', maxWidth: '120px', wordBreak: 'break-word' }} > {n.transaction_id} </div>
                              </TableCell>
                              <TableCell style={{ minWidth: '50px' }}>{n.action}</TableCell>
                            </TableRow>
                          );
                        })
                      : <tr style={{ textAlign: 'center' }}>  <td colSpan={9}> There isn't any wallet </td> </tr>
                  }                    
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 49 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
          </Paper>


          <Dialog
            open={this.state.openDialogAddWallet}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="md"
          >
            <DialogTitle id="form-dialog-title"> {"Add Wallet"} <TextField
          id="filled-orderid-input"
          placeholder="Order Id"
          type="number"
          name="order_id"
          margin="normal"
          variant="filled"
          defaultValue={order_id}
          onChange={this.onOrderIdChange}
        /><Button
          variant="contained"
          color="primary"
          onClick={this.onClickHandler}
          margin="normal"
        >
          Get User Details
        </Button></DialogTitle>
            <form onSubmit={this.addWallet}>
            <DialogContent>
              {
                this.props.loading ? (
                  <div style={{ display: 'flex', justifyContent: 'center' }}> <CircularProgress className={classes.progress} /> </div> 
                ) : (
                  <React.Fragment>
                    <TextField
                      autoFocus
                      name="userId"
                      margin="dense"
                      id="userId"
                      placeholder="User Id"
                      type="number"
                      value = {user_id || this.state.user_id}
                      fullWidth
                      onChange = {this.onChangeUserIdHandler}
                    />
                    <TextField
                      name="emailId"
                      margin="dense"
                      id="emailId"
                      placeholder="Email Id"
                      type="email"
                      value = {user_email || this.state.email_id}
                      fullWidth
                      onChange= {this.onChangeEmailIdHandler}
                    />
                    <TextField
                      name="amount"
                      margin="dense"
                      id="amount"
                      label="Amount"
                      type="number"
                      fullWidth
                    />
                    <TextField
                      name="purpose"
                      margin="dense"
                      id="purpose"
                      label="Purpose"
                      type="text"
                      fullWidth
                    />
                    <TextField
                      name="addedBy"
                      margin="dense"
                      id="rec_name"
                      label="Added By"
                      type="text"
                      fullWidth
                    />
                  </React.Fragment>
                )
              }
            </DialogContent>
            {
              !this.props.loading ? (
            <DialogActions>
                  <Button onClick={this.handleClose} color="default">
                    Cancel
                  </Button>
                  <Button variant="outlined" type="reset">
                    Reset
                  </Button>
                  <Button variant="contained" type="submit" color="primary">
                    Add
                  </Button>
            </DialogActions>
          ) : ''
          
          }
          </form>

        </Dialog>

      </div>
    );
  }
}

Wallet.propTypes = {
  classes: PropTypes.object.isRequired,
  UserDetails: PropTypes.object.isRequired,
  getUserDetails: PropTypes.func.isRequired,
  getSearchResults: PropTypes.func.isRequired
};

const mapStateToProps = (state) => {
  const { walletList, loading } = state.walletBR;
  const UserDetails = state.userdetails.userdetails;
  const SearchResults = state.userdetails.SearchResults;
  // console.log("SearchResults,state.userdetails.SearchResults, UserDetails, walletList, loading ",SearchResults, state.userdetails.SearchResults, UserDetails, state.userdetails.userdetails, walletList, loading);
  return { walletList, loading, UserDetails, SearchResults };
};


export default connect(
  mapStateToProps,
  { getWalletList, addWallet, getUserDetails, getSearchResults}
)(withStyles(styles)(Wallet));
