import React, { Component } from 'react'
import { Link,Redirect} from 'react-router-dom'
import './Proceedpay.css'
import {OrderDetails,ShowRzPayErr,PapyPalError} from '../actions/cartAction';
import {Get_Rp_Id,Reset_Rp_Id} from '../actions/donationActions'
// import moduleName from '../actions/dona'
import { connect } from 'react-redux';
import axios from 'axios'
import config from 'react-global-configuration'
import MediaQuery from 'react-responsive'
// import PaypalExpressBtn from 'react-paypal-express-checkout'; Not in use
// import { PayPalButton } from "react-paypal-button-v2";  Not in use
// import paypal from 'paypal-checkout';
// import {paypal} from paypal-checkout-components
import ReactGA from 'react-ga';
import ReactDOM from 'react-dom';
// const PayPalButton = paypal.Button.driver('react', {React, ReactDOM});
import PayPalButton from './PayPalButton'
// import  from '../Home/images/300X240.png';
// import PaypalOffer from '../components/Home/images/PaypalOffer.png'

class ProceedToPay extends Component {

state={
   count:1,
   GotOThankYou:false,
   showPWait:false,
   PayPalPaymentErr:false,
   addedExtraFor80:false,
}
	// { "data" :
	// 	{
	// 	"amount"  : 100,
	// 	"no_of_book" : 2,
	// 	"payusing" : "test_2502",
	// 	"billing_add_id" : 15753,
	// 	"shipping_add_id" : 15753
			
	// 	}
	// } 
  closePopup=()=>{
    this.props.closePopup()
    this.setState({PayPalPaymentErr:false});
    this.setState({addedExtraFor80:false})


  }

// ***********************RAZORPAY PART********************************
options = {
  "key": "rzp_live_pvrJGzjDkVei3G", //Paste your API key here before clicking on the Pay Button. 
  // "key": "rzp_test_jxY6Dww4U2KiSA",
  "amount" : this.props.TotalPrice*100,
  "name": `Mypustak.com`,
  "description": `Total No of Books ${this.props.CartLength}`,
  "Order Id": `${this.props.RAZORPAY}`, //Razorpay Order id
  "currency" : "INR",


  "handler":  (response)=>{ 
  this.setState({showPWait:true})

  // this.props.Reset_Rp_Id()
      // console.log("in Handler Func")   
      const razorpay_payment_id=response.razorpay_payment_id;
      // const razorpay_order_id=response.razorpay_order_id;
      // const razorpay_signature=response.razorpay_signature;
      var AllbookId=[]
      var AllbookInvId=[]
      var AllrackNo=[]
      var AllQty=[]
     
       this.props.cartDetails.map((book,index)=>{
         
         AllbookId.push(`${book.bookId}`);
         AllbookInvId.push(book.bookInvId);
         AllrackNo.push(book.bookRackNo);
         AllQty.push(book.bookQty);
        //  index;
       })
      // console.log(AllbookId);
      
      const SendData={
        // {"payment_id":"12","payment_url":"www.test.com"}
        "payment_id":razorpay_payment_id,
        "payment_url":'https://razorpay.com/',
        "book_id":AllbookId,
        "book_inv_id":AllbookInvId,
        "rack_no":AllrackNo,
        "qty":AllQty,
      }
      // console.log(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`);
      
      // axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`  
      // axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
      axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
          // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
          'Authorization': `Token ${this.props.userToken}`,
          // ''
        }})
        .then(res=>{
          console.log('Getting Response ',this.props.WalletSelected);
          if(!this.props.WalletSelected){
              this.setState({GotOThankYou:true})
          }
      })
        .catch(err=>console.log(err,SendData) 
        )
      // }
      // )
        // this.setState({GotOThankYou:true})

        
        // --------------------------Add Wallet 
        if(this.props.WalletSelected){
        var today = new Date();
      var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var dateTime = date+' '+time;
      //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
      const SendDataWallet={
        "user_id":`${this.props.UserId}`,
        "user_email":`${this.props.UserEmail}`,
        "transaction_id":`PaidFromWallet${Math.round(this.props.walletbalance)}`,
        "deposit": 0,
        "withdrawl":Math.round(this.props.walletbalance),
        "payvia":"wallet",
        "time": `${dateTime}`,
        "comment":"Paid From Wallet",
        "added_by":`${this.props.UserEmail}`
      }
      axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendDataWallet,{
        headers:{'Authorization':`Token ${this.props.userToken}`,
                  'Content-Type':'application/json'}
      })
      .then(res=>{
        console.log("Wallet Transaction Done ");  
        this.setState({SuccessWalletAdded:true})
        this.setState({GotOThankYou:true})

      })
        .catch(err=>console.log(err,SendData))
    }

        if(razorpay_payment_id.length === 0){
          this.props.ShowRzPayErr()
        }
                                },

  "prefill": {
      // "method":"card,netbanking,wallet,emi,upi",
      "contact" : `${this.props.Selectedphone_no}`,
      "email" : `${this.props.UserEmail}`
      // "email":"ph15041996@gmail.com"
  },
         //             "notes": {
    //                     "order_id": "your_order_id",
    //                                 "transaction_id": "your transaction_id",
    //                                 "Receipt": “your_receipt_id"
    //             },
  "notes": {
      "Order Id": this.props.OrderId, //"order_id": "your_order_id", // Our Order id
      "address" : "customer address to be entered here"
  },
  "theme": {
      "color": "#1c92d2",
       "emi_mode" : true
              
  },
  
//   external: {
// //  wallets: ['mobikwik' , 'paytm' , 'jiomoney' , 'payumoney'],
// wallets: ['paytm'],

//   handler: function(data) {
//   console.log(this, data)
//   }
// }
};

componentDidMount(){
  ReactGA.pageview(window.location.pathname + window.location.search);
  
  // this.setState({GotOThankYou:false})
  // this.props.GetOrderId(data)
  this.rzp1 = new window.Razorpay(this.options);
  }
  closeProcedToPay(){
  this.setState({addedExtraFor80:true})
  this.props.closeProcedToPay()
}
  render() {
    console.log(this.props.WalletSelected,"showSelected");
    
    if(this.state.GotOThankYou){
      return <Redirect to="view-cart/thank-you"/>
    }
    // alert(this.props.Selectedphone_no)
    // console.log(this.props.TotalPrice)
  //   const OrderData={
  //     data :{
  //    amount  : this.props.CartPrice.TotalPayment,
  //    no_of_book : 2,
  //    payusing : "razorpay",
  //    billing_add_id : 15753,
  //    shipping_add_id : 15753,
  //    }
     
  //  }



//    { "data" :
//    {
//    "amount"  : 100,
//    "no_of_book" : 2,
//    "payusing" : "test_2502",
//    "billing_add_id" : 15753,
//    "shipping_add_id" : 15753
     
//    }
// //  }
// const token= `Token ${this.props.userToken}`

// if(this.props.OrderId !== 0 && this.props.RAZORPAY === 0){
//   let  Rpdata={
//     data :
//     {
//         ref_id : `${this.props.OrderId}`,
//         amount : `${this.props.CartPrice.TotalPayment}`,
//     }
// }
// // console.log(data);

//     this.props.Get_Rp_Id(Rpdata)
//     alert("l")
// }
// let count =1
// if(this.props.RAZORPAY !== 0 && this.state.count === 1){
//   alert("o")
//   this.rzp1.open();
//   // count+=2 
//   this.setState({count:2})
// }
const {SelectedAddress} =this.props
//            ******************************************PayPal Part**************************************
// const onSuccess = (actions) => {
//   console.log("success paypal Payment");  
//   var payment = actions
//   this.setState({showPWait:true})
//             // Congratulation, it came here means everything's fine!
//       console.log("The payment was succeeded!", payment);
//       var AllbookId=[]
//       var AllbookInvId=[]
//       var AllrackNo=[]
//       var AllQty=[]
     
//        this.props.cartDetails.map((book,index)=>{
         
//          AllbookId.push(`${book.bookId}`);
//          AllbookInvId.push(book.bookInvId);
//          AllrackNo.push(book.bookRackNo);
//          AllQty.push(book.bookQty);
//         //  index;
//        })
//       // console.log(AllbookId);
      
//       const SendData={
//         // {"payment_id":"12","payment_url":"www.test.com"}
//         "payment_id":payment.paymentID,
//         "payment_url":'https://paypal.com/',
//         "payusing":"Paypal",
//         "book_id":AllbookId,
//         "book_inv_id":AllbookInvId,
//         "rack_no":AllrackNo,
//         "qty":AllQty,
//       }
//       axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
//           'Authorization': `Token ${this.props.userToken}`,
//           // ''
//         }})
//         .then(res=>{
//           console.log('Getting Response ');
//         this.setState({GotOThankYou:true})

//       })
//         .catch(err=>console.log(err,SendData) 
        // )
      // }
      // )
        // this.setState({GotOThankYou:true})

        
        // --------------------------Add Wallet 
    //     if(this.props.walletRazorpay.length !== 0){
    //     var today = new Date();
    //   var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    //   var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    //   var dateTime = date+' '+time;
    //   //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
    //   const SendDataWallet={
    //     "user_id":`${this.props.UserId}`,
    //     "user_email":`${this.props.UserEmail}`,
    //     "transaction_id":`PaidFromWallet${Math.round(this.props.walletRazorpay.walletbalance)}`,
    //     "deposit": 0,
    //     "withdrawl":Math.round(this.props.walletRazorpay.walletbalance),
    //     "payvia":"wallet",
    //     "time": `${dateTime}`,
    //     "comment":"Paid From Wallet",
    //     "added_by":`${this.props.UserEmail}`
    //   }
    //   axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendDataWallet,{
    //     headers:{'Authorization':`Token ${this.props.userToken}`,
    //               'Content-Type':'application/json'}
    //   })
    //   .then(res=>{
    //     console.log("Wallet Transaction Done ");  
    //     this.setState({SuccessWalletAdded:true})

    //   })
    //     .catch(err=>console.log(err,SendData))
    // }
                // You can bind the "payment" object's value to your state or props or whatever here, please see below for sample returned data
                
                
        // }


    var PassPayPal_Items=[]
    var TotalBookShipping=0
    this.props.cartDetails.map(books=>{
        var NewBookName=""
      if(Number(books.bookName.lenght) > 127 ){
        NewBookName=books.bookName.slice(0,120)
        console.log(NewBookName,"kk");
        
      }else{
        NewBookName=books.bookName
        console.log(NewBookName,"000",books.bookName.lenght);

      }
      // (books.bookName.lenght > 128 )?NewBookName+=books.bookName.substr(0,120):NewBookName+=books.bookName
      console.log(books.bookName.length,"length",books.bookName.slice(0,120), NewBookName );
      TotalBookShipping+=books.bookShippingCost
      var PassingItem ={
        name:(books.bookName.length > 127)?books.bookName.slice(0,120):books.bookName,
        unit_amount: {
              currency_code: "INR",
              value: books.bookShippingCost
                    },
              quantity:books.bookQty
      }
      
      PassPayPal_Items.push(PassingItem)
    })
    console.log(PassPayPal_Items,"items");
    
    

    const onApprove = (data,actions) =>{
      console.log("In Approve");
      this.setState({showPWait:true})

      actions.order
      .capture()
      .then((details) => {
        console.log("Okk",details);
          if(details.status == "COMPLETED"){
        // var payment = actions
                // Congratulation, it came here means everything's fine!
          // console.log("The payment was succeeded!", payment);
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
        
          this.props.cartDetails.map((book,index)=>{
            
            AllbookId.push(`${book.bookId}`);
            AllbookInvId.push(book.bookInvId);
            AllrackNo.push(book.bookRackNo);
            AllQty.push(book.bookQty);
            //  index;
          })
          // console.log(AllbookId);
          
          const SendData={
            // {"payment_id":"12","payment_url":"www.test.com"}
            "payment_id":details.purchase_units[0].payments.captures[0].id,
            "payment_url":'https://paypal.com/',
            "payusing":"Paypal",
            "book_id":AllbookId,
            "book_inv_id":AllbookInvId,
            "rack_no":AllrackNo,
            "qty":AllQty,
          }
          axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
              'Authorization': `Token ${this.props.userToken}`,
              // ''
            }})
            .then(res=>{
              console.log('Getting Response ');
            if(!this.props.WalletSelected){

            this.setState({GotOThankYou:true})

            }else{

            }

          })
            .catch(err=>console.log(err,SendData) 
            )
          // }
          // )
            // this.setState({GotOThankYou:true})

            // --------------------------Add Wallet 
            if(this.props.WalletSelected){
            var today = new Date();
          var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
          var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
          var dateTime = date+' '+time;
          //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
          const SendDataWallet={
            "user_id":`${this.props.UserId}`,
            "user_email":`${this.props.UserEmail}`,
            "transaction_id":`PaidFromWallet${Math.round(this.props.walletbalance)}`,
            "deposit": 0,
            "withdrawl":Math.round(this.props.walletbalance),
            "payvia":"wallet",
            "time": `${dateTime}`,
            "comment":"Paid From Wallet",
            "added_by":`${this.props.UserEmail}`
          }
          axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendDataWallet,{
            headers:{'Authorization':`Token ${this.props.userToken}`,
                      'Content-Type':'application/json'}
          })
          .then(res=>{
            console.log("Wallet Transaction Done ");  
            this.setState({SuccessWalletAdded:true})
            this.setState({GotOThankYou:true})

          })
          .catch(err=>console.log(err,SendData))
          }
                      // You can bind the "payment" object's value to your state or props or whatever here, please see below for sample returned data

        }else{
          this.setState({PayPalPaymentErr:true});
            const SendData={
              "payusing":"Paypal",
              "payment_url" :`${details.status}`,

            }
            this.props.PapyPalError(this.props.userToken,SendData,this.props.OrderId);
        }
      })
      .catch((err) => {

        console.log(err,"Error In Approve");
        
          if (this.props.catchError) {
              console.log(this.props.catchError(err));
          }
        });
      
    }
    const PayPalAreaCode =(stateName) =>{
      console.log(stateName,"PassingState");
      
      // if(stateName == "WEST BENGAL"){
      //   return "West Bengal"
      // }
      if(stateName == "JAMMU & KASHMIR"){
        return "Jammu and Kashmir"
      }
      if(stateName == "DELHI"){
        return "Delhi (NCT)"
      }
      if(stateName == "DADRA AND NAGAR HAVELI"){
        return "Dadra and Nagar Haveli"
      }

      if(stateName == "DAMAN & DIU"){
        return "Daman and Diu"
      }
      if(stateName == "ANDAMAN & NICOBAR" || stateName == "Andaman and Nicobar Islands"){
        return "Andaman and Nicobar Islands"
      }

      var lowerCaseStateName = stateName.toLowerCase()
      console.log(lowerCaseStateName.split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' '));
      return lowerCaseStateName.split(' ').map(s => s.charAt(0).toUpperCase() + s.slice(1)).join(' ');
    }
    var Ccount=1
    const createOrder = (data,actions) =>{
    // alert(Ccount)
    Ccount+=1
    console.log("In CreateOrder",TotalBookShipping,this.props.CartPrice.wallet,Math.round(TotalBookShipping-this.props.CartPrice.wallet));
    if(this.props.TotalPrice == 80 && !this.state.addedExtraFor80){
      var Passing_ShippingCharge_Item ={
        name:"Extra Shipping Charge",
        unit_amount: {
              currency_code: "INR",
              value: 80-TotalBookShipping
                    },
              quantity:1
      }
      this.setState({addedExtraFor80:true})
      PassPayPal_Items.push(Passing_ShippingCharge_Item)
      console.log("added for 80");
      
    }
    return actions.order.create({
      application_context:{
        brand_name:"MyPustak",
        locale:"en-IN",
        user_action:"PAY_NOW",
        shipping_preference:"SET_PROVIDED_ADDRESS",
        // payment_method:{
        // payer_selected:"PAYPAL"
        // }
      },payer:{
              name:{
                    given_name:this.props.getuserdetails.name,
                    surname:this.props.getuserdetails.name,
              },
              phone: {
                phone_type: 'MOBILE',
                phone_number: {
                                national_number: `91${this.props.Selectedphone_no}`
                              }
              },
              email_address: this.props.UserEmail,             
              address: {
                    address_line_1: this.props.SelectedAddress.address,
                    postal_code: this.props.SelectedAddress.pincode,
                    country_code: "IN",
                    admin_area_1: PayPalAreaCode(this.props.SelectedAddress.state_name).replace(/[\n\r]/g, ''),
                    admin_area_2: this.props.SelectedAddress.city_name
              },
      },
//       purchase_units: [{
//         "invoice_id": this.props.OrderId,
//        amount: {
//                      currency_code: "INR",
//                      value: this.props.TotalPrice,
//        },
//        items: [
//        {
//                      name: "Item 1",
//                      unit_amount: {
//                                    currency_code: "INR",
//                                    value: "9.00"
//                      },
//                      quantity: "1",
//        },
//        {
//                      name: "Item 2",
//                      unit_amount: {
//                                    currency_code: "INR",
//                                    value: "9.00"
//                      },                          
//                      quantity: "1",
//        }
//        ]

// }]
// MyPustak.com,First Floor ,East Bera beri , shiker Bagan , gopalpur, Kolkata- 700136 

        purchase_units: [{
                      "invoice_id": this.props.OrderId  ,
                      shipping: {
                        address: {
                
                          address_line_1: this.props.SelectedAddress.address,
                          postal_code: this.props.SelectedAddress.pincode,
                          country_code: "IN",
                          admin_area_1: PayPalAreaCode(this.props.SelectedAddress.state_name),
                          admin_area_2: this.props.SelectedAddress.city_name
                
                        }
                      },
                      amount: {
                      currency_code: "INR",
                      value: (this.props.CartPrice.wallet !==0)?Math.round(TotalBookShipping-this.props.CartPrice.wallet):this.props.TotalPrice, 
                      breakdown: {
                                item_total: {
                                          currency_code: "INR",
                                          value: (this.props.CartPrice.wallet !==0)?TotalBookShipping:this.props.TotalPrice
                                },
                                discount: {
                                currency_code: "INR",
                                value: (this.props.CartPrice.wallet !==0)?Math.round(this.props.CartPrice.wallet):0 // “discount” amount as positive value. Use “discount” as all small caps
                                }
                        }
                      },
                      items: PassPayPal_Items

        }]

        });
    //  return data.id; 
    
      }
 



        const onCancel = (data) => {
            // User pressed "cancel" or close Paypal's popup!
            console.log('The payment was cancelled!', data);
            const SendData={
              "payusing":"Paypal",
              "status":2
            }
            this.props.PapyPalError(this.props.userToken,SendData,this.props.OrderId);
            // You can bind the "data" object's value to your state or props or whatever here, please see below for sample returned data
        }
 


        const onError = (err) => {
            // The main Paypal's script cannot be loaded or somethings block the loading of that script!
            console.log("Error!", err);
            // err.map((data)=>{
            //   console.log(data.Res);
              
            // })
            this.setState({PayPalPaymentErr:true});
            const SendData={
              "payusing":"Paypal",
              "payment_url" :`${err}`,

            }
            this.props.PapyPalError(this.props.userToken,SendData,this.props.OrderId);
            // Because the Paypal's main script is loaded asynchronously from "https://www.paypalobjects.com/api/checkout.js"
            // => sometimes it may take about 0.5 second for everything to get set, or for the button to appear
        }
 
        const ENV = process.env.NODE_ENV === 'production'
                  ? 'production'
                  : 'sandbox'; // you can set here to 'production' for production
        let currency = 'INR'; // or you can set this value from your props or state
        let total = this.props.TotalPrice; // same as above, this is the total amount (based on currency) to be paid by using Paypal express checkout
        // Document on Paypal's currency code: https://developer.paypal.com/docs/classic/api/currency_codes/
 
        const client = {
            sandbox:    'AduSZOo6hVpXQkA0T_g1uC09oduf03joly63Sm2qCPyIl8580IVc9VhV3rhNLF1IZFX8kuaDkVVAEX0O',
            production: 'EDePA-AO2OnrxSwxtfyJ8KWkiqP4NU2uFwVSzZRPJKiRaL_T6Hf0nMDhVEUb0PSfSNElrS8BYsTzsoEw',
        }
        // paypal.Buttons().render('#paypal-button-container');
        const opts = {
          env: 'sandbox',
          style : { 
            layout:  'horizontal',
            color:   'blue',
            shape:   'rect',
            label:   'pay'
          }
      };

      const PayPalButton = window.paypal.Buttons.driver("react", {
        React,
        ReactDOM,
    });
    return (
      <div>
        {/* // -------------------------------- desktop ------------------------ */}

      <MediaQuery minWidth={992}>
      <div style={{ fontSize:'1.3vw',textAlign:'center'}}>
        <p style={{ textAlign:'end',lineHeight:'0px',fontWeight:'bold',cursor:'pointer'}}>
        <a style={{ color:'rgb(0,186,242)' }} onClick={this.closePopup}> &times;</a>

        </p>
          <p style={{ padding:'1%' }}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.CartLength}</p>
          <button id="PopupProceedToPayBtn" onClick={()=>this.rzp1.open()} >Confirm And Pay Rs. {this.props.TotalPrice}</button>
          {/* <p id="Payusing1">Payment Method Available (Card,Netbanking,Wallet,Upi)</p>
          <hr style={{ color:'black',border:'1px solid #1c86de' }}/> */}
          <div style={{ margin:'5%' }}> 
                            {/* ----------------------Test this one----------------------------------- */}

          {/* <div id="paypal-button" style={{ 'marginTop': '5px' }}>

          <PayPalButton
            env={ENV} client={client} 
            currency={currency} total={total} 
            onError={onError}
            createOrder={createOrder}
            // onSuccess={onSuccess}
            onApprove={onApprove}
            onCancel={onCancel} 
            // onAuthorize={onAuthorize}
            style= {{ 
               layout:  'horizontal',
               color:   'blue',
               shape:   'rect',
               label:   'pay'
            }
            }
             />

                    </div>  */}
                        
             </div>
             {/* <ima */}
             {/* <p id="Payusing2">Pay Using Paypal And Get 50% CashBack Upto Rs.200 In Your PayPal Wallet<span id="InPayusing2"><br/>(For First Time PayPal Users Only)</span></p> */}
             {/* <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
             {/* <img src={PaypalOffer} id="PayPalOfferImage"/>
             <p id={(this.state.PayPalPaymentErr)?"plWait":"NoplWait"}>Error Occoured Please Contact Customer Support</p>
          <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
        </div>
    
        </MediaQuery>
        {/* // --------------------------------tab ------------------------ */}
              <MediaQuery maxWidth={991} and minWidth={768}>
              <div style={{ textAlign:'center' ,fontSize:'2vw' }}>
              <p style={{ textAlign:'end',lineHeight:'0px',fontWeight:'bold',cursor:'pointer'}}>
                  <a style={{ color:'rgb(0,186,242)' }} onClick={this.closePopup}> &times;</a>
              </p>
                <p style={{ padding:'1%' }}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.CartLength}</p>
                <button id="PopupProceedToPayBtn" onClick={()=>this.rzp1.open()} >Confirm And Pay Rs. {this.props.TotalPrice}</button>
                {/* <p id="Payusing1">Payment Method Available (Card,Netbanking,Wallet,Upi)</p> */}
                            {/* ----------------------Test this one----------------------------------- */}
                {/* <p>-----------------------------------------------</p> */}
                {/* <hr/> */}
                {/* <div id="paypal-button" style={{ 'marginTop': '5px' }}>
                    <PayPalButton
                      env={ENV} client={client} 
                      currency={currency} total={total} 
                      onError={onError}
                      createOrder={createOrder}
                      // onSuccess={onSuccess}
                      onApprove={onApprove}
                      onCancel={onCancel} 
                      // onAuthorize={onAuthorize}
                      style= {{ 
                        layout:  'horizontal',
                        color:   'blue',
                        shape:   'rect',
                        label:   'pay'
                      }
                      }
                      />

                </div>        */}
             {/* <p id="Payusing2">Pay Using Paypal And Get 50% CashBack In Your PayPal Wallet<span id="InPayusing2"><br/>(For First Time PayPal Users Only)</span></p> */}
             {/* <img src={PaypalOffer} id="PayPalOfferImage"/> */}
             {/* <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
             {/* <p id={(this.state.PayPalPaymentErr)?"plWait":"NoplWait"}>Error Occoured Please Contact Customer Support</p> */}
                {/* <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
              </div>
              </MediaQuery>

              {/*----------------------- larger mobile--------------------------- */}
              <MediaQuery maxWidth={767} and minWidth={539}>
              <div style={{fontSize:'2.5vw',textAlign:'center' }}>
              <p style={{ textAlign:'end',lineHeight:'0px',fontWeight:'bold'}}>
                  <a style={{ color:'rgb(0,186,242)' }} onClick={this.closePopup}> &times;</a>
              </p>
                <p style={{ padding:'1%' }}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.CartLength}</p>
                <button id="PopupProceedToPayBtn" onClick={()=>this.rzp1.open()} >Confirm And Pay Rs. {this.props.TotalPrice}</button>
                {/* <div style={{ margin:'5%' }}> */}
                {/* <p id="Payusing1">Payment Method Available (Card,Netbanking,Wallet,Upi)</p> */}

          {/* <div id="paypal-button" style={{ 'marginTop': '5px' }}>
              <PayPalButton
                env={ENV} client={client} 
                currency={currency} total={total} 
                onError={onError}
                createOrder={createOrder}
                // onSuccess={onSuccess}
                onApprove={onApprove}
                onCancel={onCancel} 
                // onAuthorize={onAuthorize}
                style= {{ 
                  layout:  'horizontal',
                  color:   'blue',
                  shape:   'rect',
                  label:   'pay'
                }
                }
                />
          </div>  */}
                        
             {/* <p id="Payusing2">Pay Using Paypal And Get 50% CashBack In Your PayPal Wallet<span id="InPayusing2"><br/>(For First Time PayPal Users Only)</span></p> */}
             {/* <img src={PaypalOffer} id="PayPalOfferImage"/> */}
              {/* </div> */}
             {/* <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
            {/* <p id={(this.state.PayPalPaymentErr)?"plWait":"NoplWait"}>Error Occoured Please Contact Customer Support</p>
            <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
              </div>
              </MediaQuery>
                            {/*----------------- smaller mobile-------------------*/}
                            <MediaQuery maxWidth={538}>
              <div style={{fontSize:'3vw',textAlign:'center',marginTop:'1%' }}>
              <p style={{ textAlign:'end',lineHeight:'0px',fontWeight:'bold'}}>
                  <a style={{ color:'rgb(0,186,242)' }} onClick={this.closePopup}> &times;</a>
              </p>
                <p style={{ padding:'1%' }}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.CartLength}</p>
                <button id="PopupProceedToPayBtn" onClick={()=>this.rzp1.open()} >Confirm And Pay Rs. {this.props.TotalPrice}</button>
                {/* <p id="Payusing1">Payment Method Available (Card,Netbanking,Wallet,Upi)</p> */}
          {/* <div style={{ margin:'5%' }}> */}
                            {/* ----------------------Test this one----------------------------------- */}
{/* 
          <div id="paypal-button" style={{ 'marginTop': '5px' }}>
          <PayPalButton
            env={ENV} client={client} 
            currency={currency} total={total} 
            onError={onError}
            createOrder={createOrder}
            // onSuccess={onSuccess}
            onApprove={onApprove}
            onCancel={onCancel} 
            // onAuthorize={onAuthorize}
            style= {{ 
               layout:  'horizontal',
               color:   'blue',
               shape:   'rect',
               label:   'pay'
            }
            }
             />

                    </div> 
                        
             </div> */}
             {/* <p id="Payusing2">Pay Using Paypal And Get 50% CashBack In Your PayPal Wallet<span id="InPayusing2"><br/>(For First Time PayPal Users Only)</span></p> */}
             {/* <p>*</p> */}
             {/* <img src={PaypalOffer} id="PayPalOfferImage"/> */}
             {/* <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
             {/* <p id={(this.state.PayPalPaymentErr)?"plWait":"NoplWait"}>Error Occoured Please Contact Customer Support</p>
             <p id={(this.state.showPWait)?"plWait":"NoplWait"}>Please Wait....</p> */}
              </div>
              </MediaQuery>
              </div>
    );
  }
}

const mapStateToProps = state => ({
  getuserdetails:state.userdetailsR.getuserdetails,
  // getadd:state.accountR.getadd,
  userToken:state.accountR.token,
  CartPrice:state.cartReduc.CartPrice,
  CartLength:state.cartReduc.cartLength,
  cartDetails:state.cartReduc.MyCart,
  walletRazorpay:state.cartReduc.walletRazorpayState,
  // AddresId:state.cartReduc.AddresId,
  OrderId:state.cartReduc.OrderId,
  RAZORPAY:state.donationR.rp_id,
  TotalPrice:state.cartReduc.TotalPrice,
  UserEmail:state.userdetailsR.getuserdetails.email,
  Selectedphone_no:state.accountR.selectedAddress.phone_no,
  // Selectedphone_no:state.accountR.getuserdetails.phone,
  SelectedAddress:state.accountR.selectedAddress,
  walletbalance:state.walletR.walletbalance,

})

export default connect(mapStateToProps,{OrderDetails,Get_Rp_Id,ShowRzPayErr,Reset_Rp_Id,PapyPalError})(ProceedToPay)