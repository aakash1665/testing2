import React, {Component} from 'react';
import './sideDrawer.css';
import { Link,Redirect,browserHistory } from 'react-router-dom';
import { connect } from 'react-redux';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import {AddToCart,CartopenModal,CartcloseModal,removeAllCart,RemoveCart,CartSession,ToBeAddedToCart,RemoveToBeAddedToCart,removeFromCartLogout,MobileCartRedirect} from '../../../actions/cartAction';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

class SideDrawer extends Component {
    state = {
      openDialog:false,
      bookinvetry:' ' ,
      cartid:' ',
    }

    RemoveFromCart=(bookInvId,Cart_id)=>{

      // alert(bookInvId,Cart_id)

        for (var i=0; i<=sessionStorage.length-1; i++)  
        {   
          // alert("rmo")
            let key = sessionStorage.key(i);  
            try {
              if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){
                  let val = JSON.parse(sessionStorage.getItem(key));
                  if(`${val.bookInvId}` === `${bookInvId}`)  {
                    // alert("rm")
                    sessionStorage.removeItem(bookInvId);
                  }
              }
            } catch (error) {
              console.log(error);
              
            }
    
            // val.map(det=>console.log(det))
            // this.props.AddToCart(val)
            // alert(val)
        }
        if(localStorage.getItem('user') === null){
    
        this.props.removeFromCartLogout(bookInvId)
        }else{
          const data={"is_deleted":"Y"}
    
        this.props.RemoveCart(Cart_id,bookInvId,data)
    
        }
        this.closeDialog()
       }
        CheckLogin=()=>{
        if(this.props.userToken === null){
         this.ToggleASignupOpenModal()
         this.setState({goTocart:true})
         // if(this.props.userToken !== null){
         //   this.setState({RedirectCart:true})
         // }
       }else{
         this.setState({RedirectCart:true})
       }
       }

       closeDialog = () =>{
        
        this.setState({openDialog: false})
      }

      ConfRemoveFromCart = (bookInvId,Cart_id) =>{
        this.setState({bookinvetry:bookInvId,cartid:Cart_id, openDialog:true})

      }

      rerendercond =(condition)=>{
          if(condition =="AverageButInReadableCondition"){
              return "Readable"

          }
          else{
            return condition
          }
      }
    render () {
      const {cartDetails} =this.props;
        let drawerClasses = 'side-Drawer';
        var ShipCost =0;
        console.log(this.props.show,"show");
        
        if (this.props.show)
        {
          // alert(this.props.show)
            drawerClasses = 'side-Drawer open';
        }
        
        cartDetails.map(cart=> ShipCost+=Number(cart.bookShippingCost))
      return (
     <div>
              <nav className={drawerClasses}>
         <div id="Maincartbody"> 
          <div id ="cartHeader">
              <div id= "cartTitle"><span id="icon"><i class="fas fa-shopping-cart"></i></span>Cart Details
              <hr/>
              <div id="CarttotalAmount"> 
              <div id ="nobooks">#Books:&nbsp;{cartDetails.length}</div>
              <span id="amtbook">Total Amount &#8377;{ShipCost} </span> 
                  
              </div>
              <div id="cartbutton">
                
              {cartDetails.length > 0 ?
              <Link to="/view-cart"> 
                  <button id="BgCartcheckoutBtn" onClick={()=>this.CheckLogin()} >Proceed To Checkout</button></Link>:
                      <Link to="/"><button id="continueShopingCart">Continue Shopping  </button></Link>
                }

                  {/* <Link to=""><button id="continueShopingCart">Continue Shopping > </button></Link> */}
              </div>
              </div>
                  
            </div>
        
        <div id ="BgCartPopupScroll">
          {cartDetails.map(cart=>

            <div id="BgCartPopupBody">
               <div id="BgCartBookName">
                        <span id="">{cart.bookName}</span>
                    </div>
                <div id="BgCartBookData">
                    <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${cart.bookThumb}`}  style={{ float:'left',height:'30%',width:'15%' }}id="book"/>
                      <div id="BgCartBookCondPrice">
                        <div id="BgCartBookprice">
                              <div id= "mrptag">
                                <span id="mrp">MRP:
                                  <span id="orgprice"> &#8377;<strike>{Math.round(cart.bookPrice ) }</strike></span>
                                </span>
                            <span style={{ color:'green'}}>&nbsp;(Free)</span></div>
                            <div id = "bookcond" >Book Condition :{this.rerendercond(cart.bookCond)}<label id="newprice"></label></div>
                            
                        </div>          
                       <div id= "shippingCost"> <span id="BgCartBookshipping">Shipping & Handling Charges &#8377;{Math.round(cart.bookShippingCost)}</span></div>
                        <div><button id="removebtn" onClick={()=>this.ConfRemoveFromCart(cart.bookInvId,cart.Cart_id)}>Remove</button></div>
                        

                    </div>
                   
                    {/* <div id="BgCartBookName">
                        <span id="">{cart.bookName}</span>
                    </div> */}
                 
                </div>
                <hr id=""/>
            </div>
            )}
            {cartDetails.length == 0 ?<div id = "cartmsg">Looks like your cart is empty.Keep Shopping!</div>:null}
            </div>
            {/* <div id="cartbuttonblw">
               <div id="CarttotalAmountblw">Total Amount: &nbsp;&#8377;{ShipCost}</div>
                <Link to="/view-cart"> 
                 <button id="BgCartcheckoutBtn" onClick={()=>this.CheckLogin()} >Proceed To Checkout</button></Link>

                  <Link to=""><button id="continueShopingCart">Continue Shopping > </button></Link>
                   </div>
                   */}
           

    </div>

              </nav>

              <Dialog
            open={this.state.openDialog}
            onClose={this.closeDialog}
            aria-labelledby="form-dialog-title"
            TransitionComponent={Transition}
            transitionDuration ={600}
            scroll={"body"}
            size="sm" >
            <DialogTitle id="form-dialog-title">{'Are you sure you want to delete?'}</DialogTitle>
            <DialogContent>
                <div id = "dialogmsg">
                   
                    <button onClick = {()=>this.closeDialog()}>No</button>
                    <button onClick={()=>this.RemoveFromCart(this.state.bookinvetry,this.state.cartid)}>Yes</button>
                   
                   
                </div>

            </DialogContent>

        </Dialog>

          </div>
 
            
      
      )
    }
  }
  const mapStateToProps = state => ({
   
    userToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
    cartDetails:state.cartReduc.MyCart,
    PopupCart:state.cartReduc.PopupCart,
    ItemsInCart:state.cartReduc.cartLength,
 
    ClickedCategory:state.homeR.ClickedCategory,

    CartSessionData:state.cartReduc.CartSessionData,
    ToBeAddedToCartState:state.cartReduc.ToBeAddedToCartState,
    cartRedirectMobile:state.cartReduc.cartRedirectMobile,


  })
  
  export default connect(mapStateToProps,{removeFromCartLogout,RemoveCart} )(SideDrawer);
//   export default connect(mapStateToProps,{navurl,setCategory,CartopenModal,CartcloseModal,doSearch,logout,makeSearchBlank,CartSession,ToBeAddedToCart,RemoveToBeAddedToCart,removeFromCartLogout,MobileCartRedirect,
//     AddToCart,removeAllCart,getSavedToken,RemoveCart,Getaddress,userdetails,Editaddress,addAddressAction,ActivteSuccesPopupOther,AfterLoginRedirect})(MainHeader)
