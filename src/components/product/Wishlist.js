import React, { Component } from 'react';
import './wishlist.css';
import {Helmet} from 'react-helmet';
// import book from './book.jfif';
// import del from './Product_Images/del.png';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import {Uwishlist,SetWishlistReloader} from '../../actions/wishlistAction'
import {AddToCart,CartopenModal,CartSession} from '../../actions/cartAction'
import { connect } from 'react-redux';
import config from 'react-global-configuration'
import axios from 'axios'

class Wishlist extends Component {
  state={
    wl_id:'',
    count:1,
    userToken:'',
    AlreadyinCartMsg:'',
    alreadyCartId:'',
  }
  DeleteDone=()=>{
    // window.location.href = `/my-wish-list`
   }

  componentDidMount() {
    window.scrollTo(0, 0);

    if(localStorage.getItem('user') === null){
      window.location.href="/"
    }else{
    const details=`Token ${this.props.userToken}`
    this.props.Uwishlist(details)
    }
  }
  componentWillReceiveProps(userToken){
    // console.log(userToken.userToken);
    if(this.state.userToken !== userToken.userToken){
    const details=`Token ${userToken.userToken}`
    this.props.Uwishlist(details);
    this.setState({userToken:userToken.userToken})
  }
    
  }
render(){
  const {userwishlistInv} =this.props

  if(this.props.userToken !== null && this.state.count ===1){
    const details=`Token ${this.props.userToken}`
    this.props.Uwishlist(details)
    this.setState({count:2})
  }
  const toCart=(book)=>{
 
    AddingToCart(book)
}
var alreadyCartId=0
const AddingToCart=(book)=>{
  var msgg="";

  var SHIPPINGCOST=''
  var DonorName=''
  // var TOTALQUANTITY=''
  var RACK_NO=''
  var BOOK_INV_ID=''
  var BOOK_COND=''
  Object.keys(userwishlistInv).map(function(Invbook){
    // RACK_NO=Invbook
    if(userwishlistInv[Invbook].wl_id == book.wl_id){
      RACK_NO=userwishlistInv[Invbook].rack_no;
      BOOK_COND=userwishlistInv[Invbook].book_condition;
      SHIPPINGCOST=userwishlistInv[Invbook].new_pricing_model;
      BOOK_INV_ID=userwishlistInv[Invbook].book_inv_id;

      // console.log(RACK_NO);

    }
    // console.log(userwishlistInv[Invbook].rack_no);
    
  })

  this.props.FromMycart.map(cart=>{
    // alert("runn")
    // (`${cart.bookInvId}`===`${BOOK_INV_ID}`)?  msgg="already in cart": null
    if(`${cart.bookInvId}`===`${BOOK_INV_ID}`)
    {
      msgg="already in cart";
      alreadyCartId=cart.wl_id;
  // this.setState({alreadyCartId:cart.wl_id})

    } 
    
  } )
  if(msgg === "")
  {
  const MyCart ={
    bookId:book.book_id,
    bookName:book.title,
    bookSlug:book.slug,
    // bookAuthor:book.author,
    // bookShippingCost:Math.round(SHIPPING),
    bookThumb:book.thumb,
    // bookDonor:DonorName,
    bookQty:1,
    bookCond:BOOK_COND,
    bookRackNo:RACK_NO,
    bookInvId:BOOK_INV_ID,
    // bookPrice:,
    bookShippingCost:Math.round(SHIPPINGCOST),
    // bookDonner
  }
  console.log(MyCart);
  
  this.props.CartopenModal()
  // alert('get')
  // let data = sessionStorage.getItem(RACK_NO)

  const sendCartSession = { 
    "book_id" : book.book_id,
    "book_inv_id":BOOK_INV_ID,
    'content-type': 'application/json',
     }

     axios.post(`${config.get('apiDomain')}/common/addtocart/`,sendCartSession,{headers:{
              'Authorization': `Token ${this.props.userToken}`
            }} )
     .then(res=>{console.log(res.status);
          // RefreshCart(); 
          // const RefreshCart=()=>{
            const token= localStorage.getItem('user')
            const details=`Token ${token}`
            this.props.CartSession(details)
            // console.log("Refreshing");
            // alert("Refreshing")
          // }               
    }
     )
     .catch(err=>{console.log(err.response.status);console.log(sendCartSession);}
     )


     const token= localStorage.getItem('user')
     const details=`Token ${token}`
  // this.props.AddToCart(MyCart)
  this.props.CartSession(details)

}else{
  window.scrollTo(0, 0);
  this.setState({AlreadyinCartMsg:"Already In cart"})
}
if(this.state.AlreadyinCartMsg.length !== 0){
  setTimeout(()=>{this.setState({AlreadyinCartMsg:""})},3000)
}
  this.props.CartopenModal()
}
if(this.props.ReloadWishlist){
  const details=`Token ${this.props.userToken}`
  this.props.Uwishlist(details);
  this.props.SetWishlistReloader()

}
const Deletewishlist=(wl_id)=>{
  // alert('ok')
  // const {wl_id}=this.state
  const token=`Token ${this.props.userToken}`
  // e.preventDefault();
  // console.log("ok")
  axios.delete(`${config.get('apiDomain')}/api/v1/wishlist/delete-wishlist/${wl_id}`,{headers:{
    'Authorization': token,
  }}  
  ).then(res=>{ 
    this.props.SetWishlistReloader()
    console.log(`${res.data.message}`);
    }).catch(err=>console.log());
};
if(this.state.AlreadyinCartMsg.length !== 0){
  setTimeout(()=>{this.setState({AlreadyinCartMsg:""})},3000)
}
  const {userwishlistBook}=this.props
  console.log();
  // alert(this.state.AlreadyinCartMsg)
return (
<div>
  <MainHeader/>
  <Helmet>
<style>{'body{background-color:#f3f7f8;}'}</style></Helmet>
<p id="My-Wishlist">My Wishlist({userwishlistBook.length})<span id="WishlistAlert">{this.state.AlreadyinCartMsg}</span></p>
<hr />
{userwishlistBook.map(wishlist=>
<div id="div3">
<img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${wishlist.thumb}`} id="wishlistbookimg" alt="Book Image"/>
<p id="wishlistbookauthor">{wishlist.title} By {wishlist.author}</p>
<p id="wishlistbookpubl"><span style={{color: '#9e9e9e'}}>Publication:</span> {wishlist.publication} </p>
<p id="wishlistbookisbn">ISBN: </p>
<div id="rightPartdiv3">
<div style={{ flex:'1' }}>
<button id="wishAddCartBtn" onClick={()=>toCart(wishlist)}>Add to Cart</button>
{/* // {()?this.state.AlreadyinCartMsg:null} */}
</div>
<div style={{ flex:'1' }}>
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png`} id="wishlistdeletebtn" onClick={()=>{Deletewishlist(wishlist.wl_id);this.DeleteDone()}}></img>
</div>
</div>
</div>
)}
<div style={{ marginTop:'15%' }}>
<MainFooter/>

</div>
</div>

);
}
}
const mapStateToProps = state => ({
  userwishlistBook:state.wishlistR.userwishlistBook,
  userwishlistInv:state.wishlistR.userwishlistInv,
  userToken:state.accountR.token,
  ReloadWishlist:state.wishlistR.ReloadWishlist,
  FromMycart:state.cartReduc.MyCart,

})
export default connect(mapStateToProps,{Uwishlist,SetWishlistReloader,AddToCart,CartopenModal,CartSession})(Wishlist);