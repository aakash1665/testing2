import React, { Component } from 'react'
import { connect } from 'react-redux';
import Qualityassurance from '../Home/qualityassurance'
import {getBook} from '../../actions/booksActions'
import {AddToCart,CartopenModal,CartSession,removeAllCart,ToBeAddedToCart} from '../../actions/cartAction'
import {userdetails} from '../../actions/accountAction'
import axios from 'axios'
import config from 'react-global-configuration'
import {setNewPricing} from '../../actions/productAction';
import MediaQuery from 'react-responsive';
import { Helmet } from "react-helmet";
import DocumentTitle from 'react-document-title'
// import DocumentMeta from "react-document-meta";

import './newProduct.css'
// import '../Home/footer.css';

// import Popup from 'reactjs-popup' 
// import { connect } from 'react-redux';
// import UserLogin from './UserLogin'
// import UserSignup from './UserSignup';
import Popup from 'reactjs-popup' 
import { Link,Redirect,browserHistory } from 'react-router-dom'
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';

import ReactGA from 'react-ga';

// import './product.css'
class Product extends Component {
  state={

    instock:false,
    productData:true,
    SelectCond:'',
    GotoCart:false,
    AlreadyinCartMsg:'',
    notifyopen:false,
    PincodeCheck:'',
    status:'',
    user_id:'',
    WishlistMsg:'',
    AddedToCart_Msg:'',
    sendEmail:"",

  }
  componentWillReceiveProps(nextProps){
    console.log(this.props.CartSessionData.length , nextProps.CartSessionData.length);
    
    if(this.props.CartSessionData.length !== nextProps.CartSessionData.length){
      console.log(nextProps.CartSessionData.length);
      if(localStorage.getItem('user') != null){
        console.log("Doing Refresh");
        
      nextProps.CartSessionData.map((book)=>{
        console.log(book,"Form Db")
        if(book != 0){
          const MyCart ={
          Cart_id:book.cart_id,
          bookId:book.book_id,
          bookName:book.title,
          bookSlug:book.slug,
          bookPrice:Math.round(book.price),
          bookShippingCost:Math.round(book.shipping_cost),
          bookThumb:book.thumb,
          // bookDonor:DonorName,
          bookQty:1,
          bookCond:book.condition,
          bookRackNo:book.rack_no,
          bookInvId:book.book_inv_id,
          // bookDonner
          }
          // Count+=1
          // console.log(MyCart);
        this.props.AddToCart(MyCart)          
          try {
        console.log(book,"Form Db To Local")
              
            sessionStorage.setItem(book.book_inv_id,JSON.stringify(MyCart));


          } catch (error) {
          }

          if(sessionStorage.length !==0){
            this.props.removeAllCart()
         for (var i=0; i<=sessionStorage.length-1; i++)  
         {   
          if(sessionStorage.key(i) !== "UserOrderId"){
    
             let key = sessionStorage.key(i);  
             try {
              let val = JSON.parse(sessionStorage.getItem(key));
              console.log(val.bookInvId  )
              // val.map(det=>console.log(det))
              
              this.props.AddToCart(val)
              // alert(val)
             } catch (error) {
              //  console.log(error);
               
             }
    
          }
         } }
        }
      }
        )
      }else{
        console.log("Not Login");
        
      }
  }
  }
  notifyopenModal=()=>{
    if(localStorage.getItem('user') != null){
      this.setState({sendEmail:this.props.getuserdetails.email})
    }
    this.setState({ notifyopen: true })

  }

  notifycloseModal=()=>{
    
    this.setState({ notifyopen: false })
   
  }
  slug=this.props.match.params.slug
  Notify=(e)=>{
    e.preventDefault();
    // alert(this.props.match.params.slug)
    const passdata = {
      
        "data" :
        {
        "email": this.state.sendEmail,
        "slug" : `${this.props.match.params.slug}`
      }
      
      }
      const token=`Token ${this.props.userToken}`
      // ${config.get('apiDomain')}
      axios.post(`http://localhost:8000/api/v1/post/notify_me`,passdata,
      ).then(res=>{
        // console.log(res);
        
        this.setState({
        data:res.data.message,
        })
        // console.log(res.data.message);
        setTimeout(this.notifycloseModal,3000)
        }).catch(err=>{
          if(err.response.status == 400){
            console.log("The notify error is ",err.response.data.message)
            this.setState({
              data: err.response.data.message,
              })
          }
          // console.log();
          
        // setTimeout(this.notifycloseModal,3000)

        });
  };
  addresscheck=(e)=>{
    // e.preventdefault();
    
  //   const {PincodeCheck}=this.state
  //   // alert(PincodeCheck)
  //   if(PincodeCheck.toString().length === 6){
  //     // alert("ok")
  //     axios.get(`${config.get('apiDomain')}/pincode/cod_check/${this.state.PincodeCheck}/`,
  //     )
  //     .then(res=>{this.setState({status:res.data.status});
  //               setTimeout(()=>{this.setState({status:""})},3000)

  //               })
  //     // .then(res=>console.log(res.data))
      
  //     .catch(err=>console.log(this.status)
  //     )
  //  }
   }
  //  AddBook=(book_id)=>{
  //   const passdata = {
  //     "book_id":book_id,
  //  }
  componentDidMount(){
    window.scrollTo(0, 0);
    this.setState({SelectCond:''})
    // alert(this.state.SelectCond)
    this.props.getBook(this.slug)
    this.setState({WishlistMsg:''})
  //   const {compExamBooks} = this.props
  console.log(this.slug);
  // this.listAllItems()
  };


  changeDataBottom=()=>{
    // alert("ch")
    this.setState({productData:!this.state.productData})
  }


//  listAllItems=()=>{  
//    alert("in see")
//    if(sessionStorage.length !==0){
//   for (var i=0; i<=sessionStorage.length-1; i++)  
//   {   
//       let key = sessionStorage.key(i);  
//       let val = JSON.parse(sessionStorage.getItem(key));
//       console.log(val)
//       // val.map(det=>console.log(det))
//       this.props.AddToCart(val)
//       // alert(val)
//   } }

// } 
  // SetShipping(bookShippingCost){
    
  //   this.setState({bookShippingCost:bookShippingCost})
  // }
  onChanged = e => {
                  this.setState({[e.target.name]:e.target.value})
                  const {PincodeCheck}=this.state
                  // alert(PincodeCheck)
                  console.log(e.target.value);
                  
                  if(e.target.value.length === 6){
                    // alert("ok")
                    axios.get(`${config.get('apiDomain')}/pincode/cod_check/${e.target.value}/`,
                    )
                    .then(res=>{
                      console.log(res);
                      // alert(res)
                      this.setState({status:res.data.status});
                              setTimeout(()=>{this.setState({status:""})},5000)
              
                              })
                    // .then(res=>console.log(res.data))
                    
                    .catch(err=>console.log(this.status)
                    )
                 }
                    }
  render() {
    let VeryGood =[]
    let AlmostNew=[]
    let AverageButInReadableCondition=[]
    let BrandNew=[]
    let SHIPPINGCOST=''
    let DonorName=''
    let TOTALQUANTITY=''
    let RACK_NO=''
    let BOOK_INV_ID=''

    // let =''

    // this.componentDidMount(){}
    const {book,bookCondition} = this.props
    console.log(bookCondition);
    // this.setState({bookName:book.title})
    // this.setState({bookSlug:book.slug})
    // this.setState({bookPrice:book.price})
    // this.setState({bookShippingCost:SHIPPINGCOST})
    // this.setState({bookCondition:this.state.SelectCond})
      const toCart=(e)=>{   

        e.preventDefault();
        var msgg="";
       this.props.FromMycart.map(cart=>(
        // alert("runn")
        // (`${cart.bookInvId}`===`${BOOK_INV_ID}`)?  msgg="already in cart": null
        (`${cart.bookInvId}`===`${BOOK_INV_ID}`)?  msgg="already in cart": null
        
      ) ) 
        if(msgg === "")
          {checkInCart()
            this.setState({AddedToCart_Msg:"Book Added To Cart"})
            setTimeout(()=>{ this.setState({AddedToCart_Msg:""})},2000)
          }else{
            this.setState({AlreadyinCartMsg:"Already In cart"})
          }
          
    } 
    if(this.state.AlreadyinCartMsg.length !== 0){
      setTimeout(()=>{this.setState({AlreadyinCartMsg:""})},3000)
    }

    const checkInCart=()=>{

      const MyCart ={
        bookId:book.book_id,
        bookName:book.title,
        bookSlug:this.state.bookSlug,
        bookPrice:Math.round(book.price),
        bookShippingCost:Math.round(SHIPPINGCOST),
        // bookShippingCost:Math.round(SHIPPING),
        bookThumb:book.thumb,
        bookDonor:DonorName,
        bookQty:TOTALQUANTITY,
        bookCond:this.state.SelectCond,
        bookRackNo:RACK_NO,
        bookInvId:BOOK_INV_ID
        // bookDonner
      }

      ReactGA.plugin.execute('ec', 'addProduct', {
          id: MyCart.bookId,
          name: MyCart.bookName,
          sku: MyCart.bookInvId,
          category: MyCart.bookSlug,
          price: MyCart.bookPrice,
          quantity: MyCart.bookQty,
      });

      const sendCartSession = { 
        "book_id" : book.book_id,
        "book_inv_id":BOOK_INV_ID[0],
        'content-type': 'application/json',
         }
        //  Add to Cart Session
         if(localStorage.getItem('user') != null){
         axios.post(`${config.get('apiDomain')}/common/addtocart/`,sendCartSession,{headers:{
                  'Authorization': `Token ${this.props.userToken}`
                }} )
         .then(res=>{
           console.log(res.status,sendCartSession);
          RefreshCart();
          }
         
         )
         .catch(err=>{console.log(err.response.status);console.log(sendCartSession);}
         )
        //  Refreshing Cart after response
          const RefreshCart=()=>{
            const token= localStorage.getItem('user')
            const details=`Token ${token}`
            this.props.CartSession(details)
            console.log("Refreshing");
          }
        }else{
          this.props.ToBeAddedToCart({"book_id" : book.book_id,"book_inv_id":BOOK_INV_ID[0]})
        }

        this.props.AddToCart(MyCart)

  // if(localStorage.getItem('user') !== null){
  //   const token= localStorage.getItem('user')
  //   const details=`Token ${token}`
  //   this.props.CartSession(details)}

    try {
      sessionStorage.setItem(BOOK_INV_ID,JSON.stringify(MyCart));
      
    } catch (error) {
      
    }

      // this.props.AddToCart(MyCart)
      // }   else{
      //   alert("already in cart")
      // }   
// this.setState({GotoCart:true})
      this.props.CartopenModal()
      // alert('get')
      // let data = sessionStorage.getItem(RACK_NO)

    }
    const GetPoductData =book.book_desc
    // console.log(GetPoductData);

    Object.keys(bookCondition).map((val)=>{

      // this.setState({SelectCond:val})
      const Vg =bookCondition[val];
      if(val === 'VeryGood'){
        VeryGood.push(Vg);
        // console.log(VeryGood[0].shipping)
        if(this.state.SelectCond === ''){
          this.setState({SelectCond:'VeryGood'})
        } 

      }
      if(val === 'AlmostNew'){
        AlmostNew.push(Vg)
        if(this.state.SelectCond === ''){
          this.setState({SelectCond:'AlmostNew'})
        } 
      }
      if(val === 'AverageButInReadableCondition'){
        AverageButInReadableCondition.push(Vg) 
        if(this.state.SelectCond === ''){
          this.setState({SelectCond:'AverageButInReadableCondition'}) 
          // MakeChangeHere 
        } 
        // this.setState({SelectCond:'AverageButInReadableCondition'})

      }
      // console.log(val.toString());
     var Check_Brand= val.search("and")
    //  console.log(Check_Brand);
     
      if(Check_Brand === 2){

      // if("Brand" in val ){

        // alert("Brand")
        BrandNew.push(Vg)
        if(this.state.SelectCond === ''){
          this.setState({SelectCond:'BrandNew'})
        } 
      } 
      console.log(VeryGood,AlmostNew,AverageButInReadableCondition,BrandNew);
      


      Object.keys(Vg).map(function(child){
        // console.log(`${child} => ${Vg[child]}`);
        
        const chil = Vg[child]
        // console.log(chil,child);

      })
      
    })
    // const allCond = VeryGood.map((cond)=>
    // <p>{cond.VeryGood}</p>
    // );Home
    const publication_date = book.publication_date
    // console.log(`${publication_date.toString()} oo`);
    if(publication_date)
    {
      // console.log(`${publication_date.toString().length} oo`);
      var PubliDateLength = publication_date.toString().length
    }
    // const PubliDateLength = publication_date
    var GetPoductReview =`REE`
    var bottomDatta = GetPoductData
    if(this.state.productData === true){
      var bottomDatta = GetPoductData
    }else{
      var bottomDatta = GetPoductReview
    }
    // console.log(this.props.book["title"])
    
    // console.log(book);
    const thumb=(book.thumb)
    // const src=`https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/${thumb}`
    // const src=`https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/gate-2010-electronic-communication-engineering`
    const src=`https://d1m4cm33ta71ff.cloudfront.net/uploads/books/medium/${thumb}`
    var RACKNO = ''
    if(this.state.SelectCond === 'VeryGood'){
      RACKNO=  VeryGood.map(child=><span>{child.rack_no}</span>)}
    else if(this.state.SelectCond === 'BrandNew'){
      RACKNO=  BrandNew.map(child=><span>{child.rack_no}</span>)}
    else if(this.state.SelectCond === 'AverageButInReadableCondition'){
      RACKNO=  AverageButInReadableCondition.map(child=><span>{child.rack_no}</span>)}
    else if(this.state.SelectCond === 'AlmostNew'){
      RACKNO=  AlmostNew.map(child=><span>{child.rack_no}</span>)}
    else{
     RACKNO = ''
    }

    var SHIPPING = ''
    
       
    if(this.state.SelectCond === 'VeryGood'){
      
      // SHIPPING=  VeryGood.map(child=><span id="productShippingCost">&#8377;{Math.round(child.shipping)}</span>)
      SHIPPING=  VeryGood.map(child=>{return Math.round(child.shipping)})
      BOOK_INV_ID=VeryGood.map(child=>{return child.book_inv_id})
      SHIPPINGCOST=VeryGood.map(child=>{return Math.round(child.shipping)})

      // console.log(SHIPPING);
      
      // ----------------------------NEW PRICEING CALCULATION VeryGood ---------------------- 
      // good very good
      if(isNaN(Math.round(SHIPPINGCOST))){
      // console.log(Math.round(SHIPPING),"shipping");
      // console.log(`MRP:${book.price},WT ${book.weight},Cond : VeryGood`,);
      var VGCalBookPrice = Number(book.price);
      var VGCalBookWt = Number(book.weight);
      var VGCalBookCond = "VeryGood";
      var VGCalbookInvId = BOOK_INV_ID[0]
      var VGNewPriceingModel = 0;

      var VGColAPrice = Number(VGCalBookWt * 197.5); 

      var VGColBPrice = Number((33 * VGCalBookPrice)/100)

      var VGColCprice = 0
      
      if(VGCalBookWt<0.95){
        console.log("in less than  0.95");
        VGColCprice = Number(VGCalBookWt*140)  
      }else if(VGCalBookWt >= 0.95){
        console.log("in Greater than 0.95");
        (VGCalBookWt*90>135)?VGColCprice=Number(VGCalBookWt*90):VGColCprice=135
      }

      var VGColDprice = 0
      if(VGColBPrice<VGColCprice){
        VGColDprice=VGColCprice}
      else{
        VGColDprice=VGColBPrice}

      var VGColEPrice = VGCalBookWt*300

      var VGColFPrice =  0
      if(VGColDprice > VGColEPrice){
        console.log("in Assign E to F");
        
        VGColFPrice=VGColEPrice
       VGNewPriceingModel= VGColFPrice
      }
      else{
        // console.log("in Assign D to F");

          VGColFPrice=VGColDprice

          if(VGColFPrice < ((20*VGCalBookPrice)/100)){
            // console.log("in Assign D to F less 20");
            VGColFPrice = (20*VGCalBookPrice)/100;
            
            VGNewPriceingModel=VGColFPrice
          }
          if((VGCalBookWt*90)<=VGColFPrice<=(VGCalBookWt*300) )
            {
              // console.log("in Assign D to F btw 90 300 ");
              VGNewPriceingModel=VGColFPrice
            }
          if(VGColFPrice<VGCalBookWt*90){
            // console.log("in Assign D to F less 90Wt ");
            VGColFPrice= VGCalBookWt*90
          }
      }

      var VGCalbookInvId = BOOK_INV_ID[0]
      // console.log(VGColAPrice,VGColBPrice,VGColCprice,VGColDprice,VGColEPrice,VGColFPrice,VGNewPriceingModel,`new Pricing Model -> ${VGNewPriceingModel}`,VGCalbookInvId,);

      SHIPPING=Math.round(VGNewPriceingModel)
      SHIPPINGCOST=Math.round(VGNewPriceingModel)
      
      const VGsendData={
        data:{
          "book_inv_id":VGCalbookInvId,
          "colA": VGColAPrice,
          "colB": VGColBPrice,
          "colC": VGColCprice,
          "colD": VGColDprice,
          "colE": VGColEPrice,
          "colF": VGColFPrice,
          "weight": VGCalBookWt,
          "mrp": VGCalBookPrice,
          "condition":"VeryGood",
          "new_price": VGNewPriceingModel
      
        }
      }
      this.props.setNewPricing(VGsendData);
      }
      // ------------------------END NEW PRICEING CALCULATION verGood------------------------------- 
      DonorName=VeryGood.map(child=>{return child.donor_name})
      TOTALQUANTITY=VeryGood.map(child=>{return child.total_qty})
      RACK_NO=VeryGood.map(child=>{return child.rack_no})
    }

    else if(this.state.SelectCond === 'BrandNew'){
      // 
      SHIPPING=  BrandNew.map(child=>{return Math.round(child.shipping)})
      // SHIPPING=  BrandNew.map(child=><span id="productShippingCost">&#8377;{Math.round(child.shipping)}</span>)
      BOOK_INV_ID=BrandNew.map(child=>{return child.book_inv_id})
      SHIPPINGCOST=BrandNew.map(child=>{return Math.round(child.shipping)})
      // ----------------------------NEW PRICEING CALCULATION BRANDNEW---------------------- 
      // BRANDNEW
      if(isNaN(Math.round(SHIPPINGCOST))){
        // console.log(Math.round(SHIPPING),"shipping");
        // console.log(`MRP:${book.price},WT ${book.weight},Cond : BRANDNEW`,);
        var BDCalBookPrice = Number(book.price);
        var BDCalBookWt = Number(book.weight);
        var BDCalBookCond = "BrandNew";
        var BDCalbookInvId = BOOK_INV_ID[0]
        var BDNewPriceingModel = 0;
  
        var BDColAPrice = Number(BDCalBookWt * 197.5); 
  
        var BDColBPrice = Number((43 * BDCalBookPrice)/100)
  
        var BDColCprice = 0
        
        if(BDCalBookWt<0.95){
          // console.log("in less than  0.95");
          BDColCprice = Number(BDCalBookWt*140)  
        }else if(BDCalBookWt >= 0.95){
          // console.log("in Greater than 0.95");
          (BDCalBookWt*90>135)?BDColCprice=Number(BDCalBookWt*90):BDColCprice=135
        }
  
        var BDColDprice = 0
        if(BDColBPrice<BDColCprice){
          BDColDprice=BDColCprice}
        else{
            BDColDprice=BDColBPrice}
  
        var BDColEPrice = BDCalBookWt*300
  
        var BDColFPrice =  0
        if(BDColDprice > BDColEPrice){
          // console.log("in Assign E to F");
          
          BDColFPrice=BDColEPrice
          BDNewPriceingModel= BDColFPrice
        }
        else{
          // console.log("in Assign D to F");
  
            BDColFPrice=BDColDprice
  
            if(BDColFPrice < ((20*BDCalBookPrice)/100)){
              // console.log("in Assign D to F less 20");
              BDColFPrice = (20*BDCalBookPrice)/100;
              
              BDNewPriceingModel=BDColFPrice
            }
            if((BDCalBookWt*90)<=BDColFPrice<=(BDCalBookWt*300) )
              {
                // console.log("in Assign D to F btw 90 300 ");
                BDNewPriceingModel=BDColFPrice
              }
            if(BDColFPrice<BDCalBookWt*90){
              // console.log("in Assign D to F less 90Wt ");
              BDColFPrice= BDCalBookWt*90
            }
        }



        var BDCalbookInvId = BOOK_INV_ID[0]
        // console.log(BDColAPrice,BDColBPrice,BDColCprice,BDColDprice,BDColEPrice,BDColFPrice,BDNewPriceingModel,`new Pricing Model -> ${BDNewPriceingModel}`,BDCalbookInvId,);
        
        SHIPPING=Math.round(BDNewPriceingModel)
        SHIPPINGCOST=Math.round(BDNewPriceingModel)

        const BDsendData={
          data:{
            "book_inv_id":BDCalbookInvId,
            "colA": BDColAPrice,
            "colB": BDColBPrice,
            "colC": BDColCprice,
            "colD": BDColDprice,
            "colE": BDColEPrice,
            "colF": BDColFPrice,
            "weight": BDCalBookWt,
            "mrp": BDCalBookPrice,
            "condition":"BRANDNEW",
            "new_price": BDNewPriceingModel
        
          }
        }
        this.props.setNewPricing(BDsendData)
        }
        // ------------------------END NEW PRICEING CALCULATION------------------------------- 

      // console.log(Math.round(SHIPPING),"shipping");
      DonorName=BrandNew.map(child=>{return child.donor_name})
      TOTALQUANTITY=BrandNew.map(child=>{return child.total_qty})
      RACK_NO=BrandNew.map(child=>{return child.rack_no})
    }

    else if(this.state.SelectCond === 'AverageButInReadableCondition'){
      // average readable
      SHIPPING=  AverageButInReadableCondition.map(child=>{return Math.round(child.shipping)})
      // SHIPPING=  AverageButInReadableCondition.map(child=><span id="productShippingCost">&#8377;{Math.round(child.shipping)}</span>)
      BOOK_INV_ID=AverageButInReadableCondition.map(child=>{return child.book_inv_id})
      SHIPPINGCOST=AverageButInReadableCondition.map(child=>{return Math.round(child.shipping)})

      // ----------------------------NEW PRICEING CALCULATION average readable---------------------- 
      // average readable
      if(isNaN(Math.round(SHIPPINGCOST))){
        // console.log(Math.round(SHIPPING),"shipping");
        // console.log(`MRP:${book.price},WT ${book.weight},Cond : average readable`,);
        var ARCalBookPrice = Number(book.price);
        var ARCalBookWt = Number(book.weight);
        var ARCalBookCond = "AverageButInReadableCondition";
        var ARCalbookInvId = BOOK_INV_ID[0]
        var ARNewPriceingModel = 0;
  
        var ARColAPrice = Number(ARCalBookWt * 197.5); 
  
        var ARColBPrice = Number((27 * ARCalBookPrice)/100)
  
        var ARColCprice = 0
        
        if(ARCalBookWt<0.95){
          // console.log("in less than  0.95");
          ARColCprice = Number(ARCalBookWt*140)  
        }else if(ARCalBookWt >= 0.95){
          // console.log("in Greater than 0.95");
          (ARCalBookWt*90>135)?ARColCprice=Number(ARCalBookWt*90):ARColCprice=135
        }
  
        var ARColDprice = 0
        if(ARColBPrice<ARColCprice){
          ARColDprice=ARColCprice}
        else{
            ARColDprice=ARColBPrice}
  
        var ARColEPrice = ARCalBookWt*300
  
        var ARColFPrice =  0
        if(ARColDprice > ARColEPrice){
          // console.log("in Assign E to F");
          
          ARColFPrice=ARColEPrice
          ARNewPriceingModel= ARColFPrice
        }
        else{
          // console.log("in Assign D to F");
  
            ARColFPrice=ARColDprice
  
            if(ARColFPrice < ((20*ARCalBookPrice)/100)){
              // console.log("in Assign D to F less 20");
              ARColFPrice = (20*ARCalBookPrice)/100;
              
              ARNewPriceingModel=ARColFPrice
            }
            if((ARCalBookWt*90)<=ARColFPrice<=(ARCalBookWt*300) )
              {
                // console.log("in Assign D to F btw 90 300 ");
                ARNewPriceingModel=ARColFPrice
              }
            if(ARColFPrice<ARCalBookWt*90){
              // console.log("in Assign D to F less 90Wt ");
              ARColFPrice= ARCalBookWt*90
            }
        }
  
        var ARCalbookInvId = BOOK_INV_ID[0]
        // console.log(ARColAPrice,ARColBPrice,ARColCprice,ARColDprice,ARColFPrice,ARNewPriceingModel,`new Pricing Model -> ${ARNewPriceingModel}`,ARCalbookInvId,);
        
        SHIPPING=Math.round(ARNewPriceingModel)
        SHIPPINGCOST=Math.round(ARNewPriceingModel)

        const ARsendData={
          data:{
            "book_inv_id":ARCalbookInvId,
            "colA": ARColAPrice,
            "colB": ARColBPrice,
            "colC": ARColCprice,
            "colD": ARColDprice,
            "colE": ARColEPrice,
            "colF": ARColFPrice,
            "weight": ARCalBookWt,
            "mrp": ARCalBookPrice,
            "condition":"AverageButInReadableCondition",
            "new_price": ARNewPriceingModel
        
          }
        }
        this.props.setNewPricing(ARsendData)
        }
        // ------------------------END NEW PRICEING CALCULATION readable------------------------------- 
      // console.log(Math.round(SHIPPING),"shipping");
      DonorName=AverageButInReadableCondition.map(child=>{return child.donor_name})
      TOTALQUANTITY=AverageButInReadableCondition.map(child=>{return child.total_qty})
      RACK_NO=AverageButInReadableCondition.map(child=>{return child.rack_no})
    }
    else if(this.state.SelectCond === 'AlmostNew'){
      // as good as new 
      SHIPPING=  AlmostNew.map(child=>{return Math.round(child.shipping)})
      // SHIPPING=  AlmostNew.map(child=><span id="productShippingCost">&#8377;{Math.round(child.shipping)}</span>)
      BOOK_INV_ID=AlmostNew.map(child=>{return child.book_inv_id})
      SHIPPINGCOST=AlmostNew.map(child=>{return Math.round(child.shipping)})

      // ----------------------------NEW PRICEING CALCULATION as good as new ---------------------- 
      // as good as new 
      if(isNaN(Math.round(SHIPPINGCOST))){
        // console.log(Math.round(SHIPPING),"shipping");
        // console.log(`MRP:${book.price},WT ${book.weight},Cond : AlmostNew`,);
        var ANCalBookPrice = Number(book.price);
        var ANCalBookWt = Number(book.weight);
        var ANCalBookCond = "AlmostNew";
        var ANCalbookInvId = BOOK_INV_ID[0]
        var ANNewPriceingModel = 0;
  
        var ANColAPrice = Number(ANCalBookWt * 197.5); 
  
        var ANColBPrice = Number((37 * ANCalBookPrice)/100)
  
        var ANColCprice = 0
        
        if(ANCalBookWt<0.95){
          // console.log("in less than  0.95");
          ANColCprice = Number(ANCalBookWt*140)  
        }else if(ANCalBookWt >= 0.95){
          // console.log("in Greater than 0.95");
          (ANCalBookWt*90>135)?ANColCprice=Number(ANCalBookWt*90):ANColCprice=135
        }
  
        var ANColDprice = 0
        if(ANColBPrice<ANColCprice){
          ANColDprice=ANColCprice}
        else{
            ANColDprice=ANColBPrice}
  
        var ANColEPrice = ANCalBookWt*300
  
        var ANColFPrice =  0
        if(ANColDprice > ANColEPrice){
          // console.log("in Assign E to F");
          
          ANColFPrice=ANColEPrice
          ANNewPriceingModel= ANColFPrice
        }
        else{
          // console.log("in Assign D to F");
  
            ANColFPrice=ANColDprice
  
            if(ANColFPrice < ((20*ANCalBookPrice)/100)){
              // console.log("in Assign D to F less 20");
              ANColFPrice = (20*ANCalBookPrice)/100;
              
              ANNewPriceingModel=ANColFPrice
            }
            if((ANCalBookWt*90)<=ANColFPrice<=(ANCalBookWt*300) )
              {
                // console.log("in Assign D to F btw 90 300 ");
                ANNewPriceingModel=ANColFPrice
              }
            if(ANColFPrice<ANCalBookWt*90){
              // console.log("in Assign D to F less 90Wt ");
              ANColFPrice= ANCalBookWt*90
            }
        }
  
        var ANCalbookInvId = BOOK_INV_ID[0]
        console.log(ANColAPrice,ANColBPrice,ANColCprice,ANColDprice,ANColEPrice,ANColFPrice,ANNewPriceingModel,`new Pricing Model -> ${ANNewPriceingModel}`,ANCalbookInvId,);
        
        SHIPPING=Math.round(ANNewPriceingModel)
        SHIPPINGCOST=Math.round(ARNewPriceingModel)

        const ANsendData={
          data:{
            "book_inv_id":ANCalbookInvId,
            "colA": ANColAPrice,
            "colB": ANColBPrice,
            "colC": ANColCprice,
            "colD": ANColDprice,
            "colE": ANColEPrice,
            "colF": ANColFPrice,
            "weight": ANCalBookWt,
            "mrp": ANCalBookPrice,
            "condition":"AlmostNew",
            "new_price": ANNewPriceingModel
        
          }
        }
        this.props.setNewPricing(ANsendData)
        }
        // ------------------------END NEW PRICEING CALCULATION------------------------------- 
      // console.log(Math.round(SHIPPING),"shipping");
      // console.log(SHIPPINGCOST);
      DonorName=AlmostNew.map(child=>{return child.donor_name})
      TOTALQUANTITY=AlmostNew.map(child=>{return child.total_qty})
      RACK_NO=AlmostNew.map(child=>{return child.rack_no})
    }
    else{
    //  SHIPPING = ''
    }
    if (this.state.GotoCart !== false){
      // return <Redirect to='/view-cart'/>;
    }
    // while(this.state.SelectCond === 'VeryGood'){
    //   this.setState({bookShippingCost:VeryGood.shipping});
    //   break
    // }
    // if(this.state.SelectCond === "VeryGood"){
    //   console.log(VeryGood[0].shipping);
      
    // }
    // // function 
    // if(SHIPPING === "NAN"){
     
      
    // }
   const  Addwishlist=(id,book_id)=>{
      // alert(id)
      const passdata = {
        "user_id":id,
        "book_id":book_id,
        "book_inv_id":BOOK_INV_ID[0]
  }
      const token=`Token ${this.props.userToken}`
      // e.preventDefault();
      // console.log(passdata)
      axios.post(`${config.get('apiDomain')}/api/v1/wishlist/add-wishlist`,passdata,{headers:{
        'Authorization': token,
      }}  
      ).then(res=>{
        // console.log(`${res.data.message}`);
        this.setState({WishlistMsg:res.data.message})
        }).catch(err=>console.log(err,passdata));
  };

  let meta_title, meta_desc, meta_url = window.location.href;
  if(book.title){
    meta_desc = this.props.book.title + ". From mypustak.com. Only Genuine Products, Free of Cost. In Good Condition in " + this.props.book.language + " Language. Published by " + this.props.book.publication + ". Written by " + this.props.book.author
    meta_title = this.props.book.title + " Buy " + this.props.book.title + " free of cost only at shipping and handling charges | Mypustak.com"
  }



  // let meta_desc;
  // if(book.title){
  //   meta_desc = book.title.slice(0,60);
  // }

  // let meta = {
  //   title: book.title,
  //   meta: {
  //     charset: "utf-8",
  //     name: {
  //       title: book.title,
  //       description:  meta_desc
  //     }
  //   }
  // };








    return (
    <div> 
      <MainHeader />  
      {/* <DocumentTitle title="Meta"></DocumentTitle> */}
      <Helmet>
          <title>{meta_title}</title>
          <meta name="og_title" property="og:title" content={meta_title} />
          <meta name="Description" property="og:description" content={meta_desc} />
          <meta name="og_url" property="og:url" content={meta_url} />
        </Helmet>
        {/* <DocumentMeta {...meta} /> */}
      <div id="productBody">
        <div id="productLeft">
              <div id="productImg">
                <img src={src} />
              </div>

              <div id="productDetails">
                <p id="productTitle"> {book.title}</p>
                <hr/>
                {book.author && book.author!='N/A' ? <p id="productAuthor">Author : <span id="productAuthorName">{book.author}</span></p>: null}
                {book.publication ? <p id="productPublic">Publication :  
                <span id="productPublicName">{book.publication}<span id={(PubliDateLength !== 4?"NopublicationDate":"")} >({book.publication_date})</span></span>
                 <span id={(book.no_of_pages !== null?"productPages":"NoproductPages")}> &nbsp;
                {book.no_of_pages} pages</span></p> : null }
                <hr/>
                <div id="productformat">
                {book.language && book.binding ? <p id="ProductisbnFormat">
                  {book.language ? <span id="language">Language:{book.language}</span> : null}
                  {book.binding ? <span id="format"> &nbsp;Format:{book.binding}</span> : <span id="format"></span> }

                  </p> : null}
                  
                  <p id="ProducteditionLanguage">
                  {book.weight ? <span id="weight">Weight:{book.weight} </span> : null }
                  <span id={(bookCondition.is_soldout === 'Y')?"NOrack_no":"rack_no"}>Rack No:
                  {RACKNO}
                </span> </p>
                <p id="ProducteditionLanguage">
                {book.isbn ? <span id={(book.isbn !== ""?"":"Noisbn") }>ISBN:{book.isbn} </span> : null} 
                  <span id={( book.edition !== '' ?( book.edition !== null ?"edition":"Noedition"):"Noedition")} >Edition :{book.edition}</span> </p>
              </div>
              </div>

            <form id="PincodeChecker" onSubmit={this.addresscheck()}>
              <input type="text" name="PincodeCheck" 
              placeholder="Enter Pincode to Check Cash On Delivery Service"
              id="InputPincodeChecker"
              onChange={this.onChanged}
              maxLength='6'
              />
              {/* <button type="submit" id="checkButton">Check</button></form> */}
              <input type="button"  id="checkButton" style={{width:"20%",marginLeft:'5%'}} onClick={()=>this.addresscheck()} value="Check"/>
                <p style={{ textAlign:'center',color:'red' ,width:'60%'}}>{(this.state.status.length !==0)?`cod is ${this.state.status}`:null}</p>
              </form>
              {/* <label style={{ position }}:'fixed',marginTop:'6%',marginLeft:'15%',color:'red'}}>{this.state.status}</label> */}
          <div id={GetPoductData === null?"NoproductleftBottom":(GetPoductData !== ''?"productleftBottom":"NoproductleftBottom")}>
          {/* <p><span id="Productdescription" onClick={this.changeDataBottom}>Description</span><span id="Productreviews" onClick={this.changeDataBottom}>Reviews</span></p> */}
          <p><span id="Productdescription">Description</span></p>
          <div id="productleftBottomData">
          {bottomDatta}
          </div>
          </div>
        </div>
              {/*---------------------------------- Desktop--------------------------------------- */}
        <MediaQuery minWidth={992} >
        <Popup
          open={this.state.notifyopen}
          closeOnDocumentClick
          onClose={()=>this.setState({notifyopen:false})}
          contentStyle={{ 
                        width:'28.9%',
                        borderRadius:'5px',
                        marginLeft:'69%',
                        marginTop:'20%',
                        position:'absolute'
                        }} 
          overlayStyle={{
              background:'transparent'
          }}
        > 
        <div >
            <a className="close" onClick={this.notifycloseModal} style={{
    color: 'black',
    marginLeft: '-1%',float:'left'}}>
              &times;
            </a>
            {/* <label>{this.state.data}</label> */}
            <form onSubmit={this.Notify}>
              <input type="email"  placeholder="Enter Your Registered Email" onChange={this.onChanged} name="sendEmail" value={this.state.sendEmail}/>
              <div style={{ textAlign:"center" }}>
              <input type="submit" value="submit" id="NotifySubmitBtn"/>
              </div>
            </form>
            <p style={{ textAlign:'center' }}>{this.state.data}</p>
            
        </div>
        </Popup>
        </MediaQuery>
         {/* ------------------------------------tab---------------------------------------*/}
 <MediaQuery maxWidth={991} and minWidth={768}>
        <Popup
          open={this.state.notifyopen}
          closeOnDocumentClick
          onClose={()=>this.setState({notifyopen:false})}
          contentStyle={{ 
                        width:'28.9%',
                        borderRadius:'5px',
                        marginLeft:'69%',
                        marginTop:'37%',
                        position:'absolute'
                        }} 
          overlayStyle={{
              background:'transparent'
          }}
        > 
        <div >
            <a className="close" onClick={this.notifycloseModal} style={{
    color: 'black',
    marginLeft: '-1%',float:'left'}}>
              &times;
            </a>
            {/* <label>{this.state.data}</label> */}
            <form onSubmit={this.Notify}>
              <input type="email"  placeholder="Enter Your Registered Email" onChange={this.onChanged} name="sendEmail" value={this.state.sendEmail}/>
              <div style={{ textAlign:"center" }}>
              <input type="submit" value="submit" id="NotifySubmitBtn"/>
              </div>
            </form>
            <p style={{ textAlign:'center' }}>{this.state.data}</p>
            
        </div>
        </Popup>
        </MediaQuery>
        {/*------------------------larger mobile----------------------------- */}
        <MediaQuery maxWidth={767} and minWidth={539}>
        <Popup
          open={this.state.notifyopen}
          closeOnDocumentClick
          onClose={()=>this.setState({notifyopen:false})}
          contentStyle={{ 
                        width:'35%',
                        borderRadius:'5px',
                        marginTop:'37%',
                        }} 
          overlayStyle={{
              background:'transparent'
          }}
        > 
        <div >
            <a className="close" onClick={this.notifycloseModal} style={{
    color: 'black',
    marginLeft: '-1%',float:'left'}}>
              &times;
            </a>
            {/* <label>{this.state.data}</label> */}
            <form onSubmit={this.Notify}>
              <input type="email"  placeholder="Enter Your Registered Email" onChange={this.onChanged} name="sendEmail" value={this.state.sendEmail}/>
              <div style={{ textAlign:"center" }}>
              <input type="submit" value="submit" id="NotifySubmitBtn"/>
              </div>
            </form>
            <p style={{ textAlign:'center' }}>{this.state.data}</p>
            
        </div>
        </Popup>
        </MediaQuery>
        {/* smaller mobiles */}
      <MediaQuery maxWidth={538} >
      <Popup
          open={this.state.notifyopen}
          closeOnDocumentClick
          onClose={()=>this.setState({notifyopen:false})}
          contentStyle={{ 
                        width:'60%',
                        borderRadius:'5px',
                        marginTop:'37%',
                        }} 
          overlayStyle={{
              background:'transparent'
          }}
        > 
        <div >
            <a className="close" onClick={this.notifycloseModal} style={{
    color: 'black',
    marginLeft: '-1%',float:'left'}}>
              &times;
            </a>
            {/* <label>{this.state.data}</label> */}
            <form onSubmit={this.Notify}>
              <input type="email"  placeholder="Enter Your Registered Email" onChange={this.onChanged} name="sendEmail" value={this.state.sendEmail}/>
              <div style={{ textAlign:"center" }}>
              <input type="submit" value="submit" id="NotifySubmitBtn"/>
              </div>
            </form>
            <p style={{ textAlign:'center' }}>{this.state.data}</p>
            
        </div>
        </Popup>
      </MediaQuery>
          <div id="productRight">
          <div id="productRightFirstPart">
            <p id="productRightBook">Book</p>
            <p id="productRightWishlist" onClick={()=>{Addwishlist(this.props.getuserdetails.id,book.book_id)}}> Save for Later </p>
            {(bookCondition.is_soldout === 'Y')?
            
              <div id="productRightdetailsSoldOut">
              {<span style={{ color:'red',marginLeft:'20%' }}>{this.state.WishlistMsg}</span>}
              <p id="SoldOut"> OUT OF STOCK 
              <button id="NotifyMeBtn" onClick={()=>{this.notifyopenModal()}}>NOTIFY ME</button>
              </p>
              <p id="SOproductMRP">MRP : &#8377;<span id="productPrice">{Math.round(book.price)}</span> 
              <span id="productFREE">(FREE)</span></p>
              </div>
              :<div id="productRightdetails">
              <form onSubmit={toCart}>
              {<span style={{ color:'red',marginLeft:'20%' }}>{this.state.WishlistMsg}</span>}
              <p id="productMRP">MRP : &#8377;<span id="productPrice">{Math.round(book.price)}</span> 
              <span id="productFREE">(FREE)</span></p>
              <div id="productShipping"><span>Pay Only<br/> Shipping & Handling</span>
              {/* {VeryGood.map(child=><span id="productShippingCost">&#8377;{Math.round(child.shipping)}</span>)} */}
              <span id="productShippingCost">&#8377;{SHIPPING}</span>
              </div>
              <hr style={{ marginTop:'0px',marginBottom:'0px' }}/>
              <div id="productRightCond">
                  <p>Select Book Condition</p>
                  <div id="productRightCondSelect">

                  {(BrandNew.length !== 0)?
                    <div><input type="radio" name="SelectCond" value="BrandNew" checked={this.state.SelectCond ==="BrandNew"}
                  onChange={this.onChanged} required/><span id={(this.state.SelectCond ==="BrandNew")?"slectedColor":"null"}>Brand New
                  <span id="totalQty">({TOTALQUANTITY} Books in stock)</span></span><br/></div>:null}

                  {(AlmostNew.length !== 0)?
                    <div><input type="radio" name="SelectCond" value="AlmostNew" checked={this.state.SelectCond ==="AlmostNew"}
                    onChange={this.onChanged} required/><span id={(this.state.SelectCond ==="AlmostNew")?"slectedColor":"null"}>As Good As New
                    <span id="totalQty">({TOTALQUANTITY} Books in stock)</span></span><br/></div>:null}

                  {(VeryGood.length !== 0)?
                    <div><input type="radio" name="SelectCond" value="VeryGood" checked={this.state.SelectCond ==="VeryGood"}
                    onChange={this.onChanged} required/><span id={(this.state.SelectCond ==="VeryGood")?"slectedColor":"null"}>Very Good
                    <span id="totalQty">({TOTALQUANTITY} Books in stock)</span></span><br/></div>:null}


                    {(AverageButInReadableCondition.length !== 0)?
                    <div><input type="radio" name="SelectCond" value="AverageButInReadableCondition" checked={this.state.SelectCond ==="AverageButInReadableCondition"}
                    onChange={this.onChanged} required/><span id={(this.state.SelectCond ==="AverageButInReadableCondition")?"slectedColor":"null"}>Good And Readable
                    <span id="totalQty">({TOTALQUANTITY} Books in stock)</span></span><br/></div>:null}
                  </div>
              </div>
              <hr/>
              <div id="productRightDelivered">
                <p>Delivered in 4-7 days <span style={{ color:'red' }}>{this.state.AlreadyinCartMsg}</span></p>
              {/* <button id="AddToCartBtn" onClick={toCart}>Add to Cart</button> */}
              <input id="AddToCartBtn"  type="submit" value="Add to Cart"  />
                <span style={{ color:'red' }}>{this.state.AddedToCart_Msg}</span>
              </div>
              </form>
              </div>}
          </div>
            </div>
      </div> 
      
     <Qualityassurance/>
      <MainFooter/>
    </div>
    
    )
  }
}
const mapStateToProps = state => ({
  book:state.compExam.book,
  bookCondition:state.compExam.bookCondition,
  FromMycart:state.cartReduc.MyCart,
  userToken:state.accountR.token,
  getuserdetails:state.userdetailsR.getuserdetails,
  CartSessionData:state.cartReduc.CartSessionData,
})

export default connect(mapStateToProps,{getBook,AddToCart,CartopenModal,ToBeAddedToCart,userdetails,setNewPricing,CartSession,removeAllCart})(Product);


