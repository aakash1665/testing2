import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import './Proceedpay.css'
import {SetWallet,SendWalletRechargeMail} from '../actions/walletAction'
import {OrderDetails} from '../actions/cartAction';
import {Get_Rp_Id} from '../actions/donationActions'
import { connect } from 'react-redux';
import axios from 'axios';
import { Redirect} from 'react-router-dom'
import config from 'react-global-configuration'
// import {AddPriceCart,RemoveCart,OrderDetails,SetOrderId,walletRazorpay,RedirectWalletToCart} from '../../actions/cartAction'
import {RedirectWalletToCart} from '../actions/cartAction';
class WalletProceedToPay extends Component {

state={
   count:1,
   SuccessWalletAdded:false,
   redirectBackCart:false,
}
	// { "data" :
	// 	{
	// 	"amount"  : 100,
	// 	"no_of_book" : 2,
	// 	"payusing" : "test_2502",
	// 	"billing_add_id" : 15753,
	// 	"shipping_add_id" : 15753
			
	// 	}
  // } 

  closePopup=()=>{
    this.props.closePopup()
  }
 getRZPID=(data)=>{
console.log(`${data} ttt`)
}
// ***********************RAZORPAY PART********************************
options = {
  "key": "rzp_live_pvrJGzjDkVei3G", //Paste your API key here before clicking on the Pay Button. 
  // "key": "rzp_test_jxY6Dww4U2KiSA",
  "amount" : this.props.RechargeAmt,
  "name": `Mypustak.com`,
  "description": `Recharge`,
  "Order Id": `${this.props.RAZORPAY}`, //Razorpay Order id
  "currency" : "INR",


  "handler": (response)=>{    
      var razorpay_payment_id=response.razorpay_payment_id;
      const razorpay_order_id=response.razorpay_order_id;
      const razorpay_signature=response.razorpay_signature;
      // console.log(response);
      var today = new Date();
      var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var dateTime = date+' '+time;
      //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
      const SendData={
        "user_id":`${this.props.UserId}`,
        "user_email":`${this.props.UserEmail}`,
        "transaction_id":razorpay_payment_id,
        "deposit":this.props.RechargeAmt/100,
        "withdrawl":"0",
        "payvia":"razorpay",
        "time": `${dateTime}`,
        "comment":"Added To wallet",
        "added_by":`${this.props.UserEmail}`
      }
      axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendData,{
        headers:{'Authorization':`Token ${this.props.userToken}`,
                  'Content-Type':'application/json'}
      })
      .then(res=>{
        // console.log(res,"Done "); 
        const details=`Token ${localStorage.getItem('user')}`
        this.props.SetWallet(details)   
        const SendBody={
          "user_email":`${this.props.UserEmail}`,
          "transaction_id":razorpay_payment_id,
          "amount":this.props.RechargeAmt/100,
        }  
        const token = `Token ${localStorage.getItem('user')}`;
        this.props.SendWalletRechargeMail(token,SendBody);   
        this.setState({SuccessWalletAdded:true})
        if(this.props.redirectWallettoCart){
          console.log("In Redirect Back");

            this.setState({redirectBackCart:true})
            
        }
      })
        .catch(err=>console.log(err,SendData))
          

    //   axios.post(`${config.get('apiDomain')}/api/v1/post/order_details`,data,{headers:{
    //     'Authorization': this.,
    //     'Content-Type':'application/json',
    //   }} )
    // .then(res=>{       
    //     dispatch({
    //         type:ORDERDETAILS,
    //         payload:res.data
    //     })
    // })
    // .catch(err=>console.log(err,data))
      
      

        // alert(response.razorpay_order_id)
        // alert(response.razorpay_signature)
                                },

  "prefill": {
      // "method":"card,netbanking,wallet,emi,upi",
      "contact" : `${this.props.Selectedphone_no}`,
      "email" : `${this.props.UserEmail}`
      // "email":"ph15041996@gmail.com"
  },
         //             "notes": {
    //                     "order_id": "your_order_id",
    //                                 "transaction_id": "your transaction_id",
    //                                 "Receipt": “your_receipt_id"
    //             },
  "notes": {
      "Order Id": this.props.WalletOrderId, //"order_id": "your_order_id",
      "address" : "customer address to be entered here"
  },
  "theme": {
      "color": "#1c92d2",
       "emi_mode" : true
              
  },
  
  external: {
 wallets: ['mobikwik' , 'jiomoney' ],
  handler: function(data) {
  // console.log(this, data)
  }
}
};


componentDidMount(){
  this.setState({SuccessWalletAdded:false})
  // this.props.GetOrderId(data)
  this.rzp1 = new window.Razorpay(this.options);
  this.setState({redirectBackCart:false})

  }
  closeProcedToPay(){
  this.props.closeProcedToPay()
}
  render() {
    
const {SelectedAddress} =this.props
console.log(this.razorpay_order_id);
if(this.state.redirectBackCart){
  console.log("Doing In Redirect Back");

  this.props.RedirectWalletToCart()
 return <Redirect to="view-cart"/>

}
    return (
      
        <div id="WalletProPay">
        {(this.state.SuccessWalletAdded === false)?
          <button style={{   backgroundColor: 'rgb(0,186,242)',color:' white',
          border:' none', padding: '3%', borderRadius: '3px',fontWeight: 'bold'}} onClick={()=>this.rzp1.open()} >Add to Wallet Rs. {this.props.RechargeAmt/100}</button>
          :
          <p style={{ color:'#38ACEC',textAlign:'center' }}>Amount Added to Wallet Successfull</p>
        }
        </div>

        
    
    )
  }
}

const mapStateToProps = state => ({
  // getuserdetails:state.userdetailsR.getuserdetails,
  // getadd:state.accountR.getadd,
  userToken:state.accountR.token,
  // CartPrice:state.cartReduc.CartPrice,
//   CartLength:state.cartReduc.cartLength,
  // AddresId:state.cartReduc.AddresId,
  // OrderId:state.cartReduc.OrderId,
  UserId:state.userdetailsR.getuserdetails.id,
  TotalPrice:state.cartReduc.TotalPrice,
  UserEmail:state.userdetailsR.getuserdetails.email,
  RechargeAmt:state.walletR.RechargeAmout,
  WalletOrderId:state.walletR.WalletOrderId,
  // Selectedphone_no:state.accountR.selectedAddress.phone_no,
  Selectedphone_no:state.accountR.getuserdetails.phone,
  redirectWallettoCart:state.cartReduc.redirectWallettoCartState
})

export default connect(mapStateToProps,{OrderDetails,Get_Rp_Id,RedirectWalletToCart,SetWallet,SendWalletRechargeMail})(WalletProceedToPay)