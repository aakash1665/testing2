import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import FilterListIcon from "@material-ui/icons/FilterList";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import CircularProgress from '@material-ui/core/CircularProgress';
import { lighten } from "@material-ui/core/styles/colorManipulator";
import Divider from "@material-ui/core/Divider";
import moment from "moment";
import { connect } from "react-redux";

import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Search from "@material-ui/icons/Search";
import Edit from "@material-ui/icons/Edit";
import Delete from '@material-ui/icons/Delete';
import Clear from "@material-ui/icons/Clear";

import { getBookInventoryList, getBookInventoryByBookId, changeCurrentInventory, updateBookInventory, deleteBookInventory, fetchBookByBookId, updateBook, deleteBook, getBooksSearchResult } from "../../actions/backendBookInventoryActions";

import BookInventoryHead from "./BookInventoryHead";
import BookInventoryDiv from './BookInventoryDiv';
import Menu from '../DropDownMenu';
import axios from 'axios';
import RackDataFilter from './rackdatafilter'
import DateBookFilter from './DateBookFilter'

import { desc, stableSort, getSorting } from '../../util/helpers';

// statuses for order
const PUBLISH_STATUSES = [
  { id: 0, name: "Published", status: "Y" },  
  { id: 1, name: "UnPublished", status: "N" },
];

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020
  },
  tablecell: {
    fontSize: '11pt',
    padding: '4px'
  },
  tableWrapper: {
    overflowX: "auto"
  },
  button: {
    whiteSpace: "nowrap",
    marginTop: '4px',
    marginLeft: "6px"
  },
  margin: {
    margin: "0 4px"
  }
});

class BookInventory extends React.Component {
  state = {
    order: "desc",
    orderBy: "bookinventory_req_id",
    selected: [],
    bookinventoryList: [],
    page: 0,
    rowsPerPage: 10,
    
    token: '',
    openDialogBook: false,
    openDialogInventory: false,
    openDialogAddBookInventory: false,
    searchValue:"",
    clearValue:1,
    openbookstockDialog:false,
    bookstocklist:[],
    img_path:
      "https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/",
      book_inv_id:"",
      book_id:"",
      book_order_id:"",
      openConfirmationOutstockDialog:false,
      openDeleteIsbn:false,
  };

  componentDidMount() {
    this.setState({ token: localStorage.getItem('user') });
    if(!this.props.bookinventoryList.length) {
      this.props.getBookInventoryList({ page: this.state.page, token: localStorage.getItem('user') });
    }else {
      this.setState({
        bookinventoryList: this.props.bookinventoryList
      })
    }
  }

  getBookInventoryByBookId= () => {
    this.props.getBookInventoryByBookId({ book_id: this.state.book_id, token: this.state.token });
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.bookinventoryList.length !== nextProps.bookinventoryList.length) {
      let { bookinventoryList } = nextProps;
      this.setState(
        {
          bookinventoryList: bookinventoryList
        }
      );
    }
  }

    // create data is a function which will create data for table row,
  // in this file we use this, you can check below.
  // In this we are creating data as per table header
  createData = (book) => {
    const {
      book_id,
      thumb,
      title,
      isbn,
      slug,
      qty,
      published,
      i_date
    } = book;
    let statusObj = PUBLISH_STATUSES.find(statusobj => statusobj.status == published);
    return {
      id: book_id,
      book_id,
    thumb: thumb,
    title,
    isbn,
    slug,
    qty,
    published,
      action: <div style={{display: 'flex', alignItems: 'center'}} > <Menu icon={<Edit fontSize="small" />}  options={PUBLISH_STATUSES} status={statusObj} orderId={book} handleSelect={this.handleUpdateBookStatus} /> <Delete fontSize="small" onClick={() => this.confirmForDelete(book_id)} /> </div>
    };
    // <Delete fontSize="small" onClick={() => this.deleteBook(book_id)} />
  }

  // it will handle sorting request
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  // it is for handle select all click, right now we're not using but if we need in future so it's there.
  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.bookinventoryList.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  // handle click on Checkbox and select accordingly
  // its for signlular selection - you can only select one row at a time
  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);

    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = [id];
    } else if (selectedIndex === 0) {
      newSelected = [];
    }
    this.setState({ selected: newSelected });
  };

  // handle request for page change
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  // handle pagination using type
  handlePaging = type => {
    if (type == "right") {
      if(this.props.bookinventoryList.length / this.state.rowsPerPage <= this.state.page + 1 ) {
        this.props.getBookInventoryList({ page: this.state.page + 1, token: this.state.token });
      }
      this.setState({ page: this.state.page + 1 });
    } else {
      this.setState({ page: this.state.page - 1 });
    }
  };

  handleupdateBookInventoryAclick() {
    this.setState({
      openDialogAddBookInventory: true
    })
  }

  updateBook = (e) => {
    e.preventDefault();
    let book = {
      book_id: this.props.currentBook.book_id,
      title:e.target.title.value,
      isbn: e.target.isbn.value,
      author: e.target.author.value,
      publication: e.target.publication.value,
      category: e.target.category.value,
      publication_date:e.target.publication_date.value,
      price:e.target.price.value,
      no_of_pages:e.target.no_of_pages.value,
      weight: e.target.weight.value,
      binding: e.target.binding.value,
      edition: e.target.edition.value,
      qty: e.target.qty.value,
      language: e.target.language.value,
      book_desc: e.target.book_desc.value,
      book_desc_url: e.target.book_desc_url.value,
      uploaded_by: e.target.uploaded_by.value,
    }
    this.props.updateBook({ book, token: this.state.token });
    this.handleClose();
  }

  handleUpdateBookStatus = (orderId, status) => {
    let updatedBook = {
      ...orderId,
      published: PUBLISH_STATUSES[status].status,
    };
    console.log(updatedBook, "updadsf")
    this.props.updateBook({book: updatedBook, token: this.state.token});
  }

  deleteBook = (book_id) => {
    this.props.deleteBook({ book_id, page: this.state.page, token: this.state.token });
    this.handleClose();
  }

  outOfStock = (book) => {
    let outOfStockBook = {
      ...book,
      is_out_of_stack:"Y",
      qty:book.qty-1
    }
    console.log(outOfStockBook,"out");
    
    // this.props.updateBook({ outOfStockBook, token: this.state.token });
    // this.props.deleteBook({ book_id, page: this.state.page, token: this.state.token });
    this.handleClose();
  }

  // hanldClose shipping address 
  handleClose = () => {
    this.setState({
      openDialogInventory: false,
      openDialogBook : false,
      openDialogForConfirmation: false,
      openbookstockDialog:false,
    })
  };

  handleBookEditDialog = (book_id) => {
    this.props.fetchBookByBookId({ book_id, token: this.state.token });
    this.setState({
      openDialogBook: true
    })
  }

  handleClickOnBookId = (book_id) => {
    this.props.getBookInventoryByBookId({ book_id, token: this.state.token });
    this.setState({
      openDialogInventory: true,
      book_id
    });
  }

  handleClickOnRackNo = (index) => {
    this.props.changeCurrentInventory(index);
    this.forceUpdate();
  }

  confirmForDelete = (book_id) => {
    this.setState({
      openDialogForConfirmation: true,
      deleteBookId: book_id
    })
  }

  handleSearchResult = () => {
    if(this.state.searchValue.length){
    this.setState({page : 0, clearValue: 0})
    this.props.getBooksSearchResult(localStorage.getItem('user'), this.state.searchValue)
    }
  }

  clearSearch = () =>{
    this.setState({searchValue:"", clearValue: 1, page:0})
  }

  onChangeHandlerSearch = (e) =>{
    this.setState({searchValue:e.target.value});
  }
  onClickISBNDeleteConfirmation = () =>{
    this.setState({openDeleteIsbn:true})
  }
  onClickISBNDelete = () =>{
    let header = {
      headers: {
        Authorization: `Token ${this.state.token}`
      }
    };

    axios.patch(`https://data.mypustak.com/api/v1/post/get_bookinfo/delete-isbn`, null, header)
    .then(res =>{
      this.setState({openDeleteIsbn:false})
      alert(`ISBN deleted successfully`)
    })
    .catch(err =>{
      alert(`Something went wrong`)
    })

  }

  onClickBooksStillInstock = () =>{
    let header = {
      headers: {
        Authorization: `Token ${this.state.token}`
      }
    };
    axios.get(`https://data.mypustak.com/api/v1/checkstocking`, header)
    .then(res =>{
      this.setState({openbookstockDialog: true, bookstocklist:res.data.order_stock_list})
      console.log("BookstockData are ", res.data.order_stock_list)
    })
    .catch(err =>{
      console.log("err is ", err)
    })

  }


  onClickButtonHandler = (book_inv_id, book_id, order_id) =>{
    this.setState({openConfirmationOutstockDialog: true, book_inv_id: book_inv_id, book_id: book_id, book_order_id: order_id})

  }

  handleCloseConfirmation = () =>{
    this.setState({openConfirmationOutstockDialog: false})
  }


  onOutStockHandler = (book_id, book_inv_id) => {
    console.log(book_id, book_inv_id,"outstock");
    
    let header = {
      headers: {
        Authorization: `Token ${localStorage.getItem("user")}`
      }
    };

    axios
      .post(
        `https://data.mypustak.com/api/v1/restock_return_order/outstock/${book_inv_id}/${book_id}`,
        null,
        header
      )
      .then(res => {
        if (res.data.status == 200) {
          alert(`Book outstocked successfully`)
          this.setState({ openConfirmationOutstockDialog: false });
          this.onClickBooksStillInstock()
        }
      })
      .catch(err => {
        alert(`something went wrong`)
        console.log(err)});
  };


  onOutStockHandlerBookInvPopup = (book_id, book_inv_id) => {
    console.log(book_id, book_inv_id,"outstock");
    
    let header = {
      headers: {
        Authorization: `Token ${localStorage.getItem("user")}`
      }
    };

    axios
      .post(
        `https://data.mypustak.com/api/v1/restock_return_order/outstock/${book_inv_id}/${book_id}`,
        null,
        header
      )
      .then(res => {
        if (res.data.status == 200) {
          alert(`Book outstocked successfully`)
          this.setState({ openConfirmationOutstockDialog: false });
          // this.onClickBooksStillInstock()
        }
      })
      .catch(err => {
        alert(`something went wrong`)
        console.log(err)});
  };



  render() {
    const { classes, inventoriesByBookId, 
      currentInventory, currentBook, loading } = this.props;
    let {
      bookinventoryList,
      order,
      orderBy,
      selected,
      rowsPerPage,
      page
    } = this.state;

    console.log(currentInventory, "currentInventory")
    
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, bookinventoryList.length - page * rowsPerPage);

    bookinventoryList = bookinventoryList.map(bookinventory => {
      return this.createData(bookinventory);
    });

    console.log(currentBook, "currentBook")
    let bookinventoryListSub = bookinventoryList
    if(this.props.searchbookresults.length && this.state.searchValue.length && !this.state.clearValue){
      bookinventoryList = this.props.searchbookresults;
    }
    else if(this.state.searchValue =="" && this.state.clearValue){
      bookinventoryList = bookinventoryListSub
    }

    // console.log(currentInventory, "currentInventory")

    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "16px",
            flexWrap: 'wrap',
            alignItems: 'flex-end',
          }}
        >
                <div>
        <DateBookFilter/>
        </div>
          <div style={{ display: "flex", paddingBottom: "16px", flexGrow: '8' }}>
          <TextField
            id="search"
            className={classNames(classes.margin, classes.textField)}
            variant="outlined"
            type={"text"}
            label="Search"
            placeholder="Seach by Author/ Book Id/ Uploader/ Title/ Slug/ Publication/ Book Inv Id/ RackNo/ ISBN"
            value={this.state.searchValue}
            onChange={this.onChangeHandlerSearch}
            InputProps={{
              endAdornment: (
                <InputAdornment position="end">
                <IconButton
                      aria-label="clear"
                      onClick={this.clearSearch}
                    >
                      <Clear />
                    </IconButton>
                  <IconButton
                    aria-label="Search"
                    onClick={this.handleSearchResult}
                  >
                    <Search />
                  </IconButton>
                </InputAdornment>
              )
            }}
            fullWidth
          />
        </div>
        <div style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "16px",
            flexWrap: 'wrap',
          }}>
              <Button
                variant="contained"
                color="primary"
                size="small"
                className={classes.button}
                onClick={ () => this.handleupdateBookInventoryAclick() }
              >
                Add New Books
              </Button>
              <Button
                variant="contained"
                color="secondary"
                size="small"
                className={classes.button}
                onClick={this.onClickISBNDeleteConfirmation}
              >
                Delete ISBN
              </Button>

              <Button
                variant="contained"
                size="small"
                className={classes.button}
                onClick={this.onClickBooksStillInstock}
                style={{backgroundColor:"cyan"}}
              >
              Books instock
              </Button>

          </div>
          
        </div>
        <Divider />

          <Paper className={classes.root}>
          <div
              style={{
                padding: "12px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <IconButton
                aria-label="Delete"
                disabled={this.state.page === 0}
                className={classes.margin}
                onClick={() => this.handlePaging("left")}
              >
                <KeyboardArrowLeft fontSize="small" />
              </IconButton>
              <IconButton
                aria-label="Delete"
                className={classes.margin}
                onClick={() => this.handlePaging("right")}
                disabled={ this.props.loading }
              >
                <KeyboardArrowRight fontSize="small" />
              </IconButton>
              Page: {this.state.page}
            </div>
            <div className={classes.tableWrapper}>
              <Table className={classes.table} aria-labelledby="tableTitle">
                <BookInventoryHead
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={this.handleSelectAllClick}
                  onRequestSort={this.handleRequestSort}
                  rowCount={bookinventoryList.length}
                />
                <TableBody>
                {
                    loading 
                    ? <tr style={{ textAlign: 'center' }}>  <td colSpan={9}> Loading... </td> </tr>
                    : bookinventoryList.length 
                      ? stableSort(bookinventoryList, getSorting(order, orderBy))
                        .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        .map(n => {
                          const isSelected = this.isSelected(n.id);
                          {/* Here below we setting row data and divs conditionally */}
                          return (
                            <TableRow
                              hover
                              onClick={event => this.handleClick(event, n.id)}
                              role="checkbox"
                              aria-checked={isSelected}
                              tabIndex={-1}
                              key={n.id}
                              selected={isSelected}
                            >

                              {/* <TableCell padding="checkbox">
                                <Checkbox checked={isSelected} inputProps={{ style: { display: 'none' } }} />
                              </TableCell> */}
                              <TableCell style={{ padding: 'unset', width: '12px' }}/>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '50px', maxWidth: '150px', textOverflow: 'ellipsis', overflow: 'hidden' }}> <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${n.thumb}`} height="150px" /></div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '90px', maxWidth: '200px', textOverflow: 'ellipsis', overflow: 'hidden' }}> {n.title} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '90px', maxWidth: '200px', textOverflow: 'ellipsis', overflow: 'hidden' }}> {n.isbn} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '120px', maxWidth: '150px', textOverflow: 'ellipsis', overflow: 'hidden', color: 'blue' }} onClick= {() => { this.handleClickOnBookId(n.book_id) }} > {n.book_id} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '120px', maxWidth: '200px', textOverflow: 'ellipsis', overflow: 'hidden' }}> {n.qty} </div>
                              </TableCell>
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ minWidth: '50px', maxWidth: '100px', textOverflow: 'ellipsis', overflow: 'hidden' }}> {n.published} </div>
                              </TableCell>
                              
                              <TableCell className={ classes.tablecell } component="th" scope="row" padding="none">
                                <div style={{ color: 'blue' }} onClick={() => this.handleBookEditDialog(n.book_id)} > {"Details"} </div>
                              </TableCell>
                              <TableCell>{n.action}</TableCell>
                            </TableRow>
                          );
                        })
                      : <tr style={{ textAlign: 'center' }}>  <td colSpan={9}> There isn't any wallet </td> </tr>
                  }
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 49 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
          </Paper>


        <Dialog 
            open={this.state.openDialogBook}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="lg"
          >
            <DialogTitle id="form-dialog-title"> {`Book - ${currentBook.book_id || 'NA'}`} </DialogTitle>
            <form onSubmit={this.updateBook}>
            <DialogContent>
              {
                  this.props.loading ? (
                    <div style={{ display: 'flex', justifyContent: 'center' }}> <CircularProgress className={classes.progress} /> </div> 
                  ) : (
                    <div style={{ border: '1px solid #d5d5d5', borderRadius: '8px', padding: '12px' }}>
                    <TextField
                      name="book_id"
                      margin="dense"
                      id="book_id"
                      label="Book Id"
                      type="text"
                      value={currentBook.book_id}
                      disabled
                      fullWidth
                    />
                    <div> <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${currentBook.thumb}`} height="80px"/> </div>
                    <TextField
                      name="title"
                      margin="dense"
                      id="book_title"
                      label="Name"
                      type="text"
                      defaultValue={currentBook.title}
                      fullWidth
                    />
                    <TextField
                      name="isbn"
                      margin="dense"
                      id="isbn"
                      label="ISBN"
                      type="text"
                      defaultValue={currentBook.isbn}
                      fullWidth
                    />
                    <TextField
                      name="author"
                      margin="dense"
                      id="author"
                      label="Author"
                      type="text"
                      defaultValue={currentBook.author}
                      fullWidth
                    />
                    <TextField
                      name="publication"
                      margin="dense"
                      id="publication"
                      label="Publication"
                      type="text"
                      defaultValue={currentBook.publication}
                      fullWidth
                    />
                    <TextField
                      name="category"
                      margin="dense"
                      id="category"
                      label="Category"
                      type="text"
                      defaultValue={currentBook.category}
                      fullWidth
                    />
                    <TextField
                      name="publication_date"
                      margin="dense"
                      id="publication_date"
                      label="Publication Date"
                      type="text"
                      defaultValue={currentBook.publication_date}
                      fullWidth
                    />
                    <TextField
                      name="price"
                      margin="dense"
                      id="price"
                      label="Price"
                      type="text"
                      defaultValue={currentBook.price}
                      fullWidth
                    />
                    <TextField
                      name="no_of_pages"
                      margin="dense"
                      id="no_of_pages"
                      label="No Of Pages"
                      type="text"
                      defaultValue={currentBook.no_of_pages}
                      fullWidth
                    />
                    <TextField
                      name="weight"
                      margin="dense"
                      id="weight"
                      label="Weight"
                      type="text"
                      defaultValue={currentBook.weight}
                      fullWidth
                    />
                    <TextField
                      name="binding"
                      margin="dense"
                      id="binding"
                      label="Binding"
                      type="text"
                      defaultValue={currentBook.binding}
                      fullWidth
                    />
                    <TextField
                      name="edition"
                      margin="dense"
                      id="edition"
                      label="Edition"
                      type="text"
                      defaultValue={currentBook.edition}
                      fullWidth
                    />
                    <TextField
                      name="qty"
                      margin="dense"
                      id="qty"
                      label="Quantity"
                      type="text"
                      defaultValue={currentBook.qty}
                      fullWidth
                    />
                    <TextField
                      name="language"
                      margin="dense"
                      id="language"
                      label="Language"
                      type="text"
                      defaultValue={currentBook.language}
                      fullWidth
                    />
                    <TextField
                      name="book_desc"
                      margin="dense"
                      id="book_desc"
                      label="Book Description"
                      type="text"
                      defaultValue={currentBook.book_desc}
                      fullWidth
                    />
                    <TextField
                      name="book_desc_url"
                      margin="dense"
                      id="book_desc_url"
                      label="Book Description Url"
                      type="text"
                      defaultValue={currentBook.book_desc_url}
                      fullWidth
                    />
                    <TextField
                      name="uploaded_by"
                      margin="dense"
                      id="uploaded_by"
                      label="Uploaded By"
                      type="text"
                      defaultValue={currentBook.uploaded_by}
                      disabled
                      fullWidth
                    />
                    <div style={{ marginTop: '8px', display: 'flex', justifyContent: 'space-between' }}>
                      <Button variant="outlined" onClick={() => this.deleteBook(currentBook.book_id)}>
                        Delete
                      </Button>


                      <div>
                        <Button type="reset" color="default" style={{ marginRight: '6px' }}>
                          Reset
                        </Button>
                        <Button type="submit" color="primary" variant="contained">
                          Update
                        </Button>
                      </div>
                    </div>
                    </div>
                  )
              }
            </DialogContent>
          </form>
        </Dialog>


        <Dialog 
            open={this.state.openbookstockDialog}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="md"
          >
            <DialogTitle> {`List of books which order have been successfully placed but are still in stock in inventory`} </DialogTitle>
            <div style={{
                marginBottom:15,
                marginTop:15,
                padding:10}}>{`Total Books are ${this.state.bookstocklist.length}`}</div>
            {this.state.bookstocklist.map((book, i)=>
          
              <div key={i} style={{
                border: "3px solid cyan",
                marginBottom:15,
                marginTop:15,
                padding:10,

              }}>
              <div style={{display:'inline-block', marginLeft:10}}>
              <span style={{display:'block'}}>{`order_id #${book.order_id} `}</span>
              <img style={{width:70, height:100,display:'block'}} src={this.state.img_path + book.thumb}></img>

              </div>
              <div style={{display:'inline-block'}}>
              <span style={{display:'block', marginTop:20}}>{`#${book.rack}`}</span>
              <span style={{display:'block', margingTop:20}}>{book.title}</span>
              </div>
              <div style={{margin:20}}>
              <Button
              variant="contained"
              color="primary"
              margin="normal"
              style={{
                display: "flex",
                flexWrap: "wrap",
                margintop: "theme.spacing(1)",
                padding: 10,
                marginLeft: 8
              }}
              onClick={() =>
                this.onClickButtonHandler(book.book_inv_id, book.book_id, book.order_id)
              }
            >
              OutStock?
            </Button>
              </div>

              
            </div>
            )}
            
        </Dialog>


        <Dialog
          open={this.state.openConfirmationOutstockDialog}
          onClose={this.handleCloseConfirmation}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="lg"
        >
          <DialogTitle id="form-dialog-title">
            {" "}
            {`Are you sure you want to Outstock This Book book_id #${
              this.state.book_id
            }, book_inv_id #${this.state.book_inv_id} and order_id #${this.state.book_order_id} ?`}
          </DialogTitle>
          <DialogContent>
            This can't be undone and it will Outstock this book from
            Bookinventory and reduce the quantity from book
          </DialogContent>
          <DialogActions>
            <Button color="default" onClick={this.handleCloseConfirmation}>
              Cancel
            </Button>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              onClick={() =>
                this.onOutStockHandler(
                  this.state.book_id,
                  this.state.book_inv_id
                )
              }
            >
              Done
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={this.state.openDeleteIsbn}
          onClose={()=>this.setState({openDeleteIsbn:false})}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="lg"
        >
          <DialogTitle id="form-dialog-title">
            {" "}
            Confirmation
          </DialogTitle>
          <DialogContent>
            Confirm Isbn Delete
          </DialogContent>
          <DialogActions>
            <Button color="default" onClick={()=>this.setState({openDeleteIsbn:false})}>
              Cancel
            </Button>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              onClick={this.onClickISBNDelete}
            >
              Done
            </Button>
          </DialogActions>
        </Dialog>


        <Dialog 
            open={this.state.openDialogInventory}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="lg"
          >
            <DialogTitle id="form-dialog-title"> {`Book Inventory - ${currentInventory.book_inv_id || 'NA'}`} </DialogTitle>
            <form onSubmit={this.addWallet}>
            <DialogContent>
              {
                this.props.inventoriesByBookId.length ? 
                  this.props.loading ? (
                    <div style={{ display: 'flex', justifyContent: 'center' }}> <CircularProgress className={classes.progress} /> </div> 
                  ) : (
                    <div style={{ display: 'flex', border: '1px solid #d5d5d5', borderRadius: '8px' }}>
                      <div style={{ width: '30%' }}>
                        <List style={{ maxHeight: '80vh', overflowY: 'scroll' }}>
                          {
                            inventoriesByBookId.map((inventory, i) => (
                              <ListItem button key={i} onClick={() => this.handleClickOnRackNo(i)} selected={inventory.rack_no === currentInventory.rack_no}>
                                <ListItemText primary={inventory.rack_no} />
                              </ListItem>
                            ))
                          }
                        </List>
                      </div>
                      <div style={{ width: '100%', padding: '8px' }}>
                          {/* {JSON.stringify(currentInventory)} */}
                          {
                            currentInventory ? (
                              <BookInventoryDiv getBookInventoryByBookId={this.getBookInventoryByBookId} currentInventory={currentInventory} updateBookInventory={this.props.updateBookInventory} deleteBookInventory={this.props.deleteBookInventory} onOutStockHandlerBookInvPopup={this.onOutStockHandlerBookInvPopup} bookId={this.state.book_id} token={this.state.token} />
                            ) : ''
                          }
                      </div>
                    </div>
                  )
                :
                <div style={{ textAlign: 'center' }}> There isn't any inventory </div>
              }
            </DialogContent>
            {
              !this.props.loading ? (
            <DialogActions>
                  {/* <Button onClick={this.handleClose} color="default">
                    Cancel
                  </Button>
                  <Button variant="outlined" type="reset">
                    Reset
                  </Button>
                  <Button variant="contained" type="submit" color="primary">
                    Add
                  </Button> */}
            </DialogActions>
          ) : ''
          
          }
          </form>

        </Dialog>


        <Dialog 
            open={this.state.openDialogForConfirmation}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="lg"
          >
            <DialogTitle id="form-dialog-title"> {`Are you sure you want to delete Book ${this.state.deleteBookId} ?`} </DialogTitle>
            <DialogContent>
            This can't be undone and it will delete complete data of this book and all details of its inventory.
            </DialogContent>
            {
              !this.props.loading ? (
              <DialogActions>
                    <Button color="default" onClick={this.handleClose}>
                      Cancel
                    </Button>
                    <Button variant="contained" type="submit" color="primary" onClick={() => this.deleteBook(this.state.deleteBookId)}>
                      Done
                    </Button>
              </DialogActions>
          ) : ''
          }
        </Dialog>

      </div>
    );
  }
}

BookInventory.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = (state) => {
  const { bookinventoryList, loading, inventoriesByBookId, currentInventory, currentBook } = state.bookInventoryBR;
  const searchbookresults = state.bookInventoryBR.searchbookresults;
  return { bookinventoryList, loading, inventoriesByBookId, currentInventory, currentBook, searchbookresults };
};

export default connect(
  mapStateToProps,
  { getBooksSearchResult, getBookInventoryList, getBookInventoryByBookId, changeCurrentInventory, updateBookInventory, deleteBookInventory,  fetchBookByBookId, updateBook, deleteBook }
)(withStyles(styles)(BookInventory));
