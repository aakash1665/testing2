import React from 'react';

import TextField from "@material-ui/core/TextField";
import moment from "moment";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";

class BookInventoryDiv extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
     ...props.currentInventory
    }
  }

  componentWillReceiveProps(nextProps) {
    if(this.props.currentInventory !== nextProps.currentInventory) {
      this.setState({
        ...nextProps.currentInventory
      })
    }
  }

  handleUpdate = name => e => {
    this.setState({ [name] : e.target.value })
  }

  updateBookInventory = (book_inv_id) => {
    let bookInventory = {
      ...this.state
    }
    this.props.updateBookInventory({ bookInventory, token: this.props.token });
    this.props.getBookInventoryByBookId();
    alert(`Book inventory ${book_inv_id} updated.`)
  }

  deleteBookInventory = (book_inv_id) => {
    if(window.confirm(`Are you sure you want to delete inventory ${book_inv_id}`)) {
      this.props.deleteBookInventory({ book_inv_id, token: this.props.token });
    }
  }

  render() {
    const { book_inv_id, donation_req_id, rack_no, book_condition, u_date, uploaded_by, i_date, new_price } = this.state;
    console.log(this.props,"state");
    
    return (
      <div>
      <TextField
        name="donation_req_id"
        margin="dense"
        id="donation_req_id"
        label="Donation Req ID"
        type="text"
        value={donation_req_id}
        disabled
        fullWidth
      />
      <TextField
        name="rack_no"
        margin="dense"
        id="rack_no"
        label="Rack No"
        type="text"
        value={this.state.rack_no}
        onChange={this.handleUpdate('rack_no')}
        fullWidth
      />
      <TextField
        name="book_condition"
        margin="dense"
        id="book_condition"
        label="Book Condition"
        type="text"
        value={book_condition}
        onChange={this.handleUpdate('book_condition')}
        fullWidth
      />    
      <TextField
        name="u_date"
        margin="dense"
        id="u_date"
        label="Last Updated Date"
        type="text"
        value={moment.unix(u_date).format("DD-MM-YYYY h:mm a")}
        disabled
        fullWidth
      />
      <TextField
        name="uploaded_by"
        margin="dense"
        id="uploaded_by"
        label="Uploaded By"
        type="text"
        value={uploaded_by}
        onChange={this.handleUpdate('uploaded_by')}
        fullWidth
      />
      <TextField
        name="i_date"
        margin="dense"
        id="i_date"
        label="Created Date"
        type="text"
        value={moment.unix(i_date).format("DD-MM-YYYY  h:mm a")}
        disabled
        fullWidth
      />
      <TextField
        name="new_price"
        margin="dense"
        id="new_price"
        label="New Price"
        type="text"
        value={new_price}
        onChange={this.handleUpdate('new_price')}
        fullWidth
      />
      <div style={{ marginTop: '8px', display: 'flex', justifyContent: 'space-between' }}>
        <Button variant="outlined" onClick={() => this.deleteBookInventory(book_inv_id)}>
          Delete
        </Button>

        <Button variant="contained" color="secondary" onClick={() => this.props.onOutStockHandlerBookInvPopup(this.props.bookId,book_inv_id)}>
                        OutStock
                      </Button>

        <Button onClick={() => this.updateBookInventory(book_inv_id)} color="primary" variant="contained">
          Update
        </Button>
      </div>
      </div>
    )
  }
}

export default BookInventoryDiv;