import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import FilterListIcon from "@material-ui/icons/FilterList";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import { lighten } from "@material-ui/core/styles/colorManipulator";


const rows = [
  
  {
    id: "thumb",
    numeric: false,
    disablePadding: true,
    label: "Thumb"
  },
  {
    id: "title",
    numeric: false,
    disablePadding: false,
    label: "Name"
  },
  { id: "isbn", numeric: false, disablePadding: false, label: "ISBN" },
  { id: "book_id", numeric: false, disablePadding: true, label: "Book Id" },
  {
    id: "qty",
    numeric: false,
    disablePadding: false,
    label: "Stock"
  },
  {
    id: "published",
    numeric: false,
    disablePadding: false,
    label: "is Pub?"
  },
  { id: "details", numeric: false, disablePadding: false, label: "Details" },
  { id: "action", numeric: false, disablePadding: false, label: "Action" }
];

class DonationHead extends React.Component {
  createSortHandler = property => event => {
    this.props.onRequestSort(event, property);
  };

  render() {
    const {
      onSelectAllClick,
      order,
      orderBy,
      numSelected,
      rowCount
    } = this.props;

    return (
      <TableHead>
        <TableRow>
          <TableCell padding="checkbox">
            {/* <Checkbox
              indeterminate={numSelected > 0 && numSelected < rowCount}
              checked={numSelected === rowCount}
              onChange={onSelectAllClick}
            /> */}
          </TableCell>
          {rows.map(
            (row, i) => (
              <TableCell
                key={row.id}
                align={row.numeric ? "right" : "left"}
                padding={"none"}
                sortDirection={orderBy === row.id ? order : false}
                style={ i === 0 ? { fontSize: '11pt' } : { fontSize: '11pt' }}
              >
                <Tooltip
                  title="Sort"
                  placement={row.numeric ? "bottom-end" : "bottom-start"}
                  enterDelay={300}
                >
                  <TableSortLabel
                    active={orderBy === row.id}
                    direction={order}
                    onClick={this.createSortHandler(row.id)}
                  >
                    {row.label}
                  </TableSortLabel>
                </Tooltip>
              </TableCell>
            ),
            this
          )}
        </TableRow>
      </TableHead>
    );
  }
}

DonationHead.propTypes = {
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.string.isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

export default DonationHead;
