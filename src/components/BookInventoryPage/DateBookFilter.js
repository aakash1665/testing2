import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import axios from "axios";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TablePagination from "@material-ui/core/TablePagination";
import ReactPaginate from "react-paginate";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import IconButton from "@material-ui/core/IconButton";
import classNames from "classnames";
import config from "react-global-configuration";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import { Discardbook } from "../../actions/backendBookInventoryActions";
import { connect } from "react-redux";

class DateBookFilter extends Component {
  state = {
    startDate: "",
    book_condition: "",
    bookdata: "",
    count: 0,
    openBookDataDialog: false,
    loader: false,
    img_path:
      "https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/",
    book_id: "",
    book_inv_id: "",
    openDialogForConfirmation: false,
    rack_no: "",
    page: 1,
    rackNo: "",
    openDiscardConfirmDialog: false,
  };

  onChangeStartDate = e => {
    this.setState({
      startDate: e.target.value
    });
  };

  onClickFilterHandler = ({ page }) => {
    console.log(page, "page");

    this.setState({
      token: localStorage.getItem("user"),
      loader: true
    });

    let book_condition = [],
      i_date;
    book_condition.push(this.state.book_condition);
    // alert(book_condition);
    if (this.state.startDate.length < 1) {
      i_date = Math.floor(Date.now() / 1000);
    } else {
      i_date = Math.floor(new Date(this.state.startDate).getTime() / 1000);
    }
    if (
      this.state.book_condition == "" ||
      this.state.book_condition == "None"
    ) {
      book_condition = [
        "VeryGood",
        "AlmostNew",
        "BrandNew",
        "AverageButInReadableCondition"
      ];
    }

    let header = {
      headers: {
        Authorization: `Token ${localStorage.getItem("user")}`
      }
    };

    let body = {
      book_condition: book_condition,
      i_date: i_date,
      rackNo: this.state.rackNo
    };

    axios
      .post(
        `${config.get('apiDomain')}/api/v1/get_rackNo/date-condition-book-filter/${page}`,
        body,
        header
      )
      .then(res => {
        console.log(res);
        
        this.setState({
          bookdata: res.data.books,
          count: res.data.total_books,
          loader: false,
          openBookDataDialog: true
        });
      })
      .catch(err => {
        console.log(err);
        
        this.setState({ loader: false, bookdata: [], count: 0 });
      });
  };

  onChangeSelectHandler = e => {
    this.setState({ book_condition: e.target.value });
  };

  handleClose = () => {
    this.setState({
      count: 0,
      bookdata: "",
      openBookDataDialog: false,
      page: 1
    });
  };

  onChangeRackNo = e => {
    this.setState({ rackNo: e.target.value });
  };

  onClickButtonHandler = (book_id, book_inv_id, rackNo) => {
    // alert("In button handler");
    this.setState({
      openDialogForConfirmation: true,
      book_id: book_id,
      book_inv_id: book_inv_id,
      rack_no: rackNo
    });
  };

  onOutStockHandler = (book_id, book_inv_id) => {
    console.log(book_id, book_inv_id,"book");
    
    let header = {
      headers: {
        Authorization: `Token ${localStorage.getItem("user")}`
      }
    };

    axios
      .post(
        `${config.get('apiDomain')}/api/v1/restock_return_order/outstock/${book_inv_id}/${book_id}`,
        null,
        header
      )
      .then(res => {
        if (res.data.status == 200) {
          alert(`Book outstocked successfully`);
          this.setState({ openDialogForConfirmation: false });
          this.onClickFilterHandler({ page: this.state.page });
        }
      })
      .catch(err => {
        alert(`Something went wrong`);
        console.log(err);
      });
  };

  handleCloseConfirmation = () => {
    this.setState({ openDialogForConfirmation: false});
  };


  handleCloseDiscardConfirmation = () => {
    this.setState({ openDiscardConfirmDialog: false});
  };


  onChangeCheckBoxHandler = (e, book_inv_id) => {
    let header = {
      headers: {
        Authorization: `Token ${localStorage.getItem("user")}`
      }
    };

    let body = {
      checkvalue: e.target.checked,
      book_inv_id: book_inv_id
    };

    axios
      .post(
        `${config.get('apiDomain')}/api/v1/get_rackNo/update-availability`,
        body,
        header
      )
      .then(res => {
        console.log(res);
        // if (res.data.status == 200) {
        // this.setState({ color: "green" });
        this.onClickFilterHandler({ page: this.state.page });
        // }
      })
      .catch(err => {
        alert(`some error occured`);
      });
  };
  HandleDiscardConfirmation =(book_id,book_inv_id)=>{
    this.setState({
      openDiscardConfirmDialog:!this.state.openDiscardConfirmDialog,
      book_id: book_id,
      book_inv_id: book_inv_id,
    })
  }
  HandleDiscard = (book_id, book_inv_id) =>{
    console.log(book_id, book_inv_id,"checkbox Available");
    
      let token =`Token ${localStorage.getItem("user")}`
      let header = {
        headers: {
          Authorization: token
        }
      };

      axios
      .post(
        `https://data.mypustak.com/api/v1/restock_return_order/discardbook/${book_inv_id}/${book_id}`,
        null,
        header
      )
      .then(res => {
        if (res.data.status == 200) {

          alert(`Book outstocked successfully`)
          this.handleCloseDiscardConfirmation();
          // this.setState({ openConfirmationOutstockDialog: false });
          // this.onClickBooksStillInstock()
          this.onClickFilterHandler({ page: this.state.page});
        }
      })
      .catch(err => {
        alert(`something went wrong`)
        console.log(err)});
  
  }
  handlePaging = type => {
    if (type == "left") {
      this.setState({ page: this.state.page - 1 });
      this.onClickFilterHandler({ page: this.state.page - 1 });
    } else if (type == "right") {
      this.setState({ page: this.state.page + 1 });
      this.onClickFilterHandler({ page: this.state.page + 1 });
    }
  };

  render() {
    // console.log(this.props.BookDiscarded,"Got Book");

    let color = "white";
    return (
      <React.Fragment>
        {localStorage.getItem("user") ? (
          <div>
            <form noValidate>
              <TextField
                id="rack_no"
                label="Rack No"
                type="text"
                value={this.state.rackNo}
                placeholder="Enter a Rack No"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap"
                }}
                onChange={this.onChangeRackNo}
                // InputLabelProps={{
                //   shrink: true
                // }}
              />

              <TextField
                id="startDate"
                label="Before Date"
                type="date"
                defaultValue=""
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onChange={this.onChangeStartDate}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <FormControl style={{ marginBottom: 30, width: 200 }}>
                <InputLabel htmlFor="book_condition" style={{ width: 200 }}>
                  Book Condition
                </InputLabel>
                <Select
                  value={this.state.book_condition}
                  onChange={this.onChangeSelectHandler}
                  inputProps={{
                    name: "book-condition",
                    id: "book_condition"
                  }}
                >
                  <MenuItem value="AverageButInReadableCondition">
                    Average
                  </MenuItem>
                  <MenuItem value="VeryGood">Very Good</MenuItem>
                  <MenuItem value="AlmostNew">Almost New</MenuItem>
                  <MenuItem value="BrandNew">Brand New</MenuItem>
                  <MenuItem value="None" selected>
                    None
                  </MenuItem>
                </Select>
              </FormControl>

              <Button
                variant="contained"
                color="primary"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onClick={() => this.onClickFilterHandler({ page: 1 })}
              >
                Filter Books
              </Button>
              <br />
            </form>
          </div>
        ) : null}

        <Dialog
          open={this.state.openBookDataDialog}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="md"
        >
          <DialogContent>
            {this.state.loader ? (
              <DialogTitle id="form-dialog-title">Loading...</DialogTitle>
            ) : (
              <DialogTitle id="form-dialog-title">
                {`Book data :--`}
                <div
                  style={{
                    padding: "12px",
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  <IconButton
                    aria-label="Delete"
                    disabled={this.state.page === 1}
                    // className={classes.margin}
                    onClick={() => this.handlePaging("left")}
                  >
                    <KeyboardArrowLeft fontSize="small" />
                  </IconButton>
                  <IconButton
                    aria-label="Delete"
                    // className={classes.margin}
                    onClick={() => this.handlePaging("right")}
                    disabled={this.state.bookdata.length < 20 ? true : false}
                  >
                    <KeyboardArrowRight fontSize="small" />
                  </IconButton>
                  Page: {this.state.page - 1}
                </div>
                <p>{`Total data #${this.state.count}`}</p>
              </DialogTitle>
            )}

            <React.Fragment>
              {this.state.bookdata.length ? (
                this.state.bookdata.map(data => (
                  <React.Fragment>
                    <p style={{ display: "None" }}>
                      {data.availability == "Y"
                        ? (color = "green")
                        : data.discarded == "Y"?(color= "orange"):(color = "white")
                        
                        }
                    </p>
                    <div
                      style={{
                        display: "grid",
                        gridTemplateColumns: "110 150",
                        backgroundColor: color,
                        border: "2px solid black",
                        width: "100%",
                        textAlign: "center",
                        marginTop: 10
                      }}
                    >
                      {/* {data.availability == "Y"
                        ? (color = "green")
                        : (color = "white")} */}
                      <div
                        style={{
                          paddingTop: 10,
                          width: 120,
                          paddingBottom: 10
                        }}
                      >
                        <img
                          style={{
                            width: 70,
                            height: 100,
                            marginLeft: 0
                          }}
                          src={this.state.img_path + data.thumb}
                          // alt={this.state.img_path + data.thumb}
                        />
                        <div
                          style={{
                            padding: 10
                          }}
                        >
                          <Button
                            variant="contained"
                            color="primary"
                            margin="normal"
                            style={{
                              display: "flex",
                              flexWrap: "wrap",
                              margintop: "theme.spacing(1)",
                              padding: 10,
                              marginLeft: 8
                            }}
                            onClick={() =>
                              this.onClickButtonHandler(
                                data.book_id,
                                data.book_inv_id,
                                data.rack_no
                              )
                            }
                          >
                            OutStock?
                          </Button>

           

                        </div>
             
                      </div>
     
                      <div
                        style={{
                          paddingTop: 10,
                          width: 150,
                          paddingBottom: 10
                        }}
                      >

                        {/* <FormControlLabel
                            control={
                              <Button
                                inputProps={{ style: { display: "none" } }}
                                // value="checkedB"
                                // checked={this.listTime === "latestLast30Days"}
                                checked={data.discarded == "Y"}
                                name="Discarded"
                                onChange={e=>this.HandleDiscard(
                                  data.book_id,
                                  data.book_inv_id,
                                e
                                )}
                              />
                            }
                            labelPlacement="start"
                            label="is Discarded?"
                            style={{
                              height: 20
                            }}
                        /> */}
                        <Button
                          variant="contained"
                          color="primary"
                          margin="normal"
                          style={{
                            display: "flex",
                            flexWrap: "wrap",
                            margintop: "theme.spacing(1)",
                            padding: 10,
                            marginLeft: 17
                          }}
                          onClick={() =>
                            this.HandleDiscardConfirmation(
                              data.book_id,
                              data.book_inv_id
                            )
                          }
                        >
                        Discarded
                        </Button>
                        {data.rack_no}
                        <div style={{ paddingTop: 10, marginLeft: 20 }}>
                          {data.title}
                          <div
                            style={{
                              padding: 2,
                              marginLeft: 20,
                              display: "flex",
                              alignItems: "center",
                              position: "relative"
                            }}
                          >
                            Is Available ?
                            <input
                              type="checkbox"
                              style={{ width: "10%" }}
                              onChange={e =>
                                this.onChangeCheckBoxHandler(
                                  e,
                                  data.book_inv_id
                                )
                              }
                              defaultChecked={
                                data.availability == "Y" ? true : false
                              }
                            />
                          </div>

    
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                ))
              ) : (
                <p>There is no more data</p>
              )}
            </React.Fragment>
          </DialogContent>
        </Dialog>

        <Dialog
          open={this.state.openDialogForConfirmation}
          onClose={this.handleCloseConfirmation}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="lg"
        >
          <DialogTitle id="form-dialog-title">
            {" "}
            {`Are you sure you want to Outstock This Book Rack #${
              this.state.rack_no
            } ?`}
          </DialogTitle>
          <DialogContent>
            This can't be undone and it will Outstock this book from
            Bookinventory and reduce the quantity from book
          </DialogContent>
          <DialogActions>
            <Button color="default" onClick={this.handleCloseConfirmation}>
              Cancel
            </Button>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              onClick={() =>
                this.onOutStockHandler(
                  this.state.book_id,
                  this.state.book_inv_id
                )
              }
            >
              Done
            </Button>
          </DialogActions>
        </Dialog>

        <Dialog
          open={this.state.openDiscardConfirmDialog}
          onClose={this.handleCloseDiscardConfirmation}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="lg"
        >
          <DialogTitle id="form-dialog-title">
            {" "}
            {`Are you sure you want to Discard This Book ?`}
          </DialogTitle>
          <DialogContent>
            This can't be undone and it will Outstock this book from
            Bookinventory and reduce the quantity from book
          </DialogContent>
          <DialogActions>
            <Button color="default" onClick={this.handleCloseDiscardConfirmation}>
              Cancel
            </Button>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              onClick={() =>
                this.HandleDiscard(
                  this.state.book_id,
                  this.state.book_inv_id
                )
              }
            >
              Done
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}
const mapStatesToProps = (state) =>({
  BookDiscarded  : state.bookInventoryBR.BookDiscarded
})
export default connect(mapStatesToProps,{Discardbook})(DateBookFilter);
