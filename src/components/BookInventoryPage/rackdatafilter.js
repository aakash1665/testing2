import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import { getRackFilterResult } from "../../actions/rackdatafilterActions";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import axios from "axios";
import { green } from "@material-ui/core/colors";
import Skeleton from '@grovertb/react-skeletor'
import config from "react-global-configuration";
class RackDataFilter extends Component {
  static propTypes = {
    RackData: PropTypes.object.isRequired,
    getRackFilterResult: PropTypes.func.isRequired
  };

  state = {
    rack_no: "",
    token: "",
    openorderDialog: false,
    openDialogForConfirmation: false,
    img_path:
      "https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/"
  };

  onChangeRackNo = e => {
    this.setState({ rack_no: e.target.value });
  };

  onClickRackFilterHandler = () => {
    // alert(`in rack filter handler`);
    this.setState({
      token: localStorage.getItem("user"),
      openorderDialog: true
    });
    if (this.state.rack_no.length) {
      this.props.getRackFilterResult(
        this.state.rack_no,
        localStorage.getItem("user")
      );
    }
  };

  handleClose = () => {
    this.setState({ openorderDialog: false, openDialogForConfirmation: false });
  };

  handleCloseConfirmation = () => {
    this.setState({ openDialogForConfirmation: false });
  };

  onClickButtonHandler = (book_id, book_inv_id, rackNo) => {
    // alert("In button handler");
    this.setState({
      openDialogForConfirmation: true,
      book_id: book_id,
      book_inv_id: book_inv_id,
      rackNo: rackNo
    });
  };

  onChangeCheckBoxHandler = (e, book_inv_id) => {
    let header = {
      headers: {
        Authorization: `Token ${localStorage.getItem("user")}`
      }
    };

    let body = {
      checkvalue: e.target.checked,
      book_inv_id: book_inv_id
    };

    axios
      .post(
        `${config.get("apiDomain")}/api/v1/get_rackNo/update-availability`,
        body,
        header
      )
      .then(res => {
        console.log(res);
        this.props.getRackFilterResult(
          this.state.rack_no,
          localStorage.getItem("user")
        );

        // if (res.data.status == 200) {
        // this.setState({ color: "green" });
        // this.onClickRackFilterHandler();
        // }
      })
      .catch(err => {
        console.log(err,body);
        
        alert(`some error occured`);
      });

    // alert(`${e.target.checked}`);
  };

  onOutStockHandler = (book_id, book_inv_id) => {
    let header = {
      headers: {
        Authorization: `Token ${localStorage.getItem("user")}`
      }
    };

    axios
      .post(
        `${config.get("apiDomain")}/api/v1/restock_return_order/outstock/${book_inv_id}/${book_id}`,
        null,
        header
      )
      .then(res => {
        if (res.data.status == 200) {
          this.setState({ openDialogForConfirmation: false });
          this.onClickRackFilterHandler();
          alert('Succesfull')
        }
      })
      .catch(err => {
        console.log(err)
        alert(`Error :- ${err}`)
      });
  };

  render() {
    const { data, total_count } = this.props.RackData;
    let color;
    // console.log("In component and data are ", data, this.props.RackData);

    return (
      <div>
        <React.Fragment>
          <form noValidate>
            <TextField
              id="rack_no"
              label="Rack No"
              type="text"
              value={this.state.rack_no}
              placeholder="Enter a Rack No"
              margin="normal"
              style={{
                marginLeft: "theme.spacing(1)",
                marginRight: "theme.spacing(1)",
                width: 200,
                display: "flex",
                flexWrap: "wrap"
              }}
              onChange={this.onChangeRackNo}
              InputLabelProps={{
                shrink: true
              }}
            />
            <Button
              variant="contained"
              color="primary"
              margin="normal"
              style={{
                marginLeft: "theme.spacing(1)",
                marginRight: "theme.spacing(1)",
                width: 200,
                display: "flex",
                flexWrap: "wrap",
                margintop: "theme.spacing(1)"
              }}
              onClick={this.onClickRackFilterHandler}
            >
              Get Details
            </Button>
            <br />
          </form>
        </React.Fragment>

        <Dialog
          open={this.state.openorderDialog}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="md"
        >
          <DialogContent>
            {data ? (
              <DialogTitle id="form-dialog-title">
                {`This Rack no #${this.state.rack_no} details`}
                <p>{`Total data #${total_count}`}</p>
              </DialogTitle>
            ) : this.props.loader ? (
              <DialogTitle id="form-dialog-title">

              <Skeleton height={250} width={200} style={{ 
                  width:"100%",
                  height:"30vh",
                  margin:"5%"
                
               }}/> 
              
              {/* <Skeleton height={250} width={200}/>  */}
              
              Loading...
              
              
              </DialogTitle>
            ) : (
              <DialogTitle id="form-dialog-title">
                {`This Rack no #${this.state.rack_no} is not present`}
              </DialogTitle>
            )}

            <React.Fragment>
              {data
                ? data.map(data => (
                    <React.Fragment>
                      <p style={{ display: "None" }}>
                        {data.availability == "Y"
                          ? (color = "green")
                          : (color = "white")}
                      </p>
                      <div
                        style={{
                          display: "grid",
                          gridTemplateColumns: "110 150",
                          backgroundColor: color,
                          border: "2px solid black",
                          width: "100%",
                          textAlign: "center",
                          marginTop: 10
                        }}
                      >
                        {/* {data.availability == "Y"
                        ? (color = "green")
                        : (color = "white")} */}
                        <div
                          style={{
                            paddingTop: 10,
                            width: 120,
                            // paddingBottom: 10
                          }}
                        >
                        <div 
                        style={{ 
                          display:'flex',

                         }}
                        
                        >

                          <img
                            style={{
                              width: '80%',
                              height: '150px',
                              marginLeft: 0,
                              padding: '5%',
                            }}
                            src={this.state.img_path + data.thumb}
                            // alt={this.state.img_path + data.thumb}
                          />
                          <div>
                          <Button
                              variant="contained"
                              color="primary"
                              margin="normal"
                              size="small"
                              style={{
                                display: "flex",
                                flexWrap: "wrap",
                                margintop: "theme.spacing(1)",
                               marginTop:"7%",
                                marginLeft: 8
                              }}
                              onClick={() =>
                                this.onClickButtonHandler(
                                  data.book_id,
                                  data.book_inv_id,
                                  data.rack_no
                                )
                              }
                            >
                              OutStock?
                            </Button>

                            <p 
                              style={{ 
                                marginTop:'50%',
                                fontWeight:"bold"
                               }}
                            >
                          {data.rack_no}
                          </p>
                          </div>
                          
                        </div>

                        </div>
                        <div
                        >
                          <div style={{ paddingTop: 5,fontSize:'90%',fontWeight:"bold"}}>
                            {data.title}
                            <div
                              style={{
                                padding: 2,
                                // marginLeft: 20,
                                display: "flex",
                                alignItems: "center",
                                position: "relative",
                                fontWeight:"normal",
                                textAlign:"center"
                              }}
                            >
                            <p
                            style={{ 
                              textAlign:"center",
                              width: "70%"
                             }}
                            >
                              Is Available ?
                              <input
                                type="checkbox"
                                style={{ 
                                  width: "10%" ,
                                  transform: "scale(1.3)",
                                  marginLeft: "10%",
                                  padding:'3%'
                              }}
                                onChange={e =>
                                  this.onChangeCheckBoxHandler(
                                    e,
                                    data.book_inv_id
                                  )
                                }
                                defaultChecked={
                                  data.availability == "Y" ? true : false
                                }
                              />
                              </p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </React.Fragment>
                  ))
                : null}
            </React.Fragment>
          </DialogContent>
        </Dialog>

        <Dialog
          open={this.state.openDialogForConfirmation}
          onClose={this.handleCloseConfirmation}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="lg"
        >
          <DialogTitle id="form-dialog-title">
            {" "}
            {`Are you sure you want to Outstock This Book Rack #${
              this.state.rackNo
            } ?`}
          </DialogTitle>
          <DialogContent>
            This can't be undone and it will Outstock this book from
            Bookinventory and reduce the quantity from book
          </DialogContent>
          <DialogActions>
            <Button color="default" onClick={this.handleCloseConfirmation}>
              Cancel
            </Button>
            <Button
              variant="contained"
              type="submit"
              color="primary"
              onClick={() =>
                this.onOutStockHandler(
                  this.state.book_id,
                  this.state.book_inv_id
                )
              }
            >
              Done
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  RackData: state.rackData.rackData,
  loader: state.rackData.loader
});

export default connect(
  mapStateToProps,
  { getRackFilterResult }
)(RackDataFilter);
