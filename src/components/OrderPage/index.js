import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import FilterListIcon from "@material-ui/icons/FilterList";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import { lighten } from "@material-ui/core/styles/colorManipulator";
import Divider from "@material-ui/core/Divider";
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import red from '@material-ui/core/colors/red';
import moment from "moment";
import { connect } from "react-redux";

import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Search from "@material-ui/icons/Search";
import Clear from "@material-ui/icons/Clear";
import Edit from "@material-ui/icons/Edit";
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

import { getOrdersList, updateOrder, getShippingAddressInfo, updateShippingAddress,getFilterOrders } from "../../actions/BackendorderAction";

import OrderHead from "./OrderHead";
import Menu from '../DropDownMenu';

import axios from 'axios';

import { desc, stableSort, getSorting } from '../../util/helpers';
import { getOrderDetails, RestockBook ,makeBookOutStock} from '../../actions/backofficeorderdetailsAction';

// statuses for order
const STATUSES = [
  { id: 0, name: "Processing", color: "blue" },  
  { id: 1, name: "Delivered", color: "green" },
  { id: 2, name: "Cancelled", color: "red" },
  { id: 3, name: "Refunded", color: "brown" },
  { id: 4, name: "Failed", color: "red" },
  { id: 5, name: "Shipping", color: "gray" },
  { id: 6, name: "Shipment Booked", color: "orange" },
  { id: 7, name: "Ready to ship", color: "pink" },
  { id: 'NA', name: "NA", color: "yellow" }
];

// cod statuses for order
const COD_STATUSES = [
  { id: 0, name: "COD Offer Interested", color: "blue" },  
  { id: 1, name: "COD Waiting", color: "green" },
  { id: 2, name: "COD Confirmed", color: "red" }
];


const COURIERS = [
  { value: 1, label: "Fedex" },  
  { value: 4, label: "Delhivery" },
  { value: 1001, label: "Indiapost" },
  { value: 2, label: "Aramax" },
  { value: 39, label: "Spoton" }    
]

const CP_STATUS = [
  {value: 0,label: "Default"},
  {value: 1,label: "ORDER PLACED" },
  {value: 2,label: "PICKUP PENDING" },
  {value: 3,label: "PICKUP FAILED" },
  {value: 4,label: "PICKED UP" },
  {value: 5,label: "INTRANSIT" },
  {value: 6,label: "OUT FOR DELIVERY" },
  {value: 7,label: "NOT SERVICEABLE" },
  {value: 8,label: "DELIVERED" },
  {value: 9,label: "FAILED DELIVERY" },
  {value: 10,label: "CANCELLED ORDER" },
  {value: 11,label: "RTO REQUESTED" },
  {value: 12,label: "RTO" },
  {value: 13,label: "RTO OUT FOR DELIVERY" },
  {value: 14,label: "RTO DELIVERED" },
  {value: 15,label: "RTO FAILED" },
  {value: 16,label: "LOST" },
  {value: 17,label: "DAMAGED" },
  {value: 18,label: "SHIPMENT DELAYED" },
  {value: 19,label: "CONTACT CUSTOMER CARE" },
  {value: 20,label: "SHIPMENT HELD" },
  {value: 21,label: "RTO INTRANSIT" },
  {value: 25,label: "OUT FOR PICKUP" },
  {value: 26,label: "RTO CONTACT CUSTOMER CARE" },
  {value: 27,label: "RTO SHIPMENT DELAY" },
  {value: 28,label: "AWB REGISTERED" },
  {value: 101,label: "RETURN ORDER PLACED" },

]

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020
  },
  tablecell: {
    fontSize: '11pt',
    padding: '4px'
  },
  tableWrapper: {
    overflowX: "auto"
  },
  button: {
    whiteSpace: "nowrap",
    marginTop: '4px',
    marginLeft: "6px"
  },
  margin: {
    margin: "0 4px"
  }
});

class Order extends React.Component {
  state = {
    order: "desc",
    orderBy: "order_id",
    selected: [],
    orderList: [],
    page: 0,
    rowsPerPage: 10,
    token: '',
    openorderDialog : false,

    openShippingAddDialog: false,
    img_path : "https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/",

    districtname: '',
    statename: '',
    opencodtoprepaidDialog:false,
    payusing_cod: "",
    cod_chargeCOD:"",
    amount_COD:"",
    order_id_COD:"",
    comment_COD: "",
    payment_url_COD: "",
    SelectedStatus:"None",
    FilterDate:"",
    FilterApplied:false
   };

  componentDidMount() {
    this.setState({ token: localStorage.getItem('user') });
    if(!this.props.orderList.length) {
      this.props.getOrdersList({ page: this.state.page, token: localStorage.getItem('user') });
    } else {
      this.setState({
        orderList: this.props.orderList
      })
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.orderList !== nextProps.orderList) {
      let { orderList } = nextProps;
      this.setState(
        {
          orderList: orderList
        }
      );
    }
  }

  handleUpdateStatusUpdate = (orderId, status) => {
    let updatedOrder = {
      order_id: orderId,
      status: status
    };
    this.props.updateOrder(updatedOrder, this.state.token);
  }

  handleUpdateCODStatusUpdate = (orderId, cod_status) => {
    console.log(orderId, cod_status, "CODUPDATE");
    let updatedOrder = {
      order_id: orderId,
      cod_offer_interested: cod_status
    };
    this.props.updateOrder(updatedOrder, this.state.token);
  }

  setValue = (e) => {
    this.setState({searchValue: e.target.value})
  }

  handleChangeSearch = () => {
    // console.log(e.target.value, data);
    const searchValue = this.state.searchValue;
    if(searchValue) {
      this.setState({ page: 0 });
      this.props.getOrdersList({ page: this.state.page, token: this.state.token, search: true, searchValue });
    } else {
      this.setState({ page: 0 });
      this.props.getOrdersList({ page: this.state.page, token: this.state.token, search: true });
    }
  }

  clearSearch = () => {

    this.setState({FilterApplied:false,SelectProps:"None",SelectedStatus:'None'})
    this.setState({ searchValue: '' }, () => this.handleChangeSearch() );
  }

  // create data is a function which will create data for table row,
  // in this file we use this, you can check below.
  // In this we are creating data as per table header
  createData = ({
    cod_charge,
    order_id,
    user_id,
    no_of_book,
    cp_status,
    amount,
    payusing,
    i_date,
    shipping_add_id,
    fast_delivery,
    status,
    cod_offer_interested
  }) => {
    let statusObj = STATUSES.find(statusobj => statusobj.id == status);
    let CpstatusObj = CP_STATUS.find(cp_statusobj => cp_statusobj.value == cp_status);
    let codStatusObj = COD_STATUSES.find(cod_statusobj => cod_statusobj.id == cod_offer_interested)
    return {
      id: order_id,
      status: statusObj.name, 
      order: order_id,
      purchased: no_of_book,
      cp_status : CpstatusObj.label,
      falster_delivery: fast_delivery,
      total: amount,
      cod_charge,
      date: moment.unix(i_date).format("DD-MM-YYYY h:mm a"),
      // cp_status:0,
      action: (
        <div style={{ display: 'flex' }}>
          <Menu icon={<Edit fontSize="small" />} orderId={order_id} options={STATUSES} status={statusObj} handleSelect={this.handleUpdateStatusUpdate} /> 
          {
            payusing === 'cod' ? (
              <Menu icon={<ArrowDropDown fontSize="small" />} orderId={order_id} options={COD_STATUSES} status={codStatusObj} handleSelect={this.handleUpdateCODStatusUpdate} /> 
            ): ''
          }
        </div>
      ),
      user_id,
      shipping_add_id,
      amount,
      payusing,
      statusObj,
      codStatusObj,
      cod_offer_interested: payusing === 'cod' ? '- ' + COD_STATUSES.find(cod_status => cod_status.id === cod_offer_interested).name : ''
    };
  }

  // it will handle sorting request
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  // it is for handle select all click, right now we're not using but if we need in future so it's there.
  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.orderList.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  // handle click on Checkbox and select accordingly
  // its for multiple selection
  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, id);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }
    this.setState({ selected: newSelected });
  };

  // handle request for page change
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  // handle pagination using type
  handlePaging = type => {
    if(!this.state.FilterApplied){
        if (type == "right") {
          if(this.props.orderList.length / this.state.rowsPerPage == this.state.page + 1 ) {
            this.props.getOrdersList({ page: this.state.page + 1, token: this.state.token });
          }
          this.setState({ page: this.state.page + 1 });
        } else {
          this.setState({ page: this.state.page - 1 });
        }
      }else{
        if(type == "right"){
          if(this.props.orderList.length / this.state.rowsPerPage == this.state.page + 1 ) {
            this.props.getFilterOrders({ page: this.state.page +1 , token: localStorage.getItem('user'),status: this.state.SelectedStatus,date : this.state.FilterDate  })
          }
            this.setState({ page: this.state.page + 1 });
            console.log();
            

        }else{
          this.setState({ page: this.state.page - 1 });
        }
      }
  };

  // when you click on more button in order column
  // this function will call and set the modal variable to true
  getShippingAddressInfo = order_id => {
    this.props.getShippingAddressInfo(order_id, this.state.token);
    this.setState({
      openShippingAddDialog: true,
    })
  };

  // hanldClose shipping address 
  handleClose = () => {
    this.setState({
      openShippingAddDialog: false,
      openorderDialog: false,
      opencodtoprepaidDialog: false
    })
  };

  // Update shipping address 
  // when you click on update button in shipping address modal
  // below function will call
  updateShippingAddress = (e) => {
    e.preventDefault();
    let updatedShippingAddress = this.props.shippingAddress;
    Object.assign(updatedShippingAddress, {
      rec_name: e.target.rec_name.value,
      email: e.target.email.value,
      phone: e.target.phone.value,
      address: e.target.address.value,
      landmark: e.target.landmark.value,
      pincode: e.target.pincode.value,
      city: e.target.city.value,
      state: e.target.state.value,
      courier_name: this.state.courier_name,
      awb: e.target.awb.value
    });
    this.props.updateShippingAddress(updatedShippingAddress, this.state.token);
    this.setState({ courier_name: '' })
    this.handleClose();
  }

  // below is for getting destrict and state from pincode
  // below function will call the api and get the information
  pincodeChange = (e) => {
    if(e.target.value) {
      axios.get(`http://data.mypustak.com/pincode/pin_check/${e.target.value}/`)
      .then( res => {
        console.log(res);
        if(res.data.length) {
          const { districtname, statename } = res.data[0];
          this.setState({
            districtname,
            statename
          })
        } else {
          this.setState({
            districtname: 'NA',
            statename: 'NA'
          })
        }
      })
      .catch( err => console.log(err))
    }
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };


  onClickHandler = (order_id) =>{
    this.props.getOrderDetails(localStorage.getItem('user'), order_id);
    this.setState({openorderDialog : true});
  }

  onReturnHandler = (book_id, book_inv_id) =>{
    this.props.RestockBook(localStorage.getItem('user'), book_id, book_inv_id);
  }

  onClickoutStockHandler = (book_id, book_inv_id) =>{
    this.props.makeBookOutStock(localStorage.getItem('user'), book_id, book_inv_id);
  }


  onClickCODToPrepadiHandler = (order_id, cod_charge, amount) =>{
    this.setState({opencodtoprepaidDialog:true, order_id_COD: order_id, cod_chargeCOD: cod_charge, amount_COD: amount})
  }

  onChangeSelectHandler = (e) =>{
    this.setState({payusing_cod: e.target.value})
  }


  onChangePaymentCODURL = (e) =>{
    this.setState({payment_url_COD: e.target.value})
  }

  onChangeCommentCOD = (e) =>{
    this.setState({comment_COD: e.target.value})
  }




  onClickCODToPrepaidConvertHandler = () =>{

    let header = {
      headers: {
        Authorization: `Token ${this.state.token}`
      }
    };

    let body={
      'cod_charge': this.state.cod_chargeCOD,
      'amount': this.state.amount_COD,
      'payusing': this.state.payusing_cod,
      'comment': this.state.comment_COD,
      'payment_url': this.state.payment_url_COD
    }
    axios.patch(`https://data.mypustak.com/api/v1/convert_cod_order_to_prepaid_order/convert-cod-order-to-prepaid-order-manually/${this.state.order_id_COD}`, body, header)
    .then(res =>{
      console.log(res);
      alert(`Order_id #${this.state.order_id_COD} successfully converted to prepaid`);
      this.setState({opencodtoprepaidDialog:false})
    })
    .catch(err =>{
      console.log(err)
      alert(`Something went wrong`);
    })


  }

  selectFilterDate=e=>{
    console.log(e.target.value,"date",new Date(e.target.value).getTime()/1000);
    
    this.setState({FilterDate:new Date(e.target.value).getTime()/1000})
  }

  handleSelectFilterOptions=e=>{
    console.log(e.target.value,"selected filter");
    // if(e.target.value !== "None"){
      this.setState({SelectedStatus:e.target.value})
    // }
    
  }

  ApplyFilter=()=>{
    console.log("Clicked");
    if(this.state.SelectedStatus !== "None" && this.state.FilterDate){
      // console.log(this.state.SelectedStatus ,this.state.FilterDate ,"logg");
      
      this.setState({FilterApplied:true,page:0})
      this.props.getFilterOrders({ page: 0 , token: localStorage.getItem('user'),status: this.state.SelectedStatus,date : this.state.FilterDate  })
    }else{
      alert("Select the Status and Date")
    }
  }

  render() {

    const { orde_details, } = this.props.OrderDetails;
    console.log("Order_details is ", orde_details); 
    console.log("Order_List is ", this.state.orderList); 

    const { classes, shippingAddress, loading } = this.props;
    const { id, order_id, i_date, rec_name, email, phone, address, landmark, pincode, city, state, country, courier_name, awb } = shippingAddress;
    let {
      orderList,
      order,
      orderBy,
      selected,
      rowsPerPage,
      page
    } = this.state;
    const emptyRows = rowsPerPage - Math.min(rowsPerPage, orderList.length - page * rowsPerPage);

    orderList = orderList.map(order => {
      // console.log(order);
      return this.createData(order);
    });

    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "16px",
            flexWrap: 'wrap',
            alignItems: 'flex-end',
            
          }}
        >
          <div style={{ display: "flex", paddingBottom: "16px", flexGrow: '8' }}>
            <TextField
              id="search"
              className={classNames(classes.margin, classes.textField)}
              variant="outlined"
              type={"text"}
              label="Search"
              placeholder="Seach by Customer Name/Order Request ID/ Email ID/ Mobile Number"
              // value={this.state.search}
              onChange={this.setValue}
              InputLabelProps={{ shrink: true }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="Search"
                      onClick={this.clearSearch}
                    >
                      <Clear />
                    </IconButton>
                    <IconButton
                      aria-label="Search"
                      onClick={this.handleChangeSearch}
                    >
                      <Search />
                    </IconButton>
                  </InputAdornment>
                )
              }}
              fullWidth
            />
          </div>
          <div style={{
              display: "flex",
    
              paddingBottom: "16px",
              flexWrap: 'wrap',
            }}>
            <form>
              <select onChange={this.handleSelectFilterOptions} required value={this.state.SelectedStatus}>
              <option value="None">Select</option>
                <option value="0">Processing</option>
                <option value="1">Deliverd</option>
                <option value="2">Cancelled</option>
                <option value="3">Refunded</option>
                <option value="4">Failed</option>
                <option value="5">Shipping</option>
                <option value="6">Shipment Booked</option>
                <option value="7">Ready to ship</option>
              </select>
              <input type='date' style={{ width:'140px' }} onChange={this.selectFilterDate}/>
            <Button
              variant="contained"
              size="small"
              color="secondary"
              className={classes.button}
              onClick={this.ApplyFilter}
            >
              Filter
            </Button>
            </form>
          </div>
          <div style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingBottom: "16px",
              flexWrap: 'wrap',
            }}>
            {/* <Button
              variant="contained"
              size="small"
              color="primary"
              className={classes.button}
            >
              Get confirmed Order
            </Button> */}
            <Button
              variant="contained"
              size="small"
              color="primary"
              className={classes.button}
            >
              Print Invoice
            </Button>
            <Button
              variant="contained"
              size="small"
              color="primary"
              className={classes.button}
            >
              Print Label
            </Button>
            </div>
          </div>
          <Divider />
          
          <Paper className={classes.root}>
          <div
              style={{
                padding: "12px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <IconButton
                aria-label="Delete"
                disabled={this.state.page === 0}
                className={classes.margin}
                onClick={() => this.handlePaging("left")}
              >
                <KeyboardArrowLeft fontSize="small" />
              </IconButton>
              <IconButton
                aria-label="Delete"
                className={classes.margin}
                onClick={() => this.handlePaging("right")}
                disabled={ this.props.loading }
              >
                <KeyboardArrowRight fontSize="small" />
              </IconButton>
              Page: {this.state.page}
            </div>
            <div className={classes.tableWrapper}>
              <Table className={classes.table} aria-labelledby="tableTitle">
                <OrderHead
                  numSelected={selected.length}
                  order={order}
                  orderBy={orderBy}
                  onSelectAllClick={this.handleSelectAllClick}
                  onRequestSort={this.handleRequestSort}
                  rowCount={orderList.length}
                />
                <TableBody>
                  {
                    loading 
                    ? <tr style={{ textAlign: 'center' }}>  <td colSpan={9}> Loading... </td> </tr>
                    : orderList.length 
                      ? stableSort(orderList, getSorting(order, orderBy))
                          .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                          .map(n => {
                            const isSelected = this.isSelected(n.id);
                            let status = STATUSES.find(
                              status => status.id === n.status
                            );
                            {/* Here below we setting row data and divs conditionally */}
                            return (
                              <TableRow
                                hover
                                onClick={event => this.handleClick(event, n.id)}
                                role="checkbox"
                                aria-checked={isSelected}
                                tabIndex={-1}
                                key={n.id}
                                selected={isSelected}
                              >
                                <TableCell padding="checkbox">
                                  <Checkbox checked={isSelected} inputProps={{ style: { display: 'none' } }} />
                                </TableCell>
                                <TableCell style={{ padding: 'unset', width: '12px' }}/>
                                <TableCell className={ classes.tablecell } component="th" scope="row" padding="none"><div > {n.status} </div></TableCell>
                                <TableCell className={ classes.tablecell }> <div > {"#"+n.order} <span style={{ color: 'blue', cursor: 'pointer' }} onClick={() => this.getShippingAddressInfo(n.order)} > {"more"} </span> </div> </TableCell>
                                <TableCell className={ classes.tablecell }> <div style={{ minWidth: '80px' }}> {n.purchased+" Book"} </div> </TableCell>
                                <TableCell className={ classes.tablecell }> 
                                          <div style={{ maxWidth: '70px',
                                                         backgroundColor:'green',
                                                         color:'white',
                                                         borderRadius:'4px',
                                                         fontWeight:'bold',
                                                         textAlign:'center',
                                                         fontSize:'80%',
                                                         }}> 
                                                {n.cp_status} 
                                          </div> </TableCell>

                                <TableCell className={ classes.tablecell }>
                                  <div style={{ minWidth: '190px' }}> 
                                    <strong> {n.amount+"₹"} </strong> {` by ${n.payusing}` } 
                                    <span style={{ fontSize: '12px', color: n.codStatusObj.color }}> { `${n.cod_offer_interested}`} </span> 
                                    <div style={{ fontSize: '12px' }}>  </div> 
                                  </div> 
                                  </TableCell>
                                <TableCell className={ classes.tablecell }> <div style={{ maxWidth: '60px' }}> {n.fast_delivery ? "Y" : ""} </div> </TableCell>
                                <TableCell className={ classes.tablecell }> <div style={{ minWidth: '150px' }}> {n.date} </div> </TableCell>
                                <TableCell className={ classes.tablecell }>
                                <div style={{ }}>{n.action}{n.cod_charge || n.status=="Failed" ? <Tooltip
                                    title="Convert to Prepaid"
                                    enterDelay={100}
                                  ><IconButton
                                        aria-label="convert cod to prepaid"
                                        style={{"backgroundColor":"rgb(234,134,167)",}}
                                        onClick={() => this.onClickCODToPrepadiHandler(n.order, n.cod_charge, n.amount)}
                                      >
                                        <i class="fab fa-google-wallet"></i>
                                      </IconButton></Tooltip>
                                  : null}
                                  </div>
                               </TableCell>
                                <TableCell><Button
          variant="contained"
          color="primary"
          margin="normal"
          style={{
            // marginLeft: "theme.spacing(1)",
            marginRight: "theme.spacing(1)",
            width: 150,
            display: "flex",
            flexWrap: "wrap",
            margintop: "theme.spacing(1)"
          }}
          onClick={() => this.onClickHandler(n.order)}
        >
          Order Details
        </Button></TableCell>
        
        {/* {n.cod_charge ? <TableCell><Button
          variant="contained"
          color="secondary"
          margin="normal"
          style={{
            marginLeft: "theme.spacing(1)",
            marginRight: "theme.spacing(1)",
            width: 170,
            display: "flex",
            flexWrap: "wrap",
            margintop: "theme.spacing(1)"
          }}
          onClick={() => this.onClickCODToPrepadiHandler(n.order, n.cod_charge, n.amount)}
        >
         COD To Prepaid
        </Button></TableCell>: null} */}

         

                              </TableRow>
                            );
                          }) 
                      : <tr style={{ textAlign: 'center' }}>  <td colSpan={9}> There isn't any order </td> </tr>
                  }
                  {emptyRows > 0 && (
                    <TableRow style={{ height: 49 * emptyRows }}>
                      <TableCell colSpan={6} />
                    </TableRow>
                  )}
                </TableBody>
              </Table>
            </div>
          </Paper>
            


          <Dialog
            open={this.state.opencodtoprepaidDialog}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="md"
          >
            <DialogTitle id="form-dialog-title">Title</DialogTitle>
            <DialogContent>
                  <React.Fragment>
                  <form autoComplete="off">
                    <TextField
                      autoFocus
                      name="order_id"
                      margin="dense"
                      id="order_id"
                      label="Order Id"
                      type="text"
                      value={this.state.order_id_COD}
                      fullWidth
                      disabled
                    />
                    <div style={{ display: 'flex' }}>
                    <TextField
                      name="amount"
                      margin="dense"
                      id="amount"
                      label="Amount"
                      type="text"
                      value={this.state.amount_COD}
                      inputProps={{
                        style: { width: 'unset', height: 'unset', marginLeft: 'unset', fontSize: 'unset' }
                      }}
                      fullWidth
                      disabled
                    />
                    <TextField
                      name="cod_charge"
                      margin="dense"
                      id="cod_charge"
                      label="cod charge"
                      type="text"
                      value={this.state.cod_chargeCOD}
                      style={{ marginLeft: '8px' }}
                      inputProps={{
                        style: { width: 'unset', height: 'unset', marginLeft: 'unset', fontSize: 'unset' }
                      }}
                      fullWidth
                      disabled
                    />
                    </div>
                    <TextField
                      name="payment_url"
                      margin="dense"
                      id="payment_url"
                      label="Payment URL"
                      type="text"
                      onChange={this.onChangePaymentCODURL}
                      placeholder="Only 100 Characters"
                      // inputProps={{
                      //   maxLength: 100,
                      //   style:{color:"rgb(255,11,11)"}
                      // }}
                      defaultValue={this.state.payment_url_COD}
                      fullWidth
                    />
                    <TextField
                      name="comment"
                      margin="dense"
                      id="comment"
                      label="Payment Id"
                      required
                      type="text"
                      placeholder="Only 100 Characters"
                      onChange={this.onChangeCommentCOD}
                      inputProps={{
                        maxLength: 100,
                        // style:{color:"rgb(255,11,11)"}
                      }}
                      defaultValue={this.state.comment_COD}
                      fullWidth
                    />
        <FormControl fullWidth style={{marginBottom:30}}>
        <InputLabel htmlFor="payusing" fullWidth>Paid via</InputLabel>
        <Select
          value={this.state.payusing_cod}
          onChange={this.onChangeSelectHandler}
          inputProps={{
            name: 'payusing-codtoprepaid',
            id: 'payusing',
          }}
        >
          <MenuItem value="Paytm">Paytm</MenuItem>
          <MenuItem value="razorpay">Rajorpay</MenuItem>
          <MenuItem value="wallet">MyPustak Wallet</MenuItem>
          <MenuItem value="Google Pay">Google Pay</MenuItem>
          <MenuItem value="PhonePe">PhonePe</MenuItem>
          <MenuItem value="Bank">Bank</MenuItem>
        </Select>
      </FormControl>
      <Button
          variant="contained"
          color="secondary"
          margin="normal"

          style={{
            width: 110,
            display: "flex",
            flexWrap: "wrap",
            margintop: "theme.spacing(1)"
          }}
          onClick={() => this.onClickCODToPrepaidConvertHandler()}
        >
         Convert
        </Button>
      </form>
                    
                  </React.Fragment>

            </DialogContent>

        </Dialog>
        

          <Dialog
          open = {this.state.openorderDialog && orde_details}
          onClose = {this.handleClose}
          aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="md"
          >
          <DialogContent>
          <DialogTitle id="form-dialog-title"> {`This Order's Details`} </DialogTitle>

          <React.Fragment>
          {orde_details ? orde_details.map(order =>
            <table style={{border: "2px solid grey", padding:20, margin:10, backgroundColor:"pink", textAlign:"center"}}>
            <tbody>
            <tr>
            <td style={{width:200, padding: 20}}>Book Id :</td>
            <td style={{width:200, padding: 20}}>{order.book_id}</td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>Book Inv Id :</td>
            <td style={{width:200, padding: 20}}>{order.book_inv_id}</td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>Book Image :</td>
            <td style={{width:200, padding: 20}}><img src={ this.state.img_path + order.thumb} alt={this.state.img_path + order.thumb}></img></td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>Book Title :</td>
            <td style={{width:200, padding: 20}}>{order.title}</td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>Book Author :</td>
            <td style={{width:200, padding: 20}}>{order.author}</td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>Book Slug :</td>
            <td style={{width:200, padding: 20}}>{order.slug}</td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>New Price :</td>
            <td style={{width:200, padding: 20}}>{order.new_price}</td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>Is Book Found :</td>
            <td style={{width:200, padding: 20}}>{order.is_book_found}</td>
            </tr>
            <tr>
            <td style={{width:200, padding: 20}}>Is Book Soldout :</td>
            <td style={{width:200, padding: 20}}>{order.is_soldout}</td>
            </tr>
            <tr>
              <td style={{width:200, padding: 20}}></td>
              <td style={{ display:'flex',  justifyContent: 'space-around' }}>          
                <Button
          variant="contained"
          color="primary"
          margin="normal"
          style={{
            width: 100,
            display: "flex",
            flexWrap: "wrap",
            margintop: "theme.spacing(1)"
          }}
          onClick={() => this.onReturnHandler(order.book_id, order.book_inv_id)}
        >
          Instock?
        </Button>
        
        <Button
          variant="contained"
          color="secondary"
          margin="normal"
          style={{
            width: 100,
            display: "flex",
            flexWrap: "wrap",
            margintop: "theme.spacing(1)"
          }}
          onClick={() => this.onClickoutStockHandler(order.book_id, order.book_inv_id)}
        >
          Outstock?
        </Button>
        
        </td>
            </tr>

            </tbody>
            </table>
          ): null}
          </React.Fragment>
          </DialogContent>

          </Dialog>


          {/* Dialog box for address info */}         
          <Dialog
            open={this.state.openShippingAddDialog}
            onClose={this.handleClose}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="md"
          >
            <DialogTitle id="form-dialog-title"> {`Shipping Address - ID: ${id} Order #${order_id} at ${moment.unix(i_date).format("DD-MM-YYYY h:mm a")} `} </DialogTitle>
            <form onSubmit={this.updateShippingAddress}>
            <DialogContent>
              {
                this.props.shippingAddressLoading ? (
                  <div style={{ display: 'flex', justifyContent: 'center' }}> <CircularProgress className={classes.progress} /> </div> 
                ) : (
                  <React.Fragment>
                    <TextField
                      autoFocus
                      name="rec_name"
                      margin="dense"
                      id="rec_name"
                      label="Recepient name"
                      type="text"
                      defaultValue={rec_name}
                      fullWidth
                    />
                    <div style={{ display: 'flex' }}>
                    <TextField
                      name="email"
                      margin="dense"
                      id="email"
                      label="Email"
                      type="email"
                      defaultValue={email}
                      inputProps={{
                        style: { width: 'unset', height: 'unset', marginLeft: 'unset', fontSize: 'unset' }
                      }}
                      fullWidth
                    />
                    <TextField
                      name="phone"
                      margin="dense"
                      id="phone"
                      label="Phone"
                      type="text"
                      maxLength={10}
                      // maxLength={40}
                      defaultValue={phone}
                      style={{ marginLeft: '8px' }}
                      inputProps={{
                        style: { width: 'unset', height: 'unset', marginLeft: 'unset', fontSize: 'unset' }
                      }}
                      fullWidth
                    />
                    </div>
                    <TextField
                      name="address"
                      margin="dense"
                      id="address"
                      label="Address"
                      type="text"
                      defaultValue={address}
                      fullWidth
                    />
                    <TextField
                      name="landmark"
                      margin="dense"
                      id="landmark"
                      label="Landmark"
                      type="text"
                      defaultValue={landmark}
                      fullWidth
                    />
                    <TextField
                      name="pincode"
                      margin="dense"
                      id="pincode"
                      label="Pincode"
                      type="text"
                      defaultValue={pincode}
                      onChange={ this.pincodeChange }
                      fullWidth
                    />
                    <TextField
                      name="city"
                      margin="dense"
                      id="city"
                      label="City"
                      type="text"
                      value={ this.state.districtname || city }
                      disabled
                      required
                      // defaultValue={ city}
                      fullWidth
                    />
                    <TextField
                      name="state"
                      margin="dense"
                      id="state"
                      label="State"
                      type="text"
                      value={ this.state.statename || state }
                      disabled
                      required
                      // defaultValue={state}
                      fullWidth
                    />
                    <TextField
                      name="country"
                      margin="dense"
                      id="country"
                      label="Country"
                      type="text"
                      defaultValue={country}
                      fullWidth
                      disabled
                    />
                    <TextField
                      id="standard-select-currency"
                      select
                      label="Courier Name"
                      name="courier_name"
                      className={classes.textField}
                      value={ this.state.courier_name || courier_name }
                      onChange={this.handleChange('courier_name')}
                      SelectProps={{
                        MenuProps: {
                          className: classes.menu,
                        },
                      }}
                      style={{ minWidth: '150px' }}
                      margin="dense"
                    >
                      {COURIERS.map(option => (
                        <MenuItem key={option.value} value={option.value}>
                          {option.label}
                        </MenuItem>
                      ))}
                    </TextField>
                    <TextField
                      name="awb"
                      margin="dense"
                      id="awb"
                      label="AWB"
                      type="text"
                      defaultValue={awb}
                      fullWidth
                    />
                  </React.Fragment>
                )
              }
            </DialogContent>
            {
              !this.props.shippingAddressLoading ? (
            <DialogActions>
                  <Button onClick={this.handleClose} color="default">
                    Cancel
                  </Button>
                  <Button variant="outlined" type="reset">
                    Reset
                  </Button>
                  <Button variant="contained" type="submit" color="primary">
                    Update
                  </Button>
            </DialogActions>
          ) : ''
          
          }
          </form>

        </Dialog>

      </div>
    );
  }
}

Order.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps =(state) => {
  const { orderList, loading, shippingAddress, shippingAddressLoading } = state.orderBR;
  const OrderDetails = state.orderdetails.orderdetails;
  const Restock = state.orderdetails.restock;
  return { orderList, loading, shippingAddress, shippingAddressLoading, OrderDetails, Restock };
};

export default connect(
  mapStateToProps,
  { getOrdersList, updateOrder, getShippingAddressInfo, updateShippingAddress, getOrderDetails, RestockBook ,makeBookOutStock,getFilterOrders}
)(withStyles(styles)(Order));
