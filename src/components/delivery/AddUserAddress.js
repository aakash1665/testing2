import React, { Component } from 'react'
import UserInputGroup from './UserInputGroup';
import './useraddress.css'
import { connect } from 'react-redux';
import {addAddressAction,Getaddress} from '../../actions/accountAction';
import axios from 'axios'
import config from 'react-global-configuration'

class AddUserAddress extends Component {
    state ={
        fullname :'',
        landmark:'',
        phone_no:'',
        alt_phone_no:'',
        pincode:'',
        address1:'',
        // address2:'',
        city_name:'',
        state_name:'',
        title:'',
        errors:{},
        asd:'',
        PinCodeOkk:'',
    };
    
closedone=()=>{
  // console.log(this.props.closemodule);
}
onSubmit =(e)=>{
    // alert("full");
    e.preventDefault();
            const {pincode,phone_no,alt_phone_no,fullname,state,landmark,address1,address_id,is_primary,city_name,errors,state_name,title} = this.state;

    if(fullname===""){
      this.setState({errors:{fullname:"Enter your name"}});
      return;
  }
  if(address1===""){
    this.setState({errors:{address1:"Enter your address"}});
    return;
  }
  if(landmark===""){
    this.setState({errors:{landmark:"Enter your landmark"}});
    return;
  }


    if(phone_no===""){
        this.setState({errors:{phone_no:"Enter number"}});
        return;
    }
    if(isNaN(phone_no)){
        this.setState({errors:{phone_no:"Enter 10 Digits"}});
        return;
    }
    if(isNaN(pincode)){
        this.setState({errors:{pincode:"Enter Pincode"}});
        return;
    }
    if(pincode===""){
      this.setState({errors:{pincode:"Enter Pincode"}});
      return;
  }
 

    if(phone_no.length !== 10){
        this.setState({errors:{phone_no:"Contact number must be 10 digits"}});
        return;
    }
    if(pincode.length !== 6){
        this.setState({errors:{pincode:"Pincode must be 6 digits"}});
        return;
    }
    // if(this.state.title.length === 0){
    //     this.setState({errors:{title:"Select any one"}});
    //     return;
    // }
    const address={
        data :{
  
            title,
            rec_name:fullname,
            address:address1,
            landmark:landmark,
            phone_no:phone_no,
            alt_phone_no:alt_phone_no,
            pincode:pincode,
            city_name:city_name,
            state_name:state_name,
            country_name:'IND',
            is_primary:'Y',
 
        }
    }
    if(this.state.PinCodeOkk === true){
    const token= `Token ${this.props.userToken}`
    this.props.addAddressAction(token,address)
    this.setState({
        fullname :'',
        landmark:'',
        phone_no:'',
        pincode:'',
        address1:'',
        city_name:'',
        state_name:'',
        title:'',
        errors:{}
    })
    this.props.CloseAddUserAddress()
    const details=`Token ${this.props.userToken}`
    // this.props.userdetails(details)
    this.props.Getaddress(details)
}
}
    // onChange = e =>{ this.setState({[e.target.name]:e.target.value})};
  render() {
    const onChange = e =>{ this.setState({[e.target.name]:e.target.value})};

    const {fullname,landmark,phone_no,alt_phone_no,address1,city_name,state_name,pincode,title,errors,is_primary} = this.state;
    var PincodeErr=""
    var PhoneErr=""
    var AltPhoneErr=""
    if(pincode.toString().length === 6 ){
       axios.get(`${config.get('apiDomain')}/pincode/pin_check/${this.state.pincode}/`,
        )
        .then(res=>{
          if(res.data.length === 0 ){
            this.setState({errors:{pincode:"Enter Correct Pincode"}});
            // return;
            this.setState({PinCodeOkk:false})
            PincodeErr="Enter Correct Pincode"
            // alert(PincodeErr)
        }else{
        this.setState({city_name:res.data[0].districtname,state_name:res.data[0].statename});
        this.setState({errors:{pincode:""}});
        // return;
        this.setState({PinCodeOkk:true});
        PincodeErr=""}})

        .catch(err=>{
          console.log(err)
        // alert("Wrong Pincode Entered")
        this.setState({city_name:"",state_name:""});
      
      }
        
        )
        // PincodeErr=""
     }
    //  alert(PincodeErr)

     if(isNaN(this.state.phone_no)){
      PhoneErr="Enter 10 Digits"
     }
     if(this.state.phone_no.length !== 10){
      PhoneErr="Enter 10 Digits"
     }

     if(this.state.alt_phone_no && isNaN(this.state.alt_phone_no)){
      AltPhoneErr="Enter 10 Digits"
     }
     if(this.state.alt_phone_no && this.state.alt_phone_no.length !== 10){
      AltPhoneErr="Enter 10 Digits"
     } 

     if(isNaN(this.state.pincode)){
      // PincodeErr="Enter Correct Pincode"
     }
     if(this.state.pincode.length !== 6){
      // PincodeErr="Enter Correct Pincode"
     }
    //  if()
     const onPinChange=()=>{
       if(this.state.pincode.length !== 6){
        PincodeErr="Enter Pincode "
       }
     }
    return (
        <div class="container-1" style={{marginRight:'3%',marginLeft:'3%'  }}>
        <div id="addressdiv">
        <a className="close" onClick={this.props.CloseAddUserAddress} style={{
    color: 'white',zIndex:'999',opacity:'1',fontWeight:'normal',marginTop:'-1vh',
    float:'left'}}>
              &times;
            </a>
        <div id="Add-New-Address">Add New Address
        </div></div>
        <br/><br/><br/>
        <form onSubmit={this.onSubmit}>

<UserInputGroup
            name="fullname"
            // label="Name"
            placeholder="Full Name"
            value={fullname}
            onChange={onChange}
            errors={errors.fullname}
            maxLength={40}
            />
<UserInputGroup
            name="address1"
            // label="Name"
            placeholder="Address 1"
            value={address1}
            onChange={onChange}
            errors={errors.name}
            maxLength={250}
            />
<UserInputGroup
            name="landmark"
            // label="Name"
            placeholder="Landmark"
            value={landmark}
            onChange={onChange}
            errors={errors.landmark}
            maxLength={80}
            />
<UserInputGroup
       
            name="phone_no"
            // label="Name"
            placeholder="Mobile Number"
            value={phone_no}
            onChange={onChange}
            // errors={errors.phone_no}
            maxLength="10"
            />
            <p style={{ fontSize:'70%',color:'red',textAlign:'center' }}>{PhoneErr}</p>

<UserInputGroup
       
       name="alt_phone_no"
       // label="Name"
       placeholder="Alternate Mobile Number Optional"
       value={alt_phone_no}
       onChange={onChange}
       // errors={errors.phone_no}
       maxLength="10"
       />
       <p style={{ fontSize:'70%',color:'red',textAlign:'center' }}>{AltPhoneErr}</p>
          {/* {(this.state.phone_no.length !== 10)?<span>Enter 10 digits</span>:null} */}
         
<UserInputGroup 
            name="pincode"
            // label="Name"
            placeholder="Pincode"
            value={pincode}
            onChange={onChange}
            errors={errors.pincode}
            maxLength="6"
            />
            <p style={{ fontSize:'70%',color:'red',textAlign:'center'  }}>{PincodeErr}</p>
  {/* {(this.state.pincode.length !== 6)?<span>Enter 10 digits</span>:null} */}

<UserInputGroup
            name="city"
            // label="Name"
            placeholder="City"
            value={city_name}
            onChange={(e)=>{this.setState({city_name:e.target.value})}}
            errors={errors.name}
            maxLength={40}
            />
<UserInputGroup
            name="state"
            // label="Name"
            placeholder="State"
            value={state_name}
            onChange={(e)=>{this.setState({state_name:e.target.value})}}
            errors={errors.name}
            maxLength={40}
            />

            {/* For future use  */}
{/* <input type="Checkbox" checked={state.is_primary} name="setdefault" style={{width:'13px',marginTop:'3%'}}  onChange={changecheck}/><span style={{    marginLeft: '6%',}}>Set As Default</span>  */}
{/* <InputGroup
            name="title"
            // label="Name"
            placeholder="Address Title"
            value={title}
            onChange={onChange}
            errors={errors.name}
            /> */}
            <br/>
            <label style={{marginTop:'2%'}}>Address Title</label>
<div>            
<input type="radio" value="Home" name="title" style={{width:'13px'}}  onChange={onChange} required/> 
<span style={{    marginLeft: '6%',}}  >Home</span>
<input type="radio" value="Office" name="title" style={{width:'13px',marginLeft: '16%'}} onChange={this.onChange}/> <span style={{    marginLeft: '6%',}} >Office</span>
</div>
<input type="submit" id="donebtn_button"  onClick={this.closedone()} value="Done"/> 
 </form>
</div>

    )
  }
}
const mapStateToProps = state => ({
    searchBook:state.homeR.searchBook,
    userToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
  })
// export default (AddAddress)

export default connect(mapStateToProps,{addAddressAction,Getaddress})(AddUserAddress);