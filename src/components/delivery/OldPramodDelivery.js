import React, { Component } from 'react'
import Popup from 'reactjs-popup'
import './address.css'
import { connect } from 'react-redux';
import AddAddress from './AddAddress';
import AddUserAddress from './AddUserAddress';
import CartAddUserAddress from '../Cart/CartAddUserAddress';
import CartUEditAddress from '../Cart/CartUEditAddress';
import {Getaddress,SelectedAddress,Editaddress} from '../../actions/accountAction'
import UEditAddress from './UEditAddress'
import {SendAddressId} from '../../actions/cartAction';
import ProceedToPay from '../ProceedToPay';
import {Get_Rp_Id} from '../../actions/donationActions'
class Delivery extends Component {

  state={
    open: false,
    OpenAddAddress:false,
    ProcedToPay:false,
    addressopen:false,
    UpdatedUserAddState:'',
  }
  // componentDidMount(){
  //   const details=`Token ${this.props.userToken}`
  //   console.log(details);
    
  //   // this.props.userdetails(details)
  //   this.props.Getaddress(details)
  // }
  openModal (){
    this.setState({ open: !this.state.open })
  }
  closeModal () {
    this.setState({ open: false })
  }
  // OpenProcedPay(uaddress.address_id,uaddress.address,uaddress.city_name,
  //   uaddress.pincode,uaddress.state_name)
  OpenProcedPay=(address_id,address,city_name,pincode,state_name,title,phone_no)=>{
    // alert("open")
  //   data={
  //     data :
  //     {
  //         "ref_id" : 515776,
  //         "amount" : this.props.CartPrice.TotalPayment
  //     }
  // }
  const selectedAddressData={
    address_id,
    address,
    city_name,
    pincode,
    state_name,
    title,
    phone_no
  }
    this.props.SelectedAddress(selectedAddressData)
    this.props.SendAddressId(address_id)
    // alert(data)
    this.props.Get_Rp_Id()
    this.props.CloseDelivery()
    // this.setState({ProcedToPay:true})
  }

  addressopenModal=(data)=>{
    this.setState({ addressopen: true })
    console.log(data)
    this.props.Editaddress(data)
  }
  addresscloseModal=()=>{
    
    this.setState({ addressopen: false })
   
  }

  addAddress=()=>
  {
    // alert("okk")
    this.setState({OpenAddAddress:true})
    // alert("calling")
    // window.location.href = "file:///home/pramod/pg_iframe.html"
  }
  // componentDidMount(){
  //   this.props.GetAddressAction()
  // //   const {compExamBooks} = this.props
  // // console.log(compExamBooks);
  
  // }
// ***********************RAZORPAY PART********************************
// options = {
//   "key": "rzp_live_pvrJGzjDkVei3G", //Paste your API key here before clicking on the Pay Button. 
//   // "key": "rzp_test_jxY6Dww4U2KiSA",
//   "name": `test`,
//   "amount" : `100`,
//   "Order Id": `${this.props.donation_req_id}`, //Razorpay Order id
//   "currency" : "INR",
//   "description": "Test Description",

//   "handler": function (response){    
//       const razorpay_payment_id=response.razorpay_payment_id;
//       const razorpay_order_id=response.razorpay_order_id;
//       const razorpay_signature=response.razorpay_signature;
//       console.log(razorpay_payment_id)
//       console.log(razorpay_order_id)
//       console.log(razorpay_signature)
//       // alert(response.razorpay_payment_id);
//       // alert(response.razorpay_order_id);
//       // alert(response.razorpay_signature);
      
      

//         // alert(response.razorpay_order_id)
//         // alert(response.razorpay_signature)
//                                 },

//   "prefill": {
//       // "method":"card,netbanking,wallet,emi,upi",
//       "contact" : '9123337544',
//       "email" : `email@email.com`
//   },
//          //             "notes": {
//     //                     "order_id": "your_order_id",
//     //                                 "transaction_id": "your transaction_id",
//     //                                 "Receipt": “your_receipt_id"
//     //             },
//   "notes": {
//       "Order Id": `12345`, //"order_id": "your_order_id",
//       "address" : "customer address to be entered here"
//   },
//   "theme": {
//       "color": "#1c92d2",
//        "emi_mode" : true
              
//   },
  
//   external: {
//  wallets: ['mobikwik' , 'paytm' , 'jiomoney' , 'payumoney'],
//   handler: function(data) {
//   console.log(this, data)
//   }
// }
// };

componentDidMount(){
  // this.props.Get_Rp_Id(this.data)
  // this.rzp1 = new window.Razorpay(this.options);
  }
  
  componentWillReceiveProps(UpdatedUserAdd){
    if(UpdatedUserAdd.UpdatedUserAdd !== this.state.UpdatedUserAddState ){
      const details=`Token ${this.props.userToken}`
      // this.props.userdetails(details)
      this.props.Getaddress(details)
      this.setState({UpdatedUserAddState:UpdatedUserAdd.UpdatedUserAdd})
      // this.setState({AddedAddressState:AddedAddress.AddedAddress})
    }
  }

  // onClick={()=>{this.rzp1.open();}}


  
  // "address_id": 32913,
  // "user_id": 214,
  // "title": "test_2702",
  // "rec_name": "test_aayush",
  // "pincode": 700156,
  // "address": "test",
  // "landmark": "test",
  // "phone_no": "8334827085",
  // "address_type": 0,
  // "is_primary": "N",
  // "state_name": "west bengal",
  // "city_name": "kolkata",
  // "country_name": "india"


  render() {
    const {getadd}=this.props
    return (

        <div id="DeliveryBody" style={{marginRight:'50%',height:'90%' }}>
        
       
          <div id="DALayer-2"> <h1 id="SelectAddress">Select a Delivery Address</h1>
          </div>
          <div style={{ height:'90%',overflow:'auto'}}>
          {getadd.map(uaddress=>
           <div style={{ borderBottom:'1px solid grey',overflow:'hidden'}}>
              <div id="DALayer-4">
                <div id="DAaddress">
                  <p id="DAuser-name">{uaddress.rec_name}</p>
                  <button id="DAhome-button" disabled><span id="home-button-text">{uaddress.title}</span></button>
                  <p id="DAaddress-info">{uaddress.address}, {uaddress.city_name} {uaddress.pincode}, {uaddress.state_name}</p>
                  {/* <p id="DAedit" onClick={()=>this.addressopenModal(uaddress)}>Edit</p> */}
                </div>
                <div id="DAselect"> 
                
                {/* <button id="Layer-10" onClick={()=>{this.rzp1.open();}}><span id="select-button">Select</span></button> */}
                {/* <button id="DALayer-10" onClick={()=>this.OpenProcedPay(uaddress.address_id,uaddress.address,uaddress.city_name,
                  uaddress.pincode,uaddress.state_name,uaddress.title,uaddress.phone_no)}>
                <span id="select-button">Select</span></button> */}
                                <button id="DALayer-10" onClick={()=>this.OpenProcedPay(uaddress.address_id,uaddress.address,uaddress.city_name,
                  uaddress.pincode,uaddress.state_name,uaddress.title)}>
                <span id="select-button">Select</span></button>
                <span id="DAedit" onClick={()=>this.addressopenModal(uaddress)}>Edit</span>
                {/* <Popup
              //  lockScroll={true}
                  open={this.state.OpenAddAddress}
                  onClose={()=>this.setState({OpenAddAddress:false})}
                  contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' }}
                  overlayStyle={{ background:'transparent' }}
                  >
                  <div >
                    <a className="close" onClick={()=>this.setState({OpenAddAddress:false})} style={{ marginLeft:'-6%',position:'absolute' }}>
                    &times;
                    </a>
                    <AddUserAddress/>
                  </div>
                </Popup> */}
                         {/* <Popup
               lockScroll={true}
                  open={this.state.ProcedToPay}
                  onClose={()=>this.setState({ProcedToPay:false})}
                  contentStyle={{ margin:'unset',marginLeft:'68%',width:'31%',float:'right',marginTop:'4%' }}
                  overlayStyle={{ background:'transparent' }}
                  >
                  <div >
                    <a className="close" onClick={()=>this.setState({ProcedToPay:false})} style={{ marginLeft:'-56%',position:'absolute' }}>
                    &times;
                    </a>
                    </div>
                    <ProceedToPay  closeProcedToPay={()=>this.setState({ProcedToPay:false})}/>
                </Popup> */}
                </div>
              </div>
              <hr/>
              <Popup
          open={this.state.addressopen}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({addressopen:false})}
          contentStyle={{ 
                        width:'58.9%',
                        height:'95%',
                        borderRadius:'5px',
                        marginLeft:'67%',
                        marginTop:'4%',
                        // background:'transparent'
                        }} 
          overlayStyle={{ 
            background:'transparent',
           }}
        > 
        <div >
            {/* <a className="close" onClick={this.addresscloseModal} style={{
    color: 'white',
    marginLeft: '-7%',float:'left'}}>
              &times;
            </a> */}
        {/* <UEditAddress uaddress= {uaddress}/> */}
        <CartUEditAddress uaddress= {uaddress} closeCartUEditAddr={()=>this.setState({addressopen:false})}/>
        </div>
        </Popup>
        </div>
        
          )}
          </div>
          <div class="DAadd-new-address">
               {/* <button onClick={this.openModal.bind(this)}>Add New Address</button> */}
               <button onClick={this.addAddress} >Add New Address</button>
               {/* <Popup
               lockScroll={true}
                  open={this.state.ProcedToPay}
                  onClose={()=>this.setState({ProcedToPay:false})}
                  contentStyle={{ margin:'unset',marginLeft:'68%',width:'31%',float:'right',marginTop:'4%' }}
                  >
                  <div >
                    <a className="close" onClick={()=>this.setState({ProcedToPay:false})} style={{ marginLeft:'-56%',position:'absolute' }}>
                    &times;
                    </a>
                    </div>
                    <ProceedToPay  closeProcedToPay={()=>this.setState({ProcedToPay:false})}/>
                </Popup> */}
                <Popup
              //  lockScroll={true}
                  open={this.state.OpenAddAddress}
                  closeOnDocumentClick={false}
                  onClose={()=>this.setState({OpenAddAddress:false})}
                  contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' }}
                  overlayStyle={{ background:'transparent' }}
                  >
                  <div >
                    {/* <a className="close" onClick={()=>this.setState({OpenAddAddress:false})} style={{ marginLeft:'-6%',position:'absolute' }}>
                    &times;
                    </a> */}
                    <CartAddUserAddress CloseCartAddUserAddress={()=>this.setState({OpenAddAddress:false})}/>
                    {/* <AddUserAddress/> */}
                  </div>
                </Popup>
          </div>
      </div>

    )
  }
}
const mapStateToProps = state => ({
  // getuserdetails:state.userdetailsR.getuserdetails,
  getadd:state.accountR.getadd,
  userToken:state.accountR.token,
  CartPrice:state.cartReduc.CartPrice,
})
export default connect(mapStateToProps,{Getaddress,SendAddressId,Get_Rp_Id,SelectedAddress,Editaddress})(Delivery)
