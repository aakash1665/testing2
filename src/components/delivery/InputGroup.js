import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
import './InputGroup.css'
const InputGroup =({
    label,
    value,
    name,
    placeholder,
    type,
    onChange ,
    errors
}) => {

    return (
        <div id="DeivIGroup">
        <label htmlFor={name}>{label}</label>
        <input 
        type={type} 
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        // className={classnames('form-control ',{'is-invalid':errors})}
        required
        style={{ paddingTop:'0px' }}
        />
        {errors && <div style={{ color:'red' }}>{errors}</div>}
        {/* style={{  }} */}
    </div>
    );
  };
//   TextInputGroup.prototype= {
//       name:PropTypes.string.isRequired, 
//       type:PropTypes.string.isRequired, 
//       value:PropTypes.string.isRequired, 
//       placeholder:PropTypes.string.isRequired, 
//       onChange:PropTypes.func.isRequired, 
//       label:PropTypes.string.isRequired, 
//       errors:PropTypes.string,
//   }
//   TextInputGroup.defaultProps = {
//       type:'text'
//   }
export default InputGroup