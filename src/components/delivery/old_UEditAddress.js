import React, { Component } from 'react'
import UserInputGroup from './UserInputGroup';
// import {AddAddressAction} from '../../actions/addressAction';
import { connect } from 'react-redux';
import './useraddress.css'
import {EdituserAddressAction,Editaddress} from '../../actions/accountAction';
import axios from 'axios'
import config from 'react-global-configuration'
class UEditAddress extends Component {
    state ={
      
        address_id:this.props.editadd.address_id,
        fullname :this.props.editadd.rec_name,
        landmark:this.props.editadd.landmark,
        phone_no:this.props.editadd.phone_no,
        pincode:this.props.editadd.pincode,
        address1:this.props.editadd.address,
        city_name:'',
        state_name:'',
        title:this.props.editadd.title,
        errors:{},
        is_primary:this.props.editadd.is_primary,
        addaddressopen:false,
    };
    // onaddressChanged = e => {this.setState({[e.target.name]:e.target.value})}
    // changecheck=()=>{
    //   this.setState({is_primary:!this.state.is_primary})
    // }
    closedone=()=>{
      console.log(this.props.closemodule);
    }
    EditDone=()=>{
      if(this.state.errors==={}){
       
        window.location.href = `/customer/customer_account`
       }
 
    }
    
  addaddresscloseModal=()=>{
    
    this.setState({ addaddressopen: false })
   }
onSubmit =(e)=>{
    // alert(e);
    e.preventDefault();
    const {pincode,phone_no,fullname,state,landmark,address1,address_id,is_primary,city_name,errors,state_name,title} = this.state;

    if(isNaN(phone_no)){
        this.setState({errors:{phone_no:"Enter number"}});
        return;
    }
    if(isNaN(pincode)){
      this.setState({errors:{pincode:"Enter Pincode"}});
      return;
  }

    if(phone_no.length !== 10){

        this.setState({errors:{phone_no:"Contact number must be 10 digits"}
      });
        return;
    }

    // if(pincode.toString().length !== 6){
    //     this.setState({errors:{pincode:`Sorry, we could not locate this pincode.` }});
    //     return;
    // }

    const address={
        data :{
        "address_id":address_id,
        "title":title,
        "rec_name":fullname,
        "address":address1,
        "landmark":landmark,
        "phone_no":phone_no,
        "pincode":pincode,
        "city_name":city_name,
        "state_name":state_name,
        "country_name":'IND',
        "is_primary":is_primary,
        // errors,
        }
    }
    const token= `Token ${this.props.userToken}`
    this.props.EdituserAddressAction(token,address)
    // this.setState({
    //     addressid:'',
    //     fullname :'',
    //     landmark:'',
    //     phone_no:'',
    //     pincode:'',
    //     city:'',
    //     state:'',
    //     title:'',
    //     address1:'',
    //     errors:{}
    // })
    window.location.href = `/customer/customer_account`
}

    onChange = e =>{ this.setState({[e.target.name]:e.target.value});}
  
  render() {
    // console.log(this.props.editadd)
    const {fullname,landmark,phone_no,address1,city_name,state_name,pincode,title,errors,is_primary} = this.state;
    let PincodeErr=""
    let PhoneErr=""
    if(pincode.toString().length === 6){
    
      axios.get(`${config.get('apiDomain')}/pincode/pin_check/${this.state.pincode}/`,
      )
      .then(res=>this.setState({city_name:res.data[0].districtname,state_name:res.data[0].statename}))
      .catch(err=>console.log(err)
      )
      PincodeErr=""
    }
    // For pincode validation
    else {
      PincodeErr=" Please enter 6 digits pincode"
    }
  //   For Phone Number validation
    if(phone_no.length !== 10){
      //   alert("stop")
     PhoneErr = "Please enter 10 digits mobile number"
      // document.getElementsById("mobile").disable=true
      // this.mobilechecker()
  }else{
       PhoneErr=""
   }

  
    return (
        <div class="container-1" style={{marginRight:'3%',marginLeft:'3%' }}>
        <div id="addressdiv">
        <div id="Add-New-Address">Edit Address
        </div></div>
        <br/><br/><br/>
        {/* <div class="Layer-4">
           <div class="address">
            <p class="user-name">SOMETHING</p>
            <button class="home-button"><span class="home-button-text">Home</span></button>
            <p class="address-info">Narayanpur, Rajarhat, Kolkata 700136, West Bengal</p>
            <p class="edit">Edit</p>
           </div>
           <div class="select"> 
            <button class="Layer-10"><span class="select-button">Select</span></button>
           </div>
        </div> */}
        {/* <hr/> */}
        {/* <div class="Layer-4">
          <div class="address">
            <p class="user-name">SOMETHING</p>
            <button class="home-button"><span class="home-button-text">Home</span></button>
            <p class="address-info">Narayanpur, Rajarhat, Kolkata 700136, West Bengal</p>
            <p class="edit">Edit</p>
           </div>
           <div class="select"> 
            <button class="Layer-10"><span class="select-button">Select</span></button>
           </div>
        </div> */}
        <form onSubmit={this.onSubmit}>

        <UserInputGroup
                    name="fullname"
                    // label="Name"
                    placeholder="Full Name"
                    value={fullname}
                    onChange={this.onChange}
                    errors={errors.fullname}
                    />
        <UserInputGroup
                    name="address1"
                    // label="Name"
                    placeholder="Address 1"
                    value={address1}
                    onChange={this.onChange}
                    errors={errors.name}
                    />
        <UserInputGroup
                    name="landmark"
                    // label="Name"
                    placeholder="Landmark"
                    value={landmark}
                    onChange={this.onChange}
                    errors={errors.landmark}
                    />
        <UserInputGroup
               
                    name="phone_no"
                    // label="Name"
                    placeholder="Mobile Number"
                    value={phone_no}
                    onChange={this.onChange}
                    errors={errors.phone_no}
                    maxLength="10"
                    />
                                         <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,fontWeight:'lighter',height:'5%',lineHeight:'0px'}}>{PhoneErr}</p>
        <UserInputGroup 
                    name="pincode"
                    // label="Name"
                    placeholder="Pincode"
                    value={pincode}
                    onChange={this.onChange}
                    errors={errors.pincode}
                    maxLength="6"
                    />
                                        <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,fontWeight:'lighter',height:'5%',lineHeight:'0px'}}>{PincodeErr}</p>
        <UserInputGroup
                    name="city"
                    // label="Name"
                    placeholder="City"
                    value={city_name}
                    onChange={this.onChange}
                    errors={errors.name}
                    />
        <UserInputGroup
                    name="state"
                    // label="Name"
                    placeholder="State"
                    value={state_name}
                    onChange={this.onChange}
                    errors={errors.name}
                    />

                    {/* For future use  */}
        {/* <input type="Checkbox" checked={this.state.is_primary} name="setdefault" style={{width:'13px',marginTop:'3%'}}  onChange={this.changecheck}/><span style={{    marginLeft: '6%',}}>Set As Default</span>  */}
        {/* <InputGroup
                    name="title"
                    // label="Name"
                    placeholder="Address Title"
                    value={title}
                    onChange={this.onChange}
                    errors={errors.name}
                    /> */}
                    <br/>
                    <label style={{marginTop:'2%'}}>Address Title</label>
        <div>            
        <input type="radio" value="Home" name="title" style={{width:'13px'}}  onChange={this.onChange} checked={(title==="Home")}/><span style={{    marginLeft: '6%',}}>Home</span>
        <input type="radio" value="Office" name="title" style={{width:'13px',    marginLeft: '16%'}} onChange={this.onChange} checked={(title==="Office")} /> <span style={{marginLeft: '6%',}}>Office</span>
        </div>
        <input type="submit" id="donebtn_button"  onClick={this.closedone} value="Done"/> 
         </form>
    </div>
// onClick={this.EditDone}
    )
  }
}

const mapStateToProps = state => ({
    getuseradd:state.accountR.getuseradd,
    userToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
    editadd:state.accountR.editadd,
  })


export default connect(mapStateToProps,{EdituserAddressAction,Editaddress,})(UEditAddress);