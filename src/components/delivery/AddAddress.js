import React, { Component } from 'react'
import InputGroup from './InputGroup';
import './address.css'
import { connect } from 'react-redux';
import {addAddressAction} from '../../actions/accountAction';
class AddAddress extends Component {
    state ={
        fullname :'',
        landmark:'',
        phone_no:'',
        pincode:'',
        address1:'',
        address2:'',
        city:'',
        state:'',
        title:'',
        errors:{}
    };
    

onSubmit =(e)=>{
    alert("full");
    e.preventDefault();
    const {pincode,phone_no,fullname,state,landmark,address1,address2} = this.state;

    if(isNaN(phone_no)){
        this.setState({errors:{phone_no:"Enter number"}});
        return;
    }
    if(isNaN(pincode)){
      this.setState({errors:{pincode:"Enter Pincode"}});
      return;
  }

    if(phone_no.length !== 10){
        this.setState({errors:{phone_no:"Contact number must be 10 digits"}});
        return;
    }
    if(pincode.length !== 6){
        this.setState({errors:{pincode:"Pincode must be 6 digits"}});
        return;
    }

    // "title" : "test_final",
    // "rec_name" : "aayush_test",
    // "pincode" : 12345,
    // "address" : "test",
    // "landmark" : "test",
    // "phone_no" : 123456789,
    // "state_name" : "test",
    // "city_name" : "test",
    // "country_name" : "test",
    // "is_primary" : "Y"

    const address={
        data :{
        title:fullname,
        rec_name:fullname,
        address:`${address1}, ${address2}`,
        landmark:landmark,
        phone_no:phone_no,
        pincode:pincode,
        city_name:'testcity',
        state_name:'wb',
        country_name:'IND',
        is_primary:'N',}
    }
    const token= `Token ${this.props.userToken}`
    this.props.addAddressAction(token,address)
    this.setState({
        fullname :'',
        landmark:'',
        phone_no:'',
        pincode:'',
        address1:'',
        address2:'',
        city:'',
        state:'',
        title:'',
        errors:{}
    })
}
    onChange = e => this.setState({[e.target.name]:e.target.value});
  render() {
    const {fullname,landmark,phone_no,address1,address2,city,state,pincode,title,errors,} = this.state;
    return (
        <div class="container-1" style={{marginRight:'3%',marginLeft:'3%'  }}>
        <div id="AddNewAddress"> Add New Address
        </div>
        {/* <div class="Layer-4">
           <div class="address">
            <p class="user-name">SOMETHING</p>
            <button class="home-button"><span class="home-button-text">Home</span></button>
            <p class="address-info">Narayanpur, Rajarhat, Kolkata 700136, West Bengal</p>
            <p class="edit">Edit</p>
           </div>
           <div class="select"> 
            <button class="Layer-10"><span class="select-button">Select</span></button>
           </div>
        </div> */}
        {/* <hr/> */}
        {/* <div class="Layer-4">
          <div class="address">
            <p class="user-name">SOMETHING</p>
            <button class="home-button"><span class="home-button-text">Home</span></button>
            <p class="address-info">Narayanpur, Rajarhat, Kolkata 700136, West Bengal</p>
            <p class="edit">Edit</p>
           </div>
           <div class="select"> 
            <button class="Layer-10"><span class="select-button">Select</span></button>
           </div>
        </div> */}
        <form onSubmit={this.onSubmit}>

        <InputGroup
                    name="fullname"
                    // label="Name"
                    placeholder="Full Name"
                    value={fullname}
                    onChange={this.onChange}
                    errors={errors.fullname}
                    />
        <InputGroup
                    name="address1"
                    // label="Name"
                    placeholder="Address 1"
                    value={address1}
                    onChange={this.onChange}
                    errors={errors.name}
                    maxLength={250}

                    />
       <InputGroup
                    name="address2"
                    // label="Name"
                    placeholder="Address 2"
                    value={address2}
                    onChange={this.onChange}
                    errors={errors.name}
                    />
        <InputGroup
                    name="landmark"
                    // label="Name"
                    placeholder="Landmark"
                    value={landmark}
                    onChange={this.onChange}
                    errors={errors.landmark}
                    />
        <InputGroup
                    name="phone_no"
                    // label="Name"
                    placeholder="Mobile Number"
                    value={phone_no}
                    onChange={this.onChange}
                    errors={errors.phone_no}
                    />
        <InputGroup
                    name="pincode"
                    // label="Name"
                    placeholder="Pincode"
                    value={pincode}
                    onChange={this.onChange}
                    errors={errors.pincode}
                    />
        <InputGroup
                    name="city"
                    // label="Name"
                    placeholder="City"
                    value={city}
                    onChange={this.onChange}
                    errors={errors.name}
                    />
        <InputGroup
                    name="state"
                    // label="Name"
                    placeholder="State"
                    value={state}
                    onChange={this.onChange}
                    errors={errors.name}
                    />
        <input type="Checkbox" value="setDefault" name="setdefault" style={{ width:'10px' }}/> Set As Default
        <InputGroup
                    name="title"
                    // label="Name"
                    placeholder="Address Title"
                    value={title}
                    onChange={this.onChange}
                    errors={errors.name}
                    />
        <div>            
        <input type="radio" value="Home" name="addresstype" style={{ width:'10px' }}/> Home
        <input type="radio" value="Office" name="addresstype" style={{ width:'10px' }}/> Office
        </div>
        <div class="add-new-address">
             <button type="submit">Done</button>
        </div>
        </form>
    </div>

    )
  }
}
const mapStateToProps = state => ({
    searchBook:state.homeR.searchBook,
    userToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
  })
// export default (AddAddress)

export default connect(mapStateToProps,{addAddressAction})(AddAddress);