import React, { Component } from "react";
import Button from "@material-ui/core/Button";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import TextField from "@material-ui/core/TextField";
import {
  getTotalSaleFilterThisMonth,
  getTotalSaleFilterByDateRange
} from "../../actions/totalsalefilterAction";

import { Dialog } from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";

import { getSeoData } from "../../actions/seodataAction";
import { Helmet } from "react-helmet";

class TotalSaleFilter extends Component {
  static propTypes = {
    sales: PropTypes.object.isRequired,
    getTotalSaleFilterThisMonth: PropTypes.func.isRequired,
    getTotalSaleFilterByDateRange: PropTypes.func.isRequired,
    SaleThisMonth: PropTypes.object.isRequired,
    SeoData: PropTypes.object.isRequired,
    getSeoData: PropTypes.func.isRequired
  };
  state = {
    signal: false,
    books: "",
    amount: "",
    status: "",
    startDate: "2017-05-24",
    endDate: "2017-05-24",
    startTime: "12:30",
    endTime: "17:30",
    token: "",
    password:"",
    Sessionpassword:"",
    openpasswordDialog:true
  };

  onChangeStartDate = e => {
    this.setState({
      startDate: e.target.value
    });
  };

  onChangeEndDate = e => {
    this.setState({
      endDate: e.target.value
    });
    // console.log("The token in enddate ", localStorage.getItem("user"));
  };

  onChangeStartTime = e => {
    this.setState({
      startTime: e.target.value
    });
  };

  onChangeEndTime = e => {
    this.setState({
      endTime: e.target.value
    });
  };

  onClickDateRangeHandler = () => {
    this.setState({
      token: localStorage.getItem("user")
    });
    this.props.getTotalSaleFilterByDateRange(
      localStorage.getItem("user"),
      Math.floor(
        new Date(this.state.startDate + " " + this.state.startTime).getTime() /
          1000
      ),
      Math.floor(
        new Date(this.state.endDate + " " + this.state.endTime).getTime() / 1000
      )
    );
  };

  componentDidMount() {
    this.setState({ token: localStorage.getItem("user") });
    this.props.getTotalSaleFilterThisMonth(localStorage.getItem("user"));
    if(sessionStorage.getItem('password')){
      this.setState({openpasswordDialog:false})
    }
    // this.props.getSeoData(
    //   "https://www.mypustak.com/category/competitive-exams/engineering/"
    // );
    // console.log(this.props, "In componentDidMount");
  }

  constructor(props) {
    super(props);
    this.props.getSeoData(
      "https://www.mypustak.com/category/competitive-exams/engineering/"
    );
  }

  onClickHandler = () => {
    this.props.getTotalSaleFilterThisMonth(localStorage.getItem("user"));
    // console.log(this.props, "In click Buttton and the button is clicked!!");
    this.setState({
      signal: !this.state.signal,
      books: this.props.sales["Total_Books"],
      amount: this.props.sales["Total_amount"],
      status: this.props.sales["status"]
    });
  };

  handelOnChnagegetPassword =(e)=>{
    this.setState({password:e.target.value})
  }
  getPassword =(e)=>{
    e.preventDefault();
    console.log(e.target,"in");
    
    if(this.state.password == 123){
      console.log("entered");
      
      sessionStorage.setItem('password',this.state.password)
      this.setState({openpasswordDialog:false})
    }
  }

  render() {
    const data = this.props.sales;
    const seoData = this.props.SeoData;
    // console.log("The token is ", this.state.token);
    // console.log("seodata are ", seoData);
    // console.log("seodata length is ", seoData.length);

    return (
      <React.Fragment>
        <Helmet>
          <title>{seoData.title_tag}</title>
          <meta name="description" content={seoData.meta_desc} />
        </Helmet>
        {localStorage.getItem("user") ? (
          <div>
            <form noValidate>
              <TextField
                id="startDate"
                label="From"
                type="date"
                defaultValue="2017-05-24"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onChange={this.onChangeStartDate}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                id="startTime"
                type="time"
                defaultValue="12:30"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap"
                }}
                onChange={this.onChangeStartTime}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                id="endDate"
                label="To"
                type="date"
                defaultValue="2017-05-24"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onChange={this.onChangeEndDate}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                id="endTime"
                type="time"
                defaultValue="17:30"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap"
                }}
                onChange={this.onChangeEndTime}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <Button
                variant="contained"
                color="primary"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onClick={this.onClickDateRangeHandler}
              >
                Get Total Sale
              </Button>
              <br />
            </form>

            {this.state.signal && this.state.status == 200 ? (
              <div>
                <h1>Total No. Books This month : {data["Total_Books"]}</h1>
                <h1>Total amount This month : {data["Total_amount"]}</h1>
              </div>
            ) : this.state.signal && this.state.status != 200 ? (
              <p>Data fetching....</p>
            ) : null}
            <form noValidate autoComplete="off">
              <Button
                variant="contained"
                color="secondary"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onClick={this.onClickHandler}
              >
                This Month Transactions
              </Button>
            </form>

            {/* <Dialog
            open ={this.state.openpasswordDialog}
            aria-labelledby="form-dialog-title"
            size="lg"
            >
                <DialogTitle id="form-dialog-title">Enter Password</DialogTitle>
                <form onSubmit={this.getPassword}>
                <DialogContent>
                  <input type="password" 
                  placeholder="*********" 
                  value={this.state.password} 
                  onChange={this.handelOnChnagegetPassword}></input>
                </DialogContent>
                <DialogActions>
                <div style={{ textAlign:'center' }}>
                    <Button color="primary" 
                    variant="contained"  
                    style={{ justifyContent:'start' }}
                    type="submit" 
                    >
                      Enter
                    </Button>
                </div>
                </DialogActions>
                </form>
            </Dialog> */}

          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  sales: state.totalsalefilter.sales,
  SaleThisMonth: state.totalsalefilter.salethismonth,
  SeoData: state.seodata.seodata
});

export default connect(
  mapStateToProps,
  { getTotalSaleFilterThisMonth, getTotalSaleFilterByDateRange, getSeoData }
)(TotalSaleFilter);
