import React from 'react'
import ReactDOM from 'react-dom'
import scriptLoader from 'react-async-script-loader';
class PayPalButton extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showButton: true,
        };

        window.React = React;
        window.ReactDOM = ReactDOM;
    }
    componentDidMount() {
        const {
            isScriptLoaded,
            isScriptLoadSuccess
        } = this.props

        if (isScriptLoaded && isScriptLoadSuccess) {
            this.setState({
                showButton: true
            });
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            isScriptLoaded,
            isScriptLoadSuccess
        } = nextProps;

        const isLoadedButWasntLoadedBefore = 
        !this.state.showButton && !this.props.isScriptLoaded && isScriptLoaded;

        if(isLoadedButWasntLoadedBefore){
            if(isScriptLoadSuccess){
                this.setState({showButton:true})
            }else {
                console.log('Cannot load Paypal script!');
                this.props.onError();
              }
        }
    }
    render() {
        const paypal = window.PAYPAL
        const {
            total,
            currency,
            env,
            commit,
            client,
            onSuccess,
            onError,
            onCancel
        } = this.props

        const {
            showButton
        } = this.state

        const payment = () =>
                paypal.rest.payment.create(env,client,{
                transactions:[
                    {
                        ammount : {
                            total,
                            currency,
                        }
                    }
                ]
            });

        const onAuthorize = (data,actions) =>
            actions.payment.execute()
                .then(() => {
                    const payment = {
                        paid:true,
                        cancelled:false,
                        payerID:data.payerID,
                        paymentID:data.PaymentID,
                        paymentToken:data.paymentToken,
                        returnUrl:data.returnUrl,
                    }
                    onSuccess(payment);
                });

                const Button = window.paypal.Buttons.driver("react", {
                    React,
                    ReactDOM,
                });
                
        return ( 
            <div>
                 <Button
                    env = {env}
                    client = {client}
                    commit = {commit}
                    payment = {payment}
                    onAuthorize = {onAuthorize} 
                    onCancel = {onCancel}
                    onError = {onError}
                    style= {{ 
                        layout:  'horizontal',
                        color:   'blue',
                        shape:   'rect',
                        label:   'paypal'
                      }}
                />
            </div>
        );
    }
}
export default PayPalButton