import React, { Component } from 'react'
import './DonationThanks.css'
import { connect } from 'react-redux';
import MainHeader from '../../MainHeader';
import MainFooter from '../../MainFooter';
import {SetPickup} from '../../../actions/donationActions';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import axios from 'axios'
import config from 'react-global-configuration'
import MediaQuery from 'react-responsive';
import ReactGA from 'react-ga';

class DonationThanks extends Component {

  
  backToHome(){

    window.location = `/donate`
    
      }
      componentDidMount(){
    ReactGA.pageview(window.location.pathname + window.location.search);

        this.props.SetPickup()
        if(this.props.donation_req_id !== 0){
          localStorage.setItem('UserDonationId',this.props.donation_req_id);
        // }    
        // if(this.props.donation_req_id.length !== 0){
          // localStorage.setItem('UserOrderId',this.props.donation_req_id);
    
          // "donor_name":donor_name,
          // "address":`${this.state.address1 } ${this.state.address2}`,
          // "state":this.state.state_name,
          // "source":Know_aboutData,
          // "category":Cdata,
          // "city":this.state.city_name,
          // "mobile":phone,
          // "landmark":landmark,
          // "country":"India",
          // "zipcode":zipcode,
          // "no_of_book":no_of_book,
          // "no_of_cartons":no_of_cartons,
          // "app_books_weight":app_books_weight,
          // "pickup_date": `${this.state.startDate}`



          const Data={ 
          donationId:this.props.donation_req_id,
          // source:this.props.sendMailData.data.Know_aboutData,
          // category:this.props.sendMailData.data.Cdata,
          // city:this.state.city_name,
          // mobile:this.props.sendMailData.data.phone,
          // landmark:this.props.sendMailData.data.landmark,
          // country:this.props.sendMailData.data.India,
          // zipcode:this.props.sendMailData.data.zipcode,
          // no_of_book:this.props.sendMailData.data.no_of_book,
          // no_of_cartons:this.props.sendMailData.data.no_of_cartons,
          // app_books_weight:this.props.sendMailData.data.app_books_weight,
          // pickup_date: `${this.state.startDate}`,
          // totalPayment:this.props.sendMailData.amount,
          // donationPickup:this.props.sendMailData.pickupType,
          email:this.props.UserEmail,
        }
    
        
    
          axios.post(`${config.get('apiDomain')}/common/donationEmail/`,Data,
        //   {headers:{'Authorization': `Token ${this.props.userToken}`
        // }}
        )
          .then(res=>{console.log(res,Data);
            window.location.reload();
          
          })
          .catch(err=>console.log(err,Data) 
          )
          }
      }
  
  render() {

    const id = this.props.donation_req_id
    console.log(this.props.UserEmail);

    return (
      
      <div>
        
  
      <MainHeader/>
      <div id="thanksbody">
          <img id="handshake"  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ThankYou.77288eb6.png`} alt=""/>
          {/* <img id="A_iconimg" /> */}
          {/* <br/> */}
        <p id="l1_thanks" className="center" >Thank You For Your Donation Request </p>
        <p id="l2_we"  className="center">We Have Successfully Recieved Your Donation Request. </p>
        <p className="center"><span id="part1">Your Donation Request Id Is:</span>
                       <span id="part2"><b>{(this.props.donation_req_id == 0)?localStorage.getItem("UserDonationId"):this.props.donation_req_id}</b>(Please Note For Future Reference)</span>
        </p>
        <p className="center" style={{ fontSize:'90%' }}>Someone From MyPustak Team Will Contact You Very Soon To Schedule Your pick Up.  </p>
        {/* <hr/> */}
        {/* <p className="center" style={{ fontSize:'90%' }}>Free Pick Ups Will Be Done On First Come First Serve basis.
             We Will Give our Best To Schedule Your Pick Up As Soon As Possible.</p>
        <p className="center" style={{ fontSize:'110%' }}>For Immediate Pick Ups,
         Pay Nominal Shipping Cost To Schedule Your Instant Pick Up.</p>
        <button id="procced" >Procced</button>
        <hr/> */}
        {/* <p style={{ textAlign:'center' ,fontSize:'100%'}} >How Did You Know About Us?</p>
        <MediaQuery minWidth={768}>
        <form id="how">
        <input type="checkbox" style={{ width:'2%', paddingLeft:'10%',paddingTop: '0%',marginRight:'1%' }}value="Quora"/>
            <label htmlFor="Quora" style={{  paddingRight:'1%'}}>Quora </label> 

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%',marginRight:'1%'}}value="Google"/>
            <label htmlFor="Google" style={{  paddingRight:'1%'}}>Google </label>

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%',marginRight:'1%' }} value="Facebook"/>
            <label htmlFor="Facebook" style={{ paddingRight:'1%' }}>Facebook </label>

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%' ,marginRight:'1%'}}value="Yahoo"/>
            <label htmlFor="Yahoo" style={{  paddingRight:'1%' }}>Yahoo </label>

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%',marginRight:'1%' }} value="Twitter"/>
            <label htmlFor="Twitter" style={{  paddingRight:'1%' }}>Twitter </label>
            <input type="submit" id="thankssubmit" value="Submit"/>
        </form>
        </MediaQuery> */}

        {/* <MediaQuery maxWidth={767}>
        <form id="how">
            <input type="checkbox" style={{ width:'5%', paddingLeft:'10%',paddingTop: '0%',marginRight:'1%' }}value="Quora"/>
            <label htmlFor="Quora" style={{  paddingRight:'1%'}}>Quora </label> 
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%',marginRight:'1%'}}value="Google"/>
            <label htmlFor="Google" style={{  paddingRight:'1%'}}>Google </label>
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%',marginRight:'1%' }} value="Facebook"/>
            <label htmlFor="Facebook" style={{ paddingRight:'1%' }}>Facebook </label>
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%' ,marginRight:'1%'}}value="Yahoo"/>
            <label htmlFor="Yahoo" style={{  paddingRight:'1%' }}>Yahoo </label>
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%',marginRight:'1%' }} value="Twitter"/>
            <label htmlFor="Twitter" style={{  paddingRight:'1%' }}>Twitter </label>
            <br/>
            <input type="submit" id="thankssubmit" value="Submit"/>
        </form>
        </MediaQuery>      */}
           <hr/>
        {/* <p style={{fontSize:'110%',marginLeft:'20%'  }}>You Can Track Your Donation Request Status From */}
        <p style={{fontSize:'110%',textAlign:'center'  }}>You Can Track Your Donation Request Status From

        <Link to="/donor/donor_donation_request" style={{ textDecoration:"none" }}><span style={{ color:'#d15714' }}> Donation Account.</span></Link></p>
         <Link to="/" style={{ textDecoration:"none" }}><button id="back" >Back To Home</button></Link>
         
      </div>
      <MainFooter/>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  donation_req_id:state.donationR.donation_req_id,
  sendMailData:state.donationR.sendDonationMailData,
  // app_books_weight:state.donationR.app_books_weight,
  // pickup:state.donationR.pickup,
  // name:state.donationR.name,
  UserEmail:state.userdetailsR.getuserdetails.email,
  // pickup:state.donationR.pickup,
  // userId:state.accountR.userId,
  // RAZORPAY:state.donationR.rp_id,
})
export default connect(mapStateToProps,{SetPickup})(DonationThanks)