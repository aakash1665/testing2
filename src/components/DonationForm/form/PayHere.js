import React, { Component } from 'react'
import './payhere.css'
import { connect } from 'react-redux';
class PayHere extends Component {

    state={
        card:false,
        upi:false,
        netBanking:false,
        paytm:false,
        cash:false,
        QR:false,
      };

      // Getting data id weight from url ,
      // name,email from the reducers
    donation_req_id =this.props.donation_req_id
    app_books_weight = this.props.app_books_weight
    name=this.props.name
    email=this.props.email

    Changecard=(e,donation_req_id)=>{
        this.setState({card:true});  
        this.setState({upi:false});
        this.setState({netBanking:false});
        this.setState({paytm:false});
        this.setState({cash:false});
        this.setState({QR:false});
        
        var upi = document.getElementById("middle")
    // alert(donation_req_id)
    }
    Changeupi=(e)=>{
        this.setState({upi:true});  
        this.setState({card:false});
        this.setState({netBanking:false});
        this.setState({paytm:false});
        this.setState({cash:false});
        this.setState({QR:false});
    }
    ChangecnetBanking=(e)=>{
        this.setState({netBanking:true});  
        this.setState({card:false});
        this.setState({upi:false});
        this.setState({paytm:false});
        this.setState({cash:false});
        this.setState({QR:false});
    }
    Changepaytm=(e)=>{
        this.setState({paytm:true});  
        this.setState({card:false});
        this.setState({upi:false});
        this.setState({netBanking:false});
        this.setState({cash:this.state.cash,});
        this.setState({QR:false});
    }
    Changecash=(e)=>{
        this.setState({cash:true});  
        this.setState({card:false});
        this.setState({upi:false});
        this.setState({paytm:false});
        this.setState({netBanking:false});
        this.setState({QR:false});
    }
    ChangeQR=(e)=>{
        this.setState({QR:true});  
        this.setState({card:false});
        this.setState({upi:false});
        this.setState({paytm:false});
        this.setState({cash:false});
        this.setState({netBanking:false});
    }

    //Calculating the price
    greater(wt){
        const new_wt= 20*wt
        const val = 200
       return (new_wt>val)?new_wt:val
        
        }

        options = {
            "key": "rzp_live_pvrJGzjDkVei3G", //Paste your API key here before clicking on the Pay Button. 
            "name": `${this.props.name}`,
            "amount" : `${this.greater(this.props.wt)}00`,
            
            "currency" : "INR",
            "description": "Test Description",

            "handler": function (response){    
                alert(response.razorpay_payment_id)
                                          },

            "prefill": {

                "contact" : '999999999',
                "email" : `${this.props.email}`
            },
                
            "notes": {
                "Order Id": "order123",
                "address" : "customer address to be entered here"
            },
            "theme": {
                "color": "#1c92d2",
                 "emi_mode" : true
                        
            },
            
        //        external: {src
        //    wallets: ['mobikwik' , 'paytm' , 'jiomoney' , 'payumoney'],
        //     handler: function(data) {
        //     console.log(this, data)
        //     }
        //   }
        };
    

        componentDidMount(){
            this.rzp1 = new window.Razorpay(this.options);
            }

        
  render() {
    const wt = this.props.match.params.wt
    const id = this.props.match.params.id
    
    const price = this.greater(wt)
    const name = this.name
    const email = this.email
    /// New code
    

   
    // document.getElementById('rzp-button1').onClick = function(e){
    //     this.rzp1.open();
    //     e.preventDefault();
    // }





    /// end New code
    return (

        
      <div style={{ zIndex:'19999' }}>
        <div id="payhereBody" style={{ zIndex:'19999' }}>
            <div id="first"> {/*paymentMethod*/}
                <p id="first_title"><b>Please Select Your Payment Method</b></p>
                
                {/* ()=openifarame=>{ <iframe >
                <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
                
               
                </iframe> */}
}
                <button className="leftbtns" style={{ marginTop:'5%' }} name="card" 
                onClick={()=>{this.rzp1.open();}}
                // onClick={()=>{this.openifarame()}}
                id={(this.state.card)?"changeColor":"color"} >
                <img  src={require('./debit_&_credit_card.png')} alt="" style={{ float:'left' }}/>
                <p style={{ marginTop:'4%'}}>Credit/Debit Card</p>
                </button>

                <button className="leftbtns" style={{ marginTop:'5%',marginLeft:'16%' }} 
                id={(this.state.upi)?"changeColor":"color"}  onClick={()=>{this.rzp1.open();}}>
                <img  src={require('./UPI.png')} alt="" style={{ float:'left',marginLeft:'30%' }}/>
                <p style={{float:'left', marginTop:'4%' }}>UPI</p>
                </button>
                
                <button className="leftbtns" style={{ marginTop:'10.5%' }} 
                onClick={()=>{this.rzp1.open();}}
                id={(this.state.netBanking)?"changeColor":"color"} onClick={this.ChangecnetBanking}>
                <img  src={require('./NetBanking.png')} alt="" style={{ float:'left',marginLeft:'15%' }}/>
                <p style={{ marginTop:'4%' ,float:'left'}}>Net Banking</p>
                </button>

                <button className="leftbtns" style={{ marginTop:'10.5%',marginLeft:'16%' }}
                onClick={()=>{this.rzp1.open();}}
                 id={(this.state.paytm)?"changeColor":"color"} >
                <img  src={require('./paytm_wallet.png')} alt="" style={{ float:'left',marginLeft:'10%' }}/>
                <p style={{marginTop:'4%',float:'left' }}>Paytm Wallet</p>
                </button>

                <button className="leftbtns" style={{ marginTop:'16%' }} 
                id={(this.state.cash)?"changeColor":"color"} onClick={this.Changecash}>
                <img  src={require('./Cash_On_delivery.png')} alt="" style={{ float:'left' }}/>
                <p style={{ marginTop:'4%' ,float:'left'}}>Cash On Delivery</p>
                </button>
                
                <button className="leftbtns" style={{ marginTop:'16%',marginLeft:'16%' }} 
                id={(this.state.QR)?"changeColor":"color"} onClick={this.ChangeQR}>
                <img  src={require('./QR_Code.png')} alt="" style={{ float:'left',marginLeft:'10%' }}/>
                <p style={{marginTop:'4%',float:'left' }}>QR Code</p>
                </button>

                <div id="leftlow"> 
                <button style={{ 
                            width:'35%',height: '14%',
                            position: 'absolute',
                            borderColor: '#00baf2',
                            borderStyle: 'solid',
                            borderRadius: '5px',
                            marginLeft:'31%',
                            marginTop:'2%',
                            /* color: #00baf2; */
                            // display: inline-block;
                            // padding-top: .7%;
                            // margin: 1.6%;
                            outline: '0',
                            cursor: 'pointer'

                     }}>Click Here</button>
                    <p style={{ color:' #00baf2',marginLeft:'15%' ,paddingTop:'3%'}}>
                    {/* <span style={{ fontWeight:'bold',fontSize:'110%',cursor:'pointer' }} onClick={()=>alert('clicked')} >&#8226; <input type="radio"/></span>    */}
                <br/>
                    <b>Other Available Payment Wallets</b>
                    <img  src={require('./Wallet.png')} alt=""/>
                    </p>
                    <div style={{ marginLeft:'20%' }}>
                    <img  src={require('./Mobiquick.png')} style={{ marginRight:'10%' }} alt=""/>
                    <img  src={require('./FreeCharge.png')} style={{ marginRight:'10%' }} alt=""/>
                    <img  src={require('./Amazon_Pay.png')} style={{ marginRight:'10%' }} alt=""/>
                    </div>

                    <div style={{ marginLeft:'20%' }}>
                    <img  src={require('./OlaMoney.png')} style={{ marginRight:'10%' }} alt=""/>
                    <img  src={require('./MPesa.png')} style={{ marginRight:'10%' }} alt=""/>
                    <img  src={require('./jioMoney.png')} style={{ marginRight:'10%' }} alt=""/>
                    </div>
                    <div style={{ marginLeft:'20%' }}>
                    <img  src={require('./AirTelMoney.png')} style={{ marginRight:'10%' }} alt=""/>
                    <img  src={require('./SBIwallet.png')} style={{ marginRight:'10%' }} alt=""/>
                    <img  src={require('./PayZapp.png')} style={{ marginRight:'10%' }} alt=""/>
                    </div>
                </div>

            </div>
            <div id="middle" style={{ }}>
            {/* All the data  */}
            id is :- {id}<br/>
            Rs:- {price}<br/>
            Name :- {name}<br/>
            Email :- {email}
            <div id="DonationCheckout"></div>
            </div>
            {/* Button to be clicked */}
            {/* <button id="rzp-button1" onClick={()=>{this.rzp1.open();}} ><i class="fas fa-money-bill"></i> Pay with Razorpay</button> */}
            {/* <div id="orderDetails">
            <div id="tophalf">
            <p id="tophalf_title">Pick Up Address</p>
            </div>
            <div></div>
            </div> */}
      </div>
      </div>
    )
  }
}
// export const id= {id}
const mapStateToProps = state => ({
    donation_req_id:state.donationR.donation_req_id,
    app_books_weight:state.donationR.app_books_weight,
    // pickup:state.donationR.pickup,
    name:state.donationR.name,
    email:state.donationR.email,
  })
//   export default PayHere
export default connect(mapStateToProps)(PayHere)