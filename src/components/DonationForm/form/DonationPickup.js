import React, { Component } from 'react'
import './DonationPickup.css'
// import {ChangeLogin} from '../../../action/donationActions'
// import {Get_Rp_Id} from '../../../action/donationActions'
import {Get_Rp_Id,ChangeLogin,DonationMailData} from '../../../actions/donationActions';
import { connect } from 'react-redux';
import axios from 'axios'
import config from 'react-global-configuration'
// import { BrowserRouter as Router, Route, Link } from "react-router-dom"
// import {  } from 'react-router'
// import { browserHistory,push } from 'react-router-redux';
import { Link,Redirect,browserHistory } from 'react-router-dom'
// import {Helmet} from "react-helmet";
import Donationform from './Donationform';
// import DonationCheckout from '../layout/DonationCheckout';

class DonationPickup extends Component {

  donation_req_id =this.props.donation_req_id
  app_books_weight = this.props.app_books_weight
  name=this.props.name
  email=this.props.email

  greater(wt){
    const new_wt= 20*wt
    const val = 150
   return (new_wt>val)?new_wt:val
    // return (new_wt>val)?2:1

    }

    state={
      razorpay_payment_id:"",
      RedirectToDonaThanks:false,
      SendRzPay:"",
    }
   
    CallOut(a){
      // alert(a)
    }
  //   postTo(url, query) {
  //     var request = (XMLHttpRequest?new XMLHttpRequest():new ActiveXObject());
  //     request.open('POST', url, true);
  //     request.send(query);
  // }
    //New Code For payment Gateway


    //********************** */ CODE SEND BY RAZORPAY  */********************** */

    //        var options = {
    //             "key": "YOUR_KEY_ID",
    //             "amount": "2000", // 2000 paise = INR 20
    //             "name": "Merchant Name",
    //             "description": "Purchase Description",
    //             "image": "/your_logo.png",               
    //                 "order_id": "Razorpay_order_id",
    //             "handler": function (response){
    //                 alert(response.razorpay_payment_id);
    //                     alert(response.razorpay_order_id);
    //                     alert(response.razorpay_signature);
    //             },
    //             "prefill": {
    //                     "name": "Harshil Mathur",
    //                     "email": "harshil@razorpay.com",
    //                                 "phone": "+919999999999"
    //             },
    //             "notes": {
    //                     "order_id": "your_order_id",
    //                                 "transaction_id": "your transaction_id",
    //                                 "Receipt": “your_receipt_id"
    //             },
    //             "theme": {
    //                     "color": "#F37254"
    //             }
    //     };
    // var rzp1 = new Razorpay(options);
    
    // document.getElementById('rzp-button1').onclick = function(e){
    //     rzp1.open();
    //     e.preventDefault();

    //********************** */ END OF CODE SEND BY RAZORPAY  */********************** */

    options = {
      "key": "rzp_live_pvrJGzjDkVei3G", //Paste your API key here before clicking on the Pay Button. 
      // "key": "rzp_test_jxY6Dww4U2KiSA",
      "amount" : `${this.greater(this.props.PassToPickup.app_books_weight)}00`,
      "name": `Mypustak.com`,
      "description": "Donation",
      "Order Id": `${this.props.RAZORPAY}`, //Razorpay Order id
      "currency" : "INR",
      

      "handler": (response)=>{    
          const razorpay_payment_id=response.razorpay_payment_id;
          const razorpay_order_id=response.razorpay_order_id;
          const razorpay_signature=response.razorpay_signature;
          console.log(razorpay_payment_id)
          console.log(razorpay_order_id)
          console.log(razorpay_signature)
          this.CallOut(razorpay_payment_id)
          this.setState({SendRzPay:razorpay_payment_id})
          const SendData={
            "payment_id":`${razorpay_payment_id}`,
            "payment_url":`${this.props.RAZORPAY}`,
            "payvalue":this.greater(this.props.PassToPickup.app_books_weight),
            "is_paid_donation":"Y",
          }
          // http://103.217.220.149:80/api/v1/patch/add_paymentid_donationreqtable/update-donation/7213 
           axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_donationreqtable/update-donation/${this.props.donation_req_id}`,SendData,{headers:{
          'Authorization': `Token ${this.props.userToken}`,
          'Content-Type':'application/json',
          }})
          .then(res=>{console.log('Getting Response ');this.setState({RedirectToDonaThanks:true})})
          .catch(err=>console.log(err) 
            )
                                    },

      "prefill": {
          // "method":"card,netbanking,wallet,emi,upi",
          "contact" : `${this.props.PassToPickup.mobile}`,
          "email" : `${this.props.UserEmail}`
      },
          
      "notes": {
          "Order Id": `${this.props.donation_req_id}`,
          "address" : "customer address to be entered here"
      },
      "theme": {
          "color": "#1c92d2",
           "emi_mode" : true
                  
      },
      
      external: {
     wallets: ['mobikwik', 'jiomoney'],
      handler: function(data) {
      console.log(this, data)
      // alert(data)
      }
    }
    


  };
  data={
    data:{
      "ref_id": `${this.props.userId}`,
      // "amount": `${this.greater(this.props.match.params.wt)}`,
      "amount":this.greater(this.props.PassToPickup.app_books_weight),
    }
  }
componentWillMount(){
  this.props.Get_Rp_Id(this.data)
}

  componentDidMount(){
    window.scrollTo(0, 0);
    console.log(this.props.PassToPickup);
    
      // this.props.Get_Rp_Id(this.data)
      this.rzp1 = new window.Razorpay(this.options);
      }


      onLoad(){
        // alert("okkk")
      }
  // Payment end
//   paid(id,wt){

// window.location = `/donate/payhere/${id}/${wt}`

//   }

      // postTo('http://127.0.0.1:8000/get_razorpayid','ref_id="678",amount="1"');
    // window.console.log(conv)
    // conv = JSON.parse(razorpay_payment_id)

  render() {
    const wt = this.props.ApproxWt
    // const id = this.props.match.params.id
    // alert(this.razorpay_payment_id)
    const price = this.greater(wt)
    const name = this.name
    const email = this.email
    if(this.state.RedirectToDonaThanks){
      const SendMailData={
        donation_req_id:this.props.donation_req_id,
        email:this.props.UserEmail,
        address:this.props.PassToPickup.address,
        phone:this.props.PassToPickup.mobile,
        amount:(this.state.SendRzPay)?this.greater(this.props.PassToPickup.app_books_weight):0,
        pincode:this.props.PassToPickup.pincode,
        pickupType:(this.state.SendRzPay)?"Paid Pickup":"Free Pickup"
      }
      this.props.DonationMailData(SendMailData)
      return <Redirect to='/donate/thank-you'/>
    }
    return (
      
        <div className="backdrop" id='backdropStyle'>
        {/* <button type="button" id="closePickup" onClick={this.props.ChangeLogin}>X</button> */}
       
            <div id='modalStyle'>
            {/* <button type="button" id="closePickup" onClick={this.props.ChangeLogin}>X</button> */}
            <p id='id_title'>Thank You For Donation Request! &nbsp; &nbsp;<span id="req_id">    
            Your Donation request id is {this.props.donation_req_id}</span></p>
            <p id="method">Please Select your Pickup Method</p>
              <div id="bodypart">
                <div id="m_leftpart">
                <p id="m_leftpart_t">Paid Pick Up</p>
                <p id="m_leftpart_b">You Can Pay On Pick Up Cost For:-
                      <br/>Immediate Pick Up 
                      <br/>Making it more cheaper for the next user of your valuable books.</p>

                <input type="submit" value="OK, Let Me Make The Payment." id="m_leftbtn"  
                // onClick={()=>this.paid(this.props.donation_req_id,this.props.app_books_weight)}
                onClick={()=>{this.rzp1.open();}}
                />

                {/* </form> */}
                {/* <button id="rzp-button1" onClick={this.payout}>Pay</button> */}
                {/* <DonationCheckout/> */}
                {/* <div id="DonationCheckout"><DonationCheckout/></div> */}
                {/* <div id="DonationCheckout" style={{ backgroundColor:'red' ,height:'40px' }}></div> */}
                </div>
                <div id="m_rightpart">
                <p id="m_rightpart_t">Free Pick Up</p>
                <p id="m_rightpart_b">Thank You For Showing Interest For Book 
                    Donation.<br/> Currently 2814 Forms Are In Queue.<br/>
                    Pick Up Will Be Done On First Come First 
                    Serve Basis.</p>

                   {/* <h1> { this.razorpay_payment_id }</h1> */}
                   

                <input type="submit" value="OK, I Would Like To Wait" id="m_rightbtn" onClick={()=>this.setState({RedirectToDonaThanks:true})}/>
                </div>

              </div>  
              {/*  This part is added using ::after in the css file
               <p id="p_bodyfooter">  **Do You Know MyPustak Is Paying Minimum Of Rs 600 
                    For This Shipment. You Can Fast-forward Your Book Donation 
                    Pickup By Making This Payment.</p> */}
            </div>
        </div>
      
    )}
    // else
    // return(<Donationform/>)
  // }
}

const mapStateToProps = state => ({
  donation_req_id:state.donationR.donation_req_id,
  // app_books_weight:state.donationR.app_books_weight,
  // pickup:state.donationR.pickup,
  userToken:state.accountR.token,
  name:state.donationR.name,
  UserEmail:state.userdetailsR.getuserdetails.email,
  pickup:state.donationR.pickup,
  userId:state.accountR.getuserdetails.id,
  RAZORPAY:state.donationR.rp_id,
})
// const mapStateToProps = state => ({
//   userId:state.accountR.userId
// })
//   export default PayHere
// export default connect(mapStateToProps)(PayHere)


export default connect(mapStateToProps,{ChangeLogin,Get_Rp_Id,DonationMailData})(DonationPickup)