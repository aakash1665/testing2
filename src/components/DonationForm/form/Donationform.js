import React, { Component } from 'react'
// import Header from './components/HomePage/layout/Header'
import RCG from 'react-captcha-generator';
import 'bootstrap/dist/css/bootstrap.min.css';
import Error from './Error'
import './Donationform.css'
import { connect } from 'react-redux';
import Button from './Button'
import Popup from 'reactjs-popup'
import ReactGA from 'react-ga';

import {AddDonation,ChangeLogin,DonationMailData} from '../../../actions/donationActions';
import DonationPickup from '../../DonationForm/form/DonationPickup'
// import DateTimePicker from 'react-widgets/lib/DateTimePicker'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import InputDonation from './InputDonation'
import axios from 'axios'
import config from 'react-global-configuration'
import { Link,Redirect,browserHistory } from 'react-router-dom'

import Recaptcha from "react-recaptcha";
import {Helmet} from 'react-helmet';

class Donationform extends Component {

  state = {
    donor_name: '',
    address1:'',
    address2:'',
    phone:'',
    city_name:'',
    landmark:'',
    state_name:'',
    zipcode:'',
    // books_category:'',
    donated_book_category:'',
    categories:[
      {value:'NCERT',ncrt:false,add:false},
      {value:'Novel',novels:false,add:false},
      {value:'Engineering',Engineering:false,add:false},
      {value:'Medical',Medical:false,add:false},
      {value:'School',School:false,add:false},
      {value:'Children',Children:false,add:false},
      {value:'MBA',MBA:false,add:false},
      {value:'Study package',study_package:false,add:false},
      {value:'Entrance exam',Entrance_exam:false,add:false},
      {value:'Others',Others:false,add:false},
      ],
    no_of_book:'',
    no_of_cartons:'',
    how_do_u_know_abt_us:'',
    // Facebook/Instagram, google search , friends/colleague, Quora ,others
    know_abt_us:[
      {value:'Facebook',Facebook:false,add:false},
      {value:'Instagram',Instagram:false,add:false},
      {value:'google search',google_search:false,add:false},
      {value:'friends',google_search:false,add:false},
      {value:'Quora',Quora:false,add:false},
      {value:'others',others:false,add:false},



    ],
    app_books_weight:'',
    // pickup_time:'',
 
    condition:'',
    our_condition:[
      // {value:'good',good:false,add:false},
      // {value:'very good',very_good:false,add:false},
      {value:'Almost new',Almost_new:false,add:false},
      {value:'good & readable',good_and_readable:false,add:false},
      {value:`slightly marked with no page missing`,slightly_marke_with_no_page_missing:false,add:false},
      {value:'slight damage',slight_damage:false,add:false},
    ],
    startDate: '',
    errors:{},
    captcha: '',
    SlectedKwAbt:'',
    Imgcaptcha:'',
    RedirectToPickup:false,
    conditionErr:false,
    categoryErr:false,
    know_aboutErr:false,
    Error_captcha:'',
  }
  componentDidMount() {
    ReactGA.pageview(window.location.pathname + window.location.search);

    window.scrollTo(0, 0);
  }

  handleChange(date) {
    // alert(date.getTime())
    var covdate = date.getTime()
    // alert(covdate)
    this.setState({
      startDate: covdate
    });
  }
  // result(text) {
  //   this.setState({
  //     captcha: text
  //   })
  // }
  result=(text)=>{
    this.setState({
      Imgcaptcha: text
    })
  }
  check() {
    // console.log(this.state.captcha, this.captchaEnter.value, this.state.captcha === this.captchaEnter.value)
  }
  CaptchaErr=''
  
  updateCategories=()=>{
    // return {
    //   ...this.state,
    //   donated_book_category:[this.map(category => category.id === getCategory.id ? 
    //     (category.add=!category.add,this.updateCategories()):null),...this.state.donated_book_category]
    // }
    this.setState({donated_book_category:[]})
    this.state.categories.map(category => category.add === true? 
      this.setState({donated_book_category:[...this.state.donated_book_category,[category.value]]}):null)
  }
PassToPickup= {}
  onSubmit=(e)=>{
    e.preventDefault();
      // alert("okk")

      let Carr = "";
      for (var Ckey in this.state.categories) {
        if(this.state.categories[Ckey].add === true){
          Carr+=`${this.state.categories[Ckey].value} ,`
        } 
      }
      let Cdata = Carr

      let Karr=""
      for (var Kkey in this.state.know_abt_us) {
        if(this.state.know_abt_us[Kkey].add === true){
          Karr+=`${this.state.know_abt_us[Kkey].value} ,`
        } 
      }
      let Know_aboutData = Karr
      console.log(Know_aboutData);
      
      let Coarr=""
      for (var Cokey in this.state.our_condition) {
        if(this.state.our_condition[Cokey].add === true){
          Coarr+=`${this.state.our_condition[Cokey].value} ,`
        } 
      }
      let Codata = Coarr

      this.check()
      // how_do_u_know_abt_us   pickup_time   books_category, condition 
      const { donor_name,address,phone,city_name,state,startDate,errors,captcha,
        landmark,zipcode,no_of_book,no_of_cartons,app_books_weight,how_do_u_know_abt_us,donated_book_category} = this.state;
    //Errors
        if(isNaN(phone)){
          this.setState({errors:{phone:"Enter number"}});
          return;
      }
      if(phone.length !== 10 ){
        this.setState({errors:{phone:"Contact number must be 10 digits"}});
        return;
    }
      if(isNaN(zipcode)){
        this.setState({errors:{zipcode:"Enter Pincode"}});
        return;
    }
    if(zipcode.length !== 6 ){
      this.setState({errors:{zipcode:"Pincode must be 6 digits"}});
      return;
  }

      if(isNaN(no_of_book)){
        this.setState({errors:{no_of_book:"Enter number"}});
        return;
    }
      if(no_of_book < 1 ){
        this.setState({errors:{no_of_book:"Number Book must be more than zero"}});
        return;
    }

    if(isNaN(no_of_cartons)){
      this.setState({errors:{no_of_cartons:"Enter number"}});
      return;
  }
    if(no_of_cartons < 1 ){
      this.setState({errors:{no_of_cartons:"Number Cartons must be more than zero "}});
      return;
    }
    if(isNaN(app_books_weight)){
      this.setState({errors:{app_books_weight:"Enter number"}});
      return;
  }
  if(app_books_weight < 15 ){
    this.setState({errors:{app_books_weight:"Total weight must be more than 15 Kg "}});
    return;
  }
  if(!this.state.captcha ){
    this.setState({Error_captcha:"Please select the Captcha"});
    return;
  }

  // if(startDate > new Date() ){
  //   this.setState({errors:{error:"Enter Corrrect date"}});
  //   return;
  // }

  // if(this.state.errors){
  //   this.state.errors.map(error=>alert(error))
  // }
  if(Know_aboutData.length ===0){
    this.setState({know_aboutErr:true})
     setTimeout(()=>{this.setState({know_aboutErr:""})},2000)

  }
  if(Cdata.length ===0){
    this.setState({categoryErr:true})
    setTimeout(()=>{this.setState({categoryErr:""})},2000)

  }
  if(Codata.length ===0){
    this.setState({conditionErr:true})
    setTimeout(()=>{this.setState({conditionErr:""})},2000)

  }

if(Know_aboutData.length !==0 && Cdata.length !==0 && Codata.length !==0 && this.CaptchaErr === '' ){
      const token= `Token ${this.props.userToken}`
      const donation={
        "data":{
        "donor_name":donor_name,
        "address":`${this.state.address1 } ${this.state.address2}`,
        "state":this.state.state_name,
        "source":Know_aboutData,
        "category":Cdata,
        "city":this.state.city_name,
        "mobile":phone,
        "landmark":landmark,
        "country":"India",
        "zipcode":zipcode,
        "no_of_book":no_of_book,
        "no_of_cartons":no_of_cartons,
        "app_books_weight":app_books_weight,
        "pickup_date": `${this.state.startDate/1000}`	,
        // "book_Condition":Codata
      }};

      this.PassToPickup={
        donor_name:this.state.donor_name,
        mobile:Number(this.state.phone),
        app_books_weight:this.state.app_books_weight,
        address:`${this.state.address1 } ${this.state.address2}`,
        pincode: zipcode
      }
      // const {errors } = this.state
      // if(this.state.errors ==={} && this.state.SlectedKwAbt !== ""){
        // alert("okk")d           
      this.props.DonationMailData(donation)
      this.props.AddDonation(donation,token);
    // }
      // {document.getElementById("output").innerHTML=JSON.stringify(donation)}

      console.log(this.props.pickup)
      //clear the form 
      this.setState({
        // donor_name:'',
        address1:'',
        address2:'',
        state_name:'',
        // books_category:'',
        how_do_u_know_abt_us:'',
        // pickup_time:'',
        donated_book_category:'',
        city_name:'',
        // phone:'',
        landmark:'',
        zipcode:'',
        no_of_book:'',
        no_of_cartons:'',
        // app_books_weight:'',
        // condition:'',
        // pickup_date_time:''
        errors:{}
      })
    }else{
      window.scrollTo({ top: 0, behavior: 'smooth' }); 
    }
  }
    // formatter = Globalize.dateFormatter(raw: 'MMM dd, yyyy' })
  
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value })
    // alert(e.target.name);

    if(e.target.name == "no_of_book"){
      
      if(isNaN(e.target.value)){
        this.setState({errors:{no_of_book:"Enter number"}});
        return;
      }
      else if(e.target.value < 1 ){
        this.setState({errors:{no_of_book:"Number Book must be more than zero"}});
        return;
      }
      else{
        this.setState({errors:{no_of_book:""}});
      }
    }
    if(e.target.name == "zipcode" && e.target.value.length==6){
      // alert(e.target.value.length)
      axios.get(`${config.get('apiDomain')}/pincode/pin_check/${e.target.value}/`,
      )
      .then(res=>{
        this.setState({city_name:res.data[0].districtname,state_name:res.data[0].statename})
        console.log(res.data);
        
        }
        )
      .catch(err=>{
        console.log(err);
        
        this.setState({city_name:"",state_name:""})
      }
      )
   }
    if(e.target.name == "no_of_cartons"){
      
      if(isNaN(e.target.value)){
        this.setState({errors:{no_of_cartons:"Enter number"}});
        return;
      }
      else if(e.target.value < 1 ){
        this.setState({errors:{no_of_cartons:"Number Cartons must be more than zero "}});
        return;
      }
      else{
        this.setState({errors:{no_of_cartons:""}});
      }
    }

    if(e.target.name == "app_books_weight"){
      
      if(isNaN(e.target.value)){
        this.setState({errors:{app_books_weight:"Enter number"}});
        return;
      }
      else if(e.target.value < 15 ){
        this.setState({errors:{app_books_weight:"Number Book must be more than zero"}});
        return;
      }
      
      else{
        this.setState({errors:{app_books_weight:""}});
        // return
      }
      if(e.target.value.length ==0 ){
        this.setState({errors:{app_books_weight:""}});
        return;
      }
    }
    
  };
  ChangeCategories=(value)=>{
    // alert('okk')
    var getCategory=value
    return {
      ...this.state,
      categories:this.state.categories.map(category => category.value === getCategory ? 
        (category.add=!category.add):null),
    }
  }

  Changeknowabout=(value)=>{
    // alert(value)
    var getknowabout=value
    return {
      ...this.state,
      know_abt_us:this.state.know_abt_us.map(knowabout => knowabout.value === getknowabout ? 
        (knowabout.add=!knowabout.add):null),
    }
  }

  ChangeCondition=(value)=>{
    // alert(value)
    var getcondition=value
    return {
      ...this.state,
      our_condition:this.state.our_condition.map(condition => condition.value === getcondition ? 
        (condition.add=!condition.add):null),
    }
  }

  SetKnowAbout=(Sdata)=>{
    this.setState({SlectedKwAbt:this.state.SlectedKwAbt.concat(Sdata)})
  }

  Clear=(e)=>{
    e.preventDefault();
    this.setState({
      donor_name:'',
      address1:'',
      address2:'',
      state_name:'',
      // books_category:'',
      how_do_u_know_abt_us:'',
      // pickup_time:'',
      donated_book_category:'',
      city_name:'',
      phone:'',
      landmark:'',
      zipcode:'',
      no_of_book:'',
      no_of_cartons:'',
      app_books_weight:'',
      // condition:'',
      // pickup_date_time:''
      errors:{}
    })
  }

  render() {
    if(this.state.RedirectToPickup){
      // return <Redirect to={`/donate/pickup/${this.state.app_books_weight}`}/>
    }
    // if(this.state.captcha){
    //   // this.setState({CaptchaErr:"Captcha Did not match"});
    //   this.CaptchaErr='Enter correct Captcha'
    //   // return;
    // }else{
    //   // this.setState({CaptchaErr:""});
    //   this.CaptchaErr=''
    // }

    const {
      donor_name,
    address1,
    address2,
    state_name,
    // books_category,
    // how_do_u_know_abt_us,
    // pickup_time,
    // donated_book_category,
    errors,
    city_name,
    phone,
    landmark,
    zipcode,
    no_of_book,
    no_of_cartons,
    app_books_weight,} = this.state
  //   if(zipcode.toString().length === 6){
    
  //     axios.get(`${config.get('apiDomain')}/pincode/pin_check/${this.state.zipcode}/`,
  //     )
  //     .then(res=>this.setState({city_name:res.data[0].districtname,state_name:res.data[0].statename}))
  //     .catch(err=>{
  //       this.setState({city_name:"",state_name:""})
  //     }
  //     )
  //  }
    if(this.props.pickup===true){
      // return <Redirect to={`/donate/pickup/${this.state.app_books_weight}`}/>
    }
    // var  inputstye={ margin:"1em 2em",outline:'0' ,border:'0',width:'30%',
    // background: 'transparent',borderBottom :'1px solid #c5c0c0' ,color:'#00000'}
    // const PassToPickup={
    //   donor_name:this.state.donor_name,
    //   mobile:Number(this.state.phone),
    //   app_books_weight:this.state.app_books_weight,
    // }
    var today = new Date();
    // var DateToday = today.getDate();
    // console.log(today);
    function onChange() {
      console.log("Captcha Loaded");
    }
    const verifyCallback = (response)=>{
      console.log(response);
      this.setState({captcha:response,Error_captcha:''})
      
    };
    return (
<div id="DonationPage">
      <Helmet>
      <script src="https://www.google.com/recaptcha/api.js" async defer></script> 
          <script type="text/javascript" src="https://checkout.razorpay.com/v1/checkout.js">
          </script>
      </Helmet>

    <div id="formarea" >
      <div id="header">
      {/* <button type="button" id="close" onClick={this.props.ChangeLogin}>X</button> */}
      {/* <Link to='/pages/book-condition-guidelines' style={{ textDecoration:'none',color:'black' }}> */}
      <Link to='/pages/book-condition-guidelines' style={{ textDecoration:'underline',textDecorationColor:'black'}}>

      <p id="formheader">Follow Our Book Condition Guideline Before Filling This Form</p></Link>
        <form onSubmit={this.onSubmit} >
          <div id="leftpart">

          {/* {categories.map((category)=>
                  // console.log(i)
                  (<Button value={category.value} category={()=>this.category(category.value)}/>)
                )  
                } */}
{/* category={()=>this.category(category.value)} */}
          <Error id="errors" error={this.state.errors} />
         
          {/* </error> */}
          <InputDonation
                    name="donor_name"
                    // label="Name"
                    placeholder="Full Name"
                    value={donor_name}
                    onChange={this.onChange}
                    errors={errors.donor_name}
                    maxLength={40}
                    />
        <InputDonation
                    name="phone"
                    // label="Name"
                    placeholder="Mobile Number"
                    value={phone}
                    onChange={this.onChange}
                    errors={""}
                    maxLength={10}
                    />
                  {(this.state.phone.length < 10 )?
          <sapn style={{ fontSize:'60%',color:'red',marginLeft:'36%',fontWeight:"lighter"}}>Enter 10 Digits</sapn>:""
                  }
      <InputDonation
                    name="address1"
                    // label="Name"
                    placeholder="Address 1"
                    value={address1}
                    onChange={this.onChange}
                    errors={errors.address1}
                    maxLength={250}
                    />
       <InputDonation
                    name="address2"
                    // label="Name"
                    placeholder="Address 2"
                    value={address2}
                    onChange={this.onChange}
                    errors={errors.address2}
                    maxLength={250}
                    />
        <InputDonation
                    name="landmark"
                    // label="Name"
                    placeholder="Landmark"
                    value={landmark}
                    onChange={this.onChange}
                    errors={errors.landmark}
                    maxLength={40}
                    />

        <InputDonation
                    name="zipcode"
                    // label="Name"
                    placeholder="Pincode"
                    value={zipcode}
                    onChange={this.onChange}
                    errors={errors.zipcode}
                    maxLength={6}
                    />
   
        <InputDonation
                    name="city_name"
                    // label="Name"
                    placeholder="City"
                    value={city_name}
                    onChange={(e)=>{this.setState({city_name:e.target.value})}}
                    errors={errors.city_name}
                    />
        <InputDonation
                    name="state_name"
                    // label="Name"
                    placeholder="State"
                    value={state_name}
                    onChange={this.onChange}
                    errors={(e)=>{this.setState({state_name:e.target.value})}}
                    />
        <InputDonation
                    name="no_of_book"
                    // label="Name"
                    placeholder="No Of Books You Want To Donate"
                    value={no_of_book}
                    onChange={this.onChange}
                    errors={errors.no_of_book}
                    maxLength={5}
                    />
       <InputDonation
                    name="no_of_cartons"
                    // label="Name"
                    placeholder="No Of Cartoons 
                    (In Which Books To Be Filled)"
                    value={no_of_cartons}
                    onChange={this.onChange}
                    errors={errors.no_of_cartons}
                    maxLength={5}
                    />
      <InputDonation
                    name="app_books_weight"
                    // label="Name"
                    placeholder="Approx. Weight Of Books
                    (Min 15Kg)"
                    value={app_books_weight}
                    onChange={this.onChange}
                    errors={errors.app_books_weight}
                    maxLength={5}
                    /> 
                   {/* {(this.state.app_books_weight < 15 )?
                    // <sapn style={{ fontSize:'60%',color:'red',fontWeight:"lighter" }}>Total weight must be more than 15 Kg</sapn>:""
                    <span id="totalweight">Total weight must be more than 15 Kg</span>:""

                  } */}
          </div>

          <div id="rightpart">
          
              <div id="categories">
                <p id="cat_title">Select Donated Book Categories</p>
                {(this.state.categoryErr)?<span id="selectdonation">(*Please Select atleast One)</span>:null}    
                {this.state.categories.map((category)=>
                  // console.log(i)
                  (<Button value={category.value} ChangeCategories={()=>this.ChangeCategories(category.value)} />)
                )  
                }
                {/* <input type="button" value/> */}
              </div>

             
            {/* <label className="form-check-label">
              <input type="checkbox"
                checked={this.state.isJB}
                onChange={this.toggleChangeJB}
                
              /> */}

              <div id="knowabout">
                <p id="knowabout_title">From Where You Got To Know About MyPustak?</p>
                {/* {(this.state.SlectedKwAbt === '')?<span style={{ fontSize:'70%',fontWeight:'lighter',color:'red',position:'absolute',marginLeft:'15%' }}>(*Please Select any One)</span>:null} */}
                {(this.state.know_aboutErr)?<span id="selectdonation">(*Please Select any One)</span>:null}

                {this.state.know_abt_us.map((knowabout)=>{
                return (<Button value={knowabout.value} ChangeCategories={()=>this.Changeknowabout(knowabout.value)}/>)
                })  
                }
                
                {/* <input type="button" value="Facebook" 
                onClick={()=>this.SetKnowAbout("Facebook")} id={this.state.SlectedKwAbt==="Facebook"?"ClickedKwAbtBtn":"KwAbtBtn"}/>
                <input type="button" value="Whatsapp" 
                 onClick={()=>this.SetKnowAbout("Whatsapp")} id={this.state.SlectedKwAbt==="Whatsapp"?"ClickedKwAbtBtn":"KwAbtBtn"}/>
                 <input type="button" value="Quaro" 
                 onClick={()=>this.SetKnowAbout("Quaro")} id={this.state.SlectedKwAbt==="Quaro"?"ClickedKwAbtBtn":"KwAbtBtn"}/>
                  <input type="button" value="Google" 
                 onClick={()=>this.SetKnowAbout("Google")} id={this.state.SlectedKwAbt==="Google"?"ClickedKwAbtBtn":"KwAbtBtn"}/> */}
              </div>

              <div id="condition">
                  <p id="condition_title">Book Condition</p>
                {/* {(this.state.SlectedKwAbt === '')?<span style={{ fontSize:'70%',fontWeight:'lighter',color:'red',position:'absolute',marginLeft:'15%' }}>(*Please Select any One)</span>:null} */}
                {(this.state.conditionErr)?<span id="selectdonation">(*Please Select any One)</span>:null}


                  {this.state.our_condition.map((condition)=>{
                    return (<Button value={condition.value} style={{ fontSize:'80%' }}ChangeCategories={()=>this.ChangeCondition(condition.value)}/>)
                  })  
                  }
                  
              </div>

              <div id="leftbuttom">
                  
                  {/* <img id ="dateimg" src="https://image.flaticon.com/icons/png/512/9/9460.png"/> */}
                  <img id ="dateimg" src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/calender.2f9a1869.png`} alt=""/>
                
                    <DatePicker name="pickup_time" required
                    placeholderText="Book Collection Preferred Date"
                    selected={this.state.startDate}
                    onChange={this.handleChange.bind(this)}
                    
                    // dateFormat="h:mm aa"
                    minDate={today}
                    // excludeTimes={[setHours(setMinutes(new Date(), 0), 17), setHours(setMinutes(new Date(), 30), 18), setHours(setMinutes(new Date(), 30), 19), setHours(setMinutes(new Date(), 30), 17)]}
                    />

                 <p id="pickup" >Pickup Time :-2PM - 6PM
                  <p>No Pickup Are Avilable on Sunday</p>
                  </p>
                  <div id="captcha">
                      {/* <p style={{textAlign:'center'}}>Enter Captcha</p>
                      <div id="displaycaptcha">
                      <RCG result={this.result.bind(this)} />
                      </div>
                      <input type='text' name="captcha" className={'xxx'} ref={ref => this.captchaEnter = ref} maxLength="20"
                      onChange={e => this.setState({captcha: e.target.value})} placeholder="Enter Captcha" required/>

                      <p style={{ color:'red',fontWeight:'normal' }}>{this.CaptchaErr}</p> */}
                      <p style={{ color:'red',fontWeight:'normal' }}>{this.state.Error_captcha}</p>
                        <Recaptcha
                          sitekey="6Lc34KsUAAAAAJw3W3rYY_WQ_WUyO51CT6tcHSIH"
                          render="explicit"
                          verifyCallback={verifyCallback}
                          onloadCallback={onChange}
                          type= 'image'
                          size="compact"
                          // onChange={()=>this.setState({Error_captcha:''})}
                        />,
                  </div>
              </div>
              <span style={{ marginLeft:'10%' }}>
              <input id="donatesubmit" type="submit" value="Submit" 
 
                />
                <button id="clear" 

                onClick={this.Clear} >Clear</button></span>
          </div>
          {/* <span style={{ marginLeft:'10%' }}>
          <input style={{ backgroundColor:'#00baf2',color:'white',borderRadius:'5px',border:'none',
                      width:'15%',height:'10.2%',marginLeft:'2.2%',marginTop:'4%' ,padding:'.5%',fontWeight:'bold'
                }} type="submit" value="Submit" 
                />
        <button id="clear"  style={{ backgroundColor:'#00baf2',color:'white',borderRadius:'5px',border:'none',
                  marginLeft:'2.2%',marginTop:'4%' ,fontWeight:'bold',height:'105%'
            }}
        onClick={this.Clear} >Clear</button></span> */}
            
          
        </form>
        <Popup
                  open={this.props.pickup}
                  // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                    {/* {var PassToPickup:{
                    donor_name:this.state.donor_name,
                    phone:this.state.phone,
                    app_books_weight:this.state.app_books_weight,
                    }} */}
                  <DonationPickup PassToPickup={this.PassToPickup}/>
                  </Popup>
        <div id="output"></div>
      </div>
    </div>
</div >
    )}

  }

const mapStateToProps = state => ({
  pickup:state.donationR.pickup,
  userToken:state.accountR.token,
})

// const mapStateToProps = state => ({
//   visible:state.donationR.visible
// });


export default connect(mapStateToProps,{AddDonation,ChangeLogin,DonationMailData},)(Donationform)