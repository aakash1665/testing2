import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classnames from 'classnames'
const InputDonation =({
    label,
    value,
    name,
    placeholder,
    type,
    onChange ,
    errors,
    maxLength,
}) => {

    return (
        <div>
        <label htmlFor={name}>{label}</label>
        <input 
        style={{ paddingTop:'2%' }}
        type={type} 
        name={name}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
        maxLength={maxLength}
        // className={classnames('form-control ',{'is-invalid':errors})}
        required
        />
        {errors && <div style={{ color:'red',fontSize:'70%',fontWeight:'lighter' }}>{errors}</div>}
    </div>
    );
  };
//   TextInputGroup.prototype= {
//       name:PropTypes.string.isRequired, 
//       type:PropTypes.string.isRequired, 
//       value:PropTypes.string.isRequired, 
//       placeholder:PropTypes.string.isRequired, 
//       onChange:PropTypes.func.isRequired, 
//       label:PropTypes.string.isRequired, 
//       errors:PropTypes.string,
//   }
//   TextInputGroup.defaultProps = {
//       type:'text'
//   }
export default InputDonation