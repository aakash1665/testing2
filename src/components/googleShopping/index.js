import React,{ Component } from 'react'
import { connect } from "react-redux";
import { Button } from '@material-ui/core';
import {getBooksForBooks} from '../../actions/backofficeGoogleShoppingAction'
import BookInventoryDiv from '../BookInventoryPage/BookInventoryDiv';
import axios from 'axios'
import { OauthReceiver } from 'react-oauth-flow';
import { Google } from 'react-oauth2';
import { Helmet } from "react-helmet";

class GoogleShopping extends Component {
    state={
        FetchechedGoogleBooksState:{},
        OpenBookList:false,
        SendDataToGoogleShopping:{},
        token:'',
    }
    onClickGetBooks=()=>{
        this.props.getBooksForBooks()

    }

    componentDidMount(){
        // alert("Create Description")
        var url = window.location
        try {
            

        var new_url =  url.hash.split('&').filter(function(el) { if(el.match('access_token') !== null) return true; })[0].split('=')[1]
        console.log(new_url,"urls");

        this.setState({token:new_url})
    } catch (error) {
            
    }
        
    }


    componentWillReceiveProps(nextProps){
        if(this.props.FetchedBookForGoogle !== nextProps.FetchedBookForGoogle){
            this.setState({FetchechedGoogleBooksState:nextProps.FetchedBookForGoogle})
            this.setState({OpenBookList:true})
            // console.log(nextProps.FetchedBookForGoogle,"l");
            // nextProps.FetchedBookForGoogle.map(book=>{
            //     console.log(book);
                
            // })
            
        }

    }



    // {
    //     "entries": [
    //       {
    //         "batchId": 74005,
    //         "merchantId": 138173542,
    //         "method": "insert",
    //         "product": {
    //           "kind": "content#product",
    //           "offerId": "74005",
    //           "title": "Computer Science C++ A Text Book For Class 11 (English) by sumit arora",
    //           "description": "Computer Science C++ A Text Book For Class 11 (English) by sumit arora. From mypustak.com. Only Genuine Products, Free of Cost. In Good Condition in English Language. Published by Dhanpat Rai. Written by Sumit Arora",
    //           "link": "https://mypustak.com/product/lots-more-tell-me-why-",
    //           "imageLink": "https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/Computer-Science-C-A-Text-Book-For-Class-11.jpg",
    //           "contentLanguage": "en",
    //           "targetCountry": "IN",
    //           "channel": "online",
    //           "availability": "in stock",
    //           "condition": "used",
    //           "googleProductCategory": "784",
    //           "gtin": "608802531656",
    //           "price": {
    //             "value": "107",
    //             "currency": "INR"
    //           }
    //         }
    //       },
    //       {
    //         "batchId": 4,
    //         "merchantId": 138173542,
    //         "method": "insert",
    //         "product": {
    //           "kind": "content#product",
    //           "offerId": "118",
    //           "title": " NOOTAN ISC Chemistry for Class 12 (English) by Dr. H. C. Srivastava",
    //           "description": " NOOTAN ISC Chemistry for Class 12 (English) by Dr. H. C. Srivastava",
    //           "link": "https://mypustak.com/product/NOOTAN-ISC-Chemistry-for-Class-12-English-by-Dr-H-C-Srivastava/",
    //           "imageLink": "https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/NOOTAN-ISC-Chemistry-for-Class-12.jpg",
    //           "contentLanguage": "en",
    //           "targetCountry": "IN",
    //           "channel": "online",
    //           "availability": "in stock",
    //           "condition": "used",
    //           "googleProductCategory": "784",
    //           "gtin": "608802531649",
    //           "price": {
    //             "value": "175",
    //             "currency": "INR"
    //           }
    //         }
    //       }
    //     ]
    //   }


    // {
    //     "batchId": 74005,
    //     "merchantId": 138173542,
    //     "method": "insert",
    //     "product": {
    //       "kind": "content#product",
    //       "offerId": "74005",
    //       "title": "Computer Science C++ A Text Book For Class 11 (English) by sumit arora",
    //       "description": "Computer Science C++ A Text Book For Class 11 (English) by sumit arora. From mypustak.com. Only Genuine Products, Free of Cost. In Good Condition in English Language. Published by Dhanpat Rai. Written by Sumit Arora",
    //       "link": "https://mypustak.com/product/lots-more-tell-me-why-",
    //       "imageLink": "https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/Computer-Science-C-A-Text-Book-For-Class-11.jpg",
    //       "contentLanguage": "en",
    //       "targetCountry": "IN",
    //       "channel": "online",
    //       "availability": "in stock",
    //       "condition": "used",
    //       "googleProductCategory": "784",
    //       "gtin": "608802531656",
    //       "price": {
    //         "value": "107",
    //         "currency": "INR"
    //       }
    //     }
    //   }

    onClickSendBooks=()=>{
        let entries =[]
        this.state.FetchechedGoogleBooksState.map(book=>{
            let temp_data={
                "batchId":book.book_id,
                "merchantId": 138173542,
                "method": "insert",
                "product": {
                    "kind": "content#product",
                    "offerId": book.book_id,
                    "title":  `${book.book_title}`,
                    "description": `${book.book_title} From mypustak.com. Only Genuine Products, In Good Condition in ${book.book_lang} Language.`,
                    "link": `https://mypustak.com/product/${book.book_slug}/`,
                    "imageLink": `https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${book.book_thumb}`,
                    "contentLanguage": "en",
                    "targetCountry": "IN",
                    "channel": "online",
                    "availability": "in stock",
                    "condition": "used",
                    "googleProductCategory": "784",
                    "price": {
                    "value": `${Math.round(book.book_price)}`,
                    "currency": "INR"
                    }
                }
            }

            entries.push(temp_data)
            // console.log(temp_data,"tda");
            
        })

        console.log(entries,"ent");

        this.setState({SendDataToGoogleShopping:{"entries":entries}})
        
    }


    onClickOAuth=()=>{
        const url="https://accounts.google.com/o/oauth2/auth?response_type=token&scope=https://www.googleapis.com/auth/content&client_id=584441755319-lvvakabeol3k2kvqvmtt5hburb68edqe.apps.googleusercontent.com&redirect_uri=http://localhost:3000/backoffice/googleshopping&token_uri=https://oauth2.googleapis.com/token"
        window.open(url)
        
        // fetch(url)
        // .then(res=>{console.log(res);
        // })
        // axios.get("https://accounts.google.com/o/oauth2/auth?response_type=token&scope=https://www.googleapis.com/auth/content&client_id=584441755319-lvvakabeol3k2kvqvmtt5hburb68edqe.apps.googleusercontent.com&redirect_uri=https://www.mypustak.com&token_uri=https://oauth2.googleapis.com/token/",{headers: {
        //     'Access-Control-Allow-Origin': '*',
        //   },proxy: {
        //     host: 'localhost',
        //     port: 3000
        //   }},)
        // .then(res=>{
        //     console.log(res);
            
        // })
        // .catch(err=>{

        // }) Error: "Request failed with status code 401"
    }
    onClickConfirm_Sending=()=>{
            console.log(this.state.SendDataToGoogleShopping,this.state.token);
            
        axios.post("https://www.googleapis.com/content/v2.1/products/batch",{headers: {
            'Authorization': `Bearer ${this.state.token}`,
            'Content-type':'application/json'
          }},{body:this.state.SendDataToGoogleShopping})
        .then(res=>{
            console.log(res);
            
        })
        .catch(err=>{
console.log(err,`Bearer ${this.state.token}`);

        })

    }

    handleSuccess = async (accessToken, { response, state }) => {
        console.log('Successfully authorized',accessToken,response);
        // await setProfileFromDropbox(accessToken);
        // await redirect(state.from);
      };
      handleError = error => {
        console.error('An error occured');
        console.error(error.message);
      };

      TestAuth=()=>{
        var apiKey = 'AIzaSyAdjHPT5Pb7Nu56WJ_nlrMGOAgUAtKjiPM';
        var clientId = '584441755319-lvvakabeol3k2kvqvmtt5hburb68edqe.apps.googleusercontent.com';
        var scopes = 'https://www.googleapis.com/auth/content';
        const gapi = window.gapi
        gapi.load('client', initClient);
        
        function initClient() {
            gapi.client.init({
              apiKey: apiKey,
              clientId: clientId,
              scope: scopes
            }).then();
          }
      }

    render(){

        return(
                <div>
                    <Helmet>
                    <script async defer src="https://apis.google.com/js/api.js">
                    </script>
                    </Helmet>
                    <h3>GOOGLE SHOPPING</h3>
                    <div>
                        <Button
                        variant="contained"
                        color="primary"
                        margin="normal"
                        style={{
                            marginLeft: "theme.spacing(1)",
                            marginRight: "theme.spacing(1)",
                            width: 200,
                            display: "flex",
                            flexWrap: "wrap",
                            margintop: "theme.spacing(1)"
                        }}
                        onClick={this.onClickGetBooks}
                        >
                        Get the Books
                        </Button>

                        <Button
                        variant="contained"
                        color="secondary"
                        margin="normal"
                        style={{
                            marginLeft: "theme.spacing(1)",
                            marginRight: "theme.spacing(1)",
                            width: 200,
                            display: "flex",
                            flexWrap: "wrap",
                            margintop: "theme.spacing(1)"
                        }}
                        onClick={this.onClickOAuth}
                        >
                        OAuth2
                        </Button>

     
                        <Button
                        variant="contained"
                        color="secondary"
                        margin="normal"
                        style={{
                            marginLeft: "theme.spacing(1)",
                            marginRight: "theme.spacing(1)",
                            width: 200,
                            display: "flex",
                            flexWrap: "wrap",
                            margintop: "theme.spacing(1)"
                        }}
                        onClick={this.onClickSendBooks}
                        >
                        Create data Google Shopping
                        </Button>

                        <Button
                        variant="contained"
                        color="secondary"
                        margin="normal"
                        style={{
                            marginLeft: "theme.spacing(1)",
                            marginRight: "theme.spacing(1)",
                            width: 200,
                            display: "flex",
                            flexWrap: "wrap",
                            margintop: "theme.spacing(1)"
                        }}
                        onClick={this.onClickConfirm_Sending}
                        >
                        Confirm Sending
                        </Button>

                    </div>

                    {/* <OauthReceiver
        tokenUrl="https://oauth2.googleapis.com/token"
        clientId={`584441755319-lvvakabeol3k2kvqvmtt5hburb68edqe.apps.googleusercontent.com`}
        clientSecret={`l5HzGjzZgn7aPdEswR7sP6ff`}
        redirectUri="https://www.mypustak.com"
        onAuthSuccess={this.handleSuccess}
        onAuthError={this.handleError}
        render={({ processing, state, error }) => (
          <div>
            {processing && <p>Authorizing now...</p>}
            {error && (
              <p className="error">An error occured: {error.message}</p>
            )}
          </div>
        )}
      />
      <input type="text" value={this.state.token} onChange={(e)=>this.setState({token:e.target.value})}/>

<Google
        url={'http://localhost:3000'}
        clientId={'584441755319-lvvakabeol3k2kvqvmtt5hburb68edqe.apps.googleusercontent.com'}
        clientSecret={'l5HzGjzZgn7aPdEswR7sP6ff'}
        redirectUri={'https://www.mypustak.com'}
        scope={['https://www.googleapis.com/auth/content']}
        width={300}
        height={300}
        callback={this.google}
        style={{ color: 'green' }}
      >
        Login With Google From component
  </Google>*/}
                </div> 
        )
    }   
}
const mapStateToProps = state=>({
    FetchedBookForGoogle:state.googleReducer.FetchechedGoogleBooks
})

export default connect(mapStateToProps,{getBooksForBooks})(GoogleShopping)
