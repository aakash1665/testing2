import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Paper from '@material-ui/core/Paper';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import { updateTrackingAndBarcode } from '../../actions/backendUpdateTRandBarcode';

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  dense: {
    marginTop: 19,
  },
  menu: {
    width: 200,
  },
  card: {
    minWidth: 275,
  },
});

class UpdateTrackingAndBarcode extends React.Component {

  state={
    loading: false,
  }

  componentDidMount() {
    this.setState({
      token: localStorage.getItem('user')
    })
  }

  updateTrackingAndBarCode = async (e) => {
    e.preventDefault();

    this.setState({
      loading: true
    })

    let tracking_no = e.target.tracking_no.value;
    let from_barcode = e.target.from_barcode.value;
    let to_barcode = e.target.to_barcode.value;
    let study_material = e.target.study_material.value;
    let book_wastage = e.target.book_wastage.value;
    
    let res = await updateTrackingAndBarcode({ tracking_no, from_barcode, to_barcode, study_material, book_wastage, token: this.state.token })
    if(res) {
    alert(` ${tracking_no} - ${res}`);
    this.setState({
      loading: false
    })
    this.formRef.reset();      
    }
  }

  render() {
    const { classes } = this.props;
    const { loading } = this.state;

    return (
      <div>
        <Paper className={classes.root} elevation={1}>
          <div style={{ maxWidth: '220px', display: 'flex', flexDirection: 'column', justifyContent: 'center' }}>
          <form ref={ node => this.formRef = node} onSubmit={this.updateTrackingAndBarCode}>
          <Card className={classes.card}>
            <CardContent>
            <TextField
              id="tracking_no"
              name="tracking_no"
              label="Tracking Number"
              className={classes.textField}
              margin="dense"
              fullWidth
            />
            <TextField
              id="from_barcode"
              name="from_barcode"
              label="From Barcode No"
              className={classes.textField}
              margin="dense"
              fullWidth
            />
            <TextField
              id="to_barcode"
              name="to_barcode"
              label="To Barcode No"
              className={classes.textField}
              margin="dense"
              fullWidth
            />
            <TextField
              id="study_material"
              name="study_material"
              label="Study Material"
              type="number"
              className={classes.textField}
              defaultValue= {0}
              margin="dense"
              fullWidth
            />
            <TextField
              id="book_wastage"
              name="book_wastage"
              label="Wastage"
              type="number"
              className={classes.textField}
              defaultValue= {0}
              margin="dense"
              fullWidth
            />
            <Divider  style={{ margin: '12px 0' }} />
            {
              loading ? <div> Loading... </div>
              : (
                <div style={{ display: 'flex', justifyContent: 'space-between' }}>
                  <Button size="small" className={classes.button}>
                    Reset
                  </Button>
                  <Button size="small" type="submit" variant="outlined" color="primary" className={classes.button}>
                    Save
                  </Button>
                </div>
              )
            }
          </CardContent>
          </Card>
          </form>
          </div>
        </Paper>
      </div>
    );
  }
}

UpdateTrackingAndBarcode.propTypes = {
  classes: PropTypes.object.isRequired
};

export default (withStyles(styles)(UpdateTrackingAndBarcode));
