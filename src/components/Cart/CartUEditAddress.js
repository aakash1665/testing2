import React, { Component } from 'react'
import UserInputGroup from '../delivery/UserInputGroup'
// import {AddAddressAction} from '../../actions/addressAction';
import { connect } from 'react-redux';
import '../delivery/useraddress.css'
import {EdituserAddressAction,Editaddress,Getaddress} from '../../actions/accountAction';
import axios from 'axios'
import config from 'react-global-configuration'
class CartUEditAddress extends Component {
    state ={
      
        address_id:this.props.editadd.address_id,
        fullname :this.props.editadd.rec_name,
        landmark:this.props.editadd.landmark,
        phone_no:this.props.editadd.phone_no,
        // alt_phone_no:this.props.ed
        pincode:this.props.editadd.pincode,
        address1:this.props.editadd.address,
        city_name:this.props.editadd.city_name,
        state_name:this.props.editadd.state_name,
        title:this.props.editadd.title,
        errors:{},
        is_primary:this.props.editadd.is_primary,
        addaddressopen:false,
        PinCodeOkk:true

    };
    // onaddressChanged = e => {this.setState({[e.target.name]:e.target.value})}
    // changecheck=()=>{
    //   this.setState({is_primary:!this.state.is_primary})
    // }
    closedone=()=>{
      // console.log(this.props.closemodule);
    }
    EditDone=()=>{
      if(this.state.errors==={}){
       
        // window.location.href = `/view-caart`
       }
 
    }
    
  addaddresscloseModal=()=>{
    
    this.setState({ addaddressopen: false })
   }

onSubmit =(e)=>{
    // alert(e);
    e.preventDefault();
    const {pincode,phone_no,fullname,state,landmark,address1,address_id,is_primary,city_name,errors,state_name,title} = this.state;

    if(isNaN(phone_no)){
        this.setState({errors:{phone_no:"Enter number"}});
        return;
    }
  //   if(isNaN(pincode)){
  //     this.setState({errors:{pincode:"Enter Pincode"}});
  //     return;
  // }

    if(phone_no.length !== 10){

        this.setState({errors:{phone_no:"Contact number must be 10 digits"}
      });
        return;
    }

    // if(pincode.toString().length !== 6){
    //     this.setState({errors:{pincode:`Sorry, we could not locate this pincode.` }});
    //     return;
    // }

    const address={
        data :{
          "address_id":address_id,
          "title":title,
          "rec_name":fullname,
          "address":address1,
          "landmark":landmark,
          "phone_no":phone_no,
          "pincode":pincode,
          "city_name":city_name,
          "state_name":state_name,
          "country_name":'IND',
          "is_primary":is_primary,
        }
    }

    if(this.state.PinCodeOkk === true){

    const token= `Token ${this.props.userToken}`
    this.props.EdituserAddressAction(token,address)
    // this.setState({
    //     addressid:'',
    //     fullname :'',
    //     landmark:'',
    //     phone_no:'',
    //     pincode:'',
    //     city:'',
    //     state:'',
    //     title:'',
    //     address1:'',
    //     errors:{}
    // })
    // window.location.href = `/view-cart`
    this.props.closeCartUEditAddr()
    const details=`Token ${this.props.userToken}`
    // this.props.userdetails(details)
    this.props.Getaddress(details)
    }
}

    onChange = e =>{ this.setState({[e.target.name]:e.target.value});
  
    if(e.target.name == "pincode"){
      console.log(this.state.pincode,typeof  this.state.pincode);
      
      if(e.target.value.length === 6){
        console.log("In 6");
        console.log(this.state.pincode);
        
        axios.get(`${config.get('apiDomain')}/pincode/pin_check/${e.target.value}/`,
        )
        .then(res=>{
          console.log(res.data,this.state.pincode,);
          if(res.data.length){
        this.setState({city_name:res.data[0].districtname,state_name:res.data[0].statename});
        // this.setState({errors:{pincode:""}});
        this.setState({PinCodeOkk:true});}
      })   
        .catch(err=>{console.log(err)
          this.setState({city_name:"",state_name:""});
        
        }
        )
        console.log("In Ca");
        
      }else{
      this.setState({PinCodeOkk:false});
        
      }
    }
  }
  
  render() {
    // console.log(this.props.editadd)
    const {fullname,landmark,alt_phone_no,phone_no,address1,city_name,state_name,pincode,title,errors,is_primary} = this.state;
    let PincodeErr=""
    let PhoneErr=""
    // if(pincode.toString().length === 6){
    
    //   axios.get(`${config.get('apiDomain')}/pincode/pin_check/${this.state.pincode}/`,
    //   )
    //   .then(res=>{
    //     if(res.data.length === 0){
    //       this.setState({errors:{pincode:"Enter Correct Pincode"}});
    //   }else{
    //   this.setState({city_name:res.data[0].districtname,state_name:res.data[0].statename});
    //   this.setState({errors:{pincode:""}});}})
    //   .catch(err=>console.log(err)
    //   )   
    //      PincodeErr=""
    // }
    // // For pincode validation
    // else {
    //   PincodeErr=" Please enter 6 digits pincode"
    // }
     //   For Phone Number validation

     try {
       

   if(phone_no.length !== 10){
    //   alert("stop")
   PhoneErr = "Please enter 10 digits mobile number"
    // document.getElementsById("mobile").disable=true
    // this.mobilechecker()
}else{
     PhoneErr=""
}

} catch (error) {
       
}

  
    return (
        <div class="container-1" style={{marginRight:'3%',marginLeft:'3%' }}>
        <div id="addressdiv">
        <a className="close" onClick={this.props.closeCartUEditAddr} style={{
    color: 'white',zIndex:'999',opacity:'1',fontWeight:'normal',marginTop:'-1vh',
    float:'left'}}>
              &times;
            </a>
        <div id="Add-New-Address">Edit Address
        </div></div>
        <br/><br/><br/>
        <form onSubmit={this.onSubmit}>

        <UserInputGroup
                    name="fullname"
                    // label="Name"
                    placeholder="Full Name"
                    value={fullname}
                    onChange={this.onChange}
                    errors={errors.fullname}
                    maxLength={40}
                    />
        <UserInputGroup
                    name="address1"
                    // label="Name"
                    placeholder="Address 1"
                    value={address1}
                    onChange={this.onChange}
                    errors={errors.name}
                    maxLength={250}
                    />
        <UserInputGroup
                    name="landmark"
                    // label="Name"
                    placeholder="Landmark"
                    value={landmark}
                    onChange={this.onChange}
                    errors={errors.landmark}
                    maxLength={80}
                    />
        <UserInputGroup
               
                    name="phone_no"
                    // label="Name"
                    placeholder="Mobile Number"
                    value={phone_no}
                    onChange={this.onChange}
                    errors={errors.phone_no}
                    maxLength="10"
                    />
                     <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,fontWeight:'lighter',height:'5%',lineHeight:'0px'}}>{PhoneErr}</p>


                     <UserInputGroup
               
               name="alt_phone_no"
               // label="Name"
               placeholder="Alernate Mobile Number"
               value={alt_phone_no}
               onChange={this.onChange}
               errors={errors.alt_phone_no}
               maxLength="10"
               />
                <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,fontWeight:'lighter',height:'5%',lineHeight:'0px'}}>{PhoneErr}</p>

        <UserInputGroup 
                    name="pincode"
                    // label="Name"
                    placeholder="Pincode"
                    value={pincode}
                    onChange={this.onChange}
                    errors={errors.pincode}
                    maxLength="6"
                    />
                    <p style={{ fontSize:"70%",color:'red',padding:'0px',margin:'0px' ,fontWeight:'lighter',height:'5%',lineHeight:'0px'}}>{(this.state.PinCodeOkk)?null:`Enter Correct Pincode`}</p>

        <UserInputGroup
                    name="city"
                    // label="Name"
                    placeholder="City"
                    value={city_name}
                    onChange={(e)=>{this.setState({city_name:e.target.value})}}
                    errors={errors.name}
                    maxLength={40}
                    />
        <UserInputGroup
                    name="state"
                    // label="Name"
                    placeholder="State"
                    value={state_name}
                    onChange={(e)=>{this.setState({state_name:e.target.value})}}
                    errors={errors.name}
                    maxLength={40}
                    />

                    {/* For future use  */}
        {/* <input type="Checkbox" checked={this.state.is_primary} name="setdefault" style={{width:'13px',marginTop:'3%'}}  onChange={this.changecheck}/><span style={{    marginLeft: '6%',}}>Set As Default</span>  */}
        {/* <InputGroup
                    name="title"
                    // label="Name"
                    placeholder="Address Title"
                    value={title}
                    onChange={this.onChange}
                    errors={errors.name}
                    /> */}
                    <br/>
                    <label style={{marginTop:'2%'}}>Address Title</label>
        <div>            
        <input type="radio" value="Home" name="title" style={{width:'13px'}}  onChange={this.onChange} checked={(title==="Home")}/><span style={{    marginLeft: '6%',}}>Home</span>
        <input type="radio" value="Office" name="title" style={{width:'13px',    marginLeft: '16%'}} onChange={this.onChange} checked={(title==="Office")} /> <span style={{marginLeft: '6%',}}>Office</span>
        </div>
        <input type="submit" id="donebtn_button"  onClick={this.closedone} value="Done"/> 
         </form>
    </div>
// onClick={this.EditDone}
    )
  }
}

const mapStateToProps = state => ({
    getuseradd:state.accountR.getuseradd,
    userToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
    editadd:state.accountR.editadd,
  })


export default connect(mapStateToProps,{EdituserAddressAction,Editaddress,Getaddress})(CartUEditAddress);