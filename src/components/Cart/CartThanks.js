import React, { Component } from 'react'
import './CartThanks.css'
import { connect } from 'react-redux';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import axios from 'axios'
import Popup from 'reactjs-popup'
import {Getwalletd,WalletRecarge,SetWallet} from '../../actions/walletAction'
import {AddToCart,CartopenModal,CartcloseModal,removeAllCart,RemoveCart,CartSession,ToBeAddedToCart,removeToBeAddedToCart,removeFromCartLogout,ShowRzPayErr} from '../../actions/cartAction'
import config from 'react-global-configuration'
import MediaQuery from 'react-responsive';

import ReactGA from 'react-ga';

import { Link,Redirect,browserHistory } from 'react-router-dom'
class CartThanks extends Component {

state={
  ShowPls:false,
}
  backToHome(){

    // window.location = `/donate`

    
      }
      componentDidMount(){
        console.log(window.location.pathname);
        
        sessionStorage.clear()

        if(localStorage.getItem('user') === null){
          // alert("RED")
          window.location.href="/"
        }else{

        }

        const details=`Token ${localStorage.getItem('user')}`
        this.props.SetWallet(details)
        this.props.Getwalletd(details)
        // alert(this.props.OrderId)
        // console.log(window.location.pathname);
        
      if(this.props.OrderId !== 0){
        this.setState({ShowPls:true})
      localStorage.setItem('UserOrderId',this.props.OrderId);
      // {"orderid":"999","tran_id":"999","payment_id":"999","pay_via":"cash","OrderDate":"12/03/19","thumb":"abc","Name":"test","author":"test","rack_no":"999","price":"99","wallet":"0","total_pay":99,"total_value":100}
      // {"phone_no":"00000000","pincode":700012,"email":"test@email.in","user_add":"jdkajkdjkadj"}
      var AllbookName=[]
      var BookData=[]
      var AllrackNo=[]
      var AllThumb=[]
      var AllbookShippingCost=[]
      var today = new Date();
      var dd = today.getDate();
      var mm = today.getMonth()+1; //January is 0!
      var yyyy = today.getFullYear()
      today = dd + '/' + mm + '/' + yyyy
        var AllBooks=""
      this.props.cartDetails.map((book,index)=>{
        AllBooks+=`Book Name is ${book.bookName}`
        AllBooks+=`Book Cost is ${book.bookShippingCost}`
        AllBooks+=`Book Rack no is ${book.bookRackNo}.`


        // AllBooks.push(arrayBook)
        AllbookName.push(`${book.bookName}`);
        AllbookShippingCost.push(book.bookShippingCost);
        AllrackNo.push(book.bookRackNo);
        AllThumb.push(book.bookThumb);
       //  index;
      })
      // alert("o")
      const Data={ 
        orderId:this.props.OrderId,
        transaction_Id:this.props.cartPriceDetails.transaction_id,
        paymentId:this.props.cartPriceDetails.transaction_id,
        payVia:this.props.cartPriceDetails.paytype,
        orderDate:today,
        thumb:AllThumb.toString(),
        bookName:AllbookName,
        author:"",
        rackNo:AllrackNo,
        price:AllbookShippingCost,
        wallet:this.props.cartPriceDetails.wallet,
        totalPayment:this.props.cartPriceDetails.TotalPayment,
        // total_value:this.props.cartPriceDetails,
        phoneNo:this.props.SelectedAddress.phone_no,
        pincode:this.props.SelectedAddress.pincode,
        userAddress:this.props.SelectedAddress.address,
        landmark:this.props.SelectedAddress.landmark,
        state:this.props.SelectedAddress.state_name,
        city:this.props.SelectedAddress.city_name,
        recname:this.props.SelectedAddress.rec_name,
        noofbook:this.props.ItemsInCart,
        allBooks:AllBooks,

        
        email:`${this.props.userDetails.email}`
    }


      ReactGA.plugin.execute('ec', 'setAction', 'purchase', {
          id: Data.transaction_Id,
          affiliation: Data.bookName,
          revenue: Data.totalPayment,
          // shipping: Data.,
          // tax: Data.,
          // currency: Data.,
          payVia: Data.payVia,
      });
      ReactGA.plugin.execute('ec', 'send');      
      ReactGA.pageview('/confirmation');
      ReactGA.plugin.execute('ec', 'clear');

      axios.post(`${config.get('apiDomain')}/common/email_data/`,Data,
    //   {headers:{'Authorization': `Token ${this.props.userToken}`
    // }}
    )
      .then(res=>{
        console.log(res,Data);
        window.location.reload();
      })
      .catch(err=>console.log(err,Data) 
      )

const  RemoveFromCart=(bookInvId,Cart_id)=>{
        for (var i=0; i<=sessionStorage.length-1; i++)  
        {   
          // alert("rmo")
            let key = sessionStorage.key(i);  
            try {
              if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){
                  let val = JSON.parse(sessionStorage.getItem(key));
                  if(`${val.bookInvId}` === `${bookInvId}`)  {
                    alert("rm")
                    sessionStorage.removeItem(Number(val.bookInvId));

                  }
              }
              sessionStorage.clear()

            } catch (error) {
              console.log(error);
              
            }
    
            // val.map(det=>console.log(det))
            // this.props.AddToCart(val)
            // alert(val)
        }
        if(localStorage.getItem('user') === null){
    
        this.props.removeFromCartLogout(bookInvId)
        }else{
        const data={"is_deleted":"Y","book_puchased":"Y"}
        this.props.RemoveCart(Cart_id,bookInvId,data)
    
        }
       }
      try {
        this.props.cartDetails.map((book,index)=>{
          RemoveFromCart(book.bookInvId,book.Cart_id)
          sessionStorage.clear()
        })
      } catch (error) {
        
      }

      sessionStorage.clear()
      this.props.removeAllCart()
      } 
      }
 

  
  render() {
    const id = this.props.match.params.id
    return (
      <div>
      <MainHeader/>
      {/* -----------------------For Other than Small Mobile---------------- */}
      <MediaQuery minWidth={538}>
        <Popup
                      open={this.props.RzPayErr}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false});this.props.ShowRzPayErr()}}
                      // closeOnDocumentClick={false}
                      contentStyle={{ }}
                      > 
                       <p style={{ textAlign:'center', }}>
                      If your order has not been generated and money has been deducted from your account . 
                      Please write us at support@mypustak.com along with your transaction details
                      so that we can resolve the issue as earliest as possible.
                      </p>
                     <div>
                      
                     </div>
                  </Popup>
                  </MediaQuery>
                  {/* -----------------------For Small Mobile---------------- */}
                  <MediaQuery maxWidth={538}>
                  <Popup
                      open={this.props.RzPayErr}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false});this.props.ShowRzPayErr()}}
                      // closeOnDocumentClick={false}
                      contentStyle={{ width:'80%'}}
                      > 
                       <p style={{ textAlign:'center', }}>
                      If your order has not been generated and money has been deducted from your account . 
                      Please write us at support@mypustak.com along with your transaction details
                      so that we can resolve the issue as earliest as possible.
                      </p>
                     <div>
                      
                     </div>
                  </Popup>
                  </MediaQuery>
      <div id="thanksbody">
          <img id="handshake"  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ThankYou.77288eb6.png`} alt=""/>
          {/* <img id="A_iconimg" /> */}
          {/* <br/> */}
        <p id="Cl1_thanks" style={{ textAlign:'center' }} >YIPPEE...</p>
        {/* {this.props.OrderSuccess? */}
        <p id="l2_we"  style={{ textAlign:'center' }}>Your Order Has Been Placed Successfully!!! </p>
        {/* // :<p id="l2_we"  style={{ textAlign:'center' }}>Your Order  Placement Failed !!! </p>} */}
        <hr/>
        <p id="ThanksPurchase">Thank You For Your Purchase.</p>
        <p style={{ textAlign:'center' }}><span id="part1">Your Order Id Is: <span style={{ color:'#00baf2',fontWeight:'bold' }}>{localStorage.getItem('UserOrderId')}</span> </span>
                       {/* <span id="part2"><b> {id} </b>(Please Note For Future Reference)</span> */}
        </p>
        {this.state.ShowPls?<p id="PlWait">Please Wait....</p>:null}
        {/* <p  style={{ fontSize:'90%',marginLeft:'23%' }}>You Will Receave An Order Confirmation Email With Details Of Your Order And A Link to Track Your Progress.</p> */}
        <p  style={{ fontSize:'90%',textAlign:'center',padding:'3%'}}>You Will Receive  Tracking details of your order once it will be shipped. You can also track your order at home page under track order tab</p>

        <hr/>
        {/* <p  style={{ fontSize:'90%',textAlign:'center',marginLeft:'0%',fontWeight:'bold',fontSize:'' }}>You Can Convert Your Order To Prepaid And (Get Extra Discount). </p> */}
        {/* <p style={{ textAlign:'center' }} style={{ fontSize:'110%' }}>For Immediate Pick Ups,
         Pay Nominal Shipping Cost To Schedule Your Instant Pick Up.</p> */}
        {/* <button id="Cprocced" >Procced</button> */}
        {/* <hr/> */}
        {/* <p style={{ textAlign:'center',fontSize:'100%'}} >How Did You Know About Us?</p> */}
        {/* <MediaQuery minWidth={768}>      */}
        {/* <form id="how">
        <input type="checkbox" style={{ width:'2%', paddingLeft:'10%',paddingTop: '0%',marginRight:'1%' }}value="Quora"/>
            <label htmlFor="Quora" style={{  paddingRight:'1%'}}>Quora </label> 

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%',marginRight:'1%'}}value="Google"/>
            <label htmlFor="Google" style={{  paddingRight:'1%'}}>Google </label>

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%',marginRight:'1%' }} value="Facebook"/>
            <label htmlFor="Facebook" style={{ paddingRight:'1%' }}>Facebook </label>

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%' ,marginRight:'1%'}}value="Yahoo"/>
            <label htmlFor="Yahoo" style={{  paddingRight:'1%' }}>Yahoo </label>

            <input type="checkbox" style={{ width:'2%', paddingTop: '0%',marginRight:'1%' }} value="Twitter"/>
            <label htmlFor="Twitter" style={{  paddingRight:'1%' }}>Twitter </label>

            <input type="submit" id="thankssubmit" value="Submit"/>
        </form> */}
        {/* </MediaQuery> */}

        {/* <MediaQuery maxWidth={767}> */}
        {/* <form id="how">
            <input type="checkbox" style={{ width:'5%', paddingLeft:'10%',paddingTop: '0%',marginRight:'1%' }}value="Quora"/>
            <label htmlFor="Quora" style={{  paddingRight:'1%'}}>Quora </label> 
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%',marginRight:'1%'}}value="Google"/>
            <label htmlFor="Google" style={{  paddingRight:'1%'}}>Google </label>
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%',marginRight:'1%' }} value="Facebook"/>
            <label htmlFor="Facebook" style={{ paddingRight:'1%' }}>Facebook </label>
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%' ,marginRight:'1%'}}value="Yahoo"/>
            <label htmlFor="Yahoo" style={{  paddingRight:'1%' }}>Yahoo </label>
            <br/>
            <input type="checkbox" style={{ width:'5%', paddingTop: '0%',marginRight:'1%' }} value="Twitter"/>
            <label htmlFor="Twitter" style={{  paddingRight:'1%' }}>Twitter </label>
            <br/>
            <input type="submit" id="thankssubmit" value="Submit"/>
        </form> */}
        {/* </MediaQuery> */}
        {/* <hr/> */}
        <p style={{fontSize:'110%',textAlign:'center',padding:'3%'}}>You Can See Details Of Your Order From Customer Account.  
        <Link to="/customer/customer_order" style={{ textDecoration:"none" }}><span style={{ color:'#d15714' }}> Customer Account.</span></Link></p>
         {/* <button id="back" onClick={()=>this.backToHome()}>Back To Home</button> */}
         <Link to="/" style={{ textDecoration:"none" }}><button id="back" >Back To Home</button></Link>

      </div>
      <MainFooter/>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  OrderId:state.cartReduc.OrderId,
  cartDetails:state.cartReduc.MyCart,
  userDetails:state.userdetailsR.getuserdetails,
  SelectedAddress:state.accountR.selectedAddress,
  cartPriceDetails:state.cartReduc.CartPrice,
  ItemsInCart:state.cartReduc.cartLength,
  // OrderSuccess:state.cartReduc.orderSuccess,

})
export default connect(mapStateToProps,{RemoveCart,removeFromCartLogout,removeAllCart,ShowRzPayErr,Getwalletd,SetWallet})(CartThanks)


// select t1.order_id, t1.amount, t1.no_of_book, t1.actual_date_upload, t1.status, t1.payusing, t1.i_date, title, thumb, price from 
//     					  (select orders.order_id, user_id, amount, no_of_book, actual_date_upload, status, payusing, i_date, book_id from orders inner join order_books 
//     					  on orders.order_id = order_books.order_id where user_id= %s) t1 inner join books on t1.book_id= books.book_id order by t1.i_date desc limit %s,10'
// '', (userid, start))
