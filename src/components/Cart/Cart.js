import React, { Component } from 'react'
import './cart.css'
import { connect } from 'react-redux';
import {getBook} from '../../actions/booksActions'
import {AddPriceCart,RemoveCart,OrderDetails,SetOrderId,walletRazorpay,RedirectWalletToCart,RemoveToBeAddedToCart,removeFromCartLogout,ShowRzPayErr,orderUpdateSucc,OutOfStock} from '../../actions/cartAction'
import {Getaddress,SetSelectedAddressBlank} from '../../actions/accountAction'
import {Getwalletd,SetWallet} from '../../actions/walletAction'
import { Link,Redirect} from 'react-router-dom'
import Popup from 'reactjs-popup'
import Delivery from '../delivery/Delivery';
import MainHeader from '../MainHeader';
import MainFooter from '../MainFooter';
import {Get_Rp_Id} from '../../actions/donationActions'
// import del from '../product/Product_Images/del.png';
import ProceedToPay from '../ProceedToPay';
import Loader from 'react-loader-spinner'
import axios from 'axios'
import config from 'react-global-configuration'
import MediaQuery from 'react-responsive';
// import { PayPalButton } from "react-paypal-button-v2";
// import { Z_NEED_DICT } from 'zlib';
// import { runInThisContext } from 'vm';
import ReactGA from 'react-ga';
import { isNull } from 'util';
import { Helmet } from "react-helmet";

class Cart extends Component {

  state={
    open: false,
    openDelivery:false,
    totalShipping:0,
    below:70,
    above:50,
    paytype:'prepaid',
    OpenProceedPayBtn:false,
    COUNT:1,
    OpenLoader:false,
    GotOThankYou:false,
    Cartloader:true,
    OpenConfirmcod:false,
    AddressCOUNT:1,
    WalletSelected:true,
    walletDed:1,
    afterConfirmLoader:false,
    isButtonDisabled:false,
    Paypalbtn:'proceedToPayBtn',
    CartBtnLoader:false,
    DisableProccedTopay:true,
    ShowClickedWallerCod:false,
  }
  // options = {
  //   "key": "rzp_live_pvrJGzjDkVei3G", //Paste your API key here before clicking on the Pay Button. 
  //   // "key": "rzp_test_jxY6Dww4U2KiSA",
  //   "name": `test`,
  //   "amount" : `${this.props.TotalPrice}00`,
  //   "Order Id": `${this.props.RAZORPAY}`, //Razorpay Order id
  //   "currency" : "INR",
  //   "description": "Test Description",
  
  //   "handler": function (response){    
  //       const razorpay_payment_id=response.razorpay_payment_id;
  //       const razorpay_order_id=response.razorpay_order_id;
  //       const razorpay_signature=response.razorpay_signature;
  //       console.log(response);
        
  //       console.log(razorpay_payment_id)
  //       console.log(razorpay_order_id)
  //       console.log(razorpay_signature)
  //       // alert(response.razorpay_payment_id);
  //       // alert(response.razorpay_order_id);
  //       // alert(response.razorpay_signature);
        
        
  
  //         // alert(response.razorpay_order_id)
  //         // alert(response.razorpay_signature)
  //                                 },
  
  //   "prefill": {
  //       // "method":"card,netbanking,wallet,emi,upi",
  //       "contact" : '9123337544',
  //       "email" : `email@email.com`
  //   },
  //          //             "notes": {
  //     //                     "order_id": "your_order_id",
  //     //                                 "transaction_id": "your transaction_id",
  //     //                                 "Receipt": “your_receipt_id"
  //     //             },
  //   "notes": {
  //       "Order Id": this.props.OrderId, //"order_id": "your_order_id",
  //       "address" : "customer address to be entered here"
  //   },
  //   "theme": {
  //       "color": "#1c92d2",
  //        "emi_mode" : true
                
  //   },
    
  //   external: {
  //  wallets: ['mobikwik' , 'paytm' , 'jiomoney' , 'payumoney'],
  //   handler: function(data) {
  //   // console.log(this, data)
  //   }
  // }
  // };
  componentDidMount(){

    ReactGA.pageview(window.location.pathname + window.location.search);
    

    // alert(window.screen.availWidth)
    
    window.scrollTo(0, 0);

    this.setState({AddressCOUNT:1,GotOThankYou:false});
    this.props.SetOrderId()
    if(localStorage.getItem('user') === null){
      // alert("RED")
      var RedirectUrl="/"
    // alert((window.innerWidth > 0) ? `${window.innerWidth} aa` : window.screen.width)
    if(window.innerWidth > 0){
      if(window.innerWidth <991){
        // RedirectUrl="/login"
      }
    }
      // window.location.href=RedirectUrl
    }else{
      const details=`Token ${localStorage.getItem('user')}`
      this.props.SetWallet(details)
    }

    try {
        if(this.props.walletbalance <= 0){
          this.setState({WalletSelected : false})
        }
    } catch (error) {
    
    }
    this.setState({ token: localStorage.getItem('user') });

    const details = `Token ${localStorage.getItem('user')}`
    console.log(details);
    this.props.Getwalletd(details)
    // this.props.userdetails(details)
    this.props.Getaddress(details)
    this.props.SetSelectedAddressBlank()


  }
  componentWillReceiveProps(nextProps){
    if(nextProps.ItemsInCart === 0 && this.state.AddressCOUNT === 1){
      // alert("removed")
      this.props.SetSelectedAddressBlank();
      this.setState({AddressCOUNT:2});
      if(this.props.walletbalance < 0 ){
        this.setState({WalletSelected:false})}
    }
    if(nextProps.walletbalance != this.props.walletbalance)
    {
      if(nextProps.walletbalance <= 0){
        this.setState({WalletSelected : false})
      }
    }
    if(nextProps.OutOfStockBooks !== this.props.OutOfStockBooks){
      if(nextProps.OutOfStockBooks.length !== 0){
        this.setState({CartBtnLoader:false})
      }
    }
    if(this.props.SelectedAddress !== nextProps.SelectedAddress){
      if(nextProps.SelectedAddress.length !== 0)
      this.setState({DisableProccedTopay:false})
    }
    if(this.props.ItemsInCart !== nextProps.ItemsInCart ){
      if(nextProps.ItemsInCart == 0){
        this.setState({DisableProccedTopay:true})
        console.log("In0");
      }else{
        console.log("InNot 0");
      }
    }
    if(nextProps.cartDetails != this.props.cartDetails){
    // ----------------------Get all Out of stock Books and show then in the front-end---------------------
        console.log("In");
    
        const token = `Token ${localStorage.getItem('user')}`
        let AllBooksData = this.props.cartDetails
        console.log(AllBooksData,"AllBooksData");
        var getAllOutOfStockBook_Inv =[]

        AllBooksData.map((books)=>{
          console.log(books.bookInvId);
          getAllOutOfStockBook_Inv.push(books.bookInvId)
          
        })
        const BookInvdata = {
          "book_inv":getAllOutOfStockBook_Inv
        }
        // alert("okk")
        console.log(BookInvdata,"bookInvData");
        
        this.props.OutOfStock(token,BookInvdata)
    }
  }
  openModal (){
    this.setState({ open: !this.state.open })
  }
  closeModal () {
    this.setState({ open: false })
  }
  // RemoveFromCart=(bookInvId)=>{

  //   for (var i=0; i<=sessionStorage.length-1; i++)  
  //   {   
  //     // alert("rmo")
  //       let key = sessionStorage.key(i);  
  //       let val = JSON.parse(sessionStorage.getItem(key));
  //       if(`${val.bookInvId}` === `${bookInvId}`)  {
  //         // alert("rm")
  //         sessionStorage.removeItem(bookInvId);
  //       }
  //       // val.map(det=>console.log(det))
  //       // this.props.AddToCart(val)
  //       // alert(val)
  //   }
  //   this.props.RemoveCart(bookInvId)

  //  }

  RemoveFromCart=(bookInvId,Cart_id)=>{

    for (var i=0; i<=sessionStorage.length-1; i++)  
    {   
      // alert("rmo")
        let key = sessionStorage.key(i);  
        try {
          if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){
              let val = JSON.parse(sessionStorage.getItem(key));
              if(`${val.bookInvId}` === `${bookInvId}`)  {
                // alert("rm")
                sessionStorage.removeItem(bookInvId);
              }
          }
        } catch (error) {
          console.log(error);
          
        }

        // val.map(det=>console.log(det))
        // this.props.AddToCart(val)
        // alert(val)
    }
    if(localStorage.getItem('user') === null){

    this.props.removeFromCartLogout(bookInvId)
    }else{
      const data={"is_deleted":"Y"}
    this.props.RemoveCart(Cart_id,bookInvId,data)

    }
   }

  // slug=this.props.match.params.slug
  // componentDidMount(){
  //     this.props.getBook(this.slug)
  //   //   const {compExamBooks} = this.props
  //   console.log(this.slug);
    
  // };
  static contextTypes = {
    router: () => true, // replace with PropTypes.object if you use them
  }

  render() {
    console.log(this.props.OutOfStockBooks,"dataTest");

    if(localStorage.getItem('user') === null){
      var RedirectUrl="/"
      // alert((window.innerWidth > 0) ? `${window.innerWidth} aa` : window.screen.width)
      if(window.innerWidth > 0){
        if(window.innerWidth <991){
          RedirectUrl="/login"
          return <Redirect to="/login"/>
        }
      // return <Redirect to="/"/>
      }
    }
    if(this.state.GotOThankYou){
      return <Redirect to="view-cart/thank-you"/>
    }
  //   const OrderData={
  //     data :{
  //    amount  : this.props.CartPrice.TotalPayment,
  //    no_of_book : 2,
  //    payusing : "razorpay",
  //    billing_add_id : 15753,
  //    shipping_add_id : 15753,
  //    }
     
  //  }
   const token= `Token ${this.props.userToken}`
    // var totalShipping=0
    // console.log(this.props.book)
    // const {book} = this.props

    // const thumb=book.map(data=>(data.thumb))
    // const src=`https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/medium/${thumb}`
    // const src=`https://s3.ap-south-1.amazonaws.com/mypustak-3/uploads/books/thumbs/${thumb}`
    // console.log(thumb[0]);
    var ShipCost =0
    var CashCollection =0
    var NewWalletBal = 0
    var NewTotalPaymet= 0
    const {cartDetails} = this.props
    cartDetails.map(cart=> ShipCost+=Number(cart.bookShippingCost))
    // this.setState({totalShipping:Cost})
    // console.log(ShipCost)
    var TotalPayment =0
    var CodTotalPayment =0 
    // {(this.state.paytype==="cod")?"150":"80"}
    // if()

    //Adding Wallet Balanace
    if(this.state.paytype === 'cod'){
      // if(ShipCost>=150){
      //       TotalPayment=ShipCost+this.state.above
      //       CashCollection = this.state.above
      //       CodTotalPayment = TotalPayment
      //       if(TotalPayment >= Number(this.props.walletbalance) && this.state.WalletSelected){
      //         // alert(TotalPayment)
      //         TotalPayment=TotalPayment-Number(this.props.walletbalance)
      //         // alert(TotalPayment)
      //         // CashCollection = this.state.above
      //       }
      //     }
      //   else{
      //       TotalPayment=ShipCost+this.state.below
      //       CashCollection = this.state.below
      //     }
      if(ShipCost>=150){
        TotalPayment=ShipCost+this.state.above
        CashCollection = this.state.above
      }
        
        else{
        TotalPayment=ShipCost+this.state.below
        CashCollection = this.state.below
      }
    }
    // else if(this.state.paytype === 'prepaid' && this.state.WalletSelected){
    //   // console.log("In Preapai wallet");
    //   TotalPayment = ShipCost
    //    if(TotalPayment >= Number(this.props.walletbalance)){
    //       // console.log("In Preapai wallet Comp");

    //           // alert(TotalPayment)
    //           TotalPayment=TotalPayment-Number(this.props.walletbalance)
    //           CashCollection = 0
    //         }
    // }
    else if(this.state.paytype === 'prepaid' ){


      TotalPayment = ShipCost
      CashCollection = 0
            if(TotalPayment > Number(this.props.walletbalance) && this.state.WalletSelected && (Number(this.props.walletbalance) > 0)){
              console.log("In Preapai wallet Comp");
                  NewTotalPaymet=TotalPayment
                  // alert(TotalPayment)
                  TotalPayment=TotalPayment-Number(this.props.walletbalance)
                  CashCollection = 0
            }else if(TotalPayment <= Number(this.props.walletbalance) && this.state.WalletSelected && (Number(this.props.walletbalance) > 0)){
              NewTotalPaymet=TotalPayment
              console.log("Changeing NewTOtal payment");
              
              if(ShipCost > 80)
              TotalPayment=0
              CashCollection = 0
              // if(TotalPayment<80){
              if(ShipCost< 80){
                NewTotalPaymet = 80
              }
              // }
              // NewWalletBal=Number(this.props.walletbalance)-TotalPayment
            }else {
              NewTotalPaymet = ShipCost
            }
    }
    // else if(this.state.paytype === 'cod' ){
    //      if(ShipCost>=150){
    //         TotalPayment=ShipCost+this.state.above
    //         CashCollection = this.state.above
            
    //       }
    //     else{
    //         TotalPayment=ShipCost+this.state.below
    //         CashCollection = this.state.below
    //       }
    // }
    else{
      // TotalPayment = ShipCost
      // CashCollection = 0
      //  if(TotalPayment >= this.props.walletbalance){
      //         // alert(TotalPayment)
      //         TotalPayment=TotalPayment-Number(this.props.walletbalance)
      //       }
    }
  // **********************End Adding Wallet Balanace******************************

  //   ***************** Now in use***************
  //   if(this.state.paytype === 'cod'){
  //     if(ShipCost>=150){
  //           TotalPayment=ShipCost+this.state.above
  //           CashCollection = this.state.above}
  //       else{
  //           TotalPayment=ShipCost+this.state.below
  //           CashCollection = this.state.below}
  //   }else{
  //     TotalPayment = ShipCost
  //     CashCollection = 0
  //   }
  // // End Now in use



  // if(this.state.paytype === 'cod'){
  //   if(ShipCost>=150){
  //     TotalPayment=ShipCost+this.state.above
  //     CashCollection = this.state.above}
      
  //     else{
  //     if(ShipCost < 80){
  //       TotalPayment=ShipCost
        
  //     }else{
  //     TotalPayment=ShipCost+this.state.below
  //     CashCollection = this.state.below}
  //   }
  //   }else{
  //     TotalPayment = ShipCost
  //     CashCollection = 0
  //   }
  // console.log(NewTotalPaymet,"NTP",TotalPayment);
  
     const PopupOpenDiveryAdd=()=>{
      this.setState({openDelivery:true})
      }
      const MakePayment=()=>{
        this.setState({CartBtnLoader:true})
        if(ShipCost < 80 ){
          NewTotalPaymet = 80
        }
        // alert("Mkae")
        console.log("in MakePay",NewTotalPaymet <= this.props.walletbalance,this.state.paytype === "prepaid",this.state.WalletSelected);
        
        // PopupOpenDiveryAdd()
        if(this.state.paytype === "cod"){
          this.setState({OpenConfirmcod:true})
        }
        // else if( TotalPayment === 0){
        //   // this.setState({OpenConfirmcod:true})
        // }
        else if( this.state.paytype === "prepaid" && this.state.WalletSelected && NewTotalPaymet <= this.props.walletbalance){
          console.log(NewTotalPaymet,this.props.walletbalance,"Wall");
          
        this.setState({OpenConfirmcod:true})
       }

       else if( this.state.paytype === "prepaid" && this.state.WalletSelected && NewTotalPaymet > this.props.walletbalance){
        // this.setState({OpenConfirmcod:true})
        console.log(NewTotalPaymet,this.props.walletbalance,"Razp+wall");

        MakePaymentConfirm()
       }
      //  For Wallet and total below 80
      //  else if( this.state.paytype === "prepaid" && this.state.WalletSelected && NewTotalPaymet < this.props.walletbalance){
      //   // this.setState({OpenConfirmcod:true})
      //   console.log(NewTotalPaymet,this.props.walletbalance,"80 below");

      //  }
        else{
          console.log(NewTotalPaymet,this.props.walletbalance,"3");

          MakePaymentConfirm()
          console.log("RZpay 1");
          
        }
        // alert("")
        console.log(NewTotalPaymet,this.props.walletbalance,"out");

      }
      const MakePaymentConfirm=(e)=>{
        // alert("Okk")
        console.log(e);
        this.setState({isButtonDisabled:true})
        // New code for min order price 80
        if(ShipCost < 80 && this.state.paytype === 'cod'){
          TotalPayment=150
          // if(TotalPayment >= Number(this.props.walletbalance) && this.state.WalletSelected){
          //   // alert(TotalPayment)
          //   TotalPayment=TotalPayment-Number(this.props.walletbalance)
          //   // alert(TotalPayment)
          //   // CashCollection = this.state.above
          // }
          
        }else if(ShipCost < 80 && this.state.paytype !== 'cod')
        {
          // TotalPayment=80
          // if(TotalPayment <= Number(this.props.walletbalance) && this.state.WalletSelected){
          //   console.log("In Preapai wallet Comp T<W");
          //   NewTotalPaymet=80
          //       // alert(TotalPayment)
          //       // NewWalletBal=Number(this.props.walletbalance)-TotalPayment
          //       // TotalPayment=TotalPayment
          //       CashCollection = 0
          //       console.log(TotalPayment,);
                
          //     }
            NewTotalPaymet=80


        }
        // else if(TotalPayment === 0){
        //   alert("okk")
        // }
        else{

        }
        if(this.state.paytype === 'cod'){
          this.setState({OpenConfirmcod:false})

        }
        // End New code for min order price 80
        this.setState({OpenLoader:true});
        const cartData={
         bookShippingCost:ShipCost,
         TotalPayment:Math.round(TotalPayment),
         CashCollection,
         paytype:this.state.paytype
        }
       this.props.AddPriceCart(cartData)

      //  this.setState({openDelivery:true})
      OrderDeiailsApi()
       }

       const OrderDeiailsApi=()=>{
        console.log("RZpay 2");

         console.log(NewTotalPaymet,this.props.walletbalance,NewTotalPaymet<=this.props.walletbalance)
        //  const amount = `this`
        // console.log(TotalPayment.toFixed(2)*100);
        if(this.state.paytype === "cod"){
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
          this.props.cartDetails.map((book,index)=>{
             
            AllbookId.push(`${book.bookId}`);
            AllbookInvId.push(book.bookInvId);
            AllrackNo.push(book.bookRackNo);
            AllQty.push(book.bookQty);
           //  index;
          })
        // if(ShipCost >= 150)
        //   {this.state.above}
        // else{this.state.below}
        const COdOrderData={
          data :{
            // "amount"  : this.props.TotalPrice,
            // "amount":Math.floor(TotalPayment*100),
            "amount":Math.round(TotalPayment),
            "no_of_book" : this.props.ItemsInCart,
            "payusing" : "cod",
            "billing_add_id" : this.props.AddresId,
            "shipping_add_id" : this.props.AddresId,
            "cod_charge":CashCollection,
            "book_id":AllbookId,
            "book_inv_id":AllbookInvId,
            "rack_no":AllrackNo,
            "qty":AllQty,
                }  
          }
          
        this.props.OrderDetails(COdOrderData,token)
        // console.log(OrderData,token);
        // if(this.props.OrderId !== 0){
        // GetRzPayId()
        // }

        this.setState({COUNT:2})
          const cartData={
            bookShippingCost:ShipCost,
            TotalPayment:Math.round(TotalPayment),
            CashCollection,
            paytype:this.state.paytype
          }
          this.props.AddPriceCart(cartData)

          // alert("o")
          // return <Redirect to="thank-you"/> 
          // if()
          // this.setState({GotOThankYou:true})
        }
      // else if(TotalPayment === 0 && this.state.WalletSelected && NewTotalPaymet === Number(this.props.walletbalance)){
      //     const WalletOrderData={
      //       data :{
      //         // "amount"  : this.props.TotalPrice,
      //         // "amount":Math.floor(TotalPayment*100),
      //         "amount":Math.round(this.props.walletbalance),
      //         "no_of_book" : this.props.ItemsInCart,
      //         "payusing" : "wallet",
      //         "billing_add_id" : this.props.AddresId,
      //         "shipping_add_id" : this.props.AddresId,
      //         "cod_charge":CashCollection,
      //             }  
      //       }
            
      //     this.props.OrderDetails(WalletOrderData,token)
      //     // console.log(OrderData,token);
      //     // if(this.props.OrderId !== 0){
      //     // GetRzPayId()
      //     // }

      // // Hit the wallet api to reduce the wallet balance
      // var today = new Date();
      // var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      // var dateTime = date+' '+time;
      // //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
      // const SendData={
      //   "user_id":`${this.props.UserId}`,
      //   "user_email":`${this.props.UserEmail}`,
      //   "transaction_id":`PaidFromWallet`,
      //   "deposit": 0,
      //   "withdrawl":Math.round(this.props.walletbalance),
      //   "payvia":"wallet",
      //   "time": `${dateTime}`,
      //   "comment":"Paid From Wallet",
      //   "added_by":`${this.props.UserEmail}`
      // }
      // // axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendData,{
      // //   headers:{'Authorization':`Token ${this.props.userToken}`,
      // //             'Content-Type':'application/json'}
      // // })
      // // .then(res=>{
      // //   // console.log(res,"Done ");  
      // //   this.setState({SuccessWalletAdded:true})

      // // })
      // //   .catch(err=>console.log(err,SendData))
  
      //     this.setState({COUNT:2})
      //   }
        else if(this.state.WalletSelected && this.state.paytype === "prepaid" && Number(NewTotalPaymet) <= Number(this.props.walletbalance)){
          console.log(" Prepaid & wallet ");
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
          this.props.cartDetails.map((book,index)=>{
             
            AllbookId.push(`${book.bookId}`);
            AllbookInvId.push(book.bookInvId);
            AllrackNo.push(book.bookRackNo);
            AllQty.push(book.bookQty);
           //  index;
          })
       
          const WalletOrderData={
            data :{
              // "amount"  : this.props.TotalPrice,
              // "amount":Math.floor(TotalPayment*100),
              "amount":Math.round(NewTotalPaymet),
              "no_of_book" : this.props.ItemsInCart,
              "payusing" : "wallet",
              "billing_add_id" : this.props.AddresId,
              "shipping_add_id" : this.props.AddresId,
              "cod_charge":CashCollection,
              "wallet_used" : Math.round(NewTotalPaymet), 
              "book_id":AllbookId,
              "book_inv_id":AllbookInvId,
              "rack_no":AllrackNo,
              "qty":AllQty,
                  }  
            }
            // NewTotalPaymetNewTotalPaymet
          this.props.OrderDetails(WalletOrderData,token)
          // console.log(OrderData,token);
          // if(this.props.OrderId !== 0){
          // GetRzPayId()
          // }

      //     // Hit the wallet api to reduce the wallet balance
      // var today = new Date();
      // var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      // var dateTime = date+' '+time;
      // //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
      // const SendData={
      //   "user_id":`${this.props.UserId}`,
      //   "user_email":`${this.props.UserEmail}`,
      //   "transaction_id":`PaidFromWallet${Math.round(NewTotalPaymet)}`,
      //   "deposit": 0,
      //   "withdrawl":Math.round(NewTotalPaymet),
      //   "payvia":"wallet",
      //   "time": `${dateTime}`,
      //   "comment":"Paid From Wallet",
      //   "added_by":`${this.props.UserEmail}`
      // }
      // axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendData,{
      //   headers:{'Authorization':`Token ${this.props.userToken}`,
      //             'Content-Type':'application/json'}
      // })
      // .then(res=>{
      //   console.log("Wallet Transaction Done ");  
      //   this.setState({SuccessWalletAdded:true})

      // })
      //   .catch(err=>console.log(err,SendData))
  
          this.setState({COUNT:2})
          const cartData={
            bookShippingCost:ShipCost,
            TotalPayment:Math.round(NewTotalPaymet),
            CashCollection,
            paytype:"wallet",
            transaction_id:`PaidFromWallet${Math.round(NewTotalPaymet)}`,
            wallet:Math.round(NewTotalPaymet),
           }
          this.props.AddPriceCart(cartData)
          // this.setState({GotOThankYou:true})
          
        }
        // Prepaid + wallet + Total > Wallet
        else if(this.state.WalletSelected && this.state.paytype === "prepaid" && Number(NewTotalPaymet) >= Number(this.props.walletbalance)){
        //   if(this.state.paytype === 'prepaid' ){

        // }
        // 80 &bwB if 80-> if wb > 80 withdrae =80 rz =0 else wb<80 with = wb rz= 80-wbb
        console.log("RZPay Greater");
        console.log(NewTotalPaymet,"NewTot",TotalPayment,"Tot",this.props.walletbalance);
        

// ------------------------- Below 80 using Walllet + Rzp
        if(NewTotalPaymet <= 80 && Number(this.props.walletbalance)<80){
          // alert("WA")
          console.log("WA");
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
          this.props.cartDetails.map((book,index)=>{
             
            AllbookId.push(`${book.bookId}`);
            AllbookInvId.push(book.bookInvId);
            AllrackNo.push(book.bookRackNo);
            AllQty.push(book.bookQty);
           //  index;
          })
          const OrderData={
            data :{
              // "amount"  : this.props.TotalPrice,
              // "amount":Math.floor(TotalPayment*100),
              "amount":80,
              "no_of_book" : this.props.ItemsInCart,
              "payusing" : "razorpay+wallet",
              "billing_add_id" : this.props.AddresId,
              "shipping_add_id" : this.props.AddresId,
              "cod_charge":0,
              "wallet_used" : Number(this.props.walletbalance),
              // "wallet_used" : 0,
              "book_id":AllbookId,
              "book_inv_id":AllbookInvId,
              "rack_no":AllrackNo,
              "qty":AllQty,

                  }  
            }
          this.props.OrderDetails(OrderData,token)
          // console.log(OrderData,token);
          // if(this.props.OrderId !== 0){
          // GetRzPayId()
          // }

          this.setState({COUNT:2})
          const cartData={
            bookShippingCost:NewTotalPaymet,
            TotalPayment:80-Number(this.props.walletbalance),
            CashCollection,
            paytype:"prepaid",
            // "transaction_id":`PaidFromWallet${Math.round(NewTotalPaymet)}`,
            wallet:Number(this.props.walletbalance),
            // wallet:0,

           }
          this.props.AddPriceCart(cartData)
          const data={
            NewTotalPaymet,
            RazorpayPayment:TotalPayment,
            walletbalance:Number(this.props.walletbalance)
            }
            console.log(NewTotalPaymet,TotalPayment,this.props.walletbalance);
              this.props.walletRazorpay(data)
        }
        // For Below 80 
        else if(NewTotalPaymet > 80   ){
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
          this.props.cartDetails.map((book,index)=>{
             
            AllbookId.push(`${book.bookId}`);
            AllbookInvId.push(book.bookInvId);
            AllrackNo.push(book.bookRackNo);
            AllQty.push(book.bookQty);
           //  index;
          })
          const OrderData={
            data :{
              // "amount"  : this.props.TotalPrice,
              // "amount":Math.floor(TotalPayment*100),
              "amount":NewTotalPaymet,
              "no_of_book" : this.props.ItemsInCart,
              "payusing" : "razorpay+wallet",
              "billing_add_id" : this.props.AddresId,
              "shipping_add_id" : this.props.AddresId,
              "cod_charge":0,
              "wallet_used" : Number(this.props.walletbalance),
              // "wallet_used" : 0,
              "book_id":AllbookId,
              "book_inv_id":AllbookInvId,
              "rack_no":AllrackNo,
              "qty":AllQty,

                  }  
            }
          this.props.OrderDetails(OrderData,token)
          // console.log(OrderData,token);
          // if(this.props.OrderId !== 0){
          // GetRzPayId()
          // }

          this.setState({COUNT:2})
          const cartData={
            bookShippingCost:NewTotalPaymet,
            TotalPayment:Math.round(NewTotalPaymet)-Number(this.props.walletbalance),
            CashCollection,
            paytype:"prepaid",
            // "transaction_id":`PaidFromWallet${Math.round(NewTotalPaymet)}`,
            wallet:Number(this.props.walletbalance),
            // wallet:0,

           }
          this.props.AddPriceCart(cartData)
        }
        else{
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
          this.props.cartDetails.map((book,index)=>{
             
            AllbookId.push(`${book.bookId}`);
            AllbookInvId.push(book.bookInvId);
            AllrackNo.push(book.bookRackNo);
            AllQty.push(book.bookQty);
           //  index;
          })
            // alert("ele")
          const OrderData={
            data :{
              // "amount"  : this.props.TotalPrice,
              // "amount":Math.floor(TotalPayment*100),
              "amount":Math.round(NewTotalPaymet),
              "no_of_book" : this.props.ItemsInCart,
              "payusing" : "razorpay",
              "billing_add_id" : this.props.AddresId,
              "shipping_add_id" : this.props.AddresId,
              "cod_charge":0,
              "wallet_used" : Number(this.props.walletbalance),
              // "wallet_used" : 0,
              "book_id":AllbookId,
              "book_inv_id":AllbookInvId,
              "rack_no":AllrackNo,
              "qty":AllQty,

                  }  
            }
          this.props.OrderDetails(OrderData,token)
          console.log(OrderData,token);
          // if(this.props.OrderId !== 0){
          // // GetRzPayId()
          // }

          this.setState({COUNT:2})
          const cartData={
            bookShippingCost:TotalPayment,
            TotalPayment:Math.round(TotalPayment),
            CashCollection,
            paytype:"prepaid",
            // "transaction_id":`PaidFromWallet${Math.round(NewTotalPaymet)}`,
            wallet:Math.round(NewTotalPaymet)-TotalPayment,
            // wallet:0,

           }
          this.props.AddPriceCart(cartData)
        }
            // ******************************* For 400 Error ********************************************
        // this.setState({OpenLoader:false});
        // this.setState({OpenProceedPayBtn:true})

        }  else if(this.state.paytype === 'prepaid' && this.state.WalletSelected === false  ){ 
          //   if(this.state.paytype === 'prepaid' ){
  
          // }
          console.log("RZPay 4");
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
          this.props.cartDetails.map((book,index)=>{
             
            AllbookId.push(`${book.bookId}`);
            AllbookInvId.push(book.bookInvId);
            AllrackNo.push(book.bookRackNo);
            AllQty.push(book.bookQty);
           //  index;
          })
         const data={
          NewTotalPaymet,
          RazorpayPayment:TotalPayment,
          walletbalance:Number(this.props.walletbalance)
          }
          console.log(NewTotalPaymet,TotalPayment,this.props.walletbalance);
            // this.props.walletRazorpay(data)
            const OrderData={
              data :{
                // "amount"  : this.props.TotalPrice,
                // "amount":Math.floor(TotalPayment*100),
                "amount":Math.round(NewTotalPaymet),
                "no_of_book" : this.props.ItemsInCart,
                "payusing" : "razorpay",
                "billing_add_id" : this.props.AddresId,
                "shipping_add_id" : this.props.AddresId,
                "cod_charge":0,
                // "wallet_used" : Math.round(NewTotalPaymet)-TotalPayment,
                "wallet_used" : 0,
                "book_id":AllbookId,
                "book_inv_id":AllbookInvId,
                "rack_no":AllrackNo,
                "qty":AllQty,
  
                    }  
              }
            this.props.OrderDetails(OrderData,token)
            // console.log(OrderData,token);
            // if(this.props.OrderId !== 0){
            // GetRzPayId()
            // }
  
            this.setState({COUNT:2})
            const cartData={
              bookShippingCost:ShipCost,
              TotalPayment:Math.round(NewTotalPaymet),
              CashCollection,
              paytype:"prepaid",
              // "transaction_id":`PaidFromWallet${Math.round(NewTotalPaymet)}`,
              // wallet:Math.round(NewTotalPaymet)-TotalPayment,
              wallet:0,
  
             }
            this.props.AddPriceCart(cartData)

            // ******************************* For 400 Error ********************************************
          // this.setState({OpenProceedPayBtn:true})
          // this.setState({OpenLoader:false});
  
          }
        
        else{

        }
       }
// ***********************for cod order

       if(this.props.OrderId !== 0 && this.state.paytype === "cod"){
        // getName(){
          if(this.state.afterConfirmLoader != true){
          this.setState({"afterConfirmLoader":true})
          }
          var AllbookId=[]
          var AllbookInvId=[]
          var AllrackNo=[]
          var AllQty=[]
         
           this.props.cartDetails.map((book,index)=>{
             
             AllbookId.push(`${book.bookId}`);
             AllbookInvId.push(book.bookInvId);
             AllrackNo.push(book.bookRackNo);
             AllQty.push(book.bookQty);
            //  index;
           })
          console.log(AllbookId);
          
          const SendData={
            // {"payment_id":"12","payment_url":"www.test.com"}
            "payment_id":this.props.OrderId,
            "payment_url":'cod',
            "book_id":AllbookId,
            "book_inv_id":AllbookInvId,
            "rack_no":AllrackNo,
            "qty":AllQty,
          }
          axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
              // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
              'Authorization': `Token ${this.props.userToken}`,
              // ''
            }})
            .then(res=>{
              console.log(res,SendData);
              // this.props.orderUpdateSucc()
            this.setState({GotOThankYou:true})

            })
            .catch(err=>console.log(err,SendData) 
            )
          // }
          // )
            // this.setState({GotOThankYou:true})
          // }
        }
// ***********************for wallet paymet in prepaid
        if(this.props.OrderId !== 0 && this.state.WalletSelected && this.state.paytype === 'prepaid' && this.state.walletDed === 1){
          console.log("Prepaid and wallet");
          if(this.state.afterConfirmLoader != true){
            this.setState({"afterConfirmLoader":true})
            }
          if(NewTotalPaymet <= 80 && Number(this.props.walletbalance)<80){
            // Deduct After Rzpay
         console.log("Okk Prepaid 0 1")
                  const data={
          NewTotalPaymet,
          RazorpayPayment:TotalPayment,
          walletbalance:Number(this.props.walletbalance)
          }
          console.log(NewTotalPaymet,TotalPayment,this.props.walletbalance);
            // this.props.walletRazorpay(data)

                    // Hit the wallet api to reduce the wallet balance
        // var today = new Date();
        // var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        // var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        // var dateTime = date+' '+time;
        // //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
        // const SendDataWallet={
        //   "user_id":`${this.props.UserId}`,
        //   "user_email":`${this.props.UserEmail}`,
        //   "transaction_id":`PaidFromWallet${Math.round(this.props.walletbalance)}`,
        //   "deposit": 0,
        //   "withdrawl":Math.round(this.props.walletbalance),
        //   "payvia":"wallet",
        //   "time": `${dateTime}`,
        //   "comment":"Paid From Wallet",
        //   "added_by":`${this.props.UserEmail}`
        // }
        // axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendDataWallet,{
        //   headers:{'Authorization':`Token ${this.props.userToken}`,
        //             'Content-Type':'application/json'}
        // })
        // .then(res=>{
        //   console.log("Wallet Transaction Done ");  
        //   this.setState({SuccessWalletAdded:true,walletDed:2})

        // })
        //   .catch(err=>console.log(err,SendDataWallet))


        //     // getName(){
        //       var AllbookId=[]
        //       var AllbookInvId=[]
        //       var AllrackNo=[]
        //       var AllQty=[]
            
              // this.props.cartDetails.map((book,index)=>{
                
              //   AllbookId.push(`${book.bookId}`);
              //   AllbookInvId.push(book.bookInvId);
              //   AllrackNo.push(book.bookRackNo);
              //   AllQty.push(book.bookQty);
              //   //  index;
              // })
              // console.log(AllbookId);
              
              // const SendData={
              //   // {"payment_id":"12","payment_url":"www.test.com"}
              //   "payment_id":this.props.OrderId,
              //   "payment_url":'prepaid',
              //   "book_id":AllbookId,
              //   "book_inv_id":AllbookInvId,
              //   "rack_no":AllrackNo,
              //   "qty":AllQty,
              //   "wallet_used" : Math.round(this.props.CartPrice.wallet)
              // }
              // axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
              //     // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
              //     'Authorization': `Token ${this.props.userToken}`,
              //     // ''
              //   }})
              //   .then(res=>console.log(res,SendData))
              //   .catch(err=>console.log(err,SendData) 
              //   )
          }
          //  Whene Wallet Balance is < NewTota
          else if(Math.round(NewTotalPaymet) <= this.props.walletbalance && this.state.walletDed === 1){
        this.setState({walletDed:2})

          console.log(" new < WB")

                  // Hit the wallet api to reduce the wallet balance
      var today = new Date();
      var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
      var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
      var dateTime = date+' '+time;
      //{"user_id":"1","user_email":"a","transaction_id":"sdfd","deposit":"dfs","withdrawl":"sdf","payvia":"sdf","comment":"sdf","time":"2019-03-08 16:33:41","added_by":"sdf"}
      const SendDataWallet={
        "user_id":`${this.props.UserId}`,
        "user_email":`${this.props.UserEmail}`,
        "transaction_id":`PaidFromWallet${Math.round(NewTotalPaymet)}`,
        "deposit": 0,
        "withdrawl":Math.round(NewTotalPaymet),
        "payvia":"wallet",
        "time": `${dateTime}`,
        "comment":"Paid From Wallet",
        "added_by":`${this.props.UserEmail}`
      }
      axios.post(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/add-wallet`,SendDataWallet,{
        headers:{'Authorization':`Token ${this.props.userToken}`,
                  'Content-Type':'application/json'}
      })
      .then(res=>{
        console.log("Wallet Transaction Done ");  
        this.setState({SuccessWalletAdded:true,walletDed:2})

      })
        .catch(err=>console.log(err,SendData))


          // getName(){
            var AllbookId=[]
            var AllbookInvId=[]
            var AllrackNo=[]
            var AllQty=[]
           
             this.props.cartDetails.map((book,index)=>{
               
               AllbookId.push(`${book.bookId}`);
               AllbookInvId.push(book.bookInvId);
               AllrackNo.push(book.bookRackNo);
               AllQty.push(book.bookQty);
              //  index;
             })
            console.log(AllbookId);
            
            const SendData={
              // {"payment_id":"12","payment_url":"www.test.com"}
              "payment_id":this.props.OrderId,
              "payment_url":'prepaid',
              "book_id":AllbookId,
              "book_inv_id":AllbookInvId,
              "rack_no":AllrackNo,
              "qty":AllQty,
              "wallet_used" : Math.round(this.props.CartPrice.wallet)
            }
            axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
                // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
                'Authorization': `Token ${this.props.userToken}`,
                // ''
              }})
              .then(res=>{console.log(res,SendData);
                  this.setState({GotOThankYou:true})
              })
              .catch(err=>console.log(err,SendData) 
              )
            // }
            // )
            // this.setState({GotOThankYou:true})
            const data={
              NewTotalPaymet,
              RazorpayPayment:TotalPayment,
              walletbalance:Number(this.props.walletbalance)
              }
              console.log(NewTotalPaymet,TotalPayment,this.props.walletbalance);
                this.props.walletRazorpay(data)
          // this.setState({GotOThankYou:true})


          }

            // }
          }
// ***********************for wallet paymet with 0 to pay in total

        // if(this.props.OrderId !== 0 && Number(TotalPayment)===0 ){
        //   // getName(){
        //     var AllbookId=[]
        //     var AllbookInvId=[]
        //     var AllrackNo=[]
        //     var AllQty=[]
           
        //      this.props.cartDetails.map((book,index)=>{
               
        //        AllbookId.push(`${book.bookId}`);
        //        AllbookInvId.push(book.bookInvId[0]);
        //        AllrackNo.push(book.bookRackNo[0]);
        //        AllQty.push(book.bookQty[0]);
        //       //  index;
        //      })
        //     console.log(AllbookId);
            
        //     const SendData={
        //       // {"payment_id":"12","payment_url":"www.test.com"}
        //       "payment_id":this.props.OrderId,
        //       "payment_url":'prepaid',
        //       "book_id":AllbookId,
        //       "book_inv_id":AllbookInvId,
        //       "rack_no":AllrackNo,
        //       "qty":AllQty,
        //     }
        //     // axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
        //     //     // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
        //     //     'Authorization': `Token ${this.props.userToken}`,
        //     //     // ''
        //     //   }})
        //     //   .then(res=>console.log(res,SendData))
        //     //   .catch(err=>console.log(err,SendData) 
        //     //   )
        //     // }
        //     // )
        //       this.setState({GotOThankYou:true})
        //     // }
        //   }
       if(this.props.TotalPrice !== 0 && this.props.OrderId !== 0 && this.state.COUNT === 2){
        console.log("Okk Prepaid RZp")

        // alert(`${typeof this.props.OrderId} O ${typeof this.props.TotalPrice}`)
         
        const SendDataRzpzy={
          data:{
           "ref_id" : this.props.OrderId,
           "amount" : Math.round(this.props.TotalPrice)*100
          //  "amount" : this.props.TotalPrice*100
          //  "ref_id" : 515776,
          //  "amount" : 70.2*100
          }
        }
        this.props.Get_Rp_Id(SendDataRzpzy)
        this.setState({OpenProceedPayBtn:true})
        this.setState({COUNT:3})
        this.setState({OpenLoader:false});
        }
        if(this.props.RAZORPAY !==0 && this.state.COUNT === 3){
          this.setState({Cartloader:false})
          this.setState({COUNT:5})
        }
      //  const GetRzPayId=()=>{
      //   //  alert("mmm")
      //   if(this.props.TotalPrice !== 0 && this.props.OrderId !== 0 && this.props.RAZORPAY === 0){
      //     alert(`${this.props.OrderId} O ${this.props.TotalPrice}`)
           
      //     const SendDataRzpzy={
      //       data:{
      //        // "ref_id" : this.props.OrderId,
      //        // "amount" : this.props.TotalPrice
      //        "ref_id" : this.props.OrderId,
      //        "amount" : Math.floor(this.props.TotalPrice)
      //       }
      //     }
      //     this.props.Get_Rp_Id(SendDataRzpzy)
      //     this.setState({OpenProceedPayBtn:true})
      //     }
      //  }
// console.log(TotalPayment);

     const onChange = e => {
       this.setState({[e.target.name]:e.target.value});
        // alert(e.target.value)
        if(e.target.value === "cod" ){
        this.setState({WalletSelected:false,Paypalbtn:'NoproceedToPayBtn'})
      
      }else{
        this.setState({Paypalbtn:'proceedToPayBtn'})

      }
      // if(e.target.value !== "cod" && this.props.WalletSelected ){
      //   this.setState({Paypalbtn:'NoproceedToPayBtn'})
      // }
      // if(this.state.paytype === "cod"){
      //   this.setState({})
      // }

      const cartData={
        bookShippingCost:ShipCost,
        TotalPayment:Math.round(TotalPayment),
        CashCollection,
       }
      // this.props.AddPriceCart(cartData)
                      };
      const OpenProceedPayBtn=()=>{
        this.setState({OpenProceedPayBtn:true})
      }
      const {SelectedAddress} =this.props
      // const {cartDetails} = this.props
      var TotalShipCost =0
      cartDetails.map(cart=> TotalShipCost+=Number(cart.bookShippingCost) )
      const WalletOnChange=()=>{
        // alert("Wal")
        if(Number(this.props.walletbalance) > 0){
          // alert("above 0")

            if(this.state.paytype === "cod"){
              // alert("in cod ")
              this.setState({WalletSelected:false})
            } else{
            // alert("togel")
            // this.setState({Paypalbtn:'NoproceedToPayBtn'})
              this.setState({WalletSelected:!this.state.WalletSelected})
        }
      }else{
        // alert("fa")
        this.setState({WalletSelected:false})
      }
      

      }
      console.log(this.props.OutOfStockBooks,"outBooks");
      
    return (
      <div>
          <Helmet>
            <script type="text/javascript" src="https://checkout.razorpay.com/v1/checkout.js">
          </script>
          <script
            src="https://www.paypal.com/sdk/js?currency=INR&client-id=AYfgkucKSrJ10n9M2KXqPAKl8wbK7fSpAauhTpDqbcV6e58T5Oo2Pr2rmYqqNxfpCSWnAfrYLDZos6LU&intent=capture&disable-funding=credit,card&commit=true&locale=en_IN">
          </script>
        </Helmet>

     <MainHeader/>
        <div id="cartBody">
        
        <p id="mycartP">My Cart({(this.props.ItemsInCart <= 0)?null:this.props.ItemsInCart}{(this.props.ItemsInCart === 1)?"Book":"Books"} )</p>

      {/* -----------------------For Other than Small Mobile---------------- */}
        <MediaQuery minWidth={539}>
        <Popup
                      open={this.props.RzPayErr}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false});this.props.ShowRzPayErr()}}
                      // closeOnDocumentClick={false}
                      contentStyle={{ }}
                      > 
                       <p style={{ textAlign:'center', }}>
                      If your order has not been generated and money has been deducted from your account . 
                      Please write us at support@mypustak.com along with your transaction details
                      so that we can resolve the issue as earliest as possible.
                      </p>
                     <div>
                      
                     </div>
                  </Popup>
                  </MediaQuery>
                  {/* -----------------------For Small Mobile---------------- */}
                  <MediaQuery maxWidth={538}>
                  <Popup
                      open={this.props.RzPayErr}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false});this.props.ShowRzPayErr()}}
                      // closeOnDocumentClick={false}
                      contentStyle={{ width:'80%'}}
                      > 
                       <p style={{ textAlign:'center', }}>
                      If your order has not been generated and money has been deducted from your account . 
                      Please write us at support@mypustak.com along with your transaction details
                      so that we can resolve the issue as earliest as possible.
                      </p>
                     <div>
                      
                     </div>
                  </Popup>
                  </MediaQuery>
                  <MediaQuery maxWidth={538}>
            <div id="cartLeftPart">
              {cartDetails.map(cart=> 
                <div id="cartBookdataD">
                            {(this.props.OutOfStockBooks.includes(cart.bookInvId))?<p id="showOutOfStock">Book Out of Stock,Please Remove It From From The Cart.</p>:(Math.round(cart.bookShippingCost) == 0)?<p id="showOutOfStock">Book Out of Stock,Please Remove It From From The Cart.</p>:null}
                
                    {/* <img src={`https://ik.imagekit.io/kbktyqbj4rrik/uploads/books/medium/${cart.bookThumb}`}  style={{ height:'144px',width:'121px',float:'left' }}/> */}
                    <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${cart.bookThumb}`}  id="cartbookimg"/>
                    <div id="cartBookinfoD">
                    
                        <div id="Bookinfopart1">
                            <p id="cartBookTitleP">{cart.bookName}            
                            </p>
                            <p id="cartBookCondP">Condition:{cart.bookCond} <br/></p>
                            {/* {(this.props.OutOfStockBooks.includes(cart.bookInvId))?<p id="showOutOfStock">Book Out of Stock,Please Remove It.</p>:null} */}

                        </div>
                        <div id="Bookinfopart2">
                        <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png" id="cartDelete" onClick={()=>this.RemoveFromCart(cart.bookInvId,cart.Cart_id)}></img>
                          <span id="cartBookpriceS">MRP: <span id="cartBookPrice">&#8377;{Math.round(cart.bookPrice)}</span><span id="cartFree">Free</span></span><br/>
                          <span id="cartBookShippingS">Shipping & Handling </span><br/>
                          <span id="CartchargeslineS">Charges</span><span id="cartBkShipChargeS">&#8377;{Math.round(cart.bookShippingCost)}</span>
                        </div>
                            
                    </div>
                 
                </div>
                          )}
            </div>
              </MediaQuery>
              <MediaQuery minWidth={539}>
            <div id="cartLeftPart">
              {cartDetails.map(cart=> 
                <div id="cartBookdataD">
                {(this.props.OutOfStockBooks.includes(cart.bookInvId))?<p id="showOutOfStock">Book Out Of Stock,Please Remove It From The Cart.</p>:(Math.round(cart.bookShippingCost) == 0)?<p id="showOutOfStock">Book Out of Stock,Please Remove It From From The Cart.</p>:isNull}

                    {/* <img src={`https://ik.imagekit.io/kbktyqbj4rrik/uploads/books/medium/${cart.bookThumb}`}  style={{ height:'144px',width:'121px',float:'left' }}/> */}
                    <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${cart.bookThumb}`}  id="cartbookimg"/>

                    <div id="cartBookinfoD">
                        <div id="Bookinfopart1">
                            <p id="cartBookTitleP">{cart.bookName} 
                            </p>
                            <p id="cartBookCondP">Condition:{cart.bookCond} </p>
                            {/* {(this.props.OutOfStockBooks.includes(cart.bookInvId))?<p id="showOutOfStock">Book Out of Stock,Please Remove It.</p>:null} */}
                        </div>
                        <div id="Bookinfopart2">

                        <img src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png" id="cartDelete" onClick={()=>this.RemoveFromCart(cart.bookInvId,cart.Cart_id)}></img>
                          <span id="cartBookpriceS">MRP: &#8377;<span id="cartBookPrice">{Math.round(cart.bookPrice)}</span><span id="cartFree">Free</span></span><br/>
                          <span id="cartBookShippingS">Shipping & Handling </span><br/>
                          <span id="CartchargeslineS">Charges</span><span id="cartBkShipChargeS">&#8377;{Math.round(cart.bookShippingCost)}</span>
                        </div>
                            
                    </div>
                    <hr/>
                </div>
               
                          )}
            </div>
            </MediaQuery>

            <div id="cartRigtoPart">
                <div id={(SelectedAddress.length === 0)?"NoDisplayCartAddress":"DisplayCartAddress"}>
                <p id="payHeaderP" ><span id="payHeader">Select Delivery Address</span>
                <span id="CartPayAnother" onClick={PopupOpenDiveryAdd}>Select Another Address ></span>
                 {/* <span id="payHeaderS">Add New Address</span> */}
                 </p>
                 <div id="PayAddressArea">
                    <div id="PayAddress">
                       {SelectedAddress.address},
                       {SelectedAddress.city_name},{SelectedAddress.pincode},<br/>
                       {SelectedAddress.state_name}

                    </div>
                    <button id="PayHomesBtn" disabled>{SelectedAddress.title}</button>
                    {/* <button id="PayEditBtn">Edit</button> */}
                 </div>
                 <hr style={{ color:'black',width:'4px',marginBottom:'0px'}}/>
                 {/* <p id="CartPayAnother" onClick={PopupOpenDiveryAdd}>Select Another Address ></p> */}
                 </div>
                <p id="cartRfirstP">Payment Summary</p>
                <p id="cartShowWallet"><div className="WalletSpanCart"><input type="checkbox" value="Wallet" id="CartWalletCheckBox" onClick={()=>(this.state.paytype=="cod")?this.setState({ShowClickedWallerCod:true}):null} onChange={()=>WalletOnChange()} checked={(Number(this.props.walletbalance) > 0)?this.state.WalletSelected:false}/> 
                {/* <span>Wallet</span></div> */}
                <label style={{position:'absolute'}}>Wallet</label></div>
                <span className="WalletSpanCart">Wallet Balance:-&#8377;{(this.props.walletbalance.length === 0)?"0":this.props.walletbalance}</span><span className="WalletSpanCart" id="CartWalletAdd" onClick={()=>this.props.RedirectWalletToCart()}><Link to="/mypustak-wallet" style={{ }}>Add Balance</Link></span></p>
                <div id="cartRfirst">
                      <div id="cartRlist">
                          <div id="cartRlistD">
                            Shipping & Handling 
                            Charge For Donated Books
                          </div>
                          <div id="cartRlistCost">
                            &#8377;{Math.round(ShipCost)}
                          </div>
                      </div>
                      <div id="cartRlist">
                        <div id="cartRlistD">
                          Cash Collection Charge <Popup trigger={<span id="QuestionMark">?</span>}
                                                    on='hover'
          
                                                    position="bottom center"
                                                  >

                                                    It is a  extra charge applicable on every CASH ON DELIVERY Order applied  by our delivery partner 
                                                    for collecting cash for the shipment value.We request you to select prepaid methods to avoid these extra charges .
                                               
                                                </Popup>
                        </div>
                        <div id="cartRlistCost">
                          &#8377;{(this.props.ItemsInCart !== 0)?CashCollection:0}
                          {/* {(this.state.paytype === 'cod')?(ShipCost >= 150)?this.state.above:this.state.below:0} */}
                        </div>
                      </div>
                      <hr/>
                      {/* <div id="cartRlist">
                          <div id="cartRlistD">
                          Total Value
                          </div>
                          <div id="cartRlistCost">
                            &#8377;{(this.state.paytype === 'cod')?TotalPayment:ShipCost}
                          </div>
                      </div> */}
                      {/* <div id="cartRlist">
                          <div id="cartRlistD">
                          (Discount Applied)
                          </div>
                          <div id="cartRlistCost">
                            &#8377;150
                          </div>
                      </div> */}
                      <hr/>
                      <div id="cartRlist">
                          <div id="cartRlistD">
                          Total Value
                          </div>
                          <div id="cartRlistCost">
                            {/* &#8377;{(this.state.paytype === 'cod')?(this.state.WalletSelected)?Math.round(CodTotalPayment):Math.round(TotalPayment):Math.round(ShipCost)} */}
                            &#8377;{(this.props.ItemsInCart !== 0)?(this.state.paytype === 'cod')?Math.round(TotalPayment):Math.round(ShipCost):0}

                          </div>
                         
                      </div>
                      {(Number(this.props.walletbalance) < 0)?null:(this.state.WalletSelected)?
                      <div id="cartRlist">
                          <div id="cartRlistD">
                          Wallet Balance
                          </div>
                          <div id="cartRlistCost">
                            &#8377;{(this.props.walletbalance.length === 0)?"0":this.props.walletbalance}
                          </div>
                      </div>:null}
                </div>
                <div id="CartRsecondPart">
                    <div id="CartPayTypeD">
                    <ul id="CartPayType">
                      <li ><input type="radio" name="paytype" value="prepaid" defaultChecked 
                      style={{ 
                        // trasnsform:'scale(1.5)'
                        transform: 'scale(1.3)'
                       }}
                      // onChange={onChange} /> <span>Prepaid</span></li>
                      // <li ><input type="radio" name="paytype" value="cod"
                      // onChange={onChange}/><span>COD</span></li>
                      onChange={onChange} /> <label id="precod">Prepaid</label></li>
                      <li ><input type="radio" name="paytype" value="cod"
                          style={{ 
                            // trasnsform:'scale(1.5)'
                            transform: 'scale(1.3)'
                           }}
                      onChange={onChange}/><label id="precod">COD</label></li>
                      {/* <li><input type="radio" name="paytype" value="express"/><span>Express Delivery</span></li> */}
                    </ul>
                    </div>
                    <p id="deliveredBy">
                    Delivered In 4-7 Days 
                    </p>
                    <hr style={{ marginBottom: '5px' }}/>
                    {/* <div id="cartCoupon">
                        <input type="radio" name="" value="" style={{ width:'0%' }}/>
                      <span> MP10 (Get 10%Off For All Prepaid Orders Max. upto 200Rs.)</span>
                    </div> */}
                    {/* <hr style={{ marginBottom:'0px' }}/> */}
                    <div id="cartLastpart">
                    {/* {console.log(this.props.SelectedAddress.length)} */}
                    {/* {(TotalShipCost>80)?:<span style={{ marginLeft:'21%' }}>minimum order value should be Rs. 80</span>} */}

                    {/* {=---------- */}
    
                      <span>
                      {this.props.SelectedAddress.length == 0?<button id="deliveryAdd" onClick={PopupOpenDiveryAdd}>Select Delivery Address First</button>:null}
                        {this.props.ItemsInCart?
                          <React.Fragment>

                         {(TotalShipCost>80)?null:<p style={{ textAlign:'center',color:'red',lineHeight: '131%',marginTop:'1%' }}>
                          <span style={{ fontSize:'150%' }}>*</span>
                          <span>minimum order value should be Rs.{(this.state.paytype==="cod")?"150":"80"},Please add more Books else proceed to pay Rs.{(this.state.paytype==="cod")?"150":"80"} and place your order</span>
                          </p>}
                        <button id={this.state.DisableProccedTopay?"Dis_proceedToPayBtn":"proceedToPayBtn"} onClick={MakePayment} disabled={this.state.DisableProccedTopay} >{
                          this.state.CartBtnLoader?  
                                <div id="CartBtnLoader">
                                <Loader 
                                type="Bars"
                                color="white"
                                height="30"	
                                width="40"
                            /></div> :
                          (ShipCost < 80 )?(this.state.paytype === "cod")?`Place your order of Rs. 150`:`Proceed To Pay Rs. 80`:
                        (this.state.paytype === "cod")?`Place your order of Rs. ${Math.round(TotalPayment)}`:`Proceed To Pay Rs. ${Math.round(TotalPayment)}`}
                        
                        </button>
                        </React.Fragment>
                        :null}

                     </span>

                      {/* <p style={{ textAlign:'center',color:'red',lineHeight: '131%',marginTop:'1%' }}><span style={{ fontSize:'150%' }}>*</span><span>minimum order value should be Rs.{(this.state.paytype==="cod")?"150":"80"},Please add more Books else proceed to pay Rs.{(this.state.paytype==="cod")?"150":"80"} and place your order</span></p> */}
                      {/* // }-------------------- */}
                      
                      {/* Popup for Showing message if car is empty */}
                      <MediaQuery minWidth={992}>
                      { (this.props.ItemsInCart !== 0)? <Popup
                  open={this.state.openDelivery}
                  // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                  contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'90%'}}
                  overlayStyle={{ backckground:'transparent' }}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                      {/* {(this.props.getadd.length !== 0)? */}
                      <Delivery CloseDelivery={()=>this.setState({openDelivery:false})}/>
                  {/* :null} */}
                  </Popup>:
                 
                  <Popup
                  open={this.state.openDelivery}
                  // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                 <p style={{ textAlign:'center' }}>Your Cart is Empty, Please Add Books to Cart to Continue</p>
                  </Popup>
                       
                      }
                      </MediaQuery>
                        {/* tab */}
                        <MediaQuery maxWidth={991} and minWidth={768}>
                      { (this.props.ItemsInCart !== 0)? 
                        <Popup
                  open={this.state.openDelivery}
                  contentStyle={{ margin:'unset',marginLeft:'56%',float:'right',marginTop:'0%' ,height:'100%'}}
                  overlayStyle={{ backckground:'transpaernt' }}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                
                  {/* {(this.props.getadd.length !== 0)? */}
                  <Delivery CloseDelivery={()=>this.setState({openDelivery:false})}/>
                  {/* :null} */}
                  </Popup>:
                 
                  <Popup
                  open={this.state.openDelivery}
                  // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                 <p style={{ textAlign:'center' }}>Your Cart is Empty, Please Add Books to Cart to Continue</p>
                  </Popup>
                       
                      }
                      </MediaQuery>
                      {/* larger mobile */}
                      <MediaQuery maxWidth={767} and minWidth={539}>
                      { (this.props.ItemsInCart !== 0)? 
                        <Popup
                  open={this.state.openDelivery}
                  contentStyle={{ margin:'unset',marginLeft:'43%',float:'right',marginTop:'0%' ,height:'100%',width:'57%'}}
                  overlayStyle={{ backckground:'transpaernt' }}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                             
                  {/* {(this.props.getadd.length !== 0)? */}
                  <Delivery CloseDelivery={()=>this.setState({openDelivery:false})}/>
                  {/* :null} */}
                  </Popup>:
                 
                  <Popup
                  open={this.state.openDelivery}
                  // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                 <p style={{ textAlign:'center' }}>Your Cart is Empty, Please Add Books to Cart to Continue</p>
                  </Popup>
                       
                      }
                      </MediaQuery>
                         {/* smaller mobile */}
                      <MediaQuery maxWidth={538}>
                      { (this.props.ItemsInCart !== 0)? 
                        <Popup
                  open={this.state.openDelivery}
                  contentStyle={{ margin:'unset',marginLeft:'30%',float:'right',marginTop:'0%' ,height:'100%',width:'70%'}}
                  overlayStyle={{ backckground:'transpaernt' }}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                
                  {/* {(this.props.getadd.length !== 0)? */}
                  <Delivery CloseDelivery={()=>this.setState({openDelivery:false})}/>
                  {/* :null} */}
                  </Popup>:
                  <Popup
                  open={this.state.openDelivery}
                  // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                  onClose={()=>this.setState({openDelivery:false})}
                  >
                 <p style={{ textAlign:'center' }}>Your Cart is Empty, Please Add Books to Cart to Continue</p>
                  </Popup>
                                   
                                  }
                                   </MediaQuery>
                {/* {this.state.OpenLoader? <div style={{  }}>
                     <Loader 
                        type="CradleLoader"
                        color="#00BFFF"
                        height="100"	
                        width="100"
                        style={{ zIndex:'999' }}
                     />  */}
                     {/* POPUP for the COD Process */}
                    {/* desktop */}
                  <MediaQuery minWidth={992}>
                  
                  <Popup
                      open={this.state.OpenConfirmcod}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenConfirmcod:false,CartBtnLoader:false});this.props.SetOrderId()}}
                      // closeOnDocumentClick
                      contentStyle={{ width:'21vw',borderRadius:'4px'}}
                      > 
                   
                   <div style={{textAlign: 'center',fontSize: '1.6vw'}}>
                    <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.afterConfirmLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {/* <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}> */}

                        {/* </div> */}
                        <p style={{ padding:'1%',textAlign:'center'}}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.ItemsInCart}</p>
                      <button id="codConfirmBtn" onClick={()=>{MakePaymentConfirm()}} disabled={this.state.isButtonDisabled}>
                      {(ShipCost <= 80 )?(this.state.paytype === "cod")?`Confirm your order of Rs.150`:(this.state.WalletSelected)?`Confirm your order of Rs 80`:null:
                        (this.state.paytype === "cod")?`Confirm your order of Rs.${Math.round(TotalPayment)}`:( Number(TotalPayment) === 0 && this.state.WalletSelected)?`Confirm your order through wallet`:null}
                      </button>
                     </div>
                  </Popup>
                  </MediaQuery>
                                    {/* tab */}
                                    <MediaQuery maxWidth={991} and minWidth={768}>
                  <Popup
                      open={this.state.OpenConfirmcod}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenConfirmcod:false,CartBtnLoader:false});this.props.SetOrderId()}}
                      // closeOnDocumentClick
                      contentStyle={{ width:'28vw',borderRadius:'4px',fontSize: '1.8vw'}}
                      > 
                     <div style={{textAlign: 'center',fontSize: '2vw'}}>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.afterConfirmLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                        <p style={{ padding:'1%',textAlign:'center' }}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.ItemsInCart}</p>
                      <button id="codConfirmBtn" onClick={()=>{MakePaymentConfirm()}} disabled={this.state.isButtonDisabled}>
                      {(ShipCost <= 80 )?(this.state.paytype === "cod")?`Confirm your order of Rs.150`:(this.state.WalletSelected)?`Confirm your order of Rs 80`:null:
                        (this.state.paytype === "cod")?`Confirm your order of Rs.${Math.round(TotalPayment)}`:( Number(TotalPayment) === 0 && this.state.WalletSelected)?`Confirm your order through wallet`:null}
                      </button>
                     </div>
                  </Popup>
                  </MediaQuery>
                  {/*  larger mobile*/}
                  <MediaQuery maxWidth={767} and minWidth={539}>
                  <Popup
                      open={this.state.OpenConfirmcod}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenConfirmcod:false,CartBtnLoader:false});this.props.SetOrderId()}}
                      // closeOnDocumentClick
                      contentStyle={{ width:'31vw',borderRadius:'4px'}}
                      > 
                     <div style={{fontSize:'2.5vw',textAlign:'center' }}>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.afterConfirmLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                        <p style={{ padding:'1%',textAlign:'center' }}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.ItemsInCart}</p>
                      <button id="codConfirmBtn" onClick={()=>{MakePaymentConfirm()}} disabled={this.state.isButtonDisabled}>
                      {(ShipCost <= 80 )?(this.state.paytype === "cod")?`Confirm your order of Rs.150`:(this.state.WalletSelected)?`Confirm your order of Rs 80`:null:
                        (this.state.paytype === "cod")?`Confirm your order of Rs.${Math.round(TotalPayment)}`:( Number(TotalPayment) === 0 && this.state.WalletSelected)?`Confirm your order through wallet`:null}
                      </button>
                     </div>
                  </Popup>
                  </MediaQuery>
                  {/* mobile */}
                  <MediaQuery maxWidth={538} >
                  <Popup
                      open={this.state.OpenConfirmcod}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenConfirmcod:false,CartBtnLoader:false});this.props.SetOrderId()}}
                      // closeOnDocumentClick
                      contentStyle={{ width:'43vw',borderRadius:'4px'}}
                      > 
                     <div style={{fontSize:'3vw',textAlign:'center' }}>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.afterConfirmLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                        <p style={{ padding:'1%',textAlign:'center' }}>Number of {(this.props.ItemsInCart === 1)?"Book":"Books"} Ordered {this.props.ItemsInCart}</p>
                      <button id="codConfirmBtn" onClick={()=>{MakePaymentConfirm()}} disabled={this.state.isButtonDisabled}>
                      {(ShipCost < 80 )?(this.state.paytype === "cod")?`Confirm your order of Rs.150`:(this.state.WalletSelected)?`Confirm your order of Rs 80`:null:
                        (this.state.paytype === "cod")?`Confirm your order of Rs.${Math.round(TotalPayment)}`:( Number(TotalPayment) === 0 && this.state.WalletSelected)?`Confirm your order through wallet`:null}
                      </button>
                     </div>
                  </Popup>
                  </MediaQuery>
                     {/* POPUP for the Razorpay payment */}
                     <MediaQuery minWidth={992}>
                  <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false});this.props.SetOrderId()}}
                      closeOnDocumentClick={false}
                      contentStyle={{ width:'20%',borderRadius:'4px'}}
                      
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?

                     <ProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false,CartBtnLoader:false,isButtonDisabled:false});  this.props.SetOrderId();}} WalletSelected={this.state.WalletSelected}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
                  {/* ---------------tab--------------------- */}
                  <MediaQuery maxWidth={991} and minWidth={539}>
                  <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false});this.props.SetOrderId()}}
                      closeOnDocumentClick
                      contentStyle={{ width:'40vw',borderRadius:'3px'}}
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?
                      
                     <ProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false,CartBtnLoader:false,isButtonDisabled:false}); this.props.SetOrderId()}}  WalletSelected={this.state.WalletSelected}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
                          {/* ---------------smaller mobile --------------*/}
                          <MediaQuery maxWidth={538}>               
                           <Popup
                      open={this.state.OpenProceedPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenProceedPayBtn:false});this.props.SetOrderId()}}
                      closeOnDocumentClick
                      contentStyle={{ width:'70vw',borderRadius:'3px'}}
                      > 
                   
                     <div>
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.Cartloader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      {(this.props.RAZORPAY !==0)?
                      
                     <ProceedToPay closePopup={()=>{this.setState({OpenProceedPayBtn:false,CartBtnLoader:false,isButtonDisabled:false}); this.props.SetOrderId()}} WalletSelected={this.state.WalletSelected}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
                  <Popup
                      open={this.state.ShowClickedWallerCod}
                      onClose={()=>this.setState({ShowClickedWallerCod:false})}
                  >
                    <p id="codWalletAlert">
                        Wallet Can Only Be Used For Prepaid Orders.
                    </p>
                  </Popup>
                  {/* </div>:null} */}
                    {/* <Link to='' style={{ textDecoration:'none' }}> */}
                    {/* <p id="contShoping" onClick={this.props.history.goBack}>Continue Shopping ></p> */}
                   <Link to="" style={{ cursor:'pointer',textDecoration:'none', }}> <p id="contShoping" >Continue Shopping ></p></Link>

                    {/* </Link> */}
                    </div>
                </div>
                
            </div>
        </div>


     <div style={{ marginTop:'10%' }}>
     <MediaQuery minWidth={539}>
          <MainFooter/>
          </MediaQuery> 
              </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  cartDetails:state.cartReduc.MyCart,
  userToken:state.accountR.token,
  ItemsInCart:state.cartReduc.cartLength,
  SelectedAddress:state.accountR.selectedAddress,
  CartPrice:state.cartReduc.CartPrice,
  AddresId:state.cartReduc.AddresId,
  TotalPrice:state.cartReduc.TotalPrice,
  OrderId:state.cartReduc.OrderId,
  RAZORPAY:state.donationR.rp_id,
  getadd:state.accountR.getadd,
  walletbalance:state.walletR.walletbalance,
  UserEmail:state.userdetailsR.getuserdetails.email,
  RzPayErr:state.cartReduc.RzPayErr,
  OutOfStockBooks:state.cartReduc.OutOfStockBooks
})

export default connect(mapStateToProps,{Getaddress,AddPriceCart,RemoveCart,OrderDetails,RedirectWalletToCart,SetWallet,RemoveToBeAddedToCart,removeFromCartLogout,orderUpdateSucc,
  Get_Rp_Id,SetOrderId,SetSelectedAddressBlank,Getwalletd,walletRazorpay,ShowRzPayErr,OutOfStock})(Cart);

