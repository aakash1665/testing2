import React, { Component } from 'react'
// import UserInputGroup from './UserInputGroup';
import UserInputGroup from '../delivery/UserInputGroup'
import '../delivery/useraddress.css'
import { connect } from 'react-redux';
// import {addAddressAction} from '../../actions/accountAction';
import {addAddressAction,Getaddress} from '../../actions/accountAction'
import axios from 'axios'
import config from 'react-global-configuration'

class CartAddUserAddress extends Component {
    state ={
        fullname :'',
        landmark:'',
        phone_no:'',
        alt_phone_no:'',
        pincode:'',
        address1:'',
        // address2:'',
        city_name:'',
        state_name:'',
        title:'',
        errors:{},
        asd:'',
    };
    
closedone=()=>{
  // console.log(this.props.closemodule);
}
onSubmit =(e)=>{
    // alert("full");
    e.preventDefault();
    const {pincode,phone_no,fullname,alt_phone_no,landmark,address1,address_id,is_primary,city_name,errors,state_name,title} = this.state;
    if(fullname===""){
      this.setState({errors:{fullname:"Enter your name"}});
      return;
  }
  if(address1===""){
    this.setState({errors:{address1:"Enter your address"}});
    return;
  }
  if(landmark===""){
    this.setState({errors:{landmark:"Enter your landmark"}});
    return;
  }


  if(phone_no===""){
    this.setState({errors:{phone_no:"Enter number"}});
    return;
  }
  if(isNaN(phone_no)){
        this.setState({errors:{phone_no:"Enter 10 Digits"}});
        return;
  }

  // if(alt_phone_no===""){
  //   this.setState({errors:{alt_phone_no:"Enter number"}});
  //   return;
  // }
  // if(isNaN(alt_phone_no)){
  //       this.setState({errors:{alt_phone_no:"Enter 10 Digits"}});
  //       return;
  // }
    
  if(isNaN(pincode)){
      this.setState({errors:{pincode:"Enter Pincode"}});
      return;
  }
  if(pincode===""){
    this.setState({errors:{pincode:"Enter Pincode"}});
    return;
}

    if(alt_phone_no.length && alt_phone_no.length !== 10){
        this.setState({errors:{alt_phone_no:"Contact number must be 10 digits"}});
        return;
    }

    if(phone_no.length !== 10){
      this.setState({errors:{phone_no:"Contact number must be 10 digits"}});
      return;
  }
    if(pincode.length !== 6){
        this.setState({errors:{pincode:"Pincode must be 6 digits"}});
        return;
    }

    const address={
        data :{
  
            title,
            rec_name:fullname,
            address:address1,
            landmark:landmark,
            phone_no:phone_no,
            pincode:pincode,
            alt_phone_no:alt_phone_no,
            city_name:city_name,
            state_name:state_name,
            country_name:'IND',
            is_primary:'Y',
 
        }
    }
    const token= `Token ${this.props.userToken}`
    this.props.addAddressAction(token,address)
    this.setState({
        fullname :'',
        landmark:'',
        phone_no:'',
        pincode:'',
        address1:'',
        city_name:'',
        state_name:'',
        title:'',
        errors:{}
    })
    this.props.CloseCartAddUserAddress()
    const details=`Token ${this.props.userToken}`
    // this.props.userdetails(details)
    this.props.Getaddress(details)
    // window.location.href = `/view-cart`
}
    onChange = e => this.setState({[e.target.name]:e.target.value});
  render() {
    const {fullname,landmark,alt_phone_no,phone_no,address1,city_name,state_name,pincode,title,errors,is_primary} = this.state;
    let PincodeErr=""
    let PhoneErr=""
    if(pincode.toString().length === 6){
    
        axios.get(`${config.get('apiDomain')}/pincode/pin_check/${this.state.pincode}/`,
        )
        .then(res=>{
          if(res.data.length === 0){
            this.setState({errors:{pincode:"Enter Correct Pincode"}});
        }else{
        this.setState({city_name:res.data[0].districtname,state_name:res.data[0].statename});
        this.setState({errors:{pincode:""}});}})
        .catch(err=>{
          this.setState({city_name:"",state_name:""});
          

        }
        )
        PincodeErr=""
     }
    return (
        <div class="container-1" style={{marginRight:'3%',marginLeft:'3%'  }}>
        <div id="addressdiv">
        <a className="close" onClick={this.props.CloseCartAddUserAddress} style={{
    color: 'white',zIndex:'999',opacity:'1',fontWeight:'normal',marginTop:'-1vh',
    float:'left'}}>
              &times;
            </a>
        <div id="Add-New-Address">Add New Address
        </div></div>
        <br/><br/><br/>
        {/* <div class="Layer-4">
           <div class="address">
            <p class="user-name">SOMETHING</p>
            <button class="home-button"><span class="home-button-text">Home</span></button>
            <p class="address-info">Narayanpur, Rajarhat, Kolkata 700136, West Bengal</p>
            <p class="edit">Edit</p>
           </div>
           <div class="select"> 
            <button class="Layer-10"><span class="select-button">Select</span></button>
           </div>
        </div> */}
        {/* <hr/> */}
        {/* <div class="Layer-4">
          <div class="address">
            <p class="user-name">SOMETHING</p>
            <button class="home-button"><span class="home-button-text">Home</span></button>
            <p class="address-info">Narayanpur, Rajarhat, Kolkata 700136, West Bengal</p>
            <p class="edit">Edit</p>
           </div>
           <div class="select"> 
            <button class="Layer-10"><span class="select-button">Select</span></button>
           </div>
        </div> */}
        <form onSubmit={this.onSubmit}>

<UserInputGroup
            name="fullname"
            // label="Name"
            placeholder="Full Name"
            value={fullname}
            onChange={this.onChange}
            errors={errors.fullname}
            maxLength={40}
            />
<UserInputGroup
            name="address1"
            // label="Name"
            placeholder="Address 1"
            value={address1}
            onChange={this.onChange}
            errors={errors.name}
            maxLength={250}
            />
<UserInputGroup
            name="landmark"
            // label="Name"
            placeholder="Landmark"
            value={landmark}
            onChange={this.onChange}
            errors={errors.landmark}
            maxLength={80}
            />
<UserInputGroup
       
            name="phone_no"
            // label="Name"
            placeholder="Mobile Number"
            value={phone_no}
            onChange={this.onChange}
            errors={errors.phone_no}
            maxLength="10"
            />
        <input 
        name="alt_phone_no"
        placeholder="Alternate Mobile Number(Optional)"
        value={alt_phone_no}
        onChange={this.onChange}
        // errors={errors.alt_phone_no}
        maxLength="10"
        style={{
         paddingTop:'2%',
 
            
        }}
        />
        {errors.alt_phone_no && <div style={{ color:'red' ,fontSize:"70%"    }}>{errors.alt_phone_no}</div>}
{/* <UserInputGroup
       
       name="alt_phone_no"
       // label="Name"
       placeholder="Alternate Mobile Number(Optional)"
       value={alt_phone_no}
       onChange={this.onChange}
       errors={errors.alt_phone_no}
       maxLength="10"
       /> */}

<UserInputGroup 
            name="pincode"
            // label="Name"
            placeholder="Pincode"
            value={pincode}
            onChange={this.onChange}
            errors={errors.pincode}
            maxLength="6"
            />
<UserInputGroup
            name="city"
            // label="Name"
            placeholder="City"
            value={city_name}
            onChange={(e)=>{this.setState({city_name:e.target.value})}}
            errors={errors.name}
            maxLength={40}
            />
<UserInputGroup
            name="state"
            // label="Name"
            placeholder="State"
            value={state_name}
            onChange={(e)=>{this.setState({state_name:e.target.value})}}
            errors={errors.name}
            maxLength={40}
            />

            {/* For future use  */}
{/* <input type="Checkbox" checked={this.state.is_primary} name="setdefault" style={{width:'13px',marginTop:'3%'}}  onChange={this.changecheck}/><span style={{    marginLeft: '6%',}}>Set As Default</span>  */}
{/* <InputGroup
            name="title"
            // label="Name"
            placeholder="Address Title"
            value={title}
            onChange={this.onChange}
            errors={errors.name}
            /> */}
            <br/>
            <label style={{marginTop:'2%'}}>Address Title</label>
<div>            
<input type="radio" value="Home" name="title" style={{width:'13px'}}  onChange={this.onChange} required/> 
<span style={{    marginLeft: '6%',}}  >Home</span>
<input type="radio" value="Office" name="title" style={{width:'13px',marginLeft: '16%'}} onChange={this.onChange}/> <span style={{    marginLeft: '6%',}} >Office</span>
</div>
<input type="submit" id="donebtn_button"  onClick={this.closedone()} value="Done"/> 
 </form>
</div>

    )
  }
}
const mapStateToProps = state => ({
    searchBook:state.homeR.searchBook,
    userToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
  })
// export default (AddAddress)

export default connect(mapStateToProps,{addAddressAction,Getaddress})(CartAddUserAddress);