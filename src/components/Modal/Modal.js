import React, {Component} from 'react';
import { connect } from 'react-redux';
import classes from './modal.css';
import Aux from '../../hoc/Aux';
import Backdrop from '../product/Backdrop/Backdrop';
import {login_backdropToggle}  from '../../actions/accountAction';

class Modal extends Component{

    shouldComponentUpdate(nextProps, nextState){
       return nextProps.show !== this.props.show || nextProps.children !== this.props.children;
    }

    componentWillUpdate(){
        console.log('[Modal] will upadate');  
    }
    
    render(){

        return(  
        <Aux> 
          <Backdrop
            show ={this.props.show}
            clicked = {this.props.modalClosed}
            click = {this.props.login_backdropToggle}
            />
            <div 
                className = "Modal"
                style = {{
                transform: this.props.show ? 'translateX(0)': 'translateX(-100vh)',
                opacity: this.props.show ? '1':'0'
                 }}
                >
                     {/* sjhgjsgkgzgkjg */}
                {this.props.children}

            </div>
          
       </Aux>);
    }
}
export default connect(null,{login_backdropToggle})(Modal)

