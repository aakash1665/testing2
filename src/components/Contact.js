import React, { Component } from 'react';
// import logo from './logo.png';
import './contact.css';
import {Helmet} from 'react-helmet'
import MainHeader from './MainHeader';
import MainFooter from './MainFooter';
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import {BreadcrumbsItem} from 'react-breadcrumbs-dynamic'
import axios from 'axios'
import config from 'react-global-configuration'
import { Link,Redirect,browserHistory } from 'react-router-dom'

class Contact extends Component {
  state={
    name:'',
    email_phone:'',
    subject:'',
    body:'',
    EmailMsg:'',
  }
  componentDidMount(){
    window.scrollTo(0, 0);
  }
  render() {
    const ContactSubmit=(e)=>{
      e.preventDefault();
      // alert("ca")
      const Data={ 
        
        name:this.state.name,
        userMail:this.state.email_phone,
        subject:this.state.subject,
        body:this.state.body,
        email:'support@mypustak.com '

      }



      axios.post(`${config.get('apiDomain')}/common/email_contactUs/`,Data,
    //   {headers:{'Authorization': `Token ${this.props.userToken}`
    // }}
    )
      .then(res=>{
        window.scrollTo(0, 0);
        console.log(res,Data);
        this.setState({EmailMsg:"Email Sent Successfully "})
        
      // if(this.state.AlreadyinCartMsg.length !== 0){
        setTimeout(()=>{this.setState({EmailMsg:""})},5000)
      // }
      })
      .catch(err=>{
        window.scrollTo(0, 0);
        console.log(err,Data);
        this.setState({EmailMsg:"Email Mot Sent "})

        setTimeout(()=>{this.setState({EmailMsg:""})},5000)
      
      }
      
      )
      this.setState({
        name:'',
        email_phone:'',
        subject:'',
        body:'',
        // email:'support@mypustak.com '
      })
    }
    const onChange=e=>{
      this.setState({[e.target.name]:e.target.value})
    }
    return (
      <div>
<BreadcrumbsItem to='/contact-us'>contact-us</BreadcrumbsItem>

        <MainHeader/>
        <Helmet>
          <style>
            {'body{background-color:#f3f7f8;}'}
          </style>
        </Helmet>
	  <div>
        <div className="divcontact1">
       <label className="contact-us"> Contact Us </label>

       {/* <div className="cstyle2"><span className="txtstyle3">If You Are Already A MyPustak User Then</span> <a className="linkstyle" href="">Log In </a><span className="txtstyle3">Before Contacting Us.</span></div> */}
       <div className="divsection2">
       <label className="style3"> Call Us <span className="txtstyle1">:033-79603826</span> <br/>
       Email Us: <span className="txtstyle2"> support@mypustak.com </span> 
       <p id="ContactEmailMsg">{this.state.EmailMsg}</p> </label> </div> <br/>

       <form onSubmit={ContactSubmit}>
       <span className="align1">Your Name</span><br/>
        {/* <input className="tbox1" type="text" placeholder="Enter Your Name"></input> */}
        <InputGroup className="mb-3" id="tbox1">
    <FormControl
      aria-label="Default"
      aria-describedby="inputGroup-sizing-default"
      placeholder="Enter your name"
      name="name"
      onChange={onChange}
      maxLength={40}
      value={this.state.name}
      required
    />
  </InputGroup>

       <span className="align2"> Your Email ID</span><br/>
       {/* <input id="textbox2" type="text" 
       placeholder="Enter Your Email ID/Mobile Number"></input> */}
      <InputGroup className="mb-3" id="textbox2">
    <FormControl
      aria-label="Default"
      aria-describedby="inputGroup-sizing-default"
      placeholder="Enter Your Email ID"
      name="email_phone"
      onChange={onChange}
      maxLength={40}
       type='email'
      value={this.state.email_phone}
      required

    />
  </InputGroup>
    
     <span className="align3"> Your Message</span><br/>
     {/* <input className="tbox3" type="text" placeholder="Subject(Optional)"></input> */}
      
     <InputGroup className="mb-3" id="tbox3">
    <FormControl
      aria-label="Default"
      aria-describedby="inputGroup-sizing-default"
      placeholder="Subject(Optional)"
      name="subject"
      onChange={onChange}
      maxLength={100}
      value={this.state.subject}

    />
  </InputGroup>

      {/* <input className="tbox2" type="text"></input> */}
      <InputGroup id="tbox2">
    <FormControl as="textarea" aria-label="With textarea" rows="4" name="body" onChange={onChange} 
      maxLength={400}
      value={this.state.body}
      required
    />
  </InputGroup>
      {/* <button className="box3"  >Send</button> */}
      <Button variant="primary" size="sm" id="box4" type="submit">
        Submit
      </Button>
    </form>
       </div>
       </div>
      <MainFooter/>
      </div>
     
    );
  }
}

export default Contact;
