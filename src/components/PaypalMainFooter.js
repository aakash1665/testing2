import React, { Component } from 'react'
import './mainFooter.css';
import { Link } from 'react-router-dom'
import MediaQuery from 'react-responsive'
export default class MainFooter extends Component {
  render() {
    return (
      <div id="footerdiv">
      <div id="flexdiv">
       <div style={{flex:'1'}}>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ourlinkIcon.png`} id="linksimg"></img>
            <label id="fp">OUR LINKS</label>
        
        <ul id="ful">
        <li><Link to="/contact-us">Contact Us</Link></li>
        <li ><Link to="/pages/about-us">About Us</Link></li>
        <li ><Link to="/pages/book-condition-guidelines">Book Condition Guidelines</Link></li>
        <li ><Link to="/pages/privacy-policy">Privacy Policy</Link></li>
        <li ><Link to="/pages/terms-conditions">Terms And Condition</Link></li>
        </ul>
        </div>
       <div style={{flex:'0.7'}}>        
         <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/ourStoryIcon.png`} id="storyimg"></img>  
         <label id="storycss">OUR STORY</label>
        <ul id="ful1">
        <li ><Link to="/proud-donors">Our Proud Donors</Link></li>
        <li ><Link to="/faq">FAQ</Link></li>
        < a href="https://facebook.com/mypustak"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/fb.png`} id="footerfbimg"></img></a>
        <a href="https://twitter.com/mypustak"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerTwitter.png`} id="footertwitterimg"></img></a>
        <a href="https://www.instagram.com/mypustak/"><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/instagram.png`} id="footerinstaimg"/></a>
        </ul>
        </div>
       <div style={{flex:'1'}}>        
         <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerPaymentGatewayIcon.png`} id="paymentimg"></img>  
         <label id="paymentg">PAYMENT GATEWAY</label>
         <div>
        <p id="paymentcss">  
        
        <span style={{color:'#f9a954'}}>Payment Through MyPustak is 
        100% secured</span><br/>
        Our website is secured by 256 bit SSL encryption issued by Lets Encrypt Authority x3 making your transactions at MyPustak as secure as possible.</p>
        
        <div >
        <div id="paymentFooterIcons">
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerVisa.png`} id="footervisaimg"></img>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Mastercard.png`} id="footermastercardimg"></img>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerRupay.png`} id="footerrupayimg"></img>
        {/* <a href="https://www.paypal.com/in/webapps/mpp/paypal-popup" title="How PayPal Works" onclick="javascript:window.open('https://www.paypal.com/in/webapps/mpp/paypal-popup','WIPaypal','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700'); return false;">
        <img src="https://www.paypalobjects.com/webstatic/mktg/Logo/pp-logo-100px.png" id= "paypalIconFooter"border="0" alt="PayPal Logo"/>

        </a> */}
        </div>
        <div id="paymentFooterIcons">
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/NetBanking.png`} id="footernetbankimg"></img>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerwalleticon.png`} id="footerwalletimg"></img>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/footerupi.png`} id="footerupiimg"></img>
      <MediaQuery minWidth={992}>
          <div id="DummyContnet">test test test test</div>
      </MediaQuery>
      <MediaQuery maxWidth={991} and minWidth={768}>
          <div id="DummyContnet">test test test test</div>
      </MediaQuery>
      <MediaQuery maxWidth={767} and minWidth={538}>
          <div id="DummyContnet">test test </div>
      </MediaQuery>
      <MediaQuery maxWidth={537} and minWidth={319}>
          <div id="DummyContnet"></div>
      </MediaQuery>
        {/* <!-- PayPal Logo --> */}
        {/* <table border="0" cellpadding="10" cellspacing="0" align="center">
        <tr><td align="center"></td></tr>
        <tr><td align="center"> */}
        </div>
                </div>
        {/* </td>
        </tr>
        </table> */}
        {/* <!-- PayPal Logo --> */}
        {/* </table> */}
        </div>
        </div>
        <div style={{flex:'1'}} id="Last">
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/LogesticPartnerIcon.png`} id="logisticimg"></img>  
        <label id="logisticcss">OUR LOGISTIC PARTNER</label>

        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/delhivery.png`} id="delhiveryimg"></img>
        <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/FedexIcon.png`} id="fedeximg"></img>
        </div>
        </div>
        </div>
    );
  }
}