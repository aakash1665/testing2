import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import PropTypes from "prop-types";
import Button from "@material-ui/core/Button";
import {
  getUploaderCount,
  getUploaderCountThisMonth
} from "../../actions/dataentrysalebydaterangeAction";
import { connect } from "react-redux";
import { Dialog } from "@material-ui/core";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { log } from "util";

// const style = textField: {
//   marginLeft: theme.spacing(1),
//   marginRight: theme.spacing(1),
//   width: 200
// }

class DataEntrySale extends Component {
  static propTypes = {
    uploader: PropTypes.object.isRequired,
    getUploaderCount: PropTypes.func.isRequired,
    getUploaderCountThisMonth: PropTypes.func.isRequired,
    uploaderthismonth: PropTypes.object.isRequired
  };

  state = {
    uploader: "",
    startDate: "2017-05-24",
    endDate: "2017-05-24",
    startTime: "12:30",
    endTime: "17:30",
    token: "",
    uploaderthismonth: "",
    password:"",
    Sessionpassword:"",
    openpasswordDialog:true
  };
  componentDidMount(){
    // console.log(sessionStorage.getItem('password'));
    if(sessionStorage.getItem('password')){
      this.setState({openpasswordDialog:false})
    }
  }
  onChangeUploader = e => {
    this.setState({
      uploader: e.target.value
    });
  };

  onChangeUploaderThisMonth = e => {
    this.setState({
      uploaderthismonth: e.target.value
    });
  };

  onChangeStartDate = e => {
    this.setState({
      startDate: e.target.value
    });
  };

  onChangeEndDate = e => {
    this.setState({
      endDate: e.target.value
    });
    // console.log("The token in enddate ", localStorage.getItem("user"));
  };

  onChangeStartTime = e => {
    this.setState({
      startTime: e.target.value
    });
  };

  onChangeEndTime = e => {
    this.setState({
      endTime: e.target.value
    });
  };

  onClickThisMonth = () => {
    this.setState({
      token: localStorage.getItem("user")
    });
    if (this.state.uploaderthismonth.length) {
      this.props.getUploaderCountThisMonth(
        localStorage.getItem("user"),
        this.state.uploaderthismonth
      );
    }
  };

  onClickHandler = () => {
    this.setState({
      token: localStorage.getItem("user")
    });
    if (this.state.uploader.length) {
      this.props.getUploaderCount(
        localStorage.getItem("user"),
        this.state.uploader,
        Math.floor(
          new Date(
            this.state.startDate + " " + this.state.startTime
          ).getTime() / 1000
        ),
        Math.floor(
          new Date(this.state.endDate + " " + this.state.endTime).getTime() /
            1000
        )
      );
    }
  };
  handelOnChnagegetPassword =(e)=>{
    this.setState({password:e.target.value})
  }
  getPassword =(e)=>{
    e.preventDefault();
    console.log(e.target,"in");
    
    if(this.state.password == 123){
      console.log("entered");
      
      sessionStorage.setItem('password',this.state.password)
      this.setState({openpasswordDialog:false})
    }
    
    // sessionStorage.setItem('password',this.state.password)
  }
  render() {
    // console.log("The Uploader in Render is ", this.state.uploader);
    // console.log("Start Date in Render is ", this.state.startDate);
    // console.log("Start Time in Render is ", this.state.startTime);
    // console.log("End Date in Render is ", this.state.endDate);
    // console.log("End Time in Render is ", this.state.endTime);
    // console.log(
    //   "Time stamp in Render is ",
    //   Math.floor(new Date("2017-05-24 12:30").getTime() / 1000)
    // );
    // console.log(
    //   "Time stamp enddate in Render is ",
    //   Math.floor(new Date("2017-05-24 17:30").getTime() / 1000)
    // );

    return (
      <React.Fragment>
        {localStorage.getItem("user") ? (
          <div>
            <form noValidate>
              <TextField
                id="uploader"
                label="Uploader"
                type="email"
                placeholder="Enter email of uploader"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap"
                }}
                onChange={this.onChangeUploader}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                id="startDate"
                label="From"
                type="date"
                defaultValue="2017-05-24"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onChange={this.onChangeStartDate}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                id="startTime"
                type="time"
                defaultValue="12:30"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap"
                }}
                onChange={this.onChangeStartTime}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                id="endDate"
                label="To"
                type="date"
                defaultValue="2017-05-24"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onChange={this.onChangeEndDate}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <TextField
                id="endTime"
                type="time"
                defaultValue="17:30"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap"
                }}
                onChange={this.onChangeEndTime}
                InputLabelProps={{
                  shrink: true
                }}
              />
              <Button
                variant="contained"
                color="primary"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onClick={this.onClickHandler}
              >
                Get Date Entry Details
              </Button>
            </form>
            <br />

            <form noValidate>
              <TextField
                id="uploaderthismonth"
                label="Uploader"
                type="email"
                placeholder="Enter email of uploader"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap"
                }}
                onChange={this.onChangeUploaderThisMonth}
                InputLabelProps={{
                  shrink: true
                }}
              />

              <Button
                variant="contained"
                color="secondary"
                margin="normal"
                style={{
                  marginLeft: "theme.spacing(1)",
                  marginRight: "theme.spacing(1)",
                  width: 200,
                  display: "flex",
                  flexWrap: "wrap",
                  margintop: "theme.spacing(1)"
                }}
                onClick={this.onClickThisMonth}
              >
                Entry Count This Month
              </Button>
            </form>

            {/* <Dialog
            open ={this.state.openpasswordDialog}
            aria-labelledby="form-dialog-title"
            size="lg"
            >
                <DialogTitle id="form-dialog-title">Enter Password</DialogTitle>
                <form onSubmit={this.getPassword}>
                <DialogContent>
                  <input type="password" 
                  placeholder="*********" 
                  value={this.state.password} 
                  onChange={this.handelOnChnagegetPassword}></input>
                </DialogContent>
                <DialogActions>
                <div style={{ textAlign:'center' }}>
                    <Button color="primary" 
                    variant="contained"  
                    style={{ justifyContent:'start' }}
                    type="submit" 
                    >
                      Enter
                    </Button>
                </div>
                </DialogActions>
                </form>
            </Dialog> */}


          </div>
        ) : null}
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  uploader: state.uploader.uploader,
  uploaderthismonth: state.uploader.uploaderthismonth
});

export default connect(
  mapStateToProps,
  { getUploaderCount, getUploaderCountThisMonth }
)(DataEntrySale);
