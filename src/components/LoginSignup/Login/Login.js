import React, { Component } from 'react'
import {LoginCheck,signupCheck,showLoader,clearAll,clearLoginErr,ActivteSuccesPopup} from '../../../actions/accountAction'
import { connect } from 'react-redux';
import { css } from '@emotion/core';
import ClipLoader from 'react-spinners';
import RCG from 'react-captcha-generator';
import axios from 'axios'
import Loader from 'react-loader-spinner'
import config from 'react-global-configuration'
import './Login.css';
// import { fade, withStyles, makeStyles, createMuiTheme } from '@material-ui/core/styles';
// import { ThemeProvider } from '@material-ui/styles';
// import InputBase from '@material-ui/core/InputBase';
// import InputLabel from '@material-ui/core/InputLabel';
// import TextField from '@material-ui/core/TextField';
// import FormControl from '@material-ui/core/FormControl';
// import { green } from '@material-ui/core/colors';


import MediaQuery from 'react-responsive'
import { Link,Redirect} from 'react-router-dom'
import { GoogleLogin } from 'react-google-login';
import Modal from '../../Modal/Modal';
import Aux from '../../../hoc/Aux';


class Login extends Component {
    state={
        SignupPassErr:'',
        signuptoggle:true,
        Sopen:false,
        email:'',
        password:'',
        Semail:'',
        validatepwd:'',
        openForget:false,
        captcha: '',
        Imgcaptcha: '',
        loginCliked:false,
        LoginErrMsg:this.props.ErrMsg,
        EmailErrrmsg:'',
        PassErrmsg:'',
        CaptchaErr:'',
        ForgetResponse:0,
        CountMsg:1,
        RedirectMobileHome:false,
        OpenForgetLoader:false,
        mobilenumber:""
    }
    result=(text)=>{
        this.setState({
          Imgcaptcha: text
        })
      }
    componentDidMount(){
    this.setState({ForgetResponse:0})
    this.setState({CountMsg:1})
    if(document.getElementById("foo").value.length > 0){
    document.getElementById("foo").classList.add("class_two");      
    }
    }
    componentWillReceiveProps(nextProps){
      try{


      if(document.getElementById("foo").value.length > 0){
        document.getElementById("foo").classList.add("class_two");      
        }

      }catch(error){
        
      }
    if(this.props.userToken != nextProps.userToken){
    // this.setState({RedirectMobileHome:true})
    }
    }

    sopenModal=()=>{
        this.setState({ Sopen: true })
      }
      scloseModal=()=>{
        this.props.scloseModal()
    }
    changeShowLS=()=>
    {this.props.changeShowLS()}

    ForgetPassword =(e)=>{
      this.setState({email:"",password:""})
      // alert("h")
      if (e.key !== 'Enter')
      this.setState({openForget:!this.state.openForget})

    }
    EmailErrrmsg=''
    CaptchaErr=''
    CheckSubmitForgot=()=>{
      // this./
      this.check()
      if(this.state.email === ''){
        // this.setState({EmailErrrmsg:'Please Fill up the Email'})
        this.EmailErrrmsg='Please Fill up the Email'
      }else{
        // this.setState({EmailErrrmsg:''})
        this.EmailErrrmsg=''
      }
      if(this.state.captcha !== this.state.Imgcaptcha ){
        // this.setState({CaptchaErr:"Captcha Did not match"});
        this.setState({CaptchaErr:'Captcha Did not match'})
        // return;
        this.CaptchaErr='Captcha Did not match'
      }else{
        this.setState({CaptchaErr:""});
        this.CaptchaErr=''
        console.log("SuccessErrBlank1")
      }
         // console.log(this.captchaEnter.value);
         if(this.CaptchaErr === '' && this.EmailErrrmsg === ''){
            this.SubmitForgot()
          }
          console.log(this.state.Imgcaptcha, this.state.captcha, this.state.Imgcaptcha === this.state.captcha)
        }
        check() {
          // console.log(this.state.Imgcaptcha, this.captchaEnter.value, this.state.Imgcaptcha === this.state.captcha)
        }

        SubmitForgot = ()=>{
            this.setState({OpenForgetLoader:true})
            console.log("SuccessErrBlank2")
      
            // alert("1")
            const passdata = {
              
              "email" : this.state.email,
              'content-type': 'multipart/form-data',
               'boundary':'----WebKitFormBoundary7MA4YWxkTrZu0gW'
       
               }
               //  axios.post(`http://127.0.0.1:8000/core/forgot_password/`,passdata)
       axios.post(`${config.get('apiDomain')}/core/forgot_password/`,passdata)
       .then(res=>{console.log(res.status);this.setState({ForgetResponse:res.status});
       this.setState({OpenForgetLoader:false})
      
      }
       
       )
       .catch(err=>{console.log(err.response.status,"SuccessErrBlank3");this.setState({ForgetResponse:err.response.status})}
       )
      //  this.props.scloseModal()
      }
      onChange = e => {
        this.setState({PassErrmsg:'',EmailErrrmsg:''})
        this.setState({ [e.target.name]: e.target.value });
                          if(this.state.email === ''){
  
                          }
                      };
                       Login = (e) =>{
                        e.preventDefault()
                        //  alert("S")
                         if(this.state.email === ''){
                          this.setState({EmailErrrmsg:'Please Fill up the Email'})}
                          else{
                              this.setState({EmailErrrmsg:''})
                          }
                          if(this.state.password === ''){
                            this.setState({PassErrmsg:"Please Fill up the password"})}
                          else{
                            this.setState({PassErrmsg:""})
                          }
                
                      
                        // console.log("in login func");
                        // document.getElementById("loginBtn").
                        // this.setState({loginCliked:true})
                        // this.state.password
                        // e.preventDefault()
                        // this.props.showLoader()
                       const {email,password} = this.state
                       const details={
                            email,
                            password,
                        }
                        if(this.state.EmailErrrmsg === '' && this.state.PassErrmsg === '' && this.state.email !== '' && this.state.password !== ''){
                            // alert("p")
                            this.props.showLoader()
                      
                              this.setState({loading:true})
                              this.props.LoginCheck(details)
                              }
                          }

         toggleForgetPassword =()=>{
          //  alert("t")
           this.setState({openForget:!this.state.openForget})
         }

         signupftoggle =()=>{
           this.setState({signuptoggle:!this.state.signuptoggle})
         }

     signupUser = (e) =>{
      e.preventDefault()
      const {mobile,email,password} = this.state
     const details={
          email,
          mobile,
          password,
      }
    //   if(PhoneErr === '' && PasswordErr === '' && this.state.email !== '' && this.state.password !== ''){
    //     this.props.showLoader()
    //     this.setState({fadeoutSec:""})
    //     this.setState({loading:true})
    if(this.state.SignupPassErr.length == 0 && this.state.mobileErr.length ==0){
      this.props.signupCheck(details)
    }else{
      // alert("ERR")
    }
    
    // }
     
      // if(Userstatus.length !== ''){
        // alert("Al")
        // const {mobile,email,password} = this.state
      //  const  detailsAutoLogin={
      //     email,
      //     password,
      //   }
        // this.props.LoginCheck(detailsAutoLogin)
      // }
  }
  validatepwd=(e)=>{
    e.preventDefault();
    this.setState({validatepwd:e.target.value})
    if(e.target.value.length < 6){
      this.setState({SignupPassErr:"Pass must be more than 6"})
    }
    else{
      this.setState({SignupPassErr:""})
    }
  }
  validatemobile = (e)=>{
    if(e.target.value.length < 10 ){
      this.setState({mobileErr :"Enter 10 digits"})
    }else{
      this.setState({mobileErr :""})
    }
    if(!isNaN(e.target.value)){
      this.setState({mobilenumber:e.target.value})
    }
  }

  addClass=(e)=> {
    console.log(e,"add")
    document.getElementById(e.target.id).classList.add("class_two");
    // this.divRef.current.classList.add('focus-ani')
}
RemoveClass=(e)=>{
    document.getElementById(e.target.id).classList.remove("class_two")

}

        render() {
          try {
            if(this.state.email.length > 0){
              console.log(document.getElementById("foo").value.length,"foo")
              document.getElementById("foo").classList.add("class_two");      
              }
          } catch (error) {
            
          }


        if(this.state.RedirectMobileHome){
            this.setState({RedirectMobileHome:false})
            return <Redirect to="/"/>

        }      
     var EmailErrrmsg='okk'
     var PassErrmsg=''
     let CaptchaErr=""

     const Login = (e) =>{
        e.preventDefault()
        //  alert("S")
         if(this.state.email === ''){
          this.setState({EmailErrrmsg:'Please Fill up the Email'})}
          else{
              this.setState({EmailErrrmsg:''})
          }
          if(this.state.password === ''){
            this.setState({PassErrmsg:"Please Fill up the password"})}
          else{
            this.setState({PassErrmsg:""})
          }

      
        // console.log("in login func");
        // document.getElementById("loginBtn").
        // this.setState({loginCliked:true})
        // this.state.password
        // e.preventDefault()
        // this.props.showLoader()
       const {email,password} = this.state
       const details={
            email,
            password,
        }
        if(this.state.EmailErrrmsg === '' && this.state.PassErrmsg === '' && this.state.email !== '' && this.state.password !== ''){
            // alert("p")
            this.props.showLoader()
      
              this.setState({loading:true})
              this.props.LoginCheck(details)
              }
          }
          const ShowSuccessPopup=()=>{
            console.log("SuccessErrBlank4");
            if(this.state.CountMsg === 1){
             this.props.scloseModal()
             // this.setState({OpenSendMailPop:'true'})
             this.props.ActivteSuccesPopup()
             this.setState({CountMsg:2})
             // setTimeout(()=>{this.props.ActivteSuccesPopup()},4000)
           }
           }
              // alert(`${this.props.ErrMsg} er`)
    let LoginErrMsg= `${this.props.ErrMsg}`
    if(LoginErrMsg.length !== 0){
      // alert(LoginErrMsg.length)
    //   setTimeout(()=>{this.props.clearLoginErr()},3000)
    }else{
      // this.setState({fadeoutSec:""})
    }

     //  setTimeout(()=>{alert("okk")},2000)
  const responseGoogle = (response) => {
    // console.log(response.profileObj.email);
    const details={
      email:response.profileObj.email,
      password:13,   
    }
    this.props.LoginCheck(details)
  }
  const responseGoogleFailure=()=>{
    
  }

  var SignedupErr= `${this.props.SignUpErrMsg}`
  if(SignedupErr.length !== 0){
    // alert(SignedupErr.length)
    setTimeout(()=>{this.props.clearAll()},3000)
  }else{
    // this.setState({fadeoutSec:""})
  }

  return (
      <div>
          <Aux>
          <Modal show ={this.props.toggle} >
           { this.state.signuptoggle ? <div id="maindivision">
                
              { this.state.openForget?
            

                   <div id="rightdiv" >
                      <form onSubmit={this.SubmitForgot}>
                   {/* Enter register email  */}
                   <div class="input-group">
                   <input  
                     class={this.state.Semail.length > 0?"formcontrol class_two":"formcontrol"}
                     type="email" 
                     id="registermail" 
                     name="email"
                     required
                     maxLength="30" 
                     value = {this.state.Semail}
                     onChange={e => {this.setState({Semail: e.target.value});
                                           if(e.target.value.length>0)
                                           {
                                             {this.addClass(e)}
                                           }
                                           else{
                                             {this.RemoveClass(e)}
                                           }
                             
                               }}
                               />
                     
                     <label>Enter Registerd Email</label>
                      </div>
                      <button id="loginbtn"  type="submit" >Continue</button> 
                      <div id="exiuser">existing user ?
                           <span id="signupp" onClick = {()=>this.toggleForgetPassword()}>Login</span>
                      </div>
                      </form>
                    </div>
                    
                    : <div id="rightdiv">
                  
                  
                  
                  <form onSubmit={this.Login}>

                 <div id ="enteremail" class="input-group">
                  
                    {/* Enter registered email */}
                  <input 
                     class={this.state.email.length > 0?"formcontrol class_two":"formcontrol"}
                     type="email"
                     name="email" 
                     maxLength="30" 
                     id="foo"
                     onChange={e => {this.setState({email: e.target.value});
                                                        if(e.target.value.length>0)
                                                            {
                                                              {this.addClass(e)}
                                                            }
                                                         else{
                                                           {this.RemoveClass(e)}
                                                         }
                                                          }} 
                     required
                    // placeholder=" Enter registered email"
                    value={this.state.email} />
                  <label>
                    Enter Email
                  </label>
                 </div>

                  <div id = "enterpass" class ="input-group">
                    {/* Enter Password */}
                   <input 
                      id ="registerpass"
                      className={this.state.password.length > 0?"formcontrol class_two":"formcontrol"}
                      type="Password"  
                      name="password" 
                      maxLength="20"
                      onChange={e => {this.setState({password: e.target.value});
                                                    if(e.target.value.length>0)
                                                    {
                                                      {this.addClass(e)}
                                                    }
                                                else{
                                                  {this.RemoveClass(e)}
                                                }
                                                }
                                              }
                      // placeholder="Enter Password"
                      required 
                      value={this.state.password}
                      />
                    <label>Enter Password</label>  
                    <p id="forgpwd" onClick={()=>this.toggleForgetPassword()}>Forget Password?</p>
                    </div>
                      <p id ="errormsg">{this.props.ErrMsg}</p>
                    
                      <button id="loginbtn" type="submit" >Login</button> 
                      <div id ="or">OR</div>





              <div id="Google">
            
                  <GoogleLogin 
                      theme="dark"
                      style={{width:"200px"}}
                      onSuccess={responseGoogle}
                      onFailure={responseGoogleFailure} 
                      buttonText="Sign In"
                      clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
                      cookiePolicy={'single_host_origin'} />
                  </div>

                  </form>
                  <div id="newuser">New to Mypustak ?&nbsp;
                    <span id="signupp" onClick = {()=>this.signupftoggle()}>create your account</span>
                  </div>
           </div>}
                <div id ="leftdiv">
                  <div id ="Lheading">Login
                  <div id ="subLheading">Get access to your account and Order</div>
                  </div>
                  
                </div>
              </div>: 
               <div id="maindivision">
                
                  <div id="rightdiv">
                    <p id ="errmsg">{SignedupErr}</p>
                  <form onSubmit={this.signupUser} >
                    <div id="enteremail" class="input-group">
                          {/* Enter register email  */}
                          
                          <input  
                            class={this.state.Semail.length > 0?"formcontrol class_two":"formcontrol"}
                            type="email" 
                            id="forgetmail" 
                            name="email" 
                            required
                            maxLength="30" 
                            value = {this.state.Semail}
                            onChange={e => {this.setState({Semail: e.target.value});
                                                  if(e.target.value.length>0)
                                                  {
                                                    {this.addClass(e)}
                                                  }
                                                  else{
                                                    {this.RemoveClass(e)}
                                                  }
                                    
                                      }}
                                      />
                            
                            <label>Enter Email</label>
                    </div>
                    <div class="input-group">
                    <input 
                    type="text" 
                    id="signupmobil"
                    class={this.state.mobilenumber.length > 0?"formcontrol class_two":"formcontrol"}
                 
                    name="password" 
                    maxLength="10"
                    
                    onChange = {(e)=>{this.validatemobile(e)
                                        if(e.target.value.length>0)
                                        {
                                          {this.addClass(e)}
                                        }
                                          else{
                                            {this.RemoveClass(e)}
                                          }
                                } }
                    value={this.state.mobilenumber}
                    required
                        />
                    <label>Enter Mobile No</label>
                    <p id = "errmob">{this.state.mobileErr}</p>
                    </div>
                        
                    <div id = "enterpass" class="input-group">
                      {/* Create Password */}
                      
                     
                      <input 
                        type="Password" 
                        id ="registerpass"
                        

                        class={this.state.validatepwd.length > 0?"formcontrol class_two":"formcontrol"}
                        name="password" 
                        maxLength="50"
                        value = {this.state.validatepwd}
                        onChange={(e)=>{this.validatepwd(e);
                                        if(e.target.value.length>0)
                                        {
                                          {this.addClass(e)}
                                        }
                                        else{
                                          {this.RemoveClass(e)}
                                        }
                                      } }
                        required 
                      
                        />
                        <label>Enter Password</label>
                      
                      </div>
                      <p>{this.state.SignupPassErr}</p>
                      <button id= "signedup">SignUp</button>
                      <div id ="or"></div>

              
                  <div id="Google">
                              <GoogleLogin 
                                  theme="dark"
                                  onSuccess={responseGoogle}
                                  onFailure={responseGoogleFailure} 
                                  buttonText="Sign Up"
                                  clientId="777778083620-jvpvd75hnaeeof770e3b1sko5mg59j25.apps.googleusercontent.com"
                                  cookiePolicy={'single_host_origin'} />
                    </div>

                  
                    <div id="exiuserd">existing user ?
                    <span id="signupp" onClick = {()=>this.signupftoggle()}>Login</span>
                  </div>
                         <p id="terms">By Clicking on SignUp,You Agree To Our Terms & Conditions, Privacy Policy.</p>
                        

              </form>
                       
                   


                  </div>
                  <div id ="leftdiv">
                      <div id ="Lheading">Sign Up
                        <div id ="subLheading">Create your account</div>
                      </div>
                  </div>

                

               </div>


                 }
              
          </Modal>
          </Aux>
          

      </div>
  );
  }
}
const mapStateToProps = state => ({
  ErrMsg:state.accountR.ErrMsg,
  loader:state.accountR.loading,
  userToken:state.accountR.token,
  SignUpErrMsg:state.accountR.SignUpErrMsg,

  // SerLoginDat.state.acGetLoginData,

})

export default connect(mapStateToProps,{LoginCheck,clearAll,showLoader,signupCheck,clearLoginErr,ActivteSuccesPopup})(Login)
