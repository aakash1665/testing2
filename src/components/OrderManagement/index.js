import React from "react";
import './OrderManagement.css';

import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import AppBar from '@material-ui/core/AppBar';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import SwipeableViews from 'react-swipeable-views';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import moment from "moment";
import { connect } from "react-redux";
import { grey, blue } from '@material-ui/core/colors';
// import DateBookFilter from '../BackOffice/dateBookFilter';
import IconButton from "@material-ui/core/IconButton";
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import axios from 'axios';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableRow from '@material-ui/core/TableRow';
import { TableCell } from "@material-ui/core";

import { getOrders, updateOrder, cancelOrder, sendEmailForOrder, updateOrderBook, updateOrderWeight, cpRecommendation, createPOST, fetchPendingCod , ClearfetchPendingCod } from "../../actions/backendConfirmOrderAction";
import { REDIRECTWALLETTOCART } from "../../actions/types";
import RackDataFilter from "../BookInventoryPage/rackdatafilter";

const styles = theme => ({
  root: {
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  card: {
    display: 'flex',
    flexDirection: 'row-reverse',
    margin: '12px 0',
    maxWidth: '500px'
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
  },
  detailsOrder: {
    display: 'flex',
    width: '100%',
  },
  content: {
    flex: '1 0 auto',
  },
  cover: {
    width: 151,
    backgroundSize: 'contain'
  },
  button: {
    margin: theme.spacing.unit,
    backgroundColor: "lavender",
    border: "1px solid black",
    fontWeight:"bold"
  },
  input: {
    display: 'none',
  },
  fake: {
    backgroundColor: grey[200],
    height: theme.spacing.unit * 1,
    margin: theme.spacing.unit * 2,
    // Selects every two elements among any group of siblings.
        '&:nth-child(2n)': {
          marginRight: theme.spacing.unit * 3,
    }},
    paper: {
      position: 'absolute',
      top: 36,
      right: 0,
      left: 0,
  },
});

function TabContainer({ children, dir }) {
  return (
    <Typography component="div" dir={dir} style={{ padding: 8 * 3 }}>
      {children}
    </Typography>
  );
}

class OrderManagement extends React.Component {
  state = {  
    value: 0,
    ordersTabValue: 0,

    openOrder: false,
    openClickPost: false,
    currentOrder: {},
    curreentOrderSetIndex: -1,
    selectedCP: '',
    token: '',
    password:'',
    IndiaPostTrackingNo:'',
    showDropDown:false,
    openCODOrder:false,
    currentCODOrderIndex:-1,
    currentCODOrder:{},
    codPendingOrderId:0,
    dub_orders:[],
    openDDDialog: false,
    CP_Enter_Pasword:0,
  };

  componentDidMount() {
    this.setState({ token: localStorage.getItem('user') });
    this.props.getOrders({ token: localStorage.getItem('user') });
  }

  handleChangeTabs = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };

  handleChangeOrdersTabs = (event, value) => {
    this.setState({ ordersTabValue :value });
  };

  handleChangeOrdersIndex = index => {
    this.setState({ ordersTabValue: index });
  };
  HandelOpenInput = (e)=>{
    if(e.target.value == "61"){
      // console.log("Okk");
      
    this.setState({password:true})
    }
    console.log(e.target.value, this.state.password);
    
  }

  HandelIndaiPostInput = (e)=>{
    if(e.target.value){
      // console.log("Okk");
      
    this.setState({IndiaPostTrackingNo:e.target.value})
    }
    console.log(e.target.value, this.state.password);
    
  }
  clickOnOrderSet = index => {
    console.log(index)
    this.setState({
      curreentOrderSetIndex: index,
      value: 0
    })
  }

  updateOrder = (order, updateOrder) => {
    if (window.confirm(`Do you want to confirm order #${order.order_id}?`)) {
      this.props.updateOrder({ order: Object.assign(order, updateOrder), token: this.state.token });
      // alert(`Order - #${order.order_id} Updated.`);
    }
  }

  cancelOrder = (order) => {
    if (window.confirm(`Do you want to cancel order #${order.order_id}?`)) {
      this.props.cancelOrder({order, token: this.state.token});
      // alert(`Order - #${order.order_id} Cancelled.`);
    }
  }

  sendEmail = (order) => {
    if (window.confirm(`Do you want to send email for order #${order.order_id}?`)) {
      this.props.sendEmailForOrder({order, token: this.state.token});
      // alert(`Order - #${order.order_id} Cancelled.`);
    }
  }

  handleClickOrder = (order, i) => {
    console.log( i, "currentOrderIndex")
    this.setState({
      openOrder: true,
      currentOrderIndex: i
      // currentOrder: order
    })
  }

  handleClickCODOrder = (order, i) => {
    // this.props.ClearfetchPendingCod()
    this.props.fetchPendingCod(order.order_id,this.state.token)
    console.log( i, "currentCODOrderIndex",order.order_id,this.state.token)
    this.setState({
      openCODOrder: true,
      currentCODOrderIndex: i,
      codPendingOrderId:order.order_id
    })
    
  }

  handleClose = () => {
    this.setState({
      openOrder: false,
      openClickPost: false,
      openCODOrder:false
    })
  }
  convertCourierName = (data) =>{
    // if(data == )
    return data
  }
  handlClickOnBookSelect = (e, book) => {
    console.log(book.title, e.target.checked,"handlClickOnBookSelect");

    this.props.updateOrderBook({ book_id: book.order_book_id, order_id: book.order_id, is_book_found: e.target.checked?"Y":"D", token: this.state.token });
    this.props.getOrders({ token: this.state.token });
  }
  handlClickOnBookMissingSelect = (e, book) => {
    console.log(book.title, e.target.checked,"handlClickOnBookMissingSelect");

    this.props.updateOrderBook({ book_id: book.order_book_id, order_id: book.order_id, is_book_found: e.target.checked?e.target.value:"D", token: this.state.token });
    this.props.getOrders({ token: this.state.token });
  }
  handlClickOnBookMissingSelectCodPending = (e, book) => {
    console.log(book.title, e.target.checked,"handlClickOnBookMissingSelectCodPending");
    this.props.updateOrderBook({ book_id: book.order_book_id, order_id: book.order_id, is_book_found: e.target.checked?e.target.value:"D", token: this.state.token });
    // this.props.getOrders({ token: this.state.token });
    this.props.fetchPendingCod(book.order_id,this.state.token)

  }
  handleClickOnBookFound = () => {
    this.props.getOrders({ token: this.state.token });
  }

  handleClickOnClickPost = (order) => {
    console.log(order.order_id,order,"currentOrder");
    
    this.props.cpRecommendation({ order, token: this.state.token });
    this.setState({
      currentOrder: order,
      selectedCP: '',
      openClickPost: true
    });
  }
  UpdateIndiaPostTracking =(e,Clickedorder) =>{
    e.preventDefault();
    console.log(this.state.IndiaPostTrackingNo,"Ind",e.target.cpList.value);
    if(e.target.cpList.value !== "Select"){
      let order = {
        order_id:Clickedorder.order_id,
        courier_name:e.target.cpList.value,
        status: "7", 
        shipment_tracking_no:this.state.IndiaPostTrackingNo
                  }
      this.props.updateOrder({order,token: this.state.token})
      this.props.getOrders({ token: this.state.token });
      // console.log("");
      
  }else if(!this.state.IndiaPostTrackingNo){
    alert("Enter Tracking No")
  }else{
    alert("Select the Courier Partner")
  }
  }
  saveAmountAndWeight = (e, order) => {
    e.preventDefault();
    let amount = e.target.amount.value;
    let weight = e.target.weight.value;
    this.props.updateOrderWeight({order, amount, weight, token: this.state.token});
    this.props.getOrders({ token: this.state.token });
  }

  createPost = async (cp) => {
    const { currentOrder } = this.state;
    const { recommendation } = this.props;

    console.log(currentOrder, recommendation, "zxcvasdfasd")

    this.setState({
      openClickPost : false
    })
    console.log(cp.account_id, "asdfasdf")
    if(window.confirm(`Do you want to confirm CP for ${this.state.currentOrder.order_id}?`)) {

      let books = this.state.currentOrder.books.map(book => {
        return 
      });

      let order_type = "COD";

      if(currentOrder.payusing !== "cod") {
        order_type = "PREPAID"
      }

      let body = {
        "pickup_info": {
            "pickup_state": "WEST BENGAL",
            "pickup_address": "MyPustak.com,First Floor ,East Bera beri , shiker Bagan , gopalpur, Kolkata- 700136 ",
            "email": "support@mypustak.com",
            "pickup_time": "2017-05-20T12:00:00Z",
            "pickup_pincode": "700136",
            "pickup_city": "Kolkata",
            "tin": "19AMYPA9932D1ZM",
            "pickup_name": "MyPustak",
            "pickup_country": "IN",
            "pickup_phone": "9007022851"
        },
        "drop_info": {
            "drop_address": `Address:- ${currentOrder.address} , Landmark:- ${currentOrder.landmark}`,
            "drop_phone": currentOrder.phone,
            "drop_country": "IN",
            "drop_state": currentOrder.state,
            "drop_pincode": currentOrder.pincode,
            "drop_city": currentOrder.city,
            "drop_name": currentOrder.rec_name
        },
        "shipment_details": {
            "height": 12,
            "order_type": order_type.toUpperCase(),
            "invoice_value": currentOrder.amount,
            "invoice_number": currentOrder.order_id,
            "invoice_date": moment().format("YYYY-MM-DD"),
            "reference_number": currentOrder.order_id,
            "length": 10,
            "breadth": 10,
            "weight": currentOrder.weight,
            "items": [
              {
                "product_url":"MYPUSTAK.COM",
                "price": currentOrder.amount,
                "description": "BOOKS",
                "quantity": currentOrder.no_of_book,
                "sku": "XYZ1",
                "return_days": 2,
                "additional": {
                    "length": 10,
                    "height": 10,
                    "breadth": 10,
                    "weight": currentOrder.weight,
                    "images": "http://sample-file1.jpg,http://sample-file2.jpg"
                }
              }
            ],
            "cod_value": order_type === "COD" ? currentOrder.amount : 0,
            "courier_partner": cp.cp_id
        },
        "additional": {
            "account_code": cp.account_code
        }
      }
      this.setState({
        createPOSTLoading: true
      });
      console.log(body,"bodyPassing");
      
      let res = await this.props.createPOST({ body });

      if(res) {

        let order = {
            order_id: currentOrder.order_id,
            status: "7", 
            expected_price: cp.base_price,
            shipment_tracking_no: res.shipment_tracking_no,
            courier_name: cp.cp_id
        }
        
        this.props.updateOrder({order, token: this.state.token});
  
        this.props.getOrders({ token: this.state.token });
      }

      this.setState({
        createPOSTLoading: false
      });
      

    } else {
      console.log(false);
    }
  }

  handleChange = event => {
    this.setState({ selectedCP: event.target.value });
  };
  onClickDDHandler = (dub_orders)=>{
    console.log("dub_order is ", dub_orders)

    this.setState({openDDDialog: true, dub_orders: dub_orders})

    // let body = {
    //   'user_id':user_id,
    //   'pincode':pincode
    // }
    
    // let header = {
    //   headers: {
    //     Authorization: `Token ${this.state.token}`
    //   }
    // };

    // axios.post(`http://localhost:8000/api/v1/order_details_api/duplicate-orders/${order_id}`, body, header)
    // .then(res =>{
    //   this.setState({Order_orders:res.data.orders, openDDDialog:true})
    //   console.log("order_orders are ", res.data.orders)
    // })
    // .catch(err =>{
    //   console.log("order_orders err ", err)
    // })

  }

  closeDDDialog = () =>{
    this.setState({openDDDialog: false})
  }

  ActiavteCP_Btn = (e) =>{
    this.setState({CP_Enter_Pasword : e.target.value})
  }
  render() {
    const { curreentOrderSetIndex, currentOrderIndex, selectedCP,currentCODOrderIndex,codPendingOrderId } = this.state;
    const { classes, theme, orders, loading, confirmationPendingOrders, sendingEmail, recommendations, recommendationLoding ,codPending } = this.props;
    let currentOrderChunk = {};
    let currentOrder = {};
    // let currentCODOrder = {};

    if(curreentOrderSetIndex !== -1) {
        currentOrderChunk = orders[curreentOrderSetIndex];
        currentOrder = currentOrderChunk.orders[currentOrderIndex] || {};
    }
    // const handleClickCODOrder = (order, i) => {
      
    //   this.props.fetchPendingCod(order.order_id,)
    //   console.log( i, "currentCODOrderIndex",order)
    //   this.setState({
    //     openCODOrder: true,
    //     currentCODOrderIndex: i,
    //   })
    //   currentCODOrder = order
    //   console.log(currentCODOrder);
      
    // }
    console.log(currentOrder,"zxcvasdfasd")
    console.log(confirmationPendingOrders,"confirmcod");

    return (
      <div>
        {/* <Paper className={classes.root + ' ordersets-wrapper'} elevation={1}> */}
          <div style={{ minWidth: '100%' }}>

            {
              curreentOrderSetIndex === -1 ? (
                <React.Fragment>
                {/* <React.Fragment> */}
                {/* </React.Fragment> */}
                <AppBar position="static" color="default">
                  <Tabs
                    value={this.state.ordersTabValue}
                    onChange={this.handleChangeOrdersTabs}
                    indicatorColor="primary"
                    textColor="primary"
                    variant="fullWidth"
                    centered
                  >
                    <Tab label="Confirm Orders" />
                    <Tab label="COD Orders" />
                  </Tabs>
                  </AppBar>
                  <SwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={this.state.ordersTabValue}
                    onChangeIndex={this.handleChangeOrdersIndex}
                    style={{ border: '1px solid #d5d5d5', background: 'white', maxHeight: '79vh' }}
                  >
                    <TabContainer dir={theme.direction}>
                      <div className="tab-div">
                      {
                        loading 
                        ? <div> Loading... </div>
                        : curreentOrderSetIndex === -1 ? (
                              <React.Fragment>
                                {
                                  Object.keys(orders).length 
                                  ? Object.keys(orders).map((orderset, i) => (
                                      <div key={i} style={{ maxWidth: '288px' }} >
                                      <Card className={classes.card}  onClick={() => this.clickOnOrderSet(i)}>
                                        <div className={classes.details}>
                                          <CardContent className={classes.content} style={{ textAlign: 'center' }}>
                                            <Typography variant="h4" component="h3">
                                              {`OrderSet ${i}`}
                                            </Typography>
                                          </CardContent>
                                        </div>
                                    </Card>
                                    </div>
                                  ))
                                  : <div> There isn't any order </div>
                                }
                                </React.Fragment>
                            ) : ''
                      }
                      </div>
                    </TabContainer>
                    <TabContainer dir={theme.direction}>
                    <div className="tab-div">
                      {
                        loading 
                        ? 
                          sendingEmail ? (
                            <div> Sending email... </div>
                          ) : (
                            <div> Loading... </div>
                          )
                        : (
                          <div>
                          <div style={{ textAlign: 'center' }}> {confirmationPendingOrders.length + " order(s) are pending. "} </div>
                          <div style={{ display: 'flex', justifyContent: 'space-around', flexWrap: 'wrap' }}> 
                            {
                              confirmationPendingOrders.map((order, i) => {
                                return (
                                  <div key={order.order_id} style={{ maxWidth: '288px' }}>
                                    <Card className={classes.card}>
                                        <div className={classes.details}>
                                          <CardContent className={classes.content}>
                                            <div style={{ display: 'flex' }}>
                                              <div style={{ minWidth: '62%' }}>
                                                <Typography className={classes.pos} color="textSecondary">
                                                  OrderNo:
                                                </Typography>
                                                <Typography variant="h5" component="h2" onClick={() => this.handleClickCODOrder(order, i)}>
                                                  {"#" + order.order_id}
                                                </Typography>
                                                <Typography className={classes.pos} color="textSecondary">
                                                  Recepeient Name: <strong> {order.rec_name} </strong>
                                                </Typography>
                                                <Typography className={classes.pos} color="textSecondary">
                                                  Amount:  <strong style={{ textDecorationLine: 'line-through' }}> {order.amount} </strong>
                                                </Typography>
                                                <Typography className={classes.pos} color="textSecondary">
                                                  Date: <strong>{moment.unix(order.i_date).format("DD-MM-YYYY")}</strong>
                                                </Typography>
                                                <Typography className={classes.pos} color="textSecondary">
                                                  COD Charge: <strong> {order.cod_charge} </strong>
                                                </Typography>
                                                <Typography className={classes.pos} color="textSecondary">
                                                  Address: <strong> {order.city + ', ' + order.state} </strong>
                                                </Typography>
                                                <div style={{ display: 'flex' }}>
                                                  <Button size="small" color="primary" className={classes.button}>
                                                    VIEW
                                                  </Button>
                                                  <Button size="small" color="primary" className={classes.button} onClick={ () => this.sendEmail(order) }>
                                                    EMAIL
                                                  </Button>
                                                </div>
                                              </div>
                                              <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center', alignItems: 'center' }}>
                                                <Button size="small" color="primary" variant="outlined" className={classes.button} href={`tel:+91${order.phone}`}>
                                                  {order.phone}
                                                </Button>
                                                <Button size="small" color="primary" variant="outlined" className={classes.button} onClick={ () => this.sendEmail(order)}>
                                                  Convert
                                                </Button>
                                                {
                                                  order.delivery_available === "available" ? (
                                                    <Button size="small" color="primary" variant="contained" className={classes.button} onClick={() => this.updateOrder(order, { cod_offer_interested: 2 })}>
                                                      Confirm
                                                    </Button>
                                                  ) : (
                                                    <Button size="small" color="primary" variant="contained" className={classes.button} disabled>
                                                      {order.pincode}
                                                    </Button>
                                                  )
                                                }
                                                
                                                <Button size="small" color="secondary" variant="outlined" className={classes.button} onClick={() => this.cancelOrder(order)}>
                                                  Cancel
                                                </Button>
                                              </div>
                                            </div>
                                          </CardContent>
                                        </div>
                                    </Card>
                                  </div>
                                )
                                })
                            }
                          </div>
                          </div>
                        )
                        
                      }
                      </div>
                    </TabContainer>
                  </SwipeableViews>
                </React.Fragment>
              ) : ''
            }
            
            {
              curreentOrderSetIndex !== -1 ? (
                <React.Fragment>
                  
                  <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', height: '24px' }}> 
                    {/* <div onClick={ () => this.clickOnOrderSet(-1) } style={{ cursor: 'pointer' }}> {"< Back"}  </div>  */}
                    <Button variant="outlined" size="small" onClick={ () => this.clickOnOrderSet(-1) }> Back </Button>
                    <div> 
                      <Typography variant="h6" component="h3" color="textSecondary">
                        {`OrderSet ${curreentOrderSetIndex}`}
                      </Typography>
                     </div> 
                    <Button variant="contained" size="small" color="primary" onClick={ () => this.props.getOrders({ token: this.state.token }) }> Refresh </Button>
                  </div>
                  <AppBar position="static" color="default" style={{ marginTop: '12px' }}>
                    <Tabs
                      value={this.state.value}
                      onChange={this.handleChangeTabs}
                      indicatorColor="primary"
                      textColor="primary"
                      variant="fullWidth"
                      centered
                    >
                      <Tab label="Orders" />
                      <Tab label="Racks" />
                    </Tabs>
                    </AppBar>
                    <SwipeableViews
                      axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                      index={this.state.value}
                      onChangeIndex={this.handleChangeIndex}
                      style={{ border: '1px solid #d5d5d5', background: 'white', maxHeight: '75vh' }}
                    >
                      <TabContainer dir={theme.direction}>
                      <div className="tab-div" style={{ maxHeight: 'calc(100vh - 272px)', overflow: 'scroll' }}>
                        {
                          currentOrderChunk.orders.map((order, i) => 
                            <div key={order.order_id} style={{ maxWidth: '288px' }}> 
                              <Card className={classes.card} style={{ display: 'flex', flexDirection: 'column' }}>
                                  <div className={classes.detailsOrder}>
                                    <CardContent className={classes.content} style={{ maxWidth: '130px',paddingRight:'0px'}}>
                                      <Typography variant="h5" component="h2" onClick={() => this.handleClickOrder(order, i)}>
                                        {"#" + order.order_id}
                                      </Typography>
                                      <Typography className={classes.pos} color="textSecondary">
                                        {moment.unix(order.i_date).format("DD-MM-YYYY")}
                                      </Typography>
                                      <div> { order.city + ',' } </div>
                                      <div> { order.state } </div>
                                      <div
                                          style={{ 
                                            marginTop:"18%"
                                           }}                                  
                                        >
                                        <form onSubmit={(e)=>this.UpdateIndiaPostTracking(e,order)}>
                                 
                                        <select name="cpList">
                                        <option >Select</option>
                                          <option value="1001">India Post</option>
                                          <option value="4">Delhivery</option>
                                          <option value="1">Fedex</option>
                                     
                                        </select>
                                        <input type="text" onChange={this.HandelIndaiPostInput} name="indiaPostTracking"placeholder="I.P. Tracking " maxLength="40"  />
                                        <Button type="submit" size="small" color="primary" className={classes.button} style={{ lineHeight: "17px" }}>
                                          Add Tracking
                                        </Button>
                                        </form>
                                        {/* {this.state.IndiaPostTrackingNo} */}
                                        </div>
                                    </CardContent>
                                    <div style={{ minWidth: '75%', padding: '14px 0' }}>
                                      <div style={{  maxWidth: '140px', display: 'flex', justifyContent: 'space-between' }}>
                                        <div> { order.books.filter(book => book.is_book_found === "Y"|| book.is_book_found === "M").length + "/" + order.no_of_book } </div>
                                        
                                        {/* <div> { order.amount } </div> */}
                                        <div> { order.payusing } </div>
                                        {order.dub_order.length>1 ? <div><Button style={{backgroundColor:"rgb(234,141,112)"}} onClick={()=>this.onClickDDHandler(order.dub_order)}>DD</Button></div>: null}

                                        
                                      </div>
                                      
                                      <div style={{ maxWidth: '150px', display: 'flex', flexDirection: 'column', justifyContent: 'space-between', alignItems: 'center' }}>
                                          <form onSubmit={(e) => this.saveAmountAndWeight(e, order)}>
                                          <input style={{ maxWidth: '110px' }} name="weight" defaultValue={order.weight} placeholder="weight(in grams)"/>
                                          <input style={{ maxWidth: '110px' }} 
                                          disabled = {(this.state.password)? "" : "disabled"} 
                                          name="amount" defaultValue={order.amount} placeholder="amount"/>

                                          <Button type="submit" size="large" color="primary" className={classes.button} style={{ lineHeight: "17px" }}>
                                            Save
                                          </Button>
                                          </form>
                                      </div>
                                        <div
                                        style={{ 
                                          marginLeft:"16%",
                                          fontWeight:"bold",
                                         }}
                                        >
                                        <div> { "AMT: " + order.amount } </div>
                                        <div> { "WT: " + (order.weight ? order.weight : '---') + " gms" } </div>
                                        <input type="text" onChange={this.HandelOpenInput} placeholder="OTP" maxLength="2" style={{ maxWidth: '110px' }} />
                                        </div>
                                        
                              
                                    </div>
                                  </div>
                                  <Divider />
                                  <div style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}>
                                    <Button 
                                      size="small" 
                                      variant="outlined" 
                                      color="primary" 
                                      className={classes.button} 
                                      // disabled={ !(order.no_of_book === order.books.filter(book => book.is_book_found === "Y"||book.is_book_found === "M").length) }
                                      disabled={ !(order.no_of_book === order.books.filter(book => book.is_book_found === "Y"|| book.is_book_found === "M" && this.state.CP_Enter_Pasword ==  925).length)  }
                                      onClick={ () => this.handleClickOnClickPost(order) } >
                                      ClickPost
                                    </Button>
                                    {(order.books.filter(book => book.is_book_found === "M" ).length)?
                                    <div style={{ width:'25%' }}>
                                      <input 
                                      type="text" 
                                      placeholder="Activate CP" 
                                      onChange={this.ActiavteCP_Btn}
                                      maxLength={5}
                                      />
                                    </div>
                                    :null
                                    }
                                </div>
                              </Card>
                            </div>
                          )
                        }
                        </div>
                      </TabContainer>
                      <TabContainer dir={theme.direction}>
                      <div className="tab-div" style={{ maxHeight: 'calc(100vh - 272px)', overflow: 'scroll' }}>
                        {
                          currentOrderChunk.rackBooksOfOrderChunk.map((book, i) => 
                          <div key={i} style={{ maxWidth: '288px' }}>
                            <Card className={classes.card} style={ book.is_book_found == "Y" ? { background: 'green' } : {} }>
                                <div className={classes.details}>
                                  <CardContent className={classes.content}>
                                    <Typography variant="h5" component="h2">
                                      {book.racks}
                                    </Typography>
                                    <Typography className={classes.pos} color="textSecondary">
                                      {"#" + book.order_id}
                                    </Typography>
                                    <Typography component="p">
                                      {book.title}
                                    </Typography>
                                    <div style={{ display: 'flex', alignItems: 'center' }}> found?<input className="checkmark" type="checkbox" name="book" value="Is found?" defaultChecked={ book.is_book_found == "Y" ? true : false } onChange={(e) => this.handlClickOnBookSelect(e, book)}/> 
                                    
                                    </div>
                                  </CardContent>
                                </div>
                                <CardMedia
                                  className={classes.cover}
                                  image={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${book.thumb}`}
                                  title={book.title}
                                />
                            </Card>
                          </div>
                          )
                        }
                        </div>
                      </TabContainer>


                    </SwipeableViews>
                    </React.Fragment>
              ) : ''
            }

            <Dialog
              open={this.state.openOrder}
              onClose={this.handleClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">{"Order #"+currentOrder.order_id}</DialogTitle>
              
              <DialogContent>
                <div style={{ maxHeight: '500px' }}>
                  {
                    currentOrder.books && currentOrder.books.length ? currentOrder.books.map((book, i) => 
                      <div key={i}> 
                      {/* <div key={i} style={ order.is_book_found == "N" ? { background: 'red' } : {} }> */}
                        <Card className={classes.card} style={ book.is_book_found == "Y" ? { background: 'green' } : book.is_book_found == "M" ? { background: 'red' }:{} }>
                            <div className={classes.details} >
                              <CardContent className={classes.content}>
                                <Typography variant="h5" component="h2">
                                  {book.racks}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                  {"#" + book.order_id}
                                </Typography>
                                <Typography component="p">
                                  {book.title}
                                </Typography>
                                <Typography component="p">
                                  Wt: {book.weight}
                                </Typography>
                                <div style={{ display: 'flex', alignItems: 'center', position: 'relative' }}> 
                                <span style={{ fontSize:'110%',width:'25%',marginRight:'20%'}}>found?<input className="checkmark" type="checkbox" name="book" style={{ transform: "scale(1.5)",marginLeft:"10%" }} value="Is found?" checked={ book.is_book_found === "Y" ? true : false } onChange={(e) => this.handlClickOnBookSelect(e, book)}/> </span>
                                <span style={{ margin:'0% 7%',fontSize:'110%' }}>missing?<input className="checkmark" type="checkbox" name="book" style={{ transform: "scale(1.5)" }} value="M" checked={ book.is_book_found == "M" ? true : false } onChange={(e) => this.handlClickOnBookMissingSelect(e, book)}/></span> 
                                {/* {book.is_book_found} */}

                                </div>
                                </CardContent>
                            </div>
                            <CardMedia
                              className={classes.cover}
                              image={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${book.thumb}`}
                              title={book.title}
                            />
                        </Card>
                      </div>
                    ) : <div style={{ textAlign: 'center' }}> No Books </div>
                  }
                  </div>
              </DialogContent>
            </Dialog>

            <Dialog
            open={this.state.openDDDialog}
            onClose={this.closeDDDialog}
            aria-labelledby="form-dialog-title"
            scroll={"body"}
            size="sm"
          >
            <DialogTitle id="form-dialog-title">{`Duplicate Orders for this user`}</DialogTitle>
            <DialogContent>
                  <React.Fragment>
                  {this.state.dub_orders.map((order, i) =>
                  <div style={{border: "2px solid blue", marginBottom:15, backgroundColor:'rgb(200,180,180)', width:230}}>
                  <Table key={i}>
                  <TableBody style={{width:180}}>
                    <TableRow><TableCell>Order ID</TableCell><TableCell>{order.order_id}</TableCell></TableRow>
                    <TableRow><TableCell>User ID</TableCell><TableCell>{order.user_id}</TableCell></TableRow>
                    <TableRow><TableCell>Pincode</TableCell><TableCell>{order.pincode}</TableCell></TableRow>
                    <TableRow><TableCell>Rec Name</TableCell><TableCell>{order.rec_name}</TableCell></TableRow>
                    <TableRow><TableCell>Payusing</TableCell><TableCell>{order.payusing}</TableCell></TableRow>
                    <TableRow><TableCell>State</TableCell><TableCell>{order.state}</TableCell></TableRow>
                    <TableRow><TableCell>City</TableCell><TableCell>{order.city}</TableCell></TableRow>
                    </TableBody>
                    </Table>
                    </div>
                  )}

                  </React.Fragment>

            </DialogContent>

        </Dialog>





            <Dialog
              open={this.state.openCODOrder}
              onClose={this.handleClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">{"Order #"+codPendingOrderId}</DialogTitle>
              
              <DialogContent>
                <div style={{ maxHeight: '500px' }}>
                  {
                    codPending.length ? codPending.map((book, i) => 
                      <div key={i}> 
                      {/* <div key={i} style={ order.is_book_found == "N" ? { background: 'red' } : {} }> */}
                        <Card className={classes.card} style={ book.is_book_found == "Y" ? { background: 'green' } : book.is_book_found == "M" ? { background: 'red' }:{} }>
                            <div className={classes.details} >
                              <CardContent className={classes.content}>
                                <Typography variant="h5" component="h2">
                                  {book.racks}
                                </Typography>
                                <Typography className={classes.pos} color="textSecondary">
                                  {"#" + book.order_id}
                                </Typography>
                                <Typography component="p">
                                  {book.title}
                                </Typography>
                                {/* <div style={{ display: 'flex', alignItems: 'center', position: 'relative' }}> 
                                <span style={{ fontSize:'110%',width:'25%',marginRight:'20%'}}>found?<input className="checkmark" type="checkbox" name="book" style={{ transform: "scale(1.5)",marginLeft:"10%" }} value="Is found?" checked={ book.is_book_found === "Y" ? true : false } onChange={(e) => this.handlClickOnBookMissingSelectCodPending(e, book)}/> </span>
                                <span style={{ margin:'0% 7%',fontSize:'110%' }}>missing?<input className="checkmark" type="checkbox" name="book" style={{ transform: "scale(1.5)" }} value="M" checked={ book.is_book_found == "M" ? true : false } onChange={(e) => this.handlClickOnBookMissingSelectCodPending(e, book)}/></span> 

                                </div> */}
                                </CardContent>
                            </div>
                            <CardMedia
                              className={classes.cover}
                              image={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${book.thumb}`}
                              title={book.title}
                            />
                        </Card>
                      </div>
                    ) : <div style={{ textAlign: 'center' }}> No Books </div>
                  }
                  </div>
              </DialogContent>
            </Dialog>


            <Dialog
              open={this.state.openClickPost}
              onClose={this.handleClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="form-dialog-title">{"Order #"+this.state.currentOrder.order_id}</DialogTitle>
              <DialogContent>
                <div style={{ maxHeight: '500px', minWidth: '300px' }}>
                  {
                    recommendationLoding ? (
                      <div> Loading... </div>
                    ): recommendations.length 
                      ? <List>
                        {
                          recommendations.map((rec, i) => (
                            <ListItem button onClick={() => this.createPost(rec)} key={i}>
                              <ListItemText primary={rec.account_code + ' -> ' + rec.base_price.toFixed(2)} />
                            </ListItem>
                          ))
                        }
                        </List>
                      : <div> No preffered courier found. </div>
                  }
                </div>
              </DialogContent>
              {/* <DialogActions>
                <Button onClick={this.handleClose} color="default">
                  Cancel
                </Button>
                <Button onClick={this.createPost} variant="contained" color="primary" disabled={recommendationLoding} >
                  Confirm
                </Button>
              </DialogActions> */}
            </Dialog>
            
          </div>
        {/* </Paper> */}
      </div>
    );
  }
}

OrderManagement.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = ({ confirmOrderBR }) => {
  const { orders, confirmationPendingOrders, loading, sendingEmail, recommendation, recommendations, recommendationLoding,codPending,ClearfetchPendingCod } = confirmOrderBR;
  return { orders, confirmationPendingOrders, loading, sendingEmail, recommendation, recommendations, recommendationLoding,codPending,ClearfetchPendingCod };
};

export default connect(
  mapStateToProps,
  { getOrders, updateOrder, cancelOrder, sendEmailForOrder, updateOrderBook, updateOrderWeight, cpRecommendation, createPOST ,fetchPendingCod}
)(withStyles(styles, { withTheme: true })(OrderManagement));
