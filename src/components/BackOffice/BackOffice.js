import React from "react";
import PropTypes from "prop-types";
import AppBar from "@material-ui/core/AppBar";
import CssBaseline from "@material-ui/core/CssBaseline";
import Divider from "@material-ui/core/Divider";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import IconButton from "@material-ui/core/IconButton";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import MailIcon from "@material-ui/icons/Mail";
import MenuIcon from "@material-ui/icons/Menu";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";


import { withRouter } from "react-router";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect
} from "react-router-dom";

import DonationPage from "../DonationPage";
import OrderPage from "../OrderPage";
import WalletPage from "../WalletPage";
import BookInventoryPage from "../BookInventoryPage";
import OrdersManagement from "../OrderManagement";
import UpdateTrackingAndBarcode from "../UpdateTrackingAndBarcode";
import TotalSaleFilter from "../TotalSaleFilter";
import TotalDataEntryCountFilter from "../DataEntrySale";
import Googleshopping from "../googleShopping"
// import logo from "../Home/images/MyPustakLogo.png";

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: "flex"
  },
  drawer: {
    [theme.breakpoints.up("sm")]: {
      width: drawerWidth,
      flexShrink: 0
    }
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up("sm")]: {
      width: `calc(100% - ${drawerWidth}px)`
    }
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up("sm")]: {
      display: "none"
    }
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 2
  }
});

const backendPages = [
  { url: "order", name: "Order", longName: "Order" },
  {
    url: "donation",
    name: "Donation Request",
    longName: "Book Donation Request"
  },
  {
    url: "wallet",
    name: "Wallet",
    longName: "Wallet management"
  },
  {
    url: "bookinventory",
    name: "Book Inventory",
    longName: "Book Inventory Management"
  },
  {
    url: "ordersmanagement",
    name: "Orders Management",
    longName: "Orders Management"
  },
  {
    url: "updatetrackingandbarcode",
    name: "Update T&B",
    longName: "Update Tracking and Barcode"
  },
  {
    url: "dataentryreport",
    name: "Data Entry Report",
    longName: "Data Entry Report"
  },
  {
    url: "totalsalereport",
    name: "Total Sale Report",
    longName: "Total Sale Report"
  },

  {
    url: "googleshopping",
    name: "google shopping",
    longName: "Google Shopping"
  },
  {
    url: "backofficelogout",
    name: "backoffice logout",
    longName: "backoffice logout"
  },
];

class BackOffice extends React.Component {
  state = {
    mobileOpen: false,
    logoutDone: false
  };
  componentDidMount() {
    const userRole = localStorage.getItem("userRole");
    console.log(userRole);
    if (userRole != 1) {
      this.setState({ logoutDone: true });
    }
  }
  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  getRouteName() {
    let splitURL = this.props.location.pathname.split("/");
    let routeName = splitURL[splitURL.length - 1];
    return backendPages.find(page => page.url === routeName).longName;
  }

  render() {
    if (this.state.logoutDone) {
      return <Redirect to="" />;
    }
    if (window.location.pathname == "/backoffice/backofficelogout") {
      localStorage.removeItem("user");
      localStorage.removeItem("userRole");
      return <Redirect to="" />;
    }
    console.log(window.location.pathname);

    const { classes, theme } = this.props;

    const drawer = (
      <div>
        <div className={classes.toolbar}>
          {" "}
          <img src={'https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo.png'} width="100%" height="100%" />{" "}
        </div>

        <Divider />
        <List>
          {backendPages.map((page, index) => (
            <Link
              to={`${page.url}`}
              key={page.name}
              style={{ textDecoration: "unset" }}
            >
              {this.props.location.pathname.split("/")[2] === page.url ? (
                <ListItem button selected={true}>
                  {/* <ListItemIcon>
                      {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                    </ListItemIcon> */}
                  <ListItemText primary={page.name} />
                </ListItem>
              ) : (
                <ListItem button>
                  {/* <ListItemIcon>
                      {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                    </ListItemIcon> */}
                  <ListItemText primary={page.name} />
                </ListItem>
              )}
            </Link>
          ))}
        </List>
      </div>
    );

    return (
      <div className={classes.root}>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap>
              {this.getRouteName()}
            </Typography>
          </Toolbar>
        </AppBar>
        <nav
          className={classes.drawer}
          style={window.screen.width < 602 ? { width: 0 } : {}}
        >
          {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
          <Hidden smUp implementation="css">
            <Drawer
              container={this.props.container}
              variant="temporary"
              anchor={theme.direction === "rtl" ? "right" : "left"}
              open={this.state.mobileOpen}
              onClose={this.handleDrawerToggle}
              classes={{
                paper: classes.drawerPaper
              }}
            >
              {drawer}
            </Drawer>
          </Hidden>
          <Hidden xsDown implementation="css">
            <Drawer
              classes={{
                paper: classes.drawerPaper
              }}
              variant="permanent"
              open
            >
              {drawer}
            </Drawer>
          </Hidden>
        </nav>
        <main
          className={classes.content}
          style={{
            marginTop: "64px",
            height: "calc(100vh - 64px)",
            width: this.state.mobileOpen ? "100vw" : "calc(100vw - 260px)"
          }}
        >
          <Route
            exact
            path={this.props.match.url + "/order"}
            component={OrderPage}
          />
          <Route
            exact
            path={this.props.match.url + "/donation"}
            component={DonationPage}
          />
          <Route
            exact
            path={this.props.match.url + "/wallet"}
            component={WalletPage}
          />
          <Route
            exact
            path={this.props.match.url + "/bookinventory"}
            component={BookInventoryPage}
          />
          <Route
            exact
            path={this.props.match.url + "/ordersmanagement"}
            component={OrdersManagement}
          />
          <Route
            exact
            path={this.props.match.url + "/updatetrackingandbarcode"}
            component={UpdateTrackingAndBarcode}
          />
          <Route
            exact
            path={this.props.match.url + "/dataentryreport"}
            component={TotalDataEntryCountFilter}
          />
          <Route
            exact
            path={this.props.match.url + "/googleshopping"}
            component={Googleshopping}
          />
          <Route
            exact
            path={this.props.match.url + "/totalsalereport"}
            component={TotalSaleFilter}
          />
        </main>
      </div>
    );
  }
}

BackOffice.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired
};

export default withRouter(withStyles(styles, { withTheme: true })(BackOffice));
