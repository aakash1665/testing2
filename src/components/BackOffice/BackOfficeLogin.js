import React, { Component } from 'react'
import './BackOfficeLogin.css';
import PUserSignup from '../Home/PUserSignup';
import PUserLogin from '../Home/PUserLogin'
import { connect } from 'react-redux';
import { Redirect} from 'react-router-dom'

class BackOfficeLogin extends Component {
    state={
        ASignupopen:false,
        showLS:true,
        DoRedirect:false
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.userToken !== null && (nextProps.userRole == 1 || nextProps.userRole == 2) ){
            // alert("P")
            // if(this.props.userToken !== null){
              localStorage.setItem('user', nextProps.userToken)
              localStorage.setItem('userRole',nextProps.userRole)
            // }
            this.setState({DoRedirect:true})
        }
    }
    ToggleASignupOpenModal=()=>{
        // alert("onnnn")
        this.setState({ ASignupopen: !this.state.ASignupopen })
      }
      changeShowLS=()=>{
        this.setState({showLS:!this.state.showLS})
      }
    LOG_IN=<PUserLogin scloseModal={this.signupCloseModal} changeShowLS={this.changeShowLS} ShowMsg={this.props.ErrMsg}/>
    SIGN_IN=<PUserSignup scloseModal={this.signupCloseModal} changeShowLS={this.changeShowLS}/>
    
  render() {
      if(this.state.DoRedirect ){
        return <Redirect to="/backoffice/order"/>

      }
      
    return (
      <div id="BKOffLogin">
       {(this.state.showLS === true)?this.LOG_IN:this.SIGN_IN}
      </div>
    )
  }
}
const mapStateToProps = state => ({
    userToken:state.accountR.token,
    userRole:state.accountR.user_role_id,
  })
export default connect(mapStateToProps,null)(BackOfficeLogin)
