import React, { Component } from "react";
import { makeStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getUserDetails } from "../../actions/backofficeWalletUpdateActions";
import axios from "axios";

class UserDetailsWallet extends Component {
  static propTypes = {
    UserDetails: PropTypes.object.isRequired,
    getUserDetails: PropTypes.func.isRequired
  };

  state = {
    order_id: "",
    user_id: "",
    user_email: "",
    status: "",
    message: ""
  };

  onOrderIdChange = e => this.setState({ order_id: e.target.value });

  //   onUserIdChange = e => this.setState({ user_id: e.target.value });

  //   onUserEmailChange = e => this.setState({ user_email: e.target.value });

  onClickHandler = () => {
    this.props.getUserDetails(this.state.order_id);
    console.log("The data are ", this.props);
  };

  render() {
    const {
      order_id,
      user_id,
      user_email,
      status,
      message
    } = this.props.UserDetails;

    return (
      <form noValidate autoComplete="off">
        <TextField
          id="filled-orderid-input"
          placeholder="Order Id"
          type="number"
          name="order_id"
          margin="normal"
          variant="filled"
          defaultValue={order_id}
          onChange={this.onOrderIdChange}
        />
        <TextField
          id="filled-userid-input"
          placeholder="User Id"
          type="number"
          name="user_id"
          margin="normal"
          variant="filled"
          value={user_id}
          onChange={this.onUserIdChange}
        />
        <TextField
          id="filled-email-input"
          placeholder="Email"
          type="email"
          name="email"
          margin="normal"
          variant="filled"
          value={user_email}
          onChange={this.onUserEmailChange}
        />
        <Button
          variant="contained"
          color="primary"
          onClick={this.onClickHandler}
          margin="normal"
        >
          Get User Details
        </Button>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  UserDetails: state.userdetails.userdetails
});

export default connect(
  mapStateToProps,
  { getUserDetails }
)(UserDetailsWallet);
