import React, { Component } from 'react'
import axios from 'axios'
import Popup from 'reactjs-popup' 
import PUserSignup from './Home/PUserSignup';
import PUserLogin from './Home/PUserLogin'
import {doSearch,makeSearchBlank,setCategory} from '../actions/homeAction'
import { connect } from 'react-redux';
import MediaQuery from 'react-responsive';
// import homeicon from './mobimages/homeicon.png'
// import searchicon from './mobimages/searchicon.png'
// import {getSavedToken } from '../actions/accountAction'
// import {getSavedToken,logout } from '../actions/accountAction'
import {AddToCart,CartopenModal,CartcloseModal,removeAllCart,RemoveCart,CartSession,ToBeAddedToCart,RemoveToBeAddedToCart,removeFromCartLogout,MobileCartRedirect} from '../actions/cartAction'
import {logout,Getaddress,userdetails,Editaddress ,addAddressAction,getSavedToken,ActivteSuccesPopupOther,navurl,AfterLoginRedirect,login_backdropToggle} from '../actions/accountAction'
import './Popupcart.css'
// import UserLogin from '../components/Home/UserLogin'
// import UserSignup from '../components/Home/UserSignup';
import { Link,Redirect,browserHistory } from 'react-router-dom'
// import book from './book.jfif';
// import del from './product/Product_Images/del.png';
import config from 'react-global-configuration'
import Popupcat from './Home/Popupcategories'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Button from 'react-bootstrap/Button'
import Login from './LoginSignup/Login/Login';


class MainHeader extends Component {
    state={
        signuptoggle:true,
        showLS:false,
        Sopen:false,
        userBookQty:1,
        cartMsg:null,
        doRedirect:false,
        RedirectCart:false,
        ASignupopen:false,
        succesLogin:false,
        goTocart:false,
        name:'',
        Cat:false,
        logoutDone:false,
        // PopupCart:false,
    }

    componentDidMount(){
      // this.props.getBook(this.slug)
    //   const {compExamBooks} = this.props
    // console.log(this.slug);
    const value =localStorage.getItem('user');
    this.props.getSavedToken(value);
    this.setupBeforeUnloadListener();
    if(window.location.pathname != '/view-cart/thank-you'){
    this.listAllItems()}
    else{
      sessionStorage.clear()
    }
        // this.closeNav()
    // this.openNav()
    if(localStorage.getItem('user') !== null){
      const token=`Token ${localStorage.getItem('user')}`
      const details=`Token ${token}`
      this.props.CartSession(token)
      // this.props.CartSession(token)
    }else{


      
      // this.props.FromMycart.map(cart=>(
      this.props.cartDetails.map(book=>(
        this.props.ToBeAddedToCart({"book_id" : book.book_id,"book_inv_id":book.bookInvId[0]})
      ))
      // console.log("No");
      
    }
    if( window.location.pathname !== '/view-cart/thank-you'){
        localStorage.removeItem("UserOrderId")
    }

    }
    componentWillReceiveProps(nextProps){

      if(nextProps.userToken !== this.props.userToken ){
        const details=`Token ${localStorage.getItem('user')}`
        this.props.userdetails(details)
        // const details=`Token ${nextProps.userToken}`
        sessionStorage.clear();
        console.log("In Main Header Rece");
        try {
        this.props.ToBeAddedToCartState.map(book=>{
          if(book != null){
            // alert("Okk")
          // }
          const sendCartSession = { 
            "book_id" : book.book_id,
            "book_inv_id":book.book_inv_id,
            'content-type': 'application/json',
             }
          axios.post(`${config.get('apiDomain')}/common/addtocart/`,sendCartSession,{headers:{
          'Authorization': details,
          }} )
        .then(res=>{
          console.log(res.status,sendCartSession);
        console.log("cart Updated");

          RefreshCart();
          })
          .catch(err=>{console.log(err,sendCartSession);
            // console.log(sendCartSession);
          }
          )

          const RefreshCart=()=>{
            const token= localStorage.getItem('user')
            const details=`Token ${token}`
            this.props.CartSession(details)
          }
        }
        })
      } catch (error) {
        console.log(error);          
      }
        // console.log("Refreshing Header");

        this.props.CartSession(details)
  
      }
      try {
        if(this.props.CartSessionData.length !== nextProps.CartSessionData.length){
          // console.log(nextProps.CartSessionData.length);
          if(localStorage.getItem('user') != null){
            // console.log("Doing Refresh");
            
          nextProps.CartSessionData.map((book)=>{
            // console.log(book,"Form Db")
            if(book != 0){
              const MyCart ={
              Cart_id:book.cart_id,
              bookId:book.book_id,
              bookName:book.title,
              bookSlug:book.slug,
              bookPrice:Math.round(book.price),
              bookShippingCost:Math.round(book.shipping_cost),
              bookThumb:book.thumb,
              // bookDonor:DonorName,
              bookQty:1,
              bookCond:book.condition,
              bookRackNo:book.rack_no,
              bookInvId:book.book_inv_id,
              // bookDonner
              }
              // Count+=1
              // console.log(MyCart);
            this.props.AddToCart(MyCart)          
              try {
            // console.log(book,"Form Db To Local")
                  
                sessionStorage.setItem(book.book_inv_id,JSON.stringify(MyCart));
    
    
              } catch (error) {
              }
    
              if(sessionStorage.length !==0){
                this.props.removeAllCart()
            for (var i=0; i<=sessionStorage.length-1; i++)  
            {   
              if(sessionStorage.key(i) !== "UserOrderId"){
        
                let key = sessionStorage.key(i);  
                try {
                  let val = JSON.parse(sessionStorage.getItem(key));
                  // console.log(val.bookInvId  )
                  // val.map(det=>console.log(det))
                  
                  this.props.AddToCart(val)
                  // alert(val)
                } catch (error) {
                  //  console.log(error);
                  
                }
        
              }
            } }
            }
          }
            )
          }else{
            // console.log("Not Login");
            
          }
      }
      } catch (error) {
            console.log(error);
            
      }
    }
    OPENCategory=()=>{
      this.setState({Cat:true})
    }
    CLOSECategory=()=>{
      this.setState({Cat:false})
    }
    setupBeforeUnloadListener = () => {
      window.addEventListener("beforeunload", (ev) => {
          // ev.preventDefault();
          // sessionStorage.clear();
      });
  };
    listAllItems=()=>{  
      // alert("in see")
      if(sessionStorage.length !==0){
        this.props.removeAllCart()
     for (var i=0; i<=sessionStorage.length-1; i++)  
     {   
      if(sessionStorage.key(i) !== "UserOrderId" && sessionStorage.key(i) !== "TawkWindowName"){

         let key = sessionStorage.key(i);  
         try {
          let val = JSON.parse(sessionStorage.getItem(key));
          // console.log(val.bookInvId)
          // val.map(det=>console.log(det))
          if(localStorage.getItem('user') === null){
            this.props.RemoveToBeAddedToCart()
          this.props.ToBeAddedToCart({"book_id" :val.bookId ,"book_inv_id":val.bookInvId[0] })

          }
          else{
            this.props.RemoveToBeAddedToCart()
          }
          if(window.location.pathname != '/view-cart/thank-you'){
          this.props.AddToCart(val)
          }else{
            try {
                  sessionStorage.clear();
            } catch (error) {
              
            }
          }
          // alert(val)
         } catch (error) {
          //  console.log(error);
           
         }

      }
     } }
   
   }
   RemoveFromCart=(bookInvId,Cart_id)=>{

    for (var i=0; i<=sessionStorage.length-1; i++)  
    {   
      // alert("rmo")
        let key = sessionStorage.key(i);  
        try {
          if(sessionStorage.key(i) !== "UserOrderId"  && sessionStorage.key(i) !== "TawkWindowName"){
              let val = JSON.parse(sessionStorage.getItem(key));
              if(`${val.bookInvId}` === `${bookInvId}`)  {
                // alert("rm")
                sessionStorage.removeItem(bookInvId);
              }
          }
        } catch (error) {
          console.log(error);
          
        }

        // val.map(det=>console.log(det))
        // this.props.AddToCart(val)
        // alert(val)
    }
    if(localStorage.getItem('user') === null){

    this.props.removeFromCartLogout(bookInvId)
    }else{
      const data={"is_deleted":"Y"}

    this.props.RemoveCart(Cart_id,bookInvId,data)

    }
   }
    changeShowLS=()=>{
        this.setState({showLS:!this.state.showLS})
      }
    ToggleASignupOpenModal=()=>{
        // alert("onnnn")
        this.setState({ ASignupopen: !this.state.ASignupopen })
      }
      ASignupCloseModal=()=>{
        // alert("c")
        this.setState({ ASignupopen: false })
      }
    sopenModal=()=>{
      // alert("o2")
        this.setState({ Sopen: true })
      }
    scloseModal=()=>{
      // alert("c")
      //   this.setState({ Sopen: false })
    }
    IncrementItem = () => {
      if(this.state.userBookQty >= this.props.cartDetails.bookQty){
        this.setState({cartMsg:'Book Out Of Stock'})
        setTimeout(()=>{this.setState({cartMsg:''})},2000)
      }else{
        
        this.setState({ userBookQty: this.state.userBookQty + 1 });
      }
    }
    DecreaseItem = () => {
      if(this.state.userBookQty !== 1){
      // this.setState({cartMsg:'Book Out Of Stock'})
      this.setState({ userBookQty: this.state.userBookQty - 1 });
    }
    }
    CartFadeOut=()=>{
      setTimeout(()=>{this.props.CartcloseModal()},2000)
    }
    getName(){
      axios.get(`${config.get('apiDomain')}/core/user_details/ `,{headers:{
          // axios.get(`http://127.0.0.1:8000/core/hello/ `,{headers:{
          'Authorization': `Token ${this.props.userToken}`
        }})
        .then(res=>this.setState({name:res.data.name}))
        .catch(err=>console.log(err)
        )
        
      }
    // CartopenModal=()=>{
    //   this.setState({PopupCart: true})
    // }
    onSearchChange = e => {
      this.props.doSearch(this.refs.searchBookFixed.value)
      
      // this.DoSearch(e.target.value,1)
      // axios.get(`http://127.0.0.1:8000/search/${this.props.searchBook}/${this.state.start}/`)
      // axios.get(`http://103.217.220.149:80/search/${this.props.searchBook}/${this.state.start}/`)
      //   .then(res=> this.setState({SearchResult:res.data.hits}))
      //   .catch(err=>console.log(err)
      //   )
      this.setState({doRedirect:true})
      }
    LOG_IN=<PUserLogin scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS} ShowMsg={this.props.ErrMsg}/>
    SIGN_IN=<PUserSignup scloseModal={this.ToggleASignupOpenModal} changeShowLS={this.changeShowLS}/>

    CheckLogin=()=>{
      // this.ToggleASignupOpenModal()
    //   if(this.props.userToken === null){
    //     alert("check")
      
    //   if(this.props.userToken === null){
    //     this.setState({RedirectCart:true})
    //   }
    // }else{
    //     // return  <Redirect to='/view-cart'/>
    //     this.setState({RedirectCart:true})
    //   }
      // (this.props.userToken === null)?" okk":"o"
    }
    Userlogout=()=>{
      this.props.removeAllCart();
      sessionStorage.clear();
      localStorage.removeItem('user');
      this.props.logout()
      // this.signupCloseModal()
      // window.location.href = ''
      // this.setState({Sopen:false})
      // return <Redirect to =''></Redirect>
      this.setState({ASignupopen:false})
      this.setState({logoutDone:true})
    }
    goToPrevious(){

    }
      
  //  openNav=()=>{
  //   document.getElementById("mySidenav").style.display = "block";
  // }

//  closeNav=()=>{
//   document.getElementById("mySidenav").style.display = "none";
  // }
  openNav=()=>{
    // alert("OpenNavs")
    this.setState({CloseNav:!this.state.CloseNav})
    // document.getElementById("mySidenav").style.display = "block";
  }

 closeNav=()=>{
  //  alert("change")
    // document.getElementById("mySidenav").style.display = "none";
    this.setState({CloseNav:!this.state.CloseNav})
  }


  render() {
    if(localStorage.getItem('user') === null){
      this.props.AfterLoginRedirect(window.location.pathname)      
    }
    const SetCategory=(category)=>{
      // alert(category);
      this.props.setCategory(category);
      // this.props.CloseCatPopup()
    }
    if(this.props.userToken !== null){
      localStorage.setItem('user', this.props.userToken)
    }
   const CheckLogin=()=>{
     if(this.props.userToken === null){
      this.ToggleASignupOpenModal()
      this.setState({goTocart:true})
      // if(this.props.userToken !== null){
      //   this.setState({RedirectCart:true})
      // }
    }else{
      this.setState({RedirectCart:true})
    }
    }
    const MobileCheckLogin=()=>{
      if(this.props.userToken === null){
        this.props.MobileCartRedirect()
        return <Redirect to="/login"/>
     }else{
      // return <Redirect to="/view-cart"/>
      //  this.setState({RedirectCart:true})
      this.props.CartopenModal()
     }
     }
     if(this.props.cartRedirectMobile){
      return <Redirect to="/login"/>

     }
     if(this.state.goTocart === true && this.props.userToken === null){
      if(window.innerWidth > 0){
        if(window.innerWidth < 991){
      // return  <Redirect to='/login'/>;
        }
      }
    }
    if(this.state.goTocart === true && this.props.userToken !== null){
      return  <Redirect to='/view-cart'/>;
    }
    let url= window.location.href;
    var inCart =url.search('/view-cart')
    if(this.state.doRedirect ){
      // alert("redirect")
    return  <Redirect to='/search'/>;
    }
    if(this.state.RedirectCart && inCart === -1){
      // alert("redirect")
    return  <Redirect to='/view-cart'/>;
    }
    // console.log(this.props.cartDetails);
    
    const {cartDetails} = this.props
    var TotalShipCost =0
    cartDetails.map(cart=> TotalShipCost+=Number(cart.bookShippingCost) )
    let thumb = cartDetails.bookThumb;
    // console.log(`${this.props.cartDetails} `);
    if(this.props.userToken !== null && this.state.name ==='' )
    {

      this.getName()
      const details=`Token ${this.props.userToken}`
      this.props.userdetails(details)
      this.props.Getaddress(details)
      // this.props.addAddressAction(details)
      // alert("name")
    //  this.setState({sopen:false}) 
    if(this.state.name !=='')
    {
    this.scloseModal()}
    }
    if(this.state.logoutDone){
    return <Redirect to=""/>
    }
   const SearchSubmit=(e)=>{
    //  alert(e.value)
      e.preventDefault();
      // alert()
    }
    return (
      <div>
                 {/* <Popup
          open={true}
          // closeOnDocumentClick={false}
          // onClose={()=>this.setState({addressopen:false})}
          // onOpen={()=>()=>{setTimeout(alert("okk"),3000)}}
          contentStyle={{ 
                        width:'40%',
                        height:'47%',
                        borderRadius:'5px',
                        // marginLeft:'67%',
                        marginTop:'7%',
                        // background:'transparent'
                        // pa
                        }} 
          overlayStyle={{ 
            // background:'transparent',
           }}
        >
                <a className="close" onClick={this.props.ActivteSuccesPopup} style={{
                  color: 'black',zIndex:'999',opacity:'1',fontWeight:'normal',
                  float:'right'}}>
              &times;
            </a>
        <p id="HomePassRestMsg">Password Rest Successfully.</p>
        <p id="HomePassRestMsg_2"> We have send a rest link on your email please check your email to rest your Password</p>
         </Popup> */}
      {/*---------------------------------- Desktop--------------------------------------- */}
<MediaQuery minWidth={992} >




      {/* // ####################StickyTop NAV ############################  */}
      <nav id="homeMenu" style={{ display:this.state.menuS }}>
        <Link style={{ textDecoration: 'none' }} to="/">
                  <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`}  style={{ height:'90%',cursor:'pointer',width:'10vw' }} onClick={this.props.makeSearchBlank}/>
            </Link>
            <Popup
          open={this.state.Cat}
          closeOnDocumentClick={false}
          onClose={this.CLOSECategory}
          // lockScroll={true}
          contentStyle={{ width:'80%',marginTop:'5.5%',height: '69%'}}
        >
          <div style={{ color:'black' }} >
            <a className="close" onClick={this.CLOSECategory} style={{marginTop:'-0.5%'}}>
              &times;
            </a>
           <Popupcat CloseCatPopup={this.CLOSECategory}/>
          </div>
        </Popup>

                  <div id="homeMenusearch" >
                  <button id="slidercategorybtn"
                   onClick={this.OPENCategory} 
                ><span id="categoryword">Categories </span><img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/categoriesicon.png`}  id="categoryimg"/></button>
        {/*####################StickyTop SEARCH BOX ############################ */}
                    <form onSubmit={SearchSubmit}>
                    <input className="search-txt" type="search"
                    name='searchBook' value={this.props.searchBook} 
                    onChange={this.onSearchChange} ref="searchBookFixed" autoFocus
                      style={{ position:'absolute',width:'24.8vw',marginTop:'-1.1%'}}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"/>
                     

                     {/* <input className="search-txt" type="search" 
                   name='searchBook' value={this.state.searchBook}
                   onChange={this.onSearchChange}
                    style={{ position:'absolute',width:'60%',marginTop:'-4%'}}
                   placeholder="Search Books, Author, Publisher, Title, ISBN #"/> */}
                    <input type="submit" style={{ display:'none' }}/>
                    <a id="homeMenuSearchBtn" href="#" disabled>
                    <i className="fas fa-search" disabled
                      style={{ position:'absolute',marginTop:'0%' }}
                      />
                    </a>  
                    </form>
                  </div>
                {/* <button> Donate Book</button> */}
                <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                 /> <span id="DonateBooksMH">Donate Book</span></button></Link>
                 {/* <div id ="Login" style={{color:'red'}} >Login</div> */}

                 <span id="loginSignup"><img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/loginsignUp.png`} /> 
                  { (this.props.userToken === null)?
                <a onClick={this.props.login_backdropToggle}>Log In/Sign Up</a>:<a onClick={this.sopenModal} style={{ fontSize:'1vw' }}> Hi, Reader
                </a>}
</span>
                  {/* <span id="loginSignup"><img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/loginsignUp.png`} /> 
                  { (this.props.userToken === null)?
                <a onClick={this.ToggleASignupOpenModal}>Log In/Sign Up
<Popup
          open={this.state.ASignupopen}
          // closeOnDocumentClick
          // onClose={this.ASignupCloseModal}
          closeOnDocumentClick={false}
          onClose={()=>this.setState({ ASignupopen: false})}
          contentStyle={{ 
                        width:'58.9%',
                        height:'70.6%',
                        borderRadius:'5px'
                        }} 
        > 
        {(this.state.showLS === true)?this.LOG_IN:this.SIGN_IN}
        </Popup>

</a>:<Popup trigger={<a onClick={this.sopenModal} style={{ fontSize:'60%' }}> Hi, Reader
</a>}
          on="hover"
          // open={this.state.ASignupopen}
          // closeOnDocumentClick
          // onClose={this.scloseModal}
          onClose={this.scloseModal}
          contentStyle={{ 
                        width:'15%',
                        // height:'30%',
                        borderRadius:'5px'
                        }}              
        > 
        <div style={{ color:'black',height:'30%'  }}>
        <div id="listonProfile"> {this.state.name}</div>
        <div id="listonProfile"><Link to='/customer/customer_account' > View Profile</Link></div>
        <div id="listonProfile"><Link to='/customer/customer_order' > Your Orders</Link></div>
        <div id="listonProfile"><Link to='/my-wish-list' > Your Wishlist</Link></div>
        <div id="listonProfile"><Link to='/mypustak-wallet' >Your Wallet</Link></div>
        <div id="listonProfile" onClick={this.Userlogout}> Logout</div>
      </div>
        </Popup>
}</span> */}
  {/*-------------------------- Popup for Category Popup-------------------------------------- */}

{/* <Popup
          trigger={ */}
          
          <span id="cart" onClick={this.props.CartopenModal}>

          <span>{(this.props.ItemsInCart <= 0)?null:<span id="DisplayCartNos">{this.props.ItemsInCart}</span>}</span>
          <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} />
          
          Cart </span>
          {/* }

          on='hover'
          // open={this.props.PopupCart}
          // on="hover"
          modal={false}
          lockScroll={true}
          // position="bottom center"
          onClose={this.props.CartcloseModal}
          onOpen={this.CartFadeOut}
          // closeOnDocumentClick
          // onClose={this.closeModal}
          overlayStyle={{ 
            backgroundColor:'transparent',
            // display:'none'
           }}
           offsetX='10px'
           disabled={(this.props.cartDetails.length === 0)?true:false}
          contentStyle={{ 
                      width:'25%',
                      
                      // left: '865.2px',
                      left:'68vw',
                      zIndex:'5',
                        }} 
        > */}
    {/* <div style={{ }} id={(this.props.ItemsInCart === 1)?"showCartOne":"showCart"}>
          

 {this.props.cartDetails.map(cart=>

<div id="BgCartPopupBody">
            <div id="BgCartBookData">
              <img src={`https://s3.ap-south-1.amazonaws.com/mypustak-4/uploads/books/medium/${cart.bookThumb}`}  style={{ float:'left',height:'75.75px',width:'53.56px' }}id="book"/>
              <div id="BgCartBookName">
                <span id="">{cart.bookName}</span>
              </div>
              <div id="BgCartBookCondPrice">
              <div id="BgCartBookprice">
                  <span >{cart.bookCond}<label id="newprice"><b> 
                      <span id="mrp">MRP:<span id="orgprice"> &#8377;<strike>{Math.round(cart.bookPrice ) }</strike></span>
                      </span>
                  <span style={{ color:'#e26127' }}>Free</span></b></label> </span>
              </div>          
            <span id="BgCartBookshipping">Shipping Handling Charge &#8377;{Math.round(cart.bookShippingCost)}
            <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/del.png`} id="" onClick={()=>this.RemoveFromCart(cart.bookInvId,cart.Cart_id)}></img></span>

          </div>
          </div>
          <hr id=""/>
          </div>
          )}
          {this.state.cartMsg && <span id="cartMsg">{this.state.cartMsg}</span>}
          
          </div>
          {/* <Link to="/view-cart"> 
          <button id="BgCartcheckoutBtn" onClick={()=>CheckLogin()} >Proceed To Checkout</button>

<Link to=""><button id="continueShopingCart">Continue Shopping > </button></Link>
</Popup>
        */}
                  
              </nav>
              </MediaQuery>

 {/* ------------------------------------tab---------------------------------------*/}
 <MediaQuery maxWidth={991} and minWidth={768}>

<div>
<div id="modiv">
  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>
{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}

{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>

<Link style={{ textDecoration: 'none' }} to="/">  
<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>

</Link>
 <span id="cart" >
 <span>{(this.props.ItemsInCart <= 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
  </span>
<img  src="https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png" id="cartimg" onClick={()=>MobileCheckLogin()}/>
{/* </Link> */}
</div>
</div>
 {/*#################### Mobile StickyTop SEARCH BOX ############################ */}
 <div id="homeMenusearch" >
 <Link style={{ textDecoration: 'none' }} to="/"> 
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mypustaknewlogo.png`} style={{width:'7vw',height:'7vw'}} onClick={this.props.makeSearchBlank}/>
 </Link>                  
                  <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                  style={{width:'3vw'}} />
                  <span id="DonateBooksMH">Donate Book</span></button></Link>
                    <input className="search-txt" type="search" 
                    name='searchBook' value={this.props.searchBook}
                    onChange={this.onSearchChange} ref="searchBookFixed"
                      style={{ position:'absolute',width:'73%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'18%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"
                      />

                    <a  href="#">
                    
                    <i className="fas fa-search"
                      style={{ position:'absolute',marginTop:'2.5%',marginLeft:'86%' }}
                      />
                    </a> 
                    </div>
        {/* <Popup
          // open={this.state.Sopen}
          open={this.state.ASignupopen}
          closeOnDocumentClick={false}
          // onClose={this.scloseModal}
          contentStyle={{ 
                        width:'58.9%',
                        height:'70.6%',
                        borderRadius:'5px'
                        }} 
        > 
        {(this.state.showLS === true)?this.LOG_IN:this.SIGN_IN}
        </Popup> */}
                
   {/* </Carousel>       */}
             </MediaQuery>
             {/* End of tab */}
             {/* larger mobile */}
             <MediaQuery maxWidth={767} and minWidth={539}>
             <div>
<div id="modiv">
  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>
{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}

{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>
        <Link style={{ textDecoration: 'none' }} to="/"> 
<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>

</Link>
 <span id="cart">
 <span>{(this.props.ItemsInCart <= 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
  </span>
  {/* <Link to={(localStorage.getItem('user') === null)?"/view-cart":"/view-cart"}> */}
<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/Headercart.png`} id="cartimg" onClick={()=>MobileCheckLogin()} />
{/* </Link> */}
</div>
</div>
 {/*#################### Mobile StickyTop SEARCH BOX ############################ */}
 <div id="homeMenusearch" >
 <Link style={{ textDecoration: 'none' }} to="/"> 
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mypustaknewlogo.png`} style={{width:'7vw',height:'7vw'}} onClick={this.props.makeSearchBlank}/>
 </Link>
 <Link style={{ textDecoration: 'none' }} to="/donate-books">
                  <button type="submit" id="donatebooks" >
                  <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/HeaderDonateBook.png`} id="ImgDonateBookMH"
                  style={{width:'3vw'}} />
                   <span id="DonateBooksMH">Donate Book</span></button></Link>
                    <input className="search-txt" type="search" 
                    name='searchBook' value={this.props.searchBook}
                    onChange={this.onSearchChange} ref="searchBookFixed"
                      // style={{ position:'absolute',width:'89%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'1%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      style={{ position:'absolute',width:'73%',marginTop:'0.3%',borderBottom:'none',paddingBottom:'2%',paddingLeft:'2%',marginLeft:'18%',backgroundColor:'white',borderRadius:'7px',height:'6vw'}}
                      placeholder="Search Books, Author, Publisher, Title, ISBN #"/>
                     
          

                    <a  href="#">
                    
                    <i className="fas fa-search"
                      style={{ position:'absolute',marginTop:'2.5%',marginLeft:'86%' }}
                      />
                    </a>  
                  </div>

</MediaQuery>
{/* End of larger mobile */}

{/* smaller mobiles */}
<MediaQuery maxWidth={538} >
<div id="headerdiv">

             <div>
<div id="modiv">
<img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/homeicon.png`} id="homeicon" onClick={()=>this.openNav()}></img>
<div id={this.state.CloseNav?"mySidenav":"mySidenavNone"} className="sidenav">
<a href="javascript:void(0)" class="closebtn" onClick={()=>this.closeNav()}>&times;</a>
{(localStorage.getItem('user') === null)?<Link to="/login" onClick={()=>this.closeNav()}>Log In/Sign Up</Link>
:<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>
}

{/* <Link to="/signup">Sign Up</Link> */}
<Link to="/category/competitive-exams/" onClick={()=>{this.closeNav();SetCategory('/category/competitive-exams/')}}>Competitive Exams</Link>
<Link to="/category/fiction-non-fiction/" onClick={()=>{this.closeNav();SetCategory('/category/fiction-non-fiction/')}}>Fiction & Non-Fiction</Link>
{/* <Link to="/category/note-book-/" onClick={()=>{this.closeNav();SetCategory('/category/note-book-/')}}>Note Book</Link> */}
<Link to="/category/school-children-books/" onClick={()=>{this.closeNav();SetCategory('/category/school-children-books/')}}>School & Children Books</Link>
<Link to="/category/university-books/" onClick={()=>{this.closeNav();SetCategory('/category/university-books/')}}>University Books</Link>
{(localStorage.getItem('user') !== null)?<Link to='/mypustak-wallet' onClick={()=>this.closeNav()}>Wallet</Link>:null}
{/* // {(localStorage.getItem('user') !== null)?<Link to='/customer/customer_account' onClick={()=>this.closeNav()}>Profile</Link>:null} */}
{(localStorage.getItem('user') !== null)?<Link to='/my-wish-list' onClick={()=>this.closeNav()}>Wishlist</Link>:null}
{(localStorage.getItem('user') !== null)?<Link to='/customer/customer_order' onClick={()=>this.closeNav()}>Orders</Link>:null}
<Link to="/contact-us" onClick={()=>this.closeNav()}>Contact Us</Link>
<Link to="/faq" onClick={()=>this.closeNav()}>FAQ</Link>
{(localStorage.getItem('user') !== null)?<Link to="/" onClick={this.Userlogout}>Logout</Link>:null}
</div>
<Link style={{ textDecoration: 'none' }} to="/"> 
<img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/MyPustakLogo..png`} id="logoimg" onClick={this.props.makeSearchBlank}/>

</Link>
 <span id="cart">
 <span>{(this.props.ItemsInCart <= 0)?null:<span id="MoDisplayCartNos">{this.props.ItemsInCart}</span>} </span>
  </span>
<i class="fas fa-shopping-cart" id="cartimg" onClick={()=>MobileCheckLogin()}></i>
</div>
</div>
 {/*#################### Mobile StickyTop SEARCH BOX ############################ */}
 <div id="homeMenusearch" >
 {/* <Link style={{ textDecoration: 'none' }} to="/"> 
 <img  src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/mypustaknewlogo.png`} style={{width:'7vw',height:'7vw'}} onClick={this.props.makeSearchBlank}/>
 </Link> */}
 <div id="homesearchmaindiv">
  <InputGroup className="mb-3">
    <InputGroup.Prepend>
      <InputGroup.Text style={{height:'8vw'}}> 
      <i class="fa fa-search" style={{fontSize:'3vw'}}></i>
                    </InputGroup.Text>
    </InputGroup.Prepend>
    <FormControl style={{height:'8vw',border:'0px'}}
    value={this.props.searchBook}
    onChange={this.onSearchChange} ref="searchBookFixed"
      placeholder="Search Books, Author, Publisher, Title, ISBN #"
    />
     <InputGroup.Append>
      <Button id="donatebook"><Link style={{ textDecoration: 'none', marginLeft:'-2px',paddingLeft:'0px', color:'white'}} to="/donate-books">  
                  <b >Donate Book</b></Link></Button>
    </InputGroup.Append>
  </InputGroup>
  </div> 
                  </div>
                  </div>

</MediaQuery>
{/* End of smaller mobile */}
              <Login toggle={this.props.LoginBackdrop}/>
              </div>
    )
  }
}
const mapStateToProps = state => ({
    searchBook:state.homeR.searchBook,
    userToken:state.accountR.token,
    ErrMsg:state.accountR.ErrMsg,
    cartDetails:state.cartReduc.MyCart,
    PopupCart:state.cartReduc.PopupCart,
    ItemsInCart:state.cartReduc.cartLength,
    getuserdetails:state.userdetailsR.getuserdetails,
    getadd:state.getAddressR.getadd,
    editadd:state.accountR.editadd,
    ClickedCategory:state.homeR.ClickedCategory,

    SuccessSentRestMail:state.accountR.ActivateSuccPopupOtherState,
    CartSessionData:state.cartReduc.CartSessionData,
    ToBeAddedToCartState:state.cartReduc.ToBeAddedToCartState,
    cartRedirectMobile:state.cartReduc.cartRedirectMobile,
    LoginBackdrop:state.accountR.LoginBackdrop


  })
  export default connect(mapStateToProps,{navurl,setCategory,CartopenModal,CartcloseModal,doSearch,logout,makeSearchBlank,CartSession,ToBeAddedToCart,RemoveToBeAddedToCart,removeFromCartLogout,MobileCartRedirect,
    AddToCart,removeAllCart,getSavedToken,RemoveCart,Getaddress,userdetails,Editaddress,addAddressAction,ActivteSuccesPopupOther,AfterLoginRedirect,login_backdropToggle})(MainHeader)