import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableFooter from "@material-ui/core/TableFooter";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import IconButton from "@material-ui/core/IconButton";
import Tooltip from "@material-ui/core/Tooltip";
import FilterListIcon from "@material-ui/icons/FilterList";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import { lighten } from "@material-ui/core/styles/colorManipulator";
import Divider from "@material-ui/core/Divider";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import CircularProgress from "@material-ui/core/CircularProgress";
import moment from "moment";
import { connect } from "react-redux";

import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import Search from "@material-ui/icons/Search";
import Clear from "@material-ui/icons/Clear";
import Add from "@material-ui/icons/Add";

import axios from "axios";

import {
  getDonationsList,
  updateDonation,
  getDonationById,
  updateDonationById,
  addDonationTrackingId
} from "../../actions/BackenddonationActions";
import { getDonationTracking } from "../../actions/backofficedonationtrackinggetAction";

import DonationHead from "./DonationHead";

import { desc, stableSort, getSorting } from "../../util/helpers";

// STATUSES for donation
const STATUSES = [
  { id: 1, name: "Pending", color: "pink" },
  { id: 2, name: "In Queue", color: "orange" },
  { id: 3, name: "Process", color: "blue" },
  { id: 4, name: "In Shipping", color: "violate" },
  { id: 5, name: "Dispatched or Delivered", color: "green" },
  // { id: 6, name: "Not Shipping", color: "gray" },
  { id: 8, name: "Not Shipping", color: "gray" }
];

const styles = theme => ({
  root: {
    width: "100%",
    marginTop: theme.spacing.unit * 3
  },
  table: {
    minWidth: 1020
  },
  tablecell: {
    fontSize: "11pt",
    padding: "4px"
  },
  tableWrapper: {
    overflowX: "auto"
  },
  button: {
    whiteSpace: "nowrap",
    marginTop: "4px",
    marginLeft: "6px"
  },
  margin: {
    margin: "0 4px"
  }
});

class Donation extends React.Component {
  state = {
    order: "desc",
    orderBy: "donation_req_id",
    selected: [],
    donationList: [],
    page: 0,
    rowsPerPage: 10,

    openEditDonation: false,
    openAssignedTrackingNo: false,

    token: "",
    donation_req_id:""
  };

  Tracking_No = {
    tracking_no: []
  };

  onChangeTrackingValue = e => {
    this.Tracking_No.tracking_no[0] = e.target.value;
  };

  componentDidMount() {
    this.setState({ token: localStorage.getItem("user") });
    if (!this.props.donationList.length) {
      this.props.getDonationsList({
        page: this.state.page,
        token: localStorage.getItem("user")
      });
    } else {
      this.setState({
        donationList: this.props.donationList
      });
    }
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.donationList !== nextProps.donationList) {
      let { donationList } = nextProps;
      this.setState({
        donationList: donationList
      });
    }
  }

  // create data is a function which will create data for table row,
  // in this file we use this, you can check below.
  // In this we are creating data as per table header
  createData = ({
    donation_req_id,
    donor_name,
    no_of_book,
    status,
    i_date,
    app_books_weight
  }) => {
    let statusObj = STATUSES.find(statusobj => statusobj.id == status);
    return {
      id: donation_req_id,
      donation_req_id,
      donor_name,
      no_of_book,
      status: statusObj.name,
      i_date,
      app_books_weight,
      statusObj,
      details: (
        <div
          style={{ color: "blue" }}
          onClick={() => this.handleClickEditDonation(donation_req_id)}
        >
          {" "}
          Details{" "}
        </div>
      ),
      action: (
        <div>
          {" "}
          <Add
            fontSize="small"
            onClick={() => this.handleClickOnAssignTrackingNo(donation_req_id)}
          />{" "}
        </div>
      )
    };
  };

  // it will handle sorting request
  handleRequestSort = (event, property) => {
    const orderBy = property;
    let order = "desc";

    if (this.state.orderBy === property && this.state.order === "desc") {
      order = "asc";
    }

    this.setState({ order, orderBy });
  };

  // it is for handle select all click, right now we're not using but if we need in future so it's there.
  handleSelectAllClick = event => {
    if (event.target.checked) {
      this.setState(state => ({ selected: state.donationList.map(n => n.id) }));
      return;
    }
    this.setState({ selected: [] });
  };

  // handle click on Checkbox and select accordingly
  // its for signlular selection - you can only select one row at a time
  handleClick = (event, id) => {
    const { selected } = this.state;
    const selectedIndex = selected.indexOf(id);

    let newSelected = [];
    if (selectedIndex === -1) {
      newSelected = [id];
    } else if (selectedIndex === 0) {
      newSelected = [];
    }
    this.setState({ selected: newSelected });
  };

  // handle request for page change
  handleChangePage = (event, page) => {
    this.setState({ page });
  };

  isSelected = id => this.state.selected.indexOf(id) !== -1;

  // handle pagination using type
  handlePaging = type => {
    if (type == "right") {
      if (
        this.props.donationList.length / this.state.rowsPerPage ==
        this.state.page + 1
      ) {
        this.props.getDonationsList({
          page: this.state.page + 1,
          token: this.state.token
        });
      }
      this.setState({ page: this.state.page + 1 });
    } else {
      this.setState({ page: this.state.page - 1 });
    }
  };

  // below funciton will call when user click on any of buttons of statuses for donation - STATUSES buttons
  handleClickOnStatusButton = new_status => {
    this.props.updateDonationById({
      donation_req_id: this.state.selected[0],
      new_status,
      token: this.state.token
    });
  };

  updateDonation = e => {
    e.preventDefault();
    let donation = {
      user_id: e.target.user_id.value,
      donation_req_id: e.target.donation_req_id.value,
      status: e.target.status.value,
      mobile: e.target.mobile.value,
      donated_book_category: e.target.donated_book_category.value,
      donor_name: e.target.donor_name.value,
      city: e.target.city.value,
      app_books_weight: e.target.app_books_weight.value,
      no_of_book: e.target.no_of_book.value,
      address: e.target.address.value,
      landmark: e.target.landmark.value,
      country: e.target.country.value,
      state: e.target.state.value,
      zipcode: e.target.zipcode.value,
      no_of_cartons: e.target.no_of_cartons.value,
      wastage: e.target.wastage.value,
      pickup_date_time: e.target.pickup_date_time.value,
      payment_id: e.target.payment_id.value,
      is_paid_donation: e.target.is_paid_donation.value,
      document_mail_sent: e.target.document_mail_sent.value
    };
    this.props.updateDonation({ donation, token: this.state.token });
    this.handleClose();
  };

  handleClose = () => {
    this.setState({
      openEditDonation: false,
      openAssignedTrackingNo: false
    });
  };

  handleClickEditDonation(donation_req_id) {
    this.props.getDonationById({ donation_req_id, token: this.state.token });
    this.setState({
      openEditDonation: true
    });
  }

  setValue = e => {
    this.setState({ searchValue: e.target.value });
  };

  handleChangeSearch = () => {
    // console.log(e.target.value, data);
    const searchValue = this.state.searchValue;
    if (searchValue) {
      this.setState({ page: 0 });
      this.props.getDonationsList({
        page: this.state.page,
        token: this.state.token,
        search: true,
        searchValue
      });
    } else {
      this.setState({ page: 0 });
      this.props.getDonationsList({
        page: this.state.page,
        token: this.state.token,
        search: true
      });
    }
  };

  clearSearch = () => {
    this.setState({ searchValue: "" }, () => this.handleChangeSearch());
  };

  handleClickOnAssignTrackingNo = donation_req_id => {
    console.log(donation_req_id,"donationREQ");
    
    this.setState({ openAssignedTrackingNo: true ,donation_req_id:donation_req_id});
    this.props.getDonationTracking(
      localStorage.getItem("user"),
      donation_req_id
    );
  };

  updateTrackingNo = e => {
    e.preventDefault();
    console.log(this.state.donation_req_id,"In UpdateTrackingNo", e.target.assign_tracking_no.value);

    this.props.addDonationTrackingId(
      this.state.donation_req_id,
      e.target.assign_tracking_no.value,
      this.state.token
    );
    // this

    this.handleClose();
  };

  // below is for getting destrict and state from pincode
  // below function will call the api and get the information
  pincodeChange = e => {
    if (e.target.value) {
      axios
        .get(`http://data.mypustak.com/pincode/pin_check/${e.target.value}/`)
        .then(res => {
          console.log(res);
          if (res.data.length) {
            const { districtname, statename } = res.data[0];
            this.setState({
              districtname,
              statename
            });
          } else {
            this.setState({
              districtname: "NA",
              statename: "NA"
            });
          }
        })
        .catch(err => console.log(err));
    }
  };

  render() {
    try {
      for (let i = 0; i < this.props.donationTracking.tracking_no.length; i++)
        this.Tracking_No.tracking_no[
          i
        ] = this.props.donationTracking.tracking_no[i].tracking_no;
    } catch (e) {
      console.log("Not yet");
    }
    // console.log("Donation id is ", DonationTrackingId);
    const { classes, currentDonation, loading } = this.props;
    let {
      donationList,
      order,
      orderBy,
      selected,
      rowsPerPage,
      page
    } = this.state;
    const emptyRows =
      rowsPerPage -
      Math.min(rowsPerPage, donationList.length - page * rowsPerPage);

    donationList = donationList.map(donation => {
      return this.createData(donation);
    });

    let status = STATUSES.find(status => status.id === currentDonation.status);

    return (
      <div>
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            paddingBottom: "16px",
            flexWrap: "wrap",
            alignItems: "flex-end"
          }}
        >
          <div
            style={{ display: "flex", paddingBottom: "16px", flexGrow: "8" }}
          >
            <TextField
              id="search"
              className={classNames(classes.margin, classes.textField)}
              variant="outlined"
              type={"text"}
              label="Search"
              placeholder="Seach by Donor Name/Donation Request ID/ Email ID/ Mobile Number"
              // value={this.state.search}
              onChange={this.setValue}
              InputLabelProps={{ shrink: true }}
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton aria-label="Search" onClick={this.clearSearch}>
                      <Clear />
                    </IconButton>
                    <IconButton
                      aria-label="Search"
                      onClick={this.handleChangeSearch}
                    >
                      <Search />
                    </IconButton>
                  </InputAdornment>
                )
              }}
              fullWidth
            />
          </div>
          <div
            style={{
              display: "flex",
              justifyContent: "flex-end",
              paddingBottom: "16px",
              flexWrap: "wrap"
            }}
          >
            {STATUSES.map((sb, i) => {
              return (
                <Button
                  variant="contained"
                  size="small"
                  className={classes.button}
                  style={{ backgroundColor: sb.color }}
                  key={i}
                  onClick={() => this.handleClickOnStatusButton(sb.id)}
                  disabled={!this.state.selected.length || this.props.loading}
                >
                  {sb.name}
                </Button>
              );
            })}
          </div>
        </div>
        <Divider />
        <Paper className={classes.root}>
          <div
            style={{
              padding: "12px",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <IconButton
              aria-label="Delete"
              disabled={this.state.page === 0}
              className={classes.margin}
              onClick={() => this.handlePaging("left")}
            >
              <KeyboardArrowLeft fontSize="small" />
            </IconButton>
            <IconButton
              aria-label="Delete"
              className={classes.margin}
              onClick={() => this.handlePaging("right")}
              disabled={this.props.loading}
            >
              <KeyboardArrowRight fontSize="small" />
            </IconButton>
            Page: {this.state.page}
          </div>
          <div className={classes.tableWrapper}>
            <Table className={classes.table} aria-labelledby="tableTitle">
              <DonationHead
                numSelected={selected.length}
                order={order}
                orderBy={orderBy}
                onSelectAllClick={this.handleSelectAllClick}
                onRequestSort={this.handleRequestSort}
                rowCount={donationList.length}
              />
              <TableBody>
                {loading ? (
                  <tr style={{ textAlign: "center" }}>
                    {" "}
                    <td colSpan={9}> Loading... </td>{" "}
                  </tr>
                ) : donationList.length ? (
                  stableSort(donationList, getSorting(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map(n => {
                      const isSelected = this.isSelected(n.id);
                      {
                        /* Here below we setting row data and divs conditionally */
                      }
                      return (
                        <TableRow
                          hover
                          onClick={event => this.handleClick(event, n.id)}
                          role="checkbox"
                          aria-checked={isSelected}
                          tabIndex={-1}
                          key={n.id}
                          selected={isSelected}
                        >
                          <TableCell padding="checkbox">
                            <Checkbox
                              checked={isSelected}
                              inputProps={{ style: { display: "none" } }}
                            />
                          </TableCell>
                          <TableCell
                            className={classes.tablecell}
                            component="th"
                            scope="row"
                            padding="none"
                          >
                            {n.donation_req_id}
                          </TableCell>
                          <TableCell className={classes.tablecell}>
                            {n.donor_name}
                          </TableCell>
                          <TableCell className={classes.tablecell}>
                            {n.no_of_book}
                          </TableCell>
                          <TableCell className={classes.tablecell}>
                            <div
                              style={{
                                color: n.statusObj.color,
                                minWidth: "180px"
                              }}
                            >
                              {" "}
                              {n.status}{" "}
                            </div>
                          </TableCell>
                          <TableCell className={classes.tablecell}>
                            <div style={{ minWidth: "90px" }}>
                              {" "}
                              {moment.unix(n.i_date).format("DD-MM-YYYY")}{" "}
                            </div>
                          </TableCell>
                          <TableCell className={classes.tablecell}>
                            {n.app_books_weight}
                          </TableCell>
                          <TableCell>{n.details}</TableCell>
                          <TableCell>{n.action}</TableCell>
                        </TableRow>
                      );
                    })
                ) : (
                  <tr style={{ textAlign: "center" }}>
                    {" "}
                    <td colSpan={9}> There isn't any donation. </td>{" "}
                  </tr>
                )}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 49 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </TableBody>
            </Table>
          </div>
        </Paper>

        {/* Dialog box for address info */}
        <Dialog
          open={this.state.openEditDonation}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          scroll={"body"}
          size="md"
        >
          <DialogTitle id="form-dialog-title"> {`Donation Page`} </DialogTitle>
          <form onSubmit={this.updateDonation}>
            <DialogContent>
              {this.props.loading ? (
                <div style={{ display: "flex", justifyContent: "center" }}>
                  {" "}
                  <CircularProgress className={classes.progress} />{" "}
                </div>
              ) : (
                <React.Fragment>
                  <TextField
                    autoFocus
                    name="user_id"
                    margin="dense"
                    id="user_id"
                    label="Donor's userId"
                    type="text"
                    defaultValue={currentDonation.user_id}
                    disabled
                    fullWidth
                  />
                  <TextField
                    name="donation_req_id"
                    margin="dense"
                    id="donation_req_id"
                    label="Donation Req Id"
                    type="text"
                    defaultValue={currentDonation.donation_req_id}
                    disabled
                    fullWidth
                  />

                  <TextField
                    name="email_id"
                    margin="dense"
                    id="email_id"
                    label="Email Id"
                    type=""
                    defaultValue={currentDonation.email_id}
                    // disabled
                    fullWidth
                  />

                  <TextField
                    name="status"
                    margin="dense"
                    id="status"
                    label="Status"
                    type="text"
                    defaultValue={status && status.name}
                    disabled
                    fullWidth
                  />
                  <TextField
                    name="mobile"
                    margin="dense"
                    id="mobile"
                    label="Mobile"
                    type="tel"
                    defaultValue={currentDonation.mobile}
                    inputProps={{
                      maxLength: 10
                    }}
                    fullWidth
                  />
                  <TextField
                    name="donated_book_category"
                    margin="dense"
                    id="donated_book_category"
                    label="Donated Book Category"
                    type="text"
                    defaultValue={currentDonation.donated_book_category}
                    disabled
                    fullWidth
                  />
                  <TextField
                    name="donor_name"
                    margin="dense"
                    id="donor_name"
                    label="Donor Name"
                    type="text"
                    defaultValue={currentDonation.donor_name}
                    fullWidth
                  />
                  <TextField
                    name="address"
                    margin="dense"
                    id="address"
                    label="Address"
                    type="text"
                    defaultValue={currentDonation.address}
                    fullWidth
                  />
                  <TextField
                    name="landmark"
                    margin="dense"
                    id="landmark"
                    label="Landmark"
                    type="text"
                    defaultValue={currentDonation.landmark}
                    fullWidth
                  />
                  <TextField
                    name="zipcode"
                    margin="dense"
                    id="zipcode"
                    label="Zipcode"
                    type="tel"
                    defaultValue={currentDonation.zipcode}
                    onChange={this.pincodeChange}
                    inputProps={{
                      maxLength: 6
                    }}
                    fullWidth
                  />
                  <TextField
                    name="city"
                    margin="dense"
                    id="city"
                    label="City"
                    type="text"
                    value={this.state.districtname || currentDonation.city}
                    fullWidth
                  />
                  <TextField
                    name="state"
                    margin="dense"
                    id="state"
                    label="State"
                    type="text"
                    value={this.state.statename || currentDonation.state}
                    fullWidth
                  />
                  <TextField
                    name="country"
                    margin="dense"
                    id="country"
                    label="Country"
                    type="text"
                    defaultValue={currentDonation.country}
                    fullWidth
                  />

                  <TextField
                    name="app_books_weight"
                    margin="dense"
                    id="app_books_weight"
                    label="Approximate Books Weight (in KG )"
                    type="number"
                    inputProps={{
                      min: 0
                    }}
                    defaultValue={currentDonation.app_books_weight}
                    fullWidth
                  />
                  <TextField
                    name="no_of_book"
                    margin="dense"
                    id="no_of_book"
                    label="No Of Book"
                    type="number"
                    inputProps={{
                      min: 0
                    }}
                    defaultValue={currentDonation.no_of_book}
                    fullWidth
                  />

                  <TextField
                    name="no_of_cartons"
                    margin="dense"
                    id="no_of_cartons"
                    label="No Of Cartons"
                    type="nubmer"
                    defaultValue={currentDonation.no_of_cartons}
                    fullWidth
                  />
                  <TextField
                    name="wastage"
                    margin="dense"
                    id="wastage"
                    label="Wastage"
                    type="number"
                    defaultValue={currentDonation.wastage}
                    fullWidth
                  />
                  <TextField
                    name="pickup_date_time"
                    margin="dense"
                    id="pickup_date_time"
                    label="Pickup Date Time"
                    type="date"
                    defaultValue={moment(
                      currentDonation.pickup_date_time
                    ).format("YYYY-MM-DD")}
                    fullWidth
                  />
                  <TextField
                    name="payment_id"
                    margin="dense"
                    id="payment_id"
                    label="Payment Id"
                    type="text"
                    defaultValue={currentDonation.payment_id}
                    disabled
                    fullWidth
                  />
                  <TextField
                    name="is_paid_donation"
                    margin="dense"
                    id="is_paid_donation"
                    label="Is Paid Donation"
                    type="text"
                    defaultValue={currentDonation.is_paid_donation}
                    disabled
                    fullWidth
                  />
                  <TextField
                    name="document_mail_sent"
                    margin="dense"
                    id="document_mail_sent"
                    label="Document Mail Sent"
                    type="text"
                    defaultValue={currentDonation.document_mail_sent}
                    disabled
                    fullWidth
                  />
                </React.Fragment>
              )}
            </DialogContent>
            {!this.props.loading ? (
              <DialogActions>
                <Button onClick={this.handleClose} color="default">
                  Cancel
                </Button>
                <Button variant="outlined" type="reset">
                  Reset
                </Button>
                <Button variant="contained" type="submit" color="primary">
                  Update
                </Button>
              </DialogActions>
            ) : (
              ""
            )}
          </form>
        </Dialog>

        <Dialog
          open={this.state.openAssignedTrackingNo}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
          size="md"
        >
          <form onSubmit={this.updateTrackingNo}>
            <DialogTitle id="form-dialog-title">
              Assign Tracking No - #{this.state.donation_req_id}
            </DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                name="assign_tracking_no"
                id="assign_tracking_no"
                label="Enter tracking number"
                type="text"
                value={this.Tracking_No.tracking_no}
                onChange={this.onChangeTrackingValue}
                fullWidth
              />
            </DialogContent>
            <DialogActions>
              <Button onClick={this.handleClose} color="primary">
                Cancel
              </Button>
              <Button type="submit" color="primary">
                Save
              </Button>
            </DialogActions>
          </form>
        </Dialog>
      </div>
    );
  }
}

Donation.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = state => {
  const { donationList, loading, currentDonation } = state.donationBR;
  const donationTracking = state.donationTracking.donationTracking;
  // console.log(
  //   "Donation id in mapstatetoprops ",
  //   donationTracking,
  //   state.donationTracking.donationTracking
  // );
  return { donationList, loading, currentDonation, donationTracking };
};

export default connect(
  mapStateToProps,
  {
    getDonationsList,
    updateDonation,
    getDonationById,
    updateDonationById,
    getDonationTracking,
    addDonationTrackingId
  }
)(withStyles(styles)(Donation));
