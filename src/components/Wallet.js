import React, { Component } from 'react';
import './wallet.css';
import MainHeader from './MainHeader';
import MainFooter from './MainFooter';
import {Getwalletd,WalletRecarge,SetWallet} from '../actions/walletAction'
import { connect } from 'react-redux';
import {Get_Rp_Id} from '../actions/donationActions'
import Popup from 'reactjs-popup'
import ProceedToPay from './ProceedToPay';
import WalletProceedToPay from '../components/WalletProceedToPay';
import Loader from 'react-loader-spinner'
import axios from 'axios';
import { Link,Redirect,browserHistory } from 'react-router-dom'
import { timeout } from 'q';
import MediaQuery from 'react-responsive';

class Wallet extends Component {
  state={
    output:'',
    walletAmt:"",
    OpenWalletPayBtn:false,
    COUNT:1,
    WalletLoader:true,
    WalletErr:'',
}
 componentDidMount() {
  if(localStorage.getItem('user') === null){
    // alert("RED")
    window.location.href="/"
  }else{
    window.scrollTo(0, 0);
    const details=`Token ${localStorage.getItem('user')}`
    this.props.SetWallet(details)
    this.props.Getwalletd(details)

    }
 }

 componentWillReceiveProps(nextProps){
  if(nextProps.userToken !== this.props.userToken ){
    const details=`Token ${nextProps.userToken}`
    this.props.Getwalletd(details)
  }
  if(this.props.walletbalance !== nextProps.walletbalance){
    this.setState({walletAmt:""})
  }
}

//  onChange = e => this.setState({ [e.target.name]: e.target.value });
  render() {
    var WalletOrderId=`WALLETORDERID_${this.state.walletAmt}`
    const WalletSubmit=(e)=>{
      e.preventDefault();
      if(this.state.WalletErr ==="" && this.state.walletAmt.length !== 0){
        this.setState({OpenWalletPayBtn:true})
      const SendDataRzpzy={
        data:{
         "ref_id" : WalletOrderId,
         "amount" : Math.round(this.state.walletAmt)*100
        }
      }
      const WalletData={
        WalletOrderId,
        RechargeAmout:Math.round(this.state.walletAmt)*100
      }
      this.props.Get_Rp_Id(SendDataRzpzy)
      this.props.WalletRecarge(WalletData)
      this.setState({COUNT:2})}
      this.setState({walletAmt:""})
    }
    if(this.props.RAZORPAY !==0 && this.state.COUNT === 2 ){
     
      this.setState({COUNT:3})
      this.setState({WalletLoader:false})
    }
    // console.log(this.props.walletbalance.length);
    // let WalletErr="test"
    // if(isNaN(this.state.walletAmt)){
    //   // WalletErr = <span style={{ color:'red',marginLeft:'45%',fontSize:'90%'}}>Enter digits</span>
    // }else{
    //     // WalletErr=""
    //   }
      const onChange=(e)=>{
        // console.log(e.target.value.length);
        
        this.setState({walletAmt: e.target.value})
        if(isNaN(e.target.value)){
          this.setState({WalletErr:"Enter digits"})
          // setTimeout(()=>{this.setState({WalletErr:"Enter digits"})},2000)
            //  setTimeout(()=>{this.setState({WalletErr:""})},2000)

        }
        else if(e.target.value.length === 0){
          // alert("0")
          this.setState({WalletErr:"Enter Amount"})
            //  setTimeout(()=>{this.setState({WalletErr:""})},2000)

        }
        else if(e.target.value <= 0){
          // alert("0")
          this.setState({WalletErr:"Enter Amount more than 0"})
        }

        // else if(this.state.walletAmt < 0){
        //   this.setState({WalletErr:"Enter "})
        // }
        else{ 
          // alert("nn")

            // WalletErr=""
            this.setState({WalletErr:""})
          }
      }
    return (
      <div >
        <MainHeader/>
        <div id="walletBody">
        {/* {this.state.walletAmt} */}
          <div id="showBalance">
          
            
            <img src={`https://mypustak-5.s3.ap-south-1.amazonaws.com/uploads/icons/walleticon.9850212f.png`} id="walletimg"/>

            <label id="money">&#8377;{(this.props.walletbalance.length === 0)?"0":this.props.walletbalance}</label>
            <label id="wallet-balance">Your Wallet Balance</label>
          </div>
          <form id="walletForm" onSubmit={WalletSubmit}>
          <input type="text" placeholder="Enter Amount To be added in wallet" id="addToWallet" 
          // onChange={e => this.setState({walletAmt: e.target.value})} maxLength="10" required></input>
          // onChange={onChange} maxLength="10" required></input>
          value={this.state.walletAmt}
          onChange={onChange} maxLength="10" required></input>


        
          <input type="submit" id="Walletbtn" value="Add Money To Wallet" />
          <span id="WalletErr">{this.state.WalletErr}</span>
          </form>

                  <Link to='/wallet/passbook/'><button id="passbook">My Passbook</button></Link>
      {/*---------------------------------- Desktop--------------------------------------- */}

                <MediaQuery minWidth={992} >          
                  <Popup
                      open={this.state.OpenWalletPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false})}}
                      closeOnDocumentClick={false}
                      contentStyle={{ height:'15%',borderRadius:'3px',width:'16%'}}
                      > 
                       <p>
                     <a className="close" onClick={()=>{this.setState({OpenWalletPayBtn:false})}} style={{
                        color: 'red',zIndex:'999',opacity:'1',fontWeight:'normal',
                        float:'right',marginTop:'-6%',marginRight:'-3%'}}>
                          &times;
                      </a>
                      </p>
                     <div>
                      
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.WalletLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      
                      {(this.props.RAZORPAY !==0)?
                     <WalletProceedToPay closePopup={()=>{this.setState({OpenWalletPayBtn:false})}}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
      {/*----------------------------------End Desktop--------------------------------------- */}
 {/* ------------------------------------tab---------------------------------------*/}

      <MediaQuery maxWidth={991} and minWidth={768}>         
                  <Popup
                      open={this.state.OpenWalletPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false})}}
                      closeOnDocumentClick={false}
                      contentStyle={{ height:'15%',borderRadius:'3px',width:"20%"}}
                      > 
                       <p>
                     <a className="close" onClick={()=>{this.setState({OpenWalletPayBtn:false})}} style={{
                        color: 'red',zIndex:'999',opacity:'1',fontWeight:'normal',
                        float:'right',marginTop:'-6%',marginRight:'-3%'}}>
                          &times;
                      </a>
                      </p>
                     <div>
                      
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.WalletLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      
                      {(this.props.RAZORPAY !==0)?
                     <WalletProceedToPay closePopup={()=>{this.setState({OpenWalletPayBtn:false})}}/>:null}
                     </div>
                  </Popup>
                  </MediaQuery>
 {/* ------------------------------------End tab---------------------------------------*/}
 {/* -----------------------------------larger mobile---------------------------------------*/}

 <MediaQuery maxWidth={767} and minWidth={539}>
 <Popup
                      open={this.state.OpenWalletPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false})}}
                      closeOnDocumentClick={false}
                      contentStyle={{ height:'15%',borderRadius:'3px',width:"25%"}}
                      > 
                       <p>
                     <a className="close" onClick={()=>{this.setState({OpenWalletPayBtn:false})}} style={{
                        color: 'red',zIndex:'999',opacity:'1',fontWeight:'normal',
                        float:'right',marginTop:'-6%',marginRight:'-3%'}}>
                          &times;
                      </a>
                      </p>
                     <div>
                      
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.WalletLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      
                      {(this.props.RAZORPAY !==0)?
                     <WalletProceedToPay closePopup={()=>{this.setState({OpenWalletPayBtn:false})}}/>:null}
                     </div>
                  </Popup>
</MediaQuery>
{/* End of larger mobile */}

{/* smaller mobiles */}
<MediaQuery maxWidth={538} >

<Popup
                      open={this.state.OpenWalletPayBtn}
                      // contentStyle={{ margin:'unset',marginLeft:'67%',float:'right',marginTop:'4%' ,height:'95%'}}
                      onClose={()=>{this.setState({OpenWalletPayBtn:false})}}
                      closeOnDocumentClick={false}
                      contentStyle={{ height:'15%',borderRadius:'3px',width:"45%"}}
                      > 
                       <p>
                     <a className="close" onClick={()=>{this.setState({OpenWalletPayBtn:false})}} style={{
                        color: 'red',zIndex:'999',opacity:'1',fontWeight:'normal',
                        float:'right',marginTop:'-6%',marginRight:'-3%'}}>
                          &times;
                      </a>
                      </p>
                     <div>
                      
                     <div style={{ position:'absolute',marginLeft:'17%',marginTop:'10%' }}>
                        {this.state.WalletLoader?
                                  <Loader 
                                  type="CradleLoader"
                                  color="#00BFFF"
                                  height="100"	
                                  width="100"
                                  style={{ zIndex:'999' }}
                                />  :null 
                        }</div>
                      
                      {(this.props.RAZORPAY !==0)?
                     <WalletProceedToPay closePopup={()=>{this.setState({OpenWalletPayBtn:false})}}/>:null}
                     </div>
                  </Popup>
</MediaQuery>
        </div>
        <MainFooter/>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  walletbalance:state.walletR.walletbalance,
  userToken:state.accountR.token,
  RAZORPAY:state.donationR.rp_id,

})
export default connect(mapStateToProps,{Getwalletd,Get_Rp_Id,WalletRecarge,SetWallet})(Wallet);
