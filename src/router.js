import React,{ Suspense, lazy } from 'react';
import { Route, Switch, Redirect } from 'react-router';
import './App.css';
import Home from './components/Home/Home';
import Form from './components/HomePage/form/Form'
import AboutUs from './components/Home/AboutUs';
import DonationPickup from './components/DonationForm/form/DonationPickup'
import Donationform from './components/DonationForm/form/Donationform'
import Cart from './components/Cart/Cart'
import Product from './components/product/Product'
import Search from './components/Home/Search';
import Contact from './components/Contact'
import ProudDonor from './components/HomePage/form/ProudDonar';
import Wallet from './components/Wallet';
import Wishlist from './components/product/Wishlist'
import GetBooks from './components/Home/GetBooks';
import ChangeAddress from './components/Home/ChangeAddress'
import Orders from './components/Home/Order'
import Category from './components/Home/Category'
import Faq from './components/Home/faq'
import TermsCondtion from './components/Home/termsandcondition'
import PrivacyPolicy from './components/Home/privacypolicy'
import BookGuidlines from './components/Home/bookguidlines'
import DonarAccDonarReq from './components/Home/DonarAccDonarReq'
import CartThanks from './components/Cart/CartThanks';
import DonationThanks from './components/DonationForm/layout/DonationThanks'
import Passbook from './components/Passbook'
import Error404 from './components/Home/Error404';
import ResetPassword from './components/Home/ResetPassword';
import UserLogin from './components/Home/UserLogin';
import PUserLogin from './components/Home/PUserLogin';
import SignInUp from './components/Mobile/SignInUp';
import BackOfficeLogin from './components/BackOffice/BackOfficeLogin';
import BackOffice from './components/BackOffice/BackOffice'


// const Home = lazy(() => import('./components/Home/Home'));
// const Form = lazy(() => import('./components/HomePage/form/Form'));
// const AboutUs = lazy(() => import('./components/Home/AboutUs')) ;
// const DonationPickup = lazy(() => import('./components/DonationForm/form/DonationPickup')) ;
// const Donationform = lazy(() => import('./components/DonationForm/form/Donationform'));
// const Cart = lazy(() => import('./components/Cart/Cart'));
// const Product = lazy(() => import('./components/product/Product'));
// const Search = lazy(() => import('./components/Home/Search'));
// const Contact = lazy(() => import('./components/Contact'));
// const ProudDonor = lazy(() => import('./components/HomePage/form/ProudDonar'));
// const Wallet = lazy(() => import('./components/Wallet'));
// const Wishlist = lazy(() => import('./components/product/Wishlist'));
// const GetBooks = lazy(() => import('./components/Home/GetBooks'));
// const ChangeAddress = lazy(() => import('./components/Home/ChangeAddress'));
// const Orders = lazy(() => import('./components/Home/Order'));
// const Category = lazy(() => import('./components/Home/Category'));
// const Faq = lazy(() => import('./components/Home/faq'));
// const TermsCondtion = lazy(() => import('./components/Home/termsandcondition'));
// const PrivacyPolicy = lazy(() => import('./components/Home/privacypolicy'));
// const BookGuidlines = lazy(() => import('./components/Home/bookguidlines'));
// const DonarAccDonarReq = lazy(() => import('./components/Home/DonarAccDonarReq'));
// const CartThanks = lazy(() => import('./components/Cart/CartThanks'));
// const DonationThanks = lazy(() => import('./components/DonationForm/layout/DonationThanks'));
// const Passbook = lazy(() => import('./components/Passbook')); 
// const Error404 = lazy(() => import('./components/Home/Error404'));
// const ResetPassword = lazy(() => import('./components/Home/ResetPassword'));
// const UserLogin = lazy(() => import('./components/Home/UserLogin'));
// const PUserLogin = lazy(() => import('./components/Home/PUserLogin'));
// const SignInUp = lazy(() => import('./components/Mobile/SignInUp'));
// const BackOfficeLogin = lazy(() => import('./components/BackOffice/BackOfficeLogin'));
// const BackOffice = lazy(() => import('./components/BackOffice/BackOffice'));

export default (
  // <Suspense fallback={<div>Loading...</div>}>
	<Switch>
    <Route exact path="/login"
         component={SignInUp}/>

      <Route exact path="/donate-books"
         component={Form}/>
      <Route exact path="/pages/terms-conditions"
         component={TermsCondtion}/>
      <Route exact path="/wallet/passbook/"
      component={Passbook}/>
      <Route exact path="/donor/donor_donation_request"
         component={DonarAccDonarReq}/>
      <Route exact path="/pages/book-condition-guidelines"
         component={BookGuidlines}/>
      <Route exact path="/pages/privacy-policy"
         component={PrivacyPolicy}/>
      <Route exact path="/faq"
         component={Faq}/>     
      <Route exact path="/customer"
            component={ChangeAddress}/> 
          <Route exact path="/customer/customer_account"
            component={ChangeAddress}/>
        <Route exact path="/customer/customer_order"
            component={Orders}/>
          {/* <Route exact path="/search"
            component={Search}/> */}
          <Route  path="/search"
            component={Search}/>
          <Route exact path="/donate-books/form"
            component={Donationform}/>
          <Route exact path="/"
            component={Home}/>
          <Route  path="/category/" 
          //  render={(props) => <Category {...props} reload={true} />}
            component={Category}
            />
          <Route exact path="/pages"
            component={AboutUs}/>
          <Route  path="/forpassreset/:token"
            component={ResetPassword}/>
          <Route exact path="/pages/about-us"
            component={AboutUs}/>
          <Route exact path="/product/:slug"
            component={Product}/>
      <Route exact path="/product"
            component={GetBooks}/>
          <Route exact path="/view-cart"
            component={Cart}/>
          <Route exact path="/contact-us"
            component={Contact}/>
          <Route exact path="/proud-donors"
            component={ProudDonor}/>
          <Route exact path="/mypustak-wallet"
            component={Wallet}/>
          <Route exact path="/my-wish-list"
            component={Wishlist}/> 
          <Route exact path="/get-books/"
            component={GetBooks}/>
          <Route exact path="/view-cart/thank-you"
            component={CartThanks}/>
          <Route exact path="/donate/thank-you"
            component={DonationThanks}/>
      <Route exact path="/backofficelogin"
            component={BackOfficeLogin}/>
      <Route  path="/backoffice"
         component={BackOffice}/>
        <Route exact path="/donate/pickup/:wt"
         component={DonationPickup}/>
      <Route  
         component={Error404}/>
	</Switch>
  // </Suspense>
);

