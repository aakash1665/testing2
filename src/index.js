import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { preload } from 'react-router-server';
import { renderToString } from 'react-router-server';
import { Module } from 'react-router-server';
import * as serviceWorker from './serviceWorker';
import {ThroughProvider} from 'react-through'

import ReactGA from 'react-ga';
ReactGA.initialize('UA-60758198-1', { debug: true });
ReactGA.ga('require', 'ec')

const theApp = (
  <ThroughProvider>
    <App />
  </ThroughProvider>
)
ReactDOM.render(theApp, document.getElementById('root'))

// ReactDOM.render(<App />, document.getElementById('root'));

require('dotenv').config()
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
