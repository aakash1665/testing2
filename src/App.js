import React, { Component } from 'react';
/*import Header from './components/Layout/Header.js';*/
import './App.css';
import { Helmet } from "react-helmet";

// import dotenv 
// import 'bootstrap/dist/css/bootstrap.min.css';

// import moduleName from './components/Home/f';

import {Provider  } from "react-redux";
import store from './store'
import MediaQuery from 'react-responsive';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom'
import { matchRoutes, renderRoutes } from "react-router-config";
import "./config";
// import routes from './router';

import router from './router'

class App extends Component {

  state = {
    fetched:false
  }

  componentDidMount(){
    // this.mobilebtn();
  }
  // mobilebtn=()=>{
  //   let called=false;
  //         var mobileBtn=document.querySelector(".mobile-btn");
  //         var mobilemenu=document.querySelector(".mobilemenu");
  //         var closeBtn=document.querySelector(".close");
  //         var closeCategoriesBtn=document.querySelector(".close-category");
  //         var overlay=document.querySelector(".overlay");

  //         mobileBtn.addEventListener ("click", function (){
  //           mobilemenu.className += ' open';
  //           overlay.className+=" open";
  //           console.log("click");
  //           called=true;
  //           console.log(called);
  //         })
  //         closeBtn.addEventListener("click", function() {
  //           mobilemenu.className="mobilemenu";
  //           overlay.className="overlay";
  //           called=false;
  //           console.log(called);
  //         })
  //         window.addEventListener("click", function(event) {
  //           if(event.target===overlay) {
  //            mobilemenu.className="mobilemenu";
  //            overlay.className="overlay";
  //           }
  //         })
  // }
 
  render() {
    return (
      <Provider store={store}>
      {/* REACT Browser ROUTE */}
      <Helmet>
          <title>
            Books Online India, Buy Online Book In India –mypustak.com
          </title>
          <meta
            name="description"
            content="Books are the hub of knowledge. Get the books online in India with us. We aimed to aid (help) the needy one with education and knowledge."
          />
        </Helmet>
      <Router>
      <div className="App">
        {router}
      </div>
      
      </Router>
      {/* END REACT Browser ROUTE */}
      </Provider>
    );
  }
}

export default App;
