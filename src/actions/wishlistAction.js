import {USERWISHLIST,SETRELOADWISHLIST} from './types'
import axios from 'axios'
import config from 'react-global-configuration'

export const Uwishlist = (details) =>  dispatch =>{
    
    // alert("getaddress")
    axios.get(`${config.get('apiDomain')}/api/v1/wishlist/fetch-wishlists`,{headers:{
    // axios.get('http://103.217.220.149:80/api/v1/get/user_address',{headers:{
        'Authorization': details
      }} 
    ).then(res=>{
        // console.log(details)
        dispatch({
            type:USERWISHLIST,
            payload:res.data
    })
}).catch(err=>console.log(err,details))
};
export const SetWishlistReloader = () =>  dispatch =>{
    // alert("logout")
        dispatch({
            type:SETRELOADWISHLIST,
            // payload:res.data
    })  
};