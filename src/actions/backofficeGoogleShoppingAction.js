import {
    GET_GOOGLE_BOOKS
}
from './types';
import axios from "axios";
import config from "react-global-configuration";

export const getBooksForBooks=()=>dispatch=>{

    axios.get(`http://localhost:8000/api/v1/googleShopping/fetchbooks`)
    .then(res=>{
        console.log(res,"GoogleBooks");
        
        dispatch({
            type: GET_GOOGLE_BOOKS,
            payload: { data:res.data.Books }
          });
    })
    .catch(err=>{
        console.log(err)
        
    })
    
}