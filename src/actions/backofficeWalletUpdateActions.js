import axios from "axios";
import { GET_EMAIL_AND_USER_ID_IN_WALLET } from "./types";
import config from 'react-global-configuration'

export const getUserDetails = (token, order_id) => dispatch => {
  // console.log("token iS", token);
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  axios
    .get(
      `${config.get('apiDomain')}/api/v1/wallet-transaction/order-search/${order_id}`,
      header
    )
    
    .then(res => {
      // console.log(res.data);
      dispatch({
        type: GET_EMAIL_AND_USER_ID_IN_WALLET,
        payload: res.data
      });
      // console.log("In action, the data are ", res.data);
    })
    .catch(err => {
      alert(
        `The order id #${order_id} is invalid, kindly enter a valid order id`
      );
      console.log(err);
    });
};
