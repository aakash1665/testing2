import axios from "axios";
import {
  GET_ORDERDETAILS_OF_EVERY_ORDER,
  RESTOCKING_SINGAL_BOOK,
  MAKE_BOOK_OUTSTOCK
} from "./types";

export const getOrderDetails = (token, order_id) => dispatch => {
  // console.log("In actions the data are ", token, uploader, startDate, endDate);

  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  //localhost:8000/api/v1/donation-tracking/track-request-id/123456

  http: axios
    .get(
      `https://data.mypustak.com/api/v1/fetchdetailsorder/every-order/${order_id}`,
      header
    )
    .then(res => {
      console.log(res.data.orde_details);
      dispatch({
        type: GET_ORDERDETAILS_OF_EVERY_ORDER,
        payload: res.data
      });
      console.log("In action, the data are ", res.data);
    })
    .catch(err => {
      dispatch({
        type: GET_ORDERDETAILS_OF_EVERY_ORDER,
        payload: { wallet: 0 }
      });
      alert(`This id #${order_id} doesn't have any details.`);
      console.log(err);
    });
};

export const RestockBook = (token, book_id, book_inv_id) => dispatch => {
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
    axios
    .post(
      `https://data.mypustak.com/api/v1/restock_return_order/update/${book_inv_id}/${book_id}`,
      null,
      header
    )
    .then(res => {
      console.log(res.data);
      dispatch({
        type: RESTOCKING_SINGAL_BOOK,
        payload: res.data
      });
      if (res.status == 200) {
        alert(
          `Return was successful of Book Id :#${book_id} and Book Inv Id :#${book_inv_id}`
        );
      }
    })
    .catch(err => {
      alert(`ERROR : ${err}`);
    });
};

export const makeBookOutStock =(token, book_id, book_inv_id)=> dispatch =>{

  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  console.log(header, book_id, book_inv_id,"MakeBookOutStock");
  
    axios
    .post(
      `https://data.mypustak.com/api/v1/restock_return_order/outstock/${book_inv_id}/${book_id}`,
      null,
      header
    )
    .then(res => {
      dispatch({
        type:MAKE_BOOK_OUTSTOCK,
        payload:res.data
      })
      if (res.data.status == 200) {

        alert(`Book outstocked successfully`)
        // this.setState({ openConfirmationOutstockDialog: false });
        // this.onClickBooksStillInstock()
      }
    })
    .catch(err => {
      alert(`something went wrong`)
      console.log(err)});
}
;