import axios from "axios";
import { GET_TRACKINGNO_AGAINST_DONATIONID } from "./types";

export const getDonationTracking = (token, donation_req_id) => dispatch => {
  console.log("In actions the data are ", token,donation_req_id);

  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  //localhost:8000/api/v1/donation-tracking/track-request-id/123456

  axios
    .get(
      `https://data.mypustak.com/api/v1/donation_tracking/track-request-id/${donation_req_id}`,
      header
    )
    .then(res => {
      console.log(res.data.tracking_no);
      dispatch({
        type: GET_TRACKINGNO_AGAINST_DONATIONID,
        payload: res.data
      });
      console.log("In action, the data are ", res.data);
    })
    .catch(err => {
      alert(`This id #${donation_req_id} doesn't have any tracking id.`);
      console.log(err);
    });
};
