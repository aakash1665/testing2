import axios from "axios";
import { SEARCH_IN_WALLET } from "./types";

export const getSearchResults = (token, id) => dispatch => {
  console.log("token iS", token);
  let wallet = "";
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  axios
    .get(
      `https://data.mypustak.com/api/v1/wallet-transaction/search-wallet/${id}`,
      header
    )
    .then(res => {
      // console.log("In Wallet Action Search ",res.data);
      dispatch({
        type: SEARCH_IN_WALLET,
        payload: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: SEARCH_IN_WALLET,
        payload: wallet
      });
      alert(`Search Not Found`);
      console.log(err);
    });
};
