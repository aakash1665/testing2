// import {LOGIN,SIGNUP,LOGINFAILS,LOGOUT,SIGNEDUP,SHOWLOADER,GETADDRESS,ADD_ADDRESS} from './types'
import { GETDONATIONLIST,
  UPDATEDONATIONBYID,
  DONATIONLISTLOADING,
  FETCHDONATIONBYID,
  UPDATEDONATION,
  SERACHDONATIONLIST,
  ADD_DONATIONID
} from "./types";
import axios from "axios";
import config from "react-global-configuration";

export const getDonationsList = ({ page, token, search, searchValue }) => dispatch => {

  dispatch({
      type: DONATIONLISTLOADING,
      payload: {  }
    });

let header = {
  headers: {
    Authorization: `Token ${token}`
  }
};

let URL = '';
if(searchValue) {
  URL = `${config.get(
            "apiDomain"
          )}/api/v1/fetch_donation_details/fetch-donation-details/${searchValue}`;
  axios
  .get(
    URL,
    header
  )
  .then(res => {
      dispatch({
        type: SERACHDONATIONLIST,
        payload: { data: res.data }
      });
  })
  .catch(err => console.log(err));
} else {
  URL = `${config.get(
          "apiDomain"
        )}/api/v1/donation-form-status/fetch-donation-reqeust/${page + 1}/`;

  axios
  .get(
    URL,
    header
  )
  .then(res => {
    if (res.status === 200) {
      if(search) {
        dispatch({
          type: SERACHDONATIONLIST,
          payload: { data: res.data }
        });
      } else {
        dispatch({
          type: GETDONATIONLIST,
          payload: { data: res.data }
        });
      }
    }
  })
  .catch(err => console.log(err));
}
};

export const updateDonationById  = ({ donation_req_id, new_status, token }) => dispatch => {
console.log(donation_req_id,"req");

  dispatch({
      type: DONATIONLISTLOADING,
      payload: {  }
    });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  let body = {
      status: new_status
  };
  axios
  .patch(
    `${config.get("apiDomain")}/api/v1/donation-form-status/update-donation-status/${donation_req_id}`,
    body,
    header
  )
  .then(res => {
    if (res.data.status === 200) {
      dispatch({
        type: UPDATEDONATIONBYID,
        payload: { donation_req_id, new_status }
      });
      
    }
  })
  .catch(err => console.log(err));
}

export const updateDonation = ({ donation, token }) => dispatch => {

dispatch({
    type: DONATIONLISTLOADING,
    payload: {  }
  });

let header = {
    headers: {
      Authorization: `Token ${token}`
    }
};
let body = donation;
axios
.patch(
    `${config.get(
      "apiDomain"
    )}/api/v1/donation-form-status/update-donation-request/${donation.donation_req_id}`,
  body,
  header
)
.then(res => {
  if (res.data.status === 200) {
    dispatch({
      type: UPDATEDONATION,
      payload: { data: res.data }
    });
    alert(`Donation ${donation.donation_req_id} updated successfully.`)
  }
})
.catch(err => {
  console.log(err,"Update Err",body);
  alert(`Donation ${donation.donation_req_id} updated failed.`)
  dispatch({
    type: UPDATEDONATION,
    payload: { data: {} }
  });
});
}

export const getDonationById = ({ donation_req_id, token }) => dispatch => {

dispatch({
    type: DONATIONLISTLOADING,
    payload: {  }
  });

let header = {
headers: {
  Authorization: `Token ${token}`
}
};
axios
  .get(
    `${config.get(
      "apiDomain"
    )}/api/v1/donation-form-status/fetch-donation-reqeust-by-id/${donation_req_id}/`,
    header
  )
  .then(res => {
    if (res.status === 200) {
      console.log(res.data, "donationadaf")
      dispatch({
        type: FETCHDONATIONBYID,
        payload: { data: res.data.data }
      });
    }
  })
  .catch(err => console.log(err));
};

export const addDonationTrackingId = (donation_req_id, tracking_no, token) => dispatch=> {
  console.log("reqq",donation_req_id,tracking_no, token,"oo");
  
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  let body = {
    "tracking_no": tracking_no
  }
    axios
    .patch(
      `${config.get(
        "apiDomain" 
      )}/api/v1/addDonationRequest/add-donaton-request/${donation_req_id}`,
      body,
      header
    )
    .then(res => {
      dispatch({
        type: ADD_DONATIONID,
        payload: {  }
      });
      if (res.data.status === 200) {
        alert(`${res.data.message}`)
      }
    })
    .catch(err => console.log(err));
  
}
