// import {LOGIN,SIGNUP,LOGINFAILS,LOGOUT,SIGNEDUP,SHOWLOADER,GETADDRESS,ADD_ADDRESS} from './types'
import {
  ORDERSLOADING,
  GETORDERS,
  ORDERUPDATE,
  CANCELORDER,
  SENDINGEMAIL,
  SENTEMAIL,
  UPDATEORDERBOOK,
  UPDATEORDERWEIGHT,
  CPRECOMMENDATIONS,
  CPLOADING,
  FETCH_PENDING_COD,
  CLEAR_FETCH_PENDING_COD
 } from "./types";
import axios from "axios";
import config from "react-global-configuration";

import { sortBy } from 'lodash';

export const getOrders = ({ token }) => dispatch => {

  dispatch({
    type: ORDERSLOADING,
    payload: {  }
  });

  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
    axios
    .get(`${config.get('apiDomain')}/api/v1/order_details_api/confirm-order/`,
      header
    )
    .then(res => {
      if (res.status === 200) {
        let totalOrders = res.data.output;

        totalOrders = sortBy(totalOrders, ['order_id']);

        let confirmOrders = totalOrders.filter(order => (order.payusing !== "cod") || (order.payusing === "cod" && order.cod_offer_interested === 2 ) );
        let confirmationPendingOrders = totalOrders.filter(order => order.payusing === "cod" && order.cod_offer_interested !== 2)

        axios
        .get(
          `${config.get(
            "apiDomain"
          )}/api/v1/get/all_orders`,
          header
        )
        .then(async res => {
          if (res.status === 200) {
            
            let allOrdersWithBooks = res.data.output;

            allOrdersWithBooks = sortBy(allOrdersWithBooks, ['order_id']);

            confirmationPendingOrders = await Promise.all(confirmationPendingOrders.map(order => {
              return checkConfirmOrderPossibility({order, token});
            }));

            dispatch({
              type: GETORDERS,
              payload: { confirmOrders, confirmationPendingOrders, allOrdersWithBooks }
            });
          }
        })
        .catch(err => console.log(err));
      }
    })
    .catch(err => console.log(err));
};

export const updateOrder = ({order, token}) => dispatch => {

  dispatch({
      type: ORDERSLOADING,
      payload: {  }
    });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  let body = order;
  axios
  .patch(
    `${config.get(
      "apiDomain"
    )}/api/v1/order_details_api/update-order/${order.order_id}`,
    body,
    header
  )
  .then(res => {
    if (res.data.status === 200) {
      dispatch({
        type: ORDERUPDATE,
        payload: {order}
      });
    }
    console.log("Updated Order courier ");
    
  })
  .catch(err => console.log(err));
}

export const cancelOrder = ({order, token}) => dispatch => {

  dispatch({
      type: ORDERSLOADING,
      payload: {  }
    });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  let body = {
    data: { order_id: order.order_id }
  };
  axios
  .post(
    `${config.get(
      "apiDomain"
    )}/api/v1/post/cancel_order`,
    body,
    header
  )
  .then(res => {
    if (res.status === 200) {
      dispatch({
        type: CANCELORDER,
        payload: {order}
      });
      alert(`order #${order.order_id} is cancelled.`);
    }
  })
  .catch(err => console.log(err));
}

export const sendEmailForOrder = ({order, token}) => dispatch => {

  dispatch({
      type: SENDINGEMAIL,
      payload: {  }
    });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  let body = { email: order.email };
  axios
  .post(
    `${config.get("apiDomain")}/common/email_CodConfirmation/`,
    body,
    header
  )
  .then(res => {
    if (res.status === 200) {
      dispatch({
        type: SENTEMAIL,
        payload: {order}
      });
      alert(`Email is sent on ${order.email} for order #${order.order_id}`);
    }
  })
  .catch(err => {
    alert(`Send email is failed on ${order.email} for order #${order.order_id}`);
    console.log(err)
  });
}


const checkConfirmOrderPossibility = ({ order, token }) => {
  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };

  return new Promise((resolve, reject) => {
    axios
      .get(
        `${config.get(
          "apiDomain"
        )}/pincode/cod_check/${order.pincode}/`,
        header
      )
      .then(res => {
        if (res.status === 200) {
          resolve(Object.assign(order, { delivery_available: res.data.status }))
        }
      })
      .catch(err => {
        console.log(err)
      });
  })
}

export const updateOrderBook = ({book_id, order_id, is_book_found, token}) => dispatch => {

  console.log(book_id, order_id, is_book_found, "asdfasdfasdf");
  // dispatch({
  //     type: ORDERSLOADING,
  //     payload: {  }
  //   });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };

  let body = {};
    if(is_book_found == "Y") {
      body = {
        order_id,
        is_book_found: "Y"
      }
    } 
    else if(is_book_found == "M") {
      body = {
        order_id,
        is_book_found: "M"
      }
    } 
    else if(is_book_found == "D") {
      body = {
        order_id,
        is_book_found: "D"
      }
    }   
    else {
      body = {
        order_id,
        is_book_found: "N"
      }
    }

  axios
  .patch(
    `${config.get(
      "apiDomain"
    )}/api/v1/order_details_api/update-order-book/${book_id}`,
    body,
    header
  )
  .then(res => {
    if (res.data.status === 200) {
      // dispatch({
      //   type: UPDATEORDERBOOK,
      //   payload: {book}
      // });
    }
  })
  .catch(err => console.log(err));
}

export const updateOrderWeight = ({order, amount, weight, token}) => dispatch => {

  console.log(order, amount, weight, token);
  // dispatch({
  //     type: ORDERSLOADING,
  //     payload: {  }
  //   });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  let body = {
    amount : amount ? amount : order.amount,
    weight: weight
  };
  axios
  .patch(
    `${config.get(
      "apiDomain"
    )}/api/v1/order_details_api/update-order/${order.order_id}`,
    body,
    header
  )
  .then(res => {
    if (res.data.status === 200) {
      // dispatch({
      //   type: UPDATEORDERWEIGHT,
      //   payload: {order}
      // });
      alert(`Order ${order.order_id} is updated successfully.`)
    }
  })
  .catch(err => console.log(err));
}


export const cpRecommendation = ({order, token}) => dispatch => {

  dispatch({
      type: CPLOADING,
      payload: {  }
    });

  if(!order.weight) {
    alert(`Please update weight first.`);
    return false;
  }

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  let body = {
    data: {
      order_id : order.order_id,
      weight: order.weight
    }
  };
  axios
  .post(
    `${config.get(
      "apiDomain"
    )}/api/v1/post/cp_recommendation`,
    body,
    header
  )
  .then(res => {
    console.log(res)
    if (res.data.status === 200) {
      dispatch({
        type: CPRECOMMENDATIONS,
        payload: { data: res.data.output.result}
      });
      console.log(res.data.output.result,"CpRecom");
      
    }
  })
  .catch(err => console.log(err));
}

export const createPOST = ({body}) => dispatch => {

  console.log(body, "body")
  let header= {
    headers: {
    }
  }

  return new Promise((resolve, reject) => {
  axios
  .post(
    `https://www.clickpost.in/api/v3/create-order/?username=mypustak&key=1e96edab-1b4b-440b-894f-63a2d42e2be0`,
    body,
    header
  )
  .then(res => {
    console.log(res);
    if(res.data.meta.status == 200) {
      
      alert(`POST created successfully for ${res.data.order_id} and Waybill No is - ${res.data.result.waybill}`);



      resolve({ shipment_tracking_no: res.data.result.waybill })
    } else {
      alert(`POST creation failed. ${res.data.meta.message}`);
      resolve(false);
    }
  })
  .catch(err => {
    console.log(err);
    alert(`POST creation failed.`);
    resolve(false) 
  });
  })
  
}
export const ClearfetchPendingCod =()=> dispatch=>{
    dispatch({
    type: CLEAR_FETCH_PENDING_COD,
    payload: {  }
  });
  console.log("Clear");
  
}
export const fetchPendingCod = (orderid,token) =>dispatch =>{
  console.log(orderid,token);
  // dispatch({
  //   type: CLEAR_FETCH_PENDING_COD,
  //   payload: {  }
  // });
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
};
  axios.get(
    `${config.get(
      "apiDomain"
    )}/api/v1/order_details_api/fetch-pending-cod-order-book-by-orderid/${orderid}/`,header
  ).then(res =>{
          dispatch({
            type : FETCH_PENDING_COD,
            payload:res.data.output,
          })
          console.log(res.data);    
    })
  .catch(err=>console.log(err)
  )
  
}