import {CHANGE_LOGIN,ADD_DONATION,CHANAGE_CATEGORIES,RESETRPID,
    GET_RP_ID,SETPICKUP,GETDONATIONHISTORY,DONATIONMAILDATA} from './types'
import axios from 'axios'
import config from 'react-global-configuration'
export const ChangeLogin = (login) =>{
    // alert("okk");
    return{
    type:CHANGE_LOGIN,
}}

export const AddDonation = (donation,token) =>  dispatch =>{
    // console.log(donation)
    // axios.post('http://103.217.220.149:80/donate-books/api/donateBook/',donation  
    axios.post(`${config.get('apiDomain')}/api/v1/post/donation_form`,donation,{headers:
        {'Authorization': token,}}
    ).then(res=>{
        dispatch({
            type:ADD_DONATION,
            payload:res.data
    })
        
    }).catch(err=>console.log(err));
};



export const Get_Rp_Id = (data) =>  dispatch =>{
    console.log(data,"data")
    axios.post(`${config.get('apiDomain')}/api/v1/post/get_razorpayid`,data 
    // axios.post(`${config.get('apiDomain')}/api/v1/post/get_razorpayid`,data  
    ).then(res=>{
        // console.log(data);
        
        dispatch({
            type:GET_RP_ID,
            payload:res.data.output
    })
        
    }).catch(err=>console.log(err,data));
};

export const Reset_Rp_Id =()=>{
    return{
        type:RESETRPID,
        // payload:category,
    }
}


export const ChangeCategories =(category)=>{
    return{
        type:CHANAGE_CATEGORIES,
        payload:category,
    }
}
export const DonationMailData =(data)=>{
    // console.log(data);  
    return{
        type:DONATIONMAILDATA,
        payload:data,
    }
}
export const SetPickup =()=>{
    return{
        type:SETPICKUP,
        // payload:category,
    }
}

export const GetDonationHistory = (token) =>  dispatch =>{
    // console.log("donation")
    // axios.post('http://103.217.220.149:80/donate-books/api/donateBook/',donation  
    axios.get(`${config.get('apiDomain')}/donation/donorhistory/`,{headers:
        {'Authorization': token}}
    ).then(res=>{
        console.log(res);
        
        dispatch({
            type:GETDONATIONHISTORY,
            payload:res.data
    })
        
    }).catch(err=>console.log(err));
};

// axios.post(`${config.get('apiDomain')}/api/v1/post/donation_form`,donation,{headers:
//     {'Authorization': token,}}
// ).then(res=>{
//     dispatch({
//         type:ADD_DONATION,
//         payload:res.data
// })
    
// }).catch(err=>console.log(err));