import {SEARCH,GETSEARCH,FETCHSEARCH,MAKESEARCHBLANK,GETSEARCHGETBOOK,SETCATEGORY} from './types'
import axios from 'axios'
import config from 'react-global-configuration'
export const doSearch = (searchValue) =>{
    // alert("in homeAc");

    return{
    type:SEARCH,
    payload:searchValue,
}}

export const makeSearchBlank = () =>{
    // alert("in homeAc");

    return{
    type:MAKESEARCHBLANK,
    // payload:searchValue,
}}


export const GetSearchResult = (searchValue,start) =>dispatch=>{
    // alert("in homeAc");
    console.log(searchValue,"searchValue");
    
    axios.get(`${config.get('apiDomain')}/search/${searchValue}/${start}/`)
    .then(res=>{
        dispatch({
            type:GETSEARCH,
            payload:res.data.hits
        })
    })
    .catch(err=>console.log(err))
    };
    // ********************
    // axios.get(`${config.get('apiDomain')}/api/v1/get/get-books/${this.state.start}/`)
    // // axios.get(`http://103.217.220.149:80/api/v1/get/get-books/${this.state.start}/`)
    // .then(res=>this.setState({GetBookResult:res.data}))
    // .catch(err=>console.log(err)
    // )
// export const GetSearchGetBooks = () =>dispatch=>{
//     // alert("in homeAc");
//     axios.get(`${config.get('apiDomain')}/api/v1/get/get-books/1/`)
//     .then(res=>{
//         dispatch({
//             type:GETSEARCHGETBOOK,
//             payload:res.data
//         })
//     })
//     .catch(err=>console.log(err))
//     };

export const fetchBooksAcc = (searchValue,start) =>dispatch=>{
    // alert("in homeAc");
    axios.get(`${config.get('apiDomain')}/search/${searchValue}/${start}/`)
    .then(res=>{
        dispatch({
            type:FETCHSEARCH,
            payload:res.data.hits
        })
    })
    .catch(err=>console.log(err))

    };

export const setCategory = (category) =>{
    // alert("category")
    return{
        type:SETCATEGORY,
        payload:category,
}};