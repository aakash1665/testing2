import axios from "axios";
import config from "react-global-configuration";

export const updateTrackingAndBarcode = ({ tracking_no, from_barcode, to_barcode, study_material, book_wastage, token }) => {
  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };

  let body = {
    from_barcode,
    to_barcode,
    study_material,
    book_wastage
  }

  return new Promise((resolve, reject) => {
    axios
      .patch(
        `${config.get(
          "apiDomain"
        )}/api/v1/post/tracking-barcode-api/update-tracking-barcode/${tracking_no}`,
        body,
        header
      )
      .then(res => {
        if (res.data.status === 200) {
          resolve(res.data.message)
        }
      })
      .catch(err => {
        console.log(err);
        resolve(err.message)
      });
  })
}