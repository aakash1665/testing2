import axios from "axios";
import { SEARCH_IN_WALLET } from "./types";

export const getSearchResults = (token, id) => dispatch => {
  // console.log("token iS", token);
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  axios
    .get(
      `https://data.mypustak.com/api/v1/wallet-transaction/search-wallet/${id}`,
      header
    )
    .then(res => {
      // console.log(res.data);
      dispatch({
        type: SEARCH_IN_WALLET,
        payload: res.data
      });
      const wallet = res.data.wallet;
      
    })
    .catch(err => {
      alert(`The order id #${id} is invalid, kindly enter a valid order id`);
      console.log(err);
    });
};
