import {GETDONARDETAILS,GETDONORSEARCHED,SETRESPONSEMSG} from './types'
import axios from 'axios'
import config from 'react-global-configuration'

export const donardetails = (pageNo) =>  dispatch =>{
    // console.log(details)
    // alert(pageNo)
    axios.get(`${config.get('apiDomain')}/donation/donortable/page${pageNo}/`
    ).then(res=>{
        dispatch({
            type:GETDONARDETAILS,
            payload:res.data
    })
})
};

export const searchdonor = (data) =>  dispatch =>{
    // console.log(details)
    // alert(pageNo){{Nurl}}/api/v1/search_proud_donor/Mukul
    axios.get(`${config.get('apiDomain')}/api/v1/search_proud_donor/${data}`
    ).then(res=>{
        dispatch({
            type:GETDONORSEARCHED,
            payload:res.data
    })
})
};

export const setResponseMsg = () =>  dispatch =>{

        dispatch({
            type:SETRESPONSEMSG,
    })
};