import {GETORDERD,
        CANCELORDER,
        SETRELOADLORDER,
        CONVERTORDER,
        VIEWORDERDETAILS, 
        MAKECANCELMSGBLANK,
        CONVERTORDERSENDREQ,
        UPDATEPAYMENT,
} from './types'
import axios from 'axios'
import config from 'react-global-configuration'

export const orderdetails = (details,page) =>  dispatch =>{
    // alert("ok")
    // console.log(details)
    // alert(`${config.get('apiDomain')}/api/v1/get/user_orders/${page}/`)
    // http://103.217.220.149:80/api/v1/get/user_orders/1/
    // http://103.217.220.149:80/api/v1/get/user_orders/ListOrders/1
    axios.get(`${config.get('apiDomain')}/api/v1/get/user_orders/ListOrders/${page}/`,{headers:{
                  'Authorization': details
                }} 
    ).then(res=>{
        dispatch({
            type:GETORDERD,
            payload:res.data
    })
}).catch(err=>{console.log(err);
})
};

export const ViewOrderDetails = (details,Order_id) =>  dispatch =>{
    // alert("ok")  http://103.217.220.149:80/api/v1/get/user_orders/ViewOrderDetails/526427
    // console.log(details)
    // alert(`${config.get('apiDomain')}/api/v1/get/user_orders/${page}/`)
    // http://103.217.220.149:80/api/v1/get/user_orders/1/
//  http://103.217.220.149:80/api/v1/get/user_orders/ViewOrderDetails/526427
    console.log(Order_id);

    axios.get(`${config.get('apiDomain')}/api/v1/get/user_orders/ViewOrderDetails/${Order_id}`,{headers:{
                  'Authorization': details
                }} 
    ).then(res=>{
        dispatch({
            type:VIEWORDERDETAILS,
            payload:res.data
    })
    console.log(res.data);
    
}).catch(err=>{console.log(err,Order_id);
})
};


export const CancelOrder = (orderid,token) =>  dispatch =>{
    console.log(orderid,token)
    // http://103.217.220.149:80/api/v1/post/cancel_order
    // http://103.217.220.149:80/api/v1/post/cancel_order
    axios.post(`${config.get('apiDomain')}/api/v1/post/cancel_order`,orderid,{headers:{
                  'Authorization': token
                }} 
    ).then(res=>{ console.log(res);
    
        dispatch({
            type:CANCELORDER,
            payload:res.data
    })
}).catch(err=>{console.log(err,orderid,token);
})
};

export const ConvertOrder = (orderid,token) =>  dispatch =>{
    // dispatch({
    //     type:CONVERTORDERSENDREQ,
        
    //     })
    // console.log(orderid,token)
    // http://103.217.220.149:80/api/v1/convert_cod_order_to_prepaid_order/convert-cod-order-to-prepaid-order526202
    axios.get(`${config.get('apiDomain')}/api/v1/convert_cod_order_to_prepaid_order/convert-cod-order-to-prepaid-order/${orderid}`,{headers:{
                  'Authorization': token
                }} 
    ).then(res=>{
         console.log(res);
    
        dispatch({
            type:CONVERTORDER,
            payload:res.data.prepaid_order_payment_value,
    })
}).catch(err=>{console.log(err);
})
};
export const SetReloadOrder = () =>  dispatch =>{
    // alert("logout")
        dispatch({
            type:SETRELOADLORDER,
            // payload:res.data
    })  
};

export const MakeCancelOrderBlank = () =>  dispatch =>{
    // alert("logout")
        dispatch({
            type:MAKECANCELMSGBLANK,
            // payload:res.data
    })  
};

export const UpdatePayment = (userToken,SendData) => dispatch => {
    axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-order/${this.props.OrderId}`,SendData,{headers:{
        'Authorization': `Token ${userToken}`,
        // ''
      }})
      .then(res=>{
        console.log('Updated Order');
      this.setState({GotOThankYou:true})
      dispatch({
        type:CONVERTORDER,
        payload:res.data.prepaid_order_payment_value,
})

    })
      .catch(err=>console.log(err,SendData) 
      )
}