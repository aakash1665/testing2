import {ADDTOCART,
    POPUPCART,
    closePOPUPCART,
    REMOVECART,
    CARTSESSION,
    REMOVEALLCART,
    REDIRECTWALLETTOCART,
    TOBEADDEDTOCART,
    REMOVETOBEADDEDTOCART,
    REMOVECARTLOGOUT,
    MOBILECARTREDIRECT,
    SHOWRZPAYERR,
    ADDCARTPRICE,
    SENDADDRESSId,
    ORDERDETAILS,
    SETORDERID,
    WALLETRAZORPAY,
    UPDATEORDERSUCC,
    PAYPALLERROR, 
    OUTOFSTOCK,   
} from './types';
import config from 'react-global-configuration'
import axios from 'axios'
import { error } from 'util';

export const AddToCart = (cart) =>  dispatch =>{
    // console.log(cart)
    // alert("cart")
    // alert("cartAcc")
    // axios.post('http://127.0.0.1:8000/core/get_token/',details 
    // ).then(res=>{
    //     if(res.status === 200){
    //     console.log(typeof(res.data));
    //     if (typeof(res.data) !== 'string') {
    //         dispatch({
    //             type:LOGIN,
    //             payload:res.data
    //         })
    //     }else{
    //         dispatch({
    //             type:LOGINFAILS,
    //             payload:res.data
    //         })
    //     } 
    // }
    //     // if(`Token` in res.data){
    //     //     alert("wrong")
    //     // }
    // }).catch(err=>console.log(err));
    dispatch({
        type:ADDTOCART,
        payload:cart
})
};

export const MobileCartRedirect = () =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:MOBILECARTREDIRECT,
        // payload:data
})
};

export const ToBeAddedToCart = (data) =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:TOBEADDEDTOCART,
        payload:data
})
};

export const orderUpdateSucc = () =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:UPDATEORDERSUCC,
        // payload:data
})
};

export const RemoveToBeAddedToCart = (data) =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:TOBEADDEDTOCART,
        // payload:data
})
};
export const CartopenModal = () =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:POPUPCART,
        // payload:true
})
};

export const removeAllCart = () =>  dispatch =>{
    // alert("rmAcc")
    dispatch({
        type:REMOVEALLCART,
})
};

export const RemoveCart = (Cart_id,bookInvId,data) =>  dispatch =>{
    // console.log(Cart_id,bookInvId,`Token ${localStorage.getItem('user')}`);
    
    // alert(BookInvId) http://103.217.220.149:80/common/deleteCart/101848/
    axios.patch(`${config.get('apiDomain')}/common/deleteCart/${Cart_id}/`,data,{headers:{
        'Authorization': `Token ${localStorage.getItem('user')}`,   

        'Content-Type':'application/json',
      }} )
    .then(res=>{  
        console.log(res,'DeleteCart');
         
    dispatch({
        type:REMOVECART,
        payload:bookInvId,
    })
}).catch(err=>console.log(err)
)
};

export const CartcloseModal = () =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:closePOPUPCART,
        payload:false
})
};

export const removeFromCartLogout = (bookInvId) =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:REMOVECARTLOGOUT,
        payload:bookInvId
})
};
export const AddPriceCart = (data) =>  dispatch =>{
    // console.log(data);
     
    dispatch({
        type:ADDCARTPRICE,
        payload:data
})
};

export const SendAddressId = (data) =>  dispatch =>{
    // alert(data);
     
    dispatch({
        type:SENDADDRESSId,
        payload:data
})
};


export const OrderDetails = (data,token) =>  dispatch =>{
    // console.log(`${config.get('apiDomain')}/api/v1/post/order_details`,data,{headers:{
    //     'Authorization': token,
    //   }});
    // console.log(data);
    console.log("adding Order",data);
    
    axios.post(`${config.get('apiDomain')}/api/v1/post/order_details`,data,{headers:{
        'Authorization': token,
        'Content-Type':'application/json',
      }} )
    .then(res=>{       
        dispatch({
            type:ORDERDETAILS,
            payload:res.data
        })
    })
    .catch(err=>{console.log(err,data,err.status)
                if(err.response.status == 406){
                    console.log(err.response.data);
                    
                    dispatch({
                        type:OUTOFSTOCK,
                        payload:err.response.data
                        
                    })
                }
    })
};

export const SetOrderId = () =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:SETORDERID,
})
};
// console.log(NewTotalPaymet,TotalPayment,this.props.walletbalance);
export const walletRazorpay = (data) =>  dispatch =>{
    // console.log(data);
     
    dispatch({
        type:WALLETRAZORPAY,
        payload:data
})
};

export const RedirectWalletToCart = () =>  dispatch =>{
    // alert("cartAcc")
    dispatch({
        type:REDIRECTWALLETTOCART,
        // payload:TR
})
};

export const CartSession = (token) =>  dispatch =>{
    console.log("Cart Session---------------------------------------------------------");
    // http://103.217.220.149:80/common/cart_items/
    // console.log(`Token ${token}`===`Token 3f27c209c5377bcad7e7da67e16f5126dabc3813`);

axios.get(`${config.get('apiDomain')}/common/cart_items/`,{headers:{
    'Authorization': token,
    }} 
    ).then(res=>{
        dispatch({
            type:CARTSESSION,
            payload:res.data.items
        })
            // console.log(res)
            
    }).catch(err=>console.log(err)
    )
    };


export const ShowRzPayErr = () =>  dispatch =>{
        // alert("cartAcc")
        dispatch({
            type:SHOWRZPAYERR,
        // payload:TR
})
};

export const PapyPalError = (token,body,orderId) => dispatch => {
    axios.patch(`${config.get('apiDomain')}/api/v1/patch/add_paymentid_ordertable/update-paypal-error/${orderId}`,body,{headers:{
        'Authorization': `Token ${token}`,
        'Content-Type':'application/json',
        }} 
        ).then(res=>{
            console.log("Updated Paypal Error");
            
        })
        .catch(err=>console.log(err)
        )
};

export const OutOfStock = (token,body) =>  dispatch =>{
    // console.log(`${config.get('apiDomain')}/api/v1/post/order_details`,data,{headers:{
    //     'Authorization': token,
    //   }});
    // console.log(data);
    // console.log("adding Order",data);
    // {
    //     "book_inv":[147,160,154]
        
    // }
    // console.log(token,"TOKK");
    axios.post(`${config.get('apiDomain')}/api/v1/outOfStock/`,body,{headers:{
        'Authorization': token,
        'Content-Type':'application/json',
      }} )
    .then(res=>{    
        console.log(res.data,"got data");

        dispatch({
            type:OUTOFSTOCK,
            payload:res.data
            
        })
    })
    .catch(err=>console.log(err,body,"err outOfStock"))
};
// export const OrderDetails = (data,token) =>  dispatch =>{
//     console.log(data);
    
//     axios.post(`${config.get('apiDomain')}/api/v1/post/order_details`,data,{headers:{
//         'Authorization': token,
//         'Content-Type':'application/json',
//       }} )
//     .then(res=>{       
//         dispatch({
//             type:ORDERDETAILS,
//             payload:res.data
//         })
//     })
//     .catch(err=>console.log(err,data))
// };


// http://103.217.220.149:80/api/v1/post/address_form 
// {…}
// ​
// data: Object { title: "aaa", rec_name: "aaa", address: "aaa, aaatest", … }
// ​
// <prototype>: Object { … }
 
// {…}
// ​
// headers: Object { Authorization: "Token da0a3bed7fd86b67f0cddd7f49248813a14f00f4" }
// ​
// <prototype>: Object { … }

// http://103.217.220.149:80/api/v1/post/order_details 
// {…}
// ​
// data: Object { amount: 100, no_of_book: 2, payusing: "razorpay", … }
// ​
// <prototype>: Object { … }
 
// {…}
// ​
// headers: Object { Authorization: "Token da0a3bed7fd86b67f0cddd7f49248813a14f00f4" }
// ​
{/* <prototype>: Object { … } */}


// export const addAddressAction = (token,address) =>  dispatch =>{
//     console.log(address,token)
//     // alert("addres")
//     axios.post(`${config.get('apiDomain')}/api/v1/post/address_form`,address,{headers:{
//     // axios.post('http://103.217.220.149:80/api/v1/post/address_form',address,{headers:{
//         'Authorization': token,
//       }}
//     ).then(res=>{
//         dispatch({
//             type:ADD_ADDRESS,
//             payload:res.data
//     })
        
//     }).catch(err=>console.log(err));
// };

// export const EdituserAddressAction = (token,address) =>  dispatch =>{
//     console.log(address,token)
//     // alert("addres")
//     // axios.post('http://127.0.0.1:8000/donate-books/api/donateBook/',address  
//     axios.post(`${config.get('apiDomain')}/api/v1/post/edit_address`,address,{headers:{
//         'Authorization': token,
//       }}
//     ).then(res=>{
//         dispatch({
//             type:EDITUSER_ADD,
//             payload:res.data
//     })
        
//     }).catch(err=>console.log(err));
// };