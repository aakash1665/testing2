import axios from "axios";
import { TOTAL_SALE_THIS_MONTH, GET_TOTALSALE_BY_DATE_RANGE } from "./types";
import config from 'react-global-configuration'

//Get Theatre Lists

export const getTotalSaleFilterThisMonth = token => dispatch => {
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };

  axios
    .get(
      `${config.get('apiDomain')}/api/v1/data_entry_sale/sale-data-filter-this-month`,
      header
    )
    .then(res => {
      dispatch({
        type: TOTAL_SALE_THIS_MONTH,
        payload: res.data
      });
    })
    .catch(err => console.log("The error in action is ", err));
};

export const getTotalSaleFilterByDateRange = (
  token,
  startDate,
  endDate
) => dispatch => {
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };

  axios
    .get(
      `${config.get('apiDomain')}/api/v1/data_entry_sale/sale-data-filter/${startDate}/${endDate}`,
      header
    )
    .then(res => {
      dispatch({
        type: TOTAL_SALE_THIS_MONTH,
        payload: res.data
      });
      alert(
        `Total amount is ${res.data.Total_amount} and Total Books are ${
          res.data.Total_Books
        }`
      );
    })
    .catch(err => {
      alert(`ERROR :${err.message}`);
    });
};
