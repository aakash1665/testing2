// import {LOGIN,SIGNUP,LOGINFAILS,LOGOUT,SIGNEDUP,SHOWLOADER,GETADDRESS,ADD_ADDRESS} from './types'
import {
    LOGINBACKDROP,
    LOGIN,
    SIGNUP,
    LOGINFAILS,
    LOGOUT,
    SIGNEDUP,
    SHOWLOADER,
    GETADDRESS,
    GETUSERD,
    EDITUSER_ADD,
    EDITADDRESS,
    ADD_ADDRESS,
    SAVEDINFO,
    GETSELECTEDADDRESS,
    CLEARALL,
    AFTERLOGINREDIRECT,
    CLEARAFTERLOGINREDIRECT,
    CLEARALLUSERSTATUS,
    CLEARLOGINERR,
    ACTIVATESUCCESSPOPUP,
    ACTIVATESUCCESSPOPUPOTHER,
    SETSELECTEDADDRESSBLANK,
    NAVURL
} from './types'
import axios from 'axios'
import config from 'react-global-configuration'


export const login_backdropToggle = () => dispatch => {
    // alert("logout")
    dispatch({
        type: LOGINBACKDROP,
        // payload:res.data
    })
};
export const LoginCheck = (details) => dispatch => {
    // console.log(details)
    // alert("login")
    axios.post(`${config.get('apiDomain')}/core/get_token/`, details
        // axios.post('http://103.217.220.149:80/core/get_token/',details  
    ).then(res => {
        if (res.status === 200) {
            // console.log(typeof(res.data));
            alert("Success")
            if (typeof (res.data) !== 'string') {
                dispatch({
                    type: LOGIN,
                    payload: res.data
                })
            } else {
            alert("Fail1")

                dispatch({
                    type: LOGINFAILS,
                    payload: res.data
                })
            }
        }
        // if(`Token` in res.data){
        //     alert("wrong")
        // }
    }).catch(err =>{
        console.log(err)
            alert("Fail2")

    })
};

export const signupCheck = (details) => dispatch => {
    // console.log(details)
    alert("signup")      
    axios.post(`${config.get('apiDomain')}/core/user_signup/`, details).then(res => {
            if (res.status === 200) {
                // console.log(res);

                dispatch({
                    type: SIGNUP,
                    payload: res.data
                })
            }

        })
        // .catch(err=>console.log(err));
        .catch(err => {
            // alert("err")
            if (err.response.status === 500) {
                // alert("E500")
                dispatch({
                    type: SIGNEDUP,
                    payload: "Email Already Exists Please Login ."
                })
            }
        })
};

export const logout = () => dispatch => {
    // alert("logout")
    dispatch({
        type: LOGOUT,
        // payload:res.data
    })
};

export const ActivteSuccesPopup = () => dispatch => {
    // alert("a")
    dispatch({
        type: ACTIVATESUCCESSPOPUP,
        // payload:res.data
    })
};
export const ActivteSuccesPopupOther = () => dispatch => {
    // alert("a")
    dispatch({
        type: ACTIVATESUCCESSPOPUPOTHER,
        // payload:res.data
    })
};

export const clearAll = () => dispatch => {
    // alert("logout")
    dispatch({
        type: CLEARALL,
        // payload:res.data
    })
};
export const clearLoginErr = () => dispatch => {
    // alert("clear")
    dispatch({
        type: CLEARLOGINERR,
        // payload:res.data
    })
};
export const clearAllUserStatus = () => dispatch => {
    // alert("logout")
    dispatch({
        type: CLEARALLUSERSTATUS,
        // payload:res.data
    })
};

// export const clearUserstatus = () =>  dispatch =>{
//     // alert("logout")
//         dispatch({
//             type:CLEARALLUserstatus,
//             // payload:res.data
//     })  
// };

export const getSavedToken = (info) => dispatch => {
    // alert(info)
    // console.log(`Token ${info}`);

    axios.get(`${config.get('apiDomain')}/core/user_details/`, {
        headers: {
            'Authorization': `Token ${info}`,
        }
    }).then(res => {
        if (res.data.details === "Invalid token.") {
            dispatch({
                type: SAVEDINFO,
                payload: info,
            })
        } else {
            dispatch({
                type: SAVEDINFO,
                payload: info,
            })
        }
    }).catch(err => console.log(err));
};

export const AfterLoginRedirect = (data) => dispatch => {
    // alert("logout")
    dispatch({
        type: AFTERLOGINREDIRECT,
        payload: data
    })
};
export const ClearAfterLoginRedirect = () => dispatch => {
    // alert("logout")
    dispatch({
        type: CLEARAFTERLOGINREDIRECT,
        // payload:data
    })
};
// export const userdetails = (details) =>  dispatch =>{
//     console.log(details)
//     // alert("okk")
//     axios.get(`${config.get('apiDomain')}/core/user_details/`,{headers:{
//                   'Authorization': details
//                 }} 
//     ).then(res=>{
//         dispatch({
//             type:GETUSERD,
//             payload:res.data
//     })
// })
// };
////
// export const userdetails = (details) =>  dispatch =>{
//     console.log(details)
//     // alert("okk")
//     axios.get(`${config.get('apiDomain')}/core/user_details/`,{headers:{
//                   'Authorization': details
//                 }} 
//     ).then(res=>{
//         dispatch({
//             type:GETUSERD,
//             payload:res.data
//     })
// })
// };


////
// axios.get(`${config.get('apiDomain')}/core/user_details/`,{headers:{
//     'Authorization': details
//   }} 
// ).then(res=>{
// dispatch({
// type:GETUSERD,
// payload:res.data
// })
// })
export const showLoader = () => dispatch => {
    // alert("logout")
    dispatch({
        type: SHOWLOADER,
        // payload:res.data
    })
};

// GET ADDRESS
export const Getaddress = (details) => dispatch => {
    // console.log(details)
    // alert("getaddress")
    axios.get(`${config.get('apiDomain')}/api/v1/get/user_address`, {
        headers: {
            // axios.get('http://103.217.220.149:80/api/v1/get/user_address',{headers:{
            'Authorization': details
        }
    }).then(res => {
        dispatch({
            type: GETADDRESS,
            payload: res.data
        })
    })
};

export const EdituserAddressAction = (token, address) => dispatch => {
    // console.log(address,token)
    // alert("addres")
    // axios.post('http://127.0.0.1:8000/donate-books/api/donateBook/',address  
    axios.post(`${config.get('apiDomain')}/api/v1/post/edit_address`, address, {
        headers: {
            'Authorization': token,
        }
    }).then(res => {
        dispatch({
            type: EDITUSER_ADD,
            payload: res.data
        })

    }).catch(err => console.log(err));
};

export const Editaddress = (data) => dispatch => {
    // alert("inEd")
    dispatch({
        type: EDITADDRESS,
        payload: data,
    })
}


// export const AddToCart = (cart) =>  dispatch =>{
//     console.log(cart)
//     alert("cartAcc")
// dispatch({
//     type:ADDTOCART,
//     payload:cart
// })
// };
export const userdetails = (details) => dispatch => {
    // console.log(details)
    // alert("okk")
    axios.get(`${config.get('apiDomain')}/core/user_details/`, {
        headers: {
            'Authorization': details
        }
    }).then(res => {
        dispatch({
            type: GETUSERD,
            payload: res.data
        })
    })
};

// ADD ADDRESS
export const addAddressAction = (token, address) => dispatch => {
    //     console.log(`${config.get('apiDomain')}/api/v1/post/address_form`,address,{headers:{'Authorization': token,
    // }})
    // alert("addres")
    // console.log(address);

    axios.post(`${config.get('apiDomain')}/api/v1/post/address_form`, address, {
        headers: {
            // axios.post('http://103.217.220.149:80/api/v1/post/address_form',address,{headers:{
            'Authorization': token,
        }
    }).then(res => {
        // alert("add")
        dispatch({
            type: ADD_ADDRESS,
            payload: res.data
        })

    }).catch(err => console.log(err, address));
};

export const SelectedAddress = (data) => dispatch => {
    // alert(data);

    dispatch({
        type: GETSELECTEDADDRESS,
        payload: data
    })
};

export const SetSelectedAddressBlank = () => dispatch => {
    // alert("logout")
    dispatch({
        type: SETSELECTEDADDRESSBLANK,
        // payload:res.data
    })
};
export const navurl = () => dispatch => {
    // alert("logout")
    dispatch({
        type: NAVURL,
        // payload:res.data
    })
};



// sudo chmod -R 777 /var/run/screen/

// ./typesense-server --data-dir=/var/www/webroot/ROOT/typesenseData/ --api-key=password    