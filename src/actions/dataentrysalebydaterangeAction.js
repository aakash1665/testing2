import axios from "axios";
import {
  GET_DATAENTRYSALE_BY_DATE_RANGE,
  GET_DATAENTRYSALE_THIS_MONTH
} from "./types";
import config from 'react-global-configuration'

export const getUploaderCount = (
  token,
  uploader,
  startDate,
  endDate
) => dispatch => {
  // console.log("In actions the data are ", token, uploader, startDate, endDate);

  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };

  axios
    .get(
      `${config.get('apiDomain')}/api/v1/data_entry_sale/count/${uploader}/${startDate}/${endDate}`,
      header
    )
    .then(res => {
      // console.log(res.data);
      dispatch({
        type: GET_DATAENTRYSALE_BY_DATE_RANGE,
        payload: res.data
      });
      // console.log("In action, the data are ", res.data);
      if (res.status == 200 && res.data.status == 200) {
        alert(
          `the Total no. of Entry By the Uploader with email id ${uploader} is ${
            res.data.count
          }`
        );
      } else if (res.status == 200 && res.data.status == 400) {
        alert(`this Uploader with email id ${uploader} doesn't exist`);
      }
    })
    .catch(err => {
      alert(`The ERROR : ${err.message}`);
      console.log(err);
    });
};

export const getUploaderCountThisMonth = (token, uploader) => dispatch => {
  // console.log("In action dataentry");
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };

  axios
    .get(
      `${config.get('apiDomain')}/api/v1/data_entry_sale/count-this-month/${uploader}`,
      header
    )
    .then(res => {
      console.log("In This Month Action ", res.data);
      dispatch({
        type: GET_DATAENTRYSALE_THIS_MONTH,
        payload: res.data
      });
      if (res.status == 200 && res.data.status == 200) {
        alert(
          `the Total no. of Entry By the Uploader with email id ${uploader} is ${
            res.data.count
          }`
        );
      } else if (res.status == 200 && res.data.status == 404) {
        alert(`this Uploader with email id ${uploader} doesn't exist`);
      }
    })
    .catch(err => {
      alert(`The ERROR : ${err.message}`);
    });
};
