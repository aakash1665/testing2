// import {LOGIN,SIGNUP,LOGINFAILS,LOGOUT,SIGNEDUP,SHOWLOADER,GETADDRESS,ADD_ADDRESS} from './types'
import { GETORDERLIST,
  UPDATEORDERBYID,
  ORDERLISTLOADING,
  FETCHSHIPPINGADDRESS,
  SHIPPINGADDRESSLOADING,
  UPDATESHIPPINGADDRESS,
  UPDATEORDERSTATUS,
  SERACHORDERLIST,
  FILTERORDER,
  CLEAR_ORDERLIST
} from "./types";
import axios from "axios";
import config from "react-global-configuration";

export const getOrdersList = ({ page, token, search, searchValue }) => dispatch => {

dispatch({
    type: ORDERLISTLOADING,
    payload: {  }
  });

let header = {
  headers: {
    Authorization: `Token ${token}`
  }
};

let URL = '';
if(searchValue) {
  URL = `${config.get("apiDomain")}/api/v1/fetch_order_details/fetch-order-details/${searchValue}`;

  axios
  .get(
  URL,
  header
  )
  .then(res => {
  if (res.status === 200) {
    dispatch({
      type: SERACHORDERLIST,
      payload: { data: res.data }
    });
  }
  })
  .catch(err => console.log(err));
} else {
  URL = `${config.get(
          "apiDomain"
        )}/api/v1/order_details_api/fetch-order/${page + 1}/`;

  axios
  .get(
    URL,
    header
  )
  .then(res => {
    if (res.status === 200) {

      if(search) {
        dispatch({
          type: SERACHORDERLIST,
          payload: { data: res.data }
        });
      } else {
        dispatch({
          type: GETORDERLIST,
          payload: { data: res.data }
        });
      }
    }
  })
  .catch(err => console.log(err));
}
};

export const updateOrder = (order, token) => dispatch => {
  console.log(order,"order");
  
  dispatch({
      type: ORDERLISTLOADING,
      payload: {  }
    });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  let body = order;
  axios
  .patch(
    `${config.get(
      "apiDomain"
    )}/api/v1/order_details_api/update-order/${order.order_id}`,
    body,
    header
  )
  .then(res => {
    if (res.data.status === 200) {
      dispatch({
        type: UPDATEORDERBYID,
        payload: {order}
      });
    }
  })
  .catch(err => console.log(err));
}

export const updateShippingAddress = (order, token) => dispatch => {

dispatch({
    type: SHIPPINGADDRESSLOADING,
    payload: {  }
  });

let header = {
    headers: {
      Authorization: `Token ${token}`
    }
};
let body = order;
axios
.patch(
  `${config.get(
    "apiDomain"
  )}/api/v1/order_details_api/update-address/${order.id}`,
  body,
  header
)
.then(res => {
  if (res.data.status === 200) {
    dispatch({
      type: UPDATESHIPPINGADDRESS,
      payload: { order }
    });
  }
})
.catch(err => console.log(err,"update"));
}

export const getShippingAddressInfo = (order_id, token) => dispatch => {

dispatch({
  type: SHIPPINGADDRESSLOADING,
  payload: {}
});

let header = {
  headers: {
    Authorization: `Token ${token}`
  }
};
axios
.get(
`${config.get(
  "apiDomain"
)}/api/v1/order_details_api/fetch-order-address-by-orderid/${order_id}/`,
header
)
.then(res => {
if (res.status === 200) {
  dispatch({
    type: FETCHSHIPPINGADDRESS,
    payload: { order_id, data: res.data.data }
  });
}
})
.catch(err => console.log(err));

}

export const getFilterOrders = ({token ,page,status,date})=>dispatch=>{

  dispatch({
    type: ORDERLISTLOADING,
    payload: {  }
  });
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  // {{ Nurl/api/v1/order_details_api/filter-fetch-order/1/0 }}
  console.log({token ,page,status});

  // let URL = `https://data.mypustak.com/api/v1/order_details_api/filter-fetch-order/${page}/${status}/${date}`;
  // axios.get(URL,header)
  // .then(res =>{
  //   if(res.status == 200){
  //     dispatch({
  //       type: FILTERORDER,
  //       payload: { data: res.data }
  //     })
  //   }
  // })
  // .catch(err=>{
  //   console.log(err,"getFilterOrders");
    
  // })


  axios
  .get(
  `${config.get("apiDomain")}/api/v1/order_details_api/filter-fetch-order/${page+1}/${status}/${date}`,
  header
  )
  .then(res => {
  if (res.status === 200) {
    if(page===0){
      dispatch({
        type: FILTERORDER,
        payload: { data: res.data }
      });
    }else{
      dispatch({
        type: GETORDERLIST,
        payload: { data: res.data }
      });
    }

    console.log(res.data,"filter");
    
  }
  })
  .catch(err => console.log(err));

}