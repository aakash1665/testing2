import {GETWALLETD,WALLETRECHARGE,SENDWALLETD_RECHARGEMAIL} from './types'
import axios from 'axios'
import config from 'react-global-configuration'

// GET WALLET DETAILS
export const Getwalletd = (details) =>  dispatch =>{
    // console.log(details)
    // alert("getwallet")
    axios.get(`${config.get('apiDomain')}/api/v1/get/user_walletbal`,{headers:{
                  'Authorization': details
                }} 
    ).then(res=>{
        dispatch({
            type:GETWALLETD,
            payload:res.data
    })
})
};

export const WalletRecarge = (data)=>dispatch =>{
    // alert("inEd")
    dispatch({
        type:WALLETRECHARGE,
        payload:data,
    })
}

export const SetWallet = (details) =>  dispatch =>{
    // console.log(details)
    // alert("getwallet")
    axios.get(`${config.get('apiDomain')}/api/v1/calculate_wallet_balance/calculate-wallet-balance`,{headers:{
                  'Authorization': details
                }} 
    ).then(res=>{
        console.log("Setting Wallet");
        
        axios.get(`${config.get('apiDomain')}/api/v1/get/user_walletbal`,{headers:{
            'Authorization': details
          }} 
            ).then(res=>{
        console.log("Gettng Wallet");

                dispatch({
                    type:GETWALLETD,
                    payload:res.data
                })
            })
            .catch(err=>console.log(err))
}).catch(err=>console.log(err)
)
};

export const SendWalletRechargeMail = (token,sendData) =>  dispatch =>{
    console.log("Recharge mail send",token)
    // alert("getwallet")
    let header = {
        headers: {
          Authorization: token
        }
    };
    axios.post(`${config.get('apiDomain')}/common/walletRechargeEmail/`,sendData,header 
    ).then(res=>{
        console.log(res.data);
        
        dispatch({
            type:SENDWALLETD_RECHARGEMAIL,
            payload:{}
    })
}).catch(err=>{console.log(err);
})
};