import {PASSDETAILS} from './types'
import axios from 'axios'
import config from 'react-global-configuration'
// GET WALLET DETAILS
export const passbookd = (details) =>  dispatch =>{
    // console.log(details)
    // alert("getwallet")
    axios.get(`${config.get('apiDomain')}/api/v1/wallet-recharge-withdrawal/fetch-wallet`,{headers:{
                  'Authorization': details
                }} 
    ).then(res=>{
        dispatch({
            type:PASSDETAILS,
            payload:res.data
    })
    
})
.catch(err=>{console.log(err)})
};
