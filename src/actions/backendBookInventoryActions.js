import {GETBOOKINVENTORYLIST, BOOKINVENTORYLISTLOADING, FETCHINVENTORY, CHANGECURRENTINVENTORY, UPDATEBOOKINVENTORY, DELETEBOOKINVENTORY,
  FETCHBOOKBYID, UPDATEBOOK, DELETEBOOK,
  SEARCH_IN_BOOKINVENTORY,
  DISCARD_BOOK

} from './types'
import axios from "axios";
import config from "react-global-configuration";



export const getBooksSearchResult = (token, id) => dispatch => {
  let result = [];
  // alert(`${id}`);
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  //localhost:8000/api/v1/donation-tracking/track-request-id/123456

  http: axios
    .get(`${config.get(
      "apiDomain"
    )}/api/v1/booksearch/search/${id}`, header)
    .then(res => {
      // alert("In then action");
      dispatch({
        type: SEARCH_IN_BOOKINVENTORY,
        payload: res.data.book_details
      });
      console.log("In BookSearch action, the data are ", res.data);
    })
    .catch(err => {
      dispatch({
        type: SEARCH_IN_BOOKINVENTORY,
        payload: result
      });
      alert(`No books exist with this #${id} parameter`);
      console.log(err);
    });
};

export const getBookInventoryList = ({ page, token }) => dispatch => {

  console.log(page, "asdfasdfbook")
    dispatch({
        type: BOOKINVENTORYLISTLOADING,
        payload: {  }
      });

  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  axios
    .get(
      `${config.get(
        "apiDomain"
      )}/api/v1/book-data-from-latest-entry/fetch-book-data/${page + 1}/`,
      header
    )
    .then(res => {
      if (res.status === 200) {
          console.log(res.data, "resp")
        dispatch({
          type: GETBOOKINVENTORYLIST,
          payload: { data: res.data }
        });
      }
    })
    .catch(err => console.log(err));
};

export const fetchBookByBookId = ({ book_id, token }) => dispatch => {

  dispatch({
      type: BOOKINVENTORYLISTLOADING,
      payload: { }
    });

let header = {
  headers: {
    Authorization: `Token ${token}`
  }
};
axios
  .get(
    `${config.get(
      "apiDomain"
    )}/api/v1/book-data-from-latest-entry/fetch-book-by-id/${book_id}/`,
    header
  )
  .then(res => {
    if (res.status === 200) {
        console.log(res.data, "resp")
      dispatch({
        type: FETCHBOOKBYID,
        payload: { data: res.data.data }
      });
    }
  })
  .catch(err => console.log(err));
};

export const updateBook = ({ book, token }) => dispatch => {

    dispatch({
        type: BOOKINVENTORYLISTLOADING,
        payload: {  }
      });

    let header = {
        headers: {
          Authorization: `Token ${token}`
        }
    };
    let body = book
    axios
    .patch(
      `${config.get(
        "apiDomain"
      )}/api/v1/book-data-from-latest-entry/update-book/${book.book_id}`,
      body,
      header
    )
    .then(res => {
      if (res.data.status === 200) {
        dispatch({
          type: UPDATEBOOK,
          payload: { book }
        });
      }
    })
    .catch(err => console.log(err));
}

export const deleteBook = ({ book_id, page, token }) => dispatch => {

  dispatch({
      type: BOOKINVENTORYLISTLOADING,
      payload: {  }
    });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  axios
  .delete(
    `${config.get("apiDomain")}/api/v1/book-data-from-latest-entry/delete-book/${book_id}`,
    header
  )
  .then(res => {
    if (res.data.status === 200) {
      dispatch({
        type: DELETEBOOK,
        payload: { book_id }
      });
    }
  })
  .catch(err => console.log(err));
}

export const updateBookInventory = ({ bookInventory, token }) => dispatch => {

  console.log(bookInventory)

    dispatch({
        type: BOOKINVENTORYLISTLOADING,
        payload: {  }
      });

    let header = {
        headers: {
          Authorization: `Token ${token}`
        }
    };
    let body = bookInventory
    axios
    .patch(
      `${config.get(
        "apiDomain"
      )}/api/v1/book-data-from-latest-entry/update-book-inventory/${bookInventory.book_inv_id}`,
      body,
      header
    )
    .then(res => {
      if (res.data.status === 200) {
        dispatch({
          type: UPDATEBOOKINVENTORY,
          payload: { bookInventory }
        });
      }
    })
    .catch(err => console.log(err));
}

export const getBookInventoryByBookId = ({ book_id, token }) => dispatch => {

  dispatch({
      type: BOOKINVENTORYLISTLOADING,
      payload: { }
    });

let header = {
  headers: {
    Authorization: `Token ${token}`
  }
};
axios
  .get(
    `${config.get(
      "apiDomain"
    )}/api/v1/book-data-from-latest-entry/fetch-book-inventory-by-id/${book_id}/`,
    header
  )
  .then(res => {
    if (res.status === 200) {
        console.log(res.data, "resp")
      dispatch({
        type: FETCHINVENTORY,
        payload: { data: res.data }
      });
    }
  })
  .catch(err => console.log(err));
};

export const changeCurrentInventory = index => dispatch => {
  dispatch({
    type: CHANGECURRENTINVENTORY,
    payload: { index }
  });
}

export const deleteBookInventory = ({ book_inv_id, token }) => dispatch => {

  dispatch({
      type: BOOKINVENTORYLISTLOADING,
      payload: {  }
    });

  let header = {
      headers: {
        Authorization: `Token ${token}`
      }
  };
  axios
  .delete(
    `${config.get("apiDomain")}/api/v1/book-data-from-latest-entry/delete-book-inventory/${book_inv_id}`,
    header
  )
  .then(res => {
    if (res.data.status === 200) {
      dispatch({
        type: DELETEBOOKINVENTORY,
        payload: { book_inv_id }
      });
    }
  })
  .catch(err => console.log(err));
}

export const Discardbook = ({book_id, book_inv_id,token}) => dispatch =>{
// console.log(token);
  dispatch({
    type:DISCARD_BOOK,
    payload: false
  })
  let header = {
    headers: {
      Authorization: token
    }
  };

  axios.post(
    `${config.get("apiDomain")}/api/v1/restock_return_order/discardbook/${book_inv_id}/${book_id}`,null,header,
  ).then(res =>{
    dispatch({
      type:DISCARD_BOOK,
      payload: true
    })
    console.log(res,"Book Discarded");
    
    alert("Book Discarded")

  })
  .catch(err=>console.log(err)
  )

}