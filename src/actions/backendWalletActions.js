import {GETWALLETLIST, WALLETLISTLOADING, ADDWALLET} from './types'
import axios from "axios";
import config from "react-global-configuration";

export const getWalletList = ({ page, token, search, searchWallet }) => dispatch => {

    dispatch({
        type: WALLETLISTLOADING,
        payload: {  }
      });

  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };
  axios
    .get(
      `${config.get(
        "apiDomain"
      )}/api/v1/wallet-transaction/fetch-wallet/${page + 1}/`,
      header
    )
    .then(res => {
      if (res.status === 200) {
          console.log(res.data, "resp")
        dispatch({
          type: GETWALLETLIST,
          payload: { data: res.data }
        });
      }
    })
    .catch(err => console.log(err));
};

export const addWallet = ({ wallet, token }) => dispatch => {

    dispatch({
        type: WALLETLISTLOADING,
        payload: {  }
      });

    let header = {
        headers: {
          Authorization: `Token ${token}`
        }
    };
    let body = wallet
    axios
    .post(
      `${config.get(
        "apiDomain"
      )}/api/v1/wallet-transaction/add-wallet`,
      body,
      header
    )
    .then(res => {
      if (res.data.status === 200) {
        dispatch({
          type: ADDWALLET,
          payload: { wallet }
        });
      }
    })
    .catch(err => console.log(err));
}