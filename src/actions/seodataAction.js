import axios from "axios";
import { SEO_GET_DATA } from "./types";
import config from 'react-global-configuration'

//Get Theatre Lists

export const getSeoData = url => dispatch => {
  console.log(url,"passurl");
  const body ={
    "url":url
    }
  axios
    .post(`${config.get('apiDomain')}/api/v1/seo_tags/seo-data`,body)

    .then(res => {
      //   alert(`The seo Data are ${res.data.url}`);
      dispatch({
        type: SEO_GET_DATA,
        payload: res.data
      });
    })
    .catch(err => console.log("The error in action is ", err));
};
