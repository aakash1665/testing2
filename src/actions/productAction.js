import { SETNEWPRICICNG
 } from "./types";
 import axios from "axios";
import config from "react-global-configuration";

export const setNewPricing = ({  data }) => dispatch => {
  console.log("New Pricing 1");
    let body = {
        data: data
    };
    axios
    .post(
      `${config.get(
        "apiDomain"
      )}/api/v1/new-pricing-model`,
      body,
    )
    .then(res => {
      console.log("New Pricing");
      if (res.data.status === 200) {
       
        
        dispatch({
          type: SETNEWPRICICNG,
          payload: {  }
        });
      }
    })
    .catch(err => console.log(err));
};