import axios from "axios";
import { FILTER_ALL_BOOKS_AGAINST_RACK_NO } from "./types";
import config from "react-global-configuration";
//Get Theatre Lists

export const getRackFilterResult = (rackNo, token) => dispatch => {
  let header = {
    headers: {
      Authorization: `Token ${token}`
    }
  };

  let data = {
    data: null,
    total_count: 0
  };

  axios
    .get(
      `${config.get("apiDomain")}/api/v1/get_rackNo/book-filter-rack/${rackNo}`,
      header
    )
    .then(res => {
      //   alert(`The seo Data are ${res.data.url}`);
      // console.log("The Rack Filtered Data are ", res.data);
      dispatch({
        type: FILTER_ALL_BOOKS_AGAINST_RACK_NO,
        payload: res.data
      });
    })
    .catch(err => {
      console.log("The error in action is ", err);
      if (err.response) {
        alert(`${err.response.data.message}`);
      } else {
        alert(`Can't connect to the server`);
      }
    });
  // dispatch({
  //   type: FILTER_ALL_BOOKS_AGAINST_RACK_NO,
  //   payload: data
  // });
};
