import {
  GET_COMPEXAM,
  GET_FRICTION,
  GET_SCHOOLBOOK,
  GET_BOOK,
  GET_UNIVERSITYBOOK,
  ONCLICK_UNIV_BOOKS_SCROLLING,
  ONCLICK_COMPEXAMS_BOOKS_SCROLLING,
  ONCLICK_SCHOOL_BOOKS_SCROLLING,
  ONCLICK_FRICT_BOOKS_SCROLLING
} from "./types";
import axios from "axios";
import config from "react-global-configuration";
// api/v1/get/category/ competitive-exams
// api/v1/get/category/ competitive-exams/cat
// api/v1/get/category/ competitive-exams/government-jobs
// api/v1/get/category/ competitive-exams/engineering
// api/v1/get/category/ competitive-exams/general-knowledge-learni
// APi hit to get Compititive Exam Books

export const getOnclickCompExam = url => async dispatch => {
  const new_url = `${config.get("apiDomain")}/api/v1/get/category${url}/1/`;
  // console.log(new_url)
  await axios
    .get(new_url)
    .then(res => {
      dispatch({
        type: ONCLICK_COMPEXAMS_BOOKS_SCROLLING,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};

export const getOnclickFrictBooks = url => async dispatch => {
  const new_url = `${config.get("apiDomain")}/api/v1/get/category/fiction-non-fiction${url}/1/`;
  // console.log(new_url)
  await axios
    .get(new_url)
    .then(res => {
      dispatch({
        type: ONCLICK_FRICT_BOOKS_SCROLLING,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};

export const getOnclickSchoolBooks = url => async dispatch => {
  const new_url = `${config.get("apiDomain")}/api/v1/get/category/school-children-books${url}/1/`;
  // console.log(new_url)
  await axios
    .get(new_url)
    .then(res => {
      dispatch({
        type: ONCLICK_SCHOOL_BOOKS_SCROLLING,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};

export const getOnclickUnivBooks = url => async dispatch => {
  const new_url = `${config.get("apiDomain")}/api/v1/get/category/university-books${url}/1/`;
  // console.log(new_url)
  await axios
    .get(new_url)
    .then(res => {
      dispatch({
        type: ONCLICK_UNIV_BOOKS_SCROLLING,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};

export const getCompExam = (url, page) => async dispatch => {
  // alert(url)
  // console.log(`${page}`);

  const new_url = `${config.get("apiDomain")}/api/v1/get/category${url}/${page}/`;
  // console.log(new_url)
  await axios
    .get(new_url)
    .then(res => {
      dispatch({
        type: GET_COMPEXAM,
        payload: res.data
      });
    })
    .catch(err => console.log(err));
};

// axios.post('http://103.217.220.149:80/customer/customer_address',address
//     ).then(res=>{
//         dispatch({
//             type:GET_ADDRESS,
//             payload:res.data
//     })

//     }).catch(err=>console.log(err));
// };
// APi hit to get Friction Books
export const getFriction = (url, page) => async dispatch => {
  // alert('okkfri')
  const new_url = `${config.get("apiDomain")}/api/v1/get/category/fiction-non-fiction${url}/${page}/`;
  const res = await axios.get(new_url);
  dispatch({
    type: GET_FRICTION,
    payload: res.data
  });
};
// APi hit to get school-children-books
export const getSchoolbooksBooks = (url, page) => async dispatch => {
  // alert('okkfri')
  const new_url = `${config.get("apiDomain")}/api/v1/get/category/school-children-books${url}/${page}/`;
  const res = await axios.get(new_url);
  dispatch({
    type: GET_SCHOOLBOOK,
    payload: res.data
  });
};
// APi hit to get Friction Books
export const getUniversityBooks = (url, page) => async dispatch => {
  // alert('okkfri')
  const new_url = `${config.get("apiDomain")}/api/v1/get/category/university-books${url}/${page}/`;
  const res = await axios.get(new_url);
  dispatch({
    type: GET_UNIVERSITYBOOK,
    payload: res.data
  });
};
export const getBook = slug => async dispatch => {
  // const res = await axios.get('http://127.0.0.1:8000/product/pioneer-exercise-bookpack-of-5-note-books-')
  const res = await axios.get(
    `${config.get("apiDomain")}/api/v1/get/product/${slug}`
  );
  dispatch({
    type: GET_BOOK,
    payload: res.data
  });
};
// ${config.get('apiDomain')}
// export const AddDonation = (donation) =>  dispatch =>{
//     console.log(donation)
//     axios.post('http://127.0.0.1:8000/donate-books/api/donateBook/',donation
//     ).then(res=>{
//         dispatch({
//             type:ADD_DONATION,
//             payload:res.data
//     })

//     }).catch(err=>console.log(err));
// };
