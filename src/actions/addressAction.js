// import {GET_ADDRESS,ADD_ADDRESS} from './types'
// import axios from 'axios'
// export const GetAddressAction = (address) => async dispatch =>{
//     console.log(address)
//     // alert("addres")
//     // axios.post('http://127.0.0.1:8000/donate-books/api/donateBook/',address  
//     axios.post('http://127.0.0.1:8000/customer/customer_address',address  
//     ).then(res=>{
//         dispatch({
//             type:GET_ADDRESS,
//             payload:res.data
//     })
        
//     }).catch(err=>console.log(err));
// };

// export const addAddressAction = (address) => async dispatch =>{
//     console.log(address)
//     // alert("addres")
//     // axios.post('http://127.0.0.1:8000/donate-books/api/donateBook/',address  
//     axios.post('http://127.0.0.1:8000/api/v1/post/address_form',address  
//     ).then(res=>{
//         dispatch({
//             type:ADD_ADDRESS,
//             payload:res.data
//     })
        
//     }).catch(err=>console.log(err));
// };

import {GET_ADDRESS} from './types'
import axios from 'axios'
import config from 'react-global-configuration'
export const GetAddressAction = (address) => async dispatch =>{
    // console.log(address)
    // alert("addres")
    // axios.post('http://127.0.0.1:8000/donate-books/api/donateBook/',address  
    axios.post(`${config.get('apiDomain')}/customer/customer_address`,address  
    ).then(res=>{
        dispatch({
            type:GET_ADDRESS,
            payload:res.data
    })
        
    }).catch(err=>console.log(err));
};