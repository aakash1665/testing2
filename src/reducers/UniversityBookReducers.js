import {GET_SCHOOLBOOK,GET_BOOK,GET_UNIVERSITYBOOK} from '../actions/types'

const initialState = {
    universityBooks:[],
    // FrictionBooks:[],
    book:[],
};
export default function(state=initialState,action)
{
    switch(action.type){
        case GET_UNIVERSITYBOOK:
        // alert('redu')
            return {
                ...state,
                universityBooks:action.payload
            }
        // case GET_FRICTION:
        // // alert('redu')
        //     return {
        //         ...state,
        //         FrictionBooks:action.payload
        //     }
        case GET_BOOK:
        // alert('Get')
            return {
                ...state,
                book:action.payload
                }
        default:
        return state;
    }
}
