import {GET_SCHOOLBOOK,GET_BOOK} from '../actions/types'

const initialState = {
    schoolBooks:[],
    // FrictionBooks:[],
    book:[],
};
export default function(state=initialState,action)
{
    switch(action.type){
        case GET_SCHOOLBOOK:
        // alert('redu')
            return {
                ...state,
                schoolBooks:action.payload
            }
        // case GET_FRICTION:
        // // alert('redu')
        //     return {
        //         ...state,
        //         FrictionBooks:action.payload
        //     }
        case GET_BOOK:
        // alert('Get')
            return {
                ...state,
                book:action.payload
                }
        default:
        return state;
    }
}
