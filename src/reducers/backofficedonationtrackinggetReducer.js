import { GET_TRACKINGNO_AGAINST_DONATIONID } from "../actions/types";

const initialState = {
  donationTracking: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TRACKINGNO_AGAINST_DONATIONID:
      return {
        ...state,
        donationTracking: action.payload
      };
    default:
      return state;
  }
}
