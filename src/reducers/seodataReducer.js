import { SEO_GET_DATA } from "../actions/types";

const initialState = {
  seodata: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SEO_GET_DATA:
      return {
        ...state,
        seodata: action.payload
      };

    default:
      return state;
  }
}
