// import { LOGIN,SIGNUP,SIGNEDUP ,LOGINFAILS,LOGOUT,SHOWLOADER,GETADDRESS,ADD_ADDRESS } from '../actions/types';
import {GETWALLETLIST, WALLETLISTLOADING, ADDWALLET} from "../actions/types";
const initialState = {
  walletList: [],
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GETWALLETLIST:
      return {
        ...state,
        walletList: [...state.walletList, ...action.payload.data],
        loading: false
      };
    case WALLETLISTLOADING: 
      return {
        ...state,
        loading: true,
      }
    case ADDWALLET: 
      let walletList = state.walletList;
      console.log(action.payload.wallet, "wealletttt")
      return {
          ...state,
          walletList: [ action.payload.wallet, ...state.walletList ],
          loading: false,
      }
    default:
      return state;
  }
}
