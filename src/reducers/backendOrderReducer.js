// import { LOGIN,SIGNUP,SIGNEDUP ,LOGINFAILS,LOGOUT,SHOWLOADER,GETADDRESS,ADD_ADDRESS } from '../actions/types';
import { GETORDERLIST, 
  UPDATEORDERBYID,
   ORDERLISTLOADING,
   FETCHSHIPPINGADDRESS,
   SHIPPINGADDRESSLOADING,
   UPDATESHIPPINGADDRESS,
   SERACHORDERLIST,
   CLEAR_ORDERLIST,
  FILTERORDER } from "../actions/types";
const initialState = {
  orderList: [],
  loading: false,
  shippingAddress: {},
  shippingAddressLoading: false,
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GETORDERLIST:
    console.log("GETORFDR");
    
      return {
        ...state,
        orderList: [...state.orderList, ...action.payload.data],
        loading: false
      };
    case SERACHORDERLIST:
      return {
        ...state,
        orderList: action.payload.data,
        loading: false
      }
    case ORDERLISTLOADING: 
      return {
        ...state,
        loading: true,
      }
    case UPDATEORDERBYID:
      let orderList = state.orderList;
      let updatedOrderList = orderList.map((order) => {
          if(order.order_id === action.payload.order.order_id) {
            Object.assign(order, action.payload.order)
          }
          return order;
      })
      return {
          ...state,
          orderList: updatedOrderList,
          loading: false,
      }
    case FETCHSHIPPINGADDRESS:
      return {
        ...state,
        shippingAddress: action.payload.data,
        shippingAddressLoading: false,
      }
    case SHIPPINGADDRESSLOADING:
      return {
        ...state,
        shippingAddressLoading: true
      }
    
    case UPDATESHIPPINGADDRESS:
      return {
        ...state,
        shippingAddressLoading: false,
      }
      case CLEAR_ORDERLIST:
      return{
        ...state,
        orderList: ""
      }
      case FILTERORDER:
      console.log(action.payload.data,"ReducerFilter");
      
      return {
        ...state,
        orderList: action.payload.data,
        loading: false
        
      }
    default:
      return state;
  }
}
