import {
  GET_ORDERDETAILS_OF_EVERY_ORDER,
  RESTOCKING_SINGAL_BOOK
} from "../actions/types";

const initialState = {
  orderdetails: {},
  restock: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_ORDERDETAILS_OF_EVERY_ORDER:
      return {
        ...state,
        orderdetails: action.payload
      };
    case RESTOCKING_SINGAL_BOOK:
      return {
        ...state,
        restock: action.payload
      };

    default:
      return state;
  }
}
