import {
  GET_DATAENTRYSALE_BY_DATE_RANGE,
  GET_DATAENTRYSALE_THIS_MONTH
} from "../actions/types";

const initialState = {
  uploader: {},
  uploaderthismonth: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_DATAENTRYSALE_BY_DATE_RANGE:
      return {
        ...state,
        uploader: action.payload
      };

    case GET_DATAENTRYSALE_THIS_MONTH:
      return {
        ...state,
        uploaderthismonth: action.payload
      };
    default:
      return state;
  }
}
