// import { LOGIN,SIGNUP,SIGNEDUP ,LOGINFAILS,LOGOUT,SHOWLOADER,GETADDRESS,ADD_ADDRESS } from '../actions/types';
import {GETBOOKINVENTORYLIST, BOOKINVENTORYLISTLOADING, UPDATEBOOKINVENTORY, FETCHINVENTORY, CHANGECURRENTINVENTORY, DELETEBOOKINVENTORY,
  FETCHBOOKBYID, UPDATEBOOK, DELETEBOOK,SEARCH_IN_BOOKINVENTORY,DISCARD_BOOK} from "../actions/types";
const initialState = {
  bookinventoryList: [],
  inventoriesByBookId: [],
  currentInventory: {},
  currentBook: {},
  searchbookresults: {},
  BookDiscarded:false,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SEARCH_IN_BOOKINVENTORY:
    return {
      ...state,
      searchbookresults: action.payload
    };

    case GETBOOKINVENTORYLIST:
      return {
        ...state,
        bookinventoryList: [...state.bookinventoryList, ...action.payload.data],
        loading: false
      };
    case BOOKINVENTORYLISTLOADING: 
      return {
        ...state,
        loading: true,
      }
    case FETCHBOOKBYID : 
      return {
        ...state,
        currentBook: action.payload.data,
        loading: false,
      }
    case UPDATEBOOK : 
      let updatedBookList = state.bookinventoryList.map((book) => {
        if(book.book_id === action.payload.book.book_id) {
            Object.assign(book, action.payload.book)
          }
          return book;
      })
      return {
          ...state,
          bookinventoryList: updatedBookList,
          loading: false,
      }
    case DELETEBOOK : 
      let updatedBooksByBookId = state.bookinventoryList.filter(book => book.book_id !== action.payload.book_id) ;
      return {
        ...state,
        bookinventoryList: updatedBooksByBookId,
        currentBook: {},
        loading: false
      }
    case FETCHINVENTORY:
      return {
        ...state,
        inventoriesByBookId: action.payload.data,
        currentInventory: action.payload.data.length && action.payload.data[0],
        loading: false,
      }
    case UPDATEBOOKINVENTORY:
      let bookinventoryList = state.inventoriesByBookId;
      let currentInventory = {};
      let updatedBookInventoryList = bookinventoryList.map((bookInventory) => {
        console.log(bookInventory, "asdfasd", action.payload.bookInventory)
        if(bookInventory.book_inv_id === action.payload.bookInventory.book_inv_id) {
          bookInventory = Object.assign(bookInventory, action.payload.bookInventory);
          console.log(bookInventory, "bookINve")
          currentInventory = bookInventory;
          }
        return bookInventory;
      })
      return {
          ...state,
          inventoriesByBookId: updatedBookInventoryList,
          currentInventory,
          loading: false,
      }
    
    case CHANGECURRENTINVENTORY:
      return {
        ...state,
        currentInventory: state.inventoriesByBookId.length && state.inventoriesByBookId[action.payload.index],
      }
    case DELETEBOOKINVENTORY: 
      let updatedInventoriesByBookId = state.inventoriesByBookId.filter(inventory => inventory.book_inv_id !== action.payload.book_inv_id) ;
      return {
        ...state,
        inventoriesByBookId: updatedInventoriesByBookId,
        currentInventory: updatedInventoriesByBookId.length && updatedInventoriesByBookId[0],
        loading: false
      }
      case DISCARD_BOOK: 
      console.log(action.payload,"payload");
      
      return {
        ...state,
        BookDiscarded: action.payload,
      }
    default:
      return state;
  }
}
