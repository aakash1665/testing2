import {combineReducers} from 'redux';
import addressReducers from '../reducers/addressReducers'
import compExamReducers from './compExamReducers'
 import donationReducer from './donationReducer'
import accountReducer from './accountReducer'
import homeReducers from './homeReducers';
import frictionReducers from './frictionReducers';
import schoolBookReducers from './schoolBookReducers';
import UniversityBookReducers from './UniversityBookReducers'
import CartReducers from './cartReducers'
import walletReducer from './walletReducer'
import orderReducer from './orderReducer'
import wishlistReducer from './wishlistReducer'
import FaqReducer from './faqReducer'
import ProuddonarReducer from './ProuddonarReducer'
import PassbookReducer from './passbookReducer'

// BR***************
import donationBReducer from "./backendDonationReducer";
import orderBReducer from './backendOrderReducer';
import walletBReducer from "./backendWalletReducer";
import bookInventoryBReducer from "./backendBookInventoryReducer";
import confirmOrderBReducer from './backendConfirmOrderReducer';
import totalsalefilterReducers from "./totalsalefilterReducers";
import backofficeWalletUpdateReducers from "./backofficeWalletUpdateReducers";
import uploader from "./dataentrysalebydaterangeReducers";
import donationTracking from "./backofficedonationtrackinggetReducer";
import orderdetails from "./backofficeorderdetailsReducer";
import seodataReducer from "./seodataReducer";
import rackDataFilter from "./backofficeRackNoFilterReducer"; 
import backofficeGoogleReducer from "./backofficeGoogleReducer"

export default combineReducers({ 
    compExam: compExamReducers, ///contact will be passed in props -->this.props.contact(state object name)
    addAddress:addressReducers,
    donationR: donationReducer, ///contact will be passed in props -->this.props.contact(state object name)
    accountR: accountReducer,
    homeR: homeReducers,
    friction:frictionReducers,
    schoolbooks:schoolBookReducers,
    universitybooks:UniversityBookReducers,
    cartReduc:CartReducers,
    getAddressR:accountReducer,
    walletR:walletReducer,
    userdetailsR:accountReducer,
    orderdetailsR:orderReducer,
    wishlistR:wishlistReducer,
    faqR:FaqReducer,
    ProuddonarR:ProuddonarReducer,
    PassbookR:PassbookReducer,
    donationBR: donationBReducer,
    orderBR: orderBReducer,
    walletBR: walletBReducer,
    bookInventoryBR: bookInventoryBReducer,
    confirmOrderBR: confirmOrderBReducer,
    totalsalefilter: totalsalefilterReducers,
    userdetails: backofficeWalletUpdateReducers,
    uploader: uploader,
    donationTracking: donationTracking,
    orderdetails: orderdetails,
    seodata: seodataReducer,
    rackData: rackDataFilter,
    googleReducer: backofficeGoogleReducer,
 });    
   


//  import donationReducer from './donationReducer';
// import accountReducer from './accountReducer'
// export default combineReducers({ 
//     donationR: donationReducer, ///contact will be passed in props -->this.props.contact(state object name)
//     accountR: accountReducer,
//  });