// import { LOGIN,SIGNUP,SIGNEDUP ,LOGINFAILS,LOGOUT,SHOWLOADER,GETADDRESS,ADD_ADDRESS } from '../actions/types';
import { GETDONATIONLIST, UPDATEDONATIONBYID, DONATIONLISTLOADING,
  FETCHDONATIONBYID,
  UPDATEDONATION,
  SERACHDONATIONLIST } from "../actions/types";
const initialState = {
  donationList: [],
  currentDonation: {},
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GETDONATIONLIST:
      return {
        ...state,
        donationList: [...state.donationList, ...action.payload.data],
        loading: false
      };
    case SERACHDONATIONLIST:
    console.log(action.payload.data, "dafa;sldkfjasdfl;kajs;")
      return {
        ...state,
        donationList: [...action.payload.data],
        loading: false
      }
    case DONATIONLISTLOADING: 
      return {
        ...state,
        loading: true,
      }
    case UPDATEDONATIONBYID: 
      let donationList = state.donationList;
      let updatedDonationList = donationList.map((donation) => {
          if(donation.donation_req_id === action.payload.donation_req_id) {
              donation.status = action.payload.new_status              
            }
          return donation;
      })
      return {
          ...state,
          donationList: updatedDonationList,
          loading: false,
      }
    case FETCHDONATIONBYID:
      return {
        ...state,
        currentDonation: action.payload.data,
        loading: false
      }
    case UPDATEDONATION:
      return {
          ...state,
          currentDonation: action.payload.data,
          loading: false,
      }
    default:
      return state;
  }
}
