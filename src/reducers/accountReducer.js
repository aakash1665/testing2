// import { LOGIN,SIGNUP,SIGNEDUP ,LOGINFAILS,LOGOUT,SHOWLOADER,GETADDRESS,ADD_ADDRESS } from '../actions/types';
import { LOGIN,SIGNUP,SIGNEDUP ,LOGINFAILS,LOGOUT,SHOWLOADER,GETADDRESS,GETUSERD,CLEARAFTERLOGINREDIRECT,AFTERLOGINREDIRECT,
    EDITUSER_ADD,EDITADDRESS,ADD_ADDRESS,SAVEDINFO,GETSELECTEDADDRESS,CLEARALL,CLEARALLUSERSTATUS,
    CLEARLOGINERR,ACTIVATESUCCESSPOPUP,ACTIVATESUCCESSPOPUPOTHER,SETSELECTEDADDRESSBLANK,NAVURL,LOGINBACKDROP} from '../actions/types';
const initialState = {
    LoginBackdrop:false,
    getuserdetails:[],
    getadd:[],
    getuseradd:{},
    editadd:{},
    userId:undefined,
    status:'',
    token:null,
    // userId:6059,
    acount:[],
    selectedAddress:[],
    ErrMsg:[],
    loading:false,
    // getadd:[],
    SignUpErrMsg:"",
    ActivateSuccPopupState:false,
    ActivateSuccPopupOtherState:false,
    navurlstate:false,
    afterLoginSuccess:'',
    user_role_id:0,
    // added_address:'',
    // donation_req_id:'123456',
    // app_books_weight:'',
    // email:'mukul.meri@gmail.com',
    // name:'abc xyz'
};

export default function(state=initialState,action)
{

    switch(action.type){ 
        case LOGINBACKDROP:
                return{
                    ...state,
                    LoginBackdrop:!state.LoginBackdrop,
                }
        case SHOWLOADER:
            return{
                ...state,
                loading:true,
            }
        case LOGIN:
            // alert("loginSuccess") 
            if(action.payload.Token === null){
                return {
                    ...state,
                ErrMsg:["Invalid Login details.Are you trying to Sign up?"],
                loading: false
                        }
            }else{    
                // console.log(action.payload.Token)
                return{
                    ...state,
                    token:[action.payload.Token],
                    user_role_id:action.payload.user_role_id,
                    loading: false,
                    LoginBackdrop:false,

                    
                }}
        case SAVEDINFO:
            // alert("logoutSuccess")           
            // console.log(action.payload.Token)
            return{
                ...state,
                token:action.payload,
                // loading: false
                
            }
        case CLEARALL:
            // alert("logoutSuccess")           
            // console.log(action.payload.Token)
            return{
                ...state,
                SignUpErrMsg:"",
                
            }
    case CLEARLOGINERR:
            // alert("CL")           
            // console.log(action.payload.Token)
            return{
                ...state,
                ErrMsg:"",
                
            }
        case CLEARALLUSERSTATUS:
            // alert("logoutSuccess")           
            // console.log(action.payload.Token)
            return{
                ...state,
                status:"",
                
            }
        case LOGOUT:
            // alert("logoutSuccess")           
            // console.log(action.payload.Token)
            return{
                ...state,
                token:null,
                loading: false,
                getuserdetails:[],
                getuseradd:{},
                
            }
            case NAVURL:
            // alert("logoutSuccess")           
            // console.log(action.payload.Token)
            return{
                ...state,
                navurlstate: !state.navurl
                
            }
            case AFTERLOGINREDIRECT:
            // alert("logoutSuccess")           
            // console.log(action.payload.Token)
            return{
                ...state,
                afterLoginSuccess: action.payload,
                
            }
            case CLEARAFTERLOGINREDIRECT:
            // alert("logoutSuccess")           
            // console.log(action.payload.Token)
            return{
                ...state,
                afterLoginSuccess: '',
                
            }
            case ACTIVATESUCCESSPOPUP:
            // alert("logoutSuccess")      
            // alert("b")     
            // console.log(action.payload.Token)
            return{
                ...state,
                ActivateSuccPopupState:!state.ActivateSuccPopupState
                
            }
            case ACTIVATESUCCESSPOPUPOTHER:
            // alert("logoutSuccess")      
            // alert("b")     
            // console.log(action.payload.Token)
            return{
                ...state,
                ActivateSuccPopupOtherState:!state.ActivateSuccPopupOtherState
                
            }
        case LOGINFAILS:
            // alert("loginFails") 
                return {
                    ...state,
                ErrMsg:["Invalid Login details.Are you trying to Sign up?"]  ,
                loading: false 
                        }          

        case SIGNUP:
            // alert("SignupRed") 
            // console.log(action.payload);
            
            // console.log(action.payload.id)
            localStorage.setItem('user', action.payload.Token)
            return{
                ...state,
                // acount:[action.payload],
                userId:[action.payload.id],
                token:[action.payload.Token],
                LoginBackdrop:!state.LoginBackdrop,                
                status:"Thank You For Joining MyPustak Community",
                loading: false
            }

        case SIGNEDUP:
            // alert("ASigned") 
            // console.log(action.payload);
            
            // console.log(action.payload.id)
            return{
                ...state,
                // ErrMsg:action.payload,
                SignUpErrMsg:action.payload,
                loading: false,
                
            }
        
        case GETADDRESS:
            // alert('redu')
                return {
                    ...state,
                    getadd:action.payload.output,
                }

        case ADD_ADDRESS:
            // alert('redu')
                return {
                    ...state,
                    // getadd:action.payload.output,
                    getuseradd:action.payload,
                }
            case EDITUSER_ADD:
                // alert('redu')
                    return {
                        ...state,
                        getuseradd:action.payload,
                    }
            case EDITADDRESS:
                    // alert('redu')
                    return {
                            ...state,
                            editadd:action.payload,
                        }
            case GETSELECTEDADDRESS:
                // alert('redu')
                return {
                    ...state,
                    selectedAddress:action.payload,
                    
                }
            case SETSELECTEDADDRESSBLANK:
                // alert('redu')
                return {
                    ...state,
                    selectedAddress:[],
                    
                }
            case GETUSERD:
                // alert('redu')
                return {
                    ...state,
                    getuserdetails:action.payload,
                 
                }
            
        default:
        return state;
    }
}
