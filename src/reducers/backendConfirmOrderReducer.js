// import { LOGIN,SIGNUP,SIGNEDUP ,LOGINFAILS,LOGOUT,SHOWLOADER,GETADDRESS,ADD_ADDRESS } from '../actions/types';
import { ORDERSLOADING,
  GETORDERS,
  ORDERUPDATE,
  CANCELORDER,
  SENDINGEMAIL,
  SENTEMAIL,
  CPRECOMMENDATIONS,
  CPLOADING,
  FETCH_PENDING_COD,
  CLEAR_FETCH_PENDING_COD
} from "../actions/types";
const initialState = {
  orders: {},
  confirmationPendingOrders: [],
  loading: false,
  sendingEmail: false,
  recommendation: {},
  recommendations: [],
  recommendationLoding: false,
  codPending: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GETORDERS:

      let totalOrders = action.payload.orders;

      let confirmOrders = action.payload.confirmOrders;
      let confirmationPendingOrders = action.payload.confirmationPendingOrders;

      let orders = {};
      let allOrdersWithBooks = action.payload.allOrdersWithBooks;
      let chunk = 10;
      let counter = 0;

      confirmOrders = confirmOrders.map(order => {
        return Object.assign(order, { books: allOrdersWithBooks.filter(bookorder => bookorder.order_id === order.order_id) })
      })

      let tempConfirmOrders = [...confirmOrders];
      for(let i = 0; i <= confirmOrders.length; i+=chunk) {
        let orderschunk = [];
        if(tempConfirmOrders.length < chunk) {
          orderschunk = tempConfirmOrders.splice(0, tempConfirmOrders.length);
        } else {
          orderschunk = tempConfirmOrders.splice(0, chunk);
        }
        let rackBooksOfOrderChunk= [];

        orderschunk.forEach(order => {
          rackBooksOfOrderChunk.push(...allOrdersWithBooks.filter(book => book.order_id === order.order_id));
        })

        let orderElem = {};
        orderElem.orders = orderschunk;
        orderElem.rackBooksOfOrderChunk = rackBooksOfOrderChunk;
        orders[counter] = orderElem;
        counter++;
      }
      return {
        ...state,
        orders: Object.assign(state.orders, orders),
        confirmationPendingOrders,
        loading: false
      };
    case ORDERSLOADING: 
      return {
        ...state,
        loading: true,
      }
    case ORDERUPDATE:
      let updatedConfirmationPendingOrders = state.confirmationPendingOrders.filter(order => order.order_id !== action.payload.order.order_id)
      return {
        ...state,
        confirmationPendingOrders: updatedConfirmationPendingOrders,
        loading: false,
      }
    case CANCELORDER:
      updatedConfirmationPendingOrders = state.confirmationPendingOrders.filter(order => order.order_id !== action.payload.order.order_id)
      return {
        ...state,
        confirmationPendingOrders: updatedConfirmationPendingOrders,
        loading: false,
      }
    case SENDINGEMAIL:
      return {
        ...state,
        sendingEmail: true
      }
    case SENTEMAIL:
      return {
        ...state,
        sendingEmail: false
      }
    case CPLOADING:
      return {
        ...state,
        recommendationLoding: true
      }
    case CPRECOMMENDATIONS: 
    console.log(action.payload, "Asdfasdfasdf")
      return {
        ...state,
        recommendation: action.payload.data,
        recommendations: action.payload.data[0].preference_array,
        recommendationLoding: false
      }
    case CLEAR_FETCH_PENDING_COD:
    console.log(action.payload,"cOD");
    return {
      ...state,
      codPending:{}
    }
    case FETCH_PENDING_COD:
    console.log(action.payload,"cOD");
    return {
      ...state,
      codPending:action.payload
    }
    
    default:
      return state;
  }
}