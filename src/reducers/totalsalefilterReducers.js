import { TOTAL_SALE_THIS_MONTH, GET_TOTALSALE_BY_DATE_RANGE } from "../actions/types";

const initialState = {
  sales: {},
  salethismonth: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case TOTAL_SALE_THIS_MONTH:
      return {
        ...state,
        sales: action.payload
      };

    case GET_TOTALSALE_BY_DATE_RANGE:
      return {
        ...state,
        salethismonth: action.payload
      }

    default:
      return state;
  }
}
