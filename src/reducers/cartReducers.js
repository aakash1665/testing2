import {ADDTOCART,
    POPUPCART,
    closePOPUPCART,
    REMOVEALLCART,
    REMOVECARTLOGOUT,
    SHOWRZPAYERR,
    REMOVECART,
    ADDCARTPRICE,
    SENDADDRESSId,
    ORDERDETAILS,
    CARTSESSION,
    TOBEADDEDTOCART,
    REMOVETOBEADDEDTOCART,
    MOBILECARTREDIRECT,
    SETORDERID,
    WALLETRAZORPAY,
    REDIRECTWALLETTOCART,
    UPDATEORDERSUCC,
    OUTOFSTOCK} from '../actions/types';
const initialState = {
    MyCart:[],
    // CartLength:
    PopupCart:false,
    cartLength:0,
    CartPrice:[],
    AddresId:0,
    OrderId:0,
    TotalPrice:0,
    walletRazorpayState:[],
    redirectWallettoCartState:false,
    CartSessionData:[],
    ToBeAddedToCartState:[],
    cartRedirectMobile:false,
    RzPayErr:false,
    GetLoginData:[],
    orderSuccess:false,
    OutOfStockBooks:[],
}
export default function(state=initialState,action)
{
    switch(action.type){
        case ADDTOCART:
        // alert('redu')
            return {
                ...state,
                MyCart:state.MyCart.concat(action.payload),
                cartLength:state.cartLength+1,
            }
        case REMOVECART:
        // alert('redu')
        if(state.cartLength <= 0){
            return {
                ...state,
                cartLength:0,}
            }else{
            return {
                ...state,
                MyCart:state.MyCart.filter(cart=>cart.bookInvId !== action.payload),
                cartLength:state.cartLength-1,
            }
        }

        case REMOVECARTLOGOUT:
            // alert('redu')
            // alert(state.cartLength)
            if(state.cartLength <= 0){
                alert("0")
                return {
                    ...state,
                    cartLength:0,
                }
            }else{
                return {
                    ...state,
                    MyCart:state.MyCart.filter(cart=>cart.bookInvId !== action.payload),
                    cartLength:state.cartLength-1,
                }}
        case ADDCARTPRICE:
            return {
                ...state,
                CartPrice:action.payload,
                TotalPrice:action.payload.TotalPayment,
            }
        case POPUPCART:
        // alert('redu')
            return {
                ...state,
                PopupCart:!state.PopupCart
            }
        case UPDATEORDERSUCC:
        // alert('redu')
            return {
                ...state,
                orderSuccess:!state.orderSuccess
            }

        // case SENDLOGINDATA:
        //     // alert('redu')
        //         return {
        //             ...state,
        //             PopupCart:action.payload
        //         }
        case MOBILECARTREDIRECT:
            // alert('redu')
                return {
                    ...state,
                    cartRedirectMobile:!state.cartRedirectMobile
                }
        case CARTSESSION:
            return {
                ...state,
                CartSessionData:action.payload,
            }
        case SHOWRZPAYERR:
            return {
                ...state,
                RzPayErr:!state.RzPayErr,
            }
        case TOBEADDEDTOCART:
            return {
                ...state,
                ToBeAddedToCartState:state.ToBeAddedToCartState.concat(action.payload),
            }
        case REMOVETOBEADDEDTOCART:
            return {
                ...state,
                ToBeAddedToCartState:[],
            }
        case REMOVEALLCART:
        // alert('redu')
            return {
                ...state,
                MyCart:[],
                cartLength:0,
                CartSessionData:[],
            }
        case SENDADDRESSId:
            // alert('redu')
                return {
                    ...state,
                    AddresId:action.payload,
                
                }
        case ORDERDETAILS:
            // alert('redu')
                return {
                    ...state,
                    OrderId:action.payload.order_id,
                
                }
        case SETORDERID:
            // alert('redu')
                return {
                    ...state,
                    OrderId:0,
                
                }
        case closePOPUPCART:
        // alert('redu')
            return {
                ...state,
                PopupCart:action.payload
            }
        case WALLETRAZORPAY:
            // alert('redu')
                return {
                    ...state,
                    walletRazorpayState:action.payload
                }
        case REDIRECTWALLETTOCART:
            // alert('redu')
                return {
                    ...state,
                    redirectWallettoCartState:!state.redirectWallettoCartState
                }
        case OUTOFSTOCK:
            // alert('redu')
                return {
                    ...state,
                    OutOfStockBooks:action.payload.bookOutOstack
                }
            default:
            return state;
        }
    
}