// import React, { Component } from 'react'
// import { LOGIN,SIGNUP } from "../action/types";
import { GETORDERD,
    CANCELORDER,
    SETRELOADLORDER,
    CONVERTORDER,
    VIEWORDERDETAILS,
    MAKECANCELMSGBLANK,
    CONVERTORDERSENDREQ,
    UPDATEPAYMENT,
} from '../actions/types';
const initialState = {
    getorderdetails:[    

    ],
    // userId:undefined,
    token:null,
    // userId:6059,

    ErrMsg:[],
    CancelOrderHit:[],
    ReloadOrders:false,
    convertOrderState:"",
    singleOrderDetails:[],
    // donation_req_id:'123456',
    // app_books_weight:'',
    // email:'mukul.meri@gmail.com',
    // name:'abc xyz'
    LoaderConvOrder:true,
    GoToThanksState:false,
};

export default function(state=initialState,action)
{
    switch(action.type){ 
        case GETORDERD:
        // alert('redu')
        return {
            ...state,
            // getorderdetails:state.getorderdetails.concat(action.payload.output),
            getorderdetails:action.payload.output,

         
        }
        case CANCELORDER:
        // alert('true')
        return {
            ...state,
            CancelOrderHit:action.payload,
            ReloadOrders:!state.ReloadOrders,
         
        }
        case CONVERTORDER:
        // alert('true')
        return {
            ...state,
        convertOrderState:action.payload,
        LoaderConvOrder:false,
         
        }
        case VIEWORDERDETAILS:
        // alert('true')
        return {
            ...state,
            singleOrderDetails:action.payload.output,
         
        }
        case SETRELOADLORDER:
        // alert('false')
        return {
            ...state,
            ReloadOrders:false,
         
        }
        case MAKECANCELMSGBLANK:
        // alert('true')
        return {
            ...state,
        // convertOrderState:action.payload,
        CancelOrderHit:[],
        }
        default:
        return state;
    }
}
