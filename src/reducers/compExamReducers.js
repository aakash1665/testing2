import {GET_COMPEXAM,GET_FRICTION,GET_BOOK,GET_BOOK_ERR} from '../actions/types'

const initialState = {
    compExamBooks:[],
    // FrictionBooks:[],
    book:[],
    bookCondition:[],
    WrongSlug:false,
};
export default function(state=initialState,action)
{
    switch(action.type){
        case GET_COMPEXAM:
        // alert('redu')
            return {
                ...state,
                compExamBooks:action.payload,
            }
        case GET_FRICTION:
        // alert('redu')
            return {
                ...state,
                FrictionBooks:action.payload
            }
        case GET_BOOK:
        // alert('Get')
            return {
                ...state,
                book:action.payload.product_details,
                bookCondition:action.payload.product_condition,
                WrongSlug:false,
                }
        case GET_BOOK_ERR:
            return {
                ...state,
                WrongSlug:true,
            }
        default:
        return state;
    }
}
