import { SEARCH,GETSEARCH ,FETCHSEARCH,MAKESEARCHBLANK,GETSEARCHGETBOOK,SETCATEGORY} from '../actions/types';
const initialState = {
    searchBook:'',
    SearchResult:[],
    ClickedCategory:'',
    ActivateSuccPopup:false,

};
export default function(state=initialState,action)
{
    switch(action.type){ 
        case SEARCH:
            // alert("in Hreduc") 
            // console.log(action.payload);
            return{
                ...state,
               searchBook:action.payload
                
            }
        case MAKESEARCHBLANK:
            // alert("in Hreduc") 
            // console.log(action.payload);
            return{
                ...state,
               searchBook:[]
                
            }
        case GETSEARCH:
            // alert("in Hreduc") 
            // console.log(action.payload);
            return{
                ...state,
                SearchResult:action.payload,
                // this.state.SearchResult.concat(res.data.hits).then(res=> {this.setState({SearchResult:res.data.hits});
                
            }
        // case GETSEARCHGETBOOK:
        //     // alert("in Hreduc") 
        //     console.log(action.payload);
        //     return{
        //         ...state,
        //         SearchResult:action.payload,
        //         // this.state.SearchResult.concat(res.data.hits).then(res=> {this.setState({SearchResult:res.data.hits});
                
        //     }

        case FETCHSEARCH:
            // alert("in Hreduc") 
            // console.log(action.payload);
            return{
                ...state,
                SearchResult:state.SearchResult.concat  (action.payload),
                // this.state.SearchResult.concat(res.data.hits).then(res=> {this.setState({SearchResult:res.data.hits});
                
            }
        
        case SETCATEGORY:
            return{
                ...state,
                ClickedCategory:action.payload,
            }
        default:
            return state;
        }
    }