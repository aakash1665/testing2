import {
  GET_EMAIL_AND_USER_ID_IN_WALLET,
  SEARCH_IN_WALLET
} from "../actions/types";

const initialState = {
  userdetails: {},
  SearchResults: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_EMAIL_AND_USER_ID_IN_WALLET:
      return {
        ...state,
        userdetails: action.payload
      };

    case SEARCH_IN_WALLET:
      return {
        ...state,
        SearchResults: action.payload
      };

    default:
      return state;
  }
}
