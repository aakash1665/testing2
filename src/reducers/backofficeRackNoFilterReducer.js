import {
  FILTER_ALL_BOOKS_AGAINST_RACK_NO
  // RACK_FILTER_LOADER
} from "../actions/types";

const initialState = {
  rackData: {},
  loader: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case FILTER_ALL_BOOKS_AGAINST_RACK_NO:
      return {
        ...state,
        rackData: action.payload,
        loader: true
      };

    // case RACK_FILTER_LOADER:
    //   return {
    //     ...state,
    //     loading: true
    //   };

    default:
      return state;
  }
}
