import {GET_GOOGLE_BOOKS} from "../actions/types";

const initialState={
    FetchechedGoogleBooks:{}
}

export default function(state = initialState,action){
    switch (action.type) {
        case GET_GOOGLE_BOOKS:
            return{
                ...state,
                FetchechedGoogleBooks:action.payload.data
            }
    
        default:
            return state;
    }
}
