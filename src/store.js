import {createStore,applyMiddleware,compose} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers'  //import the index.js from reducers folder 

const initialState = {};
const middleware =[thunk]
// const store = createStore(rootReducer,initialState,compose(applyMiddleware(...middleware),window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
const store = createStore(rootReducer ,initialState,compose(applyMiddleware(...middleware),
)
); // Add window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__() as above to connect store to redux dev-tool
export default store;